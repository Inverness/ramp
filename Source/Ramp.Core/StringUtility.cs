using System.Text;

namespace Ramp
{
    public static class StringUtility
    {
        /// <summary>
        ///		Get a hexadecimal representation of a byte array using the 'x2' format for each byte.
        /// </summary>
        /// <param name="value"> The byte array. </param>
        /// <returns> A hexadecimal string. </returns>
        public static string GetHexString(byte[] value)
        {
            if (value == null)
                return null;

            StringBuilder sb = StringBuilderCache.Acquire();
            for (int i = 0; i < value.Length; i++)
                sb.Append(value[i].ToString("x2"));
            return StringBuilderCache.ReleaseAndGetString(sb);
        }
    }
}