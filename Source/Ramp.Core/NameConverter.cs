﻿using System;
using System.ComponentModel;
using System.Globalization;

namespace Ramp
{
    public class NameConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string))
                return true;
            return base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            string stringValue;
            if ((stringValue = value as string) != null)
                return new Name(stringValue);
            return base.ConvertFrom(context, culture, value);
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
        {
            if (destinationType == typeof(string))
                return ((Name) value).ToString();
            return base.ConvertTo(context, culture, value, destinationType);
        }
    }
}