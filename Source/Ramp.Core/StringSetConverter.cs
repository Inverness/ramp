﻿using System.ComponentModel;
using System.Globalization;
using Ramp.Data;

namespace Ramp
{
    /// <summary>
    ///		A type converter for StringSet. Supports conversion to and from comma-delimited strings.
    /// </summary>
    public class StringSetConverter : StringTypeConverter
    {
        protected override object ConvertFromStringCore(ITypeDescriptorContext context, CultureInfo culture,
                                                        string value)
        {
            return StringSet.Parse(value);
        }
    }
}