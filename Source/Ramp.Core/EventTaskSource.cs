using System;
using System.Threading.Tasks;

namespace Ramp
{
    // Copied from: http://labs.vectorform.com/2012/02/asynchronous-ui-development-in-winrt-silverlight-windows-phone-wpf-with-asyncawait-keywords-of-c-5-0/
    public sealed class EventTaskSource<TSender, TEventArgs>
    {
        private TaskCompletionSource<EventTaskResult<TSender, TEventArgs>> _tcs;
        private readonly Action<TypedEventHandler<TSender, TEventArgs>> _removeHandler;

        public EventTaskSource(
            Action<TypedEventHandler<TSender, TEventArgs>> addHandler,
            Action<TypedEventHandler<TSender, TEventArgs>> removeHandler = null,
            Action beginAction = null
            )
        {
            Validate.ArgumentNotNull(addHandler, nameof(addHandler));
            
            _removeHandler = removeHandler;
            addHandler.Invoke(OnEventCompleted);

            beginAction?.Invoke();
        }

        public Task<EventTaskResult<TSender, TEventArgs>> Task
        {
            get
            {
                if (_tcs == null)
                    _tcs = new TaskCompletionSource<EventTaskResult<TSender, TEventArgs>>();
                return _tcs.Task;
            }
        }

        private void OnEventCompleted(TSender sender, TEventArgs e)
        {
            _removeHandler?.Invoke(OnEventCompleted);

            if (_tcs != null)
            {
                TaskCompletionSource<EventTaskResult<TSender, TEventArgs>> localTcs = _tcs;
                _tcs = null;
                localTcs.SetResult(new EventTaskResult<TSender, TEventArgs>(sender, e));
            }
        }
    }

    public struct EventTaskResult<TSender, TEventArgs>
    {
        public readonly TSender Sender;

        public readonly TEventArgs E;

        public EventTaskResult(TSender sender, TEventArgs e)
        {
            Sender = sender;
            E = e;
        }
    }
}