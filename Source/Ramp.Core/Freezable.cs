using System;
using System.Diagnostics;
using Newtonsoft.Json;

namespace Ramp
{
    /// <summary>
    ///     Provides a default implementation of IFreezable.
    /// </summary>
    public abstract class Freezable : IFreezable
    {
        [JsonIgnore]
        public virtual bool CanFreeze => IsFrozen || FreezeCore(true);

        [JsonIgnore]
        public bool IsFrozen { get; private set; }

        public IFreezable Clone()
        {
            Freezable clone = CreateInstance();
            clone.CloneCore(this, true);
            return clone;
        }

        public void Freeze()
        {
            if (IsFrozen)
                return;
            FreezeCore(false);
            IsFrozen = true;
        }

        public IFreezable GetAsFrozen()
        {
            if (IsFrozen)
                return this;
            Freezable clone = CreateInstance();
            clone.CloneCore(this, false);
            clone.Freeze();
            return clone;
        }

        protected Freezable CreateInstance()
        {
            Freezable clone = CreateInstanceCore();
            Debug.Assert(clone != null, "clone != null");
            Debug.Assert(GetType() == clone.GetType(), "GetType() == clone.GetType()");
            Debug.Assert(!clone.IsFrozen, "!clone.IsFrozen");
            return clone;
        }

        /// <summary>
        ///     Subclasses must implement this to create a new instance of the current object.
        /// </summary>
        /// <returns></returns>
        protected abstract Freezable CreateInstanceCore();

        /// <summary>
        ///     Freezes the current object or checks if it can be frozen.
        /// </summary>
        /// <param name="check">True if only checking for the ability to freeze.</param>
        /// <returns>True if the current object was frozen or can be frozen, otherwise false.</returns>
        protected virtual bool FreezeCore(bool check)
        {
            return true;
        }

        /// <summary>
        ///     Deep clones members of the current object or ensures they're frozen.
        /// </summary>
        /// <param name="source">The source object that members are being copied from.</param>
        /// <param name="cloneFrozen">True if already frozen objects should be cloned.</param>
        protected virtual void CloneCore(Freezable source, bool cloneFrozen)
        {
        }

        /// <summary>
        ///     Throws an exception if the current object is frozen.
        /// </summary>
        protected void VerifyNotFrozen()
        {
            if (IsFrozen)
                throw new InvalidOperationException("Object is frozen");
        }

        /// <summary>
        ///     A helper method. Decides whether to invoke Clone() or GetAsFrozen() on the target depending on
        ///     whether cloneFrozen is true. If the target is not freezable, it is returned without change.
        /// </summary>
        /// <typeparam name="T">The target type.</typeparam>
        /// <param name="target"></param>
        /// <param name="cloneFrozen"></param>
        /// <returns></returns>
        public static T Clone<T>(T target, bool cloneFrozen)
            where T : class
        {
            var freezable = target as IFreezable;
            if (freezable == null)
                return target;

            return (T) (cloneFrozen ? freezable.Clone() : freezable.GetAsFrozen());
        }

        /// <summary>
        ///     A helper method. Decides whether to invoke Freeze() or get CanFreeze on the target depending on whether
        ///     check is true. If the target is null, returns true.
        /// </summary>
        /// <param name="target"></param>
        /// <param name="check"></param>
        /// <returns></returns>
        public static bool Freeze(IFreezable target, bool check)
        {
            if (target != null)
            {
                if (check)
                    return target.CanFreeze;

                target.Freeze();
            }

            return true;
        }

        /// <summary>
        ///     A helper method. Decides whether to invoke Freeze() or get CanFreeze on the target depending on whether
        ///     check is true. If the target is null or not IFreezable, returns true.
        /// </summary>
        /// <param name="target"></param>
        /// <param name="check"></param>
        /// <returns></returns>
        public static bool Freeze(object target, bool check)
        {
            var freezable = target as IFreezable;
            return freezable == null || Freeze(freezable, check);
        }
    }
}