﻿using System;

namespace Ramp
{
    public static class RandomExtensions
    {
        public static double NextDouble(this Random random, double min, double max)
        {
            return Lerp((float) min, (float) max, (float) random.NextDouble());
        }

        public static float NextSingle(this Random random, float min, float max)
        {
            return Lerp(min, max, (float) random.NextDouble());
        }

        private static float Lerp(float value1, float value2, float amount)
        {
            return value1 + (value2 - value1) * amount;
        }
    }
}