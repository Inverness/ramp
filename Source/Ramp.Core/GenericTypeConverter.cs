﻿using System;
using System.ComponentModel;
using Ramp.Utilities.Reflection;

namespace Ramp
{
    /// <summary>
    /// Provides methods for generically converting between types.
    /// </summary>
    public static class GenericTypeConverter
    {
        public static readonly char[] SpaceDelim = { ' ' };
        public static readonly char[] CommaDelim = { ',' };

        private static readonly Type s_convertibleType = typeof(IConvertible);
        private static readonly Type s_enumType = typeof(Enum);

        /// <summary>
        ///     Convert an object to the specified type.
        /// </summary>
        /// <typeparam name="T"> The destination type. </typeparam>
        /// <param name="value"> A value to convert. </param>
        /// <returns> A converted value. </returns>
        /// <exception cref="InvalidCastException"> Conversion to the destination type failed. </exception>
        public static T Convert<T>(object value)
        {
            return (T) Convert(typeof(T), value);
        }

        public static object Convert(Type destType, object value)
        {
            Validate.ArgumentNotNull(destType, "destType");

            if (destType.IsInstanceOfType(value))
                return value;

            if (value == null)
            {
                if (destType.IsValueType && Nullable.GetUnderlyingType(destType) == null)
                    throw new InvalidCastException("attempt to convert null to ValueType");
                return null;
            }

            // Primitive types are much more common, so try ChangeType first which will be faster.
            if (s_convertibleType.IsAssignableFrom(destType) && destType.BaseType != s_enumType)
            {
                try
                {
                    return System.Convert.ChangeType(value, destType);
                }
                catch (InvalidCastException)
                {
                }
                catch (FormatException)
                {
                }
            }

            // Fallback to a full TypeConverter
            Type valueType = value.GetType();
            TypeConverter destinationConverter = TypeConverterCache.GetConverter(destType);

            if (destinationConverter.CanConvertFrom(valueType))
            {
                try
                {
                    return destinationConverter.ConvertFrom(value);
                }
                catch (Exception ex)
                {
                    throw new InvalidCastException("destination from source conversion failed", ex);
                }
            }

            TypeConverter sourceConverter = TypeConverterCache.GetConverter(valueType);
            if (sourceConverter.CanConvertTo(destType))
            {
                try
                {
                    return sourceConverter.ConvertTo(value, destType);
                }
                catch (Exception ex)
                {
                    throw new InvalidCastException("source to destination conversion failed", ex);
                }
            }

            // Attempt to coerce using string conversion
            if (destinationConverter.CanConvertFrom(typeof(string)))
            {
                string stringValue = sourceConverter.ConvertToString(value);

                try
                {
                    return destinationConverter.ConvertFrom(stringValue);
                }
                catch (Exception ex)
                {
                    throw new InvalidCastException("value coercion failed with string coerce", ex);
                }
            }

            throw new InvalidCastException($"Unable to coerce value from {value.GetType().FullName} " +
                                           $"to {destType.FullName}");
        }

        public static bool TryConvert(Type destType, object value, out object result)
        {
            try
            {
                result = Convert(destType, value);
                return true;
            }
            catch (InvalidCastException)
            {
                result = null;
                return false;
            }
        }

        public static bool TryConvert<T>(object value, out T result)
        {
            try
            {
                result = Convert<T>(value);
                return true;
            }
            catch (InvalidCastException)
            {
                result = default(T);
                return false;
            }
        }
    }
}