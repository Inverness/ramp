﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Ramp.Utilities.Text;

namespace Ramp
{
    /// <summary>
    ///     A hashed set of strings that can be serialized as a single string.
    /// </summary>
    [TypeConverter(typeof(StringSetConverter))]
    public class StringSet : HashSet<string>
    {
        /// <summary>
        ///     Initialize an empty set.
        /// </summary>
        public StringSet()
        {
        }

        /// <summary>
        ///     Initializes a set with the specified collection.
        /// </summary>
        /// <param name="collection"> A string collection. </param>
        public StringSet(IEnumerable<string> collection)
            : base(collection)
        {
        }

        /// <summary>
        ///		Adds the values from a collection.
        /// </summary>
        /// <param name="collection"> A string collection. </param>
        /// <returns> The number of elements added that were not already present. </returns>
        public int AddRange(IEnumerable<string> collection)
        {
            Validate.ArgumentNotNull(collection, "collection");
            return collection.Count(Add);
        }

        public override string ToString()
        {
            return DelimitedStringParser.ToCommaDelimited(this);
        }

        public static StringSet Parse(string value)
        {
            Validate.ArgumentNotNull(value, "value");

            return new StringSet(DelimitedStringParser.ParseCommaDelimited(value, true));
        }
    }
}