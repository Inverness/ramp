﻿using System;
using System.ComponentModel;

namespace Ramp
{
    /// <summary>
    ///     A string that is pre-hashed and can be optionally numbered to distinguish it from others with the same value.
    /// </summary>
    [TypeConverter(typeof(NameConverter))]
    public readonly struct Name : IEquatable<Name>, IComparable<Name>
    {
        private const char Separator = '#';

        private readonly string _value;
        private readonly int _valueHash;
        private readonly uint _number;

        /// <summary>
        ///     An empty name.
        /// </summary>
        public static readonly Name Empty = new Name();

        /// <summary>
        ///     Initializes a Name instances with a string value and number.
        /// </summary>
        /// <param name="value">
        ///     A string value for the name. If noSplit is false and number is 0, this will be parsed to extract a
        ///     number suffix.
        /// </param>
        /// <param name="number"> The string number. </param>
        /// <param name="noSplit"> True if the string value should not be parsed for a number suffix. </param>
        public Name(string value, uint number = 0, bool noSplit = false)
        {
            if (value != null)
            {
                if (value.Length == 0)
                {
                    value = null;
                }
                else if (number == 0 && !noSplit && Split(value, out value, out number))
                {
                    // catch casees like "#123"
                    if (value.Length == 0)
                        value = null;
                }
            }

            _value = value;
            _valueHash = value?.GetHashCode() ?? 0;
            _number = number;
        }

        private Name(string value, int valueHash, uint number)
        {
            _value = value;
            _valueHash = valueHash;
            _number = number;
        }

        /// <summary>
        /// Gets the number of this name.
        /// </summary>
        public uint Number => _number;

        /// <summary>
        /// Gets the string value of this name.
        /// </summary>
        public string Value => _value ?? string.Empty;
        
        /// <summary>
        /// Gets whether the name has an empty value and a number of zero.
        /// </summary>
        public bool IsEmpty => _value == null && _number == 0;

        public override bool Equals(object obj)
        {
            if (obj is Name)
                return Equals((Name) obj);
            return false;
        }

        public override int GetHashCode()
        {
            return unchecked (_valueHash + (int) _number);
        }

        public override string ToString()
        {
            if (_value == null)
            {
                return _number == 0 ? string.Empty : _number.ToString();
            }
            else
            {
                return _number == 0 ? _value : _value + Separator + _number;
            }
        }

        public bool Equals(Name other)
        {
            return _valueHash == other._valueHash && _number == other._number && _value == other._value;
        }

        public int CompareTo(Name other)
        {
            if (_valueHash == other._valueHash && _value == other._value)
                return _number == other._number ? 0 : (_number < other._number ? -1 : 1);

            return string.Compare(_value, other._value, StringComparison.Ordinal);
        }

        /// <summary>
        ///     Creates a new name instance with the same string value and the specified number.
        /// </summary>
        /// <param name="number"> A number for the new name instance. </param>
        /// <returns> A name with the same string value and the specified number. </returns>
        public Name WithNumber(uint number)
        {
            return new Name(_value, _valueHash, number);
        }

        public static bool operator ==(Name left, Name right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(Name left, Name right)
        {
            return !left.Equals(right);
        }

        public static bool operator <(Name left, Name right)
        {
            return left.CompareTo(right) < 0;
        }

        public static bool operator >(Name left, Name right)
        {
            return left.CompareTo(right) > 0;
        }

        public static bool operator <=(Name left, Name right)
        {
            return left.CompareTo(right) <= 0;
        }

        public static bool operator >=(Name left, Name right)
        {
            return left.CompareTo(right) >= 0;
        }

        public static Name operator +(Name left, Name right)
        {
            string value = left.ToString() + right.ToString();
            return value.Length != 0 ? new Name(value) : Empty;
        }

        //public static implicit operator string(Name name)
        //{
        //    return name.ToString();
        //}

        private static bool Split(string value, out string newValue, out uint number)
        {
            int i;
            uint n;
            if (!char.IsDigit(value[value.Length - 1]) ||
                (i = value.LastIndexOf(Separator)) == -1 ||
                !uint.TryParse(value.Substring(i + 1), out n))
            {
                newValue = value;
                number = 0;
                return false;
            }

            newValue = value.Substring(0, i);
            number = n;
            return true;
        }
    }
}
