using System.Text.RegularExpressions;

namespace Ramp.Utilities.Text
{
    public static class WildcardPattern
    {
        private static readonly char[] s_wildcardCharacters = { '*', '?' };

        /// <summary>
        ///		Converts a simple wildcard pattern using * and ? to an equivalent regex pattern.
        /// </summary>
        /// <param name="pattern"> The wildcard pattern. </param>
        /// <returns> A string representing an equivalent regex pattern. </returns>
        public static string ToRegex(string pattern)
        {
            Validate.ArgumentNotNull(pattern, "pattern");

            return "^" + Regex.Escape(pattern).Replace(@"\*", ".*").Replace(@"\?", ".") + "$";
        }

        /// <summary>
        ///		Check if a string represents a wildcard pattern.
        /// </summary>
        /// <param name="value"> The string to check. </param>
        /// <returns> True if the value is a wildcard pattern. </returns>
        public static bool IsWildcardPattern(string value)
        {
            Validate.ArgumentNotNull(value, "value");

            return value.IndexOfAny(s_wildcardCharacters) != -1;
        }
    }
}