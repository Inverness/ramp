using System.Linq;
using System.Text.RegularExpressions;

namespace Ramp.Utilities.Text
{
    public static class StringTokenizer
    {
        private static readonly Regex s_tokenizeRegex = new Regex(@"""(.*)""|(\S+)");

        /// <summary>
        ///     Retrieves tokens from a string in the form of single words or quoted phrases with spaces.
        /// </summary>
        /// <param name="input"> The string to tokenize. </param>
        /// <returns> An array containing tokens from the input. </returns>
        public static string[] Tokenize(string input)
        {
            Validate.ArgumentNotNull(input, "input");

            return s_tokenizeRegex.Matches(input).Cast<Match>().Select(m => m.Value).ToArray();
        }
    }
}