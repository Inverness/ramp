﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Ramp.Utilities.Text
{
    public class NaturalSortComparer : IComparer<string>
    {
        private readonly bool _descending;
        private readonly StringComparison _stringComparison;
        private readonly Dictionary<string, string[]> _table = new Dictionary<string, string[]>();
        private readonly Regex _splitPattern = new Regex("([0-9]+)");

        public NaturalSortComparer(bool descending)
            : this(descending, StringComparison.CurrentCulture)
        {
        }

        public NaturalSortComparer(bool descending, StringComparison stringComparison)
        {
            _descending = descending;
            _stringComparison = stringComparison;
        }

        public int Compare(string x, string y)
        {
            if (x == y)
                return 0;

            string[] x1, y1;

            if (!_table.TryGetValue(x, out x1))
            {
                x1 = _splitPattern.Split(x.Replace(" ", ""));
                _table.Add(x, x1);
            }

            if (!_table.TryGetValue(y, out y1))
            {
                y1 = _splitPattern.Split(y.Replace(" ", ""));
                _table.Add(y, y1);
            }

            int returnVal;

            for (int i = 0; i < x1.Length && i < y1.Length; i++)
            {
                if (x1[i] != y1[i])
                {
                    returnVal = PartCompare(x1[i], y1[i]);
                    return _descending ? -returnVal : returnVal;
                }
            }

            if (y1.Length > x1.Length)
                returnVal = 1;
            else if (x1.Length > y1.Length)
                returnVal = -1;
            else
                returnVal = 0;

            return _descending ? -returnVal : returnVal;
        }

        private int PartCompare(string left, string right)
        {
            int x, y;

            if (!int.TryParse(left, out x))
                return string.Compare(left, right, _stringComparison);

            if (!int.TryParse(right, out y))
                return string.Compare(left, right, _stringComparison);

            return x.CompareTo(y);
        }
    }
}