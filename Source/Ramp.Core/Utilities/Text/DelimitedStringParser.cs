using System;
using System.Collections.Generic;

namespace Ramp.Utilities.Text
{
    public static class DelimitedStringParser
    {
        private static readonly char[] s_delimiters = { ',' };

        public static string[] ParseCommaDelimited(string value, bool removeEmpty = false)
        {
            Validate.ArgumentNotNull(value, "value");

            StringSplitOptions options = removeEmpty ? StringSplitOptions.RemoveEmptyEntries : StringSplitOptions.None;
            string[] parts = value.Split(s_delimiters, options);

            for (int i = 0; i < parts.Length; i++)
                parts[i] = parts[i].Trim();

            return parts;
        }

        public static string ToCommaDelimited<T>(IEnumerable<T> values, bool removeEmpty = false)
        {
            Validate.ArgumentNotNull(values, "values");

            var sb = StringBuilderCache.Acquire();

            foreach (T v in values)
            {
                string s = v != null ? v.ToString() : null;
                if (!removeEmpty || !String.IsNullOrWhiteSpace(s))
                    sb.Append(s).Append(", ");
            }

            if (sb.Length != 0)
                sb.Length -= 2; // if anything was inserted, trim the last comma and space.

            return StringBuilderCache.ReleaseAndGetString(sb);
        }
    }
}