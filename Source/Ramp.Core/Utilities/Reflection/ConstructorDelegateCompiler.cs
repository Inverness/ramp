using System;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;

namespace Ramp.Utilities.Reflection
{
    /// <summary>
    /// Provides methods for compiling delegates using expression trees.
    /// </summary>
    public static class ConstructorDelegateCompiler
    {
        /// <summary>
        /// Compiles a delegate that invokes the constructor with a matching signature.
        /// </summary>
        /// <typeparam name="T"> A delegate type. </typeparam>
        /// <param name="type"> The type to construct. </param>
        /// <returns> A delegate that can be used to construct an instance of the specified type. </returns>
        public static T Compile<T>(Type type)
        {
            return (T) (object) Compile(typeof(T), type);
        }

        /// <summary>
        /// Compiles a delegate that invokes the constructor with a matching signature.
        /// </summary>
        /// <param name="delegateType"> A delegate type. </param>
        /// <param name="constructType"> A type to construct. </param>
        /// <returns> A delegate that can be used to construct an instance of the specified type. </returns>
        public static Delegate Compile(Type delegateType, Type constructType)
        {
            if (delegateType == null)
                throw new ArgumentNullException(nameof(delegateType));
            if (constructType == null)
                throw new ArgumentNullException(nameof(constructType));
            if (!typeof(MulticastDelegate).IsAssignableFrom(delegateType))
                throw new ArgumentOutOfRangeException(nameof(delegateType), "must be a delegate");
            if (constructType.IsAbstract)
                throw new ArgumentOutOfRangeException(nameof(constructType), "type must not be abstract");

            // The Invoke method will have the return and parameter types we're looking for.
            MethodInfo delegateMethod = delegateType.GetMethod("Invoke", BindingFlags.Public | BindingFlags.Instance);
            if (delegateMethod == null)
                throw new MissingMethodException(delegateType.FullName, "Invoke method not found");
            Type returnType = delegateMethod.ReturnType;

            if (returnType == typeof(void))
                throw new ArgumentOutOfRangeException(nameof(delegateType), "delegate type must not return void");
            if (!returnType.IsAssignableFrom(constructType))
                throw new ArgumentOutOfRangeException(nameof(delegateType),
                                                      "delegate return type must be assignable from construction type");

            Type[] parameterTypes = delegateMethod.GetParameters().Select(p => p.ParameterType).ToArray();

            ConstructorInfo constructor = constructType.GetConstructor(parameterTypes);
            if (constructor == null)
                throw new MissingMethodException(constructType.FullName,
                                                 "no constructor found matching delegate parameter types");

            var method = new DynamicMethod("New" + constructType.Name, returnType, parameterTypes);
            ILGenerator il = method.GetILGenerator();

            for (int i = 0; i < parameterTypes.Length; i++)
                il.Emit(OpCodes.Ldarg, i);
            il.Emit(OpCodes.Newobj, constructor);
            il.Emit(OpCodes.Ret);

            return method.CreateDelegate(delegateType);
        }
    }
}