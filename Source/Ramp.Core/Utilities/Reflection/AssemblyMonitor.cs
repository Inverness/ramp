﻿using System;
using System.Reflection;

namespace Ramp.Utilities.Reflection
{
    public static class AssemblyMonitor
    {
        /// <summary>
        ///     Adds a handler that is invoked for each currently and newload loaded assembly.
        /// </summary>
        /// <param name="handler"> A handler. </param>
        /// <param name="excludeCurrent"> Whether to exclude invoking the handler for the currently loaded assemblies. </param>
        public static void Add(Action<Assembly> handler, bool excludeCurrent = false)
        {
            Validate.ArgumentNotNull(handler, "handler");

            AppDomain.CurrentDomain.AssemblyLoad += (sender, e) => { handler(e.LoadedAssembly); };

            if (excludeCurrent)
                return;

            AppDomain.CurrentDomain.GetAssemblies().ForEach(handler);
        }
    }
}