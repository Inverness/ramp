﻿using System;
using System.Collections.Generic;

namespace Ramp.Utilities.Reflection
{
    /// <summary>
    ///     Provides a unique index for a given type.
    /// </summary>
    public static class TypeIndex
    {
        private static readonly object s_lock = new object();
        private static readonly Dictionary<Type, int> s_indices = new Dictionary<Type, int>();
        private static readonly List<Type> s_types = new List<Type>();

        /// <summary>
        ///     Gets a unique index for a given type.
        /// </summary>
        /// <param name="type"> A type. </param>
        /// <returns> A non-negative index that is unique for the provided type. </returns>
        public static int GetIndex(Type type)
        {
            Validate.ArgumentNotNull(type, nameof(type));

            lock (s_lock)
            {
                int index;
                if (!s_indices.TryGetValue(type, out index))
                {
                    index = s_types.Count;
                    s_types.Add(type);
                    s_indices.Add(type, index);
                }
                return index;
            }
        }

        /// <summary>
        ///     Gets the type that has been given the specified index.
        /// </summary>
        /// <param name="index"> A non-negative index. </param>
        /// <returns> The type given the specified index, or null if the index has not been assigned. </returns>
        public static Type GetType(int index)
        {
            Validate.ArgumentInRange(index >= 0, nameof(index), "index must be non-negative");

            lock (s_lock)
            {
                return index < s_types.Count ? s_types[index] : null;
            }
        }
    }

    /// <summary>
    ///     Provides a unique index for a given type. This class allows the type's index to be cached in a static field.
    /// </summary>
    /// <typeparam name="T"> A type. </typeparam>
    public static class TypeIndex<T>
    {
        /// <summary>
        ///     Gets the index for the class's generic type parameter.
        /// </summary>
        /// <value> A non-negative index that is unique for the provided type. </value>
        public static int Index { get; } = TypeIndex.GetIndex(typeof(T));
    }
}
