using System;
using System.Collections.Concurrent;
using System.ComponentModel;

namespace Ramp.Utilities.Reflection
{
    public static class TypeConverterCache
    {
        public static readonly ConcurrentDictionary<Type, TypeConverter> s_entries =
            new ConcurrentDictionary<Type, TypeConverter>();

        public static TypeConverter GetConverter(Type type)
        {
            Validate.ArgumentNotNull(type, nameof(type));
            
            TypeConverter converter;
            if (!s_entries.TryGetValue(type, out converter))
            {
                converter = TypeDescriptor.GetConverter(type);
                s_entries[type] = converter;
            }
            return converter;
        }
    }
}