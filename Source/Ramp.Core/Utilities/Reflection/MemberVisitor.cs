﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;

namespace Ramp.Utilities.Reflection
{
    /// <summary>
    /// An object that can visit the properties and fields of a target object. Code generation is used to archieve
    /// this without the use of reflection. This class is meant to subclassed by a visitor implementation.
    /// 
    /// Non-generic overloads of VisitProperty() and VisitField() will be called for fields and properties of a
    /// specific type if they are defined. IsTargetMember should be overriden to select targets and specify context.
    /// </summary>
    /// <typeparam name="T">The type of object to visit.</typeparam>
    public abstract class MemberVisitor<T>
    {
        private const BindingFlags AllInstanceFlags =
            BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public;

        private delegate void VisitorCore(T obj, FieldInfo[] fields, PropertyInfo[] properties, object[] contexts);

        private VisitorCore _core;
        private FieldInfo[] _fields;
        private PropertyInfo[] _properties;
        private object[] _contexts;

        private static readonly VisitorCore s_noOpVisitor = (obj, f, p, c) => { };

        /// <summary>
        ///     Visits a target object.
        /// </summary>
        /// <param name="obj"> A target object. </param>
        public virtual void Visit(T obj)
        {
            if (_core == null)
                GenerateVisitorCore();
            Debug.Assert(_core != null, "_core != null");
            _core(obj, _fields, _properties, _contexts);
        }

        /// <summary>
        ///     Get the FieldInfo and PropertyInfo for members that will be visited. By default this invokes
        ///     IsTargetMember() on all instance fields and properties that are not compiler-generated.
        /// </summary>
        /// <returns> A collection of members and their contexts to be visited. </returns>
        protected virtual IEnumerable<KeyValuePair<MemberInfo, object>> GetTargetMembers()
        {
            foreach (MemberInfo m in typeof(T).GetMembers(AllInstanceFlags).Where(IsUserFieldOrProperty))
            {
                object context = null;
                if (IsTargetMember(m, ref context))
                    yield return new KeyValuePair<MemberInfo, object>(m, context);
            }
        }

        /// <summary>
        ///     Invoked by default to test if a field or property member should be a target for visiting. By default
        ///     this returns true.
        /// </summary>
        /// <param name="member"> The field or property member. </param>
        /// <param name="context"> Optional context that can be supplied when visiting this member. </param>
        /// <returns> True if this member should be visited. </returns>
        protected virtual bool IsTargetMember(MemberInfo member, ref object context)
        {
            return true;
        }

        /// <summary>
        ///     Invoked when a property of the target object is being visited.
        /// </summary>
        /// <typeparam name="TValue"> The property value type. </typeparam>
        /// <param name="obj"> The target object. </param>
        /// <param name="value"> The property value. </param>
        /// <param name="property"> The property info. </param>
        /// <param name="context"> The optional context. </param>
        /// <returns> True if the property setter should be called with the current value. </returns>
        protected virtual bool VisitProperty<TValue>(T obj, ref TValue value, PropertyInfo property, object context)
        {
            return false;
        }

        /// <summary>
        ///     Invoked when a field of the target object is being visited.
        /// </summary>
        /// <typeparam name="TValue"> The field value type. </typeparam>
        /// <param name="obj"> The target object. </param>
        /// <param name="value"> The field value. </param>
        /// <param name="field"> The field info. </param>
        /// <param name="context"> The optional context. </param>
        protected virtual void VisitField<TValue>(T obj, ref TValue value, FieldInfo field, object context)
        {
        }

        private void GenerateVisitorCore()
        {
            KeyValuePair<MemberInfo, object>[] members =
                (GetTargetMembers() ?? Array.Empty<KeyValuePair<MemberInfo, object>>()).ToArray();

            if (members.Length == 0)
            {
                // Nothing to do
                _core = s_noOpVisitor;
                return;
            }

            Type thisType = GetType();
            MethodInfo[] thisMethods = thisType.GetMethods(AllInstanceFlags).ToArray();
            Type targetType = typeof(T);

            // Find our default and overloaded visitors
            MethodInfo defaultFieldVisitor = thisMethods.First(IsDefaultFieldVisitor);

            MethodInfo[] fieldVisitors = thisMethods.Where(m => !IsDefaultFieldVisitor(m) && IsFieldVisitor(m)).ToArray();

            MethodInfo defaultPropertyVisitor = thisMethods.First(IsDefaultPropertyVisitor);

            MethodInfo[] propertyVisitors = thisMethods.Where(m => !IsDefaultPropertyVisitor(m) && IsPropertyVisitor(m)).ToArray();

            // Cache field and property info can arrays with their indices as constants, so that member info can be quickly
            // supplied to visitor methods without any sort of dictionary lookup.
            var fields = new List<FieldInfo>();
            var properties = new List<PropertyInfo>();
            var contexts = new List<object>();

            var tempValueLocals = new Dictionary<Type, LocalBuilder>();
            bool isValueType = targetType.IsValueType;
            var paramTypes = new[]
            {
                thisType,
                targetType,
                typeof(FieldInfo[]),
                typeof(PropertyInfo[]),
                typeof(object[])
            };

            var dynamicMethod = new DynamicMethod("VisitCore", null, paramTypes, thisType, true);
            ILGenerator il = dynamicMethod.GetILGenerator();

            for (int i = 0; i < members.Length; i++)
            {
                if (members[i].Key == null)
                    throw new InvalidOperationException("null member");

                if (members[i].Key.DeclaringType == null || !members[i].Key.DeclaringType.IsAssignableFrom(targetType))
                    throw new InvalidOperationException("member does not belong to target type");

                var field = members[i].Key as FieldInfo;
                var property = members[i].Key as PropertyInfo;
                object context = members[i].Value;

                // Store the context if its not null.
                int contextIndex;
                if (context != null)
                {
                    contextIndex = contexts.Count;
                    contexts.Add(context);
                }
                else
                {
                    contextIndex = -1;
                }

                if (field != null)
                {
                    // Find a valid visitor

                    MethodInfo visitor = fieldVisitors.FirstOrDefault(m => CanReceieveType(m, field.FieldType)) ??
                                         defaultFieldVisitor.MakeGenericMethod(field.FieldType);
                    Debug.Assert(visitor.ReturnType == typeof(void));

                    int fieldIndex = fields.Count;
                    fields.Add(field);

                    // Invoke the visitor

                    il.Emit(OpCodes.Ldarg_0); // 'this' of method call

                    il.Emit(OpCodes.Ldarg_1); // the object being visited

                    if (isValueType)
                        il.Emit(OpCodes.Ldarga_S, (byte) 1);
                    else
                        il.Emit(OpCodes.Ldarg_1);
                    il.Emit(OpCodes.Ldflda, field); // reference to field value

                    il.Emit(OpCodes.Ldarg_2);
                    il.Emit(OpCodes.Ldc_I4, fieldIndex);
                    il.Emit(OpCodes.Ldelem_Ref); // load FieldInfo from array

                    if (contextIndex != -1)
                    {
                        il.Emit(OpCodes.Ldarg_S, (byte) 4);
                        il.Emit(OpCodes.Ldc_I4, contextIndex);
                        il.Emit(OpCodes.Ldelem_Ref); // load context from array
                    }
                    else
                    {
                        il.Emit(OpCodes.Ldnull);
                    }

                    il.Emit(visitor.IsVirtual ? OpCodes.Callvirt : OpCodes.Call, visitor);
                }
                else if (property != null)
                {
                    // Find a valid visitor

                    MethodInfo visitor =
                        propertyVisitors.FirstOrDefault(m => CanReceieveType(m, property.PropertyType)) ??
                        defaultPropertyVisitor.MakeGenericMethod(property.PropertyType);
                    Debug.Assert(visitor.ReturnType == typeof(bool));

                    int propertyIndex = properties.Count;
                    properties.Add(property);

                    // Get a local variable for storing the property value

                    LocalBuilder localTempValue;
                    if (!tempValueLocals.TryGetValue(property.PropertyType, out localTempValue))
                    {
                        localTempValue = il.DeclareLocal(property.PropertyType);
                        tempValueLocals.Add(property.PropertyType, localTempValue);
                    }

                    // Invoke the property getter

                    if (isValueType)
                        il.Emit(OpCodes.Ldarga_S, (byte) 1);
                    else
                        il.Emit(OpCodes.Ldarg_1);

                    il.Emit(OpCodes.Callvirt, property.GetMethod);

                    il.Emit(OpCodes.Stloc, localTempValue);

                    // Invoke the visitor

                    il.Emit(OpCodes.Ldarg_0); // load 'this'

                    il.Emit(OpCodes.Ldarg_1); // the object being visited

                    il.Emit(OpCodes.Ldloca, localTempValue); // load reference to value

                    il.Emit(OpCodes.Ldarg_3);
                    il.Emit(OpCodes.Ldc_I4, propertyIndex);
                    il.Emit(OpCodes.Ldelem_Ref); // load PropertyInfo from array

                    if (contextIndex != -1)
                    {
                        il.Emit(OpCodes.Ldarg_S, (byte) 4);
                        il.Emit(OpCodes.Ldc_I4, contextIndex);
                        il.Emit(OpCodes.Ldelem_Ref); // load context from array
                    }
                    else
                    {
                        il.Emit(OpCodes.Ldnull);
                    }

                    il.Emit(visitor.IsVirtual ? OpCodes.Callvirt : OpCodes.Call, visitor);

                    // Handle the setter

                    if (property.SetMethod == null)
                    {
                        il.Emit(OpCodes.Pop); // No setter is available
                    }
                    else
                    {
                        Label skipLabel = il.DefineLabel();
                        il.Emit(OpCodes.Brfalse, skipLabel); // If visitor returns false, do not set the property

                        // Invoke setter

                        if (isValueType)
                            il.Emit(OpCodes.Ldarga_S, (byte) 1);
                        else
                            il.Emit(OpCodes.Ldarg_1);

                        il.Emit(OpCodes.Ldloc, localTempValue);

                        il.Emit(OpCodes.Callvirt, property.SetMethod);

                        il.MarkLabel(skipLabel);
                    }
                }
                else
                {
                    throw new InvalidOperationException();
                }
            }

            il.Emit(OpCodes.Ret); // must have a ret at the end

            _core = (VisitorCore) dynamicMethod.CreateDelegate(typeof(VisitorCore), this);
            _fields = fields.ToArray();
            _properties = properties.ToArray();
            _contexts = contexts.ToArray();
        }

        // Check that the member is a field or property that is not compiler-generated.
        private static bool IsUserFieldOrProperty(MemberInfo m)
        {
            return !m.Name.StartsWith("<") && (m is FieldInfo || m is PropertyInfo);
        }

        // Check that a method is suitable for being a field visitor.
        private static bool IsFieldVisitor(MethodInfo m)
        {
            ParameterInfo[] p = m.GetParameters();

            return m.Name == "FieldVisitor" &&
                   !m.IsGenericMethodDefinition &&
                   m.ReturnType == typeof(void) &&
                   p.Length == 4 &&
                   p[0].ParameterType == typeof(T) &&
                   p[1].ParameterType.IsByRef &&
                   p[2].ParameterType == typeof(FieldInfo) &&
                   p[3].ParameterType == typeof(object);
        }

        // Check that  a method is suitable for being a property visitor
        private static bool IsPropertyVisitor(MethodInfo m)
        {
            ParameterInfo[] p = m.GetParameters();

            return m.Name == "PropertyVisitor" &&
                   !m.IsGenericMethodDefinition &&
                   m.ReturnType == typeof(bool) &&
                   p.Length == 4 &&
                   p[0].ParameterType == typeof(T) &&
                   p[1].ParameterType.IsByRef &&
                   p[2].ParameterType == typeof(PropertyInfo) &&
                   p[3].ParameterType == typeof(object);
        }

        private static bool IsDefaultFieldVisitor(MethodInfo m)
        {
            return m.IsGenericMethodDefinition && m.Name == "VisitField";
        }

        private static bool IsDefaultPropertyVisitor(MethodInfo m)
        {
            return m.IsGenericMethodDefinition && m.Name == "VisitProperty";
        }

        // Check that a visitor method can accept a value of the specified type.
        private static bool CanReceieveType(MethodInfo m, Type type)
        {
            // Parameter count it being by-ref already checked.
            return m.GetParameters()[1].ParameterType.GetElementType().IsAssignableFrom(type);
        }
    }
}