using System.Collections;
using System.Collections.Generic;

namespace Ramp.Utilities.Collections
{
    /// <summary>
    ///     Provides a view of a LinkedArrayList as a read-only dictionary.
    /// </summary>
    public sealed class LinkedArrayListDictionaryView<T> : IReadOnlyDictionary<int, T>
    {
        private readonly LinkedArrayList<T> _source;

        internal LinkedArrayListDictionaryView(LinkedArrayList<T> source)
        {
            _source = source;
        }

        public int Count => _source.Count;

        public T this[int key] => _source[key];

        public bool ContainsKey(int key)
        {
            return _source.IsAllocated(key);
        }


        /// <inheritdoc />
        public IEnumerable<int> Keys
        {
            get
            {
                foreach (KeyValuePair<int, T> item in this)
                    yield return item.Key;
            }
        }


        /// <inheritdoc />
        public bool TryGetValue(int key, out T value)
        {
            return _source.TryGetItem(key, out value);
        }

        /// <inheritdoc />
        public IEnumerable<T> Values
        {
            get
            {
                foreach (KeyValuePair<int, T> item in this)
                    yield return item.Value;
            }
        }

        public LinkedArrayList<T>.DictionaryViewEnumerator GetEnumerator()
        {
            return new LinkedArrayList<T>.DictionaryViewEnumerator(_source);
        }

        IEnumerator<KeyValuePair<int, T>> IEnumerable<KeyValuePair<int, T>>.GetEnumerator()
        {
            return GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}