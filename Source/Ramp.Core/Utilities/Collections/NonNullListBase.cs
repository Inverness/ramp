﻿using System.Collections.Generic;

namespace Ramp.Utilities.Collections
{
    public class NonNullListBase<T> : ListBase<T>
    {
        public NonNullListBase()
        {
        }

        public NonNullListBase(int capacity)
            : base(capacity)
        {
        }

        public NonNullListBase(IEnumerable<T> collection)
            : base(collection)
        {
        }

        protected override void InsertItem(int index, T item)
        {
            ValidateNonNullItem(item);
            base.InsertItem(index, item);
        }

        protected override void SetItem(int index, T item)
        {
            ValidateNonNullItem(item);
            base.SetItem(index, item);
        }
    }
}