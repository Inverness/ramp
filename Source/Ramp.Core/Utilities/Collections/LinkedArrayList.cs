using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace Ramp.Utilities.Collections
{
    /// <summary>
    ///     A linked list backed by an array using indices to refer to nodes in the list. The order of the items in
    ///     the list is independent of their indices.
    /// </summary>
    /// <typeparam name="T"> The item type. </typeparam>
    /// <remarks>
    ///     The class exists to allow quick lookup of an item by its index as in a list, and to allow items to be
    ///     added and removed with a minimum of allocations.
    /// </remarks>
    [DebuggerDisplay("Count = {Count}")]
    public sealed class LinkedArrayList<T> : IReadOnlyCollection<T>, ICollection<T>
    {
        private const double GrowthRate = 2.0;
        private const int MinimumSize = 4;
        private const int NotAllocated = -2;

        private static readonly Entry[] s_emptyEntries = new Entry[0];

        private Entry[] _entries;

        private int _nextIndex;
        private int _count;
        private int _version;
        private int _entryList = -1; // a double-linked list of allocated entries
        private int _freeList = -1; // a single-linked list of free entries

        private LinkedArrayListDictionaryView<T> _dictionaryView;

        /// <summary>
        ///     Initialize an empty pooled list.
        /// </summary>
        /// <param name="capacity"> The initial capacity of the pool. </param>
        public LinkedArrayList(int capacity = 0)
        {
            Validate.ArgumentInRange(capacity >= 0, "capacity");

            _entries = capacity == 0 ? s_emptyEntries : new Entry[capacity];
        }

        /// <summary>
        ///     Initialize a pooled list from the specified enumerable.
        /// </summary>
        /// <param name="source"> A source. </param>
        public LinkedArrayList(IEnumerable<T> source)
        {
            Validate.ArgumentNotNull(source, "source");
            
            int count;
            _entries = CollectionClassHelper.TryCount(source, out count) ? new Entry[count] : s_emptyEntries;

            foreach (T item in source)
                AddLast(item);
        }

        /// <inheritdoc />
        public T this[int index]
        {
            get
            {
                if (!IsAllocated(index))
                    ThrowIndexNotAllocated();
                return _entries[index].Value;
            }

            set
            {
                if (!IsAllocated(index))
                    ThrowIndexNotAllocated();
                _entries[index].Value = value;
                _version++;
            }
        }

        /// <summary>
        ///     Gets the number of currently allocated items.
        /// </summary>
        public int Count => _count;

        /// <summary>
        ///     Gets the index of the first item in the list.
        /// </summary>
        public int FirstIndex => _entryList;

        /// <summary>
        ///     Gets the index of the last item in the list.
        /// </summary>
        public int LastIndex => _entryList != -1 ? _entries[_entryList].Prev : -1;

        /// <summary>
        ///     Gets the current capacity of this collection.
        /// </summary>
        public int Capacity => _entries.Length;

        /// <summary>
        ///     Gets the first item in the list.
        /// </summary>
        /// <exception cref="InvalidOperationException">The list is empty.</exception>
        public T First
        {
            get
            {
                if (_count == 0)
                    ThrowEmpty();
                return _entries[_entryList].Value;
            }
        }

        /// <summary>
        ///     Gets the last item in the list.
        /// </summary>
        /// <exception cref="InvalidOperationException">The list is empty.</exception>
        public T Last
        {
            get
            {
                if (_count == 0)
                    ThrowEmpty();
                return _entries[_entries[_entryList].Prev].Value;
            }
        }

        /// <summary>
        /// Gets a view of this LinkedArrayList as a read-only dictionary. This dictionary provides an optimized struct enumerator.
        /// </summary>
        public LinkedArrayListDictionaryView<T> DictionaryView
            => _dictionaryView ?? (_dictionaryView = new LinkedArrayListDictionaryView<T>(this));

        /// <inheritdoc />
        bool ICollection<T>.IsReadOnly => false;

        /// <inheritdoc />
        void ICollection<T>.Add(T item)
        {
            AddLast(item);
        }

        /// <summary>
        ///     Add an item at the end of the list.
        /// </summary>
        /// <param name="item"> The item to add. </param>
        /// <returns> The index of the added item. </returns>
        public int AddLast(T item)
        {
            return InternalAddBefore(_entryList, item);
        }

        /// <summary>
        ///     Add an item at the beginning of the list.
        /// </summary>
        /// <param name="item"> The item to add. </param>
        /// <returns> The index of the added item. </returns>
        public int AddFirst(T item)
        {
            return (_entryList = InternalAddBefore(_entryList, item));
        }

        /// <summary>
        ///     Adds an item after the item at the specified index.
        /// </summary>
        /// <param name="index"> A valid item index. </param>
        /// <param name="item"> The item to add. </param>
        /// <returns> The index of the added item. </returns>
        public int AddAfter(int index, T item)
        {
            if (!IsAllocated(index))
                ThrowIndexNotAllocated();
            return InternalAddBefore(_entries[index].Next, item);
        }

        /// <summary>
        ///     Adds an item before the item at the specified index.
        /// </summary>
        /// <param name="index"> A valid item index. </param>
        /// <param name="item"> The item to add. </param>
        /// <returns> The index of the added item. </returns>
        public int AddBefore(int index, T item)
        {
            if (!IsAllocated(index))
                ThrowIndexNotAllocated();
            return index == _entryList ? (_entryList = InternalAddBefore(index, item)) : InternalAddBefore(index, item);
        }

        /// <inheritdoc />
        public void Clear()
        {
            Array.Clear(_entries, 0, _nextIndex);
            _entryList = _entryList = -1;
            _nextIndex = 0;
            _count = 0;
            _version++;
        }

        /// <inheritdoc />
        public bool Contains(T item)
        {
            return IndexOf(item) != -1;
        }

        /// <inheritdoc />
        public void CopyTo(T[] array, int arrayIndex)
        {
            if (array == null)
                throw new ArgumentNullException(nameof(array));
            if (arrayIndex + _count > array.Length)
                throw new ArgumentException("destination array not large enough");

            int i = _entryList;
            if (i == -1)
                return;

            do
            {
                array[arrayIndex++] = _entries[i].Value;
                i = _entries[i].Next;
            } while (i != _entryList);
        }

        /// <inheritdoc />
        public IEnumerator<T> GetEnumerator()
        {
            int i = _entryList;
            if (i == -1)
                yield break;

            int version = _version;
            do
            {
                yield return _entries[i].Value;
                if (version != _version)
                    throw new InvalidOperationException("enumeration version mismatch");
                i = _entries[i].Next;
            } while (i != _entryList);
        }

        /// <inheritdoc />
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        ///     Gets the index of next item.
        /// </summary>
        /// <param name="index"> An item index. </param>
        /// <returns> The index of the next item or -1 if this is the last item in the list. </returns>
        public int GetNextIndex(int index)
        {
            if (!IsAllocated(index))
                ThrowIndexNotAllocated();
            int next;
            return (next = _entries[index].Next) != _entryList ? next : -1;
        }

        /// <summary>
        ///     Gets the index of the previous item.
        /// </summary>
        /// <param name="index"> An item index. </param>
        /// <returns> The index of the next item or -1 if this is the first item in the list. </returns>
        public int GetPreviousIndex(int index)
        {
            if (!IsAllocated(index))
                ThrowIndexNotAllocated();
            return index != _entryList ? _entries[index].Prev : -1;
        }

        /// <summary>
        ///     Gets the item at the specified index along with the next index. This method is recommended for manual
        ///     iteration.
        /// </summary>
        /// <param name="index"> An item index. </param>
        /// <param name="nextIndex"> The index of the next item, or -1 if the current item is the last. </param>
        /// <returns> The item at the specified index. </returns>
        public T GetItemAndNextIndex(int index, out int nextIndex)
        {
            if (!IsAllocated(index))
                ThrowIndexNotAllocated();
            Entry[] entries = _entries;
            if ((nextIndex = entries[index].Next) == _entryList)
                nextIndex = -1;
            return entries[index].Value;
        }

        /// <summary>
        ///     Gets the index of the specified item.
        /// </summary>
        /// <param name="item"> An item to find. </param>
        /// <returns> The index of the specified item, or -1 if not found. </returns>
        public int IndexOf(T item)
        {
            return IndexOf(item, EqualityComparer<T>.Default);
        }

        /// <summary>
        ///     Gets the index of the specified item using an equality comparer.
        /// </summary>
        /// <param name="item"> An item to find. </param>
        /// <param name="comparer"> An equality comparer. </param>
        /// <returns> The index of the specified item, or -1 if not found. </returns>
        public int IndexOf(T item, IEqualityComparer<T> comparer)
        {
            Validate.ArgumentNotNull(comparer, "comparer");

            int i = _entryList;
            if (i != -1)
            {
                do
                {
                    if (comparer.Equals(_entries[i].Value, item))
                        return i;
                    i = _entries[i].Next;
                } while (i != _entryList);
            }

            return -1;
        }

        /// <summary>
        ///     Check if an index is allocated.
        /// </summary>
        /// <param name="index"> An index. </param>
        /// <returns> True if the index is allocated. </returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool IsAllocated(int index)
        {
            return (uint) index < (uint) _nextIndex && _entries[index].Prev != NotAllocated;
        }

        /// <summary>
        ///     Removes an item from the list, and returns the index the item had before it was removed.
        /// </summary>
        /// <param name="item"> An item. </param>
        /// <returns> The index the item had before it was removed, or -1 if the item was not found. </returns>
        public int Remove(T item)
        {
            int index = IndexOf(item);
            if (index != -1)
                RemoveAt(index);
            return index;
        }

        /// <inheritdoc />
        bool ICollection<T>.Remove(T item)
        {
            return Remove(item) != -1;
        }

        /// <summary>
        ///     Remove an item from the pooled list. The index is saved for use in the next Add().
        /// </summary>
        /// <param name="index"> The item index. </param>
        public void RemoveAt(int index)
        {
            if (!IsAllocated(index))
                ThrowIndexNotAllocated();
            InternalRemoveAt(index);
        }

        /// <summary>
        ///     Trims excess items from the internal array that are not allocated.
        /// </summary>
        public void TrimExcess()
        {
            // Find the index of the last allocated entry in the array, which will always be before _nextIndex
            int lastAllocated;
            for (lastAllocated = _nextIndex - 1; lastAllocated > -1; lastAllocated--)
            {
                if (_entries[lastAllocated].Prev != NotAllocated)
                    break;
            }
            
            _nextIndex = lastAllocated + 1;

            if (_entries.Length == _nextIndex)
                return;

            Array.Resize(ref _entries, _nextIndex);

            // Rebuild the free list
            _freeList = -1;
            for (int i = 0; i < _nextIndex; i++)
            {
                if (_entries[i].Prev == NotAllocated)
                {
                    _entries[i].Next = _freeList;
                    _freeList = i;
                }
            }

            _version++;
        }

        public bool TryGetItem(int index, out T value)
        {
            if (IsAllocated(index))
            {
                value = _entries[index].Value;
                return true;
            }

            value = default(T);
            return false;
        }
        
        // Insert a value before the specified index
        private int InternalAddBefore(int index, T value)
        {
            if (_count == int.MaxValue)
                throw new InvalidOperationException("Maximum number of items reached.");

            Entry[] entries = _entries;
            int newIndex = _freeList;

            if (newIndex == -1)
            {
                if (_nextIndex == entries.Length)
                {
                    CollectionClassHelper.EnsureSize(ref entries, _nextIndex + 1, MinimumSize, GrowthRate);

                    // mark new entries as not allocated
                    for (int i = _nextIndex + 1; i < entries.Length; i++)
                        entries[i].Prev = NotAllocated;

                    _entries = entries;
                }

                newIndex = _nextIndex++;
            }
            else
            {
                _freeList = entries[newIndex].Next;
            }

            if (_entryList == -1)
            {
                _entryList = newIndex;
                entries[newIndex].Next = newIndex;
                entries[newIndex].Prev = newIndex;
            }
            else
            {
                int prev = entries[index].Prev;
                int next = index;

                entries[prev].Next = newIndex;
                entries[next].Prev = newIndex;
                entries[newIndex].Prev = prev;
                entries[newIndex].Next = next;
            }

            entries[newIndex].Value = value;

            _count++;
            _version++;

            return newIndex;
        }

        // Free the value at the specified index and add it to the free list
        private void InternalRemoveAt(int index)
        {
            Entry[] entries = _entries;

            int prev = entries[index].Prev;
            int next = entries[index].Next;

            entries[prev].Next = next;
            entries[next].Prev = prev;

            if (index == _entryList)
                _entryList = (next == _entryList ? -1 : next);

            Entry e;
            e.Next = _freeList;
            e.Prev = NotAllocated;
            e.Value = default(T);
            entries[index] = e;

            _freeList = index;

            _count--;
            _version++;
        }

        private static void ThrowIndexNotAllocated()
        {
            throw new IndexOutOfRangeException("index is not allocated");
        }

        private static void ThrowEmpty()
        {
            throw new InvalidOperationException("collection is empty");
        }
        
        private struct Entry
        {
            public int Next;
            public int Prev; // -2 indicates not allocated
            public T Value;
        }

        public struct DictionaryViewEnumerator : IEnumerator<KeyValuePair<int, T>>
        {
            private readonly LinkedArrayList<T> _list;
            private readonly int _version;
            private KeyValuePair<int, T> _current;
            private int _nextIndex;

            internal DictionaryViewEnumerator(LinkedArrayList<T> list)
            {
                _list = list;
                _version = list._version;
                _nextIndex = list._entryList;
                _current = default(KeyValuePair<int, T>);
            }

            public KeyValuePair<int, T> Current => _current;

            object IEnumerator.Current => _current;

            public bool MoveNext()
            {
                LinkedArrayList<T> list = _list;
                if (_version == list._version && _nextIndex != -1)
                {
                    Entry entry = list._entries[_nextIndex];
                    _current = new KeyValuePair<int, T>(_nextIndex, entry.Value);
                    if ((_nextIndex = entry.Next) == list._entryList)
                        _nextIndex = -1;
                    return true;
                }
                return MoveNextRare();
            }

            public void Reset()
            {
                _nextIndex = _list._entryList;
                _current = default(KeyValuePair<int, T>);
            }

            public void Dispose()
            {
                Reset();
            }

            private bool MoveNextRare()
            {
                if (_version != _list._version)
                    throw new InvalidOperationException("enumeration version mismatch");
                _nextIndex = -1;
                _current = default(KeyValuePair<int, T>);
                return false;
            }
        }
    }
}