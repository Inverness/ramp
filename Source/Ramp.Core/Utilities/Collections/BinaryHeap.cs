﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

namespace Ramp.Utilities.Collections
{
    /// <summary>
    ///     Implements the priority queue concept as a binary heap.
    /// </summary>
    /// <typeparam name="T"> The item type. </typeparam>
    [DebuggerDisplay("Count = {Count}")]
    public sealed class BinaryHeap<T> : ICollection<T>, IReadOnlyList<T>
    {
        private const int MinimumSize = 4;
        private const double GrowthRate = 2.0;

        private readonly IComparer<T> _comparer;
        private T[] _items;
        private int _count;
        private int _version;

        public BinaryHeap()
            : this(0, null)
        {
        }

        public BinaryHeap(int capacity)
            : this(capacity, null)
        {
        }

        public BinaryHeap(IComparer<T> comparer)
            : this(0, comparer)
        {
        }

        public BinaryHeap(int capacity, IComparer<T> comparer)
        {
            _items = capacity > 0 ? new T[capacity] : Array.Empty<T>();
            _comparer = comparer ?? Comparer<T>.Default;
        }

        public int Count => _count;

        bool ICollection<T>.IsReadOnly => false;

        public T this[int index]
        {
            get
            {
                if ((uint) index >= (uint) _count)
                    ThrowIndexOutOfRange();
                return _items[index];
            }
        }

        /// <summary>
        ///     Peek at the item as the top of the heap without removing it from the heap.
        /// </summary>
        /// <returns> The item at the top of the heap. </returns>
        /// <exception cref="InvalidOperationException"> The heap is empty. </exception>
        public T Peek()
        {
            if (_count == 0)
                ThrowEmpty();
            return _items[0];
        }

        /// <summary>
        ///     Removes and returns the item at the top of the heap.
        /// </summary>
        /// <returns> The item at the top of the heap. </returns>
        /// <exception cref="InvalidOperationException"> The heap is empty. </exception>
        public T Pop()
        {
            if (_count == 0)
                ThrowEmpty();

            T result = _items[0];

            RemoveAtCore(0);

            MoveDown(0); // reorders the new occupant of index 0

            return result;
        }

        /// <summary>
        ///     Pushes an item into the heap.
        /// </summary>
        /// <param name="item"> An item. </param>
        public void Push(T item)
        {
            if (_count == _items.Length)
                CollectionClassHelper.EnsureSize(ref _items, _count + 1, MinimumSize, GrowthRate);
            _items[_count++] = item;
            _version++;

            MoveUp(0, _count - 1);
        }

        /// <summary>
        ///     Removes an item from the heap found at the specified index.
        /// </summary>
        /// <param name="index"> An item index. </param>
        /// <exception cref="IndexOutOfRangeException"> The item index is out of range. </exception>
        public void RemoveAt(int index)
        {
            if (index < 0 || index >= _count)
                ThrowIndexOutOfRange();

            RemoveAtCore(index);

            MoveDown(index);
            MoveUp(0, Math.Min(index, _count - 1));
        }

        public IEnumerator<T> GetEnumerator()
        {
            int version = _version;
            for (int i = 0; i < _count; i++)
            {
                yield return _items[i];
                if (version != _version)
                    throw new InvalidOperationException("The collection was modified");
            }
        }

        public bool Remove(T item)
        {
            int index = Array.IndexOf(_items, item, 0, _count);
            if (index == -1)
                return false;
            RemoveAt(index);
            return true;
        }

        public void Clear()
        {
            Array.Clear(_items, 0, _count);
            _count = 0;
        }

        public bool Contains(T item)
        {
            return Array.IndexOf(_items, item, 0, _count) != -1;
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            _items.CopyTo(array, arrayIndex);
        }

        void ICollection<T>.Add(T item)
        {
            Push(item);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        private void RemoveAtCore(int index)
        {
            int last = _count - 1;
            if (last != index)
                _items[index] = _items[last];
            _items[last] = default(T);
            _count--;
            _version++;
        }

        private void MoveDown(int nodeIndex)
        {
            int count = _count;
            T[] items = _items;
            IComparer<T> comparer = _comparer;

            while ((nodeIndex * 2) + 1 < count)
            {
                int leftChildIndex = nodeIndex * 2 + 1;
                int rightChildIndex = leftChildIndex + 1;

                int minChildIndex = leftChildIndex;

                if (rightChildIndex < count)
                {
                    minChildIndex = comparer.Compare(items[leftChildIndex], items[rightChildIndex]) < 0
                                        ? leftChildIndex
                                        : rightChildIndex;
                }

                if (comparer.Compare(items[minChildIndex], items[nodeIndex]) >= 0)
                    break;

                T temp = items[nodeIndex];
                items[nodeIndex] = items[minChildIndex];
                items[minChildIndex] = temp;

                nodeIndex = minChildIndex;
            }
        }

        private void MoveUp(int rootIndex, int nodeIndex)
        {
            T[] items = _items;
            IComparer<T> comparer = _comparer;

            while (nodeIndex > rootIndex)
            {
                int parentIndex = (nodeIndex - 1) / 2;

                if (comparer.Compare(items[nodeIndex], items[parentIndex]) >= 0)
                    break;

                T temp = items[nodeIndex];
                items[nodeIndex] = items[parentIndex];
                items[parentIndex] = temp;

                nodeIndex = parentIndex;
            }
        }

        //private static int GetParentIndex(int index)
        //{
        //    return (index - 1) / 2;
        //}

        //private static int GetLeftChildIndex(int index)
        //{
        //    return (index * 2) + 1;
        //}

        //private static bool IsLeaf(int index, int count)
        //{
        //    return (index * 2) + 1 >= count;
        //}

        private static void ThrowIndexOutOfRange()
        {
            // ReSharper disable once NotResolvedInText
            throw new ArgumentOutOfRangeException("index");
        }

        private static void ThrowEmpty()
        {
            throw new InvalidOperationException("collection is empty");
        }
    }
}