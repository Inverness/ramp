﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

namespace Ramp.Utilities.Collections
{
    /// <summary>
    /// A base class for lists modeled after Collection&lt;T&gt; but with improved performance including a value type
    /// enumerator.
    /// </summary>
    /// <typeparam name="T">The item type</typeparam>
    public abstract class ListBase<T> : IList<T>, IReadOnlyList<T>
    {
        private const int MinimumSize = 4;
        private const double GrowthRate = 2.0;

        private T[] _items;
        private int _count;

        protected ListBase()
        {
            _items = Array.Empty<T>();
        }

        protected ListBase(int capacity)
        {
            _items = capacity > 0 ? new T[capacity] : Array.Empty<T>();
        }

        protected ListBase(IEnumerable<T> collection)
            : this(CollectionClassHelper.TryCount(collection) ?? 0)
        {
            Validate.ArgumentNotNull(collection, nameof(collection));

            foreach (T item in collection)
                Add(item);
        }

        public int Count => _count;

        public bool IsReadOnly => false;

        public T this[int index]
        {
            get
            {
                if ((uint) index >= (uint) _count)
                    ThrowIndexOutOfRange();
                return _items[index];
            }

            set
            {
                if ((uint) index >= (uint) _count)
                    ThrowIndexOutOfRange();
                SetItem(index, value);
            }
        }

        public Enumerator GetEnumerator()
        {
            return new Enumerator(this);
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(T item)
        {
            InsertItem(_count, item);
        }

        public void Clear()
        {
            ClearItems();
        }

        public bool Contains(T item)
        {
            return IndexOf(item) != -1;
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            Array.Copy(_items, 0, array, arrayIndex, _count);
        }

        public bool Remove(T item)
        {
            int index = IndexOf(item);
            if (index != -1)
            {
                RemoveAt(index);
                return true;
            }
            return false;
        }

        public int IndexOf(T item)
        {
            return Array.IndexOf(_items, item, 0, _count);
        }

        public void Insert(int index, T item)
        {
            if (index < 0 || index > _count)
                throw new ArgumentOutOfRangeException(nameof(index));
            InsertItem(index, item);
        }

        public void RemoveAt(int index)
        {
            if (index < 0 || index >= _count)
                throw new ArgumentOutOfRangeException(nameof(index));
            RemoveItem(index);
        }

        public T GetItemOrDefault(int index)
        {
            return (uint) index < (uint) _count ? _items[index] : default(T);
        }

        public T GetItemOrDefault(int index, T defaultValue)
        {
            return (uint) index < (uint) _count ? _items[index] : defaultValue;
        }

        protected virtual void ClearItems()
        {
            if (_count != 0)
            {
                Array.Clear(_items, 0, _count);
                _count = 0;
            }
        }

        protected virtual void InsertItem(int index, T item)
        {
            if (_count == _items.Length)
                CollectionClassHelper.EnsureSize(ref _items, _count + 1, MinimumSize, GrowthRate);
            if (index < _count)
                Array.Copy(_items, index, _items, index + 1, _count - index);
            _items[index] = item;
            _count++;
        }

        protected virtual void RemoveItem(int index)
        {
            _count--;
            if (index < _count)
                Array.Copy(_items, index + 1, _items, index, _count - index);
            _items[_count] = default(T);
        }

        protected virtual void SetItem(int index, T item)
        {
            _items[index] = item;
        }

        [Conditional("DEBUG"), Conditional("VALIDATE")]
        protected static void ValidateNonNullItem(T item)
        {
            if (item == null)
                throw new InvalidOperationException("items cannot be null");
        }

        private static void ThrowIndexOutOfRange()
        {
            // ReSharper disable once NotResolvedInText
            throw new ArgumentOutOfRangeException("index");
        }

        public struct Enumerator : IEnumerator<T>
        {
            private readonly ListBase<T> _list;
            private int _index;
            private T _current;

            internal Enumerator(ListBase<T> list)
            {
                _list = list;
                _index = 0;
                _current = default(T);
            }

            public T Current => _current;

            object IEnumerator.Current => _current;

            public bool MoveNext()
            {
                ListBase<T> list = _list;
                int index = _index;
                if (index < list._count)
                {
                    _current = list._items[index];
                    _index = index + 1;
                    return true;
                }
                return MoveNextRare();
            }

            public void Reset()
            {
                _index = 0;
                _current = default(T);
            }

            public void Dispose()
            {
            }

            private bool MoveNextRare()
            {
                _index = _list._count + 1;
                _current = default(T);
                return false;
            }
        }
    }
}
