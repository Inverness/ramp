using System;
using System.Collections;
using System.Collections.Generic;

namespace Ramp.Utilities.Collections
{
    public abstract class SortedListBase<T> : ICollection<T>, IReadOnlyList<T>
    {
        private const int MinimumSize = 4;
        private const double GrowthRate = 2.0;

        private T[] _items;
        private int _count;
        private readonly IComparer<T> _comparer;

        protected SortedListBase()
            : this((IComparer<T>) null)
        {
        }

        protected SortedListBase(IComparer<T> comparer)
        {
            _items = Array.Empty<T>();
            _count = 0;
            _comparer = comparer ?? Comparer<T>.Default;
        }

        protected SortedListBase(int capacity, IComparer<T> comparer = null)
        {
            _items = capacity > 0 ? new T[capacity] : Array.Empty<T>();
            _count = 0;
            _comparer = comparer ?? Comparer<T>.Default;
        }

        protected SortedListBase(IEnumerable<T> collection, IComparer<T> comparer = null)
            : this(CollectionClassHelper.TryCount(collection) ?? 0)
        {
            Validate.ArgumentNotNull(collection, nameof(collection));

            _comparer = comparer ?? Comparer<T>.Default;

            foreach (T item in collection)
                Add(item);
        }

        public int Count => _count;

        public bool IsReadOnly => false;

        public IComparer<T> Comparer => _comparer;

        public T this[int index]
        {
            get
            {
                if ((uint) index >= (uint) _count)
                    ThrowIndexOutOfRange();
                return _items[index];
            }
        }

        public Enumerator GetEnumerator()
        {
            return new Enumerator(this);
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(T item)
        {
            AddItem(item);
        }

        public void Clear()
        {
            ClearItems();
        }

        public bool Contains(T item)
        {
            return IndexOf(item) != -1;
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            Array.Copy(_items, 0, array, arrayIndex, _count);
        }

        public bool Remove(T item)
        {
            int index = IndexOf(item);
            if (index != -1)
            {
                RemoveAt(index);
                return true;
            }
            return false;
        }

        public int IndexOf(T item)
        {
            return Array.IndexOf(_items, item, 0, _count);
        }

        public void RemoveAt(int index)
        {
            if (index < 0 || index >= _count)
                throw new ArgumentOutOfRangeException(nameof(index));
            RemoveItem(index);
        }

        public int BinarySearch(T value, IComparer<T> comparer = null)
        {
            return BinarySearch(0, _count, value, comparer);
        }

        public int BinarySearch(int start, int length, T value, IComparer<T> comparer = null)
        {
            if (start < 0 || start >= _count)
                throw new ArgumentOutOfRangeException(nameof(start));
            if (start + length > _count)
                throw new ArgumentOutOfRangeException(nameof(length));
            return Array.BinarySearch(_items, start, length, value, comparer ?? Comparer<T>.Default);
        }

        protected virtual void ClearItems()
        {
            if (_count != 0)
                Array.Clear(_items, 0, _count);
            _count = 0;
        }

        protected virtual void AddItem(T item)
        {
            int index = Array.BinarySearch(_items, 0, _count, item, _comparer);
            if (index < 0)
                index = ~index;

            if (_count == _items.Length)
                CollectionClassHelper.EnsureSize(ref _items, _count + 1, MinimumSize, GrowthRate);
            if (index < _count)
                Array.Copy(_items, index, _items, index + 1, _count - index);
            _items[index] = item;
            _count++;
        }

        protected virtual void RemoveItem(int index)
        {
            _count--;
            if (index < _count)
                Array.Copy(_items, index + 1, _items, index, _count - index);
            _items[_count] = default(T);
        }

        private static void ThrowIndexOutOfRange()
        {
            throw new ArgumentOutOfRangeException();
        }

        public struct Enumerator : IEnumerator<T>
        {
            private readonly SortedListBase<T> _list;
            private int _index;
            private T _current;

            internal Enumerator(SortedListBase<T> list)
            {
                _list = list;
                _index = 0;
                _current = default(T);
            }

            public T Current => _current;

            object IEnumerator.Current => _current;

            public bool MoveNext()
            {
                SortedListBase<T> list = _list;
                if (_index < list._count)
                {
                    _current = list._items[_index++];
                    return true;
                }
                return MoveNextRare();
            }

            public void Reset()
            {
                _index = 0;
                _current = default(T);
            }

            public void Dispose()
            {
            }

            private bool MoveNextRare()
            {
                _index = _list._count + 1;
                _current = default(T);
                return false;
            }
        }
    }
}