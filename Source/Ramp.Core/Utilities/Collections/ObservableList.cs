﻿using System;
using System.Collections.Generic;

namespace Ramp.Utilities.Collections
{
    public class ObservableList<T> : ListBase<T>
    {
        private TypedEventHandler<ObservableList<T>, ObservableListChangedEventArgs> _itemAdded;
        private TypedEventHandler<ObservableList<T>, ObservableListChangedEventArgs> _itemRemoved;

        public ObservableList()
        {
        }

        public ObservableList(int capacity)
            : base(capacity)
        {
        }

        public ObservableList(IEnumerable<T> collection)
            : base (collection)
        {
        }

        public event TypedEventHandler<ObservableList<T>, ObservableListChangedEventArgs> Changed
        {
            add
            {
                _itemAdded = (TypedEventHandler<ObservableList<T>, ObservableListChangedEventArgs>) Delegate.Combine(_itemAdded, value);
                _itemRemoved = (TypedEventHandler<ObservableList<T>, ObservableListChangedEventArgs>) Delegate.Combine(value, _itemRemoved);
            }

            remove
            {
                _itemAdded = (TypedEventHandler<ObservableList<T>, ObservableListChangedEventArgs>) Delegate.Remove(_itemAdded, value);
                _itemRemoved = (TypedEventHandler<ObservableList<T>, ObservableListChangedEventArgs>) Delegate.Remove(_itemRemoved, value);
            }
        }

        protected override void ClearItems()
        {
            TypedEventHandler<ObservableList<T>, ObservableListChangedEventArgs> removed = _itemRemoved;
            if (removed != null)
            {
                for (int i = Count - 1; i > -1; i--)
                    OnItemRemoved(default(T), this[i], i, true);
            }
            base.ClearItems();
        }

        protected override void InsertItem(int index, T item)
        {
            base.InsertItem(index, item);
            OnItemAdded(item, default(T), index, true);
        }

        protected override void RemoveItem(int index)
        {
            OnItemRemoved(default(T), this[index], index, true);
            base.RemoveItem(index);
        }

        protected override void SetItem(int index, T item)
        {
            T oldItem = this[index];
            OnItemRemoved(default(T), oldItem, index, false);
            base.SetItem(index, item);
            OnItemAdded(item, oldItem, index, false);
        }

        protected virtual void OnItemAdded(T newItem, T oldItem, int index, bool isCountChanged)
        {
            _itemAdded?.Invoke(this, new ObservableListChangedEventArgs(CollectionChangedAction.Add, newItem, oldItem, index, isCountChanged));
        }

        protected virtual void OnItemRemoved(T newItem, T oldItem, int index, bool isCountChanged)
        {
            _itemRemoved?.Invoke(this, new ObservableListChangedEventArgs(CollectionChangedAction.Remove, newItem, oldItem, index, isCountChanged));
        }
    }
}
