using System;

namespace Ramp.Utilities.Collections
{
    public class ObservableListChangedEventArgs : EventArgs
    {
        public ObservableListChangedEventArgs(CollectionChangedAction action, object newItem, object oldItem, int index, bool isCountChanged)
        {
            Action = action;
            NewItem = newItem;
            OldItem = oldItem;
            Index = index;
            IsCountChanged = isCountChanged;
        }

        public CollectionChangedAction Action { get; }

        public object NewItem { get; }

        public object OldItem { get; }

        public int Index { get; }

        public bool IsCountChanged { get; }
    }
}