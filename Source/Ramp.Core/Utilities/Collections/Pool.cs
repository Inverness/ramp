using System;
using System.Collections.Generic;

namespace Ramp.Utilities.Collections
{
    /// <summary>
    ///     Provides pooling for class instances.
    /// </summary>
    /// <typeparam name="T"> A class type. </typeparam>
    public sealed class Pool<T>
        where T : class
    {
        private readonly Stack<T> _objects;
        private int _capacity;

        /// <summary>
        ///     Initialize an instance of Pool with the specified capacity.
        /// </summary>
        /// <param name="capacity"> The capacity of the pool. </param>
        public Pool(int capacity)
        {
            Validate.ArgumentInRange(capacity > 0, "capacity", "capacity must be greater than zero");

            _objects = new Stack<T>(Math.Min(capacity, 20));
            _capacity = capacity;
        }

        /// <summary>
        ///     Gets or sets amount of released objects that can be stored in the pool.
        /// </summary>
        public int Capacity
        {
            get { return _capacity; }

            set
            {
                Validate.ArgumentInRange(value > 0, "value", "capacity must be greater than zero");

                _capacity = value;

                if (_objects.Count > value)
                {
                    while (_objects.Count > value)
                        _objects.Pop();
                    _objects.TrimExcess();
                }
            }
        }

        /// <summary>
        ///     Gets the number of released objects in the pool.
        /// </summary>
        public int Count => _objects.Count;

        /// <summary>
        ///     Try acquiring an object from the pool. Returns null if no object was found.
        /// </summary>
        /// <returns> An instance of T if one is in the pool, otherwise null. </returns>
        public T Acquire()
        {
            Stack<T> objects = _objects; // for inlining
            return objects.Count != 0 ? objects.Pop() : null;
        }

        /// <summary>
        ///     Release an instance of T and add it to the object pool if it has not reached capcity.
        /// </summary>
        /// <param name="instance"> An instance of T. </param>
        /// <exception cref="ArgumentNullException"> Instance is null. </exception>
        public void Release(T instance)
        {
            Validate.ArgumentNotNull(instance, "instance");

            Stack<T> objects = _objects;
            if (objects.Count != Capacity)
                objects.Push(instance);
        }

        /// <summary>
        ///		Invokes Release() for each instance in the collection.
        /// </summary>
        /// <param name="instances"> A collection of instances. </param>
        /// <exception cref="ArgumentNullException"> Instances is null. </exception>
        public void Release(IEnumerable<T> instances)
        {
            Validate.ArgumentNotNull(instances, "instances");

            foreach (T instance in instances)
                Release(instance);
        }
    }
}