﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Ramp.Utilities.Collections
{
    /// <summary>
    /// A base class for a list where items in the list contain a key. Items cannot be null. The key of an item
    /// should not change while it is in the list. A dictionary is not used so key-based lookup will slow with the
    /// number of items in the list.
    /// </summary>
    /// <typeparam name="TKey">The key type</typeparam>
    /// <typeparam name="TItem">The item type</typeparam>
    public abstract class KeyedList<TKey, TItem> : ListBase<TItem>
    {
        protected KeyedList()
        {
        }

        protected KeyedList(int capacity)
            : base(capacity)
        {
        }

        public TItem this[TKey key]
        {
            get
            {
                int index;
                if ((index = IndexOf(key)) != -1)
                    return this[index];
                throw new KeyNotFoundException();
            }
        }

        public bool Contains(TKey key)
        {
            return IndexOf(key) != -1;
        }

        public int IndexOf(TKey key)
        {
            int localCount;
            if ((localCount = Count) != 0)
            {
                EqualityComparer<TKey> comparer = EqualityComparer<TKey>.Default;

                for (int i = 0; i < localCount; i++)
                {
                    if (comparer.Equals(GetKey(this[i]), key))
                        return i;
                }
            }

            return -1;
        }

        public bool Remove(TKey key)
        {
            int index = IndexOf(key);
            if (index != -1)
            {
                RemoveAt(index);
                return true;
            }
            return false;
        }

        protected override void SetItem(int index, TItem item)
        {
            ValidateNonNullItem(item);
            ValidateNonNullKey(item);

            base.SetItem(index, item);
        }

        protected override void InsertItem(int index, TItem item)
        {
            ValidateNonNullItem(item);
            ValidateNonNullKey(item);

            int existingIndex = IndexOf(GetKey(item));
            if (existingIndex != -1)
                throw new InvalidOperationException("item already exists with this key");

            base.InsertItem(index, item);
        }

        protected abstract TKey GetKey(TItem value);

        [Conditional("DEBUG"), Conditional("VALIDATE")]
        protected void ValidateNonNullKey(TItem item)
        {
            if (GetKey(item) == null)
                throw new ArgumentException("item key must not be null", nameof(item));
        }
    }
}
