using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Ramp.Utilities.Collections
{
    /// <summary>
    ///     An implementation of KeyedCollection that uses a delegate to handle getting the key from an item.
    /// </summary>
    /// <typeparam name="TKey"> The key type. </typeparam>
    /// <typeparam name="TItem"> The item type. </typeparam>
    public class KeyedCollectionEx<TKey, TItem> : KeyedCollection<TKey, TItem>
    {
        private readonly Func<TItem, TKey> _keyGetter;

        public KeyedCollectionEx(Func<TItem, TKey> keyGetter)
            : this(keyGetter, EqualityComparer<TKey>.Default, 0)
        {
        }

        public KeyedCollectionEx(Func<TItem, TKey> keyGetter, IEqualityComparer<TKey> comparer)
            : this(keyGetter, comparer, 0)
        {
        }

        public KeyedCollectionEx(Func<TItem, TKey> keyGetter, IEqualityComparer<TKey> comparer,
                                 int dictionaryCreationThreshold)
            : base(comparer, dictionaryCreationThreshold)
        {
            if (keyGetter == null)
                throw new ArgumentNullException(nameof(keyGetter));

            _keyGetter = keyGetter;
        }

        protected override TKey GetKeyForItem(TItem item)
        {
            return _keyGetter(item);
        }
    }
}