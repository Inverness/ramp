using System;
using System.Collections;
using System.Collections.Generic;

namespace Ramp.Utilities.Collections
{
    public class DictionaryView<TKey, TValue> : CollectionView<KeyValuePair<TKey, TValue>>, IDictionary<TKey, TValue>,
                                                IReadOnlyDictionary<TKey, TValue>
    {
        protected readonly IDictionary<TKey, TValue> InnerDict;

        public DictionaryView(IDictionary<TKey, TValue> inner)
            : base(inner)
        {
            InnerDict = inner;
        }

        void IDictionary<TKey, TValue>.Add(TKey key, TValue value)
        {
            throw new InvalidOperationException("dictionary is read-only");
        }

        public bool ContainsKey(TKey key)
        {
            return InnerDict.ContainsKey(key);
        }

        public ICollection<TKey> Keys => InnerDict.Keys;

        bool IDictionary<TKey, TValue>.Remove(TKey key)
        {
            throw new InvalidOperationException("dictionary is read-only");
        }

        public bool TryGetValue(TKey key, out TValue value)
        {
            return InnerDict.TryGetValue(key, out value);
        }

        public ICollection<TValue> Values => InnerDict.Values;

        public TValue this[TKey key]
        {
            get { return InnerDict[key]; }

            set { throw new InvalidOperationException("dictionary is read-only"); }
        }

        public new IEnumerator GetEnumerator()
        {
            return InnerDict.GetEnumerator();
        }


        IEnumerable<TKey> IReadOnlyDictionary<TKey, TValue>.Keys => InnerDict.Keys;

        IEnumerable<TValue> IReadOnlyDictionary<TKey, TValue>.Values => InnerDict.Values;
    }
}