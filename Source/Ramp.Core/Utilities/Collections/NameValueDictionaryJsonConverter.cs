﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using Newtonsoft.Json;

namespace Ramp.Utilities.Collections
{
    internal class NameValueDictionaryJsonConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType.IsSubclassOf(typeof(NameValueDictionary));
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null)
                return null;

            if (reader.TokenType != JsonToken.StartObject)
                throw new InvalidDataException("Expected StartObject token");

            var d = (NameValueDictionary) Activator.CreateInstance(objectType);

            while (reader.Read())
            {
                if (reader.TokenType == JsonToken.EndObject)
                    break;

                Debug.Assert(reader.TokenType == JsonToken.PropertyName);

                var key = (string) reader.Value;

                reader.Read();

                var value = (string) reader.Value;

                d.Add(key, value);
            }

            return d;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var d = (NameValueDictionary) value;

            writer.WriteStartObject();
            foreach (KeyValuePair<string, object> item in d)
            {
                writer.WritePropertyName(item.Key);

                object itemValue = item.Value;
                if (itemValue == null)
                {
                    writer.WriteNull();
                }
                else if (itemValue is string)
                {
                    writer.WriteValue((string) itemValue);
                }
                else
                {
                    TypeConverter converter = TypeDescriptor.GetConverter(itemValue.GetType());
                    writer.WriteValue(converter.ConvertToString(itemValue));
                }
            }
            writer.WriteEndObject();
        }
    }
}