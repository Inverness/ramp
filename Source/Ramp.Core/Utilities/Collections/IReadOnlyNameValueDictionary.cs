using System;
using System.Collections.Generic;

namespace Ramp.Utilities.Collections
{
    /// <summary>
    ///     Provides a read-only view of a NameValueDictionary.
    /// </summary>
    public interface IReadOnlyNameValueDictionary : IReadOnlyDictionary<string, object>
    {
        /// <summary>
        ///		Get a converted value.
        /// </summary>
        /// <typeparam name="T"> The type to convert the value to. </typeparam>
        /// <param name="key"> A key. </param>
        /// <param name="defaultValue"> The default value to return if a matching value does not exist. </param>
        /// <returns> A matching value, or defaultValue if the value was not in the dictionary. </returns>
        /// <exception cref="InvalidCastException">
        ///		A type converter could not convert from the source type, and a cast to the destination type failed.
        /// </exception>
        T GetValue<T>(string key, T defaultValue = default(T));

        /// <summary>
        ///     Try getting a converted value.
        /// </summary>
        /// <typeparam name="T"> The type to convert the value to. </typeparam>
        /// <param name="key"> A key. </param>
        /// <param name="value"> A matching value, or default(T) if the value was not in the dictionary. </param>
        /// <returns> True if the value was found in the dictionary, otherwise false. </returns>
        /// <exception cref="InvalidCastException"> Conversion to the destination type failed. </exception>
        bool TryGetValue<T>(string key, out T value);
    }
}