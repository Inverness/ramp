using System;
using System.Collections.Generic;

namespace Ramp.Utilities.Collections
{
    public class ListView<T> : CollectionView<T>, IList<T>, IReadOnlyList<T>
    {
        protected readonly IList<T> InnerList;

        public ListView(IList<T> list)
            : base(list)
        {
            InnerList = list;
        }

        public int IndexOf(T item)
        {
            return InnerList.IndexOf(item);
        }

        void IList<T>.Insert(int index, T item)
        {
            throw new InvalidOperationException("list is read-only");
        }

        void IList<T>.RemoveAt(int index)
        {
            throw new InvalidOperationException("list is read-only");
        }

        public T this[int index]
        {
            get { return InnerList[index]; }

            set { throw new InvalidOperationException("list is read-only"); }
        }
    }
}