﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Ramp.Utilities.Collections
{
    public class CollectionView<T> : ICollection<T>, IReadOnlyCollection<T>
    {
        protected readonly ICollection<T> Inner;

        public CollectionView(ICollection<T> inner)
        {
            Validate.ArgumentNotNull(inner, "collection");
            Inner = inner;
        }

        void ICollection<T>.Add(T item)
        {
            throw new InvalidOperationException("collection is read-only");
        }

        void ICollection<T>.Clear()
        {
            throw new InvalidOperationException("collection is read-only");
        }

        public bool Contains(T item)
        {
            return Inner.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            Inner.CopyTo(array, arrayIndex);
        }

        public int Count => Inner.Count;

        public bool IsReadOnly => true;

        bool ICollection<T>.Remove(T item)
        {
            throw new InvalidOperationException("collection is read-only");
        }

        public IEnumerator<T> GetEnumerator()
        {
            return Inner.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return Inner.GetEnumerator();
        }
    }
}