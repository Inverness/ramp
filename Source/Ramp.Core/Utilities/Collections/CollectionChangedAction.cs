namespace Ramp.Utilities.Collections
{
    public enum CollectionChangedAction : byte
    {
        Add,
        Remove
    }
}