﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Ramp.Utilities.Collections
{
    /// <summary>
    ///		A specialized dictionary for storing easily serialized primitive types. All values must provide a
    ///		TypeConverter that supports string conversion.
    /// </summary>
    [JsonConverter(typeof(NameValueDictionaryJsonConverter))]
    public sealed class NameValueDictionary : Dictionary<string, object>, IReadOnlyNameValueDictionary
    {
        public NameValueDictionary()
        {
        }

        public NameValueDictionary(IDictionary<string, object> dictionary) : base(dictionary)
        {
        }

        public NameValueDictionary(int capacity) : base(capacity)
        {
        }

        public NameValueDictionary(IReadOnlyDictionary<string, object> dictionary) : base(dictionary.Count)
        {
            foreach (KeyValuePair<string, object> item in dictionary)
                Add(item.Key, item.Value);
        }

        /// <summary>
        ///		Get a converted value.
        /// </summary>
        /// <typeparam name="T"> The type to convert the value to. </typeparam>
        /// <param name="key"> A key. </param>
        /// <param name="defaultValue"> The default value to return if a matching value does not exist. </param>
        /// <returns> A matching value, or defaultValue if the value was not in the dictionary. </returns>
        /// <exception cref="InvalidCastException">
        ///		A type converter could not convert from the source type, and a cast to the destination type failed.
        /// </exception>
        public T GetValue<T>(string key, T defaultValue = default(T))
        {
            object baseValue;
            return base.TryGetValue(key, out baseValue) ? GenericTypeConverter.Convert<T>(baseValue) : defaultValue;
        }

        /// <summary>
        ///     Try getting a converted value.
        /// </summary>
        /// <typeparam name="T"> The type to convert the value to. </typeparam>
        /// <param name="key"> A key. </param>
        /// <param name="value"> A matching value, or default(T) if the value was not in the dictionary. </param>
        /// <returns> True if the value was found in the dictionary, otherwise false. </returns>
        /// <exception cref="InvalidCastException"> Conversion to the destination type failed. </exception>
        public bool TryGetValue<T>(string key, out T value)
        {
            object baseValue;
            if (base.TryGetValue(key, out baseValue))
            {
                value = GenericTypeConverter.Convert<T>(baseValue);
                return true;
            }
            else
            {
                value = default(T);
                return false;
            }
        }

        /// <summary>
        ///		Updates the current data dictionary with the specified items.
        /// </summary>
        /// <param name="items"> A collection of items that will overwrite the current values in the data dictionary. </param>
        public void Update(IEnumerable<KeyValuePair<string, object>> items)
        {
            Validate.ArgumentNotNull(items, "items");

            Dictionary<string, object> dict;
            if ((dict = items as Dictionary<string, object>) != null)
            {
                // PERF: Uses dictionary enumerator struct instead of allocating a class
                foreach (KeyValuePair<string, object> item in dict)
                    this[item.Key] = item.Value;
            }
            else
            {
                foreach (KeyValuePair<string, object> item in items)
                    this[item.Key] = item.Value;
            }
        }
    }
}