﻿using System;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace Ramp.Utilities.Collections
{
    public static class CollectionClassHelper
    {
        /// <summary>
        ///     A helper method for collection types that grows an internal array.
        /// </summary>
        /// <typeparam name="T"> The item type. </typeparam>
        /// <param name="array"> The array that might need to be grown. </param>
        /// <param name="size"> The desired size that the array should be able to hold. </param>
        /// <param name="minimum"> The minimum allowed size for the array. </param>
        /// <param name="growthRate"> The growth rate. </param>
        public static void EnsureSize<T>(ref T[] array, int size, int minimum, double growthRate)
        {
            Validate.ArgumentInRange(size >= 0, "size");
            Validate.ArgumentInRange(minimum >= 0, "minimum");
            Validate.ArgumentInRange(growthRate >= 1, "growthRate");

            int arrayLength = array?.Length ?? 0;

            if (arrayLength >= size)
                return;

            int newSize = (int) (arrayLength * growthRate);

            // Overflow check
            if (newSize < 0)
                newSize = int.MaxValue;

            if (newSize < minimum)
                newSize = minimum;

            if (newSize < size)
                newSize = size;

            Array.Resize(ref array, newSize);
        }
        
        public static int? TryCount<T>([NoEnumeration] IEnumerable<T> source)
        {
            int count;
            return TryCount(source, out count) ? count : (int?) null;
        }

        public static bool TryCount<T>([NoEnumeration] IEnumerable<T> source, out int count)
        {
            var gc = source as ICollection<T>;
            if (gc != null)
            {
                count = gc.Count;
                return true;
            }
            var c = source as ICollection;
            if (c != null)
            {
                count = c.Count;
                return true;
            }
            var s = source as string;
            if (s != null)
            {
                count = s.Length;
                return true;
            }
            count = 0;
            return false;
        }
    }
}