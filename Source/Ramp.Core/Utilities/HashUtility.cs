using System;
using System.Diagnostics;

namespace Ramp.Utilities
{
    // Source: http://stackoverflow.com/questions/263400/

    /// <summary>
    ///     Helper class for generating hash codes suitable
    ///     for use in hashing algorithms and data structures like a hash table.
    /// </summary>
    public static class HashUtility
    {
        public const sbyte PrimeSByte = 31;
        public const short PrimeInt16 = 27427;
        public const int PrimeInt32 = 15486893;

        // This is the maximum prime smaller than Array.MaxArrayLength
        public const int MaxPrimeArrayLength = 0x7FEFFFFD;

        private const int HashPrime = 101; // from Hashtable.HashPrime

        private static readonly int[] s_primes =
        {
            3, 7, 11, 17, 23, 29, 37, 47, 59, 71, 89, 107, 131, 163, 197, 239, 293, 353, 431, 521, 631, 761, 919,
            1103, 1327, 1597, 1931, 2333, 2801, 3371, 4049, 4861, 5839, 7013, 8419, 10103, 12143, 14591,
            17519, 21023, 25229, 30293, 36353, 43627, 52361, 62851, 75431, 90523, 108631, 130363, 156437,
            187751, 225307, 270371, 324449, 389357, 467237, 560689, 672827, 807403, 968897, 1162687, 1395263,
            1674319, 2009191, 2411033, 2893249, 3471899, 4166287, 4999559, 5999471, 7199369
        };

        public static bool IsPrime(int candidate)
        {
            if ((candidate & 1) != 0)
            {
                int limit = (int) Math.Sqrt(candidate);
                for (int divisor = 3; divisor <= limit; divisor += 2)
                {
                    if ((candidate % divisor) == 0)
                        return false;
                }
                return true;
            }
            return (candidate == 2);
        }

        public static int GetPrime(int min)
        {
            if (min < 0)
                throw new ArgumentException("capacity overflow");

            for (int i = 0; i < s_primes.Length; i++)
            {
                int prime = s_primes[i];
                if (prime >= min)
                    return prime;
            }

            //outside of our predefined table. 
            //compute the hard way. 
            for (int i = (min | 1); i < int.MaxValue; i += 2)
            {
                if (IsPrime(i) && ((i - 1) % HashPrime != 0))
                    return i;
            }

            return min;
        }

        public static int GetMinPrime()
        {
            return s_primes[0];
        }
        
        // Returns size of hashtable to grow to.
        public static int ExpandPrime(int oldSize)
        {
            int newSize = 2 * oldSize;

            // Allow the hashtables to grow to maximum possible size (~2G elements) before encoutering capacity overflow.
            // Note that this check works even when _items.Length overflowed thanks to the (uint) cast
            if ((uint) newSize > MaxPrimeArrayLength && MaxPrimeArrayLength > oldSize)
            {
                Debug.Assert(MaxPrimeArrayLength == GetPrime(MaxPrimeArrayLength), "Invalid MaxPrimeArrayLength");
                return MaxPrimeArrayLength;
            }

            return GetPrime(newSize);
        }

        /// <summary>
        ///     Combines the hash codes of objects to get a new hash code.
        /// </summary>
        /// <param name="obj1">The first object.</param>
        /// <param name="obj2">The second object.</param>
        /// <returns>
        ///     A hash code, suitable for use in hashing algorithms and data
        ///     structures like a hash table.
        /// </returns>
        public static int Combine<T1, T2>(T1 obj1, T2 obj2)
        {
            return CombineValues(obj1?.GetHashCode() ?? 0, obj2?.GetHashCode() ?? 0);
        }

        /// <summary>
        ///     Combines the hash codes of objects to get a new hash code.
        /// </summary>
        /// <param name="obj1">The first object.</param>
        /// <param name="obj2">The second object.</param>
        /// <param name="obj3">The third object.</param>
        /// <returns>
        ///     A hash code, suitable for use in hashing algorithms and data
        ///     structures like a hash table.
        /// </returns>
        public static int Combine<T1, T2, T3>(T1 obj1, T2 obj2, T3 obj3)
        {
            return Combine(obj1, Combine(obj2, obj3));
        }

        /// <summary>
        ///     Combines the hash codes of objects to get a new hash code.
        /// </summary>
        /// <param name="obj1">The first object.</param>
        /// <param name="obj2">The second object.</param>
        /// <param name="obj3">The third object.</param>
        /// <param name="obj4">The fourth object.</param>
        /// <returns>
        ///     A hash code, suitable for use in hashing algorithms and
        ///     data structures like a hash table.
        /// </returns>
        public static int Combine<T1, T2, T3, T4>(T1 obj1, T2 obj2, T3 obj3,
                                                  T4 obj4)
        {
            return Combine(Combine(obj1, obj2), Combine(obj3, obj4));
        }

        /// <summary>
        ///     Combines the hash codes of objects to get a new hash code.
        /// </summary>
        /// <param name="objects">
        ///     An array of objects used for generating the hash code.
        /// </param>
        /// <returns>
        ///     A hash code, suitable for use in hashing algorithms and data
        ///     structures like a hash table.
        /// </returns>
        public static int Combine(params object[] objects)
        {
            Validate.ArgumentNotNull(objects, "objects");
            int hash = 0;
            
            for (int i = 0; i < objects.Length; i++)
                hash = CombineValues(hash, objects[i]?.GetHashCode() ?? 0);

            return hash;
        }

        /// <summary>
        ///     Combines two hash code values.
        /// </summary>
        /// <param name="a"> The first hash code. </param>
        /// <param name="b"> The second hash code. </param>
        /// <returns> A new hash code based on the arguments. </returns>
        public static int CombineValues(int a, int b)
        {
            // From Tuple.CombineHashCodes
            return unchecked(((a << 5) + a) ^ b);
        }
    }
}