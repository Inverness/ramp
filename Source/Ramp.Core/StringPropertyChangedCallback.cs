namespace Ramp
{
    /// <summary>
    ///     A typed, string-only property changed callback. This is non-standard and used in cases where it is
    ///     undesirable to allocate the event arguments object.
    /// </summary>
    /// <typeparam name="T">The sender type.</typeparam>
    /// <param name="sender">The sender.</param>
    /// <param name="propertyName">The property name.</param>
    public delegate void StringPropertyChangedCallback<in T>(T sender, string propertyName);
}