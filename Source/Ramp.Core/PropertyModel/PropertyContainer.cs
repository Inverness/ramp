﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Ramp.Threading;

namespace Ramp.PropertyModel
{
    public delegate void PropertyContainerChangedEventHandler(PropertyContainer pc, PropertyKey key);

    /// <summary>
    ///     A lightweight container for properties. Can be inherited or embedded.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class PropertyContainer : DispatcherObject, IEnumerable<KeyValuePair<PropertyKey, object>>
    {
        private const int InitialSize = 4;
        private const float NormalGrowthRate = 1.4f;
        private static readonly ValueEntry[] s_emptyEntries = new ValueEntry[0];

        private object _owner;
        [JsonProperty("Entries", ItemConverterType = typeof(ValueEntryConverter))]
        private ValueEntry[] _entries = s_emptyEntries;
        private ushort _count;

        public PropertyContainer()
        {
            _owner = this;
        }

        public PropertyContainer(object owner)
        {
            _owner = owner ?? this;
        }

        public event PropertyContainerChangedEventHandler PropertyChanged;

        /// <summary>
        ///     Gets the owner of this property container.
        /// </summary>
        public object Owner => _owner;

        public IEnumerator<KeyValuePair<PropertyKey, object>> GetEnumerator()
        {
            if (_count == 0)
                yield break;

            ValueEntry[] entries = _entries;
            ushort count = _count;

            for (int i = 0; i < count; i++)
            {
                ValueEntry e = entries[i];
                yield return new KeyValuePair<PropertyKey, object>(e.Key, e.RealValueObject);
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public bool HasValue(PropertyKey key)
        {
            if (key == null)
                ThrowNullKey();

            ushort localIndex;
            // ReSharper disable once PossibleNullReferenceException
            return FindEntry(key.GlobalIndex, out localIndex);
        }

        /// <summary>
        ///     Gets the property value with the specified key.
        /// </summary>
        /// <typeparam name="T">The property type.</typeparam>
        /// <param name="key">A property key.</param>
        /// <param name="options">Options for getting the property value.</param>
        /// <returns> The property value, or the default value of the type if no value was found. </returns>
        public T GetValue<T>(PropertyKey<T> key, PropertyOptions options = PropertyOptions.None)
        {
            if (key == null)
                ThrowNullKey();

            return InternalGetValue(key, options);
        }

        /// <summary>
        ///     Gets the property value with the specified key.
        /// </summary>
        /// <param name="key">A property key.</param>
        /// <param name="options">Options for getting the property value.</param>
        /// <returns> The property value, or the default value of the type if no value was found. </returns>
        public object GetValue(PropertyKey key, PropertyOptions options = PropertyOptions.None)
        {
            if (key == null)
                ThrowNullKey();

            // ReSharper disable once PossibleNullReferenceException
            return key.GetValue(this, options);
        }

        /// <summary>
        ///     Sets the property value.
        /// </summary>
        /// <typeparam name="T">The property type.</typeparam>
        /// <param name="key">A property key.</param>
        /// <param name="value">A value to set.</param>
        /// <param name="options">Options for setting the property value.</param>
        public void SetValue<T>(PropertyKey<T> key, T value, PropertyOptions options = PropertyOptions.None)
        {
            if (key == null)
                ThrowNullKey();

            InternalSetValue(key, value, options);
        }

        /// <summary>
        ///     Sets the property value.
        /// </summary>
        /// <param name="key">A property key.</param>
        /// <param name="value">A value to set.</param>
        /// <param name="options">Options for setting the property value.</param>
        public void SetValue(PropertyKey key, object value, PropertyOptions options = PropertyOptions.None)
        {
            if (key == null)
                ThrowNullKey();

            // ReSharper disable once PossibleNullReferenceException
            key.SetValue(this, value, options);
        }

        /// <summary>
        ///     Sets the current owner of the property container and verifies that any existing properties are valid
        ///     for this container. This method is intended to be used-post serialization to hook up a property
        ///     container to its owner again.
        /// </summary>
        /// <param name="owner"> The new owner. If null, uses the current object. </param>
        /// <returns> True if the properties were valid and the owner was changed. </returns>
        protected bool SetOwner(object owner)
        {
            _owner = owner ?? this;
            return true;
        }

        protected virtual void OnSerializing(StreamingContext context)
        {
            // Avoids having empty entries in JSON
            Array.Resize(ref _entries, _count);
        }

        /// <summary>
        ///     Invoked after the property container has been deserialized. Fixes up the property entry count and
        ///     global indices for deserialized values.
        /// </summary>
        /// <param name="context"> The streaming context. </param>
        protected virtual void OnDeserialized(StreamingContext context)
        {
            Debug.Assert(_owner != null && _count == 0);
            _count = (ushort) _entries.Length;
        }

        private T InternalGetValue<T>(PropertyKey<T> key, PropertyOptions options)
        {
            PropertyMetadata<T> metadata = key.Metadata;

            if ((options & PropertyOptions.NoAccessor) == 0 && metadata.Getter != null)
            {
                return metadata.Getter(this, key, metadata);
            }

            ushort localIndex;
            if (FindEntry(key.GlobalIndex, out localIndex))
            {
                return key.IsValueType
                           ? ((ValueBox<T>) _entries[localIndex].Value).Value
                           : (T) _entries[localIndex].Value;
            }

            if ((options & PropertyOptions.NoDefault) == 0 && metadata.HasDefaultValue)
            {
                T defaultValue = metadata.GetDefaultValue(this, key, metadata);

                if ((options & PropertyOptions.NoDefaultCache) == 0 && metadata.CacheDefaultValue)
                    InternalSetValue(key, defaultValue, PropertyOptions.Raw);

                return defaultValue;
            }

            return default(T);
        }

        private void InternalSetValue<T>(PropertyKey<T> key, T value, PropertyOptions options)
        {
            PropertyMetadata<T> metadata = key.Metadata;

            // Accessors take precedence over everything but ownership
            if ((options & PropertyOptions.NoAccessor) == 0 && metadata.Setter != null)
            {
                metadata.Setter(this, key, value, metadata);
                return;
            }

            bool hasChangedCallback = (options & PropertyOptions.NoCallback) == 0 && metadata.ChangedCallback != null;

            T oldValue = default(T);
            ushort localIndex;
            if (FindEntry(key.GlobalIndex, out localIndex))
            {
                ValueEntry entry = _entries[localIndex];

                if (key.IsValueType)
                {
                    var box = (ValueBox<T>) entry.Value;
                    if (hasChangedCallback)
                        oldValue = box.Value;
                    box.Value = value;
                }
                else
                {
                    if (hasChangedCallback)
                        oldValue = (T) entry.Value;
                    entry.Value = value;
                }
            }
            else
            {
                // NoDefaultCache lets us avoid an infinite recursion
                if (hasChangedCallback)
                    oldValue = InternalGetValue(key, PropertyOptions.NoDefaultCache);

                InsertEntry(localIndex, ValueEntry.Create(key, value));
            }

            if (hasChangedCallback && !EqualityComparer<T>.Default.Equals(oldValue, value))
                metadata.ChangedCallback(this, key, value, oldValue);

            if ((options & PropertyOptions.NoEvents) == 0)
                PropertyChanged?.Invoke(this, key);
        }

        // Return local index of the entry with the specified global index, or an insertion point if not found.
        // This method uses a binary search to identify the local entry index. This is suitable for containers
        // with a small number of properties which is common.
        private bool FindEntry(ushort globalIndex, out ushort localIndex)
        {
            uint high = _count;

            if (high == 0)
            {
                localIndex = 0;
                return false;
            }

            uint low = 0;
            ushort checkIndex;
            ValueEntry[] entries = _entries;

            while (high - low > 3)
            {
                uint pivot = (high + low) / 2;
                checkIndex = entries[pivot].GlobalIndex;
                if (globalIndex == checkIndex)
                {
                    localIndex = (ushort) pivot;
                    return true;
                }

                if (globalIndex <= checkIndex)
                    high = pivot;
                else
                    low = pivot + 1;
            }

            do
            {
                checkIndex = entries[low].GlobalIndex;

                if (checkIndex == globalIndex)
                {
                    localIndex = (ushort) low;
                    return true;
                }
                if (checkIndex > globalIndex)
                    break;
                low++;
            } while (low < high);

            localIndex = (ushort) low;
            return false;
        }

        private void InsertEntry(ushort localIndex, ValueEntry entry)
        {
            ushort count = _count;
            ValueEntry[] entries = _entries;

            if (count > 0)
            {
                if (entries.Length == count)
                {
                    var newSize = (int) (count * NormalGrowthRate);
                    var newEntries = new ValueEntry[newSize];
                    Array.Copy(entries, 0, newEntries, 0, localIndex);
                    newEntries[localIndex] = entry;
                    Array.Copy(entries, localIndex, newEntries, localIndex + 1, count - localIndex);
                    entries = newEntries;

                }
                else
                {
                    Array.Copy(entries, localIndex, entries, localIndex + 1, count - localIndex);
                    entries[localIndex] = entry;
                }
            }
            else
            {
                if (entries.Length == 0)
                    entries = new ValueEntry[InitialSize];
                entries[0] = entry;
            }

            _count = (ushort) (count + 1);
            _entries = entries;
        }

        [OnSerializing]
        private void OnSerializingCallback(StreamingContext context)
        {
            OnSerializing(context);
        }

        [OnDeserialized]
        private void OnDeserializedCallback(StreamingContext context)
        {
            OnDeserialized(context);
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        private static void ThrowNullKey()
        {
            // ReSharper disable once NotResolvedInText
            throw new ArgumentNullException("key");
        }

        internal struct ValueEntry
        {
            public readonly ushort GlobalIndex; // in struct for better cache performance

            public readonly PropertyKey Key;

            public object Value;

            private ValueEntry(PropertyKey key, object value)
            {
                GlobalIndex = key.GlobalIndex;
                Key = key;
                Value = value;
            }

            public object RealValueObject => Key.IsValueType ? ((ValueBox) Value).ValueObject : Value;

            public static ValueEntry Create<T>(PropertyKey<T> key, T value)
            {
                return new ValueEntry(key, key.IsValueType ? new ValueBox<T>(value) : (object) value);
            }

            public static ValueEntry Create(PropertyKey key, object value)
            {
                return new ValueEntry(key, key.IsValueType ? key.CreateBox(value) : value);
            }
        }

        internal abstract class ValueBox
        {
            public abstract object ValueObject { get; set; }
        }

        internal sealed class ValueBox<T> : ValueBox
        {
            public T Value;

            public ValueBox(T value)
            {
                Value = value;
            }

            public override object ValueObject
            {
                get { return Value; }

                set { Value = (T) value; }
            }
        }
    }

    internal class ValueEntryConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(PropertyContainer.ValueEntry);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            VerifyTokenType(reader, JsonToken.StartObject);

            string keyString = null;
            string typeString = null;
            JToken valueToken = null;

            while (reader.Read())
            {
                if (reader.TokenType == JsonToken.EndObject)
                    break;

                VerifyTokenType(reader, JsonToken.PropertyName);

                switch ((string) reader.Value)
                {
                    case "Key":
                        keyString = reader.ReadAsString();
                        break;
                    case "Type":
                        typeString = reader.ReadAsString();
                        break;
                    case "Value":
                        reader.Read();
                        valueToken = JToken.ReadFrom(reader);
                        break;
                }
            }

            if (keyString == null)
                throw new JsonSerializationException("No key");
            if (valueToken == null)
                throw new JsonSerializationException("No value");

            PropertyKey key = PropertyKey.GetPropertyKey(keyString);
            Type type = typeString != null ? Type.GetType(typeString, true) : null;

            return PropertyContainer.ValueEntry.Create(key, valueToken.ToObject(type, serializer));
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var entry = (PropertyContainer.ValueEntry) value;

            PropertyKey entryKey = entry.Key;
            string entryKeyName = entryKey.ToString();
            object entryValue = entry.RealValueObject;
            JToken entryValueToken = JToken.FromObject(entryValue, serializer);

            // Only write the type if it is a subclass of the property key type.
            string entryValueTypeName = entryValue != null && entryValue.GetType() != entryKey.PropertyType
                                            ? PropertyKey.GetShortAssemblyQualifiedName(entryValue.GetType())
                                            : null;

            writer.WriteStartObject();

            writer.WritePropertyName("Key");
            writer.WriteValue(entryKeyName);

            if (entryValueTypeName != null)
            {
                writer.WritePropertyName("Type");
                writer.WriteValue(entryValueTypeName);
            }

            writer.WritePropertyName("Value");
            entryValueToken.WriteTo(writer);

            writer.WriteEndObject();
        }

        private static void VerifyTokenType(JsonReader reader, JsonToken type)
        {
            if (reader.TokenType != type)
                throw new JsonReaderException("expected token: " + type);
        }
    }
}
