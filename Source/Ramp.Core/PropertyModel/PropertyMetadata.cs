
namespace Ramp.PropertyModel
{
    public abstract class PropertyMetadata
    {
        internal PropertyMetadata(bool hasDefaultValue, bool cacheDefaultValue)
        {
            HasDefaultValue = hasDefaultValue;
            CacheDefaultValue = cacheDefaultValue;
        }

        public bool CacheDefaultValue { get; protected set; }

        public bool HasDefaultValue { get; protected set; }
    }
}