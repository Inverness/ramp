﻿using System;
using System.Collections.Generic;

namespace Ramp.PropertyModel
{
    /// <summary>
    ///     A key for accessing a property in a PropertyContainer. Also defines the behavior of that property.
    ///     PropertyKey instances should only be statically allocated.
    /// </summary>
    public sealed class PropertyKey<T> : PropertyKey
    {
        private static PropertyKey<T> s_default;

        private readonly PropertyMetadata<T> _metadata; 

        private PropertyKey(string name, Type ownerType, PropertyMetadata<T> metadata)
            : base(name, typeof(T), ownerType)
        {
            _metadata = metadata ?? new PropertyMetadata<T>();
        }

        public PropertyMetadata<T> Metadata => _metadata;

        public override PropertyMetadata MetadataObject => _metadata;

        public static PropertyKey<T> Default => s_default ?? (s_default = Create(DefaultPropertyKeyName, typeof(T)));

        public static PropertyKey<T> Create(string name, Type ownerType, PropertyMetadata<T> metadata = null)
        {
            return new PropertyKey<T>(name, ownerType, metadata);
        }

        internal override object GetValue(PropertyContainer container, PropertyOptions options)
        {
            return container.GetValue(this, options);
        }

        internal override void SetValue(PropertyContainer container, object value, PropertyOptions options)
        {
            container.SetValue(this, (T) value, options);
        }

        internal override object CreateBox(object value)
        {
            return new PropertyContainer.ValueBox<T>(value != null ? (T) value : default(T));
        }
    }
}
