﻿using System;

namespace Ramp.PropertyModel
{
    /// <summary>
    ///     Options to use when getting or setting a property.
    /// </summary>
    [Flags]
    public enum PropertyOptions
    {
        /// <summary>
        ///     No options specified.
        /// </summary>
        None = 0x00,

        NoAccessor = 0x01,

        NoDefault = 0x02,

        NoCallback = 0x04,

        NoEvents = 0x08,

        NoDefaultCache = 0x10,

        /// <summary>
        ///     Get or set a value without utilizing any accessor or default value resolution.
        /// </summary>
        Raw = NoAccessor | NoDefault | NoCallback | NoEvents | NoDefaultCache
    }
}