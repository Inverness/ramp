﻿namespace Ramp.PropertyModel
{
    public delegate void PropertyChangedCallback<in T>(PropertyContainer container, PropertyKey key, T newValue, T oldValue);
}