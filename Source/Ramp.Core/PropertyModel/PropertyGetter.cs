namespace Ramp.PropertyModel
{
    public delegate T PropertyGetter<T>(PropertyContainer pc, PropertyKey<T> key, PropertyMetadata<T> metadata);
}