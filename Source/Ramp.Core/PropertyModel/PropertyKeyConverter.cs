﻿using System.ComponentModel;
using System.Globalization;
using Ramp.Data;

namespace Ramp.PropertyModel
{
    public class PropertyKeyConverter : StringTypeConverter
    {
        protected override object ConvertFromStringCore(ITypeDescriptorContext context, CultureInfo culture,
                                                        string value)
        {
            return PropertyKey.GetPropertyKey(value);
        }
    }
}
