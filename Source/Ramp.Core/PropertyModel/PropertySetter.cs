namespace Ramp.PropertyModel
{
    public delegate void PropertySetter<T>(PropertyContainer pc, PropertyKey<T> key, T value, PropertyMetadata<T> metadata);
}