﻿
namespace Ramp.PropertyModel
{
    public class PropertyMetadata<T> : PropertyMetadata
    {
        private readonly T _defaultValue;
        private readonly PropertyGetter<T> _defaultFactory;

        public PropertyMetadata(
            T defaultValue,
            PropertyChangedCallback<T> callback = null,
            PropertyGetter<T> getter = null,
            PropertySetter<T> setter = null,
            PropertyGetter<T> defaultFactory = null,
            bool cacheDefaultValue = false
            )
            : this(defaultValue, true, callback, getter, setter, defaultFactory, cacheDefaultValue)
        {
        }

        public PropertyMetadata(
            PropertyChangedCallback<T> callback = null,
            PropertyGetter<T> getter = null,
            PropertySetter<T> setter = null,
            PropertyGetter<T> defaultFactory = null,
            bool cacheDefaultValue = false
            )
            : this(default(T), false, callback, getter, setter, defaultFactory, cacheDefaultValue)
        {
        }

        protected PropertyMetadata(
            T defaultValue,
            bool hasDefaultValue,
            PropertyChangedCallback<T> callback,
            PropertyGetter<T> getter,
            PropertySetter<T> setter,
            PropertyGetter<T> defaultFactory,
            bool cacheDefaultValue
            )
            : base(hasDefaultValue, cacheDefaultValue)
        {
            if (defaultFactory != null)
                defaultValue = default(T);

            _defaultValue = defaultValue;
            _defaultFactory = defaultFactory;

            Getter = getter;
            Setter = setter;
            ChangedCallback = callback;
        }

        public PropertyGetter<T> Getter { get; protected set; }

        public PropertySetter<T> Setter { get; protected set; }

        public PropertyChangedCallback<T> ChangedCallback { get; protected set; }

        public virtual T GetDefaultValue(PropertyContainer pc, PropertyKey<T> key, PropertyMetadata<T> metadata)
        {
            return _defaultFactory == null ? _defaultValue : _defaultFactory(pc, key, metadata);
        }
    }
}
