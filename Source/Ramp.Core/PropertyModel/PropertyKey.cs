using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;

namespace Ramp.PropertyModel
{
    /// <summary>
    ///     A key for accessing a property in a PropertyContainer. Also defines the behavior of that property.
    ///     PropertyKey instances should only be statically allocated. All instances of this type are also instances
    ///     of PropertyKey&lt;T&gt;.
    /// </summary>
    [TypeConverter(typeof(PropertyKeyConverter))]
    public abstract class PropertyKey : IComparable<PropertyKey>
    {
        internal const string DefaultPropertyKeyName = "<Default>";
        internal readonly ushort GlobalIndex;
        internal readonly bool IsValueType;

        private static readonly object s_lock = new object();
        private static readonly List<PropertyKey> s_allKeys = new List<PropertyKey>();
        private static readonly Dictionary<string, PropertyKey> s_allKeysDict = new Dictionary<string, PropertyKey>();
        private static readonly Dictionary<Type, PropertyKey> s_defaultKeys = new Dictionary<Type, PropertyKey>(); 

        internal PropertyKey(string name, Type propertyType, Type ownerType)
        {
            Validate.ArgumentNotNull(name, "name");
            Validate.ArgumentNotNull(propertyType, "propertyType");
            Validate.ArgumentNotNull(ownerType, "ownerType");

            Name = name;
            PropertyType = propertyType;
            OwnerType = ownerType;

            OwnerQualifiedName = Name + ", " + GetShortAssemblyQualifiedName(ownerType);
            IsValueType = propertyType.IsValueType; 

            lock (s_lock)
            {
                int nextIndex = s_allKeys.Count + 1;
                if (nextIndex > ushort.MaxValue)
                    throw new InvalidOperationException();
                GlobalIndex = (ushort) nextIndex;
                s_allKeysDict.Add(OwnerQualifiedName, this);
                s_allKeys.Add(this);
                if (name == DefaultPropertyKeyName)
                    s_defaultKeys.Add(ownerType, this);
            }
        }

        /// <summary>
        ///     Gets the property name.
        /// </summary>
        public string Name { get; }

        /// <summary>
        ///     Gets the property value type.
        /// </summary>
        public Type PropertyType { get; }

        /// <summary>
        ///     Gets the type that owns this property.
        /// </summary>
        public Type OwnerType { get; }

        /// <summary>
        ///     Gets the metadata specified for this property.
        /// </summary>
        public abstract PropertyMetadata MetadataObject { get; }

        public string OwnerQualifiedName { get; }

        public int CompareTo(PropertyKey other)
        {
            if (other == null)
                return 0;
            return string.Compare(Name, other.Name, StringComparison.Ordinal);
        }

        /// <summary>
        ///     Returns a fully qualified string identifying the property key that can be used to retrieve it.
        /// </summary>
        /// <returns></returns>
        public sealed override string ToString()
        {
            return OwnerQualifiedName;
        }

        public static PropertyKey GetDefault(Type type)
        {
            Validate.ArgumentNotNull(type, "type");

            lock (s_lock)
            {
                PropertyKey pk;
                if (s_defaultKeys.TryGetValue(type, out pk))
                    return pk;
            }

            // Slow path

            Type propertyKeyType = typeof(PropertyKey<>).MakeGenericType(type);
            PropertyInfo property = propertyKeyType.GetProperty("Default", BindingFlags.Public | BindingFlags.Static);
            return (PropertyKey) property.GetValue(null); // will cache result
        }

        /// <summary>
        ///     Returns a property key
        /// </summary>
        /// <param name="value">The property key string.</param>
        /// <returns></returns>
        public static PropertyKey GetPropertyKey(string value)
        {
            lock (s_lock)
            {
                PropertyKey pk;
                if (s_allKeysDict.TryGetValue(value, out pk))
                    return pk;
            }

            // Slow path

            int firstComma = value.IndexOf(',');
            if (firstComma == -1)
                throw InvalidPropertyKeyName(value);

            string propertyName = value.Substring(0, firstComma).Trim();
            string typeName = value.Substring(firstComma + 1).Trim();

            Type type;
            try
            {
                type = Type.GetType(typeName, true);
            }
            catch (Exception ex)
            {
                throw InvalidPropertyKeyName(value, ex);
            }

            if (propertyName == DefaultPropertyKeyName)
                return GetDefault(type);

            // Type load should have caused creation of any property keys it might have

            lock (s_lock)
            {
                PropertyKey pk;
                if (s_allKeysDict.TryGetValue(value, out pk))
                    return pk;
            }

            throw InvalidPropertyKeyName(value);

            //string propertyFieldName = propertyName + "Property";
            //FieldInfo field = type.GetField(propertyFieldName, BindingFlags.Public | BindingFlags.Static);
            //if (field == null)
            //    throw InvalidPropertyKeyName(value);

            //return (PropertyKey) field.GetValue(null);
        }

        // Used to invoke the generic value getter method on a PropertyContainer
        internal abstract object GetValue(PropertyContainer container, PropertyOptions options);

        internal abstract void SetValue(PropertyContainer container, object value, PropertyOptions options);

        internal abstract object CreateBox(object value);

        internal static PropertyKey FromIndex(ushort globalIndex)
        {
            // global list only ever expands, no need to lock
            return s_allKeys[globalIndex];
        }

        internal static string GetShortAssemblyQualifiedName(Type type)
        {
            return type.FullName + ", " + type.Assembly.GetName().Name;
        }

        private static FormatException InvalidPropertyKeyName(string pk)
        {
            return new FormatException("Invalid property key name: " + pk);
        }

        private static FormatException InvalidPropertyKeyName(string pk, Exception inner)
        {
            return new FormatException("Invalid property key name: " + pk, inner);
        }
    }
}