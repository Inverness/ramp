﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;

namespace Ramp
{
    /// <summary>
    ///     Common utilities for enumerables and collection types.
    /// </summary>
    public static class CollectionUtility
    {
        public static T[] ToSingleOrEmptyArray<T>(T item)
        {
            return item != null ? new[] { item } : Array.Empty<T>();
        }

        public static bool IsNullOrEmpty<T>(T[] array)
        {
            return array == null || array.Length == 0;
        }

        public static int GetHashSetCapacity<T>(HashSet<T> set)
        {
            Func<HashSet<T>, int> func = Typed<T>.GetHashSetCapacityFunc;
            if (func == null)
                Typed<T>.GetHashSetCapacityFunc = func = MakeGetHashSetCapacityFunc<T>();
            return func(set);
        }

        private static Func<HashSet<T>, int> MakeGetHashSetCapacityFunc<T>()
        {
            FieldInfo slotsField = typeof(HashSet<T>).GetTypeInfo()
                                                     .GetField("m_slots", BindingFlags.NonPublic | BindingFlags.Instance);

            if (slotsField == null)
                throw new MissingFieldException("HashSet`1", "m_slots");

            var m = new DynamicMethod("GetHashSetCapacity", typeof(int), new[] { typeof(HashSet<T>) }, true);
            ILGenerator il = m.GetILGenerator();

            il.Emit(OpCodes.Ldarg_0);
            il.Emit(OpCodes.Ldfld, slotsField);
            il.Emit(OpCodes.Ldlen);
            il.Emit(OpCodes.Ret);

            return (Func<HashSet<T>, int>) m.CreateDelegate(typeof(Func<HashSet<T>, int>));
        }

        private static class Typed<T>
        {
            internal static Func<HashSet<T>, int> GetHashSetCapacityFunc;
        }
    }
}