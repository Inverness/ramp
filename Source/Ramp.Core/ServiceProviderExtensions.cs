using System;

namespace Ramp
{
    /// <summary>
    ///     Common utilities for a service model.
    /// </summary>
    public static class ServiceProviderExtensions
    {
        /// <summary>
        ///		Gets the service object of the specified type.
        /// </summary>
        /// <typeparam name="T"> The service type. </typeparam>
        /// <param name="provider"> The service provider. </param>
        /// <returns> The matching service, or null. </returns>
        public static T GetService<T>(this IServiceProvider provider) where T : class
        {
            return (T) provider.GetService(typeof(T));
        }
        
        public static T GetRequiredService<T>(this IServiceProvider provider) where T : class
        {
            var service = (T) provider.GetService(typeof(T));
            if (service == null)
                throw new InvalidOperationException("Service not found: " + typeof(T).FullName);
            return service;
        }
    }
}