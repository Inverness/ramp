﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Reflection;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using YamlDotNet.Core;
using YamlDotNet.RepresentationModel;

namespace Ramp.Data
{
    /// <summary>
    /// Provides methods to convert YAML documents to JSON documents.
    /// </summary>
    public static class YamlConverter
    {
        public static IList<JContainer> ToJson(YamlStream stream, JsonSerializerSettings settings = null)
        {
            Validate.ArgumentNotNull(stream, nameof(stream));

            var containers = new JContainer[stream.Documents.Count];

            for (int i = 0; i < containers.Length; i++)
                containers[i] = ToJson(stream.Documents[i], settings);

            return containers;
        }

        public static JContainer ToJson(YamlDocument doc, JsonSerializerSettings settings = null)
        {
            Validate.ArgumentNotNull(doc, nameof(doc));

            if (!(doc.RootNode is YamlSequenceNode) && !(doc.RootNode is YamlMappingNode))
                throw new JsonSerializationException("document root must be mapping or sequence");

            return (JContainer) InternalToJson(doc.RootNode, settings);
        }

        public static JToken ToJson(YamlNode node, JsonSerializerSettings settings = null)
        {
            Validate.ArgumentNotNull(node, nameof(node));
            
            return InternalToJson(node, settings);
        }

        public static YamlNode ToYaml(JToken token, JsonSerializerSettings settings = null)
        {
            Validate.ArgumentNotNull(token, nameof(token));

            return InternalFromJson(token, settings);
        }

        private static JToken InternalToJson(YamlNode y, JsonSerializerSettings settings)
        {
            return y switch
            {
                YamlScalarNode scalar => ScalarToJson(scalar, settings),
                YamlMappingNode mapping => MappingToJson(mapping, settings),
                YamlSequenceNode sequence => SequenceToJson(sequence, settings),
                _ => throw new JsonSerializationException("unexpected node type")
            };
        }

        private static YamlNode InternalFromJson(JToken token, JsonSerializerSettings settings)
        {
            return token switch
            {
                JValue value => ScalarFromJson(value, settings),
                JObject obj => MappingFromJson(obj, settings),
                JArray array => SequenceFromJson(array, settings),
                _ => throw new JsonSerializationException($"can't convert {token.GetType()} to YamlNode")
            };
        }

        private static JValue ScalarToJson(YamlScalarNode y, JsonSerializerSettings settings)
        {
            string value = y.Value;

            Debug.Assert(value != null, nameof(value) + " != null");

            // If its explicitly quoted or multi-line then it can't be a null, boolean, or number.
            if (y.Style <= ScalarStyle.Plain)
            {
                switch (value.Length)
                {
                    case 0:
                        return JValue.CreateNull();
                    case 1:
                        if (value[0] == '~')
                            return JValue.CreateNull();
                        break;
                    case 2:
                        if (string.Equals(value, "no", StringComparison.OrdinalIgnoreCase))
                            return new JValue(false);
                        break;
                    case 3:
                        if (string.Equals(value, "yes", StringComparison.OrdinalIgnoreCase))
                            return new JValue(true);
                        break;
                    case 4:
                        if (string.Equals(value, "null", StringComparison.OrdinalIgnoreCase))
                            return JValue.CreateNull();
                        if (string.Equals(value, "true", StringComparison.OrdinalIgnoreCase))
                            return new JValue(true);
                        break;
                    case 5:
                        if (string.Equals(value, "false", StringComparison.OrdinalIgnoreCase))
                            return new JValue(false);
                        break;
                }

                if (long.TryParse(value, out long longValue))
                    return new JValue(longValue);

                if (double.TryParse(value, out double doubleValue))
                    return new JValue(doubleValue);

                if (settings != null && settings.DateParseHandling != DateParseHandling.None)
                {
                    if (InternalAccess.TryParseDateTime(value,
                                                        settings.DateTimeZoneHandling,
                                                        settings.DateFormatString,
                                                        settings.Culture,
                                                        out DateTime dateObject))
                    {
                        return new JValue(dateObject);
                    }
                }
            }
            else
            {
                // Non-plain empty value is a string
                if (value.Length == 0)
                    return new JValue(string.Empty);
            }

            return new JValue(value);
        }

        private static YamlScalarNode ScalarFromJson(JValue value, JsonSerializerSettings settings)
        {
            switch (value.Type)
            {
                case JTokenType.Integer:
                case JTokenType.Float:
                    return new YamlScalarNode(value.ToString(CultureInfo.InvariantCulture));
                case JTokenType.String:
                    string stringValue = value.ToString(CultureInfo.InvariantCulture);

                    var scalarNode = new YamlScalarNode(stringValue);

                    // null, true, and false string values must be quoted
                    bool mustQuote = stringValue.Length <= 5 && stringValue.Length >= 4 &&
                                     (string.Equals(stringValue, "null", StringComparison.OrdinalIgnoreCase) ||
                                      string.Equals(stringValue, "true", StringComparison.OrdinalIgnoreCase) ||
                                      string.Equals(stringValue, "false", StringComparison.OrdinalIgnoreCase));

                    if (mustQuote)
                        scalarNode.Style = ScalarStyle.DoubleQuoted;

                    return scalarNode;
                case JTokenType.Boolean:
                    return new YamlScalarNode((bool) value.Value ? "true" : "false");
                case JTokenType.Null:
                    return new YamlScalarNode("null");
                case JTokenType.Date:
                    var formattable = (IFormattable) value.Value;

                    string formatted = formattable.ToString(settings.DateFormatString, CultureInfo.InvariantCulture);

                    return new YamlScalarNode(formatted);
                case JTokenType.Raw:
                case JTokenType.Bytes:
                case JTokenType.Guid:
                case JTokenType.Uri:
                case JTokenType.TimeSpan:
                    goto case JTokenType.String;
                default:
                    throw new JsonSerializationException($"can't convert {value.Type} to scalar");
            }
        }

        private static JArray SequenceToJson(YamlSequenceNode y, JsonSerializerSettings settings)
        {
            var array = new JArray();

            foreach (YamlNode c in y.Children)
                array.Add(InternalToJson(c, settings));

            return array;
        }

        private static YamlSequenceNode SequenceFromJson(JArray array, JsonSerializerSettings settings)
        {
            var s = new YamlSequenceNode();

            foreach (JToken item in array)
                s.Add(InternalFromJson(item, settings));

            return s;
        }

        private static JObject MappingToJson(YamlMappingNode y, JsonSerializerSettings settings)
        {
            var obj = new JObject();

            foreach (KeyValuePair<YamlNode, YamlNode> item in y.Children)
            {
                var scalarKey = item.Key as YamlScalarNode;
                if (scalarKey == null)
                    throw new JsonSerializationException("mapping key was not a scalar");

                obj.Add(scalarKey.Value, InternalToJson(item.Value, settings));
            }

            return obj;
        }

        private static YamlMappingNode MappingFromJson(JObject o, JsonSerializerSettings settings)
        {
            var m = new YamlMappingNode();

            foreach (KeyValuePair<string, JToken> item in o)
                m.Add(item.Key, InternalFromJson(item.Value, settings));

            return m;
        }

        internal static class InternalAccess
        {
            private const string DateTimeUtilsTypeName = "Newtonsoft.Json.Utilities.DateTimeUtils";
            private const string JsonAssemblyName = "Newtonsoft.Json";
            private const string TryParseDateTimeMethodName = "TryParseDateTime";

            private static readonly Type[] s_tryParseDateTimeTypes =
            {
                typeof(string), typeof(DateTimeZoneHandling), typeof(string), typeof(CultureInfo), typeof(DateTime).MakeByRefType()
            };

            private delegate bool TryParseDateTimeDelegateType(string s, DateTimeZoneHandling dateTimeZoneHandling, string dateFormatString, CultureInfo culture, out DateTime dt);

            private static TryParseDateTimeDelegateType s_tryParseDateTimeDelegate;

            // Json.NET's date time parsing is different than C#'s and the method internal, so need to use reflection
            // to get at it.
            internal static bool TryParseDateTime(string s, DateTimeZoneHandling dateTimeZoneHandling, string dateFormatString, CultureInfo culture, out DateTime dt)
            {
                // No need to sync
                if (s_tryParseDateTimeDelegate == null)
                {
                    Type type = Type.GetType(DateTimeUtilsTypeName + ", " + JsonAssemblyName, true);

                    MethodInfo method = type.GetMethod(TryParseDateTimeMethodName, BindingFlags.Static | BindingFlags.NonPublic, null, s_tryParseDateTimeTypes, null);

                    if (method == null)
                        throw new MissingMethodException(DateTimeUtilsTypeName, TryParseDateTimeMethodName);

                    s_tryParseDateTimeDelegate = (TryParseDateTimeDelegateType) method.CreateDelegate(typeof(TryParseDateTimeDelegateType));
                }

                return s_tryParseDateTimeDelegate(s, dateTimeZoneHandling, dateFormatString, culture, out dt);
            }
        }
    }
}
