using System.IO;
using Newtonsoft.Json;

namespace Ramp.Data
{
    public sealed class DirectoryFileManifestEntry : FileManifestEntry
    {
        /// <summary>
        ///		Initializes a file manifest entry for a file on disk.
        /// </summary>
        /// <param name="root"> The directory root. </param>
        /// <param name="path"> The path to the file from the root. </param>
        /// <param name="info"> The FileInfo object. </param>
        /// <param name="sha2"> A SHA2 hash of the file </param>
        public DirectoryFileManifestEntry(string root, string path, FileInfo info, byte[] sha2)
        {
            Validate.ArgumentNotNull(root, "root");
            Validate.ArgumentNotNull(path, "path");
            Validate.ArgumentNotNull(info, "info");
            Validate.Argument(sha2 == null || sha2.Length == 20, "expected null hash or 20 byte SHA2 hash");

            Root = root;
            Path = path;
            Modified = info.LastWriteTimeUtc;
            Size = info.Length;
            Sha2 = sha2;
        }

        [JsonConstructor]
        private DirectoryFileManifestEntry()
        {
        }

        public override bool IsReadOnly => false;
    }
}