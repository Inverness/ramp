using System;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using YamlDotNet.RepresentationModel;

namespace Ramp.Data
{
    public static class JsonUtility
    {
        /// <summary>
        ///     Loads a JSON text file at the specified path into a JObject.
        /// </summary>
        /// <param name="path"> Path to the JSON tex file. </param>
        /// <param name="schema"> Optional schema to validate the JSON object with. </param>
        /// <returns> A JObject of the file contents. </returns>
        public static JObject LoadObject(string path, JSchema schema = null)
        {
            using (var jsonReader = new JsonTextReader(new StreamReader(path)))
            {
                JObject jobject = JObject.Load(jsonReader);
                if (schema != null)
                    jobject.Validate(schema);
                return jobject;
            }
        }

        public static JToken LoadToken(string path, JSchema schema = null)
        {
            using (var jsonReader = new JsonTextReader(new StreamReader(path)))
            {
                JToken jtoken = JToken.Load(jsonReader);
                if (schema != null)
                    jtoken.Validate(schema);
                return jtoken;
            }
        }

        public static void Save(string path, JContainer container)
        {
            using (var streamWriter = new StreamWriter(path))
            using (var jsonWriter = new JsonTextWriter(streamWriter))
            {
                jsonWriter.Formatting = Formatting.Indented;
                container.WriteTo(jsonWriter);
            }
        }

        /// <summary>
        ///     Create a JSchema from an embedded resource.
        /// </summary>
        /// <param name="resource"> The resource bytes. </param>
        /// <returns> A new JSchema. </returns>
        public static JSchema ReadEmbeddedSchema(byte[] resource)
        {
            if (resource == null)
                throw new ArgumentNullException(nameof(resource));
            // We use a MemoryStream in order to detect the encoding of the file and avoid JSON parsing errors.
            using (var stream = new MemoryStream(resource))
            using (var reader = new StreamReader(stream))
            using (var jsonReader = new JsonTextReader(reader))
            {
                return JSchema.Load(jsonReader);
            }
        }

        /// <summary>
        /// Loads a JContainer from the first YAML document provided by a reader.
        /// </summary>
        public static JContainer LoadYaml(string path, JsonSerializerSettings settings = null)
        {
            var ys = new YamlStream();
            using (FileStream stream = File.OpenRead(path))
            using (StreamReader reader = new StreamReader(stream))
                ys.Load(reader);

            if (ys.Documents.Count == 0)
                throw new InvalidDataException("Expected at least one YAML document");

            return YamlConverter.ToJson(ys.Documents[0], settings);
        }

        /// <summary>
        /// Loads a JContainer from the first YAML document provided by a reader.
        /// </summary>
        public static JContainer LoadYaml(TextReader reader, JsonSerializerSettings settings = null)
        {
            var ys = new YamlStream();
            ys.Load(reader);

            if (ys.Documents.Count == 0)
                throw new InvalidDataException("Expected at least one YAML document");

            return YamlConverter.ToJson(ys.Documents[0], settings);
        }

        public static void SaveYaml(string path, JContainer container, JsonSerializerSettings settings = null)
        {
            YamlNode root = YamlConverter.ToYaml(container, settings);

            var yamlStream = new YamlStream(new YamlDocument(root));

            using (var writer = new StreamWriter(path))
                yamlStream.Save(writer);
        }

        public static T ValueToEnum<T>(this JToken token, string key)
        {
            return (T) Enum.Parse(typeof(T), token.Value<string>(key));
        }

        //public static string ReadExpectedString(this JsonReader reader)
        //{
        //	if (reader.TokenType != JsonToken.String)
        //		throw new InvalidDataException("expected string");
        //	var v = (string)reader.Value;
        //	reader.Read();
        //	return v;
        //}

        //public static T ReadExpectedInteger<T>(this JsonReader reader)
        //{
        //	if (reader.TokenType != JsonToken.Integer)
        //		throw new InvalidDataException("expected integer");
        //	var v = (T)Convert.ChangeType(reader.Value, typeof(T));
        //	reader.Read();
        //	return v;
        //}

        //public static T ReadExpectedFloat<T>(this JsonReader reader)
        //{
        //	if (reader.TokenType != JsonToken.Float)
        //		throw new InvalidDataException("expected float");
        //	var v = (T)Convert.ChangeType(reader.Value, typeof(T));
        //	reader.Read();
        //	return v;
        //}
    }

    public static class JsonExtensions
    {
        public static T ReplaceInline<T>(this JToken target, T value)
            where T : JToken
        {
            target.Replace(value);
            return value;
        }
    }
}