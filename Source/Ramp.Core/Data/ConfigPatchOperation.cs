namespace Ramp.Data
{
    /// <summary>
    /// Describes the type of operation a patch is applied with.
    /// </summary>
    public enum ConfigPatchOperation
    {
        /// <summary>
        /// The patch value will replace the existing value.
        /// </summary>
        Set,

        /// <summary>
        /// The patch value will be merged with the existing value. This only applies to arrays and objects. The
        /// existing value must be of the same type as the patch value.
        /// </summary>
        Add,

        /// <summary>
        /// The patch will remove a child of the existing value or the existing value itself. This behavior depends
        /// on the type of the patch value, which must be null, or an array. If the patch value is null, the
        /// existing value will be removed. If the patch value is an array, then it must be an array of property
        /// names to be removed from an existing object, or values to be removed from an existing array.
        /// </summary>
        Remove,

        /// <summary>
        /// Objects in the patch value will be recursively merged.
        /// </summary>
        Merge
    }
}