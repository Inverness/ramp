using ICSharpCode.SharpZipLib.Zip;
using Newtonsoft.Json;

namespace Ramp.Data
{
    public sealed class ZipFileManifestEntry : FileManifestEntry
    {
        internal int ZipEntryIndex = -1; // Cache the index of this entry within the ZipFile instance.

        /// <summary>
        ///		Initializes a file manifest entry for an item in a zip file.
        /// </summary>
        /// <param name="root"> The zip file root. </param>
        /// <param name="zipEntry"> The zip file entry being represented. </param>
        public ZipFileManifestEntry(string root, ZipEntry zipEntry)
        {
            Validate.ArgumentNotNull(root, "root");
            Validate.ArgumentNotNull(zipEntry, "zipEntry");

            Root = root;
            Path = zipEntry.Name;
            Modified = zipEntry.DateTime.ToUniversalTime();
            Size = zipEntry.Size;
            ZipEntryIndex = (int) zipEntry.ZipFileIndex;
        }

        [JsonConstructor]
        private ZipFileManifestEntry()
        {
        }

        public override bool IsReadOnly => true;

        protected override void OnManifestChanged()
        {
            ZipEntryIndex = -1;
        }
    }
}