﻿using System;
using System.IO;
using System.Security.Cryptography;
using NLog;

namespace Ramp.Data
{
    public class DirectoryFileManifest : FileManifest
    {
        private static readonly Logger s_log = LogManager.GetCurrentClassLogger();

        private readonly DirectoryInfo _directory;
        private readonly bool _hash;
        private readonly SHA512 _sha512Cng = SHA512.Create();

        public DirectoryFileManifest(string root, bool hash)
            : base(root)
        {
            _directory = new DirectoryInfo(root);
            _hash = hash;
        }

        public override void Dispose()
        {
            _sha512Cng.Dispose();
            base.Dispose();
        }

        protected override Type EntryType => typeof(DirectoryFileManifestEntry);

        protected override Stream OpenEntryCore(FileManifestEntry entry, FileMode mode, FileAccess access,
                                                FileShare share)
        {
            return new FileStream(entry.FullPath, mode, access, share);
        }

        protected override uint UpdateCore()
        {
            uint count = 0;

            s_log.Debug("Updating from " + Root);
            foreach (FileInfo fileInfo in _directory.EnumerateFiles("*.*", SearchOption.AllDirectories))
            {
                string relpath = fileInfo.FullName.Substring(_directory.FullName.Length + 1);

                string entryName = GetEntryName(relpath, true);
                if (entryName == null)
                    continue;

                byte[] hashBytes = null;

                if (_hash)
                {
                    using (FileStream stream = fileInfo.OpenRead())
                        hashBytes = _sha512Cng.ComputeHash(stream);
                }

                AddEntry(entryName, new DirectoryFileManifestEntry(Root, relpath, fileInfo, hashBytes));

                count++;
            }
            s_log.Debug("Finished update");

            return count;
        }
    }
}