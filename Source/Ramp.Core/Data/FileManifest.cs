﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using NLog;
using Newtonsoft.Json;
using Ramp.Utilities.Text;

namespace Ramp.Data
{
    [JsonObject(MemberSerialization.OptIn)]
    public abstract class FileManifest : IDisposable
    {
        private static readonly Logger s_log = LogManager.GetCurrentClassLogger();

        private static readonly JsonSerializerSettings s_serializerSettings = new JsonSerializerSettings
        {
            Formatting = Formatting.Indented,
            TypeNameHandling = TypeNameHandling.Auto
        };

        [JsonProperty("Root")]
        private readonly string _root;

        [JsonProperty("Entries")]
        private readonly Dictionary<string, FileManifestEntry> _entries = new Dictionary<string, FileManifestEntry>();

        protected readonly object InstanceLock = new object();

        protected FileManifest(string root)
        {
            Validate.ArgumentNotNull(root, "root");
            _root = root;
        }

        public string Root => _root;

        public uint EntryCount
        {
            get
            {
                lock (InstanceLock)
                    return (uint) _entries.Count;
            }
        }

        protected virtual Type EntryType => typeof(FileManifestEntry);

        public virtual void Dispose()
        {
            
        }

        public void Clear()
        {
            lock (InstanceLock)
                _entries.Clear();
        }

        public FileManifestEntry GetEntry(string name)
        {
            Validate.ArgumentNotNull(name, "name");

            lock (InstanceLock)
            {
                FileManifestEntry result;
                _entries.TryGetValue(name, out result);
                return result;
            }
        }

        public IList<FileManifestEntry> GetEntries()
        {
            lock (InstanceLock)
                return _entries.Values.ToList();
        }

        public IList<FileManifestEntry> GetEntries(string pattern, bool regex = false, bool sort = false)
        {
            Validate.ArgumentNotNull(pattern, "pattern");

            if (!regex)
            {
                if (!WildcardPattern.IsWildcardPattern(pattern))
                {
                    // Fast path
                    return CollectionUtility.ToSingleOrEmptyArray(GetEntry(pattern));
                }

                pattern = WildcardPattern.ToRegex(pattern);
            }

            var matcher = new Regex(pattern, RegexOptions.IgnoreCase);

            List<FileManifestEntry> matches;
            lock (InstanceLock)
                matches = _entries.Values.Where(s => matcher.IsMatch(s.Name)).ToList();

            if (sort && matches.Count > 1)
            {
                var comparer = new NaturalSortComparer(false);
                matches.Sort((a, b) => comparer.Compare(a.Name, b.Name));
            }

            return matches;
        }

        public bool HasEntry(string name)
        {
            Validate.ArgumentNotNull(name, "name");

            lock (InstanceLock)
                return _entries.ContainsKey(name);
        }

        public void AddEntry(string name, FileManifestEntry entry)
        {
            Validate.ArgumentNotNull(name, "name");
            Validate.ArgumentNotNull(entry, "entry");
            Validate.Argument(EntryType.IsInstanceOfType(entry), "entry must be a " + EntryType.FullName);

            lock (InstanceLock)
            {
                RemoveEntry(name);

                _entries.Add(name, entry);

                entry.Manifest = this;
                entry.Name = name;
            }

            s_log.Trace("Added entry " + name);
        }

        public FileManifestEntry RemoveEntry(string name)
        {
            Validate.ArgumentNotNull(name, "name");

            FileManifestEntry existing;
            lock (InstanceLock)
            {
                if (!_entries.TryGetValue(name, out existing))
                    return null;
                _entries.Remove(name);

                existing.Name = null;
                existing.Manifest = null;
            }

            s_log.Trace("Removed entry: " + name);
            return existing;
        }

        public void Serialize(Stream stream)
        {
            Validate.ArgumentNotNull(stream, "stream");

            using (var writer = new StreamWriter(stream))
            {
                var serializer = JsonSerializer.Create(s_serializerSettings);
                serializer.Serialize(writer, this);
            }
        }

        public static TDerived Deserialize<TDerived>(Stream stream)
            where TDerived : FileManifest
        {
            Validate.ArgumentNotNull(stream, "stream");

            using (var reader = new StreamReader(stream))
            {
                var serializer = JsonSerializer.Create(s_serializerSettings);
                return (TDerived) serializer.Deserialize(reader, typeof(TDerived));
            }
        }

        public uint Update()
        {
            lock (InstanceLock)
                return UpdateCore();
        }

        internal Stream OpenEntry(FileManifestEntry entry, FileMode mode, FileAccess access, FileShare share)
        {
            lock (InstanceLock)
                return OpenEntryCore(entry, mode, access, share);
        }

        protected abstract uint UpdateCore();


        protected abstract Stream OpenEntryCore(FileManifestEntry entry, FileMode mode, FileAccess access,
                                                FileShare share);
        
        [OnDeserialized]
        private void OnDeserialized(StreamingContext context)
        {
            foreach (KeyValuePair<string, FileManifestEntry> entry in _entries)
            {
                entry.Value.Manifest = this;
                entry.Value.Name = entry.Key;
            }
        }

        // Get the entry name that should be used in the manifest. Also checks for a conflict in the manifest.
        protected string GetEntryName(string path, bool overwrite)
        {
            string ext = Path.GetExtension(path) ?? "";
            string name = path.Substring(0, path.Length - ext.Length).Replace('\\', '/');

            if (name.Length == 0)
            {
                s_log.Error("invalid path: {0}", path);
                return null;
            }

            // Check for a collision with an existing entry
            if (!overwrite && HasEntry(name))
                return null;

            return name;
        }
    }
}