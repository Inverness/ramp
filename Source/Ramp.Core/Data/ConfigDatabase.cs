﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using NLog;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Ramp.Threading;

namespace Ramp.Data
{
    /// <summary>
    ///     A unified database for configuration information. Allows data to be loaded in "patches" which replaces,
    ///     removes, or merges Json tokens into the database.
    /// </summary>
    public class ConfigDatabase : DispatcherObject
    {
        public const char PathSeparator = '.';
        public const string MergePrefix = "++";
        public const string AddPrefix = "+";
        public const string RemovePrefix = "-";

        private static readonly Logger s_log = LogManager.GetCurrentClassLogger();

        private readonly JObject _root;
        private readonly JsonSerializer _serializer = new JsonSerializer();

        public ConfigDatabase()
        {
            _root = new JObject();
        }

        public ConfigDatabase(JObject root)
        {
            Validate.ArgumentNotNull(root, nameof(root));
            _root = root;
        }

        /// <summary>
        ///     Gets the root of the database.
        /// </summary>
        public JObject Root
        {
            get
            {
                Validate.VerifyAccess(this);
                return _root;
            }
        }

        /// <summary>
        ///     Apply a patch to the database.
        /// </summary>
        /// <param name="operation">The operation to perform.</param>
        /// <param name="patchPath"> The path to the node the patch will be applied to. </param>
        /// <param name="data"> The patch data. </param>
        public void ApplyPatch(ConfigPatchOperation operation, string patchPath, JToken data)
        {
            Validate.ArgumentNotNull(patchPath, "jsonPath");
            Validate.ArgumentNotNull(data, "data");
            Validate.VerifyAccess(this);

            ApplyPatch(_root, operation, patchPath, data);
        }

        public void ApplyPatchSet(JObject patchSet, string prefix = null)
        {
            Validate.ArgumentNotNull(patchSet, nameof(patchSet));

            foreach (KeyValuePair<string, JToken> property in patchSet)
            {
                string path = prefix != null ? prefix + property.Key : property.Key;

                ConfigPatchOperation op = ExtractOperation(ref path);

                ApplyPatch(_root, op, path, property.Value);
            }
        }

        public JObject CreatePatchSet(string jsonPath)
        {
            Validate.ArgumentNotNull(jsonPath, nameof(jsonPath));

            var patch = new JObject();

            JToken node = ResolvePath(_root, jsonPath, false);
            if (node == null)
                return patch;
            
            patch.Add(MergePrefix + jsonPath, node);

            return patch;
        }

        public JToken GetToken(string jsonPath)
        {
            Validate.ArgumentNotNull(jsonPath, "jsonPath");
            Validate.VerifyAccess(this);

            return ResolvePath(_root, jsonPath, false);
        }

        public IEnumerable<JToken> GetTokens(string jsonPath)
        {
            Validate.ArgumentNotNull(jsonPath, "jsonPath");
            Validate.VerifyAccess(this);

            JToken parent = ResolvePath(_root, jsonPath, false);

            return parent != null && parent.HasValues
                       ? parent
                       : (IEnumerable<JToken>) Array.Empty<JToken>();
        }

        public IEnumerable<JToken> GetTokens(string firstJsonPath, string secondJsonPath)
        {
            Validate.ArgumentNotNull(firstJsonPath, nameof(firstJsonPath));
            Validate.ArgumentNotNull(secondJsonPath, nameof(secondJsonPath));
            Validate.VerifyAccess(this);

            return GetTokens(firstJsonPath).Union(GetTokens(secondJsonPath));
        }

        public string GetString(string jsonPath)
        {
            return (string) GetToken(jsonPath);
        }

        public double? GetNumber(string jsonPath)
        {
            return (double?) GetToken(jsonPath);
        }

        public bool? GetBoolean(string jsonPath)
        {
            return (bool?) GetToken(jsonPath);
        }

        //public T GetValue<T>(string jsonPath, T defaultValue = default(T))
        //{
        //    Validate.ArgumentNotNull(jsonPath, "jsonPath");
        //    Validate.VerifyAccess(this);

        //    JToken token = ResolvePath(_root, jsonPath, false);
        //    if (token == null)
        //        return defaultValue;

        //    var jvalue = token as JValue;
        //    if (jvalue != null)
        //    {
        //        try
        //        {
        //            return jvalue.Value<T>();
        //        }
        //        catch (InvalidCastException)
        //        {
        //        }
        //    }

        //    return token.ToObject<T>(_serializer);
        //}

        public void SetToken(string jsonPath, JToken token)
        {
            Validate.ArgumentNotNull(jsonPath, "jsonPath");
            Validate.VerifyAccess(this);

            if (token != null)
            {
                JToken oldToken = ResolvePath(_root, jsonPath, true);
                oldToken.Replace(token);
            }
            else
            {
                JToken oldToken = ResolvePath(_root, jsonPath, false);
                oldToken?.Remove();
            }
        }

        //public void SetValue<T>(string jsonPath, T value)
        //{
        //    Validate.ArgumentNotNull(jsonPath, "jsonPath");
        //    Validate.VerifyAccess(this);
        //    JToken oldToken = ResolvePath(_root, jsonPath, true);
        //    JToken token = GetValueType(oldToken.Type, value) != null
        //                       ? new JValue(value)
        //                       : JToken.FromObject(value, _serializer);
        //    oldToken.Replace(token);
        //}

        private static void ApplyPatch(JToken target, ConfigPatchOperation operation, string path, JToken value)
        {
            switch (operation)
            {
                case ConfigPatchOperation.Add:
                    if (value.Type != JTokenType.Object && value.Type != JTokenType.Array)
                        throw new ArgumentOutOfRangeException(nameof(operation), "Expect object or array for add.");
                    break;
                case ConfigPatchOperation.Remove:
                    if (value.Type != JTokenType.Null && value.Type != JTokenType.Array)
                        throw new ArgumentOutOfRangeException(nameof(operation), "Expect null or array for remove.");
                    break;
                case ConfigPatchOperation.Merge:
                    if (value.Type != JTokenType.Object)
                        throw new ArgumentOutOfRangeException(nameof(operation), "Expect object value for merge.");
                    break;
            }

            // Detect what kind of operation this will be from a prefix in the key. This allows patches to be more
            // concise and limited to a single key-value pair in JSON.
            // 
            // + indicates a merge. This applies only to arrays and objects. Properties or items from value are added
            // to the existing container.
            //
            // - indicates a removal. The value can be null to remove the token indicated by the key. For arrays, the
            // value can be an array of values that will be removed from an array. For object the value can be an array
            // of property names to be removed from the object.
            //
            // Without either of these indicators, a replacement occurs of whatever token is selected with the new value.

            JToken parent = ResolvePath(target, path, operation != ConfigPatchOperation.Remove);

            if (parent == null)
            {
                Debug.Assert(operation == ConfigPatchOperation.Remove);
                return;
            }

            switch (operation)
            {
                case ConfigPatchOperation.Set:
                    parent.Replace(value);
                    break;
                case ConfigPatchOperation.Add:
                    if (parent.Type != JTokenType.Null && parent.Type != value.Type)
                    {
                        s_log.Warn("Branch parent is not a matching type: " + path);
                        return;
                    }

                    if (value.Type == JTokenType.Array)
                    {
                        if (parent.Type == JTokenType.Null)
                            parent = parent.ReplaceInline(new JArray());

                        var parentArray = (JArray) parent;
                        var valueArray = (JArray) value;

                        foreach (JToken item in valueArray)
                            parentArray.Add(item);
                    }
                    else
                    {
                        if (parent.Type == JTokenType.Null)
                            parent = parent.ReplaceInline(new JObject());

                        Debug.Assert(parent.Type == JTokenType.Object);

                        var parentObject = (JObject) parent;
                        var valueObject = (JObject) value;

                        foreach (KeyValuePair<string, JToken> property in valueObject)
                            parentObject[property.Key] = property.Value;
                    }
                    break;
                case ConfigPatchOperation.Remove:
                    if (value.Type == JTokenType.Null)
                    {
                        parent.Remove();
                    }
                    else
                    {
                        Debug.Assert(value.Type == JTokenType.Array);

                        switch (parent.Type)
                        {
                            case JTokenType.Array:
                                var parentArray = (JArray) parent;

                                foreach (JToken v in value)
                                    parentArray.Remove(v);

                                break;
                            case JTokenType.Object:
                                var parentObject = (JObject) parent;

                                foreach (string propertyName in value.Values<string>())
                                    parentObject.Remove(propertyName);

                                break;
                            default:
                                s_log.Error("Branch parent is not a container: " + path);
                                return;
                        }
                    }
                    break;
                case ConfigPatchOperation.Merge:
                    if (parent.Type == JTokenType.Null)
                    {
                        parent = parent.ReplaceInline(new JObject());
                    }
                    else if (parent.Type != JTokenType.Object)
                    {
                        s_log.Error("Branch parent is not a container: " + path);
                        return;
                    }
                    MergeObjects((JObject) parent, (JObject) value);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(operation), operation, null);
            }
        }

        private static void MergeObjects(JObject target, JObject source)
        {
            foreach (KeyValuePair<string, JToken> p in source)
            {
                if (p.Value.Type == JTokenType.Object)
                {
                    JToken existing = target[p.Key];

                    if (existing == null || existing.Type == JTokenType.Null)
                    {
                        target[p.Key] = p.Value;
                    }
                    else if (existing.Type == JTokenType.Object)
                    {
                        MergeObjects((JObject) existing, (JObject) p.Value);
                    }
                    else
                    {
                        throw new ArgumentException($"attempt to merge object into {p.Value.Type}");
                    }
                }
                else
                {
                    target[p.Key] = p.Value;
                }
            }
        }

        private static JToken ResolvePath(JToken root, string path, bool create)
        {
            if (path.Length == 0)
                return root;

            JToken token = root;
            int pos = 0;

            while (token != null && pos < path.Length)
            {
                // Identify the next dot, the next part, and the next position that we'll start from.
                int dotPos = path.IndexOf(PathSeparator, pos);
                string part = dotPos != -1 ? path.Substring(pos, dotPos - pos) : path.Substring(pos);
                int nextPos = dotPos != -1 ? dotPos + 1 : path.Length;

                // First see if a property matching the part name exists. If it does this takes priority.
                JToken nextToken = token[part];

                if (nextToken == null)
                {
                    // If the property doesn't exist, check to see if a property exist that matches the remainder of
                    // the path. This allows objects to contain properties with dots.
                    nextToken = token[path.Substring(pos)];

                    if (nextToken == null && create)
                    {
                        // If remainder check failed and creation is allowed, create a new node.
                        // If we're at the end of the path we'll create a null value, otherwise a new object to
                        // contain a future null value.
                        nextToken = nextPos == path.Length ? (JToken) JValue.CreateNull() : new JObject();

                        token[part] = nextToken;
                    }
                }

                pos = nextPos;
                token = nextToken;
            }

            return token;
        }

        private static JTokenType? GetValueType(JTokenType? current, object value)
        {
            if (value == null || value == DBNull.Value)
                return JTokenType.Null;
            if (value is string)
                return GetStringValueType(current);
            if (value is long || value is int || (value is short || value is sbyte) || (value is ulong || value is uint || (value is ushort || value is byte)) || (value is Enum))
                return JTokenType.Integer;
            if (value is double || value is float || value is decimal)
                return JTokenType.Float;
            if (value is DateTime || value is DateTimeOffset)
                return JTokenType.Date;
            if (value is byte[])
                return JTokenType.Bytes;
            if (value is bool)
                return JTokenType.Boolean;
            if (value is Guid)
                return JTokenType.Guid;
            if (value is Uri)
                return JTokenType.Uri;
            if (value is TimeSpan)
                return JTokenType.TimeSpan;
            return null;
        }

        private static JTokenType GetStringValueType(JTokenType? current)
        {
            if (!current.HasValue)
                return JTokenType.String;
            switch (current.Value)
            {
                case JTokenType.Comment:
                case JTokenType.String:
                case JTokenType.Raw:
                    return current.Value;
                default:
                    return JTokenType.String;
            }
        }

        private static ConfigPatchOperation ExtractOperation(ref string path)
        {
            if (path.Length != 0)
            {
                if (path.StartsWith(MergePrefix))
                {
                    path = path.Substring(MergePrefix.Length);
                    return ConfigPatchOperation.Merge;
                }

                if (path.StartsWith(AddPrefix))
                {
                    path = path.Substring(AddPrefix.Length);
                    return ConfigPatchOperation.Add;
                }

                if (path.StartsWith(RemovePrefix))
                {
                    path = path.Substring(RemovePrefix.Length);
                    return ConfigPatchOperation.Remove;
                }
            }

            return ConfigPatchOperation.Set;
        }
    }
}