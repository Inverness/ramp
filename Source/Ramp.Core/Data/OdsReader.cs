using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Xml;
using ICSharpCode.SharpZipLib.Zip;

namespace Ramp.Data
{
    /// <summary>
    ///     Reads the contents of spreadsheets in an OpenDocument Spreadsheet file and
    ///     produces a two-dimensional array of the cell values for each spreadsheet.
    /// </summary>
    public class OdsReader : IDisposable
    {
        public const int NameRowIndex = 0;
        public const int TypeRowIndex = 1;
        public const int DataRowIndex = 2;
        public const string CommentType = "comment";
        public const string NullValue = "****";
        public const string IdTypeName = "int";

        private readonly XmlReader _reader;
        private readonly bool _ownedReader;

        private readonly TypeConverterCache _converters = new TypeConverterCache();

        public OdsReader(string path)
            : this(new FileStream(path, FileMode.Open, FileAccess.Read))
        {
        }

        public OdsReader(Stream stream)
            : this(CreateXmlReader(stream))
        {
            _ownedReader = true;
        }

        public OdsReader(XmlReader reader)
        {
            _reader = reader;
        }

        public void Dispose()
        {
            if (_ownedReader)
                _reader.Dispose();
        }

        /// <summary>
        ///     Read the next table from the ODS file.
        /// </summary>
        /// <param name="name"> The name of the table read. Will be null if no tables were found or all tables were skipped due to filtering. </param>
        /// <param name="data"> The data of the table read. Will be null if no tables were found or all tables were skipped due to filtering. </param>
        /// <param name="filter"> An optional filter. If provided, only table names in the filter will be read. </param>
        /// <returns> Whether any tables were found. This will be true even if the table is skipped due to filtering. </returns>
        public bool ReadTable(out string name, out string[,] data, ISet<string> filter = null)
        {
            name = null;

            bool done = false;
            List<string[]> rows = null;
            List<string> currentRow = null;
            bool inCell = false;
            bool inAnnotation = false;
            int columnRepeat = 0;
            int? columnCount = null;
            int totalTableCount = 0;

            while (!done && _reader.Read())
            {
                switch (_reader.NodeType)
                {
                    case XmlNodeType.Element:
                        if (name == null)
                        {
                            if (_reader.Name == "table:table")
                            {
                                totalTableCount++;

                                string nextName = _reader.GetAttribute("table:name");
                                if (nextName == null)
                                    throw new InvalidDataException("no table name");

                                // Ignore tables with an underscore prefix
                                if (!nextName.StartsWith("_") && (filter == null || filter.Contains(nextName)))
                                {
                                    name = nextName;
                                    rows = new List<string[]>();
                                    currentRow = new List<string>();
                                }
                            }
                        }
                        else
                        {
                            if (_reader.Name == "table:table-row")
                            {
                            }
                            else if (_reader.Name == "table:table-cell" && !_reader.IsEmptyElement)
                            {
                                inCell = true;
                                string repeat = _reader.GetAttribute("table:number-columns-repeated");
                                if (repeat != null)
                                    columnRepeat = int.Parse(repeat);
                            }
                            else if (_reader.Name == "office:annotation")
                            {
                                inAnnotation = true;
                            }
                        }
                        break;

                    case XmlNodeType.EndElement:
                        if (name == null)
                            break;

                        if (_reader.Name == "table:table")
                        {
                            if (rows.Count == 0)
                            {
                                rows = null;
                                name = null;
                            }
                            done = true;
                        }
                        else if (_reader.Name == "table:table-row")
                        {
                            if (currentRow.Count != 0)
                            {
                                rows.Add(currentRow.ToArray());
                                if (rows.Count == 1)
                                    columnCount = rows[0].Length;
                            }
                            currentRow.Clear();
                        }
                        else if (_reader.Name == "table:table-cell")
                        {
                            inCell = false;
                            columnRepeat = 0;
                        }
                        else if (_reader.Name == "office:annotation")
                        {
                            inAnnotation = false;
                        }

                        break;

                    case XmlNodeType.Text:
                        if (!inCell || inAnnotation)
                            break;

                        if (columnRepeat < 1)
                            columnRepeat = 1;

                        for (int i = 0; i < columnRepeat; i++)
                            if (!columnCount.HasValue || currentRow.Count < columnCount)
                                currentRow.Add(_reader.Value);

                        break;
                }
            }

            if (name == null)
            {
                data = null;
            }
            else
            {
                data = new string[rows.Count, rows[0].Length];

                for (int r = 0; r < rows.Count; r++)
                    for (int c = 0; c < columnCount; c++)
                        data[r, c] = rows[r][c];
            }
            return totalTableCount != 0;
        }

        protected static Type GetType(string type)
        {
            switch (type)
            {
                case "bool":
                    return typeof(bool);
                case "byte":
                    return typeof(byte);
                case "sbyte":
                    return typeof(sbyte);
                case "char":
                    return typeof(char);
                case "decimal":
                    return typeof(decimal);
                case "double":
                    return typeof(double);
                case "float":
                    return typeof(float);
                case "int":
                    return typeof(int);
                case "uint":
                    return typeof(uint);
                case "long":
                    return typeof(long);
                case "ulong":
                    return typeof(ulong);
                case "object":
                    return typeof(object);
                case "short":
                    return typeof(short);
                case "ushort":
                    return typeof(ushort);
                case "string":
                    return typeof(string);
                default:
                    return Type.GetType(type, true);
            }
            //case "comment":
            //    throw new ArgumentOutOfRangeException("type", "tried to get comment type");
            //case "bool":
            //    return typeof(Boolean);
            //case "int":
            //    return typeof(Int32);
            //case "long":
            //    return typeof(Int64);
            //case "float":
            //    return typeof(Single);
            //case "double":
            //    return typeof(Double);
            //case "string":
            //    return typeof(String);
            //default:
            //    throw new ArgumentOutOfRangeException("type", "unhandled type");
        }

        protected object ParseValue(Type type, string value)
        {
            return value == NullValue ? null : _converters.GetConverter(type).ConvertFromString(value);
            //if (type == typeof(Boolean))
            //{
            //    // We can't use default parser because of how the spreadsheet likes to use
            //    // all caps
            //    if (value.Equals("TRUE", StringComparison.CurrentCultureIgnoreCase) ||
            //        value.Equals("T", StringComparison.CurrentCultureIgnoreCase) ||
            //        value == "1")
            //        return true;
            //    if (value.Equals("FALSE", StringComparison.CurrentCultureIgnoreCase) ||
            //        value.Equals("F", StringComparison.CurrentCultureIgnoreCase) ||
            //        value == "0")
            //        return false;
            //    throw new FormatException("boolean value is invalid");
            //}
            //if (type == typeof(Int32))
            //    return Int32.Parse(value);
            //if (type == typeof(Int64))
            //    return Int64.Parse(value);
            //if (type == typeof(Single))
            //    return Single.Parse(value);
            //if (type == typeof(Double))
            //    return Double.Parse(value);
            //if (type == typeof(string))
            //    return value;
        }

        protected static XmlReader CreateXmlReader(Stream stream)
        {
            var zipFile = new ZipFile(stream);

            int id = zipFile.FindEntry("content.xml", false);
            if (id == -1)
                throw new InvalidDataException("invalid ODS file; no content.xml");

            Stream contentStream = zipFile.GetInputStream(zipFile[id]);
            return new XmlTextReader(contentStream);
        }

        private class TypeConverterCache
        {
            private readonly Dictionary<Type, TypeConverter> _converters = new Dictionary<Type, TypeConverter>();

            public void Clear()
            {
                _converters.Clear();
            }

            public TypeConverter GetConverter(Type type)
            {
                TypeConverter result;

                if (!_converters.TryGetValue(type, out result))
                {
                    result = TypeDescriptor.GetConverter(type);
                    _converters.Add(type, result);
                }

                return result;
            }
        }
    }
}