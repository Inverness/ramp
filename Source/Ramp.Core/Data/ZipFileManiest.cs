using System;
using System.IO;
using ICSharpCode.SharpZipLib.Zip;
using NLog;

namespace Ramp.Data
{
    public class ZipFileManiest : FileManifest
    {
        private static readonly Logger s_log = LogManager.GetCurrentClassLogger();

        private ZipFile _zipFile;

        public ZipFileManiest(string root)
            : base(root)
        {
            _zipFile = new ZipFile(new FileStream(root, FileMode.Open, FileAccess.Read, FileShare.Read));
        }

        public override void Dispose()
        {
            if (_zipFile != null)
            {
                _zipFile.Close();
                _zipFile = null;
            }

            base.Dispose();
        }

        protected override Type EntryType => typeof(ZipFileManifestEntry);

        protected override Stream OpenEntryCore(
            FileManifestEntry entry,
            FileMode mode,
            FileAccess access,
            FileShare share)
        {
            if (mode != FileMode.Open || access != FileAccess.Read)
                throw new ArgumentException("Packaged file is read-only");

            var ze = (ZipFileManifestEntry) entry;
            int index = ze.ZipEntryIndex;

            // The zip file entry index is cached because FindEntry() does a sequential name string compare for every
            // single entry to find a match. This might be unreasonably slow for large files.
            if (index != -1)
                return _zipFile.GetInputStream(index);

            ze.ZipEntryIndex = index = _zipFile.FindEntry(entry.Path, true);

            if (index == -1)
                throw new InvalidOperationException("Invalid zip file manifest entry. Unable to find matching file.");

            return _zipFile.GetInputStream(index);
        }

        protected override uint UpdateCore()
        {
            uint count = 0;

            s_log.Debug("Updating from " + Root);
            foreach (ZipEntry zipEntry in _zipFile)
            {
                if (zipEntry.IsDirectory)
                    continue;
                string entryName = GetEntryName(zipEntry.Name, true);
                if (entryName == null)
                    continue;

                AddEntry(entryName, new ZipFileManifestEntry(Root, zipEntry));

                count++;
            }
            s_log.Debug("Finished update");

            return count;
        }
    }
}