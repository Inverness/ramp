using System;
using System.IO;
using Newtonsoft.Json;

namespace Ramp.Data
{
    /// <summary>
    ///     A file manifest entry.
    /// </summary>
    [JsonObject(MemberSerialization.OptOut)]
    public abstract class FileManifestEntry
    {
        private FileManifest _manifest;

        /// <summary>
        ///     Gets the manifest that this entry is a part of.
        /// </summary>
        [JsonIgnore]
        public FileManifest Manifest
        {
            get { return _manifest; }

            set
            {
                _manifest = value;
                OnManifestChanged();
            }
        }

        /// <summary>
        ///     Gets the name of the entry in its manifest.
        /// </summary>
        [JsonIgnore]
        public string Name { get; internal set; }

        /// <summary>
        ///		Gets the name of the root containing this entry.
        /// </summary>
        [JsonProperty]
        public string Root { get; protected set; }

        /// <summary>
        ///     Gets the path to this file within the root.
        /// </summary>
        [JsonProperty]
        public string Path { get; protected set; }

        /// <summary>
        ///     Gets the time the file was modified.
        /// </summary>
        [JsonProperty]
        public DateTime Modified { get; protected set; }

        /// <summary>
        ///     Gets the file size in bytes.
        /// </summary>
        [JsonProperty]
        public long Size { get; protected set; }

        /// <summary>
        ///     Gets the SHA2 hash of the file. Can be null if hashing is not being used.
        /// </summary>
        [JsonProperty]
        public byte[] Sha2 { get; protected set; }

        /// <summary>
        ///		Gets the absolute path of the file.
        /// </summary>
        [JsonIgnore]
        public virtual string FullPath => System.IO.Path.Combine(System.IO.Path.GetFullPath(Root), Path);

        /// <summary>
        ///		Gets whether the file is read-only.
        /// </summary>
        [JsonIgnore]
        public abstract bool IsReadOnly { get; }

        /// <summary>
        ///     Create a file stream with read only access.
        /// </summary>
        /// <returns> A new read-only FileStream objecct. </returns>
        /// <exception cref="InvalidOperationException">Path is null.</exception>
        public Stream Open()
        {
            return Open(FileMode.Open, FileAccess.Read, FileShare.Read);
        }

        /// <summary>
        ///     Create a file stream with the specified mode, access, and sharing.
        /// </summary>
        /// <returns> A new FileStream object. </returns>
        /// <exception cref="InvalidOperationException">Path is null.</exception>
        public virtual Stream Open(FileMode mode, FileAccess access, FileShare share)
        {
            if (Manifest == null)
                throw new InvalidOperationException("not in a manifest");
            return Manifest.OpenEntry(this, mode, access, share);
        }

        public StreamReader OpenText()
        {
            return new StreamReader(Open(FileMode.Open, FileAccess.Read, FileShare.Read));
        }

        public StreamReader OpenText(FileMode mode, FileAccess access, FileShare share)
        {
            return new StreamReader(Open(mode, access, share));
        }

        protected virtual void OnManifestChanged()
        {
        }
    }
}