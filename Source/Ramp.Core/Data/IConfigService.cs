using System;

namespace Ramp.Data
{
    /// <summary>
    ///     Provides access to the basic configuration. This service's getters are thread-safe.
    /// </summary>
    public interface IConfigService
    {
        /// <summary>
        ///     Gets the configuration database.
        /// </summary>
        ConfigDatabase Database { get; }

        /// <summary>
        ///     Gets the read-only root directory of the engine files.
        /// </summary>
        string RootDirectory { get; }

        /// <summary>
        ///     Gets a unique GUID identifying the current system.
        /// </summary>
        Guid SystemGuid { get; }

        /// <summary>
        ///     Gets the writable directory containing user files.
        /// </summary>
        string UserDirectory { get; }
    }
}