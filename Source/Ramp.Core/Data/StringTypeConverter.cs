﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Text;

namespace Ramp.Data
{
    public abstract class StringTypeConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string))
                return true;
            return base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            if (value == null)
                return ConvertFromStringCore(context, culture, null);

            var stringValue = value as string;
            if (stringValue != null)
                return ConvertFromStringCore(context, culture, stringValue);

            return base.ConvertFrom(context, culture, value);
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value,
                                         Type destinationType)
        {
            if (destinationType == typeof(string))
                return ConvertToStringCore(context, culture, value);

            return base.ConvertTo(context, culture, value, destinationType);
        }

        protected abstract object ConvertFromStringCore(ITypeDescriptorContext context, CultureInfo culture,
                                                        string value);

        protected virtual string ConvertToStringCore(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            if (value == null)
                return string.Empty;

            if (culture != null && culture.Equals(CultureInfo.CurrentCulture))
            {
                IFormattable formattable = value as IFormattable;
                if (formattable != null)
                {
                    return formattable.ToString(null, culture);
                }
            }

            return value.ToString();
        }

        protected static T[] ConvertValuesFromString<T>(
            TypeConverter valueConverter,
            ITypeDescriptorContext context,
            CultureInfo culture,
            string value,
            uint? expected = null)
        {
            if (string.IsNullOrEmpty(value))
                return null;

            if (culture == null)
                culture = CultureInfo.CurrentCulture;

            string[] parts = value.Split(new[] { culture.TextInfo.ListSeparator }, StringSplitOptions.RemoveEmptyEntries);

            if (expected.HasValue && parts.Length != expected.Value)
                throw new FormatException("Unable to convert values from string'" + value + "', expected " +
                                          expected.Value + " values.");

            var results = new T[parts.Length];
            for (int i = 0; i < parts.Length; i++)
                results[i] = (T) valueConverter.ConvertFromString(context, culture, parts[i].Trim());

            return results;
        }

        protected static string ConvertValuesToString<T>(
            TypeConverter valueConverter,
            ITypeDescriptorContext context,
            CultureInfo culture,
            T[] values)
        {
            if (values == null)
                return null;

            if (culture == null)
                culture = CultureInfo.CurrentCulture;

            StringBuilder sb = StringBuilderCache.Acquire(values.Length * 5);

            for (int i = 0; i < values.Length - 1; i++)
            {
                sb.Append(valueConverter.ConvertToString(context, culture, values[i]))
                  .Append(culture.TextInfo.ListSeparator)
                  .Append(' ');
            }

            if (values.Length != 0)
                sb.Append(valueConverter.ConvertToString(context, culture, values[values.Length - 1]));

            return StringBuilderCache.ReleaseAndGetString(sb);
        }
    }
}