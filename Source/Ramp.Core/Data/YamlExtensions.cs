﻿using YamlDotNet.RepresentationModel;
using YamlDotNet.Serialization;

namespace Ramp.Data
{
    public static class YamlExtensions
    {
        public static T ToValue<T>(this YamlNode node, Deserializer serializer = null)
        {
            Validate.ArgumentNotNull(node, nameof(node));

            var scalar = node as YamlScalarNode;
            if (scalar != null)
                return GenericTypeConverter.Convert<T>(scalar.Value);

            // This is a pretty ridiculous thing to do but is unfortunately necessary since there is no direct
            // conversion yet.
            string svalue = node.ToString();
            if (serializer == null)
                serializer = new Deserializer();
            return serializer.Deserialize<T>(svalue);
        }
    }
}
