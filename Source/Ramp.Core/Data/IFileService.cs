using System.Collections.Generic;

namespace Ramp.Data
{
    /// <summary>
    ///     Provides file related methods and a map of file names to their paths in order to
    ///     ease file management.
    /// </summary>
    public interface IFileService
    {
        /// <summary>
        ///     Gets the file manifests for each file root.
        /// </summary>
        IReadOnlyList<FileManifest> Manifests { get; }

        bool HasEntry(string name);

        FileManifestEntry GetEntry(string name);

        IList<FileManifestEntry> GetEntries(string pattern, bool regex = false, bool sort = false);
    }
}