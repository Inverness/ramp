﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using JetBrains.Annotations;
using Ramp.Threading;

namespace Ramp
{
    /// <summary>
    ///     Contains methods for validating arguments and state for debug builds.
    /// </summary>
    [DebuggerStepThrough]
    public static class Validate
    {
        [Conditional("DEBUG"), Conditional("VALIDATE")]
        [AssertionMethod]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Assert<T>(
            [AssertionCondition(AssertionConditionType.IS_TRUE)] bool expression,
            string message = null
            )
            where T : Exception
        {
            if (!expression)
                ThrowGeneric<T>(message);
        }

        [Conditional("DEBUG"), Conditional("VALIDATE")]
        [AssertionMethod]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Argument(
            [AssertionCondition(AssertionConditionType.IS_TRUE)] bool expression,
            string message = null,
            string paramName = null
            )
        {
            if (!expression)
                ThrowArgumentException(message, paramName);
        }

        [Conditional("DEBUG"), Conditional("VALIDATE")]
        [AssertionMethod]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void ArgumentNotNull(
            [AssertionCondition(AssertionConditionType.IS_NOT_NULL), NoEnumeration] object value,
            string paramName
            )
        {
            if (value == null)
                ThrowArgumentNullException(paramName);
        }

        [Conditional("DEBUG"), Conditional("VALIDATE")]
        [AssertionMethod]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void ArgumentNotNull(
            [AssertionCondition(AssertionConditionType.IS_NOT_NULL), NoEnumeration] object value,
            string paramName,
            string message
            )
        {
            if (value == null)
                ThrowArgumentNullException(paramName, message);
        }

        [Conditional("DEBUG"), Conditional("VALIDATE")]
        [AssertionMethod]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void ArgumentInRange(
            [AssertionCondition(AssertionConditionType.IS_TRUE)] bool expression,
            string paramName
            )
        {
            if (!expression)
                ThrowArgumentOutOfRange(paramName);
        }

        [Conditional("DEBUG"), Conditional("VALIDATE")]
        [AssertionMethod]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void ArgumentInRange(
            [AssertionCondition(AssertionConditionType.IS_TRUE)] bool expression,
            string paramName,
            string message
            )
        {
            if (!expression)
                ThrowArgumentOutOfRange(paramName, message);
        }

        [Conditional("DEBUG"), Conditional("VALIDATE")]
        [AssertionMethod]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void ValidOperation(
            [AssertionCondition(AssertionConditionType.IS_TRUE)] bool expression,
            string message = null
            )
        {
            if (!expression)
                ThrowInvalidOperation(message);
        }

        [Conditional("DEBUG"), Conditional("VALIDATE")]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void VerifyAccess(DispatcherObject obj)
        {
            obj.VerifyAccess();
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        private static void ThrowArgumentException(string message, string paramName)
        {
            throw new ArgumentException(message, paramName);
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        private static void ThrowArgumentNullException(string paramName)
        {
            throw new ArgumentNullException(paramName);
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        private static void ThrowArgumentNullException(string paramName, string message)
        {
            throw new ArgumentNullException(paramName, message);
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        private static void ThrowArgumentOutOfRange(string paramName)
        {
            throw new ArgumentOutOfRangeException(paramName);
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        private static void ThrowArgumentOutOfRange(string paramName, string message)
        {
            throw new ArgumentOutOfRangeException(paramName, message);
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        private static void ThrowInvalidOperation(string message)
        {
            throw new InvalidOperationException(message);
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        private static void ThrowGeneric<T>(string message)
            where T : Exception
        {
            throw (T) Activator.CreateInstance(typeof(T), message);
        }
    }
}