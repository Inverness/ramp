﻿using System;
using System.Text;

namespace Ramp
{
    public static class StringExtensions
    {
        public static bool EqualsIgnoreCase(this string self, string other)
        {
            return self.Equals(other, StringComparison.OrdinalIgnoreCase);
        }

        public static StringBuilder ToString(this StringBuilder self, out string s)
        {
            s = self.ToString();
            return self;
        }

        public static string ToStringAndClear(this StringBuilder self)
        {
            string result = self.ToString();
            self.Clear();
            return result;
        }
    }
}