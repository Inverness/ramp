﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Newtonsoft.Json;

namespace Ramp.Threading
{
    /// <summary>
    ///     The base class for objects bound to a dispatcher.
    /// </summary>
    public abstract class DispatcherObject
    {
        private Dispatcher _dispatcher;

        protected DispatcherObject()
        {
            _dispatcher = Dispatcher.Current;
            Debug.Assert(_dispatcher != null);
        }

        /// <summary>
        ///     Gets or sets the dispatcher that owns this object. The dispatcher can only be set once.
        /// </summary>
        [JsonIgnore]
        public Dispatcher Dispatcher => _dispatcher;

        /// <summary>
        ///     Check if the current thread has access to this object.
        /// </summary>
        /// <returns> True if the current thread has access to this object. </returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool CheckAccess() => _dispatcher?.CheckAccess() ?? true;

        /// <summary>
        ///     Verifies that the current thread has access to this object.
        /// </summary>
        /// <exception cref="InvalidOperationException"> The current thread does not have access to this object. </exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void VerifyAccess() => _dispatcher?.VerifyAccess();

        /// <summary>
        ///     Detaches the current object from the dispatcher, clearing the reference to it and allowing the
        ///     current object to be accessed from any thread.
        /// </summary>
        protected void DetachFromDispatcher()
        {
            _dispatcher = null;
        }
    }
}