﻿using System;
using System.Threading;

namespace Ramp.Threading
{
    /// <summary>
    ///     A synchronization context that posts to a Dispatcher. This context maintains a stack to allow a nested
    ///     hierarchy of dispatchers.
    /// </summary>
    public sealed class DispatcherSynchronizationContext : SynchronizationContext
    {
        public DispatcherSynchronizationContext(Dispatcher dispatcher)
        {
            Validate.ArgumentNotNull(dispatcher, "dispatcher");
            Dispatcher = dispatcher;
        }

        /// <summary>
        ///     Gets the dispatcher this context is bound to.
        /// </summary>
        public Dispatcher Dispatcher { get; }

        public override void Send(SendOrPostCallback d, object state)
        {
            Validate.ArgumentNotNull(d, "d");

            if (Dispatcher.CheckAccess())
                d(state);
            else
                Dispatcher.InvokeAsync(() => d(state)).Wait();
        }

        public override void Post(SendOrPostCallback d, object state)
        {
            Validate.ArgumentNotNull(d, "d");

            // TODO: Force async?
            Dispatcher.InternalInvoke(d, state, TimeSpan.Zero, CancellationToken.None, false);
        }
    }
}