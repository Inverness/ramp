﻿using System;

namespace Ramp.Threading
{
    /// <summary>
    ///     Provides an awaiter for sleeping and yielding.
    /// </summary>
    public struct DispatcherSleepAwaitable
    {
        private readonly Dispatcher _dispatcher;
        private readonly TimeSpan _endTime;
        private readonly bool _preserveContext;

        internal DispatcherSleepAwaitable(Dispatcher dispatcher, TimeSpan endTime, bool preserveContext)
        {
            _dispatcher = dispatcher;
            _endTime = endTime;
            _preserveContext = preserveContext;
        }

        public DispatcherSleepAwaiter GetAwaiter()
        {
            return new DispatcherSleepAwaiter(_dispatcher, _endTime, _preserveContext);
        }
    }
}