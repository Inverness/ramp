﻿using System;
using System.Diagnostics;
using System.Threading;
using NLog;

namespace Ramp.Threading
{
    /// <summary>
    ///     Manages a dispatcher and thread pair.
    /// </summary>
    public sealed class DispatcherThread : IDisposable
    {
        private static readonly Logger s_log = LogManager.GetCurrentClassLogger();

        private bool _disposing;
        private readonly object _instanceLock = new object();
        private bool _queued;

        /// <summary>
        ///     Initialize a new DispatcherThread and start the thread.
        /// </summary>
        /// <param name="name"> The name of the new thread. </param>
        public DispatcherThread(string name)
        {
            Thread = new Thread(Run)
            {
                IsBackground = true,
                Name = name
            };

            Dispatcher = new Dispatcher(Thread);
            Dispatcher.WorkItemQueued += OnWorkItemQueued;

            Thread.Start();
        }

        /// <summary>
        ///     Gets the thread created for the dispatcher.
        /// </summary>
        public Thread Thread { get; }

        /// <summary>
        ///     Gets the dispatcher being used for the thread.
        /// </summary>
        public Dispatcher Dispatcher { get; }

        public void Dispose()
        {
            lock (_instanceLock)
            {
                if (!_disposing)
                {
                    _disposing = true;
                    Dispatcher.Invoke(() => Dispatcher.Dispose());
                }
            }

        }

        private void Run()
        {
            Dispatcher.Current = Dispatcher;

            s_log.Info("Starting dispatcher thread");
            
            while (!Dispatcher.IsDisposed)
            {
                try
                {
                    Dispatcher.Run(DispatcherRunMode.All);
                }
                catch (AggregateException ex)
                {
                    s_log.Error("Errors when running queue:");
                    foreach (Exception iex in ex.InnerExceptions)
                        s_log.Error(iex);
                }
                catch (Exception ex)
                {
                    s_log.Error(ex, "Error when running queue:");
                }

                if (Dispatcher.IsDisposed)
                    break;

                lock (_instanceLock)
                {
                    if (!_queued)
                        Monitor.Wait(_instanceLock);
                    _queued = false;
                }
            }

            s_log.Info("Shutting down dispatcher thread");
        }

        private void OnWorkItemQueued()
        {
            lock (_instanceLock)
            {
                _queued = true;
                Monitor.PulseAll(_instanceLock);
            }
        }
    }
}