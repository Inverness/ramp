﻿using System;
using System.Runtime.CompilerServices;
using System.Threading;

namespace Ramp.Threading
{
    /// <summary>
    ///     An awaiter that signs up the completion continuation to be invoked after configured amount of time.
    /// </summary>
    public struct DispatcherSleepAwaiter : INotifyCompletion
    {
        private readonly Dispatcher _dispatcher;
        private readonly TimeSpan _endTime; // Yield mode is signified by an endTime of MinValue.
        private readonly bool _preserveContext;

        private static readonly SendOrPostCallback s_runActionInContextCallback = RunActionInContext;
        private static readonly SendOrPostCallback s_runActionCallback = RunAction;

        internal DispatcherSleepAwaiter(Dispatcher dispatcher, TimeSpan endTime, bool preserveContext)
        {
            _dispatcher = dispatcher;
            _endTime = endTime;
            _preserveContext = preserveContext;
        }

        public bool IsCompleted => _dispatcher.Time >= _endTime;

        public void GetResult()
        {
        }

        public void OnCompleted(Action continuation)
        {
            if (continuation == null)
                throw new ArgumentNullException(nameof(continuation));
            if (_dispatcher == null)
                throw new InvalidOperationException("Dispatcher sleep awaiter invalid");

            TimeSpan endTime = _endTime;

            // Add 1 tick to the current time to ensure that the yield is not run immediately if the queue is currently
            // running its work items.
            if (endTime == TimeSpan.MinValue)
                endTime = _dispatcher.Time + TimeSpan.FromTicks(1);

            SynchronizationContext context;
            if (_preserveContext && (context = SynchronizationContext.Current) != null)
            {
                _dispatcher.InternalInvoke(s_runActionInContextCallback,
                                           Tuple.Create(context, continuation),
                                           endTime,
                                           CancellationToken.None,
                                           false);
            }
            else
            {
                _dispatcher.InternalInvoke(s_runActionCallback, continuation, endTime, CancellationToken.None, false);
            }
        }

        private static void RunAction(object state)
        {
            ((Action) state)();
        }

        private static void RunActionInContext(object state)
        {
            var tuple = (Tuple<SynchronizationContext, Action>) state;
            tuple.Item1.Post(RunAction, tuple.Item2);
        }
    }
}