namespace Ramp.Threading
{
    /// <summary>
    /// Describes the behavior of a dispatcher run.
    /// </summary>
    public enum DispatcherRunMode
    {
        /// <summary>
        /// Runs all currently queued tasks and ignores any that were queued during the run even if they're eligible
        /// to run.
        /// </summary>
        Current,

        /// <summary>
        /// Runs all tasks as long as tasks are eligible to run.
        /// </summary>
        All
    }
}