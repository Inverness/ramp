﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Ramp.Threading
{
    public sealed class DispatcherTaskScheduler : TaskScheduler
    {
        private readonly Dispatcher _dispatcher;
        private readonly SendOrPostCallback _callback;

        internal DispatcherTaskScheduler(Dispatcher dispatcher)
        {
            _dispatcher = dispatcher;
            _callback = InvokeTaskCallback;
        }

        public override int MaximumConcurrencyLevel => 1;

        protected override void QueueTask(Task task)
        {
            _dispatcher.InternalInvoke(_callback, task, TimeSpan.Zero, CancellationToken.None, true);
        }

        protected override bool TryExecuteTaskInline(Task task, bool taskWasPreviouslyQueued)
        {
            return Dispatcher.Current == _dispatcher && TryExecuteTask(task);
        }

        protected override IEnumerable<Task> GetScheduledTasks()
        {
            return null;
        }

        private void InvokeTaskCallback(object task)
        {
            TryExecuteTask((Task) task);
        }
    }
}