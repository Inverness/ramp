﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using NLog;
using Ramp.Utilities.Collections;

namespace Ramp.Threading
{
    /// <summary>
    ///     Manages a queue of work items. Only one may exist per thread. This is a much more simplified implementation
    ///     of WPF's Dispatcher model.
    /// </summary>
    public sealed class Dispatcher : IDisposable
    {
        private static readonly Logger s_log = LogManager.GetCurrentClassLogger();

        private static readonly Task s_completedTask = Task.FromResult(true);

        private static readonly SendOrPostCallback s_runActionCallback = a => ((Action) a)();

        [ThreadStatic]
        private static Dispatcher t_current;

        private readonly object _instanceLock = new object();
        private readonly Thread _thread;
        private readonly Stopwatch _stopwatch = new Stopwatch();
        private readonly BinaryHeap<WorkItem> _heap = new BinaryHeap<WorkItem>(new WorkItemComparer());
        private readonly Queue<WorkItem> _runQueue = new Queue<WorkItem>();
        private readonly DispatcherTaskScheduler _taskScheduler;
        private volatile int _count;
        private volatile bool _isDisposed;

        internal Dispatcher(Thread thread)
        {
            _thread = thread;
            _taskScheduler = new DispatcherTaskScheduler(this);
            _stopwatch.Start();
        }

        // Allows DispatcherThread to wake itself
        internal event Action WorkItemQueued;

        /// <summary>
        ///     Gets the number of queued work items. This property is volatile and can be accessed from any thread.
        /// </summary>
        public int Count => _count;

        /// <summary>
        ///     Gets the thread that this dispatcher is bound to. Tasks may only be run on this thread. This property
        ///     may be accessed from any thread.
        /// </summary>
        public Thread Thread => _thread;

        /// <summary>
        ///     Gets the local time of this dispatcher. This property may be accessed from any thread.
        /// </summary>
        public TimeSpan Time => _stopwatch.Elapsed;

        /// <summary>
        ///     Gets a task scheduler that executes tasks using this dispatcher. This property may be accessed from any
        ///     thread.
        /// </summary>
        public DispatcherTaskScheduler TaskScheduler => _taskScheduler;

        /// <summary>
        ///     Gets a value indicating whether this object has been disposed.
        /// </summary>
        public bool IsDisposed => _isDisposed;

        /// <summary>
        ///     Gets the current dispatcher for the thread. If there is no current dispatcher, getting it
        ///     will create a new instance and assign it as the thread's dispatcher.
        /// </summary>
        public static Dispatcher Current
        {
            get => t_current ?? GetCurrentSlow();

            internal set => SetCurrent(value);
        }

        /// <summary>
        ///     Check that the current thread has access to this dispatcher. This method may be called from any thread.
        /// </summary>
        /// <returns> True if the current thread has access. </returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool CheckAccess()
        {
            return _thread == Thread.CurrentThread;
        }

        /// <summary>
        ///     Verify that the current thread has access to this dispatcher. This method may be called from any thread.
        /// </summary>
        /// <exception cref="InvalidOperationException"> The current thread does not have access. </exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void VerifyAccess()
        {
            if (_thread != Thread.CurrentThread)
                ThrowAccessVerificationFailed();
        }

        /// <summary>
        ///     Invoke an action on the dispatcher thread. This method may be called from any thread.
        /// </summary>
        /// <param name="action"> An action. </param>
        /// <param name="ct"> A cancellation token. </param>
        /// <param name="forceAsync">
        ///     Forces the action to be asynchronously run even if the current thread has access to this dispatcher.
        /// </param>
        public void Invoke(Action action, CancellationToken ct = default(CancellationToken), bool forceAsync = false)
        {
            Validate.ArgumentNotNull(action, "action");
            VerifyNotDisposed();
            
            // Passing the action as the state avoids any further allocations
            InternalInvoke(s_runActionCallback, action, TimeSpan.Zero, ct, forceAsync);
        }

        /// <summary>
        ///     Invoke an action on the dispatcher thread. This method may be called from any thread.
        /// </summary>
        /// <param name="action"> An action. </param>
        /// <param name="ct"> A cancellation token. </param>
        /// <param name="forceAsync">
        ///     Forces the action to be asynchronously run even if the current thread has access to this dispatcher.
        /// </param>
        /// <returns> A new task. </returns>
        public Task InvokeAsync(
            Action action,
            CancellationToken ct = default(CancellationToken),
            bool forceAsync = false)
        {
            Validate.ArgumentNotNull(action, "action");
            VerifyNotDisposed();

            if (!forceAsync && CheckAccess() && !ct.IsCancellationRequested)
            {
                InvokeCallback(action, ct);
                return s_completedTask;
            }

            // The new task will be in a cancelled state if the cancellation token has requested it
            return Task.Factory.StartNew(action, ct, TaskCreationOptions.None, _taskScheduler);
        }

        /// <summary>
        ///     Invoke a function on the dispatcher thread. This method may be called from any thread.
        /// </summary>
        /// <typeparam name="T"> The result type. </typeparam>
        /// <param name="func"> A function. </param>
        /// <param name="ct"> A cancellation token. </param>
        /// <param name="forceAsync">
        ///     Forces the action to be asynchronously run even if the current thread has access to this dispatcher.
        /// </param>
        /// <returns> A new task. </returns>
        public Task<T> InvokeAsync<T>(
            Func<T> func,
            CancellationToken ct = default(CancellationToken),
            bool forceAsync = false)
        {
            Validate.ArgumentNotNull(func, "func");
            VerifyNotDisposed();

            if (!forceAsync && CheckAccess() && !ct.IsCancellationRequested)
            {
                return Task.FromResult(InvokeCallback(func, ct));
            }

            return Task<T>.Factory.StartNew(func, ct, TaskCreationOptions.None, _taskScheduler);
        }

        /// <summary>
        ///     Get an awaitable object that completes the next time the queue is run.
        ///     This method may be called from any thread.
        /// </summary>
        /// <returns> An awaitable object. </returns>
        public DispatcherSleepAwaitable Yield(bool preserveContext = false)
        {
            VerifyNotDisposed();
            return new DispatcherSleepAwaitable(this, TimeSpan.MinValue, preserveContext);
        }

        /// <summary>
        ///     Gets an awaitable object that completes once the specified number of seconds has passed.
        ///     This method may be called from any thread.
        /// </summary>
        /// <param name="seconds"> The number of seconds to sleep for. </param>
        /// <param name="preserveContext"> Whether to preserve the synchronization context of the awaiter. </param>
        /// <returns> An awaitable object. </returns>
        /// <remarks>
        ///     The sleep time is relative to when this method was called, not when the DispatcherSleepAwaitable
        ///     was actually awaited.
        /// </remarks>
        public DispatcherSleepAwaitable Sleep(double seconds, bool preserveContext = false)
        {
            return Sleep(TimeSpan.FromSeconds(seconds), preserveContext);
        }

        /// <summary>
        ///     Gets an awaitable object that completes once the specified time span has passed.
        ///     This method may be called from any thread.
        /// </summary>
        /// <param name="time"> The time span to sleep for. </param>
        /// <param name="preserveContext"> Whether to preserve the synchronization context of the awaiter. </param>
        /// <returns> An awaitable object. </returns>
        /// <remarks>
        ///     The sleep time is relative to when this method was called, not when the DispatcherSleepAwaitable
        ///     was actually awaited.
        /// </remarks>
        public DispatcherSleepAwaitable Sleep(TimeSpan time, bool preserveContext = false)
        {
            Validate.ArgumentInRange(time >= TimeSpan.Zero, "time", "time must not be negative");
            VerifyNotDisposed();

            // Ensure the sleep time is never zero so that it is not run immediately in any case.
            if (time == TimeSpan.Zero)
                time = TimeSpan.FromTicks(1);

            return new DispatcherSleepAwaitable(this, Time + time, preserveContext);
        }

        /// <summary>
        /// Run queued work items. This method may only be called from the bound thread.
        /// </summary>
        /// <param name="runMode"> An enum describing the run behavior. </param>
        public void Run(DispatcherRunMode runMode)
        {
            // This is optimized for inlining and repeated calls by doing a quick count check first before
            // going down the slow path.
            if (_count != 0)
                RunSlow(runMode);
        }
        
        /// <summary>
        /// Waits for the execution of a task to finish while running queued work items for the current dispatcher.
        /// </summary>
        /// <param name="task"> A task that has been scheduled. </param>
        /// <param name="ct"> A cancellation token. </param>
        public void Wait(Task task, CancellationToken ct = default(CancellationToken))
        {
            Validate.ArgumentNotNull(task, nameof(task));

            if (task.Status == TaskStatus.Created)
                throw new ArgumentException("task has not been scheduled", nameof(task));

            do
            {
                if (ct.IsCancellationRequested)
                    return;

                Run(DispatcherRunMode.All);

            } while (!task.Wait(1, ct));
        }

        public void Dispose()
        {
            VerifyAccess();

            if (!_isDisposed)
            {
                lock (_instanceLock)
                {
                    _isDisposed = true;
                    _heap.Clear();
                    _count = 0;
                }
            }
        }

        // This uses SendOrPostCallback because invokes from the DispatcherSynchronizationContext are by far the most
        // common.
        internal void InternalInvoke(
            SendOrPostCallback callback,
            object state,
            TimeSpan runTime,
            CancellationToken ct,
            bool forceAsync)
        {
            Debug.Assert(runTime >= TimeSpan.Zero);

            if (ct.IsCancellationRequested)
                return;

            // If access allows, run the action synchronously and don't bother queueing.
            if (!forceAsync && CheckAccess() && Time >= runTime)
            {
                InvokeCallback(callback, state, ct);
                return;
            }

            lock (_instanceLock)
            {
                // Dispose could have happened after previous check or while waiting for the lock
                VerifyNotDisposed();

                _heap.Push(new WorkItem(callback, state, runTime, ct));
                _count++;

                WorkItemQueued?.Invoke();
            }
        }

        private void RunSlow(DispatcherRunMode runMode)
        {
            VerifyAccess();
            VerifyNotDisposed();

            // If the runMode is All, the queue is run repeatedly until no more runnable work items remain.
            // This is an option because work items may themselves add new work items to the queue. For invokes
            // The run time is set to zero ensuring that they're always runnable. Yields and sleeps will be delayed
            // until the next call to RunQueued() since their time will always be greater than the current.
            TimeSpan time = Time;
            int runCount;
            do
            {
                runCount = RunCore(time);
            } while (runCount != 0 && runMode == DispatcherRunMode.All && !_isDisposed);
        }

        // Runs all items in the queue that are currently runnable. Returns the number of items ran.
        private int RunCore(TimeSpan currentTime)
        {
            // First lock the heap and add all runnable items to the run queue
            // The run queue will only ever be accessed by this method, which is thread bound and thus safe to change.
            lock (_instanceLock)
            {
                Debug.Assert(!_isDisposed);

                while (_heap.Count != 0)
                {
                    WorkItem top = _heap.Peek();
                    if (currentTime < top.RunTime)
                        break;

                    _heap.Pop();
                    _runQueue.Enqueue(top);
                    _count--;
                }
            }

            int successCount = 0;

            try
            {
                while (_runQueue.Count != 0)
                {
                    WorkItem workItem = _runQueue.Dequeue();

                    if (!workItem.CancellationToken.IsCancellationRequested)
                    {
                        InvokeCallback(workItem.Callback, workItem.State, workItem.CancellationToken);
                        successCount++;
                    }

                    if (_isDisposed)
                    {
                        _runQueue.Clear();
                        break;
                    }
                }
            }
            catch
            {
                if (_runQueue.Count != 0 && !_isDisposed)
                {
                    // Re-queue anything that wasn't run.
                    lock (_instanceLock)
                    {
                        while (_runQueue.Count != 0)
                        {
                            _heap.Push(_runQueue.Dequeue());
                            _count++;
                        }
                    }
                }

                throw;
            }

            return successCount;
        }
        
        private void VerifyNotDisposed()
        {
            if (_isDisposed)
                ThrowObjectDisposed();
        }

        private static Dispatcher GetCurrentSlow()
        {
            Dispatcher current;
            if (SynchronizationContext.Current != null)
                throw new InvalidOperationException("current synchronization context is non-null");
            Thread thread = Thread.CurrentThread;
            t_current = current = new Dispatcher(thread);
            SynchronizationContext.SetSynchronizationContext(new DispatcherSynchronizationContext(current));
            s_log.Debug("Initialized dispatcher for thread {0} ({1})", thread.Name, thread.ManagedThreadId);
            return Current;
        }

        private static void SetCurrent(Dispatcher value)
        {
            Debug.Assert(t_current == null && value != null, "t_current == null && value != null");
            t_current = value;
            SynchronizationContext.SetSynchronizationContext(new DispatcherSynchronizationContext(value));
            Thread thread = Thread.CurrentThread;
            s_log.Debug("Initialized dispatcher for thread {0} ({1})", thread.Name, thread.ManagedThreadId);
        }

        private static void InvokeCallback(SendOrPostCallback callback, object state, CancellationToken ct)
        {
            try
            {
                callback(state);
            }
            catch (OperationCanceledException ex) when (ex.CancellationToken == ct)
            {
            }
        }

        private static void InvokeCallback(Action action, CancellationToken ct)
        {
            try
            {
                action();
            }
            catch (OperationCanceledException ex) when (ex.CancellationToken == ct)
            {
            }
        }

        private static T InvokeCallback<T>(Func<T> func, CancellationToken ct)
        {
            try
            {
                return func();
            }
            catch (OperationCanceledException ex) when (ex.CancellationToken == ct)
            {
                return default(T);
            }
        }

        private static void ThrowObjectDisposed()
        {
            throw new ObjectDisposedException(typeof(Dispatcher).FullName);
        }

        private static void ThrowAccessVerificationFailed()
        {
            throw new InvalidOperationException("Access verification failed");
        }

        // A work item in the queue
        private struct WorkItem
        {
            // The callback to invoke.
            public readonly SendOrPostCallback Callback;

            // A state provided to the callback on execution.
            public readonly object State;

            // The run time of the work item. For invokes, this will be zero which means they are always run.
            public readonly TimeSpan RunTime;

            public readonly CancellationToken CancellationToken;

            internal WorkItem(SendOrPostCallback callback, object state, TimeSpan runTime, CancellationToken ct)
            {
                Callback = callback;
                State = state;
                RunTime = runTime;
                CancellationToken = ct;
            }
        }

        private class WorkItemComparer : IComparer<WorkItem>
        {
            public int Compare(WorkItem x, WorkItem y)
            {
                return x.RunTime.CompareTo(y.RunTime);
            }
        }
    }
}