﻿using System;
using System.Text;

namespace Ramp
{
    /// <summary>
    ///     Caches StringBuilder instances per thread.
    /// </summary>
    public static class StringBuilderCache
    {
        public const int DefaultCapacity = 16; // copied from internal string builder default capacity

        public const int MaxCapacity = 360;

        [ThreadStatic]
        private static StringBuilder t_cachedInstance;

        public static StringBuilder Acquire(int capacity = DefaultCapacity)
        {
            if (capacity <= MaxCapacity)
            {
                StringBuilder sb = t_cachedInstance;
                if (sb != null && capacity < sb.Capacity)
                {
                    t_cachedInstance = null;
                    return sb.Clear();
                }
            }
            return new StringBuilder(capacity);
        }

        public static void Release(StringBuilder sb)
        {
            Validate.ArgumentNotNull(sb, "sb");
            if (sb.Capacity <= MaxCapacity)
                t_cachedInstance = sb;
        }

        public static string ReleaseAndGetString(StringBuilder sb)
        {
            Validate.ArgumentNotNull(sb, "sb");
            string s = sb.ToString();
            if (sb.Capacity <= MaxCapacity)
                t_cachedInstance = sb;
            return s;
        }
    }
}