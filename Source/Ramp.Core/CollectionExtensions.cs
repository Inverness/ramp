﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Ramp.Utilities.Collections;

namespace Ramp
{
    public static class CollectionExtensions
    {
        /// <summary>
        ///     For each item of an enumerable, apply an action.
        /// </summary>
        /// <typeparam name="T"> The enumerable type. </typeparam>
        /// <param name="enumerable"> The enumerable object. </param>
        /// <param name="action"> An action to apply to each item.  </param>
        [DebuggerStepThrough]
        public static void ForEach<T>(this IEnumerable<T> enumerable, Action<T> action)
        {
            Validate.ArgumentNotNull(enumerable, "enumerable");
            Validate.ArgumentNotNull(action, "action");

            foreach (T item in enumerable)
                action(item);
        }

        /// <summary>
        ///     For each item of an enumerable, apply an action with an index.
        /// </summary>
        /// <typeparam name="T"> The enumerable type. </typeparam>
        /// <param name="enumerable"> The enumerable object. </param>
        /// <param name="action"> An action to apply to each item. The first argument is the index of each item.  </param>
        [DebuggerStepThrough]
        public static void ForEach<T>(this IEnumerable<T> enumerable, Action<int, T> action)
        {
            Validate.ArgumentNotNull(enumerable, "enumerable");
            Validate.ArgumentNotNull(action, "action");

            int i = 0;
            foreach (T item in enumerable)
                action(i++, item);
        }

        public static void SetRange<T>(this IList<T> list, T value)
        {
            SetRange(list, 0, list.Count, value);
        }

        public static void SetRange<T>(this IList<T> list, int start, int count, T value)
        {
            if (list == null)
                throw new ArgumentNullException(nameof(list));
            if (start < 0)
                throw new ArgumentOutOfRangeException(nameof(start));
            if (count < 0)
                throw new ArgumentOutOfRangeException(nameof(count));
            if (start >= list.Count)
                throw new IndexOutOfRangeException("start index is out of range");
            int last = start + count - 1;
            if (last >= list.Count)
                throw new IndexOutOfRangeException("target range exceeds list range");
            for (int i = start; i <= last; i++)
                list[i] = value;
        }

        public static void AddRange<T>(this ICollection<T> collection, params T[] items)
        {
            AddRange(collection, (IEnumerable<T>) items);
        }

        public static void AddRange<T>(this ICollection<T> collection, IEnumerable<T> items)
        {
            if (collection == null)
                throw new ArgumentNullException(nameof(collection));
            if (items == null)
                throw new ArgumentNullException(nameof(items));
            foreach (T item in items)
                collection.Add(item);
        }

        /// <summary>
        ///     Resize a list.
        /// </summary>
        /// <typeparam name="T"> The list item type. </typeparam>
        /// <param name="list"> The list. </param>
        /// <param name="newSize"> The new list size. </param>
        /// <param name="defaultValue"> The default value to add to the list when growing it. </param>
        public static void Resize<T>(this IList<T> list, int newSize, T defaultValue = default(T))
        {
            Validate.ArgumentInRange(newSize >= 0, "newSize");

            if (list.Count == newSize)
                return;

            if (list.Count > newSize)
            {
                while (list.Count > newSize)
                    list.RemoveAt(list.Count - 1);
            }
            else
            {
                while (list.Count < newSize)
                    list.Add(defaultValue);
            }
        }

        /// <summary>
        ///     Removes an item from a list by swapping the last element into the specified index.
        ///     This avoids having to move items in the list.
        /// </summary>
        /// <typeparam name="T"> The list item type. </typeparam>
        /// <param name="list"> A list. </param>
        /// <param name="index"> An item index. </param>
        public static void RemoveAtSwap<T>(this List<T> list, int index)
        {
            int last = list.Count - 1;
            if (last != index)
                list[index] = list[last];
            list.RemoveAt(last);
        }

        /// <summary>
        ///     Removes an item from a list by swapping the last element into the specified index.
        ///     This avoids having to move items in the list.
        /// </summary>
        /// <typeparam name="T"> The list item type. </typeparam>
        /// <param name="list"> A list. </param>
        /// <param name="index"> An item index. </param>
        public static void RemoveAtSwap<T>(this IList<T> list, int index)
        {
            int last = list.Count - 1;
            if (last != index)
                list[index] = list[last];
            list.RemoveAt(last);
        }

        public static TValue GetValueOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key)
        {
            TValue result;
            dictionary.TryGetValue(key, out result);
            return result;
        }


        public static TValue GetValueOrDefaultReadOnly<TKey, TValue>(this IReadOnlyDictionary<TKey, TValue> dictionary,
                                                                     TKey key)
        {
            TValue result;
            dictionary.TryGetValue(key, out result);
            return result;
        }

        /// <summary>
        ///     Slice a collection depending on a beginning, ending, and stride.
        /// </summary>
        /// <typeparam name="T"> The item type. </typeparam>
        /// <param name="source"> The source collection. </param>
        /// <param name="begin">
        ///     The inclusive beginning index. Can be negative for positions relative to the end of the source.
        ///     Defaults to 0.
        /// </param>
        /// <param name="end">
        ///     The exclusive ending index. Can be negative for positions relative to the end of the source.
        ///     Defaults to the source size.
        /// </param>
        /// <param name="stride">
        ///     The stride when enumerating the source. A negative stride will enumerate the source in reverse.
        ///     Defaults to 1.
        /// </param>
        /// <returns> An enumerable object providing a slice of the source. </returns>
        public static IEnumerable<T> Slice<T>(this IEnumerable<T> source, int? begin = null, int? end = null,
                                              int? stride = null)
        {
            // ReSharper disable PossibleMultipleEnumeration
            Validate.ArgumentNotNull(source, "source");

            int beginValue = begin ?? 0;
            int strideValue = stride ?? 1;
            Validate.ArgumentInRange(stride != 0, "stride", "stride must not be zero");

            // Count the number of items in the source. If it's not a collection type the source must be copied
            // into a list.
            int sourceCount;
            if (!CollectionClassHelper.TryCount(source, out sourceCount))
            {
                var sourceList = new List<T>(source);
                sourceCount = sourceList.Count;
                source = sourceList;
            }

            int endValue = end ?? sourceCount;

            // Resolve negatives
            if (beginValue < 0)
                beginValue += sourceCount;
            if (endValue < 0)
                endValue += sourceCount;

            // Clamp to source collection size
            if (beginValue < 0)
                beginValue = 0;
            if (endValue > sourceCount)
                endValue = sourceCount;

            // Return empty array if slice will not have any items
            if (beginValue == endValue || beginValue > endValue || endValue < 0)
                return Array.Empty<T>();

            // Negative stride reverses the source
            if (strideValue < 0)
            {
                source = source.Reverse();
                strideValue = -strideValue;
            }

            return GetSliceCore(source, beginValue, endValue, strideValue);

            // ReSharper restore PossibleMultipleEnumeration
        }

        private static IEnumerable<T> GetSliceCore<T>(IEnumerable<T> source, int begin, int end, int stride)
        {
            using (IEnumerator<T> enumerator = source.GetEnumerator())
            {
                int current = -1;
                for (int target = begin; target < end; target += stride)
                {
                    while (current < target)
                    {
                        if (!enumerator.MoveNext())
                            yield break;
                        current++;
                    }

                    yield return enumerator.Current;
                }
            }
        }
    }
}