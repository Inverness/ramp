namespace Ramp
{
    /// <summary>
    ///     Specifies that can object can be made immutable (frozen). Frozen objects cannot be unfrozen. Frozen
    ///     objects must be thread-safe.
    /// </summary>
    public interface IFreezable
    {
        /// <summary>
        ///     Gets whether the object can be frozen.
        /// </summary>
        bool CanFreeze { get; }

        /// <summary>
        ///     Gets whether the object is currently frozen.
        /// </summary>
        bool IsFrozen { get; }

        /// <summary>
        ///     Creates a deep clone of the current object.
        /// </summary>
        /// <returns>A clone of the current object.</returns>
        IFreezable Clone();

        /// <summary>
        ///     Freezes the current object.
        /// </summary>
        void Freeze();

        /// <summary>
        ///     Gets the current object as frozen.
        /// </summary>
        /// <returns>The current instance if it is already frozen, or a frozen clone of the current instance.</returns>
        IFreezable GetAsFrozen();
    }
}