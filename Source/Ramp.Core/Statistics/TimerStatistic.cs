﻿using System;
using System.Diagnostics;

namespace Ramp.Statistics
{
    public class TimerStatistic : Statistic
    {
        private int _recurse;
        private long _calls;
        private readonly Stopwatch _sw = new Stopwatch();
        private TimeSpan _totalElapsed;
        private TimeSpan? _minimum;
        private TimeSpan? _maximum;

        public TimerStatistic(string name, string group = null)
            : base(name, group)
        {
        }

        public TimeSpan Elapsed => _sw.Elapsed;

        public TimeSpan TotalElapsed => _totalElapsed;

        public override double Value => _sw.Elapsed.TotalMilliseconds;

        public override double Minimum => (_minimum ?? TimeSpan.Zero).TotalMilliseconds;

        public override double Maximum => (_maximum ?? TimeSpan.Zero).TotalMilliseconds;

        public override double Average
        {
            get
            {
                TimeSpan average = _calls != 0 ? TimeSpan.FromTicks(_totalElapsed.Ticks / _calls) : TimeSpan.Zero;
                return average.TotalMilliseconds;
            }
        }

        public override double Calls => _calls;

        public void Start()
        {
            if (++_recurse == 1)
                _sw.Restart();
        }

        public void Stop()
        {
            if (_recurse == 0)
                throw new InvalidOperationException("not started");
            if (--_recurse > 0)
                return;
            _sw.Stop();

            TimeSpan elapsed = _sw.Elapsed;

            _totalElapsed += elapsed;
            _calls++;

            if (!_minimum.HasValue || elapsed < _minimum)
                _minimum = elapsed;
            if (!_maximum.HasValue || elapsed > _maximum)
                _maximum = elapsed;
        }

        public TimerScope StartScope()
        {
            return new TimerScope(this);
        }
    }
}