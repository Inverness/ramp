﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Ramp.Statistics
{
    public abstract class Statistic
    {
        public const string DefaultGroup = "Default";

        private static readonly object s_lock = new object();
        private static readonly Dictionary<string, Statistic> s_statistics = new Dictionary<string, Statistic>();

        protected Statistic(string name, string group = null)
        {
            Validate.ArgumentNotNull(name, "name");
            Name = name;
            Group = group ?? DefaultGroup;
            Register(this);
        }

        public string Name { get; }

        public string Group { get; }

        public abstract double Value { get; }

        public abstract double Minimum { get; }

        public abstract double Maximum { get; }

        public abstract double Average { get; }

        public abstract double Calls { get; }

        public override string ToString()
        {
            return $"{Name} = {Value}, Min:{Minimum}, Max:{Maximum}, Avg:{Average}, Calls:{Calls}";
        }

        public static IList<Statistic> GetStatistics()
        {
            lock (s_lock)
                return s_statistics.Values.ToArray();
        }

        private static void Register(Statistic s)
        {
            lock (s_lock)
            {
                try
                {
                    s_statistics.Add(s.Name, s);
                }
                catch (ArgumentException ex)
                {
                    throw new ArgumentException("The statistic is already registered: " + s.Name, ex);
                }
            }
        }
    }
}