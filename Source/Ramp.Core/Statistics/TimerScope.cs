﻿using System;

namespace Ramp.Statistics
{
    public struct TimerScope : IDisposable
    {
        private TimerStatistic _s;

        internal TimerScope(TimerStatistic s)
        {
            s.Start();
            _s = s;
        }

        public void Dispose()
        {
            if (_s != null)
            {
                _s.Stop();
                _s = null;
            }
        }
    }
}