﻿
namespace Ramp.Naming
{
    /// <summary>
    ///     Allows objects to be bound to names.
    /// </summary>
    /// <typeparam name="T"> The bound object type. </typeparam>
    public abstract class NamedBinding<T> : NamedBinding
    {
        protected NamedBinding(string name = null, NamedObject outer = null)
            : base(name, outer)
        {
        }

        /// <summary>
        ///     Gets the target value.
        /// </summary>
        public T Target
        {
            get
            {
                T value;
                TryGetTarget(out value);
                return value;
            }
        }

        /// <summary>
        /// Try getting the target value if it is still alive.
        /// </summary>
        /// <param name="target">The target value.</param>
        /// <returns>True if the target value is not null.</returns>
        public abstract bool TryGetTarget(out T target);

        public override bool TryGetTargetObject(out object targetObject)
        {
            T target;
            bool alive = TryGetTarget(out target);
            targetObject = target;
            return alive;
        }
    }
}
