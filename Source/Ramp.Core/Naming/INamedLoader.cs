﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Ramp.Naming
{
    /// <summary>
    ///     An object capable of loading named objects. This can either be added to the NamedObjectDirectory directly
    ///     or implemented by a NamedObject that is an outer of an object being loaded.
    /// </summary>
    public interface INamedLoader
    {
        /// <summary>
        ///     Try to load an object.
        /// </summary>
        /// <param name="type"> The target object type. </param>
        /// <param name="outer"> The target object's outer. </param>
        /// <param name="name"> The target object name. </param>
        /// <param name="ct"> A cancellation token. </param>
        /// <returns>
        ///     A task that represents a load operation in progress, or null if this loader cannot load the object.
        /// </returns>
        Task<NamedObject> TryLoad(Type type, NamedObject outer, string name, CancellationToken ct);
    }
}