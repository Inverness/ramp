﻿using System;

namespace Ramp.Naming
{
    /// <summary>
    /// Binds using a weak reference. The weak reference is cleared when this object is disposed.
    /// </summary>
    /// <typeparam name="T"> The bound object type. </typeparam>
    public class WeakNamedBinding<T> : NamedBinding<T>
        where T : class
    {
        private readonly WeakReference<T> _wr;

        public WeakNamedBinding(T value, string name = null, NamedObject outer = null)
            : base(name, outer)
        {
            _wr = new WeakReference<T>(value);
        }

        public override bool TryGetTarget(out T target) => _wr.TryGetTarget(out target);

        protected override void Dispose(bool disposing)
        {
            _wr.SetTarget(null);
            base.Dispose(disposing);
        }
    }
}