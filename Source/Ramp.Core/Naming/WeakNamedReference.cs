using System;
using System.ComponentModel;
using Newtonsoft.Json;

namespace Ramp.Naming
{
    [TypeConverter(typeof(NamedReferenceConverter))]
    public class WeakNamedReference<T> : NamedReferenceBase<T>
        where T : NamedObject
    {
        private readonly WeakReference<T> _wr;

        public WeakNamedReference()
        {
            _wr = new WeakReference<T>(null);
        }

        [JsonConstructor]
        public WeakNamedReference(string name)
            : base(name)
        {
            _wr = new WeakReference<T>(null);
        }

        protected override T TargetValue
        {
            get
            {
                T result;
                _wr.TryGetTarget(out result);
                return result;
            }

            set { _wr.SetTarget(value); }
        }
    }
}