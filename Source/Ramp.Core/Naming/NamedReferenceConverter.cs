using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;

namespace Ramp.Naming
{
    // Works with any INamedObjectReference that accepts a single name string as a constructor argument.
    public class NamedReferenceConverter : TypeConverter
    {
        private readonly Type _refType;

        public NamedReferenceConverter(Type type)
        {
            // This special constructor is not documented. The type converter system will prefer constructing
            // with the type of the object that this converter is being used with. This allows us to propertly
            // construct generic instances.
            Debug.Assert(typeof(INamedReference).IsAssignableFrom(type));
            _refType = type;
        }

        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string))
                return true;
            return base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            var stringValue = value as string;
            if (stringValue != null)
                return Activator.CreateInstance(_refType, stringValue);
            return base.ConvertFrom(context, culture, value);
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value,
                                         Type destinationType)
        {
            if (destinationType == typeof(string))
                return ((INamedReference) value).Name;
            return base.ConvertTo(context, culture, value, destinationType);
        }
    }
}