﻿
namespace Ramp.Naming
{
    /// <summary>
    /// Non-generic base class for objects bound to names.
    /// </summary>
    public abstract class NamedBinding : NamedObject
    {
        protected NamedBinding(string name, NamedObject outer)
            : base(name, outer)
        {
        }
        
        /// <summary>
        /// Try getting the target value if it is still alive.
        /// </summary>
        /// <param name="targetObject">The target value as an object.</param>
        /// <returns>True if the target value is not null.</returns>
        public abstract bool TryGetTargetObject(out object targetObject);
    }
}