namespace Ramp.Naming
{
    public interface INamedReference
    {
        ReferenceStatus Status { get; }

        string Name { get; set; }
    }

    public interface INamedReference<T> : INamedReference
        where T : NamedObject
    {
        T LoadTarget();

        void SetTarget(T target);
    }
}