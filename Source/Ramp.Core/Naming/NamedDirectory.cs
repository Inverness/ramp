using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NLog;
using Ramp.Threading;
using Ramp.Utilities;
using Ramp.Utilities.Collections;

namespace Ramp.Naming
{
    /// <summary>
    ///     Options for finding named objects in a directory.
    /// </summary>
    [Flags]
    public enum FindOptions
    {
        None = 0x00,

        /// <summary>
        ///     Whether to match the exact type or allow subclasses.
        /// </summary>
        ExactType = 0x01,

        /// <summary>
        ///     Whether to ignore the outer and find any object with a matching inner name.
        /// </summary>
        AnyOuter = 0x02
    }

    /// <summary>
    ///     Manages the hierarchy of named objects for a thread.
    /// </summary>
    public sealed class NamedDirectory : DispatcherObject
    {
        private const int MaxFreeSets = 200;
        private const int MaxInnerListCapacity = 8;
        private const string NameSeparator = "#";

        private static readonly Logger s_log = LogManager.GetCurrentClassLogger();
        private static readonly Type s_namedObjectType = typeof(NamedObject);
        private static readonly Type s_packageType = typeof(NamedPackage);

        [ThreadStatic]
        private static NamedDirectory t_current;

        // A linked array list of weak references to all named objects.
        // Nodes of this list are used by other structures for quick access.
        private readonly LinkedArrayList<WeakReference<NamedObject>> _objects = new LinkedArrayList<WeakReference<NamedObject>>();

        // A dictionary mapping names object list nodes with the same name. A special list type is used for
        // the value so that an entry with a single value has no allocation overhead.
        private readonly Dictionary<string, SingleFrugalStructList<int>> _nameDict =
            new Dictionary<string, SingleFrugalStructList<int>>();

        // Maps outers to their inner objects. A hash set is used for the values since inners can be added and removed
        // in arbitrary order
        private readonly Dictionary<int, LinkedArrayList<int>> _innerLists = new Dictionary<int, LinkedArrayList<int>>();
        private readonly Stack<LinkedArrayList<int>> _freeInnerLists = new Stack<LinkedArrayList<int>>();

        // Maps object load parameters to tasks being used to fulfill them. This prevents a particular object from
        // being loaded multiple times at once.
        private readonly Dictionary<LoadTaskKey, Task<NamedObject>> _objectLoadTasks =
            new Dictionary<LoadTaskKey, Task<NamedObject>>();

        // The default object loaders.
        private readonly List<INamedLoader> _loaders = new List<INamedLoader>();

        private readonly StringBuilder _sb = new StringBuilder(64);
        private readonly Dictionary<string, int> _prefixCounters = new Dictionary<string, int>();

        private volatile int _objectListVersion;
#if DEBUG
        private int _gcCheck;
#endif

        private NamedDirectory()
        {
        }

        /// <summary>
        ///     Gets an enumeration of all living objects.
        /// </summary>
        public IEnumerable<NamedObject> Objects
        {
            get
            {
                Validate.VerifyAccess(this);

                foreach (WeakReference<NamedObject> entry in _objects)
                {
                    NamedObject obj;
                    if (entry.TryGetTarget(out obj))
                        yield return obj;
                }
            }
        }

        /// <summary>
        ///     Gets the directory for the current thread.
        /// </summary>
        public static NamedDirectory Current
        {
            get
            {
                NamedDirectory current = t_current;
                if (current == null)
                    t_current = current = new NamedDirectory();
                return current;
            }
        }

        // A version incremented whenever objects are created or disposed.
        internal int ObjectListVersion => _objectListVersion;

        public void DisposeObjects()
        {
            Validate.VerifyAccess(this);
            Objects.ToList().ForEach(obj => obj.Dispose());
            Debug.Assert(_objects.Count == 0 && _innerLists.Count == 0);
        }

        /// <summary>
        ///     Enumerates all inner objects of the specified outer.
        /// </summary>
        /// <param name="outer"> The outer object. Can be null to get top level objects. </param>
        /// <param name="depth"> The depth to recurse. </param>
        /// <returns></returns>
        public IEnumerable<NamedObject> GetInners(NamedObject outer, int depth = 0)
        {
            Validate.Argument(NamedObject.IsNullOrNotDisposed(outer), "outer object is disposed");
            Validate.ArgumentInRange(depth >= 0, nameof(depth), "must not be negative");

            Validate.VerifyAccess(this);

            LinkedArrayList<int> inners;
            if (!_innerLists.TryGetValue(GetObjectListIndex(outer), out inners))
                yield break;

            foreach (int index in inners)
            {
                NamedObject inner;
                if (!_objects[index].TryGetTarget(out inner))
                    continue;

                yield return inner;

                if (depth == 0)
                    continue;

                foreach (NamedObject subinner in GetInners(inner, depth - 1))
                    yield return subinner;
            }
        }

        /// <summary>
        ///     Find an object by name.
        /// </summary>
        /// <typeparam name="T"> The object type. </typeparam>
        /// <param name="name"> The object name. </param>
        /// <param name="outer"> The outer to look for the object in. </param>
        /// <param name="options"> The find options. </param>
        /// <returns> A matching object, or null. </returns>
        public T Find<T>(string name, NamedObject outer = null, FindOptions options = FindOptions.None)
            where T : NamedObject
        {
            return (T) Find(typeof(T), name, outer, options);
        }

        /// <summary>
        ///     Find an object by name.
        /// </summary>
        /// <param name="type"> The object type. </param>
        /// <param name="name"> The object name. </param>
        /// <param name="outer"> The outer to look for the object in. </param>
        /// <param name="options"> The find options. </param>
        /// <returns> A matching object, or null. </returns>
        public NamedObject Find(Type type, string name, NamedObject outer = null, FindOptions options = FindOptions.None)
        {
            Validate.ArgumentNotNull(type, "type");
            Validate.ArgumentNotNull(name, "name");
            Validate.Argument(NamedObject.IsNullOrNotDisposed(outer), "outer must be null or a valid object");
            Validate.Argument(typeof(NamedObject).IsAssignableFrom(type), "not a NamedObject type", nameof(type));

            Validate.VerifyAccess(this);

            if ((options & FindOptions.AnyOuter) == 0 && !ResolveName(ref outer, ref name))
                return null;

            return FindCore(type, outer, name, options);
        }

        public async Task<T> LoadAsync<T>(NamedObject outer, string name,
                                          CancellationToken ct = default(CancellationToken))
            where T : NamedObject
        {
            return (T) await LoadAsync(typeof(T), outer, name, ct);
        }

        public Task<NamedObject> LoadAsync(Type type, NamedObject outer, string name,
                                           CancellationToken ct = default(CancellationToken))
        {
            Validate.ArgumentNotNull(type, "type");
            Validate.ArgumentNotNull(name, "name");
            Validate.Argument(NamedObject.IsNullOrNotDisposed(outer), "outer must be null or a valid object");

            Validate.VerifyAccess(this);

            if (ct.IsCancellationRequested || !ResolveName(ref outer, ref name))
                return null;

            NamedObject result = FindCore(type, outer, name, FindOptions.None);
            if (result != null)
                return Task.FromResult(result);

            return LoadAsyncCore(type, outer, name, ct);
        }

        /// <summary>
        ///     Generates a unique name by appending an incremented number to a prefix.
        /// </summary>
        /// <param name="prefix"> The prefix. </param>
        /// <param name="divider"> An optional divider between the prefix and the number. </param>
        /// <returns> A unique name. </returns>
        public string GenerateName(string prefix, string divider = null)
        {
            Validate.Argument(!string.IsNullOrEmpty(prefix), "prefix must not be null or empty", "prefix");

            Validate.VerifyAccess(this);

            int count;
            _prefixCounters.TryGetValue(prefix, out count);
            _prefixCounters[prefix] = ++count;

            return _sb.Clear().Append(prefix).Append(divider).Append(count).ToString();
        }

        /// <summary>
        ///     Registers an object loader.
        /// </summary>
        /// <param name="loader"> An object loader. </param>
        public void AddLoader(INamedLoader loader)
        {
            Validate.ArgumentNotNull(loader, "loader");

            if (!_loaders.Contains(loader))
                _loaders.Add(loader);
        }

        /// <summary>
        ///     Unregisters an object loader.
        /// </summary>
        /// <param name="loader"> An object loader. </param>
        public void RemoveLoader(INamedLoader loader)
        {
            Validate.ArgumentNotNull(loader, "loader");

            _loaders.Remove(loader);
        }

        internal void Initialize(NamedObject obj, string name, NamedObject outer)
        {
            if (name != null)
            {
                if (name.IndexOf('.') != -1)
                    throw new ArgumentOutOfRangeException(nameof(name), "must not be a dotted name");

                NamedObject existing = FindCore(obj.GetType(), outer, name, FindOptions.ExactType);
                if (existing != null)
                    throw new InvalidOperationException("Name conflict");
            }
            else
            {
                name = GenerateName(obj.GetType().Name, NameSeparator);
            }
            
            int index = _objects.AddLast(new WeakReference<NamedObject>(obj));

            AddToNameDict(name, index);

            int innerListIndex = AddToInnerList(GetObjectListIndex(outer), index);

            obj.ObjectListIndex = index;
            obj.InnerListIndex = innerListIndex;
            obj.Name = name;
            obj.Outer = outer;

            _objectListVersion++;
        }

        internal void Dispose(NamedObject obj, bool disposing)
        {
#if DEBUG
            // Dispose should not be called concurrently, only either from the thread this object is bound to
            // or the finalizer thread.
            if (Interlocked.Increment(ref _gcCheck) == 2)
                Debug.Fail("concurrent finalizer");
#endif
            Debug.Assert(obj.ObjectListIndex != -1, "obj.ObjectListIndex != -1");

            int index = obj.ObjectListIndex;

            RemoveFromInnerList(GetObjectListIndex(obj.Outer), obj.InnerListIndex);
            // The current object should not be an outer to any other object at this point.
            Debug.Assert(!disposing || !_innerLists.ContainsKey(index), "!disposing || !_innerSets.ContainsKey(index)");

            RemoveFromNameDict(obj.Name, index);

            _objects.RemoveAt(index);

            obj.Outer = null;
            obj.ObjectListIndex = -1;
            obj.InnerListIndex = -1;

            _objectListVersion++;

#if DEBUG
            Interlocked.Decrement(ref _gcCheck);
#endif
        }

        internal void Rename(NamedObject obj, string newName, NamedObject newOuter)
        {
            bool nameChanged;
            if (newName != null)
            {
                if (newName.IndexOf('.') != -1)
                    throw new ArgumentOutOfRangeException(nameof(newName), "must not be a dotted name");

                NamedObject existing = FindCore(obj.GetType(), newOuter, newName, FindOptions.ExactType);
                if (existing != null)
                {
                    if (existing == obj)
                        return;
                    throw new InvalidOperationException("Name conflict");
                }
                nameChanged = obj.Name != newName;
            }
            else
            {
                newName = GenerateName(obj.GetType().Name, NameSeparator);
                nameChanged = true;
                Debug.Assert(newName != obj.Name, "newName != obj.Name");
            }

            if (nameChanged)
            {
                int index = obj.ObjectListIndex;

                RemoveFromNameDict(obj.Name, index);
                AddToNameDict(newName, index);

                obj.Name = newName;
            }

            NamedObject oldOuter = obj.Outer;
            if (newOuter != oldOuter)
            {
                RemoveFromInnerList(GetObjectListIndex(oldOuter), obj.InnerListIndex);
                obj.InnerListIndex = AddToInnerList(GetObjectListIndex(newOuter), obj.ObjectListIndex);
                obj.Outer = newOuter;
            }

            _objectListVersion++;
        }

        private void AddToNameDict(string name, int index)
        {
            SingleFrugalStructList<int> indices;
            if (_nameDict.TryGetValue(name, out indices))
            {
                if (indices.Add(index))
                    _nameDict[name] = indices;
            }
            else
            {
                _nameDict[name] = new SingleFrugalStructList<int>(index);
            }
        }

        private void RemoveFromNameDict(string name, int index)
        {
            SingleFrugalStructList<int> indices = _nameDict[name];

            if (indices.Remove(index))
                _nameDict.Remove(name);
        }

        private int AddToInnerList(int outerIndex, int innerIndex)
        {
            LinkedArrayList<int> set;
            if (!_innerLists.TryGetValue(outerIndex, out set))
            {
                set = _freeInnerLists.Count != 0 ? _freeInnerLists.Pop() : new LinkedArrayList<int>();
                _innerLists[outerIndex] = set;
            }
            return set.AddLast(innerIndex);
        }

        private void RemoveFromInnerList(int outerIndex, int innerListIndex)
        {
            LinkedArrayList<int> list = _innerLists[outerIndex];
            list.RemoveAt(innerListIndex);

            if (list.Count == 0)
            {
                _innerLists.Remove(outerIndex);

                if (_freeInnerLists.Count < MaxFreeSets && list.Capacity <= MaxInnerListCapacity)
                    _freeInnerLists.Push(list);
            }
        }

        // Find assuming name has already been resolved.
        private NamedObject FindCore(Type type, NamedObject outer, string name, FindOptions options)
        {
            bool exact = (options & FindOptions.ExactType) != 0;
            bool anyOuter = (options & FindOptions.AnyOuter) != 0;

            if (anyOuter)
            {
                int lastDotIndex = name.LastIndexOf('.');
                if (lastDotIndex != -1)
                    name = name.Substring(lastDotIndex + 1);
            }

            SingleFrugalStructList<int> indices;
            if (!_nameDict.TryGetValue(name, out indices))
                return null;
            
            for (int i = 0; i < indices.Count; i++)
            {
                WeakReference<NamedObject> entry = _objects[indices[i]];
                NamedObject obj;

                if (entry.TryGetTarget(out obj) &&
                    (obj.Outer == outer || anyOuter) &&
                    (type == null || (exact ? obj.GetType() == type : type.IsInstanceOfType(obj))))
                {
                    Debug.Assert(obj.Name == name);
                    return obj;
                }
            }

            return null;
        }

        // Resolve a dotted name down to an outer package and innermost object name. If anyOuter is true there is
        // no need to resolve the name since all outer components of the name will be ignored.
        private bool ResolveName(ref NamedObject outer, ref string name)
        {
            int i;
            while ((i = name.IndexOf('.')) != -1)
            {
                string namePart = name.Substring(0, i);
                
                NamedObject match = FindCore(s_packageType, outer, namePart, FindOptions.None);
                if (match == null)
                {
                    match = FindCore(s_namedObjectType, outer, namePart, FindOptions.None);
                    if (match == null)
                        return false;
                }

                outer = match;
                name = name.Substring(i + 1);
            }

            return true;
        }

        // The core loading task. We only get here if the object isn't already loaded.
        private async Task<NamedObject> LoadAsyncCore(Type type, NamedObject outer, string name, CancellationToken ct)
        {
            // A key for identifying existing load tasks so we don't try loading the same object twice
            var key = new LoadTaskKey(type, outer, name);

            Task<NamedObject> loadTask;
            if (_objectLoadTasks.TryGetValue(key, out loadTask))
            {
                await loadTask;
                return ct.IsCancellationRequested ? null : loadTask.Result;
            }

            s_log.Trace("Load {2}.{0} ({1})", name, type, outer);

            // If outer is specified, then find if it or a parent can load the object
            if (outer != null)
            {
                NamedObject currentOuter = outer;
                while (currentOuter != null)
                {
                    var loader = currentOuter as INamedLoader;
                    if (loader != null)
                    {
                        loadTask = loader.TryLoad(type, outer, name, ct);
                        if (loadTask != null)
                        {
                            s_log.Trace("Found outer loader {0} ({1})", loader, loader.GetType());
                            break;
                        }
                    }
                    currentOuter = currentOuter.Outer;
                }
            }

            // If there is no outer or the outer couldn't load the object, try loading with all top level object loaders
            if (loadTask == null)
            {
                foreach (INamedLoader loader in _loaders)
                {
                    loadTask = loader.TryLoad(type, outer, name, ct);
                    if (loadTask != null)
                    {
                        s_log.Trace("Found global loader {0}", loader.GetType());
                        break;
                    }
                }
            }

            if (loadTask == null)
            {
                s_log.Warn("No loader found");
                return null;
            }

            _objectLoadTasks.Add(key, loadTask);
            try
            {
                await loadTask;

                if (ct.IsCancellationRequested)
                {
                    s_log.Trace("Load cancelled {1}.{0}", name, outer);
                    return null;
                }

                s_log.Trace("Load finished {0}", loadTask.Result);

                return loadTask.Result;
            }
            finally
            {
                _objectLoadTasks.Remove(key);
            }
        }

        private static int GetObjectListIndex(NamedObject obj)
        {
            return obj?.ObjectListIndex ?? -1;
        }

        // A key for identifying loading operations in a dictionary.
        private struct LoadTaskKey : IEquatable<LoadTaskKey>
        {
            private readonly Type _type;
            private readonly NamedObject _outer;
            private readonly string _name;

            public LoadTaskKey(Type type, NamedObject outer, string name)
            {
                _type = type;
                _outer = outer;
                _name = name;
            }

            public override int GetHashCode()
            {
                return HashUtility.Combine(_type, _outer, _name);
            }

            public bool Equals(LoadTaskKey other)
            {
                return _type == other._type && _outer == other._outer && _name == other._name;
            }
        }
        
        // A collection that favors storing a single item with no additional allocations while only using an actual
        // collection if additional items are added. This is specifically used with the object name dictionary since
        // in the vast majority of cases names will be unique.
        [DebuggerDisplay("Count = {Count}")]
        internal struct SingleFrugalStructList<T>
        {
            private T _single;
            private List<T> _collection;

            public SingleFrugalStructList(T value)
            {
                _single = value;
                _collection = null;
            }

            public int Count => _collection == null ? 1 : _collection.Count;

            public T this[int index]
            {
                get
                {
                    if (_collection == null)
                    {
                        if (index != 0)
                            throw new ArgumentOutOfRangeException(nameof(index));
                        return _single;
                    }
                    else
                    {
                        return _collection[index];
                    }
                }
            }

            // Returns true if the list is promoted.
            public bool Add(T item)
            {
                if (_collection == null)
                {
                    _collection = new List<T>(2) { _single, item };
                    _single = default(T);
                    return true;
                }
                else
                {
                    _collection.Add(item);
                    return false;
                }
            }

            // Returns true if the list becomes empty, does not clear a single
            public bool Remove(T item)
            {
                if (_collection == null)
                {
                    return EqualityComparer<T>.Default.Equals(item, _single);
                }
                else
                {
                    bool success = _collection.Remove(item);
                    return success && _collection.Count == 0;
                }
            }
        }
    }
}