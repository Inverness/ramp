namespace Ramp.Naming
{
    /// <summary>
    ///     Describes the status of an object reference.
    /// </summary>
    public enum ReferenceStatus
    {
        /// <summary>
        ///     The reference does not have a valid ID and cannot be resolved.
        /// </summary>
        Invalid,

        /// <summary>
        ///     The reference has a valid ID, but is not loaded or loading.
        /// </summary>
        Pending,

        /// <summary>
        ///     The referenceed object is currently being loaded.
        /// </summary>
        Loading,

        /// <summary>
        ///     The referenced object is loaded.
        /// </summary>
        Loaded
    }
}