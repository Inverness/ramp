using System.ComponentModel;
using Newtonsoft.Json;

namespace Ramp.Naming
{
    /// <summary>
    ///     Stores a reference to a target that is found upon request.
    /// </summary>
    /// <typeparam name="T"> The target name. </typeparam>
    [TypeConverter(typeof(NamedReferenceConverter))]
    public class NamedReference<T> : NamedReferenceBase<T>
        where T : NamedObject
    {
        private T _target;

        public NamedReference()
        {
        }

        [JsonConstructor]
        public NamedReference(string name)
            : base(name)
        {
        }

        protected override T TargetValue
        {
            get { return _target; }

            set { _target = value; }
        }
    }
}