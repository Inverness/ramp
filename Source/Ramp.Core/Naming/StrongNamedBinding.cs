﻿namespace Ramp.Naming
{
    /// <summary>
    /// Binds using a normal, strong reference that keeps the target alive. The target is set to its default value
    /// when this object is disposed.
    /// </summary>
    /// <typeparam name="T"> The bound object type. </typeparam>
    public class StrongNamedBinding<T> : NamedBinding<T>
    {
        private T _value;

        public StrongNamedBinding(T value, string name = null, NamedObject outer = null)
            : base(name, outer)
        {
            _value = value;
        }

        public override bool TryGetTarget(out T target)
        {
            target = _value;
            return target != null;
        }

        protected override void Dispose(bool disposing)
        {
            _value = default(T);
            base.Dispose(disposing);
        }
    }
}