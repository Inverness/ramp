﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Ramp.Threading;

namespace Ramp.Naming
{
    /// <summary>
    ///     The base class for named objects. Named objects have a strong identifying name that allows them to be
    ///     tracked, referenced, and found in an organized hierarchy.
    /// </summary>
    [JsonObject(MemberSerialization.OptOut)]
    public abstract class NamedObject : DispatcherObject, IDisposable
    {
        internal int ObjectListIndex; // This object's index within the directory's object list. -1 if disposed.
        internal int InnerListIndex; // This object's index within it's outer's list of inners

        protected NamedObject(string name = null, NamedObject outer = null)
        {
            // It's fine for ObjectListIndex to be 0 instead of -1 at this point
            (Directory = NamedDirectory.Current).Initialize(this, name, outer);
        }

        // A finalizer is necessary because a named object must be removed from the directory
        ~NamedObject()
        {
            Dispose(false);
        }

        public event TypedEventHandler<NamedObject, EventArgs> Disposing;

        /// <summary>
        ///     Gets the name of this object. Will never be null.
        /// </summary>
        public string Name { get; internal set; }

        /// <summary>
        ///     Gets the outer of this object. Can be null for top level objects.
        /// </summary>
        public NamedObject Outer { get; internal set; }

        /// <summary>
        ///     Gets the directory for this object.
        /// </summary>
        [JsonIgnore]
        public NamedDirectory Directory { get; }

        [JsonIgnore]
        public bool IsDisposed => ObjectListIndex == -1;

        public override string ToString()
        {
            if (IsDisposed)
                return Name + " (Disposed)";
            return GetFullName();
        }

        public void Dispose()
        {
            VerifyAccess();
            if (IsDisposed)
                return;
            OnDisposing();
            Dispose(true);
            Debug.Assert(IsDisposed);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        ///     Gets the full name of this object by searching up the outer object hierarchy.
        /// </summary>
        /// <returns> The full dotted name of the current object. </returns>
        public string GetFullName()
        {
            VerifyNotDisposed();

            if (Outer == null)
                return Name;

            var sb = StringBuilderCache.Acquire(Name.Length + 32);
            Outer.AppendPath(sb);
            sb.Append(Name);
            return StringBuilderCache.ReleaseAndGetString(sb);
        }

        /// <summary>
        ///     Gets an outer object of the specified type.
        /// </summary>
        /// <typeparam name="T"> The outer object type. </typeparam>
        /// <returns> An outer object of the specified type, or null if one was not found. </returns>
        public T GetOuter<T>()
            where T : NamedObject
        {
            VerifyNotDisposed();

            NamedObject current = Outer;

            while (current != null)
            {
                var target = current as T;
                if (target != null)
                    return target;
                current = current.Outer;
            }

            return null;
        }

        /// <summary>
        ///     Gets the topmost outer object.
        /// </summary>
        /// <returns> The topmost outer object, or null if one was not found. </returns>
        public NamedObject GetTopOuter()
        {
            VerifyNotDisposed();

            NamedObject current = this;

            while (current != null)
            {
                if (current.Outer == null)
                    return current;
                current = current.Outer;
            }

            return null;
        }

        public static bool IsNullOrDisposed(NamedObject obj)
        {
            return obj == null || obj.ObjectListIndex == -1;
        }

        public static bool IsNullOrNotDisposed(NamedObject obj)
        {
            return obj == null || obj.ObjectListIndex != -1;
        }

        /// <summary>
        ///     Renames an object.
        /// </summary>
        /// <param name="name"> A new object name. </param>
        /// <param name="outer"> A new outer. </param>
        /// <exception cref="InvalidOperationException"> An object with the same name and outer already exists. </exception>
        protected virtual void Rename(string name, NamedObject outer)
        {
            VerifyNotDisposed();
            Directory.Rename(this, name, outer);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
                DisposeInners();

            if (ObjectListIndex != -1)
                Directory.Dispose(this, disposing);
        }

        protected virtual void OnDisposing()
        {
            Disposing?.Invoke(this, EventArgs.Empty);
        }

        protected void VerifyNotDisposed()
        {
            // putting the actual throw in another method allows this one to be inlined
            if (ObjectListIndex == -1)
                ThrowObjectDisposed();
        }

        protected void DisposeInners()
        {
            Directory.GetInners(this).ToList().ForEach(i => i.Dispose());
        }

        private void AppendPath(StringBuilder sb)
        {
            Outer?.AppendPath(sb);
            sb.Append(Name).Append('.');
        }

        private void ThrowObjectDisposed()
        {
            throw new ObjectDisposedException(GetType().FullName);
        }
    }
}