using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Ramp.Threading;

namespace Ramp.Naming
{
    /// <summary>
    ///     The base class for object references.
    /// </summary>
    /// <typeparam name="TTarget"> The target object type. </typeparam>
    [DebuggerDisplay("{Name}")]
    public abstract class NamedReferenceBase<TTarget> : INamedReference<TTarget>
        where TTarget : NamedObject
    {
        private static readonly Task<TTarget> s_nullResult = Task.FromResult((TTarget) null);
        
        private string _name;
        private readonly NamedDirectory _directory = NamedDirectory.Current;
        private int _lastVersion;
        private LoadInfo _loadInfo;

        protected NamedReferenceBase()
        {
        } 

        protected NamedReferenceBase(string name)
        {
            _name = name;
        }

        /// <summary>
        ///     Gets or sets the target object ID. Changing the ID will clear the current target and cancel a
        ///     load.
        /// </summary>
        public string Name
        {
            get { return _name; }

            set
            {
                if (_name == value)
                    return;

                _name = value;
                _lastVersion--; // ensure next resolve attempt goes ahead
                TargetValue = null;
                CancelLoad();

                OnNameChanged();
            }
        }

        /// <summary>
        ///     Gets the current object status.
        /// </summary>
        [DebuggerBrowsable(DebuggerBrowsableState.Never)] // Would trigger a load on debug thread
        public ReferenceStatus Status
        {
            get
            {
                if (_name == null)
                    return ReferenceStatus.Invalid;
                if (LoadTarget() != null)
                    return ReferenceStatus.Loaded;
                return _loadInfo != null ? ReferenceStatus.Loading : ReferenceStatus.Pending;
            }
        }

        /// <summary>
        ///     Gets whether the current object status is Loaded.
        /// </summary>
        [DebuggerBrowsable(DebuggerBrowsableState.Never)] // Would trigger a load on debug thread
        public bool IsLoaded => Status == ReferenceStatus.Loaded;

        /// <summary>
        ///     Gets the reference's directory.
        /// </summary>
        public NamedDirectory Directory => _directory;
        
        /// <summary>
        /// Gets or sets the resolved target value. This should be stored in a simple strong or weak reference.
        /// </summary>
        protected abstract TTarget TargetValue { get; set; }

        /// <summary>
        ///     Gets the target if it is already loaded, or begins loading it if possible.
        /// </summary>
        /// <returns>
        ///     A task that completes when the object is finished loading. The task result is null if the target name
        ///     is invalid or the load failed.
        /// </returns>
        public Task<TTarget> LoadTargetAsync()
        {
            TTarget target = LoadTarget();

            // We can't cache a result task because a class that derives from NamedReferenceBase must be able to
            // implement weak reference storage of the target.
            if (target != null)
                return Task.FromResult(target);

            if (_loadInfo != null)
                return _loadInfo.Task;

            return s_nullResult;
        }

        /// <summary>
        ///     Find or load the target object.
        /// </summary>
        /// <returns>
        ///     The target object if the ID was valid, the object was already loaded, or the load finished immediately.
        ///     Otherwise, null.
        /// </returns>
        public TTarget LoadTarget()
        {
            if (_name == null || _loadInfo != null)
                return null;

            TTarget target = TargetValue;

            // Clear disposed target
            if (target != null && target.IsDisposed)
            {
                target = null;
                TargetValue = null;
            }

            // Check if we need to resolve the target
            if (target == null && _lastVersion != _directory.ObjectListVersion)
            {
                _loadInfo = new LoadInfo();

                // This will have already ran to completion if the target already exists.
                Task<TTarget> loadTask = _directory.LoadAsync<TTarget>(null, _name, _loadInfo.Cts.Token);

                if (loadTask.IsCompleted)
                {
                    target = FinishLoad(loadTask);
                }
                else
                {
                    // Passing the cancellation token to ContinueWith() means that FinishLoad() will not be invoked
                    // if a cancel occurrs, we don't want that. FinishLoad() handles cancelling by returning null.
                    _loadInfo.Task = loadTask.ContinueWith<TTarget>(FinishLoad,
                                                                    CancellationToken.None,
                                                                    TaskContinuationOptions.None,
                                                                    Dispatcher.Current.TaskScheduler);
                }
            }

            Debug.Assert(target == null || !target.IsDisposed, "target must not be disposed");

            return target;
        }

        public void SetTarget(TTarget value)
        {
            _name = value?.GetFullName();
            _lastVersion--;
            TargetValue = value == null || !value.IsDisposed ? value : null;
            CancelLoad();

            OnNameChanged();
        }

        protected virtual void OnNameChanged()
        {
        }

        private TTarget FinishLoad(Task<TTarget> task)
        {
            if (task.IsCanceled)
                return null; // cleanup was already performed

            Debug.Assert(_loadInfo != null, "_loadInfo != null");

            _loadInfo.Cts.Dispose();
            _loadInfo = null;

            if (task.IsFaulted)
            {
                Debug.Assert(task.Exception != null);
                throw task.Exception;
            }

            _lastVersion = Directory.ObjectListVersion;

            // Null the target if it's already been disposed
            TTarget target = task.Result;

            if (target != null && target.IsDisposed)
                target = null;

            TargetValue = target;

            return target;
        }

        private void CancelLoad()
        {
            if (_loadInfo != null)
            {
                _loadInfo.Cts.Cancel();
                _loadInfo.Cts.Dispose();
                _loadInfo = null;
            }
        }

        private class LoadInfo
        {
            public readonly CancellationTokenSource Cts = new CancellationTokenSource();
            public Task<TTarget> Task;
        }
    }
}