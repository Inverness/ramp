using System;

namespace Ramp.Naming
{
    /// <summary>
    ///     Represents a logical collection of named objects. Packages take priority when resolving object paths.
    /// </summary>
    public class NamedPackage : NamedObject
    {
        public NamedPackage(string name, NamedObject outer = null)
            : base(ValidatePackageName(name), outer)
        {
        }

        private static string ValidatePackageName(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentException("Invalid package name", nameof(name));
            return name;
        }
    }
}