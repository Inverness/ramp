﻿using Ramp.Threading;

namespace Ramp
{
    /// <summary>
    ///     Provides access to the dispatchers for the game and worker threads.
    /// </summary>
    public interface IDispatcherService
    {
        /// <summary>
        ///     Gets the main dispatcher for the game thread.
        /// </summary>
        Dispatcher GameDispatcher { get; }

        /// <summary>
        ///     Gets the dispatcher for the worker thread.
        /// </summary>
        Dispatcher WorkDispatcher { get; }
    }
}