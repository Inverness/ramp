﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.Xna.Framework;

namespace Ramp.Utilities.Collections
{
    /// <summary>
    ///     A quad tree for testing the bounds of arbitrary items. This class is optimized so that its instances
    ///     should be reused rather than creating a new QuadTree for each operation.
    /// </summary>
    /// <typeparam name="T"> The item type. </typeparam>
    [DebuggerDisplay("Count = {Count}")]
    public sealed class QuadTree<T>
    {
        private const int DefaultMaxDepth = 10;
        private const int DefaultMaxNodeItems = 8;

        private readonly Vector2 _min;
        private readonly Vector2 _max;
        private readonly Stack<QuadTreeNode<T>> _freeNodes = new Stack<QuadTreeNode<T>>();
        private QuadTreeNode<T> _root;
        private int _count;

        /// <summary>
        ///     Initializes a QuadTree instance.
        /// </summary>
        /// <param name="min"> The minimum bound of the tree. </param>
        /// <param name="max"> The maximum bound of the tree. </param>
        /// <param name="maxDepth"> The maximum depth of the tree. A value less than 1 will default to 10. </param>
        /// <param name="maxNodeItems"> The maximum number of items per node. A value less than 1 will default to 8. </param>
        public QuadTree(Vector2 min, Vector2 max, int maxDepth = 0, int maxNodeItems = 0)
        {
            if (max.X < min.X || max.Y < min.Y)
                throw new ArgumentException("maximum bound is within the minimum bound");

            _min = min;
            _max = max;
            MaxDepth = maxDepth > 0 ? maxDepth : DefaultMaxDepth;
            MaxNodeItems = maxNodeItems > 0 ? maxNodeItems : DefaultMaxNodeItems;
        }

        /// <summary>
        ///     Gets the maximum depth of the quad tree.
        /// </summary>
        public int MaxDepth { get; }

        /// <summary>
        ///     Gets the maximum number of items per node. This is ignored if the maximum depth is reached.
        /// </summary>
        public int MaxNodeItems { get; }

        /// <summary>
        ///     Gets the number of items in the tree.
        /// </summary>
        public int Count => _count;

        /// <summary>
        ///     Add an item into the tree.
        /// </summary>
        /// <param name="item"> An item. </param>
        /// <param name="box"> A bounding box. The third dimension will be ignored. </param>
        public void Add(T item, BoundingBox box)
        {
            Vector2 min, max;
            GetBounds(box, out min, out max);
            Add(item, min, max);
        }

        /// <summary>
        ///     ADd an item into the tree.
        /// </summary>
        /// <param name="item"> An item. </param>
        /// <param name="min"> The minimum bound of the item. </param>
        /// <param name="max"> The maximum bound of the item. </param>
        public void Add(T item, Vector2 min, Vector2 max)
        {
            if (_root == null)
                _root = AllocateNode(0, _min, _max);

            _root.Add(item, min, max);
            _count++;
        }

        /// <summary>
        ///     Clears all items from the tree.
        /// </summary>
        public void Clear()
        {
            if (_root != null)
            {
                FreeNode(_root);
                _root = null;
                _count = 0;
            }
        }

        /// <summary>
        ///     Finds all items within the specified bounds.
        /// </summary>
        /// <param name="box"> A bounding box. The third dimensional will be ignored. </param>
        /// <param name="results"> A collection that will receive the resulting items. </param>
        public void Find(BoundingBox box, ICollection<T> results)
        {
            Vector2 min, max;
            GetBounds(box, out min, out max);
            Find(min, max, results);
        }

        /// <summary>
        ///     Finds all items within the specified bounds.
        /// </summary>
        /// <param name="min"> The minimum bound. </param>
        /// <param name="max"> The maximum bound. </param>
        /// <param name="results"> A collection that will receive the resulting items. </param>
        public void Find(Vector2 min, Vector2 max, ICollection<T> results)
        {
            Validate.ArgumentNotNull(results, "results");
            Validate.Argument(max.X >= min.X && max.Y >= min.Y, "invalid bounds", "max");

            _root?.Find(min, max, results);
        }

        public bool Remove(QuadTreeNode<T> node, T item)
        {
            Validate.ArgumentNotNull(node, "node");
            Validate.Argument(node.Tree == this, "node does not belong to this tree");

            bool success = node.Remove(item);
            if (success)
                _count--;
            return success;
        }

        internal QuadTreeNode<T> AllocateNode(int depth, Vector2 min, Vector2 max)
        {
            QuadTreeNode<T> node = _freeNodes.Count != 0 ? _freeNodes.Pop() : new QuadTreeNode<T>(this);
            node.OnAllocate(min, max, depth);
            return node;
        }

        internal void FreeNode(QuadTreeNode<T> node)
        {
            node.OnFree();
            _freeNodes.Push(node);
        }

        private static void GetBounds(BoundingBox box, out Vector2 min, out Vector2 max)
        {
            min = new Vector2(box.Min.X, box.Min.Y);
            max = new Vector2(box.Max.X, box.Max.Y);
        }

        private static bool Intersects(Vector2 minA, Vector2 maxA, Vector2 minB, Vector2 maxB)
        {
            return minB.X < maxA.X && minA.X < maxB.X && minB.Y < maxA.Y && minA.Y < maxB.Y;
        }
    }
}