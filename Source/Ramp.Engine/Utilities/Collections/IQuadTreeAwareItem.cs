namespace Ramp.Utilities.Collections
{
    /// <summary>
    ///     An item that is aware of quad tree insertion and receives the node it is inserted in.
    /// </summary>
    /// <typeparam name="T"> The item type. </typeparam>
    public interface IQuadTreeAwareItem<T>
    {
        void OnNodeChanged(QuadTreeNode<T> node);
    }
}