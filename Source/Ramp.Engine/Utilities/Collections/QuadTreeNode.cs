using System;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.Xna.Framework;

namespace Ramp.Utilities.Collections
{
    /// <summary>
    ///     A node in the tree. These are cached in a stack so they don't need to be reallocated for subsequent uses
    ///     of the same QuadTree instance.
    /// </summary>
    [DebuggerDisplay("Count = {DebugCount}, Depth = {_depth}")]
    public sealed class QuadTreeNode<T>
    {
        private readonly QuadTree<T> _tree; // The parent tree
        private Vector2 _min; // The inclusive minimum bound
        private Vector2 _max; // The inclusive maximum bound
        private Vector2 _center; // The cached center
        private int _depth; // The depth of the node, where 0 is the root
        private QuadTreeNode<T> _nw; // The northwest child node
        private QuadTreeNode<T> _ne; // The northeast child node
        private QuadTreeNode<T> _se; // The southeast child node
        private QuadTreeNode<T> _sw; // The southwest child node
        private readonly List<Entry> _items; // This items in this node

        internal QuadTreeNode(QuadTree<T> tree)
        {
            _tree = tree;
            _items = new List<Entry>(Math.Min(4, _tree.MaxNodeItems));
        }

        /// <summary>
        ///     Gets the tree that contains this node.
        /// </summary>
        public QuadTree<T> Tree => _tree;

        internal int DebugCount => _items?.Count ?? 0;

        internal void OnAllocate(Vector2 min, Vector2 max, int depth)
        {
            _min = min;
            _max = max;
            _depth = depth;
            _center = min + ((max - min) / 2);
        }

        internal void OnFree()
        {
            if (_items != null)
            {
                for (int i = 0; i < _items.Count; i++)
                    InvokeOnNodeChanged(_items[i].Item, null);
                _items.Clear();
            }

            if (_nw != null)
            {
                _tree.FreeNode(_nw);
                _nw = null;

                _tree.FreeNode(_ne);
                _ne = null;

                _tree.FreeNode(_se);
                _se = null;

                _tree.FreeNode(_sw);
                _sw = null;
            }
        }

        internal void Add(T item, Vector2 itemMin, Vector2 itemMax)
        {
            // Try inserting into a child node.
            if (_nw != null)
            {
                QuadTreeNode<T> node = GetContainingNode(itemMin, itemMax);
                if (node != null)
                {
                    node.Add(item, itemMin, itemMax);
                    return;
                }
            }

            // Does not fit into a child node, or there are no child nodes, so add it to this one.
            _items.Add(new Entry(item, itemMin, itemMax));
            InvokeOnNodeChanged(item, this);

            // Stop here if we've reached the maximum depth or the items array has not yet hit the maximum.
            // If we've reached the maximum depth then the maximum items number is ignored. Insertions are never
            // denied. Even if an item is out of the bounds of the root, it just ends up in the root's items list.
            if (_items.Count <= _tree.MaxNodeItems || _depth >= _tree.MaxDepth)
                return;

            // Create child nodes and begin re-insertion.
            if (_nw == null)
            {
                // Inclusiveness means that the max bound of the child nodes can overlap with the min bound of ohers,
                // however GetNodeIndex() will deal with it appropriately.
                int newDepth = _depth + 1;
                Vector2 childSize = (_max - _min) / 2;

                Vector2 childMin = _min;
                _nw = _tree.AllocateNode(newDepth, childMin, childMin + childSize);

                childMin = new Vector2(_min.X + childSize.X, _min.Y);
                _ne = _tree.AllocateNode(newDepth, childMin, childMin + childSize);

                childMin = _min + childSize;
                _se = _tree.AllocateNode(newDepth, childMin, childMin + childSize);

                childMin = new Vector2(_min.X, _min.Y + childSize.Y);
                _sw = _tree.AllocateNode(newDepth, childMin, childMin + childSize);
            }

            for (int i = _items.Count - 1; i > -1; i--)
            {
                Entry entry = _items[i];
                QuadTreeNode<T> node = GetContainingNode(entry.Min, entry.Max);

                if (node == null)
                    continue;

                _items.RemoveAtSwap(i);
                // OnNodeChanged will be invoked in the Add()

                node.Add(entry.Item, entry.Min, entry.Max);
            }
        }

        // Remove an item only from this node, returning true on success.
        internal bool Remove(T item)
        {
            if (_items != null)
            {
                EqualityComparer<T> comparer = EqualityComparer<T>.Default;

                for (int i = 0; i < _items.Count; i++)
                {
                    if (comparer.Equals(_items[i].Item, item))
                    {
                        InvokeOnNodeChanged(_items[i].Item, null);
                        _items.RemoveAtSwap(i);
                        return true;
                    }
                }
            }

            return false;
        }

        // Gets all items in any nodes that intersect
        internal void Find(Vector2 min, Vector2 max, ICollection<T> results)
        {
            if (_nw != null)
                GetContainingNode(min, max)?.Find(min, max, results);

            for (int i = 0; i < _items.Count; i++)
                results.Add(_items[i].Item);
        }

        private void InvokeOnNodeChanged(T item, QuadTreeNode<T> node)
        {
            var qti = item as IQuadTreeAwareItem<T>;
            qti?.OnNodeChanged(node);
        }

        // Gets the node that can fully contain the bounds.
        private QuadTreeNode<T> GetContainingNode(Vector2 itemMin, Vector2 itemMax)
        {
            Vector2 thisMin = _min;
            Vector2 thisMax = _max;
            Vector2 center = _center;

            // Check left then right
            if (itemMax.X < center.X && itemMin.X >= thisMin.X)
            {
                // Check top then bottom
                if (itemMax.Y < center.Y && itemMin.Y >= thisMin.Y)
                    return _nw; // NW
                if (itemMin.Y >= center.Y && itemMax.Y <= thisMax.Y)
                    return _sw; // SW
            }
            else if (itemMin.X >= center.X && itemMax.X <= thisMax.X)
            {
                if (itemMax.Y < center.Y && itemMin.Y >= thisMin.Y)
                    return _ne; // NE
                if (itemMin.Y >= center.Y && itemMax.Y <= thisMax.Y)
                    return _se; // SE
            }

            return null;
        }

        private struct Entry
        {
            public readonly T Item;
            public readonly Vector2 Min; // The inclusive minimum bound of the item.
            public readonly Vector2 Max; // The inclusive maximum bound of the item.

            public Entry(T item, Vector2 min, Vector2 max)
            {
                Item = item;
                Min = min;
                Max = max;
            }
        }
    }
}