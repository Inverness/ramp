using System;
using System.Diagnostics;
using Lidgren.Network;
using Ramp.Naming;
using Ramp.Network;

namespace Ramp.Simulation
{
    /// <summary>
    ///     The base class for all components. Provides virtual methods and static methods for
    ///     registering and retrieving component types by name.
    /// </summary>
    /// <remarks>
    ///     Subclasses of ActorComponent must have unique non-qualified names in order to be registered
    ///     without name collision.
    /// </remarks>
    public abstract class ActorComponent : NamedObject, IReplicatable
    {
        protected ActorComponent()
            : this(ActorComponentType.CurrentConstructorArguments)
        {
        }

        private ActorComponent(ActorComponentType.ConstructorArguments args)
            : base(args.Name, args.Actor)
        {
        }

        /// <summary>
        ///     Gets the actor that owns this component.
        /// </summary>
        public Actor Actor => (Actor) Outer;

        public Level Level => (Level) Outer.Outer;

        public World World => (World) Outer.Outer.Outer;

        /// <summary>
        ///     Update the object's state.
        /// </summary>
        /// <param name="elapsed"> The amount of time that has elapsed since the previous update. </param>
        public virtual void Update(TimeSpan elapsed)
        {
        }

        /// <inheritdoc />
        public virtual void WriteDelta(DeltaWriter writer, ReplicationContext context)
        {
        }


        /// <inheritdoc />
        public virtual void ReadDelta(DeltaReader reader, ReplicationContext context)
        {
        }

        /// <summary>
        ///     Called when a component message is received.
        /// </summary>
        /// <param name="id"> ID of the received message. </param>
        /// <param name="message"> The incoming message. </param>
        /// <param name="source"> The source of the message serverside, or null if clientside. </param>
        /// <returns> True if the message was handled. </returns>
        protected internal virtual bool OnReceiveMessage(byte id, NetIncomingMessage message, IReplicaConnection source)
        {
            return false;
        }

        protected internal virtual void OnLevelChanging(Level newLevel)
        {
        }

        protected internal virtual void OnLevelChanged()
        {
        }

        /// <summary>
        ///     Called when this component's actor is being disposed.
        /// </summary>
        protected internal virtual void OnActorDisposing()
        {
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && Actor.Status != ActorStatus.Disposed)
                throw new InvalidOperationException("Actor components must be disposed by their actor");
            base.Dispose(disposing);
        }

        /// <summary>
        ///     Set a field to the matching component.
        /// </summary>
        /// <typeparam name="T"> The component type. </typeparam>
        /// <param name="field"> The field that will receive the component. </param>
        /// <returns> If throwOnNull is false, returns whether the field is null on completion. </returns>
        protected bool GetComponent<T>(out T field) where T : ActorComponent
        {
            field = Actor.GetComponent<T>();
            return field != null;
        }

        /// <summary>
        ///     Set a field to the matching component.
        /// </summary>
        /// <typeparam name="T"> The component type. </typeparam>
        /// <param name="field"> The field that will receive the component. </param>
        /// <exception cref="ActorComponentNotFoundException">The component was not found.</exception>
        protected void GetComponentRequired<T>(out T field) where T : ActorComponent
        {
            field = Actor.GetComponent<T>();
            if (field == null)
                throw new ActorComponentNotFoundException(typeof(T));
        }

        /// <summary>
        ///     Create an message to send to this component's replica.
        /// </summary>
        /// <param name="id"> A message ID. </param>
        /// <returns> A new outgoing message. </returns>
        protected NetOutgoingMessage CreateMessage(byte id)
        {
            return Actor.CreateMessage(this, id);
        }

        /// <summary>
        ///     Send a message.
        /// </summary>
        /// <param name="message"> The outgiong message. </param>
        /// <param name="method"> The delivery method. </param>
        /// <param name="connection">
        ///     Optional connection when sending from server to client. If null, sends to the client
        ///     that owns the current actor.
        /// </param>
        /// <returns> True if the message was sent successfully. </returns>
        protected bool SendMessage(NetOutgoingMessage message,
                                   NetDeliveryMethod method,
                                   IReplicaConnection connection = null)
        {
            return Actor.SendMessage(message, method, connection);
        }

        /// <summary>
        ///     Call a method on the client that owns the current actor. The target method must have the
        ///     ServerCallable attribute.
        /// </summary>
        protected bool InvokeClient(string methodName, bool reliable, params object[] args)
        {
            Debug.Assert(Actor.ServerSide);
            return Actor.SendRpc(this, methodName, args, reliable);
        }

        /// <summary>
        ///     Invoke a method on the server. The target method must have the ClientCallable attribute.
        /// </summary>
        protected bool InvokeServer(string methodName, bool reliable, params object[] args)
        {
            Debug.Assert(Actor.ClientSide);
            return Actor.SendRpc(this, methodName, args, reliable);
        }
    }
}