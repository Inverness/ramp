using Microsoft.Xna.Framework;
using Ramp.Network;

namespace Ramp.Simulation
{
    /// <summary>
    ///     Base class for components that are drawable. Providess the abstract Draw() method, and
    ///     adds the component to the level's drawables list.
    /// </summary>
    public abstract class Renderer : Behavior
    {
        private int _rendererListIndex;
        private byte? _layer;
        private readonly TransformComponent _transform;

        protected Renderer()
        {
            GetComponentRequired(out _transform);
            _layer = Actor.GetSpawnArgument<byte?>("RendererLayer");
            
            _rendererListIndex = Level.AddRenderer(this);
        }

        /// <summary>
        ///     The layer this renderer is drawn on. Out of range values will be clamped.
        /// </summary>
        public byte? Layer
        {
            get { return _layer; }

            set { _layer = value; }
        }

        public TransformComponent Transform => _transform;

        /// <summary>
        ///     Gets a bounding box in local coordinates containing all of the visual elements of this renderer. 
        /// </summary>
        public abstract BoundingBox LocalBoundingBox { get; }

        /// <summary>
        ///     The BoundingBox adjusted to account for the actor's location.
        /// </summary>
        public BoundingBox WorldBoundingBox
        {
            get
            {
                BoundingBox box = LocalBoundingBox;
                var pos = new Vector3(Transform.Location, 0);
                box.Min += pos;
                box.Max += pos;
                return box;
            }
        }

        /// <summary>
        ///     Called when it is time for the component to render itself.
        /// </summary>
        /// <param name="ri"> An interface with methods used to render the component. </param>
        public abstract void Draw(ILevelObjectRenderer ri);

        protected internal override void OnActorDisposing()
        {
            Level.RemoveRenderer(_rendererListIndex);

            base.OnActorDisposing();
        }

        public override void WriteDelta(DeltaWriter writer, ReplicationContext context)
        {
            base.WriteDelta(writer, context);
            writer.Write(_layer);
        }

        public override void ReadDelta(DeltaReader reader, ReplicationContext context)
        {
            base.ReadDelta(reader, context);
            reader.Read(ref _layer);
        }

        protected internal override void OnLevelChanging(Level newLevel)
        {
            Level.RemoveRenderer(_rendererListIndex);

            base.OnLevelChanging(newLevel);
        }

        protected internal override void OnLevelChanged()
        {
            base.OnLevelChanged();

            _rendererListIndex = Level.AddRenderer(this);
        }
    }
}