﻿namespace Ramp.Simulation.Tmx
{
    /// <summary>
    ///     Thrown when errors occur while verifying or preparing parsed data.
    /// </summary>
    public class TmxVerificationException : TmxException
    {
        public TmxVerificationException(string message = null)
            : base(message)
        {
        }
    }
}