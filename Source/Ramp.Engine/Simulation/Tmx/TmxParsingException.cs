﻿namespace Ramp.Simulation.Tmx
{
    /// <summary>
    ///     Thrown when errors occur while parsing a TMX file.
    /// </summary>
    public class TmxParsingException : TmxException
    {
        public TmxParsingException(string message = null)
            : base(message)
        {
        }
    }
}