﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using ICSharpCode.SharpZipLib.Zip.Compression;
using NLog;
using Microsoft.Xna.Framework;
using Newtonsoft.Json;
using Ramp.Simulation.Tiling;
using Ramp.Utilities.Collections;

namespace Ramp.Simulation.Tmx
{
    /// <summary>
    ///     Loads a TMX documented created with TilEd and builds a level from it.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public sealed class TmxLoader
    {
        // Parsed tile codes are 4 byte but used the first few bits to indicate tile transformations
        private const uint DFlipBit = 1U << 29;
        private const uint VFlipBit = 1U << 30;
        private const uint HFlipBit = 1U << 31;
        private const uint FlipBits = DFlipBit | VFlipBit | HFlipBit;

        private static readonly Logger s_log = LogManager.GetCurrentClassLogger();

        private int _currentVisualLayer;

        public TmxLoader()
        {
            TileLayers = new List<TmxTileLayer>();
            Tilesets = new List<TmxTileset>();
            Objects = new List<TmxObject>();
            SpawnArgs = new NameValueDictionary();
        }

        [JsonProperty]
        public string SourcePath { get; private set; }

        [JsonProperty]
        public Point Size { get; private set; }

        [JsonProperty]
        public Point SizeTiles { get; private set; }

        [JsonProperty]
        public List<TmxTileLayer> TileLayers { get; }

        [JsonProperty]
        public List<TmxTileset> Tilesets { get; }

        [JsonProperty]
        public List<TmxObject> Objects { get; }

        [JsonProperty]
        public NameValueDictionary SpawnArgs { get; }

        /// <summary>
        ///     Loads and parses a TMX document and stores information needed to build a level.
        /// </summary>
        public void Load(string path)
        {
            Validate.ArgumentNotNull(path, "doc");

            Clear();

            s_log.Debug("Loading TMX document: {0}", path);
            XDocument doc;
            using (Stream stream = File.OpenRead(path))
                doc = XDocument.Load(stream);

            // Verify that this is actually a TMX and that the orientation and size are valid
            XElement map = VerifyDocument(doc);

            // Get some basic properties
            SizeTiles = new Point((int) map.Attribute("width"), (int) map.Attribute("height"));
            Size = new Point(SizeTiles.X / 64, SizeTiles.Y / 64);
            s_log.Debug("Size: {0}, {1}", Size.X, Size.Y);

            foreach (XElement element in map.Elements())
            {
                if (element.Name == "layer")
                    ReadTileLayer(element);
                else if (element.Name == "objectgroup")
                    ReadObjectGroup(element);
                else if (element.Name == "tileset")
                    ReadTileset(element, path);
            }

            MatchLayersToTilesets();

            foreach (TmxTileLayer layer in TileLayers)
                EncodeTiles(layer);

            SourcePath = path;

            s_log.Debug("Finished load: {0} layers, {1} objects, {2} tilesets", TileLayers.Count, Objects.Count, Tilesets.Count);
        }

        public void UpdateTilesetTextures(string directory)
        {
            if (SourcePath == null)
                throw new InvalidOperationException("No level data loaded");

            foreach (TmxTileset tileset in Tilesets)
            {
                tileset.ImageSource = directory + Path.GetFileNameWithoutExtension(tileset.ImageSource);
            }
        }

        /// <summary>
        ///     Creates a level using the information previously loaded from the TMX document. This can be called
        ///     repeatedly to build multiple levels from the same data.
        /// </summary>
        /// <param name="world"> The world containing the level. </param>
        /// <param name="name"> The name of the new level. </param>
        /// <returns> A new level instance with tile layers added and static actors spawned. </returns>
        public Level CreateLevel(World world, string name)
        {
            Validate.ValidOperation(SourcePath != null, "No level data loaded");

            s_log.Debug("Creating level '{0}' in world '{1}'", name, world);

            var level = new Level(name, world, Size, true);
            world.AddLevel(level);

            try
            {
                level.DefaultRendererLayer = Objects.Min(o => o.VisualLayer);
                level.DefaultCollisionLayer = Objects.Min(o => o.CollisionLayer);

                s_log.Debug("Default renderer layer: {0}", level.DefaultRendererLayer);
                s_log.Debug("Default collision layer: {0}", level.DefaultCollisionLayer);

                s_log.Debug("Creating tile layers");

                CreateLevelTileLayers(level);

                s_log.Debug("Creating actors");

                CreateLevelObjects(level);

                level.EndInitialization();

                s_log.Debug("Finished creating {0}", level);
            }
            catch
            {
                world.RemoveLevel(level);
                throw;
            }

            return level;
        }

        /// <summary>
        ///     Clears all previously loaded data.
        /// </summary>
        public void Clear()
        {
            _currentVisualLayer = -1;
            SourcePath = null;
            Size = Point.Zero;
            SizeTiles = Point.Zero;
            TileLayers.Clear();
            Tilesets.Clear();
            Objects.Clear();
        }

        private void ReadTileLayer(XElement layerElement)
        {
            string layerName = (string) layerElement.Attribute("name");
            UpdateCurrentVisualLayer(layerName, true);

            s_log.Debug("Reading tile layer: {0}", layerName);

            var layer = new TmxTileLayer
            {
                Name = layerName,
                VisualLayer = (byte) _currentVisualLayer,
                Visible = (bool?) layerElement.Attribute("visible") ?? true,
                Opacity = (float?) layerElement.Attribute("opacity") ?? 1.0f,
                Props = ParseProperties(layerElement.Element("properties"))
            };

            XElement dataElement = layerElement.Element("data");
            if (dataElement == null)
                throw new TmxParsingException("expected data element in layer");

            layer.RawTiles = ReadTileData(dataElement);
            s_log.Debug("{0} tiles", layer.RawTiles.Length / 4);

            TileLayers.Add(layer);
        }

        private void ReadTileset(XElement tilesetElement, string path)
        {
            // offset determines how we adjust tile codes since only one tileset is allowed per layer in the game, so
            // all tile codes must be zero based. Do -1 to account for the transparent tile code.
            uint offset = ((uint) tilesetElement.Attribute("firstgid") - 1) / TileCode.PageSize;

            // Check to see if we need to load an external tileset
            var tilesetSource = (string) tilesetElement.Attribute("source");
            if (!string.IsNullOrEmpty(tilesetSource))
            {
                s_log.Debug("Loading external tileset: {0}", tilesetSource);

                string tilesetAssetName = Path.Combine(Path.GetDirectoryName(path), tilesetSource);
                using (Stream stream = File.OpenRead(tilesetAssetName))
                    tilesetElement = XElement.Load(stream);
            }

            string name = (string) tilesetElement.Attribute("name");
            s_log.Debug("Reading tileset: {0}", name);

            // Engine only accepts a specific tile size
            if ((uint) tilesetElement.Attribute("tilewidth") != 16 ||
                (uint) tilesetElement.Attribute("tileheight") != 16)
                throw new TmxParsingException("tilewidth and tileheight expected to be 16 for all tilesets");

            // The tileset image must be 512 pixels in height and a multiple of 256 in width to be processed
            XElement imageElement = tilesetElement.Element("image");
            if (imageElement == null)
                throw new TmxParsingException("image element expected");
            if ((uint) imageElement.Attribute("height") != 512)
                throw new TmxParsingException("tileset height must be 512 pixels");

            uint widthTiles = (uint) imageElement.Attribute("width") / 16;
            //string imageSource = tileTextureDirectory +
            //                     Path.GetFileNameWithoutExtension((string) imageElement.Attribute("source"));

            var tileset = new TmxTileset
            {
                Name = name,
                Offset = offset,
                Width = widthTiles,
                ImageSource = (string) imageElement.Attribute("source"),
                ImageSize = (ushort) (widthTiles / TileCode.PageWidth),
                Props = new Dictionary<uint, NameValueDictionary>()
            };

            // Get the properties for each tile code.
            foreach (XElement tileElement in tilesetElement.Elements("tile"))
            {
                var tileId = (uint) tileElement.Attribute("id");
                NameValueDictionary tileProperties = ParseProperties(tileElement.Element("properties"));
                if (tileProperties != null)
                    tileset.Props[tileId] = tileProperties;
            }

            Tilesets.Add(tileset);
        }


        private byte[] ReadTileData(XElement dataElement)
        {
            if ((string) dataElement.Attribute("encoding") != "base64")
                throw new TmxParsingException("expected data encoding: base64");
            if ((string) dataElement.Attribute("compression") != "zlib")
                throw new TmxParsingException("expected data compression: zlib");

            byte[] compressedData = Convert.FromBase64String(dataElement.Value.Trim());

            // each tile is 4 bytes
            var uncompressedData = new byte[SizeTiles.X * SizeTiles.Y * 4];
            var inflater = new Inflater();
            inflater.SetInput(compressedData);
            if (inflater.Inflate(uncompressedData) != uncompressedData.Length)
                throw new TmxParsingException("invalid compressed tile data; does not match level size");

            //// We'll get an exception for remaining unprocessed input if we don't reset the inflater here,
            //// because the compressed data has 4 unused bytes at the end (for unknown reasons).
            //inflater.Reset();

            // stripping the transform bits and converting them to 2-byte tile codes will come later
            return uncompressedData;
        }

        private void ReadObjectGroup(XElement element)
        {
            string layerName = (string) element.Attribute("name");
            UpdateCurrentVisualLayer(layerName, false);

            s_log.Debug("Reading object group: {0}", layerName);

            Objects.AddRange(element.Elements("object").Select(ReadObject));
        }

        private TmxObject ReadObject(XElement element)
        {
            byte visualLayer = (byte) Math.Max(_currentVisualLayer, 0);
            byte collisionLayer = (byte) (TileLayers.Count != 0 ? TileLayers.Count - 1 : 0);

            var obj = new TmxObject
            {
                Name = (string) element.Attribute("name"),
                Type = (string) element.Attribute("type"),
                X = (float) element.Attribute("x") / 16,
                Y = (float) element.Attribute("y") / 16,
                Width = ((float?) element.Attribute("width") ?? 0) / 16,
                Height = ((float?) element.Attribute("height") ?? 0) / 16,
                VisualLayer = visualLayer,
                CollisionLayer = collisionLayer,
                Props = ParseProperties(element.Element("properties"))
            };

            // Handle special objects
            if (obj.Type?.EqualsIgnoreCase("#Blocking") ?? false)
            {
                XElement poly;
                if ((poly = element.Element("polyline")) != null)
                {
                    obj.ShapeType = TmxShapeType.Polyline;
                    obj.Points = ParsePoints(poly);
                }
                else if ((poly = element.Element("polygon")) != null)
                {
                    obj.ShapeType = TmxShapeType.Polygon;
                    obj.Points = ParsePoints(poly);
                }
                else if (element.Element("ellipse") != null)
                {
                    obj.ShapeType = TmxShapeType.Ellipse;
                }
                else
                {
                    obj.ShapeType = TmxShapeType.Rectangle;
                }
            }

            return obj;
        }

        private static Vector2[] ParsePoints(XElement element)
        {
            string stringPoints = (string) element.Attribute("points");
            string[] stringValues = stringPoints.Split(',', ' ');

            var points = new List<Vector2>();

            float x = 0, y = 0;
            int c = 0;

            foreach (string stringValue in stringValues)
            {
                switch (c)
                {
                    case 0:
                        x = float.Parse(stringValue) / 16f;
                        c = 1;
                        break;
                    case 1:
                        y = float.Parse(stringValue) / 16f;
                        points.Add(new Vector2(x, y));
                        c = 0;
                        break;
                }
            }

            return points.ToArray();
        }

        //private static TmxLine[] ParsePolyline(float ox, float oy, XElement element)
        //{
        //    string points = (string) element.Attribute("points");
        //    string[] stringValues = points.Split(',', ' ');

        //    var lines = new List<TmxLine>();

        //    float sx = 0, sy = 0, ex = 0, ey = 0;
        //    int c = 0;

        //    foreach (string stringValue in stringValues)
        //    {
        //        switch (c)
        //        {
        //            case 0:
        //                sx = float.Parse(stringValue) / 16f;
        //                break;
        //            case 1:
        //                sy = float.Parse(stringValue) / 16f;
        //                break;
        //            case 2:
        //                ex = float.Parse(stringValue) / 16f;
        //                break;
        //            case 3:
        //                ey = float.Parse(stringValue) / 16f;

        //                lines.Add(new TmxLine(ox + sx, oy + sy, ox + ex, oy + ey));

        //                c = 1;
        //                sx = ex;
        //                sy = ey;
        //                break;
        //        }
        //        c++;
        //    }

        //    return lines.ToArray();
        //}

        private void MatchLayersToTilesets()
        {
            s_log.Debug("Matching layers to tilesets");

            TmxTileset defaultTileset = GetTileset("Default");
            //if (defaultTileset == null)
            //    Log.Warn("no default tileset specified");

            foreach (TmxTileLayer layer in TileLayers)
            {
                if (layer.Props == null)
                {
                    layer.TilesetInfo = defaultTileset;
                    s_log.Debug("  {0}: {1}", layer.Name, defaultTileset.Name);
                    continue;
                }

                // The "Tileset" option specifies the name of the tileset to use for the layer
                var name = layer.Props.GetValue<string>("Tileset");

                if (name == null)
                {
                    layer.TilesetInfo = defaultTileset;
                }
                else
                {
                    layer.TilesetInfo = GetTileset(name);
                    if (layer.TilesetInfo == null)
                        throw new TmxVerificationException($"no matching tileset {name} found for layer {layer.Name}");
                }

                s_log.Debug("  {0}: {1}", layer.Name, layer.TilesetInfo.Name);
            }
        }

        private void EncodeTiles(TmxTileLayer layer)
        {
            Debug.Assert(layer.TilesetInfo != null);

            var tileArray = new TileArray(SizeTiles.X, SizeTiles.Y);
            byte[] rawTiles = layer.RawTiles;
            uint offset = layer.TilesetInfo.Offset;
            uint tilesetWidthTiles = layer.TilesetInfo.Width;
            var levelWidth = (uint) SizeTiles.X;

            for (uint i = 0, e = 0; i < rawTiles.Length; i += 4, e++)
            {
                // ToUInt32 simply casts ToInt32 result 
                uint tile = (uint) BitConverter.ToInt32(rawTiles, (int) i) & ~FlipBits;

                var x = (ushort) (e % levelWidth);
                var y = (ushort) (e / levelWidth);

                if (tile == 0)
                {
                    tileArray[x, y] = 0;
                }
                else
                {
                    // add +1 to account for transparent tile code 0
                    tile -= offset * TileCode.PageSize + 1;

                    tileArray[x, y] = TileCode.Encode((ushort) (tile % tilesetWidthTiles),
                                                      (ushort) (tile / tilesetWidthTiles));
                }
            }

            layer.RawTiles = null; // no longer need these, so let them be GCed
            layer.TileArray = tileArray;

            // Replace tile properties dictionary with a new one with encoded tile code keys
            var newProps = new Dictionary<uint, NameValueDictionary>();
            foreach (KeyValuePair<uint, NameValueDictionary> props in layer.TilesetInfo.Props)
            {
                uint tile = TileCode.Encode((ushort) (props.Key % tilesetWidthTiles),
                                            (ushort) (props.Key / tilesetWidthTiles));

                newProps[tile] = props.Value;
            }

            layer.TilesetInfo.Props = newProps;
        }

        private TmxTileset GetTileset(string name)
        {
            return Tilesets.FirstOrDefault(x => x.Name.EqualsIgnoreCase(name));
        }

        private void CreateLevelTileLayers(Level level)
        {
            // Just assume the topmost tile layer is for collision. This is sort-of hackish.
            TileMap tileMap = level.TileMap;

            foreach (TmxTileLayer tmxLayer in TileLayers)
            {
                TileLayer levelLayer = tileMap.CreateLayer(tmxLayer.VisualLayer, tmxLayer.Name);
                levelLayer.IsHidden = !tmxLayer.Visible;

                var color = Color.White;
                color.A = (byte) (tmxLayer.Opacity * byte.MaxValue);
                levelLayer.Color = color;

                //if (tmxLayer.VisualLayer == level.DefaultCollisionLayer)
                //    levelLayer.IsHidden = true;

                levelLayer.Tileset = new Tileset(tmxLayer.TilesetInfo.ImageSize, tmxLayer.TilesetInfo.ImageSource);

                //if (levlayer.Name.Equals("collision", StringComparison.InvariantCultureIgnoreCase))
                //{
                //    level.DefaultCollisionLayer = levlayer.Index;
                //    levlayer.IsHidden = true;
                //}

                // Copy tile data into the level
                for (int y = 0; y < tileMap.Size.Y; y++)
                {
                    for (int x = 0; x < tileMap.Size.X; x++)
                    {
                        TileArray tileArray = levelLayer.GetArray(x, y);
                        tileArray.Copy(tmxLayer.TileArray, Point.Zero, new Rectangle(x * 64, y * 64, 64, 64));
                    }
                }

                // Apply tile properties
                foreach (KeyValuePair<uint, NameValueDictionary> props in tmxLayer.TilesetInfo.Props)
                {
                    var blocking = props.Value.GetValue<bool>("Blocking");
                    if (blocking)
                        levelLayer.Tileset.Properties[(int) props.Key] |= TileProperty.Blocking;
                }
            }
        }

        private void CreateLevelObjects(Level level)
        {
            if (Objects.Count == 0)
                return;

            foreach (TmxObject tmxObject in Objects)
            {
                if (tmxObject.Type?.StartsWith("#") ?? false)
                {
                    // special object handling

                    if (tmxObject.Type.EqualsIgnoreCase("#Blocking"))
                        CreateLevelCollisionGeometry(level, tmxObject);
                }
                else
                {
                    CreateLevelObject(level, tmxObject);
                }
            }
        }

        private void CreateLevelObject(Level level, TmxObject tobj)
        {
            string type = string.IsNullOrWhiteSpace(tobj.Type) ? null : tobj.Type;
            var location = new Vector2(tobj.X, tobj.Y);
            var size = new Vector2(tobj.Width, tobj.Height);

            if (tobj.Props != null)
                SpawnArgs.Update(tobj.Props);

            // In the game, the actor's position should be the center of its bounding box and/or sprite. This presents
            // a problem because TilEd defines objects in boxes with the upper left corner being the position.
            // It is also possible to set the size of a TilED object to zero for point objects.
            //
            // Objects in TilEd defined with a size will be specially handled here. The actor's position will be moved
            // to the center of the box defined in TilEd, while the size of the box will be specified in spawn arguments
            // for use by colliders or renderers attached to the actor.
            if (size != Vector2.Zero)
            {
                location += size / 2;

                CheckIgnoredSpawnArg("RendererSize", tobj);
                CheckIgnoredSpawnArg("ColliderSize", tobj);
                CheckIgnoredSpawnArg("ColliderRadius", tobj);

                SpawnArgs["RendererSize"] = size;
                SpawnArgs["ColliderSize"] = size;
                SpawnArgs["ColliderRadius"] = size.X / 2;
            }

            // TODO: Support individual component types from level object?

            // Specify the rendering layer
            SpawnArgs["RendererLayer"] = tobj.VisualLayer;
            SpawnArgs["ColliderLayer"] = tobj.CollisionLayer;

            level.SpawnActor(location, type, name: tobj.Name, args: SpawnArgs);

            SpawnArgs.Clear();
        }

        private void CheckIgnoredSpawnArg(string name, TmxObject obj)
        {
            if (SpawnArgs.ContainsKey(name))
                s_log.Warn("Ignored property {0} defined in {1} at {2},{3}", name, obj.Name ?? "unnamed object",
                                 obj.X, obj.Y);
        }

        private void CreateLevelCollisionGeometry(Level level, TmxObject tobj)
        {
            // Rectangle and ellipse origin is in the center, so the half size is added to location
            switch (tobj.ShapeType)
            {
                case TmxShapeType.Rectangle:
                    level.TileMap.AddCollisionRectangle(tobj.CollisionLayer,
                                                        true,
                                                        new Vector2(tobj.X + tobj.Width / 2, tobj.Y + tobj.Height / 2),
                                                        new Vector2(tobj.Width, tobj.Height));
                    break;
                case TmxShapeType.Ellipse:
                    level.TileMap.AddCollisionEllipse(tobj.CollisionLayer,
                                                      true,
                                                      new Vector2(tobj.X + tobj.Width / 2, tobj.Y + tobj.Height / 2),
                                                      new Vector2(tobj.Width / 2, tobj.Height / 2));
                    break;
                case TmxShapeType.Polygon:
                    level.TileMap.AddCollisionPolygon(tobj.CollisionLayer,
                                                      true,
                                                      new Vector2(tobj.X, tobj.Y),
                                                      tobj.Points);
                    break;
                case TmxShapeType.Polyline:
                    Debug.Assert(tobj.Points != null);
                    if (tobj.Points.Length == 2)
                    {
                        level.TileMap.AddCollisionEdge(tobj.CollisionLayer,
                                                       true,
                                                       new Vector2(tobj.X, tobj.Y), 
                                                       tobj.Points[0],
                                                       tobj.Points[1]);
                    }
                    else
                    {
                        level.TileMap.AddCollisionChain(tobj.CollisionLayer,
                                                        true,
                                                        new Vector2(tobj.X, tobj.Y), 
                                                        tobj.Points);
                    }
                    break;
            }
        }

        private static XElement VerifyDocument(XDocument doc)
        {
            XElement map = doc.Element("map");
            if (map == null)
                throw new TmxParsingException("Missing root map element");
            if ((string) map.Attribute("version") != "1.0")
                throw new TmxParsingException("unknown TMX file version, expected 1.0");
            if ((string) map.Attribute("orientation") != "orthogonal")
                throw new TmxParsingException("invalid orientation, expected orthogonal");
            if ((uint) map.Attribute("width") % 64 != 0 || (uint) map.Attribute("height") % 64 != 0)
                throw new TmxParsingException("invalid map size, expected width and height to be multiples of 64");
            if ((uint) map.Attribute("tilewidth") != 16 || (uint) map.Attribute("tileheight") != 16)
                throw new TmxParsingException("invalid tile size, expected tile width and height of 16");
            return map;
        }

        private void UpdateCurrentVisualLayer(string name, bool increment)
        {
            byte visualLayer;
            if (ParseLayerName(ref name, out visualLayer))
            {
                _currentVisualLayer = visualLayer;
            }
            else if (increment)
            {
                _currentVisualLayer = checked(_currentVisualLayer + 1);
            }
        }

        private static bool ParseLayerName(ref string name, out byte index)
        {
            int spaceIndex = name.IndexOf(' ');

            byte indexValue;
            if (spaceIndex == -1 || !byte.TryParse(name.Substring(0, spaceIndex), out indexValue))
            {
                index = 0;
                return false;
            }
            index = indexValue;
            name = name.Substring(spaceIndex + 1);
            return true;
        }

        private static NameValueDictionary ParseProperties(XElement element)
        {
            if (element == null || !element.HasElements)
                return null;

            var properties = new NameValueDictionary();
            foreach (XElement propertyElement in element.Elements("property"))
                properties.Add((string) propertyElement.Attribute("name"), propertyElement.Attribute("value").Value);

            return properties;
        }

        private static IEnumerable<Type> ParseComponentTypeNames(string text)
        {
            string[] typeNames = text.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            return typeNames.Select(typeName => GetClrTypeChecked(typeName.Trim()));
        }

        private static Type GetClrTypeChecked(string typeName)
        {
            ActorComponentType ct = ActorComponentType.GetType(typeName);
            if (ct == null)
                throw new ArgumentException("Invalid component type name: " + typeName, nameof(typeName));
            return ct.ClrType;
        }
    }

    public enum TmxShapeType
    {
        Rectangle,
        Ellipse,
        Polyline,
        Polygon
    }

    public class TmxObject
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public float Width { get; set; }
        public float Height { get; set; }
        public float X { get; set; }
        public float Y { get; set; }
        public byte VisualLayer { get; set; }
        public byte CollisionLayer { get; set; }
        public NameValueDictionary Props { get; set; }
        public TmxShapeType ShapeType { get; set; }
        public Vector2[] Points { get; set; }
    }

    public class TmxTileLayer
    {
        public byte VisualLayer { get; set; }
        public string Name { get; set; }
        public float Opacity { get; set; }
        public NameValueDictionary Props { get; set; }
        public byte[] RawTiles { get; set; }
        public TileArray TileArray { get; set; }
        public TmxTileset TilesetInfo { get; set; }
        public bool Visible { get; set; }
    }

    public class TmxTileset
    {
        public ushort ImageSize { get; set; } // image size in pages
        public string ImageSource { get; set; }
        public string Name { get; set; }
        public uint Offset { get; set; }
        public Dictionary<uint, NameValueDictionary> Props { get; set; }
        public uint Width { get; set; } // in tiles
    }
}