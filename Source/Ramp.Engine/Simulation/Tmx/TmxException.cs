﻿using System;

namespace Ramp.Simulation.Tmx
{
    public class TmxException : Exception
    {
        public TmxException(string message = null)
            : base(message)
        {
        }
    }
}