using System;
using System.Collections;
using System.Diagnostics;
using System.Threading.Tasks;
using NLog;
using Microsoft.Xna.Framework;

namespace Ramp.Simulation
{
    /// <summary>
    ///     A link that, when activated by overlapping or script, moves the activator to another level.
    /// </summary>
    public class LevelLinkBehavior : Behavior, IActivatable
    {
        private static readonly Logger s_log = LogManager.GetCurrentClassLogger();

        private readonly Collider _collider;

        public LevelLinkBehavior()
        {
            GetComponentRequired(out _collider);
            _collider.ActorOverlapBegan += OnActorOverlapBegan;

            TargetName = Actor.GetSpawnArgument<string>("LinkLevel");
            TargetPosition = Actor.GetSpawnArgument<Vector2>("LinkPosition");
        }

        /// <summary>
        ///     Name of the target level.
        /// </summary>
        public string TargetName { get; set; }

        /// <summary>
        ///     Target position to place the actor in.
        /// </summary>
        public Vector2 TargetPosition { get; set; }

        protected virtual void OnActorOverlapBegan(object sender, ActorCollisionEventArgs e)
        {
            Activate(e.Other.Actor);
        }

        /// <summary>
        ///     Activate the level link, causing the target to be moved to the destination level and position.
        /// </summary>
        public virtual void Activate(Actor target)
        {
            if (!Enabled || target == Actor || !target.CanChangeLevels)
                return;

            Debug.Assert(!target.IsLevelChanging, "!target.IsLevelChanging");

            target.ChangeLevel(TargetName, TargetPosition);
        }
    }
}