namespace Ramp.Simulation
{
    /// <summary>
    ///     Defines how the input vector affects movement.
    /// </summary>
    public enum InputVectorMode : byte
    {
        /// <summary>
        ///     The input vector modifies velocity by its exact value.
        /// </summary>
        Constant,

        /// <summary>
        ///     The input vector is applied as an impulse each frame.
        /// </summary>
        Impulse,

        /// <summary>
        ///     The input vector is applied to acceleration each frame.
        /// </summary>
        Acceleration
    }
}