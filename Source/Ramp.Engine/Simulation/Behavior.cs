using System;
using System.Diagnostics;

namespace Ramp.Simulation
{
    /// <summary>
    ///     An implementation of IBehavior. A behavior is a component that can be enabled and disabled.
    /// </summary>
    public abstract class Behavior : ActorComponent, IBehavior
    {
        private bool _enabled;

        protected Behavior()
        {
            _enabled = Actor.GetSpawnArgument("Enabled", true);
        }

        /// <summary>
        ///     Raised when the value of Enabled changes.
        /// </summary>
        public event TypedEventHandler<IBehavior, EventArgs> EnabledChanged;

        /// <summary>
        ///     Set the enabled state of the component.
        ///     Calls OnEnabledChanged() after the change is made.
        /// </summary>
        public bool Enabled
        {
            get { return _enabled; }

            set
            {
                if (_enabled == value)
                    return;
                _enabled = value;
                OnEnabledChanged();
            }
        }

        /// <summary>
        ///     Raise an InvalidOperationException if this behavior is not enabled.
        /// </summary>
        [Conditional("DEBUG"), Conditional("VALIDATE")]
        protected void VerifyEnabled()
        {
            VerifyNotDisposed();
            if (!Enabled)
                throw new InvalidOperationException("Behavior not enabled.");
        }

        /// <summary>
        ///     Called when Enabled changes. Raises the EnabledChanged event.
        /// </summary>
        protected virtual void OnEnabledChanged()
        {
            EnabledChanged?.Invoke(this, EventArgs.Empty);
        }

        /// <summary>
        ///     Disables all behaviors of an actor. Returns an array of states that can be provided to Enable() to
        ///     ensure that only previously enabled behaviors are enabled.
        /// </summary>
        /// <param name="actor"> The actor. </param>
        /// <returns>An array containing the current states of the behaviors.</returns>
        public static bool[] Disable(Actor actor)
        {
            if (actor == null)
                throw new ArgumentNullException(nameof(actor));

            var states = new bool[actor.ComponentCount];
            int i = 0;
            foreach (Behavior behavior in actor.GetComponents<Behavior>())
            {
                states[i++] = behavior.Enabled;
                behavior.Enabled = false;
            }
            Array.Resize(ref states, i);
            return states;
        }

        /// <summary>
        ///     Enables all behaviors of an actor.
        /// </summary>
        /// <param name="actor"> The actor. </param>
        public static void Enable(Actor actor)
        {
            Validate.ArgumentNotNull(actor, nameof(actor));
            
            foreach (Behavior behavior in actor.GetComponents<Behavior>())
                behavior.Enabled = true;
        }

        /// <summary>
        ///     Enables all behaviors of an actor.
        /// </summary>
        /// <param name="actor"> The actor. </param>
        /// <param name="states"> The states provided by Disable(). </param>
        public static void Enable(Actor actor, bool[] states)
        {
            Validate.ArgumentNotNull(actor, nameof(actor));
            Validate.ArgumentNotNull(states, nameof(states));

            int i = 0;
            foreach (Behavior behavior in actor.GetComponents<Behavior>())
            {
                // Behaviors could have been added to the actor since Disable() was called
                if (i >= states.Length)
                    break;

                behavior.Enabled = states[i++];
            }
        }
    }
}