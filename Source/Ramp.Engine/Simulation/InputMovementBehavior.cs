﻿using System;
using Microsoft.Xna.Framework;
using Ramp.Network;

namespace Ramp.Simulation
{
    /// <summary>
    ///     A movement behavior that supports more typical input from a player or AI.
    /// </summary>
    public sealed class InputMovementBehavior : MovementBehavior
    {
        private Vector2 _inputVector;
        private Vector2 _lastConstantInputVector;
        private InputVectorMode _inputVectorMode;
        private float _inputSpeed;

        public InputMovementBehavior()
        {
            _inputVectorMode = Actor.GetSpawnArgument("InputVectorMode", InputVectorMode.Constant);
            _inputSpeed = Actor.GetSpawnArgument("InputSpeed", 8f);
        }

        /// <summary>
        ///     Gets or sets the input vector, which describes how a controller is applying direction. The vector
        ///     is clamped to a length of 1.
        /// </summary>
        public Vector2 InputVector
        {
            get { return _inputVector; }

            set { _inputVector = MovementUtility.ClampLength(value, 1); }
        }

        /// <summary>
        ///     Gets or sets the direction of input. This is a wrapper around MovementUtility.GetDirection() and
        ///     MovementUtility.GetVector().
        /// </summary>
        public Direction InputDirection
        {
            get { return MovementUtility.GetDirection(_inputVector); }

            set { _inputVector = MovementUtility.GetVector(value); }
        }

        /// <summary>
        ///     Gets or sets the input vector mode, which describes how the input vector is applied to velocity.
        /// </summary>
        public InputVectorMode InputVectorMode
        {
            get { return _inputVectorMode; }

            set { _inputVectorMode = value; }
        }

        /// <summary>
        ///     Gets or sets the speed that is multiplied with the input vector.
        /// </summary>
        public float InputSpeed
        {
            get { return _inputSpeed; }

            set { _inputSpeed = value; }
        }

        protected override void UpdateVelocity(TimeSpan elapsed)
        {
            // Apply the input vector.
            // TODO: InputVector into some sort of controller
            if (Actor.Role >= NetRole.Autonomous)
            {
                Vector2 finalInputVector = _inputVector * _inputSpeed;
                switch (_inputVectorMode)
                {
                    case InputVectorMode.Constant:
                        AddImpulse(-_lastConstantInputVector + finalInputVector);
                        _lastConstantInputVector = finalInputVector;
                        break;
                    case InputVectorMode.Impulse:
                        AddImpulse(finalInputVector);
                        break;
                    case InputVectorMode.Acceleration:
                        Acceleration += finalInputVector;
                        break;
                }
            }

            base.UpdateVelocity(elapsed);
        }

        public override void WriteDelta(DeltaWriter writer, ReplicationContext context)
        {
            base.WriteDelta(writer, context);

            if (Actor.RemoteRole != NetRole.Autonomous)
            {
                writer.Write((byte) _inputVectorMode);
                writer.Write(_inputVector);
                writer.Write(_inputSpeed);
            }
        }

        public override void ReadDelta(DeltaReader reader, ReplicationContext context)
        {
            base.ReadDelta(reader, context);

            if (Actor.Role != NetRole.Autonomous)
            {
                reader.ReadEnumByte(ref _inputVectorMode);
                reader.Read(ref _inputVector);
                reader.Read(ref _inputSpeed);
            }
        }
    }
}