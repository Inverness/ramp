using System;

namespace Ramp.Simulation
{
    [Serializable]
    public class ActorComponentNotFoundException : Exception
    {
        public ActorComponentNotFoundException(Type requiredType = null, string message = null)
            : base(message)
        {
            RequiredType = requiredType;
        }

        public Type RequiredType { get; private set; }
    }
}