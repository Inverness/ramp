using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Newtonsoft.Json;
using Ramp.Threading;
using nkast.Aether.Physics2D.Collision.Shapes;
using nkast.Aether.Physics2D.Common;
using nkast.Aether.Physics2D.Dynamics;

namespace Ramp.Simulation.Tiling
{
    /// <summary>
    ///     Represents a level with world geometry consisting of tiles.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public sealed class TileMap : DispatcherObject
    {
        /// <summary>
        ///     Maximum number of tile layers allowed.
        /// </summary>
        public const byte MaxTileLayers = 64;

        /// <summary>
        ///     Width and height of a tile group.
        /// </summary>
        public const byte GroupSize = 64;

        [JsonProperty("Size")]
        private readonly Point _size;

        [JsonProperty("Layers")]
        private readonly List<TileLayer> _layers = new List<TileLayer>();

        private readonly nkast.Aether.Physics2D.Dynamics.World _physicsWorld;

        public TileMap(Point size, nkast.Aether.Physics2D.Dynamics.World physicsWorld)
        {
            Validate.ArgumentInRange(!(size.X == 0 && size.Y == 0), nameof(size), "size must not be zero");
            Validate.ArgumentNotNull(physicsWorld, nameof(physicsWorld));

            _size = size;
            _physicsWorld = physicsWorld;
        }

        /// <summary>
        ///     The size of the level in tile groups.
        /// </summary>
        public Point Size => _size;

        /// <summary>
        ///     Tile layer metadata.
        /// </summary>
        public IReadOnlyList<TileLayer> Layers => _layers;

        public void Clear()
        {
            _layers.Clear();
        }

        /// <summary>
        ///     Create a new tile layer.
        /// </summary>
        /// <param name="visualLayer"> The visual layer index. </param>
        /// <param name="name"> The name of the new layer. </param>
        /// <param name="deftile"> The default tile to initialize the layer to. </param>
        /// <returns> A new TileLayer instance. </returns>
        public TileLayer CreateLayer(byte visualLayer, string name, ushort deftile = 0)
        {
            if (_layers.Any(l => l.Index == visualLayer))
                throw new ArgumentException("a layer already exists with the specified visual layer", nameof(visualLayer));

            if (_layers.Count >= MaxTileLayers)
                throw new InvalidOperationException("Maximum number of layers reached");

            var layer = new TileLayer(this, (byte) _layers.Count, visualLayer, name, _size, deftile);
            _layers.Add(layer);

            return layer;
        }

        public TileLayer GetLayer(byte index)
        {
            return index < _layers.Count ? _layers[index] : null;
        }

        /// <summary>
        ///     Get the tile layer with the specified visual layer.
        /// </summary>
        /// <param name="visualLayer"> A visual layer index. </param>
        /// <returns> A matching tile layer, or null. </returns>
        public TileLayer GetVisualLayer(byte visualLayer)
        {
            for (int i = 0; i < _layers.Count; i++)
            {
                if (_layers[i].VisualLayer == visualLayer)
                    return _layers[i];
            }

            return null;
        }

        //public bool IsBlocking(Point point, byte layerIndex)
        //{
        //    TileLayer layer = _layers[layerIndex];
        //    if (layer.Tileset == null)
        //        return false;

        //    if (point.X < 0 || point.Y < 0 || point.X >= (_size.X * GroupSize) || point.Y >= (_size.Y * GroupSize))
        //        return false;

        //    TileArray tileArray = layer.GetArray(point.X / GroupSize, point.Y / GroupSize);
        //    ushort code = tileArray[point.X % GroupSize, point.Y % GroupSize];

        //    TileProperty[] properties = layer.Tileset.Properties;
        //    if (code >= properties.Length)
        //        return false;

        //    return (properties[code] & TileProperty.Blocking) != 0;
        //}

        public void AddCollisionRectangle(byte layerIndex, bool blocking, Vector2 location, Vector2 size)
        {
            Vector2 halfSizeMeters = PhysicsSettings.FromTiles(size) / 2;

            Vertices vertices = PolygonTools.CreateRectangle(halfSizeMeters.X, halfSizeMeters.Y);
            
            AddCollisionShape(layerIndex, blocking, location, new PolygonShape(vertices, 0));
        }

        public void AddCollisionEllipse(byte layerIndex, bool blocking, Vector2 location, Vector2 radius)
        {
            Vector2 radiusMeters = PhysicsSettings.FromTiles(radius);
            
            Shape shape;
            // ReSharper disable once CompareOfFloatsByEqualityOperator
            if (radiusMeters.X == radiusMeters.Y)
            {
                shape = new CircleShape(radiusMeters.X, 0);
            }
            else
            {
                int edges = Math.Max(12, (int) (radius.X + radius.Y)); // ballpark estimate

                Vertices vertices = PolygonTools.CreateEllipse(radiusMeters.X, radiusMeters.Y, edges);

                shape = new PolygonShape(vertices, 0);
            }
            
            AddCollisionShape(layerIndex, blocking, location, shape);
        }

        public void AddCollisionEdge(byte layerIndex, bool blocking, Vector2 location, Vector2 begin, Vector2 end)
        {
            AddCollisionShape(layerIndex, blocking, location, new EdgeShape(PhysicsSettings.FromTiles(begin),
                                                                            PhysicsSettings.FromTiles(end)));
        }

        public void AddCollisionChain(byte layerIndex, bool blocking, Vector2 location, ICollection<Vector2> points, bool loop = false)
        {
            Validate.ArgumentNotNull(points, "points");

            var vertices = new Vertices(points);

            Matrix transform;
            Matrix.CreateScale(PhysicsSettings.TilesToMetersMultiplier, out transform);
            vertices.Transform(ref transform);
            
            AddCollisionShape(layerIndex, blocking, location, new ChainShape(vertices, loop));
        }

        public void AddCollisionPolygon(byte layerIndex, bool blocking, Vector2 location, ICollection<Vector2> points)
        {
            Validate.ArgumentNotNull(points, "points");

            var vertices = new Vertices(points);

            Matrix transform;
            Matrix.CreateScale(PhysicsSettings.TilesToMetersMultiplier, out transform);
            vertices.Transform(ref transform);

            AddCollisionShape(layerIndex, blocking, location, new PolygonShape(vertices, 0));
        }

        public void AddCollisionShape(byte layerIndex, bool blocking, Vector2 location, Shape shape)
        {
            Validate.ArgumentInRange(layerIndex < _layers.Count, nameof(layerIndex));
            Validate.ArgumentNotNull(shape, nameof(shape));
            
            var body = _physicsWorld.CreateBody(PhysicsSettings.FromTiles(location), 0, BodyType.Static);
            body.Tag = _layers[layerIndex];
            Fixture fixture = body.CreateFixture(shape);
            fixture.Tag = _layers[layerIndex];
            fixture.CollidesWith = PhysicsSettings.WorldStaticCategory;
            fixture.CollisionCategories = PhysicsSettings.WorldStaticCategory;
            fixture.IsSensor = !blocking;
        }
    }
}