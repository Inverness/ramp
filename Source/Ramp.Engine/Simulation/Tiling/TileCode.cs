namespace Ramp.Simulation.Tiling
{
    /// <summary>
    ///     Provides tile encoding and decoding.
    /// </summary>
    /// <remarks>
    ///     Tile codes are organized into pages that allow for future increases to tileset size without the need
    ///     to change the code of existing tiles.
    /// </remarks>
    public static class TileCode
    {
        /// <summary>
        ///     Width of a tile page in tiles.
        /// </summary>
        public const byte PageWidth = 16;

        /// <summary>
        ///     Height of a tile page in tiles.
        /// </summary>
        public const byte PageHeight = 32;

        /// <summary>
        ///     Total number of tiles per tile page.
        /// </summary>
        public const ushort PageSize = PageWidth * PageHeight;

        /// <summary>
        ///     Encode a tile coordinate.
        /// </summary>
        /// <param name="x"> The X position of the tile. </param>
        /// <param name="y"> The Y position of the tile. </param>
        /// <returns> A non-zero tile code. </returns>
        public static ushort Encode(ushort x, ushort y)
        {
            Validate.ArgumentInRange(y < PageHeight, "y", "y must be less than PageHeight");

            x = (ushort) ((x % PageWidth) + ((x / PageWidth) * PageSize));
            y = (ushort) ((y * PageWidth) % PageSize);

            return (ushort) (x + y + 1); // add one since zero is used for transparent tiles
        }

        /// <summary>
        ///     Decode a tile code.
        /// </summary>
        /// <param name="code"> The tile code. </param>
        /// <param name="x"> The X position of the tile. </param>
        /// <param name="y"> The Y position of the tile. </param>
        public static void Decode(ushort code, out ushort x, out ushort y)
        {
            Validate.ArgumentInRange(code != 0, "code must be non-zero");

            code -= 1;
            x = (ushort) ((code % PageWidth) + ((code / PageSize) * PageWidth));
            y = (ushort) ((code / PageWidth) % PageHeight);
        }
    }
}