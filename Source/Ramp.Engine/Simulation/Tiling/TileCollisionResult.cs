﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace Ramp.Simulation.Tiling
{
    /// <summary>
    ///     Used to store the results of a tile collision test.
    /// </summary>
    public struct TileCollisionResult
    {
        /// <summary>
        ///     The collider that instigated the collision test.
        /// </summary>
        public Collider Instigator;

        /// <summary>
        ///     The destination bounding box.
        /// </summary>
        public BoundingBox Destination;

        /// <summary>
        ///     True if the bounds of the level were hit.
        /// </summary>
        public bool HitBounds;

        /// <summary>
        ///     True if all tile collisions were tested or just the first.
        /// </summary>
        public bool AllTiles;

        /// <summary>
        ///     A list of tiles that were collided with.
        /// </summary>
        public List<TileCollisionInfo> Tiles;

        public bool BlockingHit => HitBounds || Tiles.Count != 0;

        public void Reset()
        {
            Instigator = null;
            Destination = default(BoundingBox);
            HitBounds = false;
            AllTiles = false;

            if (Tiles == null)
                Tiles = new List<TileCollisionInfo>();
            else
                Tiles.Clear();
        }
    }
}