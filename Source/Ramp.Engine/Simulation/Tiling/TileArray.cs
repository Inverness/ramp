﻿using System;
using Microsoft.Xna.Framework;
using Newtonsoft.Json;

namespace Ramp.Simulation.Tiling
{
    /// <summary>
    ///     A two dimensional array of tiles. Tiles are stored in a row-major format.
    /// </summary>
    [JsonConverter(typeof(TileArrayConverter))]
    public sealed class TileArray
    {
        private readonly ushort[] _tiles;
        private readonly int _width;
        private readonly int _height;

        /// <summary>
        ///     Initialize a new empty TileArray instance.
        /// </summary>
        public TileArray()
            : this(0, 0)
        {
        }

        /// <summary>
        ///     Initialize a new TileArray instance.
        /// </summary>
        /// <param name="width">Width of tile array.</param>
        /// <param name="height">Height of tile array.</param>
        /// <param name="defaultTile">A default value to initialize all tiles to.</param>
        public TileArray(int width, int height, ushort defaultTile = 0)
        {
            if (width < 0)
                throw new ArgumentOutOfRangeException(nameof(width));
            if (height < 0)
                throw new ArgumentOutOfRangeException(nameof(height));

            _width = width;
            _height = height;
            _tiles = new ushort[width * height];

            if (defaultTile != 0)
            {
                for (uint i = 0; i < _tiles.Length; i++)
                    _tiles[i] = defaultTile;
            }
        }

        /// <summary>
        ///     Initialize a new TileArray instance.
        /// </summary>
        /// <param name="width">Width of tile array.</param>
        /// <param name="height">Height of tile array.</param>
        /// <param name="tiles">An array of tile codes.</param>
        public TileArray(int width, int height, ushort[] tiles)
            : this(width, height, tiles, true)
        {
        }

        // Used by deserializer to provide an existing ushort[] without copying it
        internal TileArray(int width, int height, ushort[] tiles, bool copy)
        {
            if (tiles == null)
                throw new ArgumentNullException(nameof(tiles));
            if (width < 0)
                throw new ArgumentOutOfRangeException(nameof(width));
            if (height < 0)
                throw new ArgumentOutOfRangeException(nameof(height));
            if (tiles.Length != width * height)
                throw new ArgumentException("tiles length does not match width * height");

            _width = width;
            _height = height;

            if (copy)
            {
                _tiles = new ushort[tiles.Length];
                Array.Copy(tiles, _tiles, _tiles.Length);
            }
            else
            {
                _tiles = tiles;
            }
        }

        /// <summary>
        ///     Area of the tile array.
        /// </summary>
        public int Area => _width * _height;

        /// <summary>
        ///     Height of the tile array.
        /// </summary>
        public int Height => _height;

        /// <summary>
        ///     Raw tile array access.
        /// </summary>
        public ushort[] Tiles => _tiles;

        /// <summary>
        ///     Width of the tile array.
        /// </summary>
        public int Width => _width;

        /// <summary>
        ///     Access the tile at the specified index.
        /// </summary>
        /// <param name="index">Index of the tile.</param>
        public ushort this[int index]
        {
            get { return _tiles[index]; }

            set { _tiles[index] = value; }
        }

        /// <summary>
        ///     Access the tile at the specified coordinates.
        /// </summary>
        /// <param name="x">X coordinate of the tile.</param>
        /// <param name="y">Y coordinate of the tile.</param>
        public ushort this[int x, int y]
        {
            get { return _tiles[checked(_width * y + x)]; }

            set { _tiles[checked(_width * y + x)] = value; }
        }

        /// <summary>
        ///     Copy tiles between two-dimensional tile arrays. If the destination position or source rectangle are out of
        ///     bounds in some way, adjustments will be made to make the operation valid.
        /// </summary>
        /// <param name="src"> Source for tile copying. </param>
        /// <param name="destPos"> Position in the destination array to begin pasting. </param>
        /// <param name="srcRect"> The rectangle of tiles to be copied from the source. </param>
        public void Copy(TileArray src, Point destPos, Rectangle srcRect = new Rectangle())
        {
            int destX = destPos.X;
            int destY = destPos.Y;

            // Clip source rect to source tiledata
            int srcX = srcRect.X;
            int srcW = srcRect.Width;
            if (srcX < 0)
            {
                srcW += srcX;
                destX -= srcX;
                srcX = 0;
            }
            int maxW = src.Width - srcX;
            if (maxW < srcW)
                srcW = maxW;

            int srcY = srcRect.Y;
            int srcH = srcRect.Height;
            if (srcY < 0)
            {
                srcH += srcY;
                destY -= srcY;
                srcY = 0;
            }
            int maxH = src.Height - srcY;
            if (maxH < srcH)
                srcH = maxH;

            // Clip dest rectangle to tiledata size
            if (destX < 0)
            {
                srcW += destX;
                srcX -= destX;
                destX = 0;
            }
            if (destY < 0)
            {
                srcH += destY;
                srcY -= destY;
                destY = 0;
            }
            int ddX = destX + srcW - _width;
            if (ddX > 0)
                srcW -= ddX;
            int ddY = destY + srcH - _height;
            if (ddY > 0)
                srcH -= ddY;

            if (srcW <= 0 || srcH <= 0)
                return; // nothing to copy

            int srcStride = src._width;
            int dstStride = _width;
            int srcIndex = (srcStride * srcY) + srcX;
            int dstIndex = (dstStride * destY) + destX;

            for (int i = 0; i < srcH; i++)
            {
                Array.Copy(src._tiles, srcIndex, _tiles, dstIndex, srcW);
                srcIndex += srcStride;
                dstIndex += dstStride;
            }
        }
    }

    // Converts a TileArray to and from JSON as an object with a width, height, and tile string
    // consisting of base 64 encoded tiles.
}