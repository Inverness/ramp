using System;
using System.Diagnostics;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Ramp.Simulation.Tiling
{
    internal class TileArrayConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(TileArray);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
                                        JsonSerializer serializer)
        {
            JObject o = JObject.Load(reader);

            var width = (ushort) o["Width"];
            var height = (ushort) o["Height"];
            var bytes = (byte[]) o["Tiles"];

            if (bytes == null)
                return new TileArray(width, height);

            ushort[] tiles = ToTileArray(bytes);

            return new TileArray(width, height, tiles, false);
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var tileArray = (TileArray) value;

            var jobj = new JObject(
                new JProperty("Width", tileArray.Width),
                new JProperty("Height", tileArray.Height),
                new JProperty("Tiles", ToByteArray(tileArray.Tiles))
            );

            jobj.WriteTo(writer);
        }

        private static byte[] ToByteArray(ushort[] tiles)
        {
            // TODO: Endianness check
            var bytes = new byte[tiles.Length * 2];

            Buffer.BlockCopy(tiles, 0, bytes, 0, bytes.Length);

            return bytes;
        }

        private static ushort[] ToTileArray(byte[] bytes)
        {
            Debug.Assert(bytes.Length % 2 == 0, "bytes.Length % 2 == 0");

            var tiles = new ushort[bytes.Length / 2];

            Buffer.BlockCopy(bytes, 0, tiles, 0, bytes.Length);

            return tiles;
        }
    }
}