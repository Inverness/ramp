using System.ComponentModel;

namespace Ramp.Simulation.Tiling
{
    /// <summary>
    ///     Contains information about a collision between an actor and tiles.
    /// </summary>
    public class TileCollisionEventArgs : CancelEventArgs
    {
        public TileCollisionEventArgs()
        {
        }
    }
}