using Microsoft.Xna.Framework.Graphics;
using Newtonsoft.Json;
using Ramp.Data;
using Ramp.Threading;

namespace Ramp.Simulation.Tiling
{
    /// <summary>
    ///     Defines a set of tiles with related texture and properties.
    /// </summary>
    public sealed class Tileset : DispatcherObject
    {
        [JsonProperty("Properties")]
        private readonly TileProperty[] _properties;

        /// <summary>
        ///     Initialize a new Tileset.
        /// </summary>
        /// <param name="size"> The size of the tileset in pages. </param>
        /// <param name="texture"> The name of the tileset's texture. </param>
        public Tileset(ushort size, string texture = null)
        {
            Size = size;
            _properties = new TileProperty[size * TileCode.PageSize];
            Texture = new AssetReference<Texture2D>();
            TextureName = texture;
        }

        /// <summary>
        ///     Size of the tileset in pages.
        /// </summary>
        public ushort Size { get; private set; }

        /// <summary>
        ///     Properties for each tile, indexed by tile code.
        /// </summary>
        [JsonIgnore]
        public TileProperty[] Properties => _properties;

        /// <summary>
        ///     The name of the tileset. Doesn't have to be unique.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        ///     The name of this tileset's texture.
        /// </summary>
        public string TextureName
        {
            get { return Texture.Name; }

            set { Texture.Name = value; }
        }

        // Cache the texture
        [JsonIgnore]
        public AssetReference<Texture2D> Texture { get;  }

        // Calculate texture size in pages
        internal uint GetTextureSize(byte pixelsPerTile)
        {
            if (!Texture.IsLoaded)
                return 0;
            return (uint) Texture.LoadTargetAsset().Width / pixelsPerTile / TileCode.PageWidth;
        }
    }
}