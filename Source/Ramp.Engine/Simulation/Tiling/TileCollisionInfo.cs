﻿using Microsoft.Xna.Framework;

namespace Ramp.Simulation.Tiling
{
    /// <summary>
    ///     Information about a collision with a specific tile.
    /// </summary>
    public struct TileCollisionInfo
    {
        /// <summary>
        ///     The code of the tile that collided.
        /// </summary>
        public ushort Code;

        /// <summary>
        ///     The properties of the tile that collided.
        /// </summary>
        public TileProperty Properties;

        /// <summary>
        ///     THe position of the tile that collided in the level.
        /// </summary>
        public Point Position;

        public TileCollisionInfo(ushort code, TileProperty properties, Point position)
        {
            Code = code;
            Properties = properties;
            Position = position;
        }
    }
}