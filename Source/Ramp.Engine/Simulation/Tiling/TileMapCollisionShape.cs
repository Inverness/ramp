using Microsoft.Xna.Framework;
using nkast.Aether.Physics2D.Collision.Shapes;

namespace Ramp.Simulation.Tiling
{
    public sealed class TileMapCollisionShape
    {
        internal TileMapCollisionShape(byte layerIndex, bool blocking, Vector2 location, Shape shape)
        {
            LayerIndex = layerIndex;
            Blocking = blocking;
            Location = location;
            Shape = shape;
        }

        public Vector2 Location { get; }

        public byte LayerIndex { get; }

        public bool Blocking { get; }

        public Shape Shape { get; }
    }
}