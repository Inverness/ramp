using System;

namespace Ramp.Simulation.Tiling
{
    /// <summary>
    ///     Per-tile details
    /// </summary>
    [Flags]
    public enum TileProperty : byte
    {
        None = 0,
        Blocking = 1
    }
}