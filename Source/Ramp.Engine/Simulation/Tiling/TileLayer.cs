using Microsoft.Xna.Framework;
using Ramp.Threading;

namespace Ramp.Simulation.Tiling
{
    /// <summary>
    ///     Represents a layer of tiles in a level.
    /// </summary>
    public sealed class TileLayer : DispatcherObject
    {
        private readonly TileArray[,] _arrays;

        internal TileLayer(TileMap tileMap, byte index, byte visualLayer, string name, Point size, ushort deftile)
        {
            TileMap = tileMap;
            Index = index;
            VisualLayer = visualLayer;
            Name = name;
            Color = Color.White;
            Size = size;

            _arrays = new TileArray[size.X, size.Y];
            for (int y = 0; y < size.Y; y++)
            {
                for (int x = 0; x < size.X; x++)
                {
                    _arrays[x, y] = new TileArray(TileMap.GroupSize, TileMap.GroupSize, deftile);
                }
            }
        }

        public TileMap TileMap { get; }

        public Color Color { get; set; }

        public byte Index { get; }

        public byte VisualLayer { get; }

        public bool IsHidden { get; set; }

        public string Name { get; private set; }

        public Tileset Tileset { get; set; }

        public Point Size { get; private set; }

        public TileArray GetArray(int x, int y)
        {
            TileArray[,] arrays = _arrays;
            return x >= 0 && y >= 0 && x < arrays.GetLength(0) && y <= arrays.GetLength(1) ? arrays[x, y] : null;
        }
    }
}