﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Ramp.Threading;
using Ramp.Utilities.Collections;

namespace Ramp.Simulation
{
    // A timer manager based wholesale on Unreal Engine 4's implementation.
    public sealed class TimerManager : DispatcherObject
    {
        private readonly BinaryHeap<Timer> _active = new BinaryHeap<Timer>();
        private readonly List<Timer> _pending = new List<Timer>();
        private readonly List<Timer> _paused = new List<Timer>();
        private TimeSpan _time;
        private bool _isExecuting;
        private Timer _executingTimer;
        private uint _handleCounter;

        public bool IsExecuting => _isExecuting;

        public void Update(TimeSpan elapsed)
        {
            VerifyAccess();

            _time += elapsed;

            while (_active.Count != 0)
            {
                Timer current = _active.Peek();
                if (_time <= current.ExpireTime)
                    break;

                _active.Pop();
                _isExecuting = true;
                _executingTimer = current;
                try
                {
                    int callCount = 1;
                    if (current.Info.Loop)
                        callCount += (int) ((_time.Ticks - current.ExpireTime.Ticks) / current.Info.Rate.Ticks);

                    for (int i = 0; i < callCount; i++)
                    {
                        current.Info.Action();
                        current = _executingTimer; // pull any changes made during the action
                        
                        if (!current.Handle.Valid)
                            break;
                    }

                    if (current.Info.Loop && current.Handle.Valid)
                    {
                        current.ExpireTime += new TimeSpan(callCount * current.Info.Rate.Ticks);
                        _active.Push(current);
                    }
                }
                finally
                {
                    _isExecuting = false;
                    _executingTimer = default(Timer);
                }
            }

            if (_pending.Count != 0)
            {
                for (int i = 0; i < _pending.Count; i++)
                {
                    Timer timer = _pending[i];
                    timer.ExpireTime += _time;
                    timer.Info.Status = TimerStatus.Active;
                    _active.Push(timer);
                }

                _pending.Clear();
            }
        }

        public void SetTimer(ref TimerHandle handle, Action action, double rateSeconds, bool loop = false,
                             double? delaySeconds = null)
        {
            SetTimer(ref handle,
                     action,
                     TimeSpan.FromSeconds(rateSeconds),
                     loop,
                     delaySeconds.HasValue ? TimeSpan.FromSeconds(delaySeconds.Value) : (TimeSpan?) null);
        }

        public void SetTimer(ref TimerHandle handle, Action action, TimeSpan rate = default(TimeSpan), bool loop = false,
                             TimeSpan? delay = null)
        {
            Validate.ArgumentNotNull(action, "action");
            VerifyAccess();

            if (handle.Valid)
                ClearTimer(ref handle);
            Debug.Assert(!handle.Valid, "!handle.Valid");

            // A rate of zero or less only clears the timer
            if (rate <= TimeSpan.Zero)
                return;

            handle = CreateValidHandle();

            var newTimer = new Timer
            {
                Handle = handle,
                Info =
                {
                    Action = action,
                    Rate = rate,
                    Loop = loop
                }
            };

            TimeSpan firstDelay = delay.HasValue && delay.Value >= TimeSpan.Zero ? delay.Value : rate;

            if (_isExecuting)
            {
                newTimer.ExpireTime = firstDelay;
                newTimer.Info.Status = TimerStatus.Pending;
                _pending.Add(newTimer);
            }
            else
            {
                newTimer.ExpireTime = _time + firstDelay;
                newTimer.Info.Status = TimerStatus.Active;
                _active.Push(newTimer);
            }
        }

        public void SetUpdate(Action action)
        {
            Validate.ArgumentNotNull(action, "action");
            VerifyAccess();

            var timer = new Timer
            {
                Handle = CreateValidHandle(),
                Info = { Action = action },
                ExpireTime = TimeSpan.Zero
            };

            if (_isExecuting)
            {
                timer.Info.Status = TimerStatus.Pending;
                _pending.Add(timer);
            }
            else
            {
                timer.Info.Status = TimerStatus.Active;
                _active.Push(timer);
            }
        }

        public bool SetTimerPaused(ref TimerHandle handle, bool paused)
        {
            if (!handle.Valid)
                return false;

            int index;
            Timer timer = FindTimer(handle, out index);

            if (!timer.Handle.Valid)
                return false;

            if (paused)
            {
                if (timer.Info.Status != TimerStatus.Paused)
                {
                    switch (timer.Info.Status)
                    {
                        case TimerStatus.Pending:
                            _pending.RemoveAtSwap(index);
                            break;
                        case TimerStatus.Active:
                            timer.ExpireTime -= _time;
                            _active.RemoveAt(index);
                            break;
                    }

                    timer.Info.Status = TimerStatus.Paused;
                    _paused.Add(timer);
                }
            }
            else
            {
                if (timer.Info.Status == TimerStatus.Paused)
                {
                    if (_isExecuting)
                    {
                        timer.Info.Status = TimerStatus.Pending;
                        _pending.Add(timer);
                    }
                    else
                    {
                        timer.ExpireTime += _time;
                        timer.Info.Status = TimerStatus.Active;
                        _active.Push(timer);
                    }

                    _paused.RemoveAtSwap(index);
                }
            }

            return true;
        }

        public bool ClearTimer(ref TimerHandle handle)
        {
            VerifyAccess();

            if (!handle.Valid)
                return false;

            int timerIndex;
            Timer timer = FindTimer(handle, out timerIndex);

            if (timer.Handle.Valid)
            {
                switch (timer.Info.Status)
                {
                    case TimerStatus.Pending:
                        _pending.RemoveAtSwap(timerIndex);
                        break;
                    case TimerStatus.Active:
                        _active.RemoveAt(timerIndex);
                        break;
                    case TimerStatus.Paused:
                        _paused.RemoveAtSwap(timerIndex);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
                handle = TimerHandle.Invalid;
                return true;
            }

            if (_isExecuting && _executingTimer.Handle == handle)
            {
                _executingTimer.Handle = TimerHandle.Invalid;
                handle = TimerHandle.Invalid;
                return true;
            }

            return false;
        }

        public TimerInfo? GetTimerInfo(ref TimerHandle handle)
        {
            VerifyAccess();
            Timer timer = FindTimer(handle);
            return timer.Handle.Valid ? timer.Info : (TimerInfo?) null;
        }

        public void ClearAllTimers(object target)
        {
            Validate.ArgumentNotNull(target, "target");
            VerifyAccess();

            for (int i = 0; i < _active.Count; i++)
            {
                if (_active[i].Info.Action.Target == target)
                {
                    _active.RemoveAt(i);
                    i--;
                }
            }

            for (int i = 0; i < _pending.Count; i++)
            {
                if (_pending[i].Info.Action.Target == target)
                {
                    _pending.RemoveAtSwap(i);
                    i--;
                }
            }

            for (int i = 0; i < _paused.Count; i++)
            {
                if (_paused[i].Info.Action.Target == target)
                {
                    _paused.RemoveAtSwap(i);
                    i--;
                }
            }

            if (_executingTimer.Handle.Valid && _executingTimer.Info.Action.Target == target)
                _executingTimer.Handle = TimerHandle.Invalid;
        }

        private Timer FindTimer(TimerHandle handle)
        {
            int unused;
            return FindTimer(handle, out unused);
        }

        private Timer FindTimer(TimerHandle handle, out int index)
        {
            for (int i = 0; i < _active.Count; i++)
            {
                if (_active[i].Handle == handle)
                {
                    index = i;
                    return _active[i];
                }
            }

            for (int i = 0; i < _pending.Count; i++)
            {
                if (_pending[i].Handle == handle)
                {
                    index = i;
                    return _pending[i];
                }
            }

            for (int i = 0; i < _paused.Count; i++)
            {
                if (_paused[i].Handle == handle)
                {
                    index = i;
                    return _paused[i];
                }
            }

            index = -1;
            return default(Timer);
        }

        private TimerHandle CreateValidHandle()
        {
            var h = new TimerHandle(++_handleCounter);
            if (!h.Valid)
                throw new InvalidOperationException("Too many handles");
            return h;
        }

        private struct Timer : IComparable<Timer>
        {
            public TimerHandle Handle;

            public TimerInfo Info;

            public TimeSpan ExpireTime;

            public int CompareTo(Timer other)
            {
                return ExpireTime.CompareTo(other.ExpireTime);
            }
        }
    }

    /// <summary>
    ///     Describes a timer.
    /// </summary>
    public struct TimerInfo
    {
        public Action Action;

        public TimeSpan Rate;

        public bool Loop;

        public TimerStatus Status;
    }
}