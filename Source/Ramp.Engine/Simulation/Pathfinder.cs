﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Ramp.Utilities.Collections;

namespace Ramp.Simulation
{
    /// <summary>
    ///     Calculates a path through a level and caches allocated structures for performance.
    /// </summary>
    public class Pathfinder
    {
        public const float DefaultRange = 1;
        public const float DefaultMaxDistance = 96;

        private readonly BinaryHeap<KeyValuePair<float, Vector2>> _openHeap =
            new BinaryHeap<KeyValuePair<float, Vector2>>(new OpenVectorComparer());

        private readonly Dictionary<Vector2, bool> _testResult = new Dictionary<Vector2, bool>();
        private readonly HashSet<Vector2> _closed = new HashSet<Vector2>();
        private readonly List<Vector2> _resultBuilder = new List<Vector2>();
        private readonly Dictionary<Vector2, Vector2> _cameFrom = new Dictionary<Vector2, Vector2>();
        private readonly Dictionary<Vector2, float> _gscore = new Dictionary<Vector2, float>();
        private readonly Dictionary<Vector2, float> _fscore = new Dictionary<Vector2, float>();
        private readonly List<Vector2> _neighbors = new List<Vector2>(8);

        /// <summary>
        ///     Finds a path through a level for the specified actor.
        /// </summary>
        /// <param name="actor"> An actor that will be tested for collision. </param>
        /// <param name="destination"> The destination location. </param>
        /// <param name="result"> A collection that will receive the resulting path. This can be null if only the path count is required. </param>
        /// <param name="range"> The range to the destination required to complete the path. </param>
        /// <param name="maxDistance"> The maximum allowed distance to a destination. </param>
        /// <returns> The number of steps in the path. 0 if no path could be found. </returns>
        public int Find(Actor actor, Vector2 destination, ICollection<Vector2> result, float? range = null,
                        float? maxDistance = null)
        {
            if (actor == null)
                throw new ArgumentNullException(nameof(actor));

            var transform = actor.GetComponent<TransformComponent>();
            if (transform == null)
                throw new ArgumentException("actor does not have a transform component", nameof(actor));

            if (!range.HasValue)
                range = DefaultRange;
            if (!maxDistance.HasValue)
                maxDistance = DefaultMaxDistance;

            float rangeSquared = range.Value * range.Value;

            _openHeap.Push(new KeyValuePair<float, Vector2>(0, transform.Location));
            _gscore[transform.Location] = 0;
            _fscore[transform.Location] = GetEstimate(transform.Location, destination);

            int resultCount = 0;

            while (_openHeap.Count != 0)
            {
                Vector2 current = _openHeap.Pop().Value;

                if (Vector2.DistanceSquared(current, destination) <= rangeSquared)
                {
                    // Prepare the result
                    _resultBuilder.Add(current);

                    while (_cameFrom.TryGetValue(current, out current))
                        _resultBuilder.Add(current);

                    resultCount = _resultBuilder.Count;

                    if (result != null)
                    {
                        for (int i = _resultBuilder.Count - 1; i > -1; i--)
                            result.Add(_resultBuilder[i]);
                    }

                    _resultBuilder.Clear();

                    break;
                }

                _closed.Add(current);
                
                var up = new Vector2(current.X, current.Y - 1);
                var left = new Vector2(current.X - 1, current.Y);
                var down = new Vector2(current.X, current.Y + 1);
                var right = new Vector2(current.X + 1, current.Y);
                var upLeft = new Vector2(current.X - 1, current.Y - 1);
                var downLeft = new Vector2(current.X - 1, current.Y + 1);
                var downRight = new Vector2(current.X + 1, current.Y + 1);
                var upRight = new Vector2(current.X + 1, current.Y - 1);

                if (!IsBlocked(actor, up, range.Value))
                    _neighbors.Add(up);
                if (!IsBlocked(actor, left, range.Value))
                    _neighbors.Add(left);
                if (!IsBlocked(actor, down, range.Value))
                    _neighbors.Add(down);
                if (!IsBlocked(actor, right, range.Value))
                    _neighbors.Add(right);
                if (!IsBlocked(actor, upLeft, range.Value))
                    _neighbors.Add(upLeft);
                if (!IsBlocked(actor, downLeft, range.Value))
                    _neighbors.Add(downLeft);
                if (!IsBlocked(actor, downRight, range.Value))
                    _neighbors.Add(downRight);
                if (!IsBlocked(actor, upRight, range.Value))
                    _neighbors.Add(upRight);

                for (int i = 0; i < _neighbors.Count; i++)
                {
                    Vector2 neighbor = _neighbors[i];

                    if (_closed.Contains(neighbor))
                        continue;

                    float newGScore = _gscore[current] + (float) Math.Round(GetDistance(current, neighbor), 1);
                    if (newGScore > maxDistance.Value)
                        continue;

                    float existingGScore;
                    if (!_gscore.TryGetValue(neighbor, out existingGScore) || newGScore < existingGScore)
                    {
                        _cameFrom[neighbor] = current;
                        _gscore[neighbor] = newGScore;
                        _fscore[neighbor] = newGScore + GetEstimate(neighbor, destination);

                        _openHeap.Push(new KeyValuePair<float, Vector2>(newGScore, neighbor));
                    }
                }
                _neighbors.Clear();
            }

            _closed.Clear();
            _openHeap.Clear();
            _cameFrom.Clear();
            _gscore.Clear();
            _fscore.Clear();
            _testResult.Clear();

            return resultCount;
        }
        
        private bool IsBlocked(Actor actor, Vector2 location, float range)
        {
            //if (proc == null)
            //    return false;

            bool result;
            if (_testResult.TryGetValue(location, out result))
                return result;

            if (!_testResult.TryGetValue(location, out result))
            {
                result = actor.Level.TestOverlap(actor, location, true, range);
                _testResult.Add(location, result);
            }

            return result;
        }
        
        private static float GetDistance(Vector2 a, Vector2 b)
        {
            return (float) Math.Sqrt(Math.Pow(b.X - a.X, 2) + Math.Pow(b.Y - a.Y, 2));
        }
        
        private static float GetEstimate(Vector2 a, Vector2 b)
        {
            return (float) Math.Floor(GetDistance(a, b));
        }

        private class OpenVectorComparer : IComparer<KeyValuePair<float, Vector2>>
        {
            public int Compare(KeyValuePair<float, Vector2> x, KeyValuePair<float, Vector2> y)
            {
                return x.Key.CompareTo(y.Key);
            }
        }
    }
}