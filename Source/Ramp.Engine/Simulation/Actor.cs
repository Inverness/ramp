using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Coroutines;
using NLog;
using Microsoft.Xna.Framework;
using Ramp.Naming;
using Ramp.Network;
using Ramp.Utilities.Collections;

namespace Ramp.Simulation
{
    /// <summary>
    ///     Describes the actor's lifetime status.
    /// </summary>
    public enum ActorStatus
    {
        /// <summary>
        ///     The actor is currently being spawned and has not finished its constructor.
        /// </summary>
        Spawning,

        /// <summary>
        ///     The actor has been spawned.
        /// </summary>
        Spawned,

        /// <summary>
        ///     The actor is scheduled to be disposed or is currently being disposed.
        /// </summary>
        Disposing,

        /// <summary>
        ///     The actor has been disposed.
        /// </summary>
        Disposed
    }

    /// <summary>
    ///     An object existing in and bound to a level that serves as a container
    ///     for components.
    /// </summary>
    /// <remarks>
    ///     IDisposable is not implemented for Actors or Components because their lifetime is managed by the level that
    ///     contains them. Actors and Components must clean up any managed or unmanaged resources they own in their
    ///     OnDisposed method.
    /// </remarks>
    [DebuggerDisplay("ComponentCount = {" + nameof(ComponentCount) + "}")]
    public sealed partial class Actor : NamedObject
    {
        public const byte MaxComponents = 200;

        private const int ComponentsInitialSize = 4; // Initial array size when adding first component
        private const float ComponentsSizeMultiplier = 1.2f; // Array size multiplier when reaching capacity.

        private static readonly Logger s_log = LogManager.GetCurrentClassLogger();
        
        private static readonly NameValueDictionary s_emptySpawnArguments = new NameValueDictionary();
        private static readonly IReadOnlyCollection<string> s_emptyTags = new string[0];
        private static readonly IReadOnlyList<Actor> s_emptyChildren = new Actor[0];
        private static readonly ActorComponent[] s_emptyComponentArray = new ActorComponent[0];

        private readonly bool _static;
        private readonly ActorArchetype _archetype;
        private readonly NameValueDictionary _spawnArguments;

        private ActorStatus _status;
        private ushort _actorListIndex;
        private ushort _updateListIndex;

        private bool _isLevelChanging;
        private bool _canChangeLevels;

        private StringSet _tags;

        private Actor _owner;
        private List<Actor> _children;

        private ActorComponent[] _components;
        private byte _componentCount;

        private bool _replicating;

        private CoroutineExecutor _coroutineExecutor;

        internal Actor(
            string name,
            Level level,
            bool isStatic,
            IReadOnlyNameValueDictionary spawnArguments,
            ActorArchetype archetype,
            Actor owner,
            NetRole? role,
            IEnumerable<Type> components)
            : base(name, level)
        {
            _status = ActorStatus.Spawning;
            _static = isStatic;
            _components = s_emptyComponentArray;
            _archetype = archetype;

            if (archetype != null && spawnArguments != null)
            {
                // This will be the majority of spawn cases
                _spawnArguments = new NameValueDictionary(archetype.SpawnArguments.Count + spawnArguments.Count);
                _spawnArguments.Update(archetype.SpawnArguments);
                _spawnArguments.Update(spawnArguments);
            }
            else if (archetype != null && archetype.SpawnArguments.Count != 0)
            {
                _spawnArguments = new NameValueDictionary(archetype.SpawnArguments);
            }
            else if (spawnArguments != null && spawnArguments.Count != 0)
            {
                _spawnArguments = new NameValueDictionary(spawnArguments);
            }
            else
            {
                _spawnArguments = s_emptySpawnArguments;
            }

            Owner = owner;
            if (role.HasValue)
                SetRoles(role.Value, role.Value == NetRole.Authority ? RemoteRole : NetRole.Authority);

            _tags = _spawnArguments.GetValue<StringSet>("Tags");
            _canChangeLevels = _spawnArguments.GetValue("CanChangeLevels", false);
            ReplicaUpdateRate = _spawnArguments.GetValue("ReplicaUpdateRate", TimeSpan.FromSeconds(0.2));

            if (_role == NetRole.Authority)
            {
                SetRoles(_role, _spawnArguments.GetValue("RemoteRole", NetRole.None));

                // Client replicated actors should never spawn their own components as those will be sent from
                // the server and spawned during deserialization.
                if (_archetype != null)
                {
                    foreach (KeyValuePair<ActorComponentType, string> component in _archetype.Components)
                        AddComponent(component.Key, component.Value);
                }

                if (components != null)
                {
                    foreach (Type type in components)
                        AddComponent(type);
                }
            }

            _actorListIndex = Level.AddActor(this);

            _status = ActorStatus.Spawned;
        }

        /// <summary>
        ///     Gets whether this object is currently changing between levels.
        /// </summary>
        public bool IsLevelChanging => _isLevelChanging;

        /// <summary>
        ///     Gets the status of the actor.
        /// </summary>
        public ActorStatus Status => _status;

        /// <summary>
        ///     Gets the current level for this object.
        /// </summary>
        public Level Level => (Level) Outer;

        /// <summary>
        ///     Gets the current level's world.
        /// </summary>
        public World World => (World) Outer.Outer;

        /// <summary>
        ///     True if this actor was created as part of level loading.
        /// </summary>
        public bool Static => _static;

        /// <summary>
        ///     The name of the archetype used when spawning this actor.
        /// </summary>
        public ActorArchetype Archetype => _archetype;

        /// <summary>
        ///     Whether or not this actor is allowed to change levels.
        /// </summary>
        public bool CanChangeLevels
        {
            get { return _canChangeLevels; }

            set { _canChangeLevels = value; }
        }

        /// <summary>
        ///     That actor that owns this actor.
        /// </summary>
        public Actor Owner
        {
            get { return _owner; }

            set
            {
                if (_owner == value)
                    return;

                _owner?._children.Remove(this);

                _owner = value;

                if (_owner != null)
                {
                    if (_owner._children == null)
                        _owner._children = new List<Actor>();
                    _owner._children.Add(this);
                }
            }
        }

        /// <summary>
        ///     Gets the spawn arugments of the actor.
        /// </summary>
        public IReadOnlyNameValueDictionary SpawnArguments => _spawnArguments;

        /// <summary>
        ///     The actors that are owned by this actor.
        /// </summary>
        public IReadOnlyList<Actor> Children => _children ?? s_emptyChildren;

        /// <summary>
        ///     The number of components attached to this actor.
        /// </summary>
        public byte ComponentCount => _componentCount;

        /// <summary>
        ///     A set of string tags that describe this actor.
        /// </summary>
        public IReadOnlyCollection<string> Tags => _tags ?? s_emptyTags;

        /// <summary>
        ///     Gets an enumeration of this actor's components.
        /// </summary>
        public IReadOnlyList<ActorComponent> Components => new ArraySegment<ActorComponent>(_components, 0, _componentCount);
        
        /// <summary>
        /// Gets or sets whether this is actor is active. Only active actors are updated.
        /// </summary>
        public bool IsActive
        {
            get { return _updateListIndex != 0; }

            set
            {
                if (value)
                {
                    if (_updateListIndex == 0)
                    {
                        _updateListIndex = Level.AddUpdateTarget(this);
                    }
                }
                else
                {
                    if (_updateListIndex != 0)
                    {
                        Level.RemoveUpdateTarget(_updateListIndex);
                        _updateListIndex = 0;
                    }
                }
            }
        }

        internal byte ComponentCapacity
        {
            get { return (byte)_components.Length; }

            set
            {
                value = Math.Min(MaxComponents, Math.Max(_componentCount, value));
                if (value != _components.Length)
                    Array.Resize(ref _components, value);
            }
        }

        /// <summary>
        ///     Check if the actor has a tag without allocating a tag dictionary on demand.
        /// </summary>
        /// <param name="tag"> The tag to check for. </param>
        /// <returns> True if the actor has the tag. </returns>
        public bool HasTag(string tag)
        {
            return _tags != null && _tags.Contains(tag);
        }

        public bool AddTag(string tag)
        {
            if (_tags == null)
                _tags = new StringSet();
            return _tags.Add(tag);
        }

        public bool RemoveTag(string tag)
        {
            return _tags != null && _tags.Remove(tag);
        }

        /// <summary>
        ///     Transfers the actor from one level to another.
        /// </summary>
        /// <param name="destination"> The destination level name. </param>
        /// <param name="location"> The destination location. </param>
        /// <exception cref="InvalidOperationException">
        ///     Raised when:
        ///     * The actor is not owned by the current level
        ///     * There is no level provider
        ///     * The actor is not allowed to change levels
        ///     * The actor was disposed
        ///     * The actor is already changing levels
        ///     * The destination level was not found
        ///     * The destination level contains an actor with the same name
        /// </exception>
        public async void ChangeLevel(string destination, Vector2 location)
        {
            await ChangeLevelAsync(destination, location);
        }

        /// <summary>
        ///     Transfers the actor from one level to another.
        /// </summary>
        /// <param name="destination"> The destination level name. </param>
        /// <param name="location"> The destination location. </param>
        /// <exception cref="InvalidOperationException">
        ///     Raised when:
        ///     * The actor is not owned by the current level
        ///     * There is no level provider
        ///     * The actor is not allowed to change levels
        ///     * The actor was disposed
        ///     * The actor is already changing levels
        ///     * The destination level was not found
        ///     * The destination level contains an actor with the same name
        /// </exception>
        public Task ChangeLevelAsync(string destination, Vector2 location)
        {
            return World.ChangeActorLevelAsync(this, destination, location);
        }

        /// <summary>
        ///     Disposes an actor during the next world update. This is the proper way of disposing actors and should
        ///     be used in favor of a direct disposal.
        /// </summary>
        public void MarkForDispose()
        {
            if (_status < ActorStatus.Disposing)
            {
                _status = ActorStatus.Disposing;
                World.AddToDisposeList(this);
            }
        }

        /// <summary>
        ///     Finds a spawn argument by looking at this actor and its archetype's spawn arguments.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public T GetSpawnArgument<T>(string name, T defaultValue = default(T))
        {
            return _spawnArguments.GetValue(name, defaultValue);
        }

        /// <summary>
        ///     Add a new component of the specified type.
        /// </summary>
        /// <typeparam name="T"> Component type </typeparam>
        /// <param name="name"> An optional component instance name. </param>
        /// <returns> A new component instance. </returns>
        public T AddComponent<T>(string name = null) where T : ActorComponent
        {
            return (T) AddComponent(typeof(T), name);
        }

        /// <summary>
        ///     Add a new component of the specified type by the type name(s) in the registry.
        /// </summary>
        /// <param name="typeName"> The registered type name of the component class. </param>
        /// <param name="name"> An optional component instance name. </param>
        /// <returns> A new component instance. </returns>
        public ActorComponent AddComponent(string typeName, string name = null)
        {
            return AddComponent(GetComponentTypeChecked(typeName), name);
        }

        /// <summary>
        ///     Add a new component of the specified type.
        /// </summary>
        /// <param name="type"> The component type </param>
        /// <param name="name"> An optional component instance name. </param>
        /// <returns> A new component instance. </returns>
        public ActorComponent AddComponent(Type type, string name = null)
        {
            Validate.ArgumentNotNull(type, "type");

            ActorComponentType ct = ActorComponentType.GetType(type);
            if (ct == null)
                throw new ArgumentException("Invalid component type: " + type.FullName);
            return AddComponent(ct, name);
        }

        /// <summary>
        ///     Add a new component of the specified type.
        /// </summary>
        /// <param name="type"> The component type </param>
        /// <param name="name"> An optional component instance name. </param>
        /// <returns> A new component instance. </returns>
        public ActorComponent AddComponent(ActorComponentType type, string name = null)
        {
            Validate.ArgumentNotNull(type, "type");

            if (IsDisposed)
                throw new InvalidOperationException("the actor was disposed");

            // A client's components must match the server's for replication to work, so adding components on an actor
            // that isn't an authority is not allowed
            if (Role != NetRole.Authority && !_replicating)
                throw new InvalidOperationException("cannot add components to non-authoritative actors");

            if (_componentCount >= MaxComponents)
                throw new InvalidOperationException("max components reached");

            // Grow the component array if necessary
            // A List<T> is not used because components are only ever added to an Actor, not removed.
            if (_componentCount == _components.Length)
            {
                int newSize = _components.Length == 0 ? ComponentsInitialSize
                                                      : (int) (_components.Length * ComponentsSizeMultiplier);

                newSize = Math.Min(Math.Max(newSize, _componentCount + 1), MaxComponents);

                Array.Resize(ref _components, newSize);
            }

            // It's possible that the component constructor might add another component
            ActorComponent component = type.CreateInstance(name, this);

            // Since components are never moved or removed, they can be referred to by index within the actor for
            // things like networking.
            _components[_componentCount++] = component;

            return component;
        }

        /// <summary>
        ///     Get the first component that matches the specified type.
        /// </summary>
        /// <typeparam name="T"> A component class or interface. </typeparam>
        /// <returns> A component if a match was found, otherwise null. </returns>
        public T GetComponent<T>() where T : class
        {
            return (T) (object) GetComponent(typeof(T));
        }

        /// <summary>
        ///     Gets a component by its index.
        /// </summary>
        /// <param name="index"> The index. </param>
        /// <returns> The matching component. </returns>
        public ActorComponent GetComponent(byte index)
        {
            if (index >= _componentCount)
                throw new IndexOutOfRangeException("invalid component index: " + index);
            return _components[index];
        }

        /// <summary>
        ///     Get the first component that matches the specified type.
        /// </summary>
        /// <param name="type"> A component type. </param>
        /// <returns> A component if a match was found, otherwise null. </returns>
        public ActorComponent GetComponent(Type type)
        {
            Validate.ArgumentNotNull(type, "type");
            Validate.ArgumentInRange(type.IsClass || type.IsInterface, "type", "must be a class or interface");

            for (int i = 0; i < _componentCount; i++)
            {
                if (type.IsInstanceOfType(_components[i]))
                    return _components[i];
            }

            return null;
        }

        /// <summary>
        ///     Get the Nth component that matches the specified type.
        /// </summary>
        /// <param name="type"> A component type. </param>
        /// <param name="index"> The index of a component in the matches. </param>
        /// <returns> A component if a match was found, otherwise null. </returns>
        public ActorComponent GetComponent(Type type, byte index)
        {
            Validate.ArgumentNotNull(type, "type");
            Validate.ArgumentInRange(type.IsClass || type.IsInterface, "type", "must be a class or interface");

            byte counter = 0;
            for (int i = 0; i < _componentCount; i++)
            {
                if (!type.IsInstanceOfType(_components[i]))
                    continue;
                if (counter == index)
                    return _components[i];
                counter++;
            }

            return null;
        }

        /// <summary>
        ///     Get the first component that matches the specified type.
        /// </summary>
        /// <param name="typeName"> A component type name. </param>
        /// <returns> A component if a match was found, otherwise null. </returns>
        public ActorComponent GetComponent(string typeName)
        {
            return GetComponent(GetComponentTypeChecked(typeName).ClrType);
        }

        /// <summary>
        ///     Get all components of the specified type.
        /// </summary>
        /// <typeparam name="T"> A component class or interface. </typeparam>
        /// <returns> A collection of components of the specified type. </returns>
        public IEnumerable<T> GetComponents<T>() where T : class
        {
            for (int i = 0; i < _componentCount; i++)
            {
                var asT = _components[i] as T;
                if (asT != null)
                    yield return asT;
            }
        }

        /// <summary>
        ///     Get all components of the specified type.
        /// </summary>
        /// <param name="type"> A component type. </param>
        /// <returns> A collection of components of the specified type. </returns>
        public IEnumerable<ActorComponent> GetComponents(Type type)
        {
            for (int i = 0; i < _componentCount; i++)
            {
                if (type.IsInstanceOfType(_components[i]))
                    yield return _components[i];
            }
        }

        public byte? IndexOfComponent(ActorComponent component)
        {
            for (byte i = 0; i < _componentCount; i++)
            {
                if (_components[i] == component)
                    return i;
            }

            return null;
        }

        /// <summary>
        /// Starts a coroutine on the current actor. The coroutine will be disposed if the actor is.
        /// </summary>
        /// <param name="cor"> A coroutine. </param>
        /// <returns> A new CoroutineThread. </returns>
        public CoroutineThread StartCoroutine(IEnumerable cor)
        {
            if (_coroutineExecutor == null)
                _coroutineExecutor = new CoroutineExecutor();
            CoroutineThread thread = _coroutineExecutor.Start(cor);
            thread.Tag = this;
            return thread;
        }

        public void Update(TimeSpan elapsed)
        {
            for (int i = 0; i < _componentCount; i++)
                _components[i].Update(elapsed);

            _coroutineExecutor?.Tick(elapsed);
        }

        // Exposes rename to World for level changing.
        internal void Rename(string newName, Level newLevel)
        {
            base.Rename(newName, newLevel);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_status != ActorStatus.Disposing)
                {
                    s_log.Warn("Undelayed actor disposal for {0}", this);
                    _status = ActorStatus.Disposing;
                }

                World.OnActorDisposing(this);

                Owner = null;

                if (_children != null && _children.Count != 0)
                    _children.ToList().ForEach(c => c.MarkForDispose());

                if (_updateListIndex != 0)
                    Level.RemoveUpdateTarget(_updateListIndex);

                _coroutineExecutor?.Dispose();

                for (byte i = 0; i < _componentCount; i++)
                    _components[i].OnActorDisposing();

                Level.RemoveActor(_actorListIndex);

                _status = ActorStatus.Disposed;
            }
            base.Dispose(disposing);
        }

        /// <summary>
        ///     Begin a level change. Sets IsLevelChanging to true and raises the LevelChanging event.
        /// </summary>
        /// <param name="newLevel"> The new level. </param>
        internal void BeginLevelChange(Level newLevel)
        {
            Debug.Assert(newLevel != null);

            _isLevelChanging = true;

            for (byte i = 0; i < _componentCount; i++)
                _components[i].OnLevelChanging(newLevel);
            
            // Don't reset the index since it will indicate that we need to re-add after level change
            if (_updateListIndex != 0)
                Level.RemoveUpdateTarget(_updateListIndex);

            Level.RemoveActor(_actorListIndex);
        }

        /// <summary>
        ///     End a level change. Sets IsLevelChanging to false, and 
        ///     raises the LevelChanged event.
        /// </summary>
        internal void EndLevelChange(Vector2 location)
        {
            this.SetLocation(location);

            _actorListIndex = Level.AddActor(this);

            _isLevelChanging = false;

            if (_updateListIndex != 0)
                _updateListIndex = Level.AddUpdateTarget(this);

            for (byte i = 0; i < _componentCount; i++)
                _components[i].OnLevelChanged();
        }

        private static ActorComponentType GetComponentTypeChecked(string typeName)
        {
            ActorComponentType ct = ActorComponentType.GetType(typeName);
            if (ct == null)
                throw new ArgumentException("Invalid component type name: " + typeName, nameof(typeName));
            return ct;
        }
    }
}