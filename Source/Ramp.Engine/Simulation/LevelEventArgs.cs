using System;

namespace Ramp.Simulation
{
    /// <summary>
    ///     Provides data for an event involving a level.
    /// </summary>
    public class LevelEventArgs : EventArgs
    {
        public LevelEventArgs(Level level)
        {
            Level = level;
        }

        public Level Level { get; private set; }
    }
}