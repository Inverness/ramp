﻿using System;
using Microsoft.Xna.Framework;
using Ramp.Network;

namespace Ramp.Simulation
{
    /// <summary>
    ///     Describes the transform of an actor.
    /// </summary>
    public class TransformComponent : ActorComponent
    {
        private Vector2 _location;
        private float _scale;
        private float _rotation;

        public TransformComponent()
        {
            _scale = Actor.GetSpawnArgument("Scale", 1f);
            _rotation = Actor.GetSpawnArgument("Rotation", 0f);
        }

        public event TypedEventHandler<TransformComponent, EventArgs> LocationChanged;

        /// <summary>
        ///     Gets or sets the actor's location.
        /// </summary>
        public Vector2 Location
        {
            get { return _location; }

            set
            {
                if (_location == value)
                    return;
                _location = value;
                OnLocationChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the actor's scale. The default value is 1.
        /// </summary>
        public float Scale
        {
            get { return _scale; }

            set { _scale = value; }
        }

        /// <summary>
        ///     Gets or sets the actor's rotation.
        /// </summary>
        public float Rotation
        {
            get { return _rotation; }

            set { _rotation = value; }
        }

        public override void WriteDelta(DeltaWriter writer, ReplicationContext context)
        {
            base.WriteDelta(writer, context);

            writer.Write(_location);
            writer.Write(_scale);
            writer.Write(_rotation);
        }

        public override void ReadDelta(DeltaReader reader, ReplicationContext context)
        {
            base.ReadDelta(reader, context);

            reader.Read(ref _location);
            reader.Read(ref _scale);
            reader.Read(ref _rotation);
        }

        protected virtual void OnLocationChanged()
        {
            LocationChanged?.Invoke(this, EventArgs.Empty);
        }
    }

    /// <summary>
    ///     Provides extension methods for common transform operations.
    /// </summary>
    public static class TransformExtensions
    {
        /// <summary>
        ///     Gets the location of an actor. Returns null if the actor does not have a transform component.
        /// </summary>
        /// <param name="actor"> An actor. </param>
        /// <returns> The actor's location, or null if the actor does not have a transform component. </returns>
        public static Vector2? GetLocation(this Actor actor)
        {
            Validate.ArgumentNotNull(actor, "actor");
            return actor.GetComponent<TransformComponent>()?.Location;
        }

        /// <summary>
        ///     Sets the location of an actor.
        /// </summary>
        /// <param name="actor"> An actor. </param>
        /// <param name="location"> The new location. </param>
        /// <returns> True if the actor had a transform component and had its location set, otherwise false. </returns>
        public static bool SetLocation(this Actor actor, Vector2 location)
        {
            Validate.ArgumentNotNull(actor, "actor");
            var transform = actor.GetComponent<TransformComponent>();
            if (transform == null)
                return false;
            transform.Location = location;
            return true;
        }
    }
}