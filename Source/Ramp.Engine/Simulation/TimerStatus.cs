namespace Ramp.Simulation
{
    public enum TimerStatus : byte
    {
        Pending,
        Active,
        Paused
    }
}