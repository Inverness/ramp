using System;
using Microsoft.Xna.Framework;
using Ramp.Network;
using nkast.Aether.Physics2D.Collision.Shapes;
using nkast.Aether.Physics2D.Common;

namespace Ramp.Simulation
{
    /// <summary>
    ///     Collider implementation using a simple axis-aligned box to do collision.
    /// </summary>
    public sealed class BoxCollider : Collider
    {
        private Vector2 _offset;
        private Vector2 _size;
        private PolygonShape _physicsShape;

        public BoxCollider()
            : base(ColliderShape.Box)
        {
            _offset = Actor.GetSpawnArgument<Vector2>("ColliderOffset");
            _size = Actor.GetSpawnArgument<Vector2>("ColliderSize");
        }

        /// <summary>
        ///     Gets or sets the offset of the bounding box's center from the actor location.
        /// </summary>
        public Vector2 Offset
        {
            get { return _offset; }

            set
            {
                if (_offset == value)
                    return;

                _offset = value;
                UpdatePhysicsShape();
            }
        }

        /// <summary>
        ///     Gets or sets the size of the bounding box.
        /// </summary>
        public Vector2 Size
        {
            get { return _size; }

            set
            {
                if (_size == value)
                    return;

                _size = value;
                UpdatePhysicsShape();
            }
        }

        /// <summary>
        ///     Get a bounding box in world coordinates based on the current properties.
        /// </summary>
        public override BoundingBox WorldBoundingBox
        {
            get
            {
                Vector2 center = Transform.Location + _offset;
                Vector2 extent = _size / 2;

                BoundingBox bb;
                bb.Min = new Vector3(center - extent, 0);
                bb.Max = new Vector3(center + extent, 0);
                return bb;
            }
        }

        public override bool Intersects(BoundingBox other, Vector2 offset = default(Vector2), float expand = 0)
        {
            if (other == null)
                throw new ArgumentNullException(nameof(other));

            BoundingBox wbb = WorldBoundingBox;
            wbb.Min += new Vector3(offset, 0);
            Expand(ref wbb, expand);
            return wbb.Intersects(other);
        }

        public override bool Intersects(BoundingSphere other, Vector2 offset = default(Vector2), float expand = 0)
        {
            if (other == null)
                throw new ArgumentNullException(nameof(other));

            BoundingBox wbb = WorldBoundingBox;
            wbb.Min += new Vector3(offset, 0);
            Expand(ref wbb, expand);
            return wbb.Intersects(other);
        }

        public override void WriteDelta(DeltaWriter writer, ReplicationContext context)
        {
            base.WriteDelta(writer, context);

            writer.Write(_offset);
            writer.Write(_size);
        }

        public override void ReadDelta(DeltaReader reader, ReplicationContext context)
        {
            base.ReadDelta(reader, context);

            reader.Read(ref _offset);
            reader.Read(ref _size);
        }

        protected override Shape CreatePhysicsShape()
        {
            _physicsShape = new PolygonShape(CreateVertices(), 0);
            return _physicsShape;
        }

        private void UpdatePhysicsShape()
        {
            if (_physicsShape != null)
                _physicsShape.Vertices = CreateVertices();
        }

        private Vertices CreateVertices()
        {
            Vertices vertices = PolygonTools.CreateRectangle(PhysicsSettings.FromTiles(_size.X) / 2,
                                                             PhysicsSettings.FromTiles(_size.Y) / 2);

            vertices.Translate(PhysicsSettings.FromTiles(_offset));

            return vertices;
        }
    }
}