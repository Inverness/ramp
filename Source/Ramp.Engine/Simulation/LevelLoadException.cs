using System;

namespace Ramp.Simulation
{
    /// <summary>
    ///     Thrown when an error is encountered while trying to load a new level.
    /// </summary>
    public class LevelLoadException : Exception
    {
        public LevelLoadException(string name = null, string message = null, Exception innerException = null)
            : base(message ?? "Unspecified Error", innerException)
        {
            Name = name;
        }

        public string Name { get; private set; }

        public override string Message
        {
            get
            {
                if (string.IsNullOrEmpty(Name))
                    return "An error occurred while loading a level: " + base.Message;
                return string.Format("An error occurred while loading the level {0}: {1}", Name, base.Message);
            }
        }
    }
}