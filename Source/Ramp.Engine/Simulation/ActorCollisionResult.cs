﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace Ramp.Simulation
{
    /// <summary>
    ///     Holds the results of an actor collision.
    /// </summary>
    public struct ActorCollisionResult
    {
        /// <summary>
        ///     The collider that instigated the collision. Will be null if no collision occured.
        /// </summary>
        public Collider Instigator;

        /// <summary>
        ///     A world bounding box representing the destination movement.
        /// </summary>
        public BoundingBox Destination;

        /// <summary>
        ///     A collider that was blocked and prevents movement. Will be null if no block occured.
        /// </summary>
        public Collider BlockTarget;

        /// <summary>
        ///     A list of colliders that will start intersecting with the collider at the destination.
        /// </summary>
        public List<Collider> BeginOverlaps;

        /// <summary>
        ///     A list of colliders that will stop intersecting with the collider at the destination.
        /// </summary>
        public List<Collider> EndOverlaps;

        public Vector2 HitNormal;

        public Vector2 NewDelta;

        public bool IsBlocking => BlockTarget != null;

        /// <summary>
        ///     Clears all collision data.
        /// </summary>
        public void Reset()
        {
            Instigator = null;
            Destination = new BoundingBox();
            BlockTarget = null;

            if (BeginOverlaps == null)
                BeginOverlaps = new List<Collider>();
            else
                BeginOverlaps.Clear();

            if (EndOverlaps == null)
                EndOverlaps = new List<Collider>();
            else
                EndOverlaps.Clear();
        }
    }
}