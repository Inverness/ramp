using System;

namespace Ramp.Simulation
{
    /// <summary>
    ///     Base class for events involving an actor
    /// </summary>
    public class ActorEventArgs : EventArgs
    {
        public ActorEventArgs(Actor actor)
        {
            Actor = actor;
        }

        public Actor Actor { get; private set; }
    }
}