﻿using System;

namespace Ramp.Simulation
{
    /// <summary>
    ///     An attribute that signifies that a component type should not be replicated to clients.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class ServerComponentAttribute : Attribute
    {
    }
}