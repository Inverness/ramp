﻿namespace Ramp.Simulation
{
    ///// <summary>
    /////     A delegate that will handle a signal sent from a SignalComponent.
    ///// </summary>
    ///// <param name="sender"> The component that sent the signal. </param>
    ///// <param name="name"> The name of the signal. </param>
    ///// <param name="args">  </param>
    //public delegate void SignalHandler(SignalComponent sender, string name, object[] args);

    ///// <summary>
    /////     Allows actors to send signals between each other
    ///// </summary>
    //public sealed class SignalComponent : ActorComponent
    //{
    //    public static readonly object[] EmptyArguments = new object[0];

    //    private readonly Dictionary<string, SignalHandler> _handlers =
    //        new Dictionary<string, SignalHandler>();

    //    /// <summary>
    //    ///     Adds a delegate to the handler list for a signal.
    //    /// </summary>
    //    /// <param name="name"> The signal name. </param>
    //    /// <param name="handler"> The handler. </param>
    //    public void AddHandler(string name, SignalHandler handler)
    //    {
    //        SignalHandler existing;
    //        if (_handlers.TryGetValue(name, out existing))
    //            _handlers[name] = existing + handler;
    //        else
    //            _handlers[name] = handler;
    //    }

    //    /// <summary>
    //    ///     Removes a delegate from the handler list of a signal.
    //    /// </summary>
    //    /// <param name="name"> The signal name. </param>
    //    /// <param name="handler"> The handler. </param>
    //    public bool RemoveHandler(string name, SignalHandler handler)
    //    {
    //        SignalHandler existing;
    //        if (!_handlers.TryGetValue(name, out existing))
    //            return false;

    //        // ReSharper disable DelegateSubtraction
    //        SignalHandler result = existing - handler;
    //        // ReSharper restore DelegateSubtraction

    //        if (result == null)
    //            _handlers.Remove(name);
    //        else
    //            _handlers[name] = result;

    //        return true;
    //    }

    //    /// <summary>
    //    ///     Check if a signal is handled.
    //    /// </summary>
    //    /// <param name="name"></param>
    //    /// <returns></returns>
    //    public bool IsHandled(string name)
    //    {
    //        return _handlers.ContainsKey(name);
    //    }

    //    /// <summary>
    //    ///     Send a signal to the specified actor.
    //    /// </summary>
    //    /// <param name="target"> The target actor. </param>
    //    /// <param name="name"> The signal name. </param>
    //    /// <param name="arg"> Optional argument. </param>
    //    /// <returns> True if the target has a signal component that can handle what is being sent. </returns>
    //    public bool Send(Actor target, string name, object[] args = null)
    //    {
    //        SignalComponent signalComponent = target.SignalComponent;
    //        if (signalComponent == null || !signalComponent.IsHandled(name))
    //            return false;

    //        signalComponent.Receive(this, name, args ?? EmptyArguments);
    //        return true;
    //    }

    //    /// <summary>
    //    ///     Send a signal to all actors in the specified level.
    //    /// </summary>
    //    /// <param name="target"> The target level. </param>
    //    /// <param name="name"> The signal name. </param>
    //    /// <param name="arg"> Optional argument. </param>
    //    /// <returns> True if any actors were able to handle the signal. </returns>
    //    public bool Send(Level target, string name, object[] args = null)
    //    {
    //        bool sent = false;
    //        foreach (Actor actor in target.Actors)
    //            sent = Send(actor, name, args) || sent;
    //        return sent;
    //    }

    //    private void Receive(SignalComponent sender, string name, object[] args)
    //    {
    //        SignalHandler handlers;
    //        if (!_handlers.TryGetValue(name, out handlers))
    //            return;
    //        Debug.Assert(handlers != null, "handlers != null");
    //        handlers(sender, name, args);
    //    }
    //}
}