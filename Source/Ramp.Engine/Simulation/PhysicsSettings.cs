using Microsoft.Xna.Framework;
using nkast.Aether.Physics2D.Dynamics;

namespace Ramp.Simulation
{
    /// <summary>
    ///     Provides settings and helper methods for interaction with the physics engine.
    /// </summary>
    public static class PhysicsSettings
    {
        public const Category VisibilityCategory = Category.Cat1;
        public const Category CameraCategory = Category.Cat2;
        public const Category WorldStaticCategory = Category.Cat3;
        public const Category WorldDynamicCategory = Category.Cat4;
        public const Category PawnCategory = Category.Cat4;

        public const float MetersToTilesMultiplier = 4f;
        public const float TilesToMetersMultiplier = 1f / 4f;

        public static float ToTiles(float value)
        {
            return value * MetersToTilesMultiplier;
        }

        public static Vector2 ToTiles(Vector2 value)
        {
            return value * MetersToTilesMultiplier;
        }

        public static float FromTiles(float value)
        {
            return value * TilesToMetersMultiplier;
        }

        public static Vector2 FromTiles(Vector2 value)
        {
            return value * TilesToMetersMultiplier;
        }
    }
}