using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Ramp.Simulation
{
    /// <summary>
    ///     This interface is provided to objects so they can render themselves when it is time.
    /// </summary>
    public interface ILevelObjectRenderer
    {
        /// <summary>
        ///     Number of pixels on a tile side. Tiles are always square.
        /// </summary>
        byte PixelsPerTile { get; }

        /// <summary>
        ///     Maximum number of layers that can be rendered.
        /// </summary>
        byte LayerCount { get; }

        /// <summary>
        ///     Draw a sprite on the screen.
        /// </summary>
        /// <param name="texture"> Desired texture. </param>
        /// <param name="position"> Position of the sprite in tile coordinates. This represents the center. </param>
        /// <param name="color"> Sprite color. Defaults to Color.White if null. </param>
        /// <param name="size"> Sprite size in tiles. Defaults to the size of the texture if null. </param>
        /// <param name="sourceRect"> Source area of the texture in pixels. </param>
        /// <param name="scale"> The fractional scale of the sprite from the texture center. </param>
        /// <param name="rotation"> The rotation of the sprite in radians. </param>
        /// <param name="renderMode"> The render mode to use for the sprite. </param>
        void DrawSprite(Texture2D texture, Vector2 position, Color? color = null, Vector2? size = null,
                        Rectangle? sourceRect = null, float scale = 1f, float rotation = 0f,
                        SpriteRenderMode renderMode = SpriteRenderMode.Default);

        /// <summary>
        ///		Draw text on the screen.
        /// </summary>
        /// <param name="text"> The text to draw. </param>
        /// <param name="position"> The position in tiles of the text origin. </param>
        /// <param name="color"> The text color. if null, uses Color.White. </param>
        /// <param name="fontName"> Unused. </param>
        /// <param name="centered"> True to move the text origin to vertical and horizontal center of the resulting text. </param>
        void DrawText(string text, Vector2 position, Color? color = null, string fontName = null, bool centered = false);
    }
}