using System;
using System.Diagnostics;
using Microsoft.Xna.Framework;

namespace Ramp.Simulation
{
    /// <summary>
    ///     Utility methods for working with movement.
    /// </summary>
    public sealed class MovementUtility
    {
        /// <summary>
        ///     A small floating point number: 1.0e-4
        /// </summary>
        public const float SmallNumber = 1.0e-4f;

        /// <summary>
        ///     A very small floating point number: 1.0e-8
        /// </summary>
        public const float VerySmallNumber = 1.0e-8f;

        private static readonly double s_radUp = Math.Atan2(0, -1);
        private static readonly double s_radLeft = Math.Atan2(-1, 0);
        private static readonly double s_radDown = Math.Atan2(0, 1);
        private static readonly double s_radRight = Math.Atan2(1, 0);
        private static readonly double s_radUpLeft = Math.Atan2(-1, -1);
        private static readonly double s_radDownLeft = Math.Atan2(-1, 1);
        private static readonly double s_radDownRight = Math.Atan2(1, 1);
        private static readonly double s_radUpRight = Math.Atan2(1, -1);
        private const float DiagMod = 0.7071068f; // diagonal limited movement modifier
        private const double RadDiff = Math.PI * 2 / 16;

        /// <summary>
        ///     Get a unit vector for the specified direction.
        /// </summary>
        /// <param name="direction"> A direction. </param>
        /// <returns> A Vector describing normalized movement in a direction. </returns>
        public static Vector2 GetVector(Direction direction)
        {
            var vec = new Vector2();

            switch (direction)
            {
                case Direction.None:
                    break;
                case Direction.Up:
                    vec.Y = -1;
                    break;
                case Direction.Left:
                    vec.X = -1;
                    break;
                case Direction.Down:
                    vec.Y = 1;
                    break;
                case Direction.Right:
                    vec.X = 1;
                    break;
                case Direction.UpLeft:
                    vec.X = -DiagMod;
                    vec.Y = -DiagMod;
                    break;
                case Direction.DownLeft:
                    vec.X = -DiagMod;
                    vec.Y = DiagMod;
                    break;
                case Direction.DownRight:
                    vec.X = DiagMod;
                    vec.Y = DiagMod;
                    break;
                case Direction.UpRight:
                    vec.X = DiagMod;
                    vec.Y = -DiagMod;
                    break;
            }

            return vec;
        }

        /// <summary>
        ///     Get the direction for the specified vector.
        /// </summary>
        /// <param name="vector"> A vector. </param>
        /// <param name="threshold"> A vector length threshold. </param>
        /// <returns>
        ///     A direction depending on the specified vector, or None if the length of the vector is below or
        ///     equal to the threshold.
        /// </returns>
        public static Direction GetDirection(Vector2 vector, float threshold = 0.0f)
        {
            if (vector.Length() <= threshold)
                return Direction.None;

            double d = Math.Atan2(vector.X, vector.Y);
            if (double.IsNaN(d))
                throw new ArgumentOutOfRangeException(nameof(vector), "Atan2 returned NaN");

            if (Math.Abs(s_radUp - d) <= RadDiff)
                return Direction.Up;
            if (Math.Abs(s_radLeft - d) <= RadDiff)
                return Direction.Left;
            if (Math.Abs(s_radDown - d) <= RadDiff)
                return Direction.Down;
            if (Math.Abs(s_radRight - d) <= RadDiff)
                return Direction.Right;
            if (Math.Abs(s_radUpLeft - d) <= RadDiff)
                return Direction.UpLeft;
            if (Math.Abs(s_radDownLeft - d) <= RadDiff)
                return Direction.DownLeft;
            if (Math.Abs(s_radDownRight - d) <= RadDiff)
                return Direction.DownRight;
            if (Math.Abs(s_radUpRight - d) <= RadDiff)
                return Direction.UpRight;
            return Direction.Up;
        }

        public static bool IsCardinal(Direction direction)
        {
            return direction >= Direction.Up && direction <= Direction.Right;
        }

        public static Vector2 ClampLength(Vector2 vector, float maxLength)
        {
            if (maxLength < SmallNumber)
            {
                vector.X = 0;
                vector.Y = 0;
            }
            else
            {
                // Inlined LengthSquared()
                float lenSq = vector.X * vector.X + vector.Y * vector.Y;

                if (lenSq > maxLength * maxLength)
                {
                    // Inlined Normalize()
                    float multiplier = 1f / (float) Math.Sqrt(lenSq) * maxLength;
                    vector.X *= multiplier;
                    vector.Y *= multiplier;
                }
            }

            return vector;
        }
    }
}