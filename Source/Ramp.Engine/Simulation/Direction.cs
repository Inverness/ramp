namespace Ramp.Simulation
{
    /// <summary>
    ///     Describes directions for movement.
    /// </summary>
    public enum Direction : byte
    {
        None,
        Up,
        Left,
        Down,
        Right,
        UpLeft,
        DownLeft,
        DownRight,
        UpRight
    }
}