namespace Ramp.Simulation
{
    /// <summary>
    ///     Services that provides management and updating of level objects. Existing levels use this service to
    ///     acquire a content client for their local content, along with loading target levels for actor level changing.
    /// </summary>
    public interface IWorldManagerService
    {
        /// <summary>
        ///     Gets or sets whether world updating is enabled.
        /// </summary>
        bool Enabled { get; set; }

        World World { get; }
    }
}