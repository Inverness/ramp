using System;

namespace Ramp.Simulation
{
    public struct TimerHandle : IEquatable<TimerHandle>
    {
        public static readonly TimerHandle Invalid = new TimerHandle();

        private readonly uint _value;

        internal TimerHandle(uint value)
        {
            _value = value;
        }

        public bool Valid => _value != 0;

        public override int GetHashCode()
        {
            return (int) _value;
        }

        public override bool Equals(object obj)
        {
            if (obj is TimerHandle)
                return Equals((TimerHandle) obj);
            return false;
        }

        public bool Equals(TimerHandle other)
        {
            return _value == other._value;
        }

        public static bool operator ==(TimerHandle left, TimerHandle right)
        {
            return left._value == right._value;
        }

        public static bool operator !=(TimerHandle left, TimerHandle right)
        {
            return left._value != right._value;
        }
    }
}