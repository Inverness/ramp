﻿using System;
using System.Reflection;

namespace Ramp.Simulation
{
    /// <summary>
    ///		Specifies a component field that will be bound to a dependent component in the same actor.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class BindAttribute : Attribute
    {
        public BindAttribute(bool optional = false)
        {
            Optional = optional;
        }

        public bool Optional { get; private set; }

        internal FieldInfo FieldInfo { get; set; }
    }
}