﻿using System;
using System.Diagnostics;
using System.Reflection;
using Lidgren.Network;
using Ramp.Network;

// ReSharper disable ConvertToAutoProperty

namespace Ramp.Simulation
{
    public sealed partial class Actor : IReplica
    {
        private const byte MaxRpcArgs = 16;
        private const byte EventMessageType = 51;
        private const byte ComponentRpcMessageType = 52;

        private NetRole _role = NetRole.Authority;
        private NetRole _remoteRole = NetRole.None;
        private uint _replicaId;
        private ushort _replicaUpdateRate;

        /// <summary>
        ///     The ID assigned to this actor for network replication.
        /// </summary>
        public uint ReplicaId => _replicaId;

        /// <summary>
        ///     The role of this actor in the local instance. This will always be Authority on the server, and vice versa
        ///     on clients. Cannot be None.
        /// </summary>
        public NetRole Role => _role;

        /// <summary>
        ///     The role of this actor on the remote machine. An actor will only be replicated if this is not None.
        /// </summary>
        public NetRole RemoteRole => _remoteRole;

        /// <summary>
        ///     The replica service tracking this replica.
        /// </summary>
        public IReplicaService ReplicaService => _replicaId != 0 ? World.ReplicaService : null;

        public TimeSpan ReplicaUpdateRate
        {
            get { return TimeSpan.FromMilliseconds(_replicaUpdateRate); }

            set
            {
                double ms = value.TotalMilliseconds;
                if (ms < 0 || ms > ushort.MaxValue)
                    throw new ArgumentOutOfRangeException(nameof(value),
                                                          "replica update rate must be between 0 and 65535 milliseconds");
                _replicaUpdateRate = (ushort) ms;
            }
        }

        /// <summary>
        ///     True if the current actor is on the server and is being replicated to clients.
        /// </summary>
        public bool ServerSide => _remoteRole != NetRole.None && _role == NetRole.Authority;

        /// <summary>
        ///     True if the current actor is on the client and being replicated from the server.
        /// </summary>
        public bool ClientSide => _role != NetRole.Authority;

        public void OnTrack(IReplicaService service, uint id)
        {
            Debug.Assert(service == World.ReplicaService);
            Debug.Assert(id != 0 && _replicaId == 0);
            Debug.Assert(RemoteRole != NetRole.None);
            _replicaId = id;
        }

        public void OnUntrack(IReplicaService service)
        {
            Debug.Assert(service == World.ReplicaService);
            Debug.Assert(_replicaId != 0);
            _replicaId = 0;
        }

        public void WriteDelta(DeltaWriter writer, ReplicationContext context)
        {
            // Serialize our variables and component variables
            writer.Write(Level.Name);
            writer.Write(_canChangeLevels);

            // Get the last written component count to determine how many need to be serialized
            byte lastSentComponentCount = writer.GetRecentValue<byte>();

            writer.Write(_componentCount);

            // Component names are only written when the component count has changed.
            for (int i = lastSentComponentCount; i < _componentCount; i++)
            {
                ActorComponentType ct = ActorComponentType.GetType(_components[i].GetType());
                Debug.Assert(ct != null, "ct != null");
                writer.Buffer.Write(ct.Name);
            }

            // It's safe to serialize components this way because components can only be added, not removed, and
            // components are not sorted in an update order.
            for (int i = 0; i < _componentCount; i++)
                _components[i].WriteDelta(writer, context);
        }

        public void ReadDelta(DeltaReader reader, ReplicationContext context)
        {
            // A level name difference will trigger a level change on the client
            string levelName = Level.Name;
            bool levelChanged = reader.Read(ref levelName);
            reader.Read(ref _canChangeLevels);

            byte remoteCount = 0;
            if (reader.Read(ref remoteCount))
            {
                try
                {
                    _replicating = true;

                    if (ComponentCapacity < remoteCount)
                        ComponentCapacity = remoteCount;

                    // If the remote component count changed then that means the missing component names were also
                    // added to the buffer
                    for (int i = _componentCount; i < remoteCount; i++)
                    {
                        string typeName = reader.Buffer.ReadString();
                        ActorComponentType ct = ActorComponentType.GetType(typeName);
                        if (ct == null)
                        {
                            s_log.Error("component replication: type not found: " + typeName);
                            continue;
                        }

                        // Create a dummy component if the type is server-only. This is because all component indeces
                        // must match the server and be valid.
                        AddComponent(ct.IsServerOnly ? typeof(DummyComponent) : ct.ClrType);
                    }
                }
                finally
                {
                    _replicating = false;
                }
            }

            for (int i = 0; i < _componentCount; i++)
                _components[i].ReadDelta(reader, context);

            if (levelChanged && levelName != null)
                HandleDeserializeLevelChange(levelName);
        }

        /// <summary>
        ///     Check if a client owns the current actor.
        /// </summary>
        /// <param name="connection"> A replica client. </param>
        /// <returns> True if the client is not null and the topmost owning player is the client. </returns>
        public bool IsNetOwner(IReplicaConnection connection)
        {
            return connection != null && ReferenceEquals(PlayerPawnBehavior.GetTopPlayer(this), connection);
        }

        /// <summary>
        ///     Test if the current actor is relevant to a client.
        /// </summary>
        /// <param name="connection"> A client to test. </param>
        /// <returns></returns>
        public bool IsRelevantTo(IReplicaConnection connection)
        {
            if (connection == null)
                throw new ArgumentNullException(nameof(connection));

            if (Static)
                return false;

            // More advanced testing will come in the future, but for now, all actors in the same level are relevant.
            return ((IPlayer) connection).Actor.Level == Level;
        }

        void IReplica.OnReceiveMessage(NetIncomingMessage message, IReplicaConnection source)
        {
            Validate.ArgumentNotNull(message, nameof(message));

            byte type = message.ReadByte();
            switch (type)
            {
                case EventMessageType:
                    byte index = message.ReadByte();
                    byte messageId = message.ReadByte();

                    if (index == byte.MaxValue)
                        OnReceiveActorMessage(messageId, message, source);
                    else if (index >= _componentCount)
                        s_log.Error("invalid component data message index: " + index);
                    else
                        _components[index].OnReceiveMessage(messageId, message, source);
                    break;

                case ComponentRpcMessageType:
                    OnReceiveRpc(message);
                    break;

                default:
                    s_log.Error("actor received unknown message type: " + type);
                    break;
            }
        }

        internal void SetRoles(NetRole role, NetRole remoteRole)
        {
            Debug.Assert(role != NetRole.None);
            Debug.Assert(role == NetRole.Authority || remoteRole == NetRole.Authority);

            bool remoteChanged = remoteRole != _remoteRole;

            _role = role;
            _remoteRole = remoteRole;

            if (remoteChanged)
                World.OnRemoteRoleChanged(this);
        }

        private void OnReceiveActorMessage(byte id, NetBuffer buffer, IReplicaConnection source)
        {
            switch (id)
            {
                default:
                    s_log.Error("unhandled actor event: " + id);
                    break;
            }
        }

        private async void HandleDeserializeLevelChange(string levelName)
        {
            try
            {
                await ChangeLevelAsync(levelName, this.GetLocation().GetValueOrDefault());
            }
            catch (InvalidOperationException ex)
            {
                s_log.Error(ex, "ChangeLevelAsync() failed during deserialization.");
            }
        }
        
        /// <summary>
        ///     Create an event message for the specified component.
        /// </summary>
        /// <param name="component"> A component. </param>
        /// <param name="id"> ID of the component event. </param>
        /// <returns> The new message. </returns>
        internal NetOutgoingMessage CreateMessage(ActorComponent component, byte id)
        {
            if (_replicaId == 0)
                throw new InvalidOperationException("no replica service");

            byte? index = IndexOfComponent(component);
            Debug.Assert(index.HasValue, "index.HasValue");

            NetOutgoingMessage buffer = World.ReplicaService.CreateReplicaMessage(this);
            buffer.Write(EventMessageType);
            buffer.Write(index.Value);
            buffer.Write(id);
            return buffer;
        }

        /// <summary>
        ///     Send an event over the network, either to the server or the client that owns the current actor.
        /// </summary>
        /// <param name="buffer"> The outgoing message. </param>
        /// <param name="method"> The network delivery method. </param>
        /// <param name="connection">
        ///     Optional client when sending from server to client. If null, sends to the client
        ///     that owns the current actor.
        /// </param>
        /// <returns> True if sent. </returns>
        internal bool SendMessage(NetOutgoingMessage buffer, NetDeliveryMethod method, IReplicaConnection connection)
        {
            if (buffer == null)
                throw new ArgumentNullException(nameof(buffer));
            if (_replicaId == 0)
                throw new InvalidOperationException("no replica service");

            IReplicaConnection recipient = null;
            if (_role == NetRole.Authority)
            {
                // If client is not specified, use the client that owns this actor.
                if ((recipient = connection) == null)
                {
                    IPlayer player = PlayerPawnBehavior.GetTopPlayer(this);
                    if (player == null || (recipient = player.ReplicaConnection) == null)
                        return false; // no client to send to
                }
            }

            World.ReplicaService.SendReplicaMessage(buffer, recipient, method);

            return true;
        }

        /// <summary>
        ///     Send an RPC event to the remote replica on either the server or the client that owns this actor.
        /// </summary>
        /// <param name="component"> The component. </param>
        /// <param name="methodName"> The method name. </param>
        /// <param name="args"> The method arguments. </param>
        /// <param name="reliable"> Whether to use a reliable send. </param>
        /// <returns> True if the event was sent, false if on the server and the current actor is not owned by a client. </returns>
        internal bool SendRpc(ActorComponent component, string methodName, object[] args, bool reliable)
        {
            if (_replicaId == 0)
                throw new InvalidOperationException("no replica service");
            if (args.Length > MaxRpcArgs)
                throw new ArgumentException("too many arguments", nameof(args));

            // Retrieve the index of the method
            ActorComponentType ctype = ActorComponentType.GetType(component.GetType());
            ushort? callableIndex = ctype.GetRemoteCallableIndex(methodName);
            if (!callableIndex.HasValue)
                throw new ArgumentException("Specified method is not remote callable: " + methodName, nameof(methodName));

            // If we're on the server, we need to identify the client we'll be sending to
            IReplicaConnection recipient = null;
            if (_role == NetRole.Authority)
            {
                IPlayer player = PlayerPawnBehavior.GetTopPlayer(this);
                if (player == null)
                {
                    s_log.Error("SendRpc() failed, no owning player for server to client call");
                    return false;
                }

                recipient = player.ReplicaConnection;
                if (recipient == null)
                {
                    s_log.Error("SendRpc() failed, owning player does not have a replica connection");
                    return false;
                }
            }

            IReplicaService replicaService = World.ReplicaService;
            var deliveryMethod = reliable ? NetDeliveryMethod.ReliableSequenced : NetDeliveryMethod.UnreliableSequenced;
            byte? componentIndex = IndexOfComponent(component);
            Debug.Assert(componentIndex.HasValue, "index.HasValue");

            NetOutgoingMessage buffer = replicaService.CreateReplicaMessage(this);
            buffer.Write(ComponentRpcMessageType);
            buffer.Write(componentIndex.Value);
            buffer.Write(callableIndex.Value);
            buffer.Write((byte) args.Length);

            if (args.Length != 0)
                PrimitiveSerializer.WriteArray(buffer, args);

            replicaService.SendReplicaMessage(buffer, recipient, deliveryMethod);

            return true;
        }

        private void OnReceiveRpc(NetBuffer buffer)
        {
            byte componentIndex = buffer.ReadByte();
            ushort methodIndex = buffer.ReadUInt16();
            byte argCount = buffer.ReadByte();

            if (componentIndex >= _componentCount)
            {
                s_log.Error("rpc receive: invalid component index");
                return;
            }

            ActorComponent component = _components[componentIndex];
            ActorComponentType ctype = ActorComponentType.GetType(component.GetType());
            MethodInfo methodInfo = ctype.GetRemoteCallableMethod(methodIndex);

            if (methodInfo == null)
            {
                s_log.Error("rpc receive: invalid method index");
                return;
            }

            var args = new object[argCount];
            if (!PrimitiveSerializer.ReadArray(buffer, args))
            {
                s_log.Error("rpc receive: error reading arguments");
                return;
            }

            methodInfo.Invoke(component, args);
        }
    }

    public class RoleChangeEventArgs : EventArgs
    {
        public RoleChangeEventArgs(NetRole previousRole)
        {
            Previous = previousRole;
        }

        public NetRole Previous { get; private set; }
    }
}