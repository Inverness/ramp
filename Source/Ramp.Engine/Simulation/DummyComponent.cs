﻿namespace Ramp.Simulation
{
    /// <summary>
    ///     A component added on the client when the serverside component is not to be replicated.
    /// </summary>
    public sealed class DummyComponent : ActorComponent
    {
    }
}