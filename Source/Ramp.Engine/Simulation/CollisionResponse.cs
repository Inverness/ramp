﻿namespace Ramp.Simulation
{
    public enum CollisionResponse : byte
    {
        Ignore,
        Overlap,
        Block
    }
}