﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using NLog;
using Lidgren.Network;
using Microsoft.Xna.Framework;
using Ramp.Data;
using Ramp.Naming;
using Ramp.Network;
using Ramp.Session;
using Ramp.Simulation.Tmx;
using Ramp.Utilities.Collections;

namespace Ramp.Simulation
{
    /// <summary>
    ///     A game world consisting of multiple levels.
    /// </summary>
    public sealed class World : NamedObject, INamedLoader
    {
        private static readonly Logger s_log = LogManager.GetCurrentClassLogger();

        private readonly List<Level> _updateList = new List<Level>();
        private readonly Dictionary<string, Level> _levels = new Dictionary<string, Level>();

        private readonly List<Actor> _disposeList = new List<Actor>();

        private readonly RampContentManager _content;

        //private readonly Dictionary<string, ActorArchetype> _archetypeCache = new Dictionary<string, ActorArchetype>(); 

        public World(IServiceProvider provider, string name)
            : base(name)
        {
            ServiceProvider = provider;
            DispatcherService = provider.GetRequiredService<IDispatcherService>();
            FileService = provider.GetRequiredService<IFileService>();
            ReplicaService = provider.GetService<IReplicaService>();

            PersistentLevel = new Level("PersistentLevel", this, new Point(1, 1), false);
            AddLevel(PersistentLevel);
            PersistentLevel.EndInitialization();

            if (ReplicaService != null)
            {
                ReplicaService.CreateReplicaHandler = OnCreateReplica;
                ReplicaService.DestroyReplicaHandler = OnDestroyReplica;
                ReplicaService.WriteCreationIdHandler = OnWriteCreationId;
            }

            _content = new RampContentManager(provider, "World");
        }

        /// <summary>
        ///     Raised when an actor is spawned.
        /// </summary>
        public event TypedEventHandler<World, ActorEventArgs> ActorSpawned;

        /// <summary>
        ///     Raised when an actor is destroyed, but before cleanup.
        /// </summary>
        public event TypedEventHandler<World, ActorEventArgs> ActorDisposed;

        /// <summary>
        ///     Raised before an actor changes level.
        /// </summary>
        public event TypedEventHandler<World, ActorLevelChangingEventArgs> ActorLevelChanging;

        /// <summary>
        ///     Raised after an actor changes level.
        /// </summary>
        public event TypedEventHandler<World, ActorEventArgs> ActorLevelChanged;

        /// <summary>
        ///     Raised after a level is added.
        /// </summary>
        public event TypedEventHandler<World, LevelEventArgs> LevelAdded;

        /// <summary>
        ///     Raised before a level is removed.
        /// </summary>
        public event TypedEventHandler<World, LevelEventArgs> LevelRemoving;

        public IDispatcherService DispatcherService { get; }

        public IFileService FileService { get; }

        public IReplicaService ReplicaService { get; }

        public IServiceProvider ServiceProvider { get; }

        /// <inheritdoc />
        public bool IsUpdatingLevels { get; private set; }

        /// <inheritdoc />
        public Level UpdateTarget { get; set; }

        /// <inheritdoc />
        public IReadOnlyDictionary<string, Level> Levels => _levels;

        public Level PersistentLevel { get; }

        public GameMaster GameMaster { get; internal set; }

        public TimerManager TimerManager { get; } = new TimerManager();

        /// <inheritdoc />
        public void Update(TimeSpan elapsed)
        {
            ProcessDisposeList();

            try
            {
                IsUpdatingLevels = true;

                // Can be mutated during update, so don't use an enumerator.
                for (int i = 0; i < _updateList.Count; i++)
                    _updateList[i].Update(elapsed);
            }
            finally
            {
                IsUpdatingLevels = false;
            }

            TimerManager.Update(elapsed);
        }

        /// <summary>
        ///     Load or get an existing level.
        /// </summary>
        /// <param name="name"> A level name. </param>
        /// <returns> A task that completes when the level has been loaded. </returns>
        public Task<Level> LoadLevelAsync(string name)
        {
            Validate.ArgumentNotNull(name, "name");

            return Directory.LoadAsync<Level>(this, name);
        }

        /// <summary>
        ///     Unloads all levels in the world.
        /// </summary>
        /// <param name="clearPersistent"> Whether to clear actors from the persistent level. </param>
        public void DisposeLevels(bool clearPersistent)
        {
            _levels.Values.Where(l => l != PersistentLevel).ToList().ForEach(level => level.Dispose());

            if (clearPersistent)
                PersistentLevel.ClearActors(true);

            //Debug.Assert(_levels.Count == 1 && _levels.Values.First() == PersistentLevel);
        }

        public void AddLevel(Level level)
        {
            Validate.ArgumentNotNull(level, "level");
            Validate.ValidOperation(!IsUpdatingLevels, "Cannot add or remove levels during update");

            // None of the actors already in the level at this point should be configured for replication.
            Debug.Assert(level.Actors.All(a => a.Static && a.Role == NetRole.Authority && a.RemoteRole == NetRole.None));

            _updateList.Add(level);
            _levels.Add(level.Name, level);

            level.Disposing += OnLevelDisposing;

            LevelAdded?.Invoke(this, new LevelEventArgs(level));
        }

        public void RemoveLevel(Level level)
        {
            Validate.ValidOperation(!IsUpdatingLevels, "Cannot add or remove levels during update");

            level.Disposing -= OnLevelDisposing;

            // Actors can only exist while the level is in a world
            level.ClearActors(false);
            ProcessDisposeList();

            LevelRemoving?.Invoke(this, new LevelEventArgs(level));

            _updateList.Remove(level);
            _levels.Remove(level.Name);

            Debug.Assert(level.Actors.All(a => a.ReplicaService == null));
        }

        async Task<NamedObject> INamedLoader.TryLoad(Type type, NamedObject outer, string name, CancellationToken ct)
        {
            Validate.ArgumentNotNull(type, "type");
            Validate.ArgumentNotNull(name, "name");

            if (type != typeof(Level) || outer != this)
                return null;

            Debug.Assert(!_levels.ContainsKey(name));

            if (ct.IsCancellationRequested)
                return null;

            FileManifestEntry entry = FileService.GetEntry(name);
            if (entry == null)
                throw new LevelLoadException(name, "Not found in the manifest");

            s_log.Info("Loading level: {0}", name);

            Asset<TmxLoader> loader = await Directory.LoadAsset<TmxLoader>(name);

            if (ct.IsCancellationRequested)
            {
                s_log.Warn("Load cancelled: {0}", name);
                return null;
            }

            s_log.Info("Creating level: {0}", name);

            Level level = loader.Target.CreateLevel(this, name);

            s_log.Info("Created level: {0}", name);

            _content.Unload<TmxLoader>(name);

            return level;
        }

        // Invoked by Level.SpawnActor
        internal Actor SpawnActor(Level level, Vector2? location, string archetypeName, Actor owner, string name,
                                  NetRole? role, NameValueDictionary args, bool isStatic, IEnumerable<Type> components)
        {
            Validate.ArgumentInRange(!role.HasValue || role.Value != NetRole.None, "role");

            // Ensure the actor name, archetype, and owner are valid
            name = name ?? Directory.GenerateName((archetypeName ?? "Actor"), "_");

            // Verify that the archetype exists if it was specified.
            ActorArchetype archetype;
            if (archetypeName != null)
            {
                archetype = Directory.Find<ActorArchetype>(archetypeName);
                if (archetype == null)
                    throw new ArgumentException("Invalid archetype: " + archetypeName, nameof(archetypeName));
            }
            else
            {
                archetype = null;
            }

            if (owner != null && owner.Status >= ActorStatus.Disposing)
                throw new ArgumentException("Owner has been disposed or is currently being disposed", nameof(owner));

            // Spawn the actor and set initial properties

            // Spawn arguments applying and component creation is done here.
            // This is also where the actor will start being tracked if Role is Authority and RemoteRole is set from
            // the spawn arguments or archetype. For actors being replicated, the Role should have been set to
            // Simulated or Autonomous on the previous line.
            var actor = new Actor(name, level, isStatic, args, archetype, owner, role, components);

            if (location.HasValue)
                actor.SetLocation(location.Value);

            Debug.Assert(actor.Role == NetRole.Authority || actor.RemoteRole == NetRole.Authority);

            ActorSpawned?.Invoke(this, new ActorEventArgs(actor));

            s_log.Debug("Spawned {0}", actor);

            return actor;
        }

        internal void AddToDisposeList(Actor actor)
        {
            _disposeList.Add(actor);
        }

        internal void ProcessDisposeList()
        {
            if (_disposeList.Count != 0)
            {
                for (int i = 0; i < _disposeList.Count; i++)
                    _disposeList[i].Dispose();
                _disposeList.Clear();
            }
        }

        internal void OnActorDisposing(Actor actor)
        {
            Debug.Assert(actor.World == this && !actor.IsDisposed);

            ActorDisposed?.Invoke(this, new ActorEventArgs(actor));

            UntrackReplica(actor);
        }

        internal void OnActorLevelChanging(Actor actor, Level newLevel)
        {
            ActorLevelChanging?.Invoke(this, new ActorLevelChangingEventArgs(actor, newLevel));
        }

        internal void OnActorLevelChanged(Actor actor)
        {
            ActorLevelChanged?.Invoke(this, new ActorEventArgs(actor));
        }

        internal void OnRemoteRoleChanged(Actor actor)
        {
            if (actor.Role == NetRole.Authority)
            {
                if (actor.RemoteRole != NetRole.None)
                    TrackReplica(actor);
                else
                    UntrackReplica(actor);
            }
        }

        // Invoked through Actor.ChangeLevelAsync().
        internal async Task ChangeActorLevelAsync(Actor actor, string destination, Vector2 location)
        {
            if (actor.World != this)
                throw new InvalidOperationException("Actor not owned by this world");

            if (!actor.CanChangeLevels)
                throw new InvalidOperationException("Illegal level change for actor: " + actor);

            if (actor.IsDisposed)
                throw new InvalidOperationException("Attempt to change level for disposed actor: " + actor);

            if (actor.IsLevelChanging)
                throw new InvalidOperationException("Actor is already changing level: " + actor);

            if (destination == actor.Level.Name)
            {
                actor.SetLocation(location);
                return;
            }

            bool[] states = Behavior.Disable(actor);
            try
            {
                // TODO: Loading screen

                Level destLevel = await LoadLevelAsync(destination);

                if (destLevel == null)
                {
                    s_log.Error("Level changed for actor {0} to {1} failed due to the level not being found", actor, destination);
                    return;
                }

                if (Directory.Find<Actor>(actor.Name, destLevel) != null)
                    throw new InvalidOperationException("Actor name collision with destination level: " + actor.Name);

                s_log.Debug("Beginning level change for actor {0} to {1}", actor, destination);

                // At this point we've verified that a level change can happen without errors.
                OnActorLevelChanging(actor, destLevel);

                actor.BeginLevelChange(destLevel);

                actor.Rename(actor.Name, destLevel);
                
                actor.EndLevelChange(location);

                OnActorLevelChanged(actor);

                s_log.Debug("Finished level change for actor {0}", actor);
            }
            finally
            {
                Behavior.Enable(actor, states);
            }
        }

        private void OnLevelDisposing(NamedObject sender, EventArgs e)
        {
            var level = (Level) sender;
            if (_updateList.Contains(level))
                RemoveLevel(level);
        }

        private void TrackReplica(Actor actor)
        {
            if (ReplicaService != null &&
                actor.ReplicaService == null &&
                !actor.Static &&
                actor.Role == NetRole.Authority)
            {
                ReplicaService.TrackReplica(actor);
            }
        }

        private void UntrackReplica(Actor actor)
        {
            if (ReplicaService != null && actor.ReplicaService == ReplicaService)
                ReplicaService.UntrackReplica(actor);
        }

        private async Task<IReplica> OnCreateReplica(uint id, NetRole role, NetBuffer buffer)
        {
            //throw new NotImplementedException();
            Debug.Assert(role == NetRole.Autonomous || role == NetRole.Simulated);

            string name = buffer.ReadStringNull();
            string levelName = buffer.ReadString();
            uint ownerId = buffer.ReadUInt32();
            Vector2 location = buffer.ReadVector2();
            string archetype = buffer.ReadStringNull();

            Actor owner = null;
            if (ownerId != 0)
            {
                owner = (Actor) ReplicaService.GetReplica(ownerId);
                if (owner == null)
                    s_log.Warn("Could not locate replica owner: Name={0}, OwnerID={1}", name, ownerId);
            }

            Level level = await LoadLevelAsync(levelName);
            Actor actor = level.SpawnActor(location, archetype, owner, name, role: role);
            Debug.Assert(actor.Role == role && actor.RemoteRole == NetRole.Authority && actor.ReplicaService == null);

            return actor;
        }

        private void OnDestroyReplica(IReplica replica)
        {
            ((Actor) replica).MarkForDispose();
        }

        private void OnWriteCreationId(IReplica replica, NetBuffer buffer)
        {
            var actor = (Actor) replica;

            // Write arguments that will be sent to Spawn() on the remote system
            // Should be no more than 128 bytes
            buffer.Write(actor.Name);
            buffer.Write(actor.Level.Name);
            buffer.Write(actor.Owner != null ? actor.Owner.ReplicaId : 0U);
            buffer.Write(actor.GetLocation().GetValueOrDefault());
            buffer.Write(actor.Archetype != null ? actor.Archetype.Name : null);
        }
    }
}