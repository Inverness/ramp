﻿namespace Ramp.Simulation
{
    /// <summary>
    ///     Describes an actor component that can be activated by interaction from a player.
    /// </summary>
    public interface IActivatable
    {
        void Activate(Actor instigator);
    }
}