﻿using System;
using Ramp.Network;
using Ramp.Utilities.Collections;

namespace Ramp.Simulation
{
    /// <summary>
    ///		 Represents a player that controls an actor.
    /// </summary>
    public interface IPlayer
    {
        /// <summary>
        ///     Raised before Actor changes.
        /// </summary>
        event TypedEventHandler<IPlayer, PlayerActorChangingEventArgs> ActorChanging;

        /// <summary>
        ///     Raised when Actor changes.
        /// </summary>
        event TypedEventHandler<IPlayer, EventArgs> ActorChanged;

        /// <summary>
        ///     Raised after the actor's level changes.
        /// </summary>
        event TypedEventHandler<IPlayer, PlayerActorLevelChangedEventArgs> ActorLevelChanged;

        /// <summary>
        ///     Gets a dictionary of arbitrary data stored persistently with the player across sessions.
        /// </summary>
        NameValueDictionary Data { get; }

        /// <summary>
        ///     Gets the actor this player is currently controlling.
        /// </summary>
        Actor Actor { get; }

        /// <summary>
        ///     Gets this player's replica connection if it has one.
        /// </summary>
        IReplicaConnection ReplicaConnection { get; }
    }
}