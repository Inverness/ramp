﻿using System;
using Microsoft.Xna.Framework;
using Ramp.Network;
using nkast.Aether.Physics2D.Collision.Shapes;

namespace Ramp.Simulation
{
    public sealed class SphereCollider : Collider
    {
        private Vector2 _offset;
        private float _radius;
        private CircleShape _physicsShape;

        public SphereCollider()
            : base(ColliderShape.Sphere)
        {
            _offset = Actor.GetSpawnArgument<Vector2>("ColliderOffset");
            _radius = Actor.GetSpawnArgument<float>("ColliderRadius");
        }

        /// <summary>
        ///     The offset of the center of the circle from the actor's location.
        /// </summary>
        public Vector2 Offset
        {
            get { return _offset; }

            set
            {
                _offset = value;
                UpdatePhysicsShape();
            }
        }

        /// <summary>
        ///     The radius of the circle in tiles.
        /// </summary>
        public float Radius
        {
            get { return _radius; }

            set
            {
                _radius = value;
                UpdatePhysicsShape();
            }
        }

        public override BoundingBox WorldBoundingBox
        {
            get
            {
                BoundingBox bb;
                bb.Min = new Vector3(Transform.Location + _offset - new Vector2(_radius), 0);
                bb.Max = bb.Min + new Vector3(new Vector2(_radius * 2), 0);
                return bb;
            }
        }

        /// <summary>
        ///     Get a bounding sphere in world coordinates based on the current properties.
        /// </summary>
        public BoundingSphere WorldBoundingSphere
        {
            get
            {
                BoundingSphere bs;
                bs.Center = new Vector3(Transform.Location + _offset, 0);
                bs.Radius = _radius;
                return bs;
            }
        }

        public override bool Intersects(BoundingBox other, Vector2 offset = default(Vector2), float expand = 0)
        {
            if (other == null)
                throw new ArgumentNullException(nameof(other));

            BoundingSphere wbs = WorldBoundingSphere;
            wbs.Center += new Vector3(offset, 0);
            Expand(ref wbs, expand);
            return wbs.Intersects(other);
        }

        public override bool Intersects(BoundingSphere other, Vector2 offset = default(Vector2), float expand = 0)
        {
            if (other == null)
                throw new ArgumentNullException(nameof(other));

            BoundingSphere wbs = WorldBoundingSphere;
            wbs.Center += new Vector3(offset, 0);
            Expand(ref wbs, expand);
            return wbs.Intersects(other);
        }

        public override void WriteDelta(DeltaWriter writer, ReplicationContext context)
        {
            base.WriteDelta(writer, context);

            writer.Write(_offset);
            writer.Write(_radius);
        }

        public override void ReadDelta(DeltaReader reader, ReplicationContext context)
        {
            base.ReadDelta(reader, context);

            reader.Read(ref _offset);
            reader.Read(ref _radius);
        }

        protected override Shape CreatePhysicsShape()
        {
            _physicsShape = new CircleShape(PhysicsSettings.FromTiles(_radius), 0)
            {
                Position = PhysicsSettings.FromTiles(_offset)
            };
            return _physicsShape;
        }

        private void UpdatePhysicsShape()
        {
            if (_physicsShape != null)
            {
                _physicsShape.Position = PhysicsSettings.FromTiles(_offset);
                _physicsShape.Radius = PhysicsSettings.FromTiles(_radius);
            }   
        }
    }
}