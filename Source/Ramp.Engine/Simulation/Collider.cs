using System;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using Ramp.Network;
using Ramp.Simulation.Tiling;
using nkast.Aether.Physics2D.Collision.Shapes;
using nkast.Aether.Physics2D.Dynamics;
using nkast.Aether.Physics2D.Dynamics.Contacts;

namespace Ramp.Simulation
{
    /// <summary>
    ///     Specifies the limited possible shapes of colliders used in a level.
    /// </summary>
    public enum ColliderShape : byte
    {
        Box,
        Sphere
    }

    /// <summary>
    ///     Abstract base class for components that implement collision.
    /// </summary>
    public abstract class Collider : Behavior
    {
        private int _colliderListIndex;
        private readonly TransformComponent _transform;
        private readonly MovementBehavior _movement;
        private readonly List<Collider> _overlappingColliders = new List<Collider>();
        private readonly ColliderShape _shape;
        private readonly BodyType _bodyType;
        private byte? _layer;
        private Body _physicsBody;
        private Fixture _physicsFixture;
        private readonly Collider _master;
        private CollisionResponse _visibilityResponse;
        private CollisionResponse _cameraResponse;
        private CollisionResponse _worldStaticResponse;
        private CollisionResponse _worldDynamicResponse;
        private CollisionResponse _pawnResponse;

        protected Collider(ColliderShape shape)
        {
            GetComponentRequired(out _transform);
            if (GetComponent(out _movement))
                _movement.Active = false;
            if (!GetComponent(out _master))
                _master = this;

            _visibilityResponse = Actor.GetSpawnArgument("VisibilityResponse", CollisionResponse.Ignore);
            _cameraResponse = Actor.GetSpawnArgument("CameraResponse", _visibilityResponse);
            _worldStaticResponse = Actor.GetSpawnArgument("WorldStaticResponse", CollisionResponse.Ignore);
            _worldDynamicResponse = Actor.GetSpawnArgument("WorldDynamicResponse", CollisionResponse.Ignore);
            _pawnResponse = Actor.GetSpawnArgument("PawnResponse", CollisionResponse.Ignore);
            _bodyType = Actor.GetSpawnArgument("ColliderBodyType", _movement != null ? BodyType.Dynamic : BodyType.Static);
            _layer = Actor.GetSpawnArgument<byte?>("ColliderLayer");

            _shape = shape;
            _colliderListIndex = Level.AddCollider(this);
        }

        /// <summary>
        ///     Raised when this collider blocks another.
        /// </summary>
        public event TypedEventHandler<Collider, ActorCollisionEventArgs> ActorBlocked;

        /// <summary>
        ///     Raised when this collider begins overlapping another.
        /// </summary>
        public event TypedEventHandler<Collider, ActorCollisionEventArgs> ActorOverlapBegan;

        /// <summary>
        ///     Raised when this collider ends overlapping another.
        /// </summary>
        public event TypedEventHandler<Collider, ActorCollisionEventArgs> ActorOverlapEnded;

        /// <summary>
        ///     Raise when this collider is blocked by the world.
        /// </summary>
        public event TypedEventHandler<Collider, TileCollisionEventArgs> WorldBlocked;

        /// <summary>
        ///     Gets the collider's shape.
        /// </summary>
        public ColliderShape Shape => _shape;

        /// <summary>
        ///     Gets the body type for the physics simulation.
        /// </summary>
        public BodyType BodyType => _bodyType;

        /// <summary>
        ///     Collision layer to be used.
        /// </summary>
        public byte? Layer
        {
            get { return _layer; }

            set { _layer = value; }
        }

        public CollisionResponse VisibilityResponse
        {
            get { return _visibilityResponse; }

            set
            {
                _visibilityResponse = value;
                UpdatePhysicsCategories();
            }
        }

        public CollisionResponse CameraResponse
        {
            get { return _cameraResponse; }

            set
            {
                _cameraResponse = value;
                UpdatePhysicsCategories();
            }
        }

        public CollisionResponse WorldStaticResponse
        {
            get { return _worldStaticResponse; }

            set
            {
                _worldStaticResponse = value;
                UpdatePhysicsCategories();
            }
        }

        public CollisionResponse WorldDynamicResponse
        {
            get { return _worldDynamicResponse; }

            set
            {
                _worldDynamicResponse = value;
                UpdatePhysicsCategories();
            }
        }

        public CollisionResponse PawnResponse
        {
            get { return _pawnResponse; }

            set
            {
                _pawnResponse = value;
                UpdatePhysicsCategories();
            }
        }

        /// <summary>
        ///     Access the list of colliders overlapping this collider.
        /// </summary>
        public IReadOnlyList<Collider> OverlappingColliders => _overlappingColliders;

        public TransformComponent Transform => _transform;

        public MovementBehavior Movement => _movement;

        /// <summary>
        ///     Compute and return the bounding box of this collider projected in world coordinates.
        /// </summary>
        public abstract BoundingBox WorldBoundingBox { get; }

        protected internal Collider Master => _master;

        protected internal Body MasterPhysicsBody => _master._physicsBody;

        protected internal Fixture PhysicsFixture => _physicsFixture;

        /// <summary>
        ///     Invoked on all colliders in a level before the physics simulation. Should update the physics body
        ///     with relevant values from the actor.
        /// </summary>
        /// <param name="elapsed"> Time elapsed since the last update. </param>
        public virtual void UpdatePrePhysics(TimeSpan elapsed)
        {
            if (_physicsFixture == null)
                InitializePhysics();

            if (_master == this)
            {
                _physicsBody.Position = PhysicsSettings.FromTiles(_transform.Location);
                _physicsBody.Rotation = _transform.Rotation;
                if (_movement != null)
                {
                    Debug.Assert(!_movement.Active, "!_movement.Active");
                    _physicsBody.LinearVelocity = PhysicsSettings.FromTiles(_movement.Velocity);
                }
            }
        }

        /// <summary>
        ///     Invoked on all colliders in a level after the physics simulation. Should update the actor with
        ///     all relevant values from the physics body.
        /// </summary>
        /// <param name="elapsed"></param>
        public virtual void UpdatePostPhysics(TimeSpan elapsed)
        {
            if (_master != this)
                return;

            if (_movement != null)
            {
                _movement.MoveTo(PhysicsSettings.ToTiles(_physicsBody.Position), _physicsBody.Rotation);
            }
            else
            {
                _transform.Location = PhysicsSettings.ToTiles(_physicsBody.Position);
                _transform.Rotation = _physicsBody.Rotation;
            }
        }

        /// <summary>
        ///     Check if this collider is overlapping another collider.
        /// </summary>
        public bool IsOverlapping(Collider other)
        {
            return _overlappingColliders.Contains(other);
        }

        /// <summary>
        ///     Check if this collider intersects with another collider.
        /// </summary>
        public virtual bool Intersects(Collider other, Vector2 offset = default(Vector2), float expand = 0)
        {
            Validate.ArgumentNotNull(other, nameof(other));

            switch (other.Shape)
            {
                case ColliderShape.Box:
                    return Intersects(((BoxCollider) other).WorldBoundingBox, offset, expand);
                case ColliderShape.Sphere:
                    return Intersects(((SphereCollider) other).WorldBoundingSphere, offset, expand);
                default:
                    throw new NotSupportedException("collider type: " + other.Shape);
            }
        }

        /// <summary>
        ///     Check if this collider intersects with a bounding box.
        /// </summary>
        public abstract bool Intersects(BoundingBox other, Vector2 offset = default(Vector2), float expand = 0);

        /// <summary>
        ///     Check if this collider intersects with a bounding sphere.
        /// </summary>
        public abstract bool Intersects(BoundingSphere other, Vector2 offset = default(Vector2), float expand = 0);

        public override void WriteDelta(DeltaWriter writer, ReplicationContext context)
        {
            base.WriteDelta(writer, context);
            
            writer.Write(_layer);
            writer.Write((byte) _visibilityResponse);
            writer.Write((byte) _cameraResponse);
            writer.Write((byte) _worldStaticResponse);
            writer.Write((byte) _worldDynamicResponse);
            writer.Write((byte) _pawnResponse);
        }

        public override void ReadDelta(DeltaReader reader, ReplicationContext context)
        {
            base.ReadDelta(reader, context);
            
            reader.Read(ref _layer);
            reader.ReadEnumByte(ref _visibilityResponse);
            reader.ReadEnumByte(ref _cameraResponse);
            reader.ReadEnumByte(ref _worldStaticResponse);
            reader.ReadEnumByte(ref _worldDynamicResponse);
            reader.ReadEnumByte(ref _pawnResponse);
        }

        /// <summary>
        ///     Expand the width and height of a bounding box.
        /// </summary>
        /// <param name="box"> The bounding box. </param>
        /// <param name="expand"> The expansion factor. </param>
        public static void Expand(ref BoundingBox box, float expand)
        {
            box.Min.X -= expand / 2;
            box.Min.Y -= expand / 2;
            box.Max.X += expand / 2;
            box.Max.Y += expand / 2;
        }

        /// <summary>
        ///     Expand the radius of a bounding sphere.
        /// </summary>
        /// <param name="sphere"> The bounding sphere. </param>
        /// <param name="expand"> The expansion factor. </param>
        public static void Expand(ref BoundingSphere sphere, float expand)
        {
            sphere.Radius += expand;
        }

        internal void OnBeginContact(Fixture thisFixture, Fixture otherFixture, Contact contact, byte defaultLayer)
        {
            if (!Enabled)
            {
                contact.Enabled = false;
                return;
            }

            // Even if the contact has been disbled already, events are still invoked so both colliders
            // can be aware of them

            var layer = otherFixture.Tag as TileLayer;
            if (layer != null)
            {
                if (layer.Index != (_layer ?? defaultLayer))
                {
                    contact.Enabled = false;
                }
                else
                {
                    contact.Enabled &= !OnBlockedWorld();
                }
            }

            var otherCollider = otherFixture.Tag as Collider;
            if (otherCollider != null)
            {
                if ((_layer ?? defaultLayer) != (otherCollider._layer ?? defaultLayer))
                {
                    contact.Enabled = false;
                }
                else if (ShouldBlock(otherCollider))
                {
                    contact.Enabled &= !OnActorBlocked(otherCollider);
                }
                else if (!IsOverlapping(otherCollider))
                {
                    // While OnBeginContact() gets called twice, only the first call in an overlap situation will
                    // handle the overlap work. This is because we want to have each component in the others' overlap
                    // list before invoking the begin overlap event.

                    _overlappingColliders.Add(otherCollider);
                    otherCollider._overlappingColliders.Add(this);

                    contact.Enabled &= !OnActorOverlapBegan(otherCollider);
                    contact.Enabled &= !otherCollider.OnActorOverlapBegan(this);

                    if (!contact.Enabled)
                    {
                        _overlappingColliders.Remove(otherCollider);
                        otherCollider._overlappingColliders.Remove(this);
                    }
                }
            }
        }

        internal void OnEndContact(Fixture thisFixture, Fixture otherFixture, Contact contact)
        {
            Debug.Assert(thisFixture == _physicsFixture, "thisFixture == _physicsFixture");

            //TileLayer layer = otherFixture.UserData as TileLayer;
            //if (layer != null)
            //{
            //}

            var otherCollider = otherFixture.Tag as Collider;
            if (otherCollider != null)
            {
                if (IsOverlapping(otherCollider))
                {
                    _overlappingColliders.Remove(otherCollider);
                    OnActorOverlapEnded(otherCollider);
                }
            }
        }

        protected internal override void OnActorDisposing()
        {
            EndAllOverlaps();
            DisposePhysics();
            Level.RemoveCollider(_colliderListIndex);
            base.OnActorDisposing();
        }

        protected internal override void OnLevelChanging(Level newLevel)
        {
            EndAllOverlaps();
            DisposePhysics();
            Level.RemoveCollider(_colliderListIndex);
            base.OnLevelChanging(newLevel);
        }

        protected internal override void OnLevelChanged()
        {
            base.OnLevelChanged();
            _colliderListIndex = Level.AddCollider(this);
        }

        protected virtual void InitializePhysics()
        {
            if (_master._physicsBody == null)
            {
                _master._physicsBody = Level.PhysicsWorld.CreateBody(
                                                PhysicsSettings.FromTiles(_transform.Location),
                                                _transform.Rotation,
                                                _bodyType);
                _master._physicsBody.Tag = this;
            }

            if (_physicsFixture == null)
            {
                _physicsFixture = _master._physicsBody.CreateFixture(CreatePhysicsShape());
                _physicsFixture.Tag = this;
            }

            UpdatePhysicsCategories();
        }

        protected virtual void DisposePhysics()
        {
            _physicsFixture?.Body.Remove(_physicsFixture);
            _physicsFixture = null;
            _physicsBody?.World.Remove(_physicsBody);
            _physicsBody = null;
        }

        protected abstract Shape CreatePhysicsShape();

        /// <summary>
        ///     End all overlaps with other colliders.
        /// </summary>
        /// <remarks>
        ///     This is called before moving to another level or when the actor is destroyed so other colliders can
        ///     react.
        /// </remarks>
        protected void EndAllOverlaps()
        {
            if (_overlappingColliders.Count == 0)
                return;

            foreach (Collider other in _overlappingColliders.ToArray())
            {
                Debug.Assert(other._overlappingColliders.Contains(this), "other._overlappingColliders.Contains(this)");

                _overlappingColliders.Remove(other);
                other._overlappingColliders.Remove(this);

                OnActorOverlapEnded(other);
                other.OnActorOverlapEnded(this);
            }
        }

        /// <summary>
        ///     Called when this collider blocks another.
        /// </summary>
        protected virtual bool OnActorBlocked(Collider other)
        {
            if (ActorBlocked == null)
                return false;
            var e = new ActorCollisionEventArgs(other);
            ActorBlocked(this, e);
            return e.Cancel;
        }

        /// <summary>
        ///     Called when this collider begins overlapping another.
        /// </summary>
        protected virtual bool OnActorOverlapBegan(Collider other)
        {
            if (ActorOverlapBegan == null)
                return false;
            var e = new ActorCollisionEventArgs(other);
            ActorOverlapBegan(this, e);
            return e.Cancel;
        }

        /// <summary>
        ///     Called when this collider ends overlapping another.
        /// </summary>
        protected virtual bool OnActorOverlapEnded(Collider other)
        {
            if (ActorOverlapEnded == null)
                return false;
            var e = new ActorCollisionEventArgs(other);
            ActorOverlapEnded(this, e);
            return e.Cancel;
        }

        /// <summary>
        ///     Called when this collides with a tile or level boundary.
        /// </summary>
        protected virtual bool OnBlockedWorld()
        {
            if (WorldBlocked == null)
                return false;
            var e = new TileCollisionEventArgs();
            WorldBlocked(this, e);
            return e.Cancel;
        }

        private void UpdatePhysicsCategories()
        {
            if (_physicsFixture == null)
                return;

            Category categories =
                (_visibilityResponse != CollisionResponse.Ignore ? PhysicsSettings.VisibilityCategory : 0) |
                (_cameraResponse != CollisionResponse.Ignore ? PhysicsSettings.CameraCategory : 0) |
                (_worldStaticResponse != CollisionResponse.Ignore ? PhysicsSettings.WorldStaticCategory : 0) |
                (_worldDynamicResponse != CollisionResponse.Ignore ? PhysicsSettings.WorldDynamicCategory : 0) |
                (_pawnResponse != CollisionResponse.Ignore ? PhysicsSettings.PawnCategory : 0);

            bool hasBlock = _visibilityResponse == CollisionResponse.Block ||
                            _cameraResponse == CollisionResponse.Block ||
                            _worldStaticResponse == CollisionResponse.Block ||
                            _worldDynamicResponse == CollisionResponse.Block ||
                            _pawnResponse == CollisionResponse.Block;

            _physicsFixture.CollisionCategories = categories;
            _physicsFixture.CollidesWith = categories;
            _physicsFixture.IsSensor = !hasBlock;
        }

        // Checks if the current collider has a matching response with another that would lead to a block, otherwise
        // they'll overlap or ignore.
        internal bool ShouldBlock(Collider other)
        {
            const CollisionResponse block = CollisionResponse.Block;

            return _visibilityResponse == block && other._visibilityResponse == block ||
                   _cameraResponse == block && other._cameraResponse == block ||
                   _worldStaticResponse == block && other._worldStaticResponse == block ||
                   _worldDynamicResponse == block && other._worldDynamicResponse == block ||
                   _pawnResponse == block && other._pawnResponse == block;
        }

        //private static BoundingBox GetBoundingBox(Fixture fixture, int? childIndex = 0)
        //{
        //    AABB aabb;
        //    fixture.GetAABB(out aabb, childIndex ?? 0);
        //    return ToBoundingBox(aabb);
        //}

        //private static BoundingBox ToBoundingBox(AABB aabb)
        //{
        //    return new BoundingBox(new Vector3(aabb.LowerBound, 0), new Vector3(aabb.UpperBound, 0));
        //}
    }
}