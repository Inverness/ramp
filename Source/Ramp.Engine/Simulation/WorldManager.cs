using System.Diagnostics;
using Microsoft.Xna.Framework;

namespace Ramp.Simulation
{
    /// <summary>
    ///     Manages all loaded levels and their state.
    /// </summary>
    public sealed class WorldManager : GameComponent, IWorldManagerService
    {
        private bool _disposed;
        private World _world;

        public WorldManager(Game game)
            : base(game)
        {
            game.Services.AddService(typeof(IWorldManagerService), this);
        }

        public World World
        {
            get
            {
                Debug.Assert(_world != null);
                return _world;
            }
        }

        /// <inheritdoc />
        public override void Initialize()
        {
            Debug.Assert(!_disposed);

            base.Initialize();

            _world = new World(Game.Services, "TheWorld");
        }

        /// <inheritdoc />
        public override void Update(GameTime gameTime)
        {
            Debug.Assert(!_disposed);
            if (Enabled)
                _world.Update(gameTime.ElapsedGameTime);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && !_disposed)
            {
                if (_world != null)
                {
                    _world.DisposeLevels(true);
                    _world.Dispose();
                    _world = null;
                }
                _disposed = true;
            }
            base.Dispose(disposing);
        }
    }
}