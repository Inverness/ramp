using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Ramp.Data;
using Ramp.Naming;
using Ramp.Utilities.Collections;

namespace Ramp.Simulation
{
    /// <summary>
    ///     Represents a template for actors.
    /// </summary>
    public sealed class ActorArchetype : Decl
    {
        public ActorArchetype(string name, NamedObject outer)
            : base(name, outer)
        {
        }

        /// <summary>
        ///     Gets the base archetype from which data is inherited.
        /// </summary>
        public ActorArchetype Base { get; private set; }

        public IReadOnlyList<KeyValuePair<ActorComponentType, string>> Components { get; private set; }

        /// <summary>
        ///		Gets the spawn arguments used for actors of this archetype. This includes all spawn arguments
        ///     inherited from the base archetype. This dictionary is frozen.
        /// </summary>
        public IReadOnlyNameValueDictionary SpawnArguments { get; private set; }

        public override Task Update(JObject decl, bool validated, DeclManager manager)
        {
            // Parse reference to base archetype.

            string baseName = decl.Value<string>("Base");
            if (baseName != null)
            {
                Base = Directory.Find<ActorArchetype>(baseName, Outer);
                if (Base == null)
                    throw new InvalidDataException("Invalid archetype base: " + baseName);
            }

            // Parse component list

            var localComponents = new List<KeyValuePair<ActorComponentType, string>>();

            if (Base != null)
                localComponents.AddRange(Base.Components);

            var componentsContainer = decl["Components"] as JContainer;
            if (componentsContainer != null)
            {
                foreach (JToken item in componentsContainer)
                {
                    string typeName, instanceName;

                    var property = item as JProperty;
                    if (property != null)
                    {
                        typeName = property.Name;
                        instanceName = (string) property.Value;
                    }
                    else
                    {
                        typeName = (string) item;
                        instanceName = null;
                    }

                    ActorComponentType componentType = ActorComponentType.GetType(typeName);
                    if (componentType == null)
                        throw new InvalidDataException("invalid component type name: " + typeName);
                    localComponents.Add(new KeyValuePair<ActorComponentType, string>(componentType, instanceName));
                }
            }

            Components = localComponents;

            // Parse spawn arguments

            var localSpawnArguments = new NameValueDictionary();

            if (Base != null)
                localSpawnArguments.Update(Base.SpawnArguments);

            var spawnArgumentsToken = decl["Spawn"] as JObject;
            if (spawnArgumentsToken != null)
            {
                foreach (KeyValuePair<string, JToken> item in spawnArgumentsToken)
                {
                    var value = item.Value as JValue;
                    if (value == null)
                        throw new InvalidDataException("archetype spawn arguments must be JValue");
                    localSpawnArguments[item.Key] = value.Value;
                }
            }

            SpawnArguments = localSpawnArguments;

            return Task.CompletedTask;
        }
    }
}