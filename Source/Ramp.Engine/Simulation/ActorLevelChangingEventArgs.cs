
namespace Ramp.Simulation
{
    public class ActorLevelChangingEventArgs : ActorEventArgs
    {
        public ActorLevelChangingEventArgs(Actor actor, Level newLevel)
            : base(actor)
        {
            NewLevel = newLevel;
        }

        public Level NewLevel { get; private set; }
    }
}