using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using NLog;
using Microsoft.Xna.Framework;
using Ramp.Data;
using Ramp.Naming;
using Ramp.Network;
using Ramp.Simulation.Tiling;
using Ramp.Utilities.Collections;
using nkast.Aether.Physics2D.Dynamics;
using nkast.Aether.Physics2D.Collision;
using nkast.Aether.Physics2D.Common;
using nkast.Aether.Physics2D.Dynamics.Contacts;

namespace Ramp.Simulation
{
    /// <summary>
    ///     Base level class, which defines a game world containing actors.
    /// </summary>
    public sealed class Level : NamedObject
    {
        public const ushort MaxUpdateTargets = 0xFFFE;

        private static readonly Logger s_log = LogManager.GetCurrentClassLogger();

        private TimeSpan _time;
        private bool _isUpdating;
        private bool _isInitializing;

        private byte _defaultRendererLayer;
        private Color _backgroundColor = Color.Black;
        private string _backgroundTextureName;

        private readonly LinkedArrayList<Actor> _actors = new LinkedArrayList<Actor>();
        private readonly LinkedArrayList<Collider> _colliders = new LinkedArrayList<Collider>();
        private readonly LinkedArrayList<Renderer> _renderers = new LinkedArrayList<Renderer>();
        private readonly LinkedArrayList<Actor> _updateTargets = new LinkedArrayList<Actor>();

        private readonly TileMap _tileMap;
        private readonly Point _size;

        private readonly nkast.Aether.Physics2D.Dynamics.World _physicsWorld;
        private byte _defaultCollisionLayer;
        // Caches to avoid additional allocations
        private List<Fixture> _queryResults;
        private QueryReportFixtureDelegate _queryResultHandler;
        private Body _queryBody;
        private bool _queryIncludeStatic;

        /// <summary>
        ///     Initializes a new Level instance.
        /// </summary>
        /// <param name="name"> The unique level name. </param>
        /// <param name="world"> The world that this level exists in. </param>
        /// <param name="size"> The size of the level in tile groups. </param>
        /// <param name="withTileMap"> Whether to create a tile map. </param>
        public Level(string name, World world, Point size, bool withTileMap)
            : base(name, world)
        {
            Validate.ArgumentNotNull(name, "name");
            Validate.ArgumentNotNull(world, "world");
            Validate.ArgumentInRange(name.Length <= 32, "name must be no more than 32 characters");

            _isInitializing = true;
            _size = size;

            _physicsWorld = new nkast.Aether.Physics2D.Dynamics.World(Vector2.Zero);
            _physicsWorld.ContactManager.BeginContact = OnBeginContact;
            _physicsWorld.ContactManager.EndContact = OnEndContact;
            _physicsWorld.ContactManager.PreSolve = OnPreSolve;
            //_physicsWorld.ContactManager.PostSolve = OnPostSolve;

            if (withTileMap)
                _tileMap = new TileMap(size, _physicsWorld);
        }

        /// <summary>
        ///     Enumerates the actors in this level.
        /// </summary>
        public IEnumerable<Actor> Actors => _actors;

        /// <summary>
        ///     Gets the number of actors in this level.
        /// </summary>
        public int ActorCount => _actors.Count;

        /// <summary>
        ///     Background color rendered beneath the level.
        /// </summary>
        public Color BackgroundColor
        {
            get { return _backgroundColor; }

            set { _backgroundColor = value; }
        }

        /// <summary>
        ///     Image rendered underneath the level on screen.
        /// </summary>
        public string BackgroundTextureName
        {
            get { return _backgroundTextureName; }

            set { _backgroundTextureName = value; }
        }

        public IReadOnlyCollection<Collider> Colliders => _colliders;

        /// <summary>
        ///     Default layer to be used for collision testing.
        /// </summary>
        public byte DefaultCollisionLayer
        {
            get { return _defaultCollisionLayer; }

            set { _defaultCollisionLayer = value; }
        }

        public byte DefaultRendererLayer
        {
            get { return _defaultRendererLayer; }

            set { _defaultRendererLayer = value; }
        }

        /// <summary>
        ///     True if the level is currently updating actors.
        /// </summary>
        public bool IsUpdating => _isUpdating;

        public IReadOnlyCollection<Renderer> Renderers => _renderers;

        public Point Size => _size;

        /// <summary>
        ///     Gets the tile map. May be null.
        /// </summary>
        public TileMap TileMap => _tileMap;

        /// <summary>
        ///     The current time in the level. Incremented with each call to Update().
        /// </summary>
        public TimeSpan Time => _time;

        public World World => (World) Outer;

        public nkast.Aether.Physics2D.Dynamics.World PhysicsWorld => _physicsWorld;

        internal LinkedArrayList<Renderer> InternalRenderers => _renderers;

        /// <summary>
        ///     Enumerates actors with the specified tag.
        /// </summary>
        /// <param name="tag"> The tag to check for </param>
        /// <returns> Actors that have the specified tag. </returns>
        public IEnumerable<Actor> GetActorsWithTag(string tag)
        {
            for (int i = _actors.FirstIndex; i != -1;)
            {
                Actor a = _actors.GetItemAndNextIndex(i, out i);
                if (a.HasTag(tag))
                    yield return a;
            }
        }

        public IEnumerable<Actor> GetActorsInBounds(Vector2 location, float radius)
        {
            var sphere = new BoundingSphere(new Vector3(location, 0), radius);
            return GetActorsInBounds(sphere);
        }

        //public IEnumerable<Actor> GetActorsInBounds(RectangleF rectangle)
        //{
        //    var box = new BoundingBox(new Vector3(rectangle.TopLeft, 0), new Vector3(rectangle.BottomRight, 0));
        //    return GetActorsInBounds(box);
        //}

        /// <summary>
        ///     Creates a list of all actors with colliders that intersect with the specified bounding sphere.
        /// </summary>
        public IEnumerable<Actor> GetActorsInBounds(BoundingSphere sphere)
        {
            return GetActorsInBounds(c => c.Intersects(sphere));
        }

        public IEnumerable<Actor> GetActorsInBounds(IEnumerable<BoundingSphere> spheres)
        {
            if (spheres == null)
                throw new ArgumentNullException(nameof(spheres));

            return GetActorsInBounds(c => spheres.Any(sphere => c.Intersects(sphere)));
        }

        /// <summary>
        ///     Creates a list of all actors with colliders that intersect with the specified bounding box.
        /// </summary>
        public IEnumerable<Actor> GetActorsInBounds(BoundingBox box)
        {
            return GetActorsInBounds(c => c.Intersects(box));
        }

        public IEnumerable<Actor> GetActorsInBounds(IEnumerable<BoundingBox> boxes)
        {
            if (boxes == null)
                throw new ArgumentNullException(nameof(boxes));

            return GetActorsInBounds(c => boxes.Any(box => c.Intersects(box)));
        }

        /// <summary>
        ///     Update level state and all actors and their components within it. Actors may be spawned or destroyed
        ///     during the update. Also runs tasks currently queued in the dispatcher.
        /// </summary>
        /// <param name="elapsed"> The elapsed time. </param>
        /// <exception cref="InvalidOperationException">Thrown if this method is called recursively.</exception>
        public void Update(TimeSpan elapsed)
        {
            Validate.ValidOperation(!_isUpdating, "Cannot update a level recursively");
            Validate.ValidOperation(!_isInitializing, "Level is currently initializing");

            try
            {
                _isUpdating = true;

                _time += elapsed;
                
                for (int index = _updateTargets.FirstIndex; index != -1;)
                    _updateTargets.GetItemAndNextIndex(index, out index).Update(elapsed);

                for (int index = _colliders.FirstIndex; index != -1;)
                    _colliders.GetItemAndNextIndex(index, out index).UpdatePrePhysics(elapsed);

                _physicsWorld.Step((float) elapsed.TotalSeconds);

                for (int index = _colliders.FirstIndex; index != -1;)
                    _colliders.GetItemAndNextIndex(index, out index).UpdatePostPhysics(elapsed);
            }
            finally
            {
                _isUpdating = false;
            }
        }

        /// <summary>
        ///     Calls DestroyActor() for all actors in the level. Actors are not removed from the actors list until a
        ///     subsequent call to ClearDestroyedActors().
        /// </summary>
        public void ClearActors(bool immediate)
        {
            if (_actors.Count == 0)
                return;
            var actors = new List<Actor>(_actors);
            if (immediate)
                actors.ForEach(a => a.Dispose());
            else
                actors.ForEach(a => a.MarkForDispose());
            Debug.Assert(!immediate || _actors.Count == 0);
        }

        /// <summary>
        ///     Spawn a new actor in this level.
        /// </summary>
        /// <param name="location"> Location to spawn the actor at. </param>
        /// <param name="archetypeName"> Name of an archetype to apply to the actor, or null if no archetype is to be applied. </param>
        /// <param name="owner"> The owner of this actor, or null if no actor owns this one. </param>
        /// <param name="name"> The name of the new actor, must be unique within the level. </param>
        /// <param name="args"> Spawn arguments for the actor. </param>
        /// <param name="role"> The actor's role on the current system. If not Authority, RemoteRole is set to Authority. </param>
        /// <param name="components"> A collection of component type names that will be added to the actor. </param>
        /// <returns> The newly spawned actor. </returns>
        public Actor SpawnActor(Vector2? location = null, string archetypeName = null, Actor owner = null,
                                string name = null, NameValueDictionary args = null, NetRole? role = null,
                                IEnumerable<Type> components = null)
        {
            return World.SpawnActor(this, location, archetypeName, owner, name, role, args, _isInitializing, components);
        }

        public void EndInitialization()
        {
            _isInitializing = false;
        }

        /// <summary>
        ///     Test if an actor is blocked by overlap when placed at a destination.
        /// </summary>
        /// <param name="actor"> An actor to test. </param>
        /// <param name="destination"> The destination that the actor's physics body will be tested against. </param>
        /// <param name="includeStatic"> Whether to include static bodies. </param>
        /// <param name="aabbExtension"></param>
        /// <returns></returns>
        public bool TestOverlap(Actor actor, Vector2 destination, bool includeStatic, float aabbExtension = 0)
        {
            Validate.ArgumentNotNull(actor, "actor");

            if (destination.X < 0 ||
                destination.Y < 0 ||
                destination.X >= _size.X * TileMap.GroupSize ||
                destination.Y >= _size.Y * TileMap.GroupSize)
                return true;

            Body body = actor.GetComponent<Collider>()?.MasterPhysicsBody;
            if (body == null)
                return false;

            // Compute a bounding box containing all of the body's fixtures
            var aabb = new AABB();

            Debug.Assert(body.FixtureList.Count != 0);
            for (int i = 0; i < body.FixtureList.Count; i++)
            {
                Fixture fixture = body.FixtureList[i];

                for (int c = 0; c < fixture.Shape.ChildCount; c++)
                {
                    AABB fixtureAabb;
                    fixture.GetAABB(out fixtureAabb, c);

                    // Don't combine with the initial empty box
                    if (i == 0 && c == 0)
                        aabb = fixtureAabb;
                    else
                        aabb.Combine(ref fixtureAabb);
                }
            }

            // Here a destination AABB is created then combined with the original, then extended further to improve
            // the range of candidates for testing.
            // Without extending the AABB, pathfinding becomes an issue due to how range works along with the target
            // and destination actor's own physics bodies. The AI becomes unable to handle shapes between it and the
            // target properly.
            Vector2 delta = PhysicsSettings.FromTiles(destination) - body.Position;
            var destAabb = new AABB(aabb.LowerBound + delta, aabb.UpperBound + delta);
            aabb.Combine(ref destAabb);
            aabb.LowerBound -= new Vector2(aabbExtension + 1f);
            aabb.UpperBound += new Vector2(aabbExtension + 1f);

            // Query for anything in the new area. This is designed to avoid any delegate or lambda allocation.
            if (_queryResultHandler == null)
                _queryResultHandler = HandleQueryResult;
            _queryBody = body;
            _queryIncludeStatic = includeStatic;
            _physicsWorld.QueryAABB(_queryResultHandler, ref aabb);
            _queryBody = null;

            if (_queryResults == null || _queryResults.Count == 0)
                return false;

            bool hit = false;
            // So there are fixtures in the area, need to test each fixture against each of the actor's colliders
            var transform = new Transform(body.Position + delta, body.Rotation);
            var otherTransform = new Transform();

            // In the vast majority of cases each body will only have one fixture.
            for (int a = 0; a < body.FixtureList.Count; a++)
            {
                Fixture fixture = body.FixtureList[a];
                var collider = (Collider) fixture.Tag;

                for (int b = 0; b < _queryResults.Count; b++)
                {
                    Fixture otherFixture = _queryResults[b];
                    otherTransform.p = otherFixture.Body.Position;
                    otherTransform.q = Complex.FromAngle(otherFixture.Body.Rotation);

                    var otherCollider = otherFixture.Tag as Collider;
                    if (otherCollider != null)
                    {
                        Debug.Assert(otherFixture.Body != body, "otherFixture.Body != body");
                        Debug.Assert(actor != otherCollider.Actor, "actor != otherCollider.Actor");

                        if (collider.ShouldBlock(otherCollider))
                        {
                            for (int ac = 0; ac < fixture.Shape.ChildCount; ac++)
                            {
                                for (int bc = 0; bc < otherFixture.Shape.ChildCount; bc++)
                                {
                                    hit = Collision.TestOverlap(fixture.Shape,
                                                                ac,
                                                                otherFixture.Shape,
                                                                bc,
                                                                ref transform,
                                                                ref otherTransform);

                                    if (hit)
                                        goto OuterBreak;
                                }
                            }
                        }
                    }
                    else
                    {
                        Debug.Assert(otherFixture.Tag is TileLayer, "otherFixture.UserData is TileLayer");
                        Debug.Assert(includeStatic, "includeStatic");

                        // Only test blocks, not overlaps.
                        if (collider.WorldStaticResponse == CollisionResponse.Block)
                        {
                            for (int ac = 0; ac < fixture.Shape.ChildCount; ac++)
                            {
                                for (int bc = 0; bc < otherFixture.Shape.ChildCount; bc++)
                                {
                                    hit = Collision.TestOverlap(fixture.Shape,
                                                                ac,
                                                                otherFixture.Shape,
                                                                bc,
                                                                ref transform,
                                                                ref otherTransform);

                                    if (hit)
                                        goto OuterBreak;
                                }
                            }
                        }
                    }
                }
            }

            OuterBreak:

            _queryResults.Clear();

            return hit;
        }

        // Add an actor to the scene. Called during actor spawn and level changing.
        internal ushort AddActor(Actor actor)
        {
            Debug.Assert(actor.Level == this);
            return checked ((ushort) (_actors.AddLast(actor) + 1));
        }

        // Remove an actor from the scene. Called during actor destruction and level changing.
        internal void RemoveActor(ushort index)
        {
            _actors.RemoveAt(index - 1);
        }

        internal int AddCollider(Collider collider)
        {
            return _colliders.AddLast(collider);
        }

        internal void RemoveCollider(int index)
        {
            _colliders.RemoveAt(index);
        }

        internal int AddRenderer(Renderer renderer)
        {
            return _renderers.AddLast(renderer);
        }

        internal void RemoveRenderer(int index)
        {
            _renderers.RemoveAt(index);
        }

        internal ushort AddUpdateTarget(Actor actor)
        {
            if (_updateTargets.Count >= MaxUpdateTargets)
                throw new InvalidOperationException("Maximum number of update targets reached");
            return (ushort) (_updateTargets.AddLast(actor) + 1);
        }

        internal void RemoveUpdateTarget(ushort index)
        {
            _updateTargets.RemoveAt(index - 1);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //_physicsWorld.BodyList.ToList().ForEach(_physicsWorld.Remove);
                _physicsWorld.Clear();
                _physicsWorld.ProcessChanges();
            }
            base.Dispose(disposing);
        }
        
        // Creates a list of all actors with colliders that satisfy the specified predicate.
        private IEnumerable<Actor> GetActorsInBounds(Predicate<Collider> predicate)
        {
            HashSet<Actor> filter = null;
            for (int i = _colliders.FirstIndex; i != -1;)
            {
                Collider collider = _colliders.GetItemAndNextIndex(i, out i);

                if (!predicate(collider))
                    continue;

                // There can be multiple colliders from the same actor
                if (filter == null)
                    filter = new HashSet<Actor>();
                else if (filter.Contains(collider.Actor))
                    continue;
                
                yield return collider.Actor;

                filter.Add(collider.Actor);
            }
        }

        private bool HandleQueryResult(Fixture fixture)
        {
            if (fixture.IsSensor || fixture.Body == _queryBody ||
                (fixture.Body.BodyType == BodyType.Static && !_queryIncludeStatic))
            {
                return true;
            }

            if (_queryResults == null)
                _queryResults = new List<Fixture>();
            _queryResults.Add(fixture);
            return true;
        }

        private bool OnBeginContact(Contact contact)
        {
            Fixture fixtureA = contact.FixtureA;
            Fixture fixtureB = contact.FixtureB;

            var colliderA = fixtureA.Tag as Collider;
            var colliderB = fixtureB.Tag as Collider;

            byte defaultLayer = _defaultCollisionLayer;
            colliderA?.OnBeginContact(fixtureA, fixtureB, contact, defaultLayer);
            colliderB?.OnBeginContact(fixtureB, fixtureA, contact, defaultLayer);

            return contact.Enabled;
        }

        private void OnEndContact(Contact contact)
        {
            Fixture fixtureA = contact.FixtureA;
            Fixture fixtureB = contact.FixtureB;

            var colliderA = fixtureA.Tag as Collider;
            var colliderB = fixtureB.Tag as Collider;

            colliderA?.OnEndContact(fixtureA, fixtureB, contact);
            colliderB?.OnEndContact(fixtureB, fixtureA, contact);
        }

        private void OnPreSolve(Contact contact, ref Manifold oldManifold)
        {
            Fixture fixtureA = contact.FixtureA;
            Fixture fixtureB = contact.FixtureB;

            var colliderA = fixtureA.Tag as Collider;
            var colliderB = fixtureB.Tag as Collider;

            byte defaultLayer = _defaultCollisionLayer;

            if (colliderA != null)
            {
                if (!colliderA.Enabled)
                    contact.Enabled = false;
                else if (colliderB == null && ((TileLayer) fixtureB.Tag).Index != (colliderA.Layer ?? defaultLayer))
                    contact.Enabled = false;
            }

            if (contact.Enabled && colliderB != null)
            {
                if (!colliderB.Enabled)
                    contact.Enabled = false;
                else if (colliderA == null && ((TileLayer) fixtureA.Tag).Index != (colliderB.Layer ?? defaultLayer))
                    contact.Enabled = false;
            }
            
            if (contact.Enabled && colliderA != null && colliderB != null)
            {
                if ((colliderA.Layer ?? defaultLayer) != (colliderB.Layer ?? defaultLayer))
                    contact.Enabled = false;
                else if (!colliderA.ShouldBlock(colliderB))
                    contact.Enabled = false;
            }
        }
    }

    //public delegate void ActorMessageHandler<T>(object sender, ref T data);

    //public sealed class ActorMessageDispatcher
    //{
    //    private readonly Dictionary<Type, Delegate> _handlers = new Dictionary<Type, Delegate>();

    //    public void AddHandler<T>(MessageHandler<T> handler)
    //    {
    //        Validate.ArgumentNotNull(handler, nameof(handler));

    //        Type htype = typeof(MessageHandler<T>);

    //        Delegate hbase;
    //        if (_handlers.TryGetValue(htype, out hbase))
    //        {
    //            _handlers[htype] = Delegate.Combine(hbase, handler);
    //        }
    //        else
    //        {
    //            _handlers[htype] = handler;
    //        }
    //    }

    //    public void RemoveHandler<T>(MessageHandler<T> handler)
    //    {
    //        Validate.ArgumentNotNull(handler, nameof(handler));

    //        Type htype = typeof(MessageHandler<T>);

    //        Delegate hbase;
    //        if (!_handlers.TryGetValue(htype, out hbase))
    //            return;

    //        hbase = Delegate.Remove(hbase, handler);
    //        if (hbase != null)
    //            _handlers[htype] = hbase;
    //        else
    //            _handlers.Remove(htype);
    //    }

    //    public void Send<T>(object sender, ref T data)
    //    {
    //        Type type = typeof(MessageHandler<T>);

    //        Delegate hbase;
    //        if (!_handlers.TryGetValue(type, out hbase))
    //            return;

    //        ((MessageHandler<T>) hbase)(sender, ref data);
    //    }
    //}
}