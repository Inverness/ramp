﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using Ramp.Network;
using Ramp.Utilities.Reflection;

namespace Ramp.Simulation
{
    /// <summary>
    ///     Contains metadata about component types.
    /// </summary>
    public class ActorComponentType
    {
        private static readonly string[] s_suffixes = { "Component", "Behavior", "Renderer", "Collider" };

        private static readonly Type[] s_constructorTypes = new Type[0];

        private static ushort s_currentIndex;

        private static readonly Dictionary<Type, ActorComponentType> s_typesByClrType =
            new Dictionary<Type, ActorComponentType>();

        private static readonly Dictionary<string, ActorComponentType> s_typesByName =
            new Dictionary<string, ActorComponentType>();

        private static readonly Dictionary<ushort, ActorComponentType> s_typesByIndex =
            new Dictionary<ushort, ActorComponentType>();

        [ThreadStatic]
        private static ConstructorArguments t_constructorArguments;

        private readonly Dictionary<ushort, MethodInfo> _remoteCallablesByIndex = new Dictionary<ushort, MethodInfo>();

        private readonly Dictionary<string, Tuple<MethodInfo, ushort>> _remoteCallablesByName =
            new Dictionary<string, Tuple<MethodInfo, ushort>>();

        private ushort _remoteCallableIndexCounter;
        private Func<ActorComponent> _constructor; 

        static ActorComponentType()
        {
            InitializeTypes(AppDomain.CurrentDomain);
        }

        /// <summary>
        ///     Gets the underlying CLR type.
        /// </summary>
        public Type ClrType { get; private set; }

        /// <summary>
        ///     Gets the unique index.
        /// </summary>
        public ushort Index { get; private set; }

        /// <summary>
        ///     Gets the name of the type.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        ///     Gets the short name of the type.
        /// </summary>
        public string ShortName { get; private set; }

        /// <summary>
        ///     Gets whether this is a server-only type.
        /// </summary>
        public bool IsServerOnly { get; private set; }

        public static ICollection<ActorComponentType> Types => s_typesByClrType.Values;

        internal static ConstructorArguments CurrentConstructorArguments
        {
            get
            {
                ConstructorArguments args = t_constructorArguments;
                Debug.Assert(args != null, "must not manually construct components");
                return args;
            }
        }

        /// <summary>
        ///     Get the MethodInfo for a remote callable method.
        /// </summary>
        /// <param name="index"> The method index. </param>
        /// <returns></returns>
        public MethodInfo GetRemoteCallableMethod(ushort index)
        {
            MethodInfo mi;
            _remoteCallablesByIndex.TryGetValue(index, out mi);
            return mi;
        }

        /// <summary>
        ///     Get the MethodInfo for a remote callable method.
        /// </summary>
        /// <param name="name"> The method name. </param>
        /// <returns></returns>
        public MethodInfo GetRemoteCallableMethod(string name)
        {
            Tuple<MethodInfo, ushort> mi;
            return _remoteCallablesByName.TryGetValue(name, out mi) ? mi.Item1 : null;
        }

        /// <summary>
        ///     Get the index for a remote callable method.
        /// </summary>
        /// <param name="name"> The method name. </param>
        /// <returns></returns>
        public ushort? GetRemoteCallableIndex(string name)
        {
            Tuple<MethodInfo, ushort> mi;
            return _remoteCallablesByName.TryGetValue(name, out mi) ? mi.Item2 : (ushort?) null;
        }

        public static ActorComponentType GetType(Type type)
        {
            if (type == null)
                throw new ArgumentNullException(nameof(type));

            ActorComponentType result;
            s_typesByClrType.TryGetValue(type, out result);
            return result;
        }

        public static ActorComponentType GetType(ushort index)
        {
            if (index == 0)
                throw new ArgumentOutOfRangeException(nameof(index));

            ActorComponentType result;
            s_typesByIndex.TryGetValue(index, out result);
            return result;
        }

        public static ActorComponentType GetType(string name)
        {
            if (name == null)
                throw new ArgumentNullException(nameof(name));

            ActorComponentType result;
            s_typesByName.TryGetValue(name, out result);
            return result;
        }

        public static void InitializeTypes(AppDomain appDomain)
        {
            if (appDomain == null)
                throw new ArgumentNullException(nameof(appDomain));

            foreach (Assembly assembly in appDomain.GetAssemblies())
                InitializeTypes(assembly);
        }

        public static void InitializeTypes(Assembly assembly)
        {
            if (assembly == null)
                throw new ArgumentNullException(nameof(assembly));

            // Will throw an exception when accessing ExportedTypes
            if (assembly.IsDynamic)
                return;

            foreach (Type type in assembly.ExportedTypes)
            {
                if (type.IsSubclassOf(typeof(ActorComponent)) && !type.IsAbstract)
                    InitializeType(type);
            }
        }

        public static ActorComponentType InitializeType(Type type)
        {
            if (type == null)
                throw new ArgumentNullException(nameof(type));
            if (!type.IsSubclassOf(typeof(ActorComponent)))
                throw new ArgumentOutOfRangeException(nameof(type), "must be a subclass of ActorComponent");
            if (type.IsAbstract)
                throw new ArgumentException("Type is abstract", nameof(type));
            if (s_typesByClrType.ContainsKey(type))
                throw new InvalidOperationException("Specified type already initialized: " + type);
            Debug.Assert(s_currentIndex != ushort.MaxValue, "_currentIndex != UInt16.MaxValue");

            ushort index = ++s_currentIndex; // never o

            // Since component type names must be unique without a namespace element, we support the TypeNameAlias
            // attribute to specify that alias for use in serialization.
            string name, shortName;
            GetTypeNames(type, out name, out shortName);

            if (s_typesByName.ContainsKey(name))
                throw new InvalidOperationException("type name already registered: " + name);
            if (shortName != null && s_typesByName.ContainsKey(shortName))
                throw new InvalidOperationException("short type name already registered: " + shortName);

            bool isServerOnly = type.GetCustomAttribute<ServerComponentAttribute>(true) != null;
            
            ConstructorInfo constructorInfo = type.GetConstructor(s_constructorTypes);

            if (constructorInfo == null)
                throw new MissingMethodException("no default constructor defined for component type: " + type.FullName);
            
            Func<ActorComponent> constructor =
                ConstructorDelegateCompiler.Compile<Func<ActorComponent>>(constructorInfo.DeclaringType);

            var ctype = new ActorComponentType
            {
                ClrType = type,
                Index = index,
                Name = name,
                ShortName = shortName,
                IsServerOnly = isServerOnly,
                _constructor = constructor
            };

            s_typesByClrType[type] = ctype;
            s_typesByIndex[index] = ctype;
            s_typesByName[name] = ctype;
            if (shortName != null)
                s_typesByName[shortName] = ctype;

            ctype.InitializeRemoteCallables();

            return ctype;
        }

        internal ActorComponent CreateInstance(string name, Actor actor)
        {
            Debug.Assert(actor != null, "actor != null");

            ConstructorArguments args = t_constructorArguments;
            if (args == null)
                t_constructorArguments = args = new ConstructorArguments();

            args.Name = name;
            args.Actor = actor;
            
            try
            {
                return _constructor();
            }
            finally
            {
                args.Name = null;
                args.Actor = null;
            }
        }

        private static string GetShortTypeName(string name)
        {
            foreach (string s in s_suffixes)
            {
                if (name.EndsWith(s, StringComparison.InvariantCulture))
                    return name.Substring(0, name.Length - s.Length);
            }

            return null;
        }

        /// <summary>
        ///     Get the type name and short type name to be used for a component.
        /// </summary>
        /// <param name="type"> A subclass of component. </param>
        /// <param name="name"> The component type name. </param>
        /// <param name="shortName"> The short component type name. </param>
        private static void GetTypeNames(Type type, out string name, out string shortName)
        {
            var alias = type.GetCustomAttribute<TypeNameAliasAttribute>();
            if (alias != null)
            {
                name = alias.Name;
                shortName = alias.ShortName;
            }
            else
            {
                name = type.Name;
                shortName = GetShortTypeName(type.Name);
            }
        }

        private void InitializeRemoteCallables()
        {
            // NOTE: Does not include private methods from base classes.
            MethodInfo[] methods = ClrType.GetMethods(BindingFlags.Instance |
                                                      BindingFlags.Public |
                                                      BindingFlags.NonPublic);

            foreach (MethodInfo method in methods)
            {
                if (method.GetCustomAttribute<RemoteCallableAttribute>(true) == null)
                    continue;

                ushort index = _remoteCallableIndexCounter++;

                _remoteCallablesByIndex.Add(index, method);
                _remoteCallablesByName.Add(method.Name, Tuple.Create(method, index));
            }
        }

        internal class ConstructorArguments
        {
            public string Name;
            public Actor Actor;
        }

        //private static void InitializeBindingInfo(Type type)
        //{
        //    var boundFields = new List<BindAttribute>();
        //    var boundEventHandlers = new List<BindEventAttribute>();

        //    FieldInfo[] fields = type.GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
        //    foreach (FieldInfo field in fields)
        //    {
        //        var attr = field.GetCustomAttribute<BindAttribute>(true);
        //        if (attr == null)
        //            continue;
        //        attr.FieldInfo = field;
        //        boundFields.Add(attr);
        //    }

        //    MethodInfo[] methods = type.GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
        //    foreach (MethodInfo method in methods)
        //    {
        //        var attr = method.GetCustomAttribute<BindEventAttribute>(true);
        //        if (attr == null)
        //            continue;
        //        attr.MethodInfo = method;
        //        attr.TargetEventInfo = attr.ComponentType.GetEvent(attr.EventName,
        //                                                           BindingFlags.Instance | BindingFlags.Public);
        //        if (attr.TargetEventInfo == null)
        //            throw new BindingException("Type does not have an event of the specified name.");
        //        boundEventHandlers.Add(attr);
        //    }

        //    _bindingInfo[type] = new ComponentTypeBindingInfo(boundFields.ToArray(), boundEventHandlers.ToArray());
        //}
    }
}