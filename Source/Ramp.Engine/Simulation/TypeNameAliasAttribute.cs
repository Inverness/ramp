using System;

namespace Ramp.Simulation
{
    /// <summary>
    ///		Specifies an alias for a component type name.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, Inherited = false)]
    public class TypeNameAliasAttribute : Attribute
    {
        public TypeNameAliasAttribute(string name, string shortName = null)
        {
            Name = name;
            ShortName = shortName;
        }

        public string Name { get; private set; }

        public string ShortName { get; private set; }
    }
}