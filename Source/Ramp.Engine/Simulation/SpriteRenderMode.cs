﻿namespace Ramp.Simulation
{
    /// <summary>
    ///     Describes how to render a sprite.
    /// </summary>
    public enum SpriteRenderMode
    {
        /// <summary>
        ///     Renders a sprite using the default blending.
        /// </summary>
        Default,

        /// <summary>
        ///     Renders a sprite as a light.
        /// </summary>
        Light
    }
}