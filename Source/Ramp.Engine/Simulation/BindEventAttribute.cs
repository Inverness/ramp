﻿using System;
using System.Reflection;

namespace Ramp.Simulation
{
    /// <summary>
    ///		Specifies a component's event handler that will be bound to another component's event.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class BindEventAttribute : Attribute
    {
        public BindEventAttribute(Type type, string name, bool optional = false)
        {
            ComponentType = type;
            EventName = name;
            Optional = optional;
        }

        public Type ComponentType { get; private set; }

        public string EventName { get; private set; }

        public bool Optional { get; private set; }

        internal MethodInfo MethodInfo { get; set; }

        internal EventInfo TargetEventInfo { get; set; }
    }
}