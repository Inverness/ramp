using System;

namespace Ramp.Simulation
{
    /// <summary>
    ///     A behavior is a component that can be enabled and disabled.
    /// </summary>
    public interface IBehavior
    {
        /// <summary>
        ///     Raised when the value of Enabled changes.
        /// </summary>
        event TypedEventHandler<IBehavior, EventArgs> EnabledChanged;

        bool Enabled { get; set; }
    }
}