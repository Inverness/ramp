using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using NLog;
using Microsoft.Xna.Framework;
using Ramp.Network;

namespace Ramp.Simulation
{
    /// <summary>
    ///     Provides actor movement based on velocity, acceleration, and direction.
    /// </summary>
    public class MovementBehavior : Behavior
    {
        private const float MaxClientError = 0.2f; // maximum allowed difference in x or y coordinate

        private static readonly Logger s_log = LogManager.GetCurrentClassLogger();

        private readonly TransformComponent _transform;
        private bool _active;
        private Direction _direction;
        private Vector2 _velocity;
        private float _maxVelocity;
        private Vector2 _acceleration;
        private float _maxAcceleration;
        private Vector2 _pendingForce;
        private Vector2 _pendingImpulse;

        private TimeSpan _lastGoodLocationTime; // last time a good location was received by the server
        private TimeSpan _lastMoveTime; // the last time a move was done
        private TimeSpan _lastSuccessfulMoveTime; // the last time a move succeeded without collision

        private bool _replayMoves; // true to replay moves since _lastGoodLocationTime during next update
        private LinkedList<SavedMove> _savedMoves; // saved autonomous client moves

        public MovementBehavior()
        {
            Actor.IsActive = true;
            GetComponentRequired(out _transform);
            _maxAcceleration = Actor.GetSpawnArgument("MaxAcceleration", 32f);
            _maxVelocity = Actor.GetSpawnArgument("MaxVelocity", 32f);
        }

        /// <summary>
        ///     Raised when Direction is changed.
        /// </summary>
        public event TypedEventHandler<MovementBehavior, EventArgs> DirectionChanged;

        /// <summary>
        ///     Gets or sets whether movement is active or passive. If true, the actor will be moved by its velocity
        ///     during each update. Otherwise, the velocity can be observed by a collider component to apply
        ///     to a physics simulation.
        /// </summary>
        public bool Active
        {
            get { return _active; }

            set { _active = value; }
        }

        /// <summary>
        ///     The direction of movement.
        /// </summary>
        public Direction Direction
        {
            get { return _direction; }

            private set
            {
                if (_direction == value)
                    return;
                _direction = value;
                OnDirectionChanged();
            }
        }

        /// <summary>
        ///     The last time a movement was made.
        /// </summary>
        public TimeSpan LastMoveTime => _lastMoveTime;

        /// <summary>
        ///     The last time a movement was made without collision.
        /// </summary>
        public TimeSpan LastSuccessfulMoveTime => _lastSuccessfulMoveTime;

        /// <summary>
        ///     Velocity of movement in tiles per second.
        /// </summary>
        public Vector2 Velocity
        {
            get { return _velocity; }

            set
            {
                _velocity = MovementUtility.ClampLength(value, _maxVelocity);
                UpdateDirectionFromVelocity();
            }
        }

        /// <summary>
        ///     Gets or sets the maximum length of the velocity vector.
        /// </summary>
        public float MaxVelocity
        {
            get { return _maxVelocity; }

            set { _maxVelocity = value; }
        }

        /// <summary>
        ///     Gets or sets the acceleration vector.
        /// </summary>
        public Vector2 Acceleration
        {
            get { return _acceleration; }

            set { _acceleration = MovementUtility.ClampLength(value, _maxAcceleration); }
        }

        /// <summary>
        ///     Gets or sets the maximum length of the acceleration vector.
        /// </summary>
        public float MaxAcceleration
        {
            get { return _maxAcceleration; }

            set { _maxAcceleration = value; }
        }

        /// <summary>
        ///     Gets the transform this component is bound to.
        /// </summary>
        public TransformComponent Transform => _transform;

        /// <summary>
        ///     Add to <c>Velocity</c>.
        /// </summary>
        /// <param name="velocity"> The value to be added to the current velocity </param>
        public void AddVelocity(Vector2 velocity)
        {
            Velocity += velocity;
        }

        /// <summary>
        ///     Adds an impulse that will be applied to the velocity during the next update. Each call is accumulated
        ///     until the update.
        /// </summary>
        /// <param name="vector"> The impulse vector. </param>
        public void AddImpulse(Vector2 vector)
        {
            _pendingImpulse += vector;
        }

        /// <summary>
        ///     Adds a force that will be applied to the velocity during the next update. Each call is accumulated
        ///     until the update. Force is scaled by elapsed time.
        /// </summary>
        /// <param name="vector"> The force vector. </param>
        public void AddForce(Vector2 vector)
        {
            _pendingForce += vector;
        }

        public override void WriteDelta(DeltaWriter writer, ReplicationContext context)
        {
            base.WriteDelta(writer, context);

            // Players uses rpc to move
            writer.Write(_active);
            if (Actor.RemoteRole != NetRole.Autonomous)
            {
                writer.Write(_velocity);
                writer.Write(_maxVelocity);
                writer.Write(_acceleration);
                writer.Write(_maxAcceleration);
                writer.Write(_pendingForce);
                writer.Write(_pendingImpulse);
            }
        }

        public override void ReadDelta(DeltaReader reader, ReplicationContext context)
        {
            base.ReadDelta(reader, context);

            reader.Read(ref _active);
            if (Actor.Role != NetRole.Autonomous)
            {
                bool velocityChanged = reader.Read(ref _velocity);
                reader.Read(ref _maxVelocity);
                reader.Read(ref _acceleration);
                reader.Read(ref _maxAcceleration);
                reader.Read(ref _pendingForce);
                reader.Read(ref _pendingImpulse);

                if (velocityChanged)
                    UpdateDirectionFromVelocity();
            }
        }

        public override void Update(TimeSpan elapsed)
        {
            base.Update(elapsed);

            Debug.Assert(!Actor.IsLevelChanging);

            if (!Enabled)
                return;

            bool autonomous = Actor.Role == NetRole.Autonomous;

            // replay moves after server corrected our location
            if (autonomous && _replayMoves)
                ReplayMoves();

            UpdateVelocity(elapsed);
            UpdateDirectionFromVelocity();

            if (_active && _velocity != Vector2.Zero)
                Move(_velocity * (float) elapsed.TotalSeconds, 0);
        }

        /// <summary>
        ///     Moves the actor to the specified location. Does not perform collision detection. Replicates autonomous
        ///     movement to the server.
        /// </summary>
        /// <param name="location"> The new location. </param>
        /// <param name="rotation"> The new rotation. </param>
        public void MoveTo(Vector2 location, float rotation)
        {
            TransformComponent transform = _transform;
            Move(location - transform.Location, rotation - transform.Rotation);
        }

        /// <summary>
        ///     Moves the actor by deltas. Does not perform collision detection. Replicates autonomous movement to
        ///     the server.
        /// </summary>
        /// <param name="deltaLocation"> The location delta. </param>
        /// <param name="deltaRotation"> The rotation delta. </param>
        public void Move(Vector2 deltaLocation, float deltaRotation)
        {
            // ReSharper disable once CompareOfFloatsByEqualityOperator
            if (deltaLocation == Vector2.Zero && deltaRotation == 0)
                return;

            TimeSpan time = Level.Time;

            _lastMoveTime = time;
            _lastSuccessfulMoveTime = time;

            _transform.Location += deltaLocation;
            _transform.Rotation += deltaRotation;

            if (Actor.Role == NetRole.Autonomous)
            {
                Vector2 newLocation = _transform.Location;

                if (_savedMoves == null)
                    _savedMoves = new LinkedList<SavedMove>();
                _savedMoves.AddLast(new SavedMove(deltaLocation, deltaRotation, time));

                ServerMove(deltaLocation, deltaRotation, (byte) _direction, newLocation, time);
            }
        }

        protected internal override void OnLevelChanging(Level newLevel)
        {
            _savedMoves?.Clear();
            base.OnLevelChanging(newLevel);
        }

        [RemoteCallable]
        protected void ServerSetDirection(byte direction)
        {
            if (Actor.ClientSide)
                InvokeServer("ServerSetDirection", false, direction);
            Direction = (Direction) direction;
        }

        /// <summary>
        ///     Updates the velocity with acceleration and pending impulses and forces.
        /// </summary>
        /// <param name="elapsed"> Elapsed time since last update. </param>
        protected virtual void UpdateVelocity(TimeSpan elapsed)
        {
            float deltaSeconds = (float) elapsed.TotalSeconds;

            // Apply acceleration and forces
            _acceleration = MovementUtility.ClampLength(_acceleration, _maxAcceleration);

            _velocity += (_acceleration * deltaSeconds) + (_pendingForce * deltaSeconds) + _pendingImpulse;

            _velocity = MovementUtility.ClampLength(_velocity, _maxVelocity);

            _pendingForce = _pendingImpulse = Vector2.Zero;
        }

        protected virtual void OnDirectionChanged()
        {
            DirectionChanged?.Invoke(this, EventArgs.Empty);
        }

        // Updates the current direction based on velocity
        private void UpdateDirectionFromVelocity()
        {
            if (Actor.Role >= NetRole.Autonomous && Actor.RemoteRole != NetRole.Autonomous)
            {
                ServerSetDirection((byte) MovementUtility.GetDirection(_velocity));
            }
        }

        /// <summary>
        ///     Called client to server to match its own autonomous movement and verify the client's result.
        /// </summary>
        /// <param name="deltaLocation"> The movement delta. </param>
        /// <param name="deltaRotation"> The delta rotation. </param>
        /// <param name="direction"> The movement direction as a byte. </param>
        /// <param name="clientLocation"> The client's location. </param>
        /// <param name="clientTime"> The client's time stamp. </param>
        [RemoteCallable]
        protected void ServerMove(Vector2 deltaLocation, float deltaRotation, byte direction, Vector2 clientLocation, TimeSpan clientTime)
        {
            // Using event messages instead of method invocation takes 40% less time, determine if this is
            // actually a useful optimization under heavy load
            // Event messages:  0.0037 to 0.0039 milliseconds average
            // Rpc:             0.00625 to 0.0064 milliseconds average
            if (Actor.ClientSide)
            {
                InvokeServer("ServerMove", false, deltaLocation, deltaRotation, direction, clientLocation, clientTime);
                return;
            }

            Debug.Assert(Actor.RemoteRole == NetRole.Autonomous);

            // Perform movement
            Move(deltaLocation, deltaRotation);
            //Direction = (Direction) direction;

            // Compare result location with client's location and possibly send adjustment.
            Vector2 newLocation = Transform.Location;
            if (Math.Abs(newLocation.X - clientLocation.X) > MaxClientError ||
                Math.Abs(newLocation.Y - clientLocation.Y) > MaxClientError)
            {
                s_log.Debug("Adjust client location: Old={0}, New={1}", clientLocation, newLocation);
                ClientAdjustLocation(newLocation, clientTime);
            }
            else
            {
                ClientAcknowledgeLocation(clientTime);
            }
        }

        /// <summary>
        ///     Called server to client to correct the client's location when it doesn't match the server's
        ///     Replays all moves that the client made since the movement that was corrected
        /// </summary>
        [RemoteCallable]
        protected void ClientAdjustLocation(Vector2 correctedLocation, TimeSpan originalTime)
        {
            if (Actor.ServerSide)
            {
                InvokeClient("ClientAdjustLocation", false, correctedLocation, originalTime);
                return;
            }

            Debug.Assert(Actor.Role == NetRole.Autonomous);

            s_log.Debug("Correcting location: Current={0}, Corrected={1}", Transform.Location, correctedLocation);
            Transform.Location = correctedLocation;
            ClearSavedMoves(originalTime);
            _lastGoodLocationTime = originalTime;
            _replayMoves = true;
        }

        /// <summary>
        ///     Called server to client to acknowledge that the client's location is correct
        /// </summary>
        [RemoteCallable]
        protected void ClientAcknowledgeLocation(TimeSpan originalTime)
        {
            if (Actor.ServerSide)
            {
                InvokeClient("ClientAcknowledgeLocation", false, originalTime);
                return;
            }

            Debug.Assert(Actor.Role == NetRole.Autonomous);
            ClearSavedMoves(originalTime);
            _lastGoodLocationTime = originalTime;
            _replayMoves = false;
        }

        /// <summary>
        ///     Replay all saved moves that happened on or after the last good location verified by the server
        /// </summary>
        private void ReplayMoves()
        {
            Debug.Assert(_replayMoves);

            s_log.Debug("Replaying saved moves since {0}:", _lastGoodLocationTime);

            LinkedListNode<SavedMove> move = _savedMoves.First;
            while (move != null)
            {
                Move(move.Value.DeltaLocation, move.Value.DeltaRotation);
                s_log.Debug("  Move: Delta={0}, Result={1}", move.Value.DeltaLocation, Transform.Location);
                move = move.Next;
            }

            _replayMoves = false;
        }

        /// <summary>
        ///     Clear all saved moves that happened before the specified time
        /// </summary>
        private void ClearSavedMoves(TimeSpan latest)
        {
            LinkedListNode<SavedMove> node;
            while ((node = _savedMoves.First) != null)
            {
                if (node.Value.Time <= latest)
                    _savedMoves.Remove(node);
                else
                    break;
            }
        }

        private struct SavedMove
        {
            public readonly Vector2 DeltaLocation;

            public readonly float DeltaRotation;

            public readonly TimeSpan Time;

            public SavedMove(Vector2 deltaLocation, float deltaRotation, TimeSpan time)
            {
                DeltaLocation = deltaLocation;
                DeltaRotation = deltaRotation;
                Time = time;
            }
        }
    }
}