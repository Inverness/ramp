﻿using System;

namespace Ramp.Simulation
{
    /// <summary>
    ///     Allows an actor to be controlled by a player.
    /// </summary>
    public abstract class PawnBehavior : Behavior
    {
        protected PawnBehavior()
        {
        }
    }

    public class PlayerPawnBehavior : PawnBehavior
    {
        public const string PlayerPawnTag = "PlayerPawn";

        private IPlayer _player;

        public PlayerPawnBehavior()
        {
            Actor.AddTag(PlayerPawnTag);
        }

        /// <summary>
        ///     Raised after the Player property has changed.
        /// </summary>
        public event TypedEventHandler<PawnBehavior, EventArgs> PlayerChanged;

        /// <summary>
        ///     The player that controls this actor, if any.
        /// </summary>
        public IPlayer Player
        {
            get { return _player; }

            internal set
            {
                if (_player == value)
                    return;
                _player = value;
                OnPlayerChanged();
            }
        }

        /// <summary>
        ///     Get the topmost player that owns an actor.
        /// </summary>
        /// <param name="actor"> An actor. </param>
        /// <returns> A Player instance if one could be found, or null. </returns>
        public static IPlayer GetTopPlayer(Actor actor)
        {
            Validate.ArgumentNotNull(actor, "actor");

            while (actor.Owner != null)
                actor = actor.Owner;

            return actor.GetComponent<PlayerPawnBehavior>()?.Player;
        }

        protected virtual void OnPlayerChanged()
        {
            PlayerChanged?.Invoke(this, EventArgs.Empty);
        }
    }
}