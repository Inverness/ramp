namespace Ramp.Simulation
{
    /// <summary>
    ///     Represents the current status of a level.
    /// </summary>
    public enum LevelStatus
    {
        /// <summary>
        ///     A file with the matching name was not found in the manifest.
        /// </summary>
        NotFound,

        /// <summary>
        ///     A file with the matching name was found in the manifest, but is not loaded.
        /// </summary>
        Exists,

        /// <summary>
        ///     A level with the matching name is currently being loaded.
        /// </summary>
        Loading,

        /// <summary>
        ///     A level with the matching name is loaded.
        /// </summary>
        Loaded
    }
}