using System.ComponentModel;

namespace Ramp.Simulation
{
    /// <summary>
    ///     Contains information about a collision between actors.
    /// </summary>
    public class ActorCollisionEventArgs : CancelEventArgs
    {
        public ActorCollisionEventArgs(Collider other)
        {
            Other = other;
        }

        /// <summary>
        ///     The other involved collider.
        /// </summary>
        public Collider Other { get; private set; }
    }
}