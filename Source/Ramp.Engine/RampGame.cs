using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;
using NLog;
using Microsoft.Xna.Framework;
using NLog.Config;
using Ramp.Data;
using Ramp.Input;
using Ramp.Naming;
using Ramp.Network;
using Ramp.Rendering;
using Ramp.Session;
using Ramp.Simulation;
using Ramp.Threading;

namespace Ramp
{
    /// <summary>
    ///     Central game manager. Creates and maintains all game components.
    /// </summary>
    public class RampGame : Game, IDispatcherService
    {
        public const uint Version = 1;
        public const uint MinVersion = 1;
        public const ulong MagicNumber = 0xDEADBEEFABCDEF10U;
        public const string BaseConfigFilename = "DemoConfig.yaml";
        public const string UserConfigFilename = "DemoUserConfig.yaml";

        private static readonly Logger s_log = LogManager.GetCurrentClassLogger();

        private static RampGame s_current;

        //private const int InputListSize = 20;
        protected readonly GraphicsDeviceManager Graphics;
        protected bool Dedicated;
        protected ConfigManager Config;
        protected CommandSystem CommandSystem;
        protected CommandVariableSystem CommandVariableSystem;
        protected FileManager FileManager;
        protected ConfigPatchManager ConfigPatchManager;
        protected DeclManager DeclManager;
        protected NetManager Net;
        protected ReplicaManager Replica;
        protected FileDownloadManager Download;
        protected InputSystem InputSystem;
        protected WorldManager WorldManager;
        protected LevelRenderer LevelRenderer;
        protected UIManager UI;
        protected SessionManager Session;
        protected MainMenuManager MainMenuManager;

        private Dispatcher _gameDispatcher;
        private DispatcherThread _workDispatcherThread;
        private AssetLoader _assetLoader;
        private readonly Stopwatch _frameStopwtch = new Stopwatch();
        private readonly Stopwatch _updateStopwatch = new Stopwatch();

        public RampGame()
        {
#if DEBUG
            LogManager.Configuration = new XmlLoggingConfiguration("NLog.Debug.config");
#else      
            LogManager.Configuration = new XmlLoggingConfiguration("NLog.Release.config");
#endif
            LogManager.ReconfigExistingLoggers();

            Debug.Assert(s_current == null);
            s_current = this;

            Graphics = new GraphicsDeviceManager(this);
            Content = null;
        }

        public new RampContentManager Content { get; private set; }

        public Dispatcher GameDispatcher => _gameDispatcher;

        public Dispatcher WorkDispatcher => _workDispatcherThread.Dispatcher;

        /// <summary>
        ///     Initializes the engine.
        /// </summary>
        protected override void Initialize()
        {
            // Initialize logging
            Thread.CurrentThread.Name = "Game";

            s_log.Info("Initializing game...");

            // Initialize type converters for XNA objects.
            XnaConverter.AddTypeConverters();

            // Initialize the synchronization context and dispatchers for our game and work threads
            Debug.Assert(SynchronizationContext.Current is WindowsFormsSynchronizationContext,
                         "SynchronizationContext.Current is WindowsFormsSynchronizationContext");
            SynchronizationContext.SetSynchronizationContext(null);
            _gameDispatcher = Dispatcher.Current;

            _workDispatcherThread = new DispatcherThread("Work");
            Services.AddService(typeof(IDispatcherService), this);

            // Ready to load the configuration now
            s_log.Debug("Loading configuration...");

            Directory.SetCurrentDirectory(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location));
            string root = Directory.GetCurrentDirectory();
            Config = new ConfigManager(this, BaseConfigFilename, root, Path.Combine(root, "User"),
                                       UserConfigFilename);
            Config.Initialize();

            // Set the screens size
            Graphics.PreferredBackBufferWidth = (int?) Config.Database.GetNumber("User.Game.ScreenWidth") ?? 1024;
            Graphics.PreferredBackBufferHeight = (int?) Config.Database.GetNumber("User.Game.ScreenHeight") ?? 768;
            Graphics.ApplyChanges();

            // Configure the window and some basic settings
            double targetFps = Config.Database.GetNumber("System.Game.TargetFps") ?? 60.0;
            IsMouseVisible = true;
            IsFixedTimeStep = true;
            TargetElapsedTime = TimeSpan.FromSeconds(1.0 / targetFps);
            Window.AllowUserResizing = true;

            s_log.Debug("Creating game systems...");
            CreateGameSystems();

            // Initialize game systems
            s_log.Debug("Initializing game systems...");
            base.Initialize();
            Config.FinishInitialization();
            s_log.Debug("Initialized game systems:");
            foreach (IGameComponent system in Components)
                s_log.Debug("  " + system.GetType().FullName);

            //UI.Console.Command += OnConsoleCommand;

            CommandSystem.AddCommand("Exit", (source, args) => Exit());

            s_log.Info("Initialized game");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _gameDispatcher.Run(DispatcherRunMode.All); // execute any remaining callbacks
                _workDispatcherThread.Dispose();
                Content.Dispose();
                NamedDirectory.Current.DisposeObjects();
            }

            base.Dispose(disposing);

            Debug.Assert(s_current == this);
            s_current = null;
        }

        protected override async void BeginRun()
        {
            s_log.Debug("Beginning run");

            // hackish command line listening for testing network stuff
            string[] args = Environment.GetCommandLineArgs();
            if (args.Length > 1)
            {
                if (args[1] == "netserver")
                {
                    Window.Title = "Ramp Demo (Server)";
                    await Session.BeginListenServerGameAsync(NetManager.DefaultPort);
                }
                else if (args[1] == "netdedserver")
                {
                    Window.Title = "Ramp Demo (Dedicated Server)";
                    await Session.BeginDedicatedServerGameAsync(NetManager.DefaultPort);
                }
                else if (args[1] == "netclient")
                {
                    Window.Title = "Ramp Demo (Client)";
                    Thread.Sleep(5000); // give time to start external server
                    await Session.BeginClientGameAsync("127.0.0.1", NetManager.DefaultPort);
                }
                else if (args[1] == "testserver")
                {
                    Window.Title = "Ramp Demo (Server)";
                    await Session.BeginListenServerGameAsync(NetManager.DefaultPort);
                    Process.Start(Assembly.GetEntryAssembly().Location, "netclient");
                }
                else if (args[1] == "testdedserver")
                {
                    Window.Title = "Ramp Demo (Dedicated Server)";
                    await Session.BeginDedicatedServerGameAsync(NetManager.DefaultPort);
                    Process.Start(Assembly.GetEntryAssembly().Location, "netclient");
                }
                else if (args[1] == "testclient")
                {
                    Window.Title = "Ramp Demo (Client)";
                    Process.Start(Assembly.GetEntryAssembly().Location, "netserver");
                    Thread.Sleep(5000); // give time to start external server
                    await Session.BeginClientGameAsync("127.0.0.1", NetManager.DefaultPort);
                }
                else if (!args[1].StartsWith("+"))
                {
                    Console.WriteLine("unknown command line argument: " + args[1]);
                    Exit();
                }
            }
            else
            {
                Window.Title = "Ramp Demo";
#if DEBUG
                await Session.BeginSingleplayerGameAsync();
#endif
                //Session.LoadGame("Save1.rsav");
                //NewGame();
            }
        }

        protected override async void EndRun()
        {
            s_log.Info("Ending run...");

            // Shutdown the game master and network connection if any. Will also unload levels
            await Session.EndGameAsync();

            //// Creating a manifest will get expensive eventually, so
            //// we want to save it to disk and just load it next time
            //FileManager.SaveManifest();

            // Save changes to user config
            Config.Save();

            base.EndRun();
        }

        protected override void Update(GameTime gameTime)
        {
            _frameStopwtch.Start();
            _updateStopwatch.Start();

            if (_gameDispatcher.Count != 0)
                _gameDispatcher.Run(DispatcherRunMode.All);

            base.Update(gameTime);

            if (_gameDispatcher.Count != 0)
                _gameDispatcher.Run(DispatcherRunMode.All);

            _updateStopwatch.Stop();

            UI.LastUpdateTime = _updateStopwatch.Elapsed;

            _updateStopwatch.Reset();

            // Full GC's can be requested in single player after level transitions
            if (Session.RequestingFullGC)
            {
                GC.Collect(GC.MaxGeneration, GCCollectionMode.Forced, true);
                Session.RequestingFullGC = false;
            }
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            // Temporary hack
            Actor actor = Session.LocalPlayerActor;
            Vector2? location;
            if (actor != null && (location = actor.GetLocation()) != null)
            {
                LevelRenderer.Level = actor.Level;
                LevelRenderer.Camera = location.Value;
            }
            else
            {
                LevelRenderer.Level = null;
            }

            base.Draw(gameTime);

            _frameStopwtch.Stop();

            UI.LastFrameTime = _frameStopwtch.Elapsed;

            _frameStopwtch.Reset();
        }

        // Creates game components and adds them to the components list.
        protected virtual void CreateGameSystems()
        {
            CommandSystem = new CommandSystem(this);
            Components.Add(CommandSystem);

            CommandVariableSystem = new CommandVariableSystem(this);
            Components.Add(CommandVariableSystem);

            // Create the file manager, which provides a manifest of filenames and most
            // importantly, maps file names to their full paths so we only need to use file
            // names at design time. This also simplifies things like packing files and download
            // them from servers since their full path is irelevant.
            FileManager = new FileManager(this);
            Components.Add(FileManager);

            ConfigPatchManager = new ConfigPatchManager(this);
            Components.Add(ConfigPatchManager);

            Content = new RampContentManager(Services, "Game");
            _assetLoader = new AssetLoader(Content, _workDispatcherThread.Dispatcher, FileManager);
            NamedDirectory.Current.AddLoader(_assetLoader);

            DeclManager = new DeclManager(this);
            Components.Add(DeclManager);
            DeclManager.RegisterTypes(Assembly.GetExecutingAssembly());

            // Create network manager
            Net = new NetManager(this)
            {
                UpdateOrder = 1,
                Enabled = true
            };
            Components.Add(Net);

            // Create file download manager
            Download = new FileDownloadManager(this)
            {
                Enabled = true
            };
            Components.Add(Download);

            // Create network replication manager
            Replica = new ReplicaManager(this)
            {
                UpdateOrder = 3,
                Enabled = true
            };
            Components.Add(Replica);

            // Create input manager
            InputSystem = new InputSystem(this, (Form) Control.FromHandle(Window.Handle))
            {
                UpdateOrder = 1,
                Enabled = true
            };
            Components.Add(InputSystem);

            // Create level manager
            WorldManager = new WorldManager(this)
            {
                UpdateOrder = 2,
                Enabled = true
            };
            Components.Add(WorldManager);

            // Create session manager
            Session = new SessionManager(this, Version, MinVersion, MagicNumber)
            {
                UpdateOrder = 0,
                Enabled = true
            };
            Components.Add(Session);

            // Create the level renderer
            LevelRenderer = new LevelRenderer(this)
            {
                UpdateOrder = 20,
                DrawOrder = 4,
                Visible = true,
                Enabled = true
            };
            Components.Add(LevelRenderer);

            // Create UI
            UI = new UIManager(this)
            {
                UpdateOrder = 21,
                DrawOrder = 5,
                Visible = true,
                Enabled = true
            };
            Components.Add(UI);

            MainMenuManager = new MainMenuManager(this);
            Components.Add(MainMenuManager);
        }

        /// <summary>
        ///     Draws and presents the loading screen immediately. Call this before a long operation.
        /// </summary>
        protected virtual void DrawLoadingScreen()
        {
            GraphicsDevice.Clear(Color.Black);
            UI.DrawLoadingScreen();
            GraphicsDevice.Present();
        }
    }
}