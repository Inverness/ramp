namespace Ramp.Data
{
    public sealed class TestDataRow : DataTableRow
    {
        public TestDataRow(string name, DataTable outer)
            : base(name, outer)
        {
        }

        public int Int { get; private set; }

        public string String { get; private set; }
    }
}