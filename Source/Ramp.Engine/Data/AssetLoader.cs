using System;
using System.Threading;
using System.Threading.Tasks;
using NLog;
using Microsoft.Xna.Framework.Content;
using Ramp.Naming;
using Ramp.Threading;

namespace Ramp.Data
{
    /// <summary>
    ///     Loads any objects of type Asset&lt;T&gt; using the MonoGame content manager.
    /// </summary>
    public sealed class AssetLoader : IDisposable, INamedLoader
    {
        private static readonly Logger s_log = LogManager.GetCurrentClassLogger();

        private readonly RampContentManager _manager;
        private readonly Dispatcher _workDispatcher;
        private readonly IFileService _fileService;

        public AssetLoader(RampContentManager manager, Dispatcher workDispatcher, IFileService fileService)
        {
            Validate.ArgumentNotNull(manager, nameof(manager));
            Validate.ArgumentNotNull(workDispatcher, nameof(workDispatcher));

            _manager = manager;
            _workDispatcher = workDispatcher;
            _fileService = fileService;
        }

        public async Task<NamedObject> TryLoad(Type type, NamedObject outer, string name, CancellationToken ct)
        {
            Validate.ArgumentNotNull(type, "type");

            if (!type.IsGenericType || type.GetGenericTypeDefinition() != typeof(Asset<>))
                return null;

            Type assetType = type.GetGenericArguments()[0];
            
            if (!_fileService.HasEntry(name))
            {
                s_log.Trace("File entry not found {0} {1}", name, assetType);
                return null;
            }

            object asset;
            try
            {
                asset = await _workDispatcher.InvokeAsync(() => LoadImpl(assetType, name), ct);
            }
            catch (ContentLoadException ex)
            {
                s_log.Error("Foad failed {0} {1}", type.Name, name);
                s_log.Error(ex);
                return null;
            }

            if (asset == null)
                return null;

            return (NamedObject) Activator.CreateInstance(type, asset, name, _manager, name, outer);
        }

        public void Dispose()
        {
            _manager.Dispose();
        }
        
        private object LoadImpl(Type assetType, string name)
        {
            s_log.Trace("Load {0} {1}", name, assetType);
            return _manager.Load(assetType, name);
        }
    }
}