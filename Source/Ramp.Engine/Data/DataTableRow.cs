using System.ComponentModel;
using Ramp.Naming;

namespace Ramp.Data
{
    /// <summary>
    /// A row in a data table.
    /// </summary>
    public abstract class DataTableRow : NamedObject, ISupportInitialize
    {
        protected DataTableRow(string name, DataTable outer)
            : base(name, outer)
        {
            Validate.ArgumentNotNull(outer, nameof(outer));
        }

        public virtual void BeginInit()
        {
        }

        public virtual void EndInit()
        {
        }
    }
}