using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Ramp.Naming;

namespace Ramp.Data
{
    /// <summary>
    ///     The base class for all decl objects.
    /// </summary>
    public abstract class Decl : NamedObject
    {
        protected Decl(string name, NamedObject outer)
            : base(name, outer)
        {
        }

        ///  <summary>
        ///      Called after construction and during reloads to update the decl's state. By default this method uses the
        /// 		default JsonSerializer to populate the decl.
        ///  </summary>
        /// <param name="decl"> The decl object. </param>
        /// <param name="validated"> Whether the JSON was validated. </param>
        /// <param name="manager"> The DeclManager that owns this instance. </param>
        public virtual Task Update(JObject decl, bool validated, DeclManager manager)
        {
            using (var reader = new JTokenReader(decl))
                manager.DeclSerializer.Populate(reader, this);

            return Task.CompletedTask;
        }
    }
}