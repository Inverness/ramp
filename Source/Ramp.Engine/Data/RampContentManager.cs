using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using Microsoft.Xna.Framework.Content;

namespace Ramp.Data
{
    public sealed class RampContentManager : ContentManager
    {
        private readonly object _instanceLock = new object();
        private readonly string _name;
        private readonly IFileService _fileService;

        private readonly Dictionary<Type, MethodInfo> _methods = new Dictionary<Type, MethodInfo>();
        private readonly List<IDisposable> _disposableAssets;

        private static readonly MethodInfo s_genericLoadMethod;


        public RampContentManager(IServiceProvider provider, string name = null)
            : base(provider)
        {
            RootDirectory = "";
            _name = name ?? "Unnamed";
            _fileService = provider.GetRequiredService<IFileService>();

            // HACK: ContentManager exposes LoadedAssets but not its disposable assets list. Disposable assets must be
            //       cleared from this list in order to avoid a memory leak.

            FieldInfo field = typeof(ContentManager).GetField("disposableAssets", BindingFlags.Instance |
                                                                                  BindingFlags.NonPublic);
            Debug.Assert(field != null, "field != null");
            _disposableAssets = (List<IDisposable>) field.GetValue(this);
        }

        static RampContentManager()
        {
            s_genericLoadMethod = typeof(ContentManager).GetMethod("Load", new[] { typeof(string) });
            Debug.Assert(s_genericLoadMethod.IsGenericMethodDefinition);
        }

        public override T Load<T>(string assetName)
        {
            lock (_instanceLock)
                return base.Load<T>(assetName);
        }

        public object Load(Type type, string assetName)
        {
            lock (_instanceLock)
            {
                MethodInfo m;
                if (!_methods.TryGetValue(type, out m))
                {
                    m = s_genericLoadMethod.MakeGenericMethod(type);
                    _methods.Add(type, m);
                }

                try
                {
                    return m.Invoke(this, new object[] { assetName });
                }
                catch (TargetInvocationException ex)
                {
                    if (ex.InnerException != null)
                        throw ex.InnerException;
                    throw;
                }
            }
        }

        public void Unload<T>(string assetName)
        {
            Unload(typeof(T), assetName);
        }

        public void Unload(Type type, string assetName)
        {
            IDisposable disposable;

            lock (_instanceLock)
            {
                object asset;
                if (!LoadedAssets.TryGetValue(assetName, out asset))
                    return;

                LoadedAssets.Remove(assetName);

                // If an asset isn't in the disposable list it should not be handled

                disposable = asset as IDisposable;
                if (disposable != null)
                {
                    int index = _disposableAssets.IndexOf(disposable);
                    if (index != -1)
                    {
                        _disposableAssets.RemoveAtSwap(index);
                    }
                    else
                    {
                        disposable = null;
                    }
                }
            }

            disposable?.Dispose();
        }

        protected override Stream OpenStream(string assetName)
        {
            FileManifestEntry entry = _fileService.GetEntry(assetName);
            if (entry == null)
                throw new FileNotFoundException(assetName);
            return entry.Open();
        }

        //protected override object ReadRawAsset<T>(string assetName, string originalAssetName)
        //{
        //    if (typeof(T) == typeof(Animation))
        //    {
        //        return Animation.Load(OpenStream(assetName));
        //    }
        //    return base.ReadRawAsset<T>(assetName, originalAssetName);
        //}
    }
}