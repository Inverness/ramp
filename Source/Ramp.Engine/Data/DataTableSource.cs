﻿using System.Collections.Generic;

namespace Ramp.Data
{
    /// <summary>
    /// A MonoGame asset that is the result of parsing data from a spreadsheet for use in a DataTable.
    /// </summary>
    public sealed class DataTableSource
    {
        public const string NullString = "\0";

        public Dictionary<string, string[,]> Tables { get; } = new Dictionary<string, string[,]>();
    }
}
