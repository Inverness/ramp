using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Ramp.Naming;

namespace Ramp.Data
{
    /// <summary>
    ///     Wraps a SharpDX Toolkit asset in a named object, allowing it to be asynchronously loaded and referenced.
    ///     The asset is unloaded when this object is disposed.
    /// </summary>
    /// <typeparam name="T"> The asset type. </typeparam>
    public sealed class Asset<T> : StrongNamedBinding<T>
        where T : class
    {
        private RampContentManager _contentManager;
        private string _assetName;
        private long _memoryPressure;

        public Asset(T asset, string assetName, RampContentManager contentManager, string name, NamedObject outer)
            : base(asset, name, outer)
        {
            // TODO: Load assets on server even if the graphics device does not exist.

            Validate.ArgumentNotNull(asset, nameof(asset));
            Validate.ArgumentNotNull(assetName, nameof(assetName));

            _assetName = assetName;
            _contentManager = contentManager;

            AddMemoryPressure();
        }

        protected override void Dispose(bool disposing)
        {
            RemoveMemoryPressure();

            // Must unload assets when disposed manually or through garbage collection
            if (_contentManager != null && _assetName != null)
            {
                // Assets should be disposed on the same thread that they were loaded on.
                if (disposing)
                {
                    Debug.Assert(CheckAccess());
                    _contentManager.Unload<T>(_assetName);
                }
                else
                {
                    RampContentManager contentManager = _contentManager;
                    string assetName = _assetName;
                    Dispatcher.Invoke(() => contentManager.Unload<T>(assetName));
                }
            }

            _assetName = null;
            _contentManager = null;

            base.Dispose(disposing);
        }

        // Calculate unmanaged memory pressure for the asset. This is a temporary implementation that
        // will only support MonoGame asset types for now.
        private void AddMemoryPressure()
        {
            if (_memoryPressure != 0)
                return;

            long mp;
            if (typeof(T) == typeof(Texture2D))
            {
                var tex = (Texture2D) (object) Target;

                mp = tex.Width * tex.Height * 4;
            }
            else if (typeof(T) == typeof(SpriteFont))
            {
                var sf = (SpriteFont) (object) Target;

                mp = sf.Texture.Width * sf.Texture.Height * 4;
            }
            else
            {
                mp = 0;
            }

            if (mp != 0)
            {
                GC.AddMemoryPressure(mp);
                _memoryPressure = mp;
            }
        }

        private void RemoveMemoryPressure()
        {
            if (_memoryPressure != 0)
            {
                GC.RemoveMemoryPressure(_memoryPressure);
                _memoryPressure = 0;
            }
        }
    }

    public static class AssetExtensions
    {
        /// <summary>
        /// Loads and asset instance.
        /// </summary>
        /// <typeparam name="T">The asset type.</typeparam>
        /// <param name="self"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static Task<Asset<T>> LoadAsset<T>(this NamedDirectory self, string name)
            where T : class
        {
            return self.LoadAsync<Asset<T>>(null, name);
        } 
    }
}