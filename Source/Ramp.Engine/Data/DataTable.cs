﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using JetBrains.Annotations;
using NLog;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Ramp.Naming;
using Ramp.Utilities.Reflection;

namespace Ramp.Data
{
    /// <summary>
    /// A table of values with strongly typed rows that can be overriden or extended by other tables.
    /// </summary>
    public sealed class DataTable : Decl
    {
        private const string NullTableString = "****";

        private static readonly Logger s_log = LogManager.GetCurrentClassLogger();

        private Type _rowType;
        private Func<string, DataTable, DataTableRow> _rowConstructor;
        private readonly List<DataTableRow> _rows = new List<DataTableRow>();
        private readonly Dictionary<string, DataTableRow> _rowMap = new Dictionary<string, DataTableRow>();
        private List<DataTable> _overriders; 

        public DataTable(string name, NamedObject outer)
            : base(name, outer)
        {
        }

        /// <summary>
        /// Gets the name of the DataTableSource this table is using.
        /// </summary>
        [JsonProperty]
        public string Source { get; [UsedImplicitly] private set; }

        /// <summary>
        /// Gets the name of the table within the source that this table is using.
        /// </summary>
        [JsonProperty]
        public string SourceTable { get; [UsedImplicitly] private set; }

        /// <summary>
        /// Gets the name of the type that will be used for this table's rows.
        /// </summary>
        [JsonProperty]
        public string RowTypeName { get; [UsedImplicitly] private set; }

        [JsonProperty]
        public string Override { get;[UsedImplicitly] private set; }

        public IReadOnlyList<DataTableRow> Rows => _rows;

        public T FindRow<T>(string name)
            where T : DataTableRow
        {
            DataTableRow row;
            return _rowMap.TryGetValue(name, out row) ? row as T : null;
        }

        public override async Task Update(JObject decl, bool validated, DeclManager manager)
        {
            await base.Update(decl, validated, manager);

            _rows.Clear();
            _rowMap.Clear();

            _rowType = Type.GetType(RowTypeName, true);
            _rowConstructor = ConstructorDelegateCompiler.Compile<Func<string, DataTable, DataTableRow>>(_rowType);

            try
            {
                if (Source != null)
                {
                    await LoadSourceAsset();
                }
            }
            catch (Exception ex)
            {
                s_log.Error(ex, "Source load failed");
                throw;
            }

            // Override rows in another table.
            if (Override != null)
            {
                var target = Directory.Find<DataTable>(Override);
                if (target != null)
                {
                    if (!target._rowType.IsAssignableFrom(_rowType))
                        throw new InvalidAssetReferenceException(Override, "override table row type not assignable");

                    if (target._overriders == null)
                        target._overriders = new List<DataTable>();
                    target._overriders.Add(this);

                    foreach (DataTableRow row in _rows)
                        target._rows.Add(row);

                    foreach (KeyValuePair<string, DataTableRow> row in _rowMap)
                        target._rowMap[row.Key] = row.Value;
                }
            }
        }

        private async Task LoadSourceAsset()
        {
            Asset<DataTableSource> src = await Directory.LoadAsset<DataTableSource>(Source);

            string tableName = SourceTable ?? src.Target.Tables.Keys.First();

            LoadData(src.Target.Tables[tableName]);
        }

        private void LoadData(string[,] data)
        {
            const BindingFlags allInstanceFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;

            var properties = new PropertyInfo[data.GetLength(1) - 1];
            var converters = new TypeConverter[properties.Length];

            for (int c = 0; c < properties.Length; c++)
            {
                string name = data[0, c + 1];
                PropertyInfo property = _rowType.GetProperty(name, allInstanceFlags);
                if (property == null)
                    throw new InvalidDataException("Invalid property name: " + name);

                // Private setters are not inherited
                if (!property.CanWrite)
                {
                    if (property.DeclaringType != null && property.DeclaringType != _rowType)
                        property = property.DeclaringType.GetProperty(name, allInstanceFlags);
                    if (!property.CanWrite)
                        throw new InvalidDataException("Property is not writeable: " + property);
                }

                properties[c] = property;
                converters[c] = TypeConverterCache.GetConverter(property.PropertyType);
            }

            int rowCount = data.GetLength(0);

            for (int r = 1; r < rowCount; r++)
            {
                string name = data[r, 0];
                DataTableRow row = _rowConstructor(name, this);

                for (int c = 0; c < properties.Length; c++)
                {
                    string sourceValue = data[r, c + 1];
                    try
                    {
                        object value = sourceValue != NullTableString ? converters[c].ConvertFrom(sourceValue) : null;
                        properties[c].SetValue(row, value);
                    }
                    catch (Exception ex)
                    {
                        s_log.Error("Exception while setting property: {0}", properties[c]);
                        s_log.Error(ex);
                        throw;
                    }
                }

                row.EndInit();

                _rowMap[row.Name] = row;
                _rows.Add(row);
            }

            _rows.TrimExcess();
        }
    }
}