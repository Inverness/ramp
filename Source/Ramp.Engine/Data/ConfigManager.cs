using System;
using System.IO;
using NLog;
using Microsoft.Xna.Framework;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Ramp.Data
{
    // Implements IConfigService
    public sealed class ConfigManager : GameComponent, IConfigService
    {
        private static readonly Logger s_log = LogManager.GetCurrentClassLogger();

        public ConfigManager(Game game, string baseFile, string rootDirectory, string userDirectory,
                             string userFileName)
            : base(game)
        {
            Validate.ArgumentNotNull(baseFile, "baseFile");
            Validate.ArgumentNotNull(rootDirectory, "rootDirectory");

            BaseConfigFile = Path.Combine(rootDirectory, baseFile);
            RootDirectory = rootDirectory;
            UserDirectory = userDirectory;
            UserConfigFile = Path.Combine(UserDirectory, userFileName);
            Database = new ConfigDatabase();

            // The Init node will contain data only loaded to initialize other services. This node is removed
            // after initialization is complete.
            Database.Root["Init"] = new JObject();

            // Contains all system-wide data that is stored along with the executable.
            Database.Root["System"] = new JObject();

            // Contains all data that is saved to and loaded from disk for each computer user.
            Database.Root["User"] = new JObject();

            game.Services.AddService(typeof(IConfigService), this);
        }

        public ConfigDatabase Database { get; }

        public string Name { get; set; }

        public string BaseConfigFile { get; set; }

        public string RootDirectory { get; }

        public Guid SystemGuid { get; private set; }

        public string UserConfigFile { get; }

        public string UserDirectory { get; }

        public override void Initialize()
        {
            if (!Directory.Exists(UserDirectory))
                Directory.CreateDirectory(UserDirectory);

            s_log.Info("Loading base configuration file: {0}", BaseConfigFile);
            Database.ApplyPatchSet((JObject) JsonUtility.LoadYaml(BaseConfigFile));

            if (File.Exists(UserConfigFile))
            {
                var userData = (JObject) JsonUtility.LoadYaml(UserConfigFile);
                Database.ApplyPatchSet(userData);
            }

            JToken guidToken = Database.GetToken("User.SystemGuid");
            if (guidToken == null)
            {
                SystemGuid = Guid.NewGuid();
                Database.SetToken("User.SystemGuid", SystemGuid);
            }
            else
            {
                SystemGuid = Guid.Parse((string) guidToken);
            }

            s_log.Info("System GUID: " + SystemGuid);
        }

        public void Save()
        {
            s_log.Info("Saving User node to: {0}", UserConfigFile);
            var ps = Database.CreatePatchSet("User");

            JsonUtility.Save(UserConfigFile, ps);
        }

        public void FinishInitialization()
        {
            s_log.Debug("Removing Init node");
            Database.Root.Remove("Init");
        }
    }
}