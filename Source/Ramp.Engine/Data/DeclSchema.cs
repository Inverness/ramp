﻿using System;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using Ramp.Naming;

namespace Ramp.Data
{
    public class DeclSchema : Decl
    {
        public DeclSchema(string name, NamedObject outer)
            : base(name, outer)
        {
        }

        public JSchema Schema { get; private set; }

        public override Task Update(JObject decl, bool validated, DeclManager manager)
        {
            Schema = JSchema.Load(new JTokenReader(decl));
            return Task.CompletedTask;
        }
    }
}