using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using Microsoft.Xna.Framework;

namespace Ramp.Data
{
    /// <summary>
    ///     Manages the setup of TypeConverter instances for XNA types.
    /// </summary>
    public static class XnaConverter
    {
        private static readonly Tuple<Type, TypeConverterAttribute>[] s_converters =
        {
            Tuple.Create(typeof(Rectangle), new TypeConverterAttribute(typeof(RectangleConverter))),
            Tuple.Create(typeof(Point), new TypeConverterAttribute(typeof(PointConverter))),
            Tuple.Create(typeof(Color), new TypeConverterAttribute(typeof(ColorConverter))),
            Tuple.Create(typeof(Vector2), new TypeConverterAttribute(typeof(Vector2Converter))),
            Tuple.Create(typeof(Vector3), new TypeConverterAttribute(typeof(Vector3Converter)))
        };

        static XnaConverter()
        {
            // This fixes an issue when trying to get a TypeConverter for a type that existed in a dynamically loaded
            // assembly. This is necessary so the type converters can be used in the content pipeline at build time.
            AppDomain.CurrentDomain.AssemblyResolve += OnAssemblyResolve;
        }

        /// <summary>
        ///     Adds XNA type converters to the type descriptor system and returns an object which can be disposed
        ///     to remove them.
        /// </summary>
        /// <returns> An object that can be disposed to remove XNA type converters. </returns>
        public static IDisposable AddTypeConverters()
        {
            var providers = new TypeDescriptionProvider[s_converters.Length];
            for (int i = 0; i < s_converters.Length; i++)
                providers[i] = TypeDescriptor.AddAttributes(s_converters[i].Item1, s_converters[i].Item2);

            return new Scope(providers);
        }

        private static Assembly OnAssemblyResolve(object sender, ResolveEventArgs args)
        {
            return ((AppDomain) sender).GetAssemblies().FirstOrDefault(a => a.FullName == args.Name);
        }

        private class Scope : IDisposable
        {
            private TypeDescriptionProvider[] _providers;

            public Scope(TypeDescriptionProvider[] providers)
            {
                _providers = providers;
            }

            public void Dispose()
            {
                if (_providers == null)
                    return;

                for (int i = 0; i < s_converters.Length; i++)
                    TypeDescriptor.RemoveProvider(_providers[i], s_converters[i].Item1);

                _providers = null;
            }
        }
    }

    internal class PointConverter : StringTypeConverter
    {
        private readonly TypeConverter _itemConverter = TypeDescriptor.GetConverter(typeof(int));

        protected override object ConvertFromStringCore(ITypeDescriptorContext context, CultureInfo culture,
                                                        string value)
        {
            int[] values = ConvertValuesFromString<int>(_itemConverter, context, culture, value, 2);
            return new Point(values[0], values[1]);
        }

        protected override string ConvertToStringCore(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            var point = (Point) value;
            return ConvertValuesToString(_itemConverter, context, culture, new[] { point.X, point.Y });
        }
    }

    internal class RectangleConverter : StringTypeConverter
    {
        private readonly TypeConverter _itemConverter = TypeDescriptor.GetConverter(typeof(int));

        protected override object ConvertFromStringCore(ITypeDescriptorContext context, CultureInfo culture,
                                                        string value)
        {
            int[] values = ConvertValuesFromString<int>(_itemConverter, context, culture, value, 4);
            return new Rectangle(values[0], values[1], values[2], values[3]);
        }

        protected override string ConvertToStringCore(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            var rect = (Rectangle) value;
            return ConvertValuesToString(_itemConverter, context, culture, new[] { rect.X, rect.Y, rect.Width, rect.Height });
        }
    }

    internal class ColorConverter : StringTypeConverter
    {
        private readonly TypeConverter _itemConverter = TypeDescriptor.GetConverter(typeof(byte));

        private static readonly Dictionary<string, Color> s_colors;
        //private static readonly Dictionary<Color, string> s_names; 

        static ColorConverter()
        {
            s_colors = new Dictionary<string, Color>();
            //s_names = new Dictionary<Color, string>();

            foreach (PropertyInfo property in typeof(Color).GetProperties(BindingFlags.Static | BindingFlags.Public))
            {
                if (property.PropertyType == typeof(Color))
                {
                    var color = (Color) property.GetValue(null);
                    s_colors[property.Name] = color;
                    //s_names[color] = property.Name;
                }
            }
        }

        protected override object ConvertFromStringCore(ITypeDescriptorContext context, CultureInfo culture,
                                                        string value)
        {
            value = value.Trim();

            if (value.Length != 0 && char.IsLetter(value[0]))
            {
                Color result;
                if (!s_colors.TryGetValue(value, out result))
                    throw new FormatException("Invalid color name: " + value);
                return result;
            }

            byte[] values = ConvertValuesFromString<byte>(_itemConverter, context, culture, value, 4);
            return new Color(values[0], values[1], values[2], values[3]);
        }

        protected override string ConvertToStringCore(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            var color = (Color) value;

            //string name;
            //if (s_names.TryGetValue(color, out name))
            //    return name;

            return ConvertValuesToString(_itemConverter, context, culture, new[] { color.R, color.G, color.B, color.A });
        }
    }

    internal class Vector2Converter : StringTypeConverter
    {
        private readonly TypeConverter _itemConverter = TypeDescriptor.GetConverter(typeof(float));

        protected override object ConvertFromStringCore(ITypeDescriptorContext context, CultureInfo culture,
                                                        string value)
        {
            float[] values = ConvertValuesFromString<float>(_itemConverter, context, culture, value, 2);
            return new Vector2(values[0], values[1]);
        }

        protected override string ConvertToStringCore(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            var v = (Vector2) value;
            return ConvertValuesToString(_itemConverter, context, culture, new[] { v.X, v.Y });
        }
    }

    internal class Vector3Converter : StringTypeConverter
    {
        private readonly TypeConverter _itemConverter = TypeDescriptor.GetConverter(typeof(float));

        protected override object ConvertFromStringCore(ITypeDescriptorContext context, CultureInfo culture,
                                                        string value)
        {
            float[] values = ConvertValuesFromString<float>(_itemConverter, context, culture, value, 3);
            return new Vector3(values[0], values[1], values[2]);
        }

        protected override string ConvertToStringCore(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            var v = (Vector3) value;
            return ConvertValuesToString(_itemConverter, context, culture, new[] { v.X, v.Y, v.Z });
        }
    }
}