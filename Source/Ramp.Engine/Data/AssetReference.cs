using System.Threading.Tasks;
using Ramp.Naming;
using Ramp.Network;

namespace Ramp.Data
{
    /// <summary>
    ///     Wraps a NamedObjectReference containing an Asset&lt;T&gt;. Automatically loads the target
    ///     object when constructed with a name or when the name is changed.
    /// </summary>
    /// <typeparam name="T"> The asset type. </typeparam>
    public sealed class AssetReference<T> : NamedReference<Asset<T>>
        where T : class
    {
        public AssetReference()
        {
        }

        public AssetReference(string name)
            : base(name)
        {
            LoadTarget();
        }

        public T LoadTargetAsset() => LoadTarget()?.Target;

        public async Task<T> LoadTargetAssetAsync() => (await LoadTargetAsync())?.Target;

        public void Serialize(DeltaWriter writer, ReplicationContext context)
        {
            writer.Write(Name);
        }

        public void Deserialize(DeltaReader reader, ReplicationContext context)
        {
            string fullName = null;
            if (reader.Read(ref fullName))
                Name = fullName;
        }

        protected override void OnNameChanged()
        {
            LoadTarget();
        }
    }
}