using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using NLog;
using Microsoft.Xna.Framework;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using Ramp.Naming;
using Ramp.Threading;

namespace Ramp.Data
{
    /// <summary>
    ///     Loads and provides objects created from small declaration ("decl") files.
    /// </summary>
    public class DeclManager : GameComponent
    {
        private const int MaxNameSize = 32;

        // Registered decl types
        private static readonly Dictionary<string, Type> s_types = new Dictionary<string, Type>();

        private static readonly Logger s_log = LogManager.GetCurrentClassLogger();

        private readonly IConfigService _config;

        private NamedDirectory _directory;
        private readonly HashSet<Decl> _decls = new HashSet<Decl>();

        static DeclManager()
        {
            RegisterTypes();
        }

        /// <summary>
        ///     Initialize a DeclManager.
        /// </summary>
        /// <param name="game"> The game. </param>
        public DeclManager(RampGame game)
            : base(game)
        {
            _config = game.Services.GetRequiredService<IConfigService>();

            Name = "DeclManager";
        }

        /// <inheritdoc />
        public string Name { get; set; }

        public JsonSerializer DeclSerializer { get; } = new JsonSerializer();

        /// <summary>
        ///     Register the names of all decl types in an assembly.
        /// </summary>
        /// <param name="assembly"> The assembly to register. </param>
        public static void RegisterTypes(Assembly assembly = null)
        {
            if (assembly == null)
                assembly = Assembly.GetCallingAssembly();

            Type declType = typeof(Decl);

            foreach (Type type in assembly.GetExportedTypes())
            {
                if (type.IsAbstract || !type.IsSubclassOf(declType))
                    continue;

                s_types[type.Name] = type;
            }
        }

        /// <inheritdoc />
        public override void Initialize()
        {
            _directory = NamedDirectory.Current;

            s_log.Debug("Loading decl files...");

            var decls = _config.Database.GetToken("Init.Decls") as JObject;
            if (decls != null)
                LoadDeclFile(decls);

            //var declFiles = (JArray) _config.Database.GetToken("System.Decl.Files");
            //if (declFiles != null)
            //{
            //    foreach (string filename in declFiles.Values<string>())
            //    {
            //        FileManifestEntry entry = _fileService.GetEntry(filename);
            //        if (entry == null)
            //        {
            //            Log.ErrorFormat("  Invalid decl filename: " + filename);
            //            continue;
            //        }

            //        Log.DebugFormat("  {0}...", filename);

            //        using (var reader = new JsonTextReader(new StreamReader(entry.Open())))
            //            LoadDeclFile(JObject.Load(reader));
            //    }
            //}
        }

        private void LoadDeclFile(JObject declFile)
        {
            foreach (KeyValuePair<string, JToken> typeGroup in declFile)
            {
                if (typeGroup.Value.Type != JTokenType.Object)
                    throw new InvalidDataException("Expected object for type group: " + typeGroup.Key);

                foreach (KeyValuePair<string, JToken> decl in (JObject) typeGroup.Value)
                {
                    if (decl.Value.Type != JTokenType.Object)
                        throw new InvalidDataException("Expected object for decl name: " + decl.Key);

                    try
                    {
                        LoadDecl(typeGroup.Key, decl.Key, (JObject) decl.Value);
                    }
                    catch (Exception ex)
                    {
                        s_log.Error(ex, "LoadDecl failed");
                        throw;
                    }
                }
            }
        }

        private void LoadDecl(string typeName, string name, JObject data, bool noValidate = false)
        {
            if (name.Length > MaxNameSize)
                throw new InvalidDataException("Decl name too long: " + name);

            Type type = ResolveDeclType(typeName);

            RemoveExisting(type, name);

            // Validate using the schema if one is available.
            bool validated = !noValidate && ValidateDecl(typeName, name, type, data);

            var decl = (Decl) Activator.CreateInstance(type, name, null);

            Task updateTask = decl.Update(data, validated, this);

            if (!updateTask.IsCompleted)
            {
                // The update task likely has callbacks on this thread that must be run before the loading
                // can continue. Must wait for it to complete.
                Dispatcher.Current.Wait(updateTask);
            }

            _decls.Add(decl);
        }

        private bool ValidateDecl(string typeName, string name, Type declType, JObject data)
        {
            var schema = _directory.Find<DeclSchema>(declType.Name);
            if (schema == null)
                return false;

            try
            {
                data.Validate(schema.Schema);
                return true;
            }
            catch (JSchemaException ex)
            {
                throw new InvalidDataException($"Decl validation failed for {typeName} {name}", ex);
            }
        }

        private void RemoveExisting(Type type, string name)
        {
            var existing = (Decl) _directory.Find(type, name);
            if (existing != null)
            {
                s_log.Warn("Decl key conflict: Name = {0}, Type = {1}", name, type.Name);
                existing.Dispose();
                _decls.Remove(existing);
            }
        }

        private static Type ResolveDeclType(string typeName)
        {
            Type type;
            if (!s_types.TryGetValue(typeName, out type))
                throw new InvalidDataException("Decl type not found: " + typeName);
            return type;
        }
    }
}