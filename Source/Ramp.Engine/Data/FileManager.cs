using System.Collections.Generic;
using System.IO;
using System.Linq;
using NLog;
using Microsoft.Xna.Framework;
using Newtonsoft.Json.Linq;

namespace Ramp.Data
{
    /// <summary>
    ///     Manages file metadata including a manifest of content files keyed by the filename without
    ///     extension in order to simplify content management and references.
    /// </summary>
    public sealed class FileManager : GameComponent, IFileService
    {
        private static readonly Logger s_log = LogManager.GetCurrentClassLogger();

        private readonly IConfigService _config;
        private FileManifest[] _manifests;

        public FileManager(Game game)
            : base(game)
        {
            _config = game.Services.GetRequiredService<IConfigService>();
            game.Services.AddService(typeof(IFileService), this);
        }

        public IReadOnlyList<FileManifest> Manifests => _manifests;

        public override void Initialize()
        {
            IEnumerable<JToken> systemRoots = _config.Database.GetTokens("System.File.Roots");
            IEnumerable<JToken> userRoots = _config.Database.GetTokens("User.File.Roots");

            var manifests = new List<FileManifest>();
            foreach (string root in systemRoots.Union(userRoots))
            {
                s_log.Debug("Loading file root {0}", root);
                FileManifest m;
                if (File.Exists(root))
                    m = new ZipFileManiest(root);
                else if (Directory.Exists(root))
                    m = new DirectoryFileManifest(root, false);
                else
                    throw new FileNotFoundException("invalid root: " + root);
                m.Update();
                manifests.Add(m);
            }
            _manifests = manifests.ToArray();

            //LoadManifest();
            //CreateManifest(false);
            //SaveManifest();
        }

        public bool HasEntry(string name)
        {
            return _manifests.Any(t => t.HasEntry(name));
        }

        public FileManifestEntry GetEntry(string name)
        {
            // Manifests last in the list have priority
            for (int i = _manifests.Length - 1; i > -1; i--)
            {
                FileManifestEntry entry = _manifests[i].GetEntry(name);
                if (entry != null)
                    return entry;
            }
            return null;
        }

        public IList<FileManifestEntry> GetEntries(string pattern, bool regex = false,
                                                   bool sort = false)
        {
            var results = new List<FileManifestEntry>();

            foreach (FileManifest m in _manifests)
                results.AddRange(m.GetEntries(pattern, regex, sort));

            return results;
        }

        ///// <summary>
        /////     Save the manifest to the file specified by ManifestPath.
        ///// </summary>
        //public void SaveManifest()
        //{
        //    string directory = Path.GetDirectoryName(ManifestPath) ?? "";
        //    if (!Directory.Exists(directory ?? "."))
        //        Directory.CreateDirectory(directory);

        //    using (var stream = File.OpenWrite(ManifestPath))
        //        _manifest.Serialize(stream);
        //}

        //public void LoadManifest()
        //{
        //    string directory = Path.GetDirectoryName(ManifestPath) ?? "";
        //    if (!Directory.Exists(directory ?? "."))
        //        Directory.CreateDirectory(directory);

        //    if (!File.Exists(ManifestPath))
        //    {
        //        _manifest = new FileManifest();
        //        return;
        //    }

        //    using (var stream = File.OpenRead(ManifestPath))
        //        _manifest = FileManifest.Deserialize(stream);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                _manifests.ForEach(fm => fm.Dispose());
            base.Dispose(disposing);
        }

        //// Creates a manifest based on files in the specified directory and subdirectories with
        //// the specified extensions and an optional hasher.
        //private void CreateManifest(bool hash)
        //{
        //    Log.DebugFormat("Creating file manifest...");

        //    _manifest = new FileManifest();

        //    uint count = 0;
        //    foreach (string path in Roots)
        //        count += _manifest.Update(path, hash, Extensions, ContentExtensions);

        //    Log.DebugFormat("Finished creating manifest; {0} files found.", count);
        //}
    }
}