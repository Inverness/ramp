﻿using System;

namespace Ramp.Data
{
    [Serializable]
    public class InvalidAssetReferenceException : Exception
    {
        public InvalidAssetReferenceException(string name)
            : base(FormatMessage(name))
        {
        }

        public InvalidAssetReferenceException(string name, string message)
            : base(FormatMessage(name, message))
        {
        }

        public InvalidAssetReferenceException(string name, Exception innerException)
            : base(FormatMessage(name), innerException)
        {
        }

        public InvalidAssetReferenceException(string name, string message, Exception innerException)
            : base(FormatMessage(name, message), innerException)
        {
        }

        private static string FormatMessage(string name)
        {
            if (name == null)
                throw new ArgumentNullException(nameof(name));
            return string.Format("'{0}' does not refer to a valid asset.", name);
        }

        private static string FormatMessage(string name, string message)
        {
            if (name == null)
                throw new ArgumentNullException(nameof(name));
            return string.Format("'{0}' does not refer to a valid asset, {1}", name, message);
        }
    }
}