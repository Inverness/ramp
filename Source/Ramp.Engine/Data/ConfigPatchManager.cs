using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.Xna.Framework;
using Newtonsoft.Json.Linq;

namespace Ramp.Data
{
    /// <summary>
    ///     Handles finding and applying config files after the file service has been loaded.
    /// </summary>
    public sealed class ConfigPatchManager : GameComponent
    {
        private readonly IConfigService _configService;
        private readonly IFileService _fileService;

        public ConfigPatchManager(Game game)
            : base(game)
        {
            _configService = game.Services.GetRequiredService<IConfigService>();
            _fileService = game.Services.GetRequiredService<IFileService>();
        }

        public override void Initialize()
        {
            IEnumerable<JProperty> files = _configService.Database.GetTokens("System.Config.Patches").Cast<JProperty>();

            foreach (JProperty fileEntry in files)
            {
                string filePattern = fileEntry.Name;
                string prefix = (string) fileEntry.Value;

                foreach (FileManifestEntry entry in _fileService.GetEntries(filePattern))
                {
                    JContainer doc;
                    using (StreamReader reader = entry.OpenText())
                        doc = JsonUtility.LoadYaml(reader);

                    _configService.Database.ApplyPatchSet((JObject) doc, prefix);
                }
            }
        }
    }
}