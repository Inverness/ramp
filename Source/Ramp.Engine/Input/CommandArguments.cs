using System.Collections.Generic;

namespace Ramp.Input
{
    public class CommandArguments
    {
        public CommandArguments(IReadOnlyList<string> values)
        {
            Validate.ArgumentNotNull(values, "values");
            Validate.ArgumentInRange(values.Count <= byte.MaxValue, "values",
                                     "argument count must not exceed byte.MaxValue");

            Values = values;
        }

        /// <summary>
        ///		Gets or sets whether the command was handled.
        /// </summary>
        public bool Handled { get; set; }

        /// <summary>
        ///		Gets the command arguments.
        /// </summary>
        public IReadOnlyList<string> Values { get; private set; }

        public string this[byte index] => Values[index];

        public string GetValueOrDefault(byte index, string defaultValue = null)
        {
            return index < Values.Count ? Values[index] : defaultValue;
        }

        public T GetValue<T>(byte index)
        {
            return GenericTypeConverter.Convert<T>(Values[index]);
        }

        public T GetValueOrDefault<T>(byte index, T defaultValue = default(T))
        {
            return index < Values.Count ? GenericTypeConverter.Convert<T>(Values[index]) : defaultValue;
        }
    }
}