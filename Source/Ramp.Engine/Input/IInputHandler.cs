﻿using System;

namespace Ramp.Input
{
    /// <summary>
    ///     An object that handles input events.
    /// </summary>
    public interface IInputHandler
    {
        /// <summary>
        ///     Called when an input event if received.
        /// </summary>
        /// <param name="type"> The input event type. </param>
        /// <param name="e"> The event arguments. Can be cast to a derived type for mouse and key events. </param>
        void OnInputEvent(InputEventType type, EventArgs e);
    }
}