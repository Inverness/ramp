﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.Xna.Framework;

namespace Ramp.Input
{
    public sealed class CommandVariableSystem : GameComponent, ICommandVariableService
    {
        private readonly object _instanceLock = new object();
        private readonly ICommandService _commandService;
        private readonly Dictionary<string, CommandVariable> _vars = new Dictionary<string, CommandVariable>();

        public CommandVariableSystem(Game game)
            : base(game)
        {
            _commandService = game.Services.GetRequiredService<ICommandService>();
            game.Services.AddService(typeof(ICommandVariableService), this);
        }

        public override void Initialize()
        {
            _commandService.AddCommand("Get", OnCommandGet);
            _commandService.AddCommand("Set", OnCommandSet);
            AddVariable(new CommandVariable<double?>("TestVar"));
        }

        public CommandVariable GetVariable(string name)
        {
            Validate.ArgumentNotNull(name, "name");

            lock (_instanceLock)
            {
                CommandVariable result;
                _vars.TryGetValue(name, out result);
                return result;
            }
        }

        public CommandVariable<T> GetVariable<T>(string name)
        {
            return (CommandVariable<T>) GetVariable(name);
        }

        public void AddVariable(CommandVariable variable)
        {
            Validate.ArgumentNotNull(variable, "variable");

            lock (_instanceLock)
            {
                if (_vars.ContainsKey(variable.Name))
                    throw new ArgumentException("Variable with same name is already registered", nameof(variable));

                _vars[variable.Name] = variable;
            }
        }

        public CommandVariable<T> AddVariable<T>(string name, T value = default(T))
        {
            Validate.ArgumentNotNull(name, "name");

            var cvar = new CommandVariable<T>(name, value);
            AddVariable(cvar);
            return cvar;
        }

        public bool RemoveVariable(string name)
        {
            lock (_instanceLock)
                return _vars.Remove(name);
        }

        private void OnCommandGet(ICommandService source, CommandArguments args)
        {
            string vname = args[0];
            CommandVariable cvar = GetVariable(vname);
            if (cvar == null)
                throw new ArgumentException("invalid cvar name");

            source.PrintLine("{0} = {1}", vname, cvar.ObjectValue != null ? cvar.ObjectValue.ToString() : "<null>");
        }

        private void OnCommandSet(ICommandService source, CommandArguments args)
        {
            string vname = args[0];
            string value = args[1];

            CommandVariable cvar = GetVariable(vname);
            if (cvar == null)
                throw new ArgumentException("invalid cvar name");

            cvar.ObjectValue = TypeDescriptor.GetConverter(cvar.ValueType).ConvertFrom(value);

            source.PrintLine("Set {0} = {1}", vname, cvar.ObjectValue != null ? cvar.ObjectValue.ToString() : "<null>");
        }
    }
}