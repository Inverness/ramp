﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Microsoft.Xna.Framework;

namespace Ramp.Input
{
    public class InputSystem : GameComponent, IInputService
    {
        private const int InputListSize = 20;

        private readonly Form _form;

        private readonly List<InputEvent> _inputEvents = new List<InputEvent>(InputListSize);

        private readonly List<IInputHandler> _handlers = new List<IInputHandler>();

        public InputSystem(Game game, Form form)
            : base(game)
        {
            _form = form;
            game.Services.AddService(typeof(IInputService), this);
        }

        public override void Initialize()
        {
            // We'll receieve input by handling the window messages directly
            _form.MouseMove += OnMouseMove;
            _form.MouseWheel += OnMouseWheel;
            _form.MouseDown += OnMouseDown;
            _form.MouseUp += OnMouseUp;
            _form.KeyDown += OnKeyDown;
            _form.KeyUp += OnKeyUp;
            _form.Activated += OnActivated;
            _form.Deactivate += OnDeactivated;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _form.MouseMove -= OnMouseMove;
                _form.MouseWheel -= OnMouseWheel;
                _form.MouseDown -= OnMouseDown;
                _form.MouseUp -= OnMouseUp;
                _form.KeyDown -= OnKeyDown;
                _form.KeyUp -= OnKeyUp;
                _form.Activated -= OnActivated;
                _form.Deactivate -= OnDeactivated;
            }
            base.Dispose(disposing);
        }

        public override void Update(GameTime gameTime)
        {
            if (_inputEvents.Count == 0)
                return;

            // ReSharper disable ForCanBeConvertedToForeach
            for (int i = 0; i < _inputEvents.Count; i++)
            {
                // Dispatch input events to their handlers
                InputEvent inputEvent = _inputEvents[i];
                for (int h = 0; h < _handlers.Count; h++)
                    _handlers[h].OnInputEvent(inputEvent.EventType, inputEvent.Args);
            }
            _inputEvents.Clear();
            // ReSharper restore ForCanBeConvertedToForeach
        }

        public void AddHandler(IInputHandler handler)
        {
            Validate.ArgumentNotNull(handler, "handler");
            _handlers.Add(handler);
        }

        public void RemoveHandler(IInputHandler handler)
        {
            Validate.ArgumentNotNull(handler, "handler");
            _handlers.Remove(handler);
        }

        protected void OnMouseDown(object sender, MouseEventArgs e)
        {
            if (_inputEvents.Count < _inputEvents.Capacity)
                _inputEvents.Add(new InputEvent(InputEventType.MouseDown, e));
        }

        protected void OnMouseUp(object sender, MouseEventArgs e)
        {
            if (_inputEvents.Count < _inputEvents.Capacity)
                _inputEvents.Add(new InputEvent(InputEventType.MouseUp, e));
        }

        protected void OnMouseMove(object sender, MouseEventArgs e)
        {
            if (_inputEvents.Count < _inputEvents.Capacity)
                _inputEvents.Add(new InputEvent(InputEventType.MouseMove, e));
        }

        protected void OnMouseWheel(object sender, MouseEventArgs e)
        {
            if (_inputEvents.Count < _inputEvents.Capacity)
                _inputEvents.Add(new InputEvent(InputEventType.MouseWheel, e));
        }

        protected void OnKeyDown(object sender, KeyEventArgs e)
        {
            if (_inputEvents.Count < _inputEvents.Capacity)
                _inputEvents.Add(new InputEvent(InputEventType.KeyDown, e));
        }

        protected void OnKeyUp(object sender, KeyEventArgs e)
        {
            if (_inputEvents.Count < _inputEvents.Capacity)
                _inputEvents.Add(new InputEvent(InputEventType.KeyUp, e));
        }

        protected void OnActivated(object sender, EventArgs e)
        {
            if (_inputEvents.Count < _inputEvents.Capacity)
                _inputEvents.Add(new InputEvent(InputEventType.WindowActivated, e));
        }

        protected void OnDeactivated(object sender, EventArgs e)
        {
            if (_inputEvents.Count < _inputEvents.Capacity)
                _inputEvents.Add(new InputEvent(InputEventType.WindowDeactivated, e));
        }

        private struct InputEvent
        {
            public readonly InputEventType EventType;

            public readonly EventArgs Args;

            public InputEvent(InputEventType type, EventArgs args)
            {
                EventType = type;
                Args = args;
            }
        }
    }
}