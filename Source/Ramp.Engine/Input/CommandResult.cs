﻿using System;

namespace Ramp.Input
{
    /// <summary>
    ///		Holds the results of a command execution.
    /// </summary>
    public struct CommandResult
    {
        private readonly bool _handled;
        private readonly Exception _exception;

        public CommandResult(bool handled, Exception exception)
        {
            _handled = handled;
            _exception = exception;
        }

        /// <summary>
        ///		Gets whether the command was handled.
        /// </summary>
        public bool Handled => _handled;

        /// <summary>
        ///		Gets the exception that occurred while the command was executing, or null if no exception occurred.
        /// </summary>
        public Exception Exception => _exception;

        public void ThrowIfException()
        {
            if (Exception != null)
                throw Exception;
        }
    }
}