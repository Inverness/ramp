
namespace Ramp.Input
{
    public delegate void CommandHandler(ICommandService source, CommandArguments args);

    public class Command
    {
        public Command(string name, CommandHandler handler, string description = null)
        {
            Name = name;
            Handler = handler;
            Description = description;
        }

        public string Description { get; private set; }

        public CommandHandler Handler { get; private set; }

        public string Name { get; private set; }
    }
}