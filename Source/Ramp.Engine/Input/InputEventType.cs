﻿namespace Ramp.Input
{
    /// <summary>
    ///     Input event types.
    /// </summary>
    public enum InputEventType
    {
        /// <summary>
        ///     Occurs when a mouse button is pressed.
        /// </summary>
        MouseDown,

        /// <summary>
        ///     Occurs when a mouse button is released.
        /// </summary>
        MouseUp,

        /// <summary>
        ///     Occurs when the mouse is moved.
        /// </summary>
        MouseMove,

        /// <summary>
        ///     Occurs when the mouse wheel is moved.
        /// </summary>
        MouseWheel,

        /// <summary>
        ///     Occurs when a key is pressed.
        /// </summary>
        KeyDown,

        /// <summary>
        ///     Occurs when a key is released.
        /// </summary>
        KeyUp,

        /// <summary>
        ///     Occurs when a text key is pressed.
        /// </summary>
        KeyPressed,

        /// <summary>
        ///     Occurs when the game window is activated.
        /// </summary>
        WindowActivated,

        /// <summary>
        ///     Occurs when the game window is deactivated.
        /// </summary>
        WindowDeactivated
    }
}