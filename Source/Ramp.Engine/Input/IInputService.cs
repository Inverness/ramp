﻿namespace Ramp.Input
{
    public interface IInputService
    {
        void AddHandler(IInputHandler handler);

        void RemoveHandler(IInputHandler handler);
    }
}