
namespace Ramp.Input
{
    /// <summary>
    ///     Services providing centralized console and external command execution for debugging, administration, and key
    ///     bindings.
    /// </summary>
    public interface ICommandService
    {
        ///// <summary>
        /////     Raised when a command is being excuted. Handlers should specify that the command was handled in the event
        /////     arguments when necessary. If the command causes an error, an error string should be set in the event
        /////     arguments.
        ///// </summary>
        //event TypedEventHandler<ICommandService, CommandExecutingEventArgs> Executing;

        void AddCommand(string name, CommandHandler handler);

        void AddCommand(Command command);

        bool RemoveCommand(string name);

        bool HasCommand(string name);

        /// <summary>
        ///     Executes a command string.
        /// </summary>
        /// <param name="text"> The command string. </param>
        /// <returns> A command result instance. </returns>
        CommandResult Execute(string text);

        void Print(string format, params object[] args);

        void PrintLine(string text, params object[] args);
    }
}