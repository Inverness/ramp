using System;

namespace Ramp.Input
{
    /// <summary>
    ///     A variable directly accessed using commands.
    /// </summary>
    public abstract class CommandVariable
    {
        protected CommandVariable(string name, Type valueType)
        {
            Validate.ArgumentNotNull(name, "name");
            Validate.ArgumentNotNull(valueType, "valueType");

            Name = name;
            ValueType = valueType;
        }

        /// <summary>
        ///     Raised when the value has changed.
        /// </summary>
        public TypedEventHandler<CommandVariable, EventArgs> ValueChanged;

        /// <summary>
        ///     Gets the variable name.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        ///     Gets the type of the value.
        /// </summary>
        public Type ValueType { get; private set; }

        /// <summary>
        ///     Gets or sets the value.
        /// </summary>
        public abstract object ObjectValue { get; set; }

        /// <summary>
        ///     Invokes the ValueChanged event.
        /// </summary>
        protected virtual void OnValueChanged()
        {
            ValueChanged?.Invoke(this, EventArgs.Empty);
        }
    }

    /// <summary>
    ///     A command variable holding a value of the specified type.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class CommandVariable<T> : CommandVariable
    {
        private T _value;

        public CommandVariable(string name, T value = default(T))
            : base(name, typeof(T))
        {
            _value = value;
        }

        protected CommandVariable(string name, Type valueType)
            : base(name, valueType)
        {
        }

        public override object ObjectValue
        {
            get { return Value; }

            set { Value = (T) value; }
        }

        /// <summary>
        ///     Gets or sets the generic value.
        /// </summary>
        public virtual T Value
        {
            get { return _value; }

            set
            {
                // ReSharper disable once CompareNonConstrainedGenericWithNull
                if (Equals(_value, value))
                    return;

                _value = value;

                OnValueChanged();
            }
        }

        public static implicit operator T(CommandVariable<T> cv)
        {
            Validate.ArgumentNotNull(cv, "cv");
            return cv.Value;
        }
    }
}