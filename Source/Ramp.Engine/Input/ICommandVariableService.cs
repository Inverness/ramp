﻿namespace Ramp.Input
{
    public interface ICommandVariableService
    {
        CommandVariable GetVariable(string name);

        CommandVariable<T> GetVariable<T>(string name);

        void AddVariable(CommandVariable variable);

        CommandVariable<T> AddVariable<T>(string name, T value = default(T));

        bool RemoveVariable(string name);
    }
}