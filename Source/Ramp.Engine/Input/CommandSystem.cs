using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using NLog;
using Microsoft.Xna.Framework;
using static Coroutines.StandardActions;

namespace Ramp.Input
{
    // ReSharper disable ParameterHidesMember
    public sealed class CommandSystem : GameComponent, ICommandService
    {
        private static readonly Logger s_log = LogManager.GetCurrentClassLogger();
        private static readonly Regex s_commandRegex = new Regex(@"""(.*)""|(\S+)");

        private readonly object _instanceLock = new object();
        private readonly Dictionary<string, Command> _commands = new Dictionary<string, Command>();

        public CommandSystem(Game game)
            : base(game)
        {
            game.Services.AddService(typeof(ICommandService), this);
        }

        //public event TypedEventHandler<ICommandService, CommandExecutingEventArgs> Executing;

        public override void Initialize()
        {
        }

        public void AddCommand(string name, CommandHandler handler)
        {
            AddCommand(new Command(name, handler));
        }

        public void AddCommand(Command command)
        {
            lock (_instanceLock)
            {
                _commands[command.Name] = command;
                s_log.Debug("Added command: " + command.Name);
            }
        }

        public bool RemoveCommand(string name)
        {
            lock (_instanceLock)
            {
                if (!_commands.Remove(name))
                    return false;
                s_log.Debug("Removed command: " + name);
                return true;
            }
        }

        public bool HasCommand(string name)
        {
            lock (_instanceLock)
                return _commands.ContainsKey(name);
        }

        public CommandResult Execute(string text)
        {
            Validate.ArgumentNotNull(text, "text");

            string commandName;
            string[] args;

            Parse(text, out commandName, out args);
            Debug.Assert(commandName != null && args != null);

            Command command;
            lock (_instanceLock)
            {
                if (!_commands.TryGetValue(commandName, out command))
                {
                    s_log.Warn("Command does not exist: " + commandName);
                    return new CommandResult(false, null);
                }
            }

            try
            {
                command.Handler(this, new CommandArguments(args));
                return new CommandResult(true, null);
            }
            catch (Exception ex)
            {
                s_log.Error(ex, "Error during command execution");
                return new CommandResult(true, ex);
            }
        }

        public void Print(string format, params object[] args)
        {
            Console.Write(format, args);
        }

        public void PrintLine(string format, params object[] args)
        {
            Console.WriteLine(format, args);
        }

        private static void Parse(string text, out string command, out string[] args)
        {
            MatchCollection matches = s_commandRegex.Matches(text);
            if (matches.Count == 0)
            {
                command = null;
                args = null;
                return;
            }

            command = matches[0].Value;

            var tokens = new string[matches.Count - 1];
            for (int i = 0; i < tokens.Length; i++)
                tokens[i] = matches[i + 1].Value;

            args = tokens;
        }
    }

    // ReSharper restore ParameterHidesMember
}