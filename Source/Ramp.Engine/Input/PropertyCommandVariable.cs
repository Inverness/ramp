﻿using System.Reflection;

namespace Ramp.Input
{
    internal class PropertyCommandVariable<T> : CommandVariable
    {
        private readonly PropertyInfo _property;
        private readonly object _instance;

        public PropertyCommandVariable(string name, PropertyInfo property, object instance = null)
            : base(name, property.PropertyType)
        {
            _property = property;
            _instance = instance;
        }

        public override object ObjectValue
        {
            get { return GenericValue; }

            set { GenericValue = (T) value; }
        }

        public virtual T GenericValue
        {
            get { return (T) _property.GetValue(_instance); }

            set
            {
                var current = (T) _property.GetValue(_instance);

                if (Equals(current, value))
                    return;

                _property.SetValue(_instance, value);

                OnValueChanged();
            }
        }
    }
}