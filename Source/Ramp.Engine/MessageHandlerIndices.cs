﻿namespace Ramp
{
    internal static class MessageHandlerIndices
    {
        public static readonly byte Invalid = 0;
        public static readonly byte ReplicaManager = 2;
        public static readonly byte FileDownloadManager = 3;
        public static readonly byte SessionManager = 4;
    }
}