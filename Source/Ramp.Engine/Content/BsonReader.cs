﻿using Microsoft.Xna.Framework.Content;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;

namespace Ramp.Content
{
    public abstract class BsonReader<T> : ContentTypeReader<T>
    {
        protected JsonSerializer Serializer;

        protected virtual JsonSerializerSettings Settings => new JsonSerializerSettings();

        protected override T Read(ContentReader input, T existingInstance)
        {
            if (Serializer == null)
                Serializer = JsonSerializer.Create(Settings);

            using (var reader = CreateReader(input))
            {
                reader.CloseInput = false;
                return Serializer.Deserialize<T>(reader);
            }
        }

        protected virtual BsonDataReader CreateReader(ContentReader input)
        {
            return new BsonDataReader(input);
        }
    }
}