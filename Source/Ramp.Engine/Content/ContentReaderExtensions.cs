﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace Ramp.Content
{
    public static class ContentReaderExtensions
    {
        public static Rectangle ReadRectangle(this ContentReader reader)
        {
            return new Rectangle(reader.ReadInt32(), reader.ReadInt32(), reader.ReadInt32(), reader.ReadInt32());
        }

        public static Point ReadPoint(this ContentReader reader)
        {
            return new Point(reader.ReadInt32(), reader.ReadInt32());
        }
    }
}