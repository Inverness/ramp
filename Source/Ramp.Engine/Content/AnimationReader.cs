﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using Ramp.Rendering;

namespace Ramp.Content
{
    //internal class AnimationReader : BsonReader<Animation>
    //{
    //    protected override JsonSerializerSettings Settings => new JsonSerializerSettings();
    //}

    internal class AnimationReader : ContentTypeReader<Animation>
    {
        protected override Animation Read(ContentReader input, Animation a)
        {
            if (a == null)
                a = new Animation();

            a.Loop = input.ReadBoolean();
            a.Directional = input.ReadBoolean();
            int spriteTypeCount = input.ReadInt32();
            int frameListCount = input.ReadInt32();

            for (int x = 0; x < spriteTypeCount; x++)
            {
                string name = input.ReadString();

                var st = new AnimationSpriteType
                {
                    TextureName = input.ReadString(),
                    TextureRect = input.ReadRectangle()
                };

                a.SpriteTypes.Add(name, st);
            }

            for (int x = 0; x < frameListCount; x++)
            {
                string name = input.ReadString();
                var frames = new List<AnimationFrame>(input.ReadInt32());

                for (int y = 0; y < frames.Capacity; y++)
                {
                    var frame = new AnimationFrame
                    {
                        Duration = input.ReadDouble(),
                        Sprites = { Capacity = input.ReadInt32() },
                        Events = { Capacity = input.ReadInt32() }
                    };

                    for (int z = 0; z < frame.Sprites.Capacity; z++)
                    {
                        var sprite = new AnimationSprite
                        {
                            SpriteType = input.ReadString(),
                            Position = input.ReadPoint(),
                            Color = input.ReadColor(),
                            Rotation = input.ReadSingle()
                        };

                        frame.Sprites.Add(sprite);
                    }

                    for (int z = 0; z < frame.Events.Capacity; z++)
                        frame.Events.Add(input.ReadString());

                    frames.Add(frame);
                }

                a.Frames.Add(name, frames);
            }

            return a;
        }
    }
}