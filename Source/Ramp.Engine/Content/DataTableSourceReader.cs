﻿using Microsoft.Xna.Framework.Content;
using Ramp.Data;
using System.Collections.Generic;

namespace Ramp.Content
{
    internal class DataTableSourceReader : ContentTypeReader<DataTableSource>
    {
        protected override DataTableSource Read(ContentReader input, DataTableSource t)
        {
            if (t == null)
                t = new DataTableSource();

            int tableCount = input.ReadInt32();

            var strings = new Dictionary<string, string>();

            for (int i = 0; i < tableCount; i++)
            {
                string name = input.ReadString();
                int rowCount = input.ReadInt32();
                int colCount = input.ReadInt32();

                var data = new string[rowCount, colCount];

                for (int r = 0; r < rowCount; r++)
                {
                    for (int c = 0; c < colCount; c++)
                    {
                        string s = input.ReadString();

                        // String deduplication
                        if (strings.TryGetValue(s, out string ns))
                            s = ns;
                        else
                            strings.Add(s, s);

                        data[r, c] = (s.Length == 1 && s[0] == '\0') ? null : s;
                    }
                }

                t.Tables.Add(name, data);
            }

            return t;
        }
    }
}
