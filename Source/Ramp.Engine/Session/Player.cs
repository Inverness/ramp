using System;
using System.ComponentModel;
using Ramp.Network;
using Ramp.Simulation;
using Ramp.Utilities.Collections;

namespace Ramp.Session
{
    /// <summary>
    ///     Represents a player's session in the game. The player can be a local or remote networked player.
    /// </summary>
    public abstract class Player : IDisposable, IPlayer
    {
        private Actor _actor;
        private Level _previousLevel;

        /// <summary>
        ///     Raised before Actor changes.
        /// </summary>
        public event TypedEventHandler<IPlayer, PlayerActorChangingEventArgs> ActorChanging;

        /// <summary>
        ///     Raised when Actor changes.
        /// </summary>
        public event TypedEventHandler<IPlayer, EventArgs> ActorChanged;

        /// <summary>
        ///     Raised after the actor's level changes.
        /// </summary>
        public event TypedEventHandler<IPlayer, PlayerActorLevelChangedEventArgs> ActorLevelChanged;

        /// <summary>
        ///     Arbitrary data attached to the player.
        /// </summary>
        public NameValueDictionary Data { get; } = new NameValueDictionary();

        /// <summary>
        ///     The actor being controlled by this player.
        /// </summary>
        public Actor Actor
        {
            get { return _actor; }

            set
            {
                if (_actor == value)
                    return;

                var eventArgs = new PlayerActorChangingEventArgs(value);
                OnActorChanging(eventArgs);
                if (eventArgs.Cancel)
                    return;

                _actor = value;

                OnActorChanged(EventArgs.Empty);
            }
        }

        public abstract IReplicaConnection ReplicaConnection { get; }

        public virtual void Dispose()
        {
            
        }

        protected virtual void OnActorChanging(PlayerActorChangingEventArgs e)
        {
            if (Actor != null)
            {
                Actor.World.ActorLevelChanged -= OnActorLevelChanged;
                Actor.World.ActorDisposed -= OnActorDisposed;
            }
            ActorChanging?.Invoke(this, e);
        }

        /// <summary>
        ///     Called when Actor changes.
        /// </summary>
        protected virtual void OnActorChanged(EventArgs e)
        {
            if (Actor != null)
            {
                Actor.World.ActorLevelChanged += OnActorLevelChanged;
                Actor.World.ActorDisposed += OnActorDisposed;
                if (_previousLevel == null)
                    _previousLevel = Actor.Level;
            }

            ActorChanged?.Invoke(this, e);
        }

        protected virtual void OnActorLevelChanged(World sender, ActorEventArgs e)
        {
            if (e.Actor == _actor)
            {
                ActorLevelChanged?.Invoke(this, new PlayerActorLevelChangedEventArgs(_previousLevel));

                _previousLevel = Actor.Level;
            }
        }

        protected virtual void OnActorDisposed(World sender, ActorEventArgs e)
        {
            if (e.Actor == _actor)
                Actor = null;
        }
    }

    public class PlayerEventArgs : EventArgs
    {
        public PlayerEventArgs(Player player)
        {
            Player = player;
        }

        public Player Player { get; private set; }
    }
}

namespace Ramp.Simulation
{
    public class PlayerActorChangingEventArgs : CancelEventArgs
    {
        public PlayerActorChangingEventArgs(Actor newActor)
        {
            NewActor = newActor;
        }

        public Actor NewActor { get; private set; }
    }

    public class PlayerActorLevelChangedEventArgs : EventArgs
    {
        public PlayerActorLevelChangedEventArgs(Level previous)
        {
            PreviousLevel = previous;
        }

        public Level PreviousLevel { get; private set; }
    }
}