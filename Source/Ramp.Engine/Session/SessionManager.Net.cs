using System;
using System.Diagnostics;
using System.Linq;
using Lidgren.Network;
using Ramp.Network;
using Ramp.Simulation;

namespace Ramp.Session
{
    public sealed partial class SessionManager
    {
        private const byte JoinMessageId = 2;
        private const byte JoinReplyMessageId = 3;

        private readonly uint _minVersion;
        private readonly uint _version;
        private readonly ulong _magic;
        //private bool _awaitingManifest;
        private uint _clientReplicaId;

        private void Connect(string host, int port, string username = null, string password = null)
        {
            if (_netService.IsServer)
                throw new InvalidOperationException("already a server");
            if (_netService.IsClient)
                throw new InvalidOperationException("already a client");
            if (_worldManagerService.World.Levels.Count != 1)
                throw new InvalidOperationException("can't connect to a server while levels are loaded");

            _netService.SetMessageHandler(MessageHandlerIndices.SessionManager, OnDataReceived);
            _netService.ConnectionStatusChanged += OnConnectionStatusChanged;

            // Build the login message to send along with the connect
            s_log.Debug("Sending login to server: Version={0}, Username={1}, Password={2}", _version, username,
                              password);
            var hail = new NetBuffer();
            hail.Write(_version);
            hail.Write(_magic);
            hail.Write(username);
            hail.Write(password);

            _netService.Connect(host, port, hail);
        }

        private void Listen(int port)
        {
            if (_netService.IsServer)
                throw new InvalidOperationException("already a server");
            if (_netService.IsClient)
                throw new InvalidOperationException("already a client");
            if (_worldManagerService.World.Levels.Count != 1)
                throw new InvalidOperationException("can't start a server while levels are loaded");

            _netService.SetMessageHandler(MessageHandlerIndices.SessionManager, OnDataReceived);
            _netService.ConnectionApproval += OnConnectionApproval;
            _netService.ConnectionStatusChanged += OnConnectionStatusChanged;

            _netService.Listen(port);
        }

        private void OnDataReceived(INetService sender, byte channel, NetIncomingMessage message)
        {
            switch (message.ReadByte())
            {
                case JoinMessageId:
                    OnServerReceiveJoin(message);
                    break;
                case JoinReplyMessageId:
                    OnClientReceiveJoinReply(message);
                    break;
                default:
                    s_log.Error("unhandled session message");
                    break;
            }
        }

        private void OnConnectionApproval(INetService sender, NetConnectionEventArgs e)
        {
            Debug.Assert(_netService.IsServer);

            NetIncomingMessage msg = e.Connection.RemoteHailMessage;
            uint clientVersion = msg.ReadUInt32();
            ulong clientMagic = msg.ReadUInt64();
            string username = msg.ReadString();
            string password = msg.ReadString();

            s_log.Debug("Received login from client: Version={0}, Username={1}, Password={2}", clientVersion,
                              username,
                              password);

            if (clientMagic != _magic)
            {
                e.Connection.Deny("magic number mismatch");
                return;
            }
            if (clientVersion < _minVersion)
            {
                e.Connection.Deny("below minimum server version");
                return;
            }
            if (clientVersion > _version)
            {
                e.Connection.Deny("above server version");
                return;
            }

            // We can approve
            e.Connection.Approve();
        }

        private async void OnConnectionStatusChanged(INetService sender, NetConnectionStatusChangedEventArgs e)
        {
            if (sender.IsClient)
            {
                switch (e.Status)
                {
                    case NetConnectionStatus.Connected:
                        // The server has accepted our login, get the manifest and make sure we have the right files before
                        // attempting to join the game.

                        //_downloadService.ClientRequestManifest();
                        //_awaitingManifest = true;
                        s_log.Info("Connected to server, sending join request...");
                        ClientSendJoin();

                        break;
                    case NetConnectionStatus.Disconnected:
                        await EndGameAsync(); // return to main menu
                        break;
                }
            }
            else if (sender.IsServer)
            {
                switch (e.Status)
                {
                    case NetConnectionStatus.Disconnected:
                        NetPlayer player = _players.OfType<NetPlayer>()
                                                   .FirstOrDefault(p => p.Connection == e.Connection);
                        if (player == null)
                        {
                            s_log.Warn("Client disconnected without matching player found");
                            return;
                        }
                        DestroyPlayer(player);
                        break;
                }
            }
        }

        //// When the file download service receives the manifest sent in the server's join reply
        //private void OnClientReceivedManifest(IFileDownloadService sender, EventArgs e)
        //{
        //    if (!_awaitingManifest)
        //    {
        //        Log.Error("Received server manifest unexpectedly.");
        //        return;
        //    }

        //    _awaitingManifest = false;
        //    Log.Debug("Received server manifest, checking...");

        //    List<string> missing = sender.ServerManifest.Keys.Where(name => !_fileService.HasEntry(name)).ToList();

        //    if (missing.Count == 0)
        //    {
        //        ClientSendJoin();
        //    }
        //    else
        //    {
        //        Log.ErrorFormat("Client is missing {0} files:", missing.Count);
        //        foreach (string name in missing)
        //            Log.ErrorFormat("  {0}", name);
        //    }
        //}

        /// <summary>
        ///		Send a message to the server to join.
        /// </summary>
        private void ClientSendJoin()
        {
            NetOutgoingMessage msg = _netService.CreateMessage(MessageHandlerIndices.SessionManager);
            msg.Write(JoinMessageId);
            _netService.ClientPeer.SendMessage(msg, NetDeliveryMethod.ReliableOrdered);
        }

        private async void OnServerReceiveJoin(NetIncomingMessage msg)
        {
            //if (!_sessions.ContainsKey(msg.SenderConnection))
            //{
            //    Logger.Warning("received join from a connection not logged in");
            //    return;
            //}
            //SessionInfo info = _sessions[msg.SenderConnection];

            NetPlayer player;
            try
            {
                player = await CreateNetPlayer(msg.SenderConnection, null, null); //info.Username, info.Password);
            }
            catch (Exception ex)
            {
                ServerSendJoinReply(msg.SenderConnection, ex.Message);
                return;
            }
            uint replicaId = player.Actor.ReplicaId;
            Debug.Assert(replicaId != 0);
            ServerSendJoinReply(msg.SenderConnection, null, replicaId);
        }

        // Reply to the client's join request. If error message is not null or empty that indicates that the
        // join was denied. Otherwise, the client receives the replica ID of the autonomous actor that
        // belongs to it.
        private void ServerSendJoinReply(NetConnection client, string error = null, uint replicaId = 0)
        {
            NetOutgoingMessage msg = _netService.CreateMessage(MessageHandlerIndices.SessionManager);
            msg.Write(JoinReplyMessageId);
            msg.Write(error);
            msg.Write(replicaId);
            _netService.ServerPeer.SendMessage(msg, client, NetDeliveryMethod.ReliableOrdered);
        }

        private void OnClientReceiveJoinReply(NetIncomingMessage msg)
        {
            string error = msg.ReadString();
            uint replicaId = msg.ReadUInt32();

            if (error.Length > 0)
            {
                s_log.Error("Join failed: {0}", error);
                return;
            }

            s_log.Info("Received login reply: replicaId={0}", replicaId);
            _clientReplicaId = replicaId;
        }

        private void OnReplicaCreated(IReplicaService sender, ReplicaEventArgs e)
        {
            if (_clientReplicaId == 0 || e.Replica.ReplicaId != _clientReplicaId)
                return;

            // We're the client and the server just replicated the actor it created for us to control
            State = SessionState.Loading;

            CreateLocalPlayerClient((Actor) e.Replica);
            _clientReplicaId = 0;

            State = SessionState.Ingame;
            SessionType = SessionType.Client;
        }

        //private class SessionInfo
        //{
        //	public string Username;
        //	public string Password;
        //	public uint Token;
        //}
    }
}