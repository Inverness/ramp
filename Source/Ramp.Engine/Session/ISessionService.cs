using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Ramp.Simulation;

namespace Ramp.Session
{
    /// <summary>
    ///		Describes a session state.
    /// </summary>
    public enum SessionState
    {
        /// <summary>
        ///		Currently idle at the main menu.
        /// </summary>
        Idle,

        /// <summary>
        ///		Connecting to a game server.
        /// </summary>
        Connecting,

        /// <summary>
        ///		Reserved.
        /// </summary>
        Busy,

        /// <summary>
        ///		Loading a level.
        /// </summary>
        Loading,

        /// <summary>
        ///		Loaded and in-game.
        /// </summary>
        Ingame
    }

    /// <summary>
    ///		Describes what type of game session is currently active.
    /// </summary>
    public enum SessionType
    {
        None,
        Singleplayer,
        ListenServer,
        DedicatedServer,
        Client
    }

    /// <summary>
    ///     Manages the overall state of singleplayer and multiplayer games. This service also handles
    ///     input processing for the local player.
    /// </summary>
    public interface ISessionService
    {
        /// <summary>
        ///     Raised when a local or network player is logging in and needs an actor to be spawned for them.
        ///     There must be a handler for this event in order for logins to be allowed.
        /// </summary>
        event TypedEventHandler<ISessionService, PlayerEventArgs> PlayerLogin;

        /// <summary>
        ///     Raised when a player is being removed from the game. This should not destroy the player's actor.
        /// </summary>
        event TypedEventHandler<ISessionService, PlayerEventArgs> PlayerLogout;

        event TypedEventHandler<ISessionService, EventArgs> StateChanging;

        event TypedEventHandler<ISessionService, EventArgs> StateChanged;

        /// <summary>
        ///     Gets the game master component.
        /// </summary>
        GameMaster GameMaster { get; }

        /// <summary>
        ///     Players in the game.
        /// </summary>
        IReadOnlyList<Player> Players { get; }

        /// <summary>
        ///     The local player if one exists.
        /// </summary>
        LocalPlayer LocalPlayer { get; }

        /// <summary>
        ///     The local player's actor if one exists.
        /// </summary>
        Actor LocalPlayerActor { get; }

        /// <summary>
        ///		The current session state.
        /// </summary>
        SessionState State { get; }

        SessionType SessionType { get; }

        /// <summary>
        ///     Begin a single player game.
        /// </summary>
        /// <returns> The local player object. </returns>
        Task BeginSingleplayerGameAsync();

        /// <summary>
        ///     Begin a listen server game.
        /// </summary>
        /// <param name="port"> The port to listen on. </param>
        /// <returns> The local player object. </returns>
        Task BeginListenServerGameAsync(ushort port);

        /// <summary>
        ///     Begin a dedicated server game without a local player.
        /// </summary>
        /// <param name="port"> The port to listen on. </param>
        Task BeginDedicatedServerGameAsync(ushort port);

        /// <summary>
        ///     Begin a client game by connecting to a server.
        /// </summary>
        /// <param name="host"> The server host. </param>
        /// <param name="port"> The server port. </param>
        /// <param name="username"> An optional username. </param>
        /// <param name="password"> An optional password. </param>
        Task BeginClientGameAsync(string host, ushort port, string username = null, string password = null);

        /// <summary>
        ///     End the currently active game.
        /// </summary>
        Task EndGameAsync();

        ///  <summary>
        /// 		Load a game from a save file.
        ///  </summary>
        /// <param name="path"> The path to the save file. </param>
        /// <returns> The local player. </returns>
        Task LoadGameAsync(string path);

        ///  <summary>
        /// 		Save the game state to a file.
        ///  </summary>
        /// <param name="path"> The path to the save file. </param>
        Task SaveGameAsync(string path);
    }
}