using System;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Ramp.Simulation;

namespace Ramp.Session
{
    /// <summary>
    ///     Base class for managing game logic.
    /// </summary>
    public abstract class GameMaster : ActorComponent
    {
        protected ISessionService SessionService;

        protected internal virtual void Initialize(IServiceProvider serviceProvider)
        {
        }

        protected internal abstract Task<Actor> LoginAsync(Player player, string username, string password);

        protected internal virtual void Logout(Player player)
        {
        }

        protected internal virtual void OnGameSaving(JObject data)
        {
        }

        protected internal virtual void OnGameLoading(JObject data)
        {
        }
    }
}