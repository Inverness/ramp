using System.Collections.Generic;
using Lidgren.Network;
using Ramp.Network;

namespace Ramp.Session
{
    public class NetPlayer : Player, IReplicaConnection
    {
        public NetPlayer(NetConnection connection)
        {
            Connection = connection;
            ReplicaInfo = new Dictionary<IReplica, ServerReplicaInfo>();
        }

        public override IReplicaConnection ReplicaConnection => this;

        public NetConnection Connection { get; }

        public IDictionary<IReplica, ServerReplicaInfo> ReplicaInfo { get; }
    }
}