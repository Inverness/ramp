using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Microsoft.Xna.Framework;
using Newtonsoft.Json.Linq;
using Ramp.Data;
using Ramp.Input;
using Ramp.Network;
using Ramp.Rendering;
using Ramp.Simulation;

namespace Ramp.Session
{
    /// <summary>
    ///     A local player on the current machine.
    /// </summary>
    public class LocalPlayer : Player, IInputHandler
    {
        protected readonly IServiceProvider ServiceProvider;
        protected readonly IConfigService ConfigService;
        protected readonly ICommandService CommandService;
        protected readonly IInputService InputService;
        protected readonly IUIService UIService;
        protected readonly IWorldManagerService WorldManagerService;
        protected readonly ILevelRenderer LevelRenderer;
        //protected readonly ContentReferenceClient Content;

        private sbyte _ignoreMovementInput;
        private sbyte _ignoreActionInput;
        private sbyte _ignoreMenuInput;
        private InputMovementBehavior _actorMovement;

        private readonly bool[] _keyStates = new bool[byte.MaxValue];
        private readonly bool[] _movementKeys = new bool[Enum.GetValues(typeof(Direction)).Length];

        private readonly Dictionary<Keys, InputBindingCommands> _keyBindings =
            new Dictionary<Keys, InputBindingCommands>();

        /// <summary>
        ///     Initialize a new local player using the specified service provider.
        /// </summary>
        /// <param name="serviceProvider"> A service provider. </param>
        public LocalPlayer(IServiceProvider serviceProvider)
        {
            if (serviceProvider == null)
                throw new ArgumentNullException(nameof(serviceProvider));
            ServiceProvider = serviceProvider;

            ConfigService = serviceProvider.GetService<IConfigService>();
            CommandService = serviceProvider.GetService<ICommandService>();
            InputService = serviceProvider.GetService<IInputService>();
            UIService = serviceProvider.GetService<IUIService>();
            WorldManagerService = serviceProvider.GetService<IWorldManagerService>();
            LevelRenderer = serviceProvider.GetService<ILevelRenderer>();

            ResetKeyStates();

            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            InputService?.AddHandler(this);

            AddCommands();

            // Load key bindings from config
            LoadBindings();

            // ReSharper restore DoNotCallOverridableMethodsInConstructor

            //// HACK: Load persistent content types so they are not unloaded when traveling between levels
            //if (LevelService != null)
            //	Content = new ContentReferenceClient(LevelService.Content);
        }

        public override IReplicaConnection ReplicaConnection => null;

        /// <summary>
        ///     Check if movement input is enabled.
        /// </summary>
        public bool MovementInputEnabled => _ignoreMovementInput == 0;

        /// <summary>
        ///     Check if action input is enabled.
        /// </summary>
        public bool ActionInputEnabled => _ignoreActionInput == 0;

        /// <summary>
        ///     Check if menu input is enabled.
        /// </summary>
        public bool MenuInputEnabled => _ignoreMenuInput == 0;

        public Color? ScreenColor
        {
            get { return LevelRenderer?.ScreenColor; }

            set
            {
                if (LevelRenderer != null)
                    LevelRenderer.ScreenColor = value;
            }
        }

        /// <summary>
        ///     Ignore movement input.
        /// </summary>
        /// <remarks>
        ///     Multiple ignore calls can be nested, and movement will not be reenabled until
        ///     all ignores have been undone.
        /// </remarks>
        /// <param name="ignore"> Whether to ignore or disable ignoring. </param>
        public void IgnoreMovementInput(bool ignore)
        {
            if (ignore)
            {
                if (_ignoreMovementInput == sbyte.MaxValue)
                    throw new InvalidOperationException("max ignores");
                _ignoreMovementInput++;
            }
            else if (_ignoreMovementInput > 0)
            {
                _ignoreMovementInput--;
            }
            UpdateInputVector();
        }

        public void IgnoreActionInput(bool ignore)
        {
            if (ignore)
            {
                if (_ignoreActionInput == sbyte.MaxValue)
                    throw new InvalidOperationException("max ignores");
                _ignoreActionInput++;
            }
            else if (_ignoreActionInput > 0)
            {
                _ignoreActionInput--;
            }
        }

        public void IgnoreMenuInput(bool ignore)
        {
            if (ignore)
            {
                if (_ignoreMenuInput == sbyte.MaxValue)
                    throw new InvalidOperationException("max ignores");
                _ignoreMenuInput++;
            }
            else if (_ignoreMenuInput > 0)
            {
                _ignoreMenuInput--;
            }
        }

        public void OnInputEvent(InputEventType type, EventArgs e)
        {
            KeyEventArgs ke;
            switch (type)
            {
                case InputEventType.MouseDown:
                case InputEventType.MouseUp:
                case InputEventType.MouseMove:
                case InputEventType.MouseWheel:
                    if (MenuInputEnabled)
                        UIService?.OnInputEvent(type, e);
                    break;
                case InputEventType.KeyDown:
                    ke = (KeyEventArgs) e;

                    if (!ke.Handled && MenuInputEnabled)
                        UIService?.OnInputEvent(InputEventType.KeyDown, ke);

                    if (!ke.Handled)
                        ExecuteKeyBinding(type, ke.KeyCode);

                    break;
                case InputEventType.KeyUp:
                    ke = (KeyEventArgs) e;

                    if (!ke.Handled && MenuInputEnabled)
                        UIService?.OnInputEvent(InputEventType.KeyUp, ke);

                    if (!ke.Handled)
                        ExecuteKeyBinding(type, ke.KeyCode);

                    break;
                case InputEventType.WindowDeactivated:
                    ResetKeyStates();
                    UpdateInputVector();
                    break;
            }
        }

        public void PauseGame(bool pause, bool noTransition = false)
        {
            WorldManagerService.Enabled = !pause;
        }

        public void SetKeyBinding(Keys key, string down, string up)
        {
            _keyBindings[key] = new InputBindingCommands
            {
                Down = down,
                Up = up
            };
        }

        public bool ClearKeyBinding(Keys key)
        {
            return _keyBindings.Remove(key);
        }

        protected override void OnActorChanged(EventArgs e)
        {
            base.OnActorChanged(e);
            _actorMovement = Actor?.GetComponent<InputMovementBehavior>();
        }

        protected virtual void LoadBindings()
        {
            var bindings = (JObject) ConfigService.Database.GetToken("User.Bindings");

            if (bindings != null)
            {
                foreach (KeyValuePair<string, JToken> b in bindings)
                {
                    var key = (Keys) Enum.Parse(typeof(Keys), b.Key);

                    var downCommand = b.Value.Value<string>("Down");
                    var upCommand = b.Value.Value<string>("Up");

                    SetKeyBinding(key, downCommand, upCommand);
                }
            }
        }

        public override void Dispose()
        {
            InputService?.RemoveHandler(this);
            RemoveCommands();
            //Content.Dispose();

            base.Dispose();
        }

        protected virtual void AddCommands()
        {
            if (CommandService == null)
                return;
            CommandService.AddCommand("Player.Move", OnCommandMove);
            CommandService.AddCommand("Player.PauseGame", OnCommandPauseGame);
        }

        protected virtual void RemoveCommands()
        {
            if (CommandService == null)
                return;
            CommandService.RemoveCommand("Player.Move");
            CommandService.RemoveCommand("Player.PauseGame");
        }

        /// <summary>
        ///     Resets monitored key states to unpressed.
        /// </summary>
        protected void ResetKeyStates()
        {
            _keyStates.SetRange(false);
        }

        /// <summary>
        ///		Execute commands bound to the specified key.
        /// </summary>
        /// <param name="type"> The type of input used to determine which command to execute. </param>
        /// <param name="key"> The related key. </param>
        protected void ExecuteKeyBinding(InputEventType type, Keys key)
        {
            InputBindingCommands commands;
            switch (type)
            {
                case InputEventType.KeyDown:
                    bool wasDown = _keyStates[(byte) key];

                    _keyStates[(byte) key] = true;

                    // wasDown protects against key repeating
                    if (!wasDown && _keyBindings.TryGetValue(key, out commands) && commands.Down != null)
                        CommandService.Execute(commands.Down);

                    break;
                case InputEventType.KeyUp:
                    _keyStates[(byte) key] = false;

                    if (_keyBindings.TryGetValue(key, out commands) && commands.Up != null)
                        CommandService.Execute(commands.Up);

                    break;
            }
        }

        protected void OnCommandMove(ICommandService source, CommandArguments args)
        {
            if (!MovementInputEnabled)
                return;

            var magnitude = args.GetValue<float>(0);
            var direction = args.GetValue<Direction>(1);

            switch (direction)
            {
                case Direction.Up:
                case Direction.Left:
                case Direction.Down:
                case Direction.Right:
                    _movementKeys[(byte) direction] = magnitude > MovementUtility.SmallNumber;
                    break;
                default:
                    throw new ArgumentException("direction out of range");
            }

            UpdateInputVector();
        }

        protected void OnCommandPauseGame(ICommandService source, CommandArguments args)
        {
            PauseGame(args.GetValue<bool>(0), args.GetValueOrDefault(1, false));
        }

        private void UpdateInputVector()
        {
            Direction direction;
            if (MovementInputEnabled)
            {
                bool up = _movementKeys[(byte) Direction.Up];
                bool down = _movementKeys[(byte) Direction.Down];
                bool left = _movementKeys[(byte) Direction.Left];
                bool right = _movementKeys[(byte) Direction.Right];

                if ((left && right) || (up && down))
                    direction = Direction.None;
                else if (up && left)
                    direction = Direction.UpLeft;
                else if (down && left)
                    direction = Direction.DownLeft;
                else if (down && right)
                    direction = Direction.DownRight;
                else if (up && right)
                    direction = Direction.UpRight;
                else if (up)
                    direction = Direction.Up;
                else if (left)
                    direction = Direction.Left;
                else if (down)
                    direction = Direction.Down;
                else if (right)
                    direction = Direction.Right;
                else
                    direction = Direction.None;
            }
            else
            {
                direction = Direction.None;
            }

            if (_actorMovement != null)
                _actorMovement.InputDirection = direction;
        }

        private struct InputBindingCommands
        {
            public string Down;

            public string Up;
        }
    }
}