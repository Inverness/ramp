using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using NLog;
using Lidgren.Network;
using Microsoft.Xna.Framework;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Ramp.Data;
using Ramp.Input;
using Ramp.Network;
using Ramp.Simulation;

namespace Ramp.Session
{
    // Startup procedure:
    // 
    // Game sessions should have systems started in a specific order and stopped in the reverse order.
    // 1. Create GameMaster
    // 2. If multiplayer, set net service to connect or listen
    // 3. Preload any levels or assets are necessary
    // 4. Create the player and its actors

    /// <summary>
    ///     This class is the authority over the game and controls players for both single and multiplayer.
    ///     Requests to join a network game come here, and the session manager communicates with other services
    ///     to get players in the game.
    /// </summary>
    public sealed partial class SessionManager : GameComponent, ISessionService
    {
        private static readonly Logger s_log = LogManager.GetCurrentClassLogger();

        private readonly IDispatcherService _dispatcherService;
        private readonly ICommandService _commandService;
        private readonly IConfigService _config;
        private readonly IFileService _fileService;
        private readonly INetService _netService;
        private readonly IReplicaService _replicaService;
        private readonly IWorldManagerService _worldManagerService;
        //private readonly IFileDownloadService _downloadService;
        private readonly List<Player> _players = new List<Player>();
        private SessionState _state;
        private World _world;
        private string _gameMasterTypeName;
        private GameMaster _gameMaster;
        private string _localPlayerTypeName;
        private LocalPlayer _localPlayer;

        // temp
        private List<Tuple<Player, Actor>> _pendingPawnHookups;

        /// <summary>
        ///     Initializes a new instance of the SessionManager class.
        /// </summary>
        /// <param name="game"> A game instance. </param>
        /// <param name="version"> A version number used for networking. </param>
        /// <param name="minVersion"> A minimum required version for clients to connect to a server. </param>
        /// <param name="magic"> A magic number used to verify a connection. </param>
        public SessionManager(Game game, uint version, uint minVersion, ulong magic)
            : base(game)
        {
            _dispatcherService = game.Services.GetRequiredService<IDispatcherService>();
            _commandService = game.Services.GetRequiredService<ICommandService>();
            _config = game.Services.GetRequiredService<IConfigService>();
            _fileService = game.Services.GetRequiredService<IFileService>();
            _netService = game.Services.GetRequiredService<INetService>();
            _replicaService = game.Services.GetRequiredService<IReplicaService>();
            _worldManagerService = game.Services.GetRequiredService<IWorldManagerService>();
            //_downloadService = game.Services.GetService<IFileDownloadService>(true);
            //_gameSaveService = game.Services.GetService<IGameSaveService>(true);

            _version = version;
            _minVersion = minVersion;
            _magic = magic;

            UnloadLevelsWithoutPlayers = true;

            game.Services.AddService(typeof(ISessionService), this);
        }

        /// <inheritdoc />
        public event TypedEventHandler<ISessionService, PlayerEventArgs> PlayerLogin;

        /// <inheritdoc />
        public event TypedEventHandler<ISessionService, PlayerEventArgs> PlayerLogout;

        public event TypedEventHandler<ISessionService, EventArgs> StateChanging;

        public event TypedEventHandler<ISessionService, EventArgs> StateChanged;

        public bool UnloadLevelsWithoutPlayers { get; set; }

        /// <inheritdoc />
        public IReadOnlyList<Player> Players => _players;

        /// <inheritdoc />
        public LocalPlayer LocalPlayer => _localPlayer;

        /// <inheritdoc />
        public Actor LocalPlayerActor => _localPlayer?.Actor;

        public GameMaster GameMaster => _gameMaster;

        /// <inheritdoc />
        public SessionState State
        {
            get { return _state; }

            private set
            {
                if (_state == value)
                    return;
                StateChanging?.Invoke(this, EventArgs.Empty);
                _state = value;
                StateChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        /// <summary>
        ///     Gets an enumeration describing the current session type.
        /// </summary>
        public SessionType SessionType { get; private set; }

        /// <summary>
        ///     Gets or sets a boolean indicating that a full GC is requested at the end of the next loop. This is
        ///     normally set after level transitions.
        /// </summary>
        public bool RequestingFullGC { get; set; }

        /// <inheritdoc />
        public override void Initialize()
        {
            _commandService.AddCommand("BeginSingleplayerGame",
                                       async (source, args) => await BeginSingleplayerGameAsync());
            _commandService.AddCommand("EndGame", async (source, args) => await EndGameAsync());

            _world = _worldManagerService.World;

            _gameMasterTypeName = _config.Database.GetString("System.Session.GameMasterType");

            _localPlayerTypeName = _config.Database.GetString("System.Session.LocalPlayerType") ??
                                   typeof(LocalPlayer).AssemblyQualifiedName;

            _replicaService.ReplicaCreated += OnReplicaCreated;
            //_downloadService.ManifestReceived += OnClientReceivedManifest;
        }

        /// <inheritdoc />
        public override void Update(GameTime gameTime)
        {
            //if (gameTime.TotalGameTime >= TimeSpan.FromSeconds(5) && !_saved)
            //{
            //    _saved = true;
            //    //SaveGame("Save1.rsav");
            //}

            if (_pendingPawnHookups != null)
            {
                for (int i = _pendingPawnHookups.Count - 1; i > -1; i--)
                {
                    Player player = _pendingPawnHookups[i].Item1;
                    Actor actor = _pendingPawnHookups[i].Item2;

                    var pawn = actor.GetComponent<PlayerPawnBehavior>();
                    if (pawn == null)
                        continue;

                    player.Actor = actor;
                    pawn.Player = player;

                    _pendingPawnHookups.RemoveAt(i);
                }
            }
        }

        /// <inheritdoc />
        public async Task BeginSingleplayerGameAsync()
        {
            await EndGameAsync();

            s_log.Info("Beginning single player game.");

            State = SessionState.Loading;
            SpawnGameMaster();

            await CreateLocalPlayerAsync();

            State = SessionState.Ingame;
            SessionType = SessionType.Singleplayer;

            RequestingFullGC = true;
        }

        /// <inheritdoc />
        public async Task BeginListenServerGameAsync(ushort port)
        {
            await EndGameAsync();
            s_log.Info("Beginning listen server game on port {0}", port);

            State = SessionState.Loading;
            SpawnGameMaster();
            Listen(port);

            await CreateLocalPlayerAsync();

            State = SessionState.Ingame;
            SessionType = SessionType.ListenServer;

            RequestingFullGC = true;
        }

        /// <inheritdoc />
        public async Task BeginDedicatedServerGameAsync(ushort port)
        {
            await EndGameAsync();

            s_log.Info("Beginning dedicated server game on port {0}", port);

            State = SessionState.Loading;
            SpawnGameMaster();

            Listen(port);

            State = SessionState.Ingame;
            SessionType = SessionType.DedicatedServer;

            RequestingFullGC = true;
        }

        /// <inheritdoc />
        public Task BeginClientGameAsync(string host, ushort port, string username = null, string password = null)
        {
            // TODO: Make this actually asynchronous
            s_log.Info("Beginning client game with host {0}:{1}", host, port);

            Connect(host, port);

            RequestingFullGC = true;

            return Task.FromResult(true);
            // session type set when connection finishes
        }

        /// <inheritdoc />
        public Task EndGameAsync()
        {
            if (State == SessionState.Idle)
                return Task.FromResult(true);

            // Copy the player list then remove all of them from the game
            new List<Player>(_players).ForEach(p => DestroyPlayer(p));
            Debug.Assert(_players.Count == 0);

            _world.DisposeLevels(false);

            DestroyGameMaster();
            Debug.Assert(_world.PersistentLevel.ActorCount == 0);

            if (_netService.Peer != null)
                _netService.Shutdown();

            State = SessionState.Idle;
            SessionType = SessionType.None;

            RequestingFullGC = true;

            return Task.FromResult(true);
        }

        public async Task LoadGameAsync(string path)
        {
            await EndGameAsync();
            s_log.Info("Beginning single player game from save file.");
            State = SessionState.Loading;

            path = Path.Combine(_config.UserDirectory, "Saves", path);

            Func<JObject> loadObjectFunc = () =>
            {
                using (var reader = new StreamReader(path, Encoding.UTF8, true))
                using (var jreader = new JsonTextReader(reader))
                {
                    return JObject.Load(jreader);
                }
            };

            JObject data = await _dispatcherService.WorkDispatcher.InvokeAsync(loadObjectFunc);
            SpawnGameMaster();
            await CreateLocalPlayerAsync();
            _gameMaster.OnGameLoading(data);

            State = SessionState.Ingame;

            RequestingFullGC = true;
        }

        public Task SaveGameAsync(string path)
        {
            if (string.IsNullOrEmpty(path))
                throw new ArgumentException("Must specify a save file path.", nameof(path));
            if (_gameMaster == null)
                throw new InvalidOperationException();

            if (_world.IsUpdatingLevels)
                throw new InvalidOperationException("Cannot save game during level update.");

            // Get the save data first before we do any IO
            var data = new JObject();
            _gameMaster.OnGameSaving(data);

            Action saveObjectAction = () =>
            {
                path = Path.Combine(_config.UserDirectory, "Saves", path);

                string dir = Path.GetDirectoryName(path) ?? "";
                if (!Directory.Exists(dir))
                    Directory.CreateDirectory(dir);

                using (var writer = new StreamWriter(path, false, Encoding.UTF8))
                using (var jwriter = new JsonTextWriter(writer))
                {
                    data.WriteTo(jwriter);
                    jwriter.Flush();
                }
            };

            return _dispatcherService.WorkDispatcher.InvokeAsync(saveObjectAction);
        }

        private void SpawnGameMaster()
        {
            if (_gameMaster != null)
                return;
            Type type = Type.GetType(_gameMasterTypeName, true);

            var actor = _world.PersistentLevel.SpawnActor(name: "GameMasterActor");
            _gameMaster = (GameMaster) actor.AddComponent(type, "GameMaster");
            _gameMaster.Initialize(Game.Services);
            _world.GameMaster = _gameMaster;

            s_log.Debug("Spawned game master: " + type.FullName);
        }

        private void DestroyGameMaster()
        {
            if (_gameMaster == null)
                return;
            _gameMaster.Actor.Dispose();
            _world.GameMaster = null;
            _gameMaster = null;
            s_log.Debug("Destroyed game master.");
        }

        /// <summary>
        ///     Create a local player an add it to the current game.
        /// </summary>
        /// <returns> A LocalPlayer object containing the local player's state. </returns>
        private async Task CreateLocalPlayerAsync()
        {
            Debug.Assert(_localPlayer == null && !_netService.IsClient);

            Type localPlayerType = Type.GetType(_localPlayerTypeName, true);
            var localPlayer = (LocalPlayer) Activator.CreateInstance(localPlayerType, Game.Services);

            NetRole remoteRole = _netService.IsServer ? NetRole.Simulated : NetRole.None;
            Actor actor = await AddPlayerAsync(localPlayer, remoteRole);
            if (actor == null)
                return;

            _localPlayer = localPlayer;
            _localPlayer.ActorLevelChanged += OnPlayerActorLevelChanged;
        }

        // Entry point for a client that has just connected to a server and received its actor
        // This is called from OnReplicaCreated() once the replica manager notifies us that we've received
        // the client's actor
        private LocalPlayer CreateLocalPlayerClient(Actor actor)
        {
            Debug.Assert(_localPlayer == null && _netService.IsClient);

            var localPlayer = new LocalPlayer(Game.Services);
            AddClientPlayer(localPlayer, actor);
            _localPlayer = localPlayer;
            return _localPlayer;
        }

        // Entry point for starting a networked player's game on the server
        private async Task<NetPlayer> CreateNetPlayer(NetConnection connection, string username = null,
                                                      string password = null)
        {
            Debug.Assert(_netService.IsServer);

            // Add a remote player to this game
            var netPlayer = new NetPlayer(connection);
            Actor actor = await AddPlayerAsync(netPlayer, NetRole.Autonomous, username, password);
            if (actor == null)
                return null;

            // Notify the replica manager that this connection exists
            _replicaService.AddConnection(netPlayer);

            return netPlayer;
        }

        // Remove a network or local player from the game
        private bool DestroyPlayer(Player player)
        {
            int index = _players.IndexOf(player);
            if (index == -1)
                return false;

            if (player == _localPlayer)
                player.ActorLevelChanged -= OnPlayerActorLevelChanged;

            PlayerLogout?.Invoke(this, new PlayerEventArgs(player));

            Debug.Assert(player.Actor != null && !player.Actor.IsDisposed);
            Actor actor = player.Actor;
            var pawn = actor.GetComponent<PlayerPawnBehavior>();
            player.Actor = null;
            pawn.Player = null;

            actor.MarkForDispose();

            var netPlayer = player as NetPlayer;
            if (netPlayer != null)
                _replicaService.RemoveConnection(netPlayer);

            player.Dispose();

            _players.RemoveAt(index);

            return true;
        }

        /// <summary>
        ///     Add a player to the game on a server or standalone game
        /// </summary>
        /// <param name="player"> A new player object. </param>
        /// <param name="remoteRole"> The initial remote role of the player's actor. </param>
        /// <param name="username"> Optional username for login. </param>
        /// <param name="password"> Optional password for login. </param>
        /// <returns> A new Actor owned by the player, or null if there was an error spawning the actor. </returns>
        private async Task<Actor> AddPlayerAsync(Player player, NetRole remoteRole, string username = null,
                                                 string password = null)
        {
            Debug.Assert(player.Actor == null);
            Debug.Assert(remoteRole != NetRole.Authority);

            Actor actor = await _gameMaster.LoginAsync(player, username, password);

            var pawn = actor.GetComponent<PlayerPawnBehavior>() ?? actor.AddComponent<PlayerPawnBehavior>();

            // Hook things up now that we have an actor
            player.Actor = actor;
            pawn.Player = player;
            _players.Add(player);
            actor.SetRoles(NetRole.Authority, remoteRole);

            PlayerLogin?.Invoke(this, new PlayerEventArgs(player));

            return actor;
        }

        private void AddClientPlayer(LocalPlayer player, Actor actor)
        {
            Debug.Assert(player.Actor == null && actor.ComponentCount == 0);

            if (_pendingPawnHookups == null)
                _pendingPawnHookups = new List<Tuple<Player, Actor>>();

            _pendingPawnHookups.Add(Tuple.Create((Player) player, actor));
            _players.Add(player);

            Debug.Assert(actor.Role == NetRole.Autonomous);
            //Debug.Assert(player.Actor == null);

            //var pawn = actor.GetComponent<PawnBehavior>() ?? actor.AddComponent<PawnBehavior>();

            //player.Actor = actor;
            //pawn.Player = player;
            //_players.Add(player);
            //Debug.Assert(actor.Role == NetRole.Autonomous);
        }

        private void OnPlayerActorLevelChanged(IPlayer sender, PlayerActorLevelChangedEventArgs e)
        {
            if (sender == _localPlayer)
            {
                // Only for single player mode at the moment
                if (UnloadLevelsWithoutPlayers && !_netService.IsServer)
                {
                    e.PreviousLevel.Dispose();
                }

                RequestingFullGC = true;
            }
        }
    }
}