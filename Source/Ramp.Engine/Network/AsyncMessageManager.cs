﻿namespace Ramp.Network
{
    //public class AsyncMessageManager : IGameSystem, IAsyncMessageService
    //{
    //    private const byte ServiceChannel = 4;
    //    private const byte SequenceChannel = 3;
    //    private const byte DataMid = 0;
    //    private const byte ErrorMid = 1;
    //    private static readonly ILog Log = LogManager.GetCurrentClassLogger();

    //    private readonly IConfigService _config;
    //    private readonly INetService _netService;
    //    private readonly LinkedArrayList<ReceiveTask> _tasks = new LinkedArrayList<ReceiveTask>();

    //    public AsyncMessageManager(IServiceRegistry services)
    //    {
    //        _config = services.GetService<IConfigService>(true);
    //        _netService = services.GetService<INetService>(true);
    //    }

    //    public string Name
    //    {
    //        get { return "AsyncMessageManager"; }
    //        set { throw new NotImplementedException(); }
    //    }

    //    public void Initialize()
    //    {
    //        _netService.AddDataHandler(ServiceChannel, OnDataReceived);
    //        _netService.ConnectionStatusChanged += OnStatusChanged;
    //    }

    //    public bool IsChannelUsed(ushort channel)
    //    {
    //        return _tasks.IsAllocated(channel);
    //    }


    //    public NetOutgoingMessage CreateSendMessage(ushort channel)
    //    {
    //        if (_tasks.IsAllocated(channel))
    //            throw new ArgumentOutOfRangeException("channel", "Channel is already in use");
    //        if (_tasks.Count >= ushort.MaxValue)
    //            throw new InvalidOperationException("All channels in use");

    //        NetOutgoingMessage message = _netService.Peer.CreateMessage();
    //        message.Write(ServiceChannel);
    //        message.Write(channel);
    //        message.Write(DataMid);
    //        return message;
    //    }

    //    public Task<NetIncomingMessage> Send(NetOutgoingMessage message, NetConnection recipient,
    //                                         NetDeliveryMethod method)
    //    {
    //        if (message == null)
    //            throw new ArgumentNullException("message");
    //        if (recipient == null)
    //            throw new ArgumentNullException("recipient");

    //        switch (method)
    //        {
    //            case NetDeliveryMethod.ReliableUnordered:
    //            case NetDeliveryMethod.ReliableSequenced:
    //            case NetDeliveryMethod.ReliableOrdered:
    //                break;
    //            default:
    //                throw new ArgumentOutOfRangeException("method",
    //                                                      "Invalid delivery method, must be a reliable method.");
    //        }

    //        // Retrieve the async channel
    //        long pos = message.Position;
    //        message.Position = 8; // skip service channel
    //        ushort channel = message.PeekUInt16();
    //        message.Position = pos;

    //        if (_tasks.IsAllocated(channel))
    //            throw new InvalidOperationException("Existing send message had invalid channel");

    //        _netService.Peer.SendMessage(message, recipient, method, SequenceChannel);

    //        return Receive(channel);
    //    }

    //    public Task<NetIncomingMessage> Receive(ushort channel)
    //    {
    //        // TODO: timeout
    //        if (_tasks.IsAllocated(channel))
    //            throw new ArgumentOutOfRangeException("channel", "Channel is already in use");
    //        if (_tasks.Count >= ushort.MaxValue)
    //            throw new InvalidOperationException("All channels in use");

    //        var task = new ReceiveTask
    //        {
    //            Tcs = new TaskCompletionSource<NetIncomingMessage>(),
    //            Sending = false
    //        };

    //        task.Channel = (ushort) _tasks.AddLast(task);

    //        return task.Tcs.Task;
    //    }

    //    public void CancelReceive(ushort channel)
    //    {
    //        ReceiveTask task;
    //        if (!_tasks.Dictionary.TryGetValue(channel, out task))
    //            throw new ArgumentOutOfRangeException("channel", "invalid task channel");

    //        _tasks.RemoveAt(task.Channel);

    //        task.Tcs.SetCanceled();
    //    }

    //    protected void OnStatusChanged(INetService service, NetConnectionStatusChangedEventArgs e)
    //    {
    //        if (e.Status != NetConnectionStatus.Disconnected && e.Status != NetConnectionStatus.Disconnecting)
    //            return;

    //        _tasks.Dictionary.Values.Select(m => m.Channel).ForEach(CancelReceive);
    //    }

    //    protected void OnDataReceived(INetService service, byte nchannel, NetIncomingMessage message)
    //    {
    //        ushort channel = message.ReadUInt16();
    //        byte mid = message.ReadByte();

    //        ReceiveTask task;
    //        _tasks.Dictionary.TryGetValue(channel, out task);

    //        switch (mid)
    //        {
    //            case DataMid:
    //                if (task == null)
    //                {
    //                    Log.ErrorFormat("Received async message for unused channel: " + channel);

    //                    NetOutgoingMessage replyMessage = _netService.Peer.CreateMessage();
    //                    replyMessage.Write(ServiceChannel);
    //                    replyMessage.Write(channel);
    //                    replyMessage.Write(ErrorMid);
    //                    replyMessage.Write("Unused channel");
    //                    _netService.Peer.SendMessage(replyMessage, message.SenderConnection,
    //                                                 NetDeliveryMethod.ReliableUnordered, SequenceChannel);
    //                }
    //                else
    //                {
    //                    Log.InfoFormat("Received async message on channel: " + channel);

    //                    Debug.Assert(!task.Sending);
    //                    _tasks.RemoveAt(task.Channel);
    //                    task.Tcs.SetResult(message);
    //                }
    //                break;
    //            case ErrorMid:
    //                string errorMessage = message.ReadString();
    //                Log.ErrorFormat("Received async error message on channel {0}: {1}", channel, errorMessage);
    //                break;
    //        }
    //    }

    //    private class ReceiveTask
    //    {
    //        public ushort Channel;

    //        public TaskCompletionSource<NetIncomingMessage> Tcs;

    //        public bool Sending;
    //    }
    //}
}