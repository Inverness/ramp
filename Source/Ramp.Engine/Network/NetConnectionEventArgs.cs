﻿using System;
using Lidgren.Network;

namespace Ramp.Network
{
    public class NetConnectionEventArgs : EventArgs
    {
        public NetConnectionEventArgs(NetConnection connection)
        {
            if (connection == null)
                throw new ArgumentNullException(nameof(connection));
            Connection = connection;
        }

        public NetConnection Connection { get; private set; }
    }
}