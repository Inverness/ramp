namespace Ramp.Network
{
    /// <summary>
    ///     Describes the role of a replica.
    /// </summary>
    public enum NetRole : byte
    {
        /// <summary>
        ///     No role assigned.
        /// </summary>
        None,

        /// <summary>
        ///     Simualted with with no authority in operations.
        /// </summary>
        Simulated,

        /// <summary>
        ///     Autonomous for a limited amount of operations but still verified by the authority.
        /// </summary>
        Autonomous,

        /// <summary>
        ///     Authoritative for all operations.
        /// </summary>
        Authority
    }
}