﻿using System;

namespace Ramp.Network
{
    public class ReplicaEventArgs : EventArgs
    {
        public ReplicaEventArgs(IReplica replica)
        {
            Replica = replica;
        }

        public IReplica Replica { get; private set; }
    }
}