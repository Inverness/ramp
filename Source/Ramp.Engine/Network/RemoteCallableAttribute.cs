using System;

namespace Ramp.Network
{
    /// <summary>
    ///     Describes replica methods that can be called from the client to the server using RPC.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class RemoteCallableAttribute : Attribute
    {
    }
}