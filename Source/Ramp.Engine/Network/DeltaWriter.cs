﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using NLog;
using Lidgren.Network;
using Microsoft.Xna.Framework;

namespace Ramp.Network
{
    /// <summary>
    ///		Manages writing changes in data to a NetBuffer.
    /// </summary>
    public class DeltaWriter
    {
        private const bool UseLog = false;

        private const ushort Terminator = ushort.MaxValue; // marks the end of the data
        private static readonly Logger s_log = LogManager.GetCurrentClassLogger();
        private NetBuffer _buffer; // buffer being written to or read from
        private List<ValueEntry> _values; // previously serialized values
        private ushort _index; // current read or write index
        private ushort _writeCount;
        private uint _largestMessageSize; // largest previously serialized message size
        private int _originalLengthBits; // length of buffer in bits when writing started

        /// <summary>
        ///     Gets the buffer being written to.
        /// </summary>
        /// <remarks>
        ///     Data can be written directly to this buffer but it should only be done if a previously written
        ///     variable has changed.
        /// </remarks>
        public NetBuffer Buffer => _buffer;

        /// <summary>
        ///     Gets the size of the largest previously serialized message.
        /// </summary>
        /// <remarks>
        ///     This is used as a hint in order to determine an initial capacity for new messages.
        /// </remarks>
        public uint LargestMessageSize => _largestMessageSize;

        /// <summary>
        ///     Begin a serialization to the specified message.
        /// </summary>
        /// <param name="buffer"> A buffer to write to. </param>
        public void BeginWrite(NetBuffer buffer)
        {
            if (_buffer != null)
                throw new InvalidOperationException("Already writing");
            if (_values == null)
                _values = new List<ValueEntry>();

            _buffer = buffer;
            _index = 0;
            _writeCount = 0;
            _originalLengthBits = buffer.LengthBits;
        }

        /// <summary>
        ///     End a serialization and retrieve the outgoing message.
        /// </summary>
        /// <returns> The number of items written. </returns>
        public ushort EndWrite()
        {
            if (_buffer == null)
                throw new InvalidOperationException("Not currently writing");

            bool wrote = _writeCount != 0;
            // This is a sanity check since extra data in a buffer is allowed but only when a variable has
            // been changed and thus written to the buffer.
            if (wrote != (_buffer.LengthBits > _originalLengthBits))
                throw new InvalidOperationException("Invalid data was written to buffer");

            _buffer.Write(Terminator);

            if (UseLog && wrote)
            {
                s_log.Debug("Wrote delta: Index={0}, Changes={1}, Bits={2}",
                                  _index - 1, _writeCount, _buffer.LengthBits - _originalLengthBits);
            }

            _largestMessageSize = Math.Max(_largestMessageSize, (uint) _buffer.LengthBytes);
            _buffer = null;

            return _writeCount;
        }

        /// <summary>
        ///     Get the most recently written value at the current index.
        /// </summary>
        /// <param name="value">Will be set to the most recently written value if one was found.</param>
        /// <param name="indexDelta">An optional delta applied to the current index.</param>
        /// <returns>True if a recent value was found.</returns>
        public bool GetRecentValue<T>(ref T value, int indexDelta = 0)
        {
            if (_buffer == null)
                throw new InvalidOperationException("Not currently writing");
            int index = _index + indexDelta;
            if (index >= _values.Count || index < 0)
                return false;
            value = ((ValueEntry<T>) _values[index]).Value;
            return true;
        }

        public T GetRecentValue<T>(int indexDelta = 0)
        {
            T result = default(T);
            GetRecentValue(ref result, indexDelta);
            return result;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Skip(ushort count)
        {
            // no mode check for inlining
            _index += count;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(bool value)
        {
            if (WriteIndex(value))
                _buffer.Write(value);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool WriteCheck(bool value)
        {
            if (!WriteIndex(value))
                return false;
            _buffer.Write(value);
            return true;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(byte value)
        {
            if (WriteIndex(value))
                _buffer.Write(value);
        }

        public void Write(byte? value)
        {
            if (WriteIndex(value) && WriteHasValue(value))
                _buffer.Write(value.Value);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool WriteCheck(byte value)
        {
            if (!WriteIndex(value))
                return false;
            _buffer.Write(value);
            return true;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(sbyte value)
        {
            if (WriteIndex(value))
                _buffer.Write(value);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(short value)
        {
            if (WriteIndex(value))
                _buffer.Write(value);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(ushort value)
        {
            if (WriteIndex(value))
                _buffer.Write(value);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(int value)
        {
            if (WriteIndex(value))
                _buffer.Write(value);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(uint value)
        {
            if (WriteIndex(value))
                _buffer.Write(value);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(long value)
        {
            if (WriteIndex(value))
                _buffer.Write(value);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(ulong value)
        {
            if (WriteIndex(value))
                _buffer.Write(value);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(float value)
        {
            if (WriteIndex(value))
                _buffer.Write(value);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(double value)
        {
            if (WriteIndex(value))
                _buffer.Write(value);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(string value)
        {
            if (WriteIndex(value))
                _buffer.Write(value);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(Color value)
        {
            if (WriteIndex(value))
                _buffer.Write(value.PackedValue);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(TimeSpan value)
        {
            if (WriteIndex(value))
                _buffer.Write(value.Ticks);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(Vector2 value)
        {
            if (!WriteIndex(value))
                return;
            _buffer.Write(value.X);
            _buffer.Write(value.Y);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(Rectangle value)
        {
            if (!WriteIndex(value))
                return;
            _buffer.Write(value.X);
            _buffer.Write(value.Y);
            _buffer.Write(value.Width);
            _buffer.Write(value.Height);
        }

        //[MethodImpl(MethodImplOptions.AggressiveInlining)]
        //public void Write(RectangleF value)
        //{
        //    if (!PreWrite(value))
        //        return;
        //    _buffer.Write(value.X);
        //    _buffer.Write(value.Y);
        //    _buffer.Write(value.Width);
        //    _buffer.Write(value.Height);
        //}

        /// <summary>
        ///     Serialize a replica reference.
        /// </summary>
        /// <param name="value"> The replica to serialize. </param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(IReplica value)
        {
            if (value == null)
                Write((uint) 0);
            else
                Write(value.ReplicaId);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void WriteVar(int value)
        {
            if (WriteIndex(value))
                _buffer.WriteVariableInt32(value);
        }

        /// <summary>
        ///     Writes the index of a value to the buffer if it has been changed since its previous value.
        /// </summary>
        /// <typeparam name="T"> The type to be written. </typeparam>
        /// <param name="value"> The value to be written. </param>
        /// <returns> True if the value needs to be written to the buffer. </returns>
        /// <remarks>
        ///     This method uses the default equality comparer for the specified type to check whether a value has
        ///     changed by comparing it to the value of the same index this method was previously called with.
        ///     Because of this, it should only be used with value types or immutable reference types.
        /// </remarks>
        public bool WriteIndex<T>(T value)
        {
            if (_buffer == null)
                throw new InvalidOperationException("Not currently writing");
            if (_index >= Terminator)
                throw new InvalidOperationException("Indices exhausted");

            ushort index = _index++;
            bool write;

            if (index == _values.Count)
            {
                var entry = new ValueEntry<T>
                {
                    Value = value,
                    Comparer = null
                };

                _values.Add(entry);
                write = true;
            }
            else
            {
                var entry = (ValueEntry<T>) _values[index];

                if (entry.Comparer != null)
                {
                    if (!entry.Comparer.Equals(value, entry.Value))
                    {
                        entry.Value = value;
                        write = true;
                    }
                    else
                    {
                        write = false;
                    }
                }
                else
                {
                    // Now a JIT intrinsic
                    if (!EqualityComparer<T>.Default.Equals(value, entry.Value))
                    {
                        entry.Value = value;
                        write = true;
                    }
                    else
                    {
                        write = false;
                    }
                }
            }

            if (!write)
                return false;

            _buffer.Write(index);
            _writeCount++;

            //Log.DebugFormat("Write change: Index={0}, Position={3}, Type={1}, Value={2}",
            //                _index - 1, typeof(T).Name, value, _buffer.LengthBits - _originalLengthBits);

            return true;
        }

        private bool WriteHasValue<T>(T? value)
            where T : struct
        {
            _buffer.Write(value.HasValue);
            return value.HasValue;
        }

        private abstract class ValueEntry
        {
        }

        private sealed class ValueEntry<T> : ValueEntry
        {
            public T Value;
            public IEqualityComparer<T> Comparer;
        }
    }
}