﻿using Lidgren.Network;

namespace Ramp.Network
{
    public class NetConnectionStatusChangedEventArgs : NetConnectionEventArgs
    {
        public NetConnectionStatusChangedEventArgs(NetConnection connection, NetConnectionStatus status, string reason)
            : base(connection)
        {
            Status = status;
            Reason = reason;
        }

        public NetConnectionStatus Status { get; private set; }

        public string Reason { get; private set; }
    }
}