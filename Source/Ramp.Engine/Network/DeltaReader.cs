﻿using System;
using System.IO;
using System.Runtime.CompilerServices;
using NLog;
using Lidgren.Network;
using Microsoft.Xna.Framework;

// ReSharper disable ConditionIsAlwaysTrueOrFalse

namespace Ramp.Network
{
    /// <summary>
    ///		Manages reading changes in data from a NetBuffer written by DeltaWriter.
    /// </summary>
    public class DeltaReader
    {
        private const ushort Terminator = ushort.MaxValue; // marks the end of the data
        private static readonly Logger s_log = LogManager.GetCurrentClassLogger();
        private NetBuffer _buffer; // buffer being written to or read from
        private ushort _index; // current read or write index
        private ushort _readCount; // the number of changed items read
        private long _readStartPosition;

        /// <summary>
        ///     Gets the buffer being read from.
        /// </summary>
        public NetBuffer Buffer => _buffer;

        public void BeginRead(NetBuffer buffer)
        {
            if (_buffer != null)
                throw new InvalidOperationException("Already reading");

            _buffer = buffer;
            _index = 0;
            _readCount = 0;
            _readStartPosition = buffer.Position;
        }

        public ushort EndRead()
        {
            if (_buffer == null)
                throw new InvalidOperationException("Not currently reading");

            try
            {
                ushort v = _buffer.ReadUInt16();
                if (v != Terminator)
                    throw new InvalidDataException("Data terminator not found");

                //if (_readCount != 0)
                //{
                //    Log.DebugFormat("Read delta: Index={0}, Changes={1}, Bits={2}",
                //                    _index - 1, _readCount, _buffer.Position - _readStartPosition);
                //}

                return _readCount;
            }
            finally
            {
                _buffer = null;
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Skip(ushort count)
        {
            // no mode check for inlining
            _index += count;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool Read(ref bool value)
        {
            if (!BeginRead())
                return false;
            value = _buffer.ReadBoolean();
            return true;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool Read(ref byte value)
        {
            if (!BeginRead())
                return false;
            value = _buffer.ReadByte();
            return true;
        }


        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool Read(ref byte? value)
        {
            if (!BeginRead())
                return false;
            value = ReadHasValue() ? _buffer.ReadByte() : (byte?) null;
            return true;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool Read(ref sbyte value)
        {
            if (!BeginRead())
                return false;
            value = _buffer.ReadSByte();
            return true;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool Read(ref int value)
        {
            if (!BeginRead())
                return false;
            value = _buffer.ReadInt32();
            return true;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool Read(ref uint value)
        {
            if (!BeginRead())
                return false;
            value = _buffer.ReadUInt32();
            return true;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool Read(ref float value)
        {
            if (!BeginRead())
                return false;
            value = _buffer.ReadSingle();
            return true;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool Read(ref double value)
        {
            if (!BeginRead())
                return false;
            value = _buffer.ReadDouble();
            return true;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool Read(ref string value, bool allowEmpty = false)
        {
            if (!BeginRead())
                return false;
            value = _buffer.ReadString();
            if (value.Length == 0 && !allowEmpty)
                value = null;
            return true;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool Read(ref IReplica replica, IReplicaService replicaService)
        {
            if (replicaService == null)
                throw new ArgumentNullException(nameof(replicaService));
            if (BeginRead())
                return false;
            uint id = _buffer.ReadUInt32();
            replica = replicaService.GetReplica(id);
            return true;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool Read(ref Vector2 value)
        {
            if (!BeginRead())
                return false;
            value.X = _buffer.ReadFloat();
            value.Y = _buffer.ReadFloat();
            return true;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool Read(ref Rectangle value)
        {
            if (!BeginRead())
                return false;
            value.X = _buffer.ReadInt32();
            value.Y = _buffer.ReadInt32();
            value.Width = _buffer.ReadInt32();
            value.Height = _buffer.ReadInt32();
            return true;
        }

        //[MethodImpl(MethodImplOptions.AggressiveInlining)]
        //public bool Read(ref RectangleF value)
        //{
        //    if (!PreRead())
        //        return false;
        //    value.X = _buffer.ReadSingle();
        //    value.Y = _buffer.ReadSingle();
        //    value.Width = _buffer.ReadSingle();
        //    value.Height = _buffer.ReadSingle();
        //    return true;
        //}

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool Read(ref Color value)
        {
            if (!BeginRead())
                return false;
            value = new Color { PackedValue = _buffer.ReadUInt32() };
            return true;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool Read(ref TimeSpan value)
        {
            if (!BeginRead())
                return false;
            value = TimeSpan.FromTicks(_buffer.ReadInt64());
            return true;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool ReadEnumByte<T>(ref T value)
        {
            if (!BeginRead())
                return false;
            value = (T) Enum.ToObject(typeof(T), _buffer.ReadByte());
            return true;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool ReadVar(ref int value)
        {
            if (!BeginRead())
                return false;
            value = _buffer.ReadVariableInt32();
            return true;
        }

        // Peek at the buffer to see if the next index matches the one we want.
        public bool BeginRead()
        {
            if (_buffer == null)
                throw new InvalidOperationException("Not currently reading");
            if (_index >= Terminator)
                throw new InvalidOperationException("Indices exhausted");

            ushort index = _index++;
            ushort next = _buffer.PeekUInt16();
            if (next != index || next == Terminator)
                return false;

            _buffer.Position += 16;
            _readCount++;

            //Log.DebugFormat("Read change: Index={0}, Position={1}", index, _buffer.Position - _readStartPosition);

            return true;
        }

        private bool ReadHasValue()
        {
            return _buffer.ReadBoolean();
        }
    }
}