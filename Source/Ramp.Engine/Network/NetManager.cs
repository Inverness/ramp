﻿using System;
using System.Net;
using NLog;
using Lidgren.Network;
using Microsoft.Xna.Framework;

namespace Ramp.Network
{
    //public enum NetMode
    //{
    //    Standalone,
    //    ListenServer,
    //    DedicatedServer,
    //    Client
    //}

    public delegate void MessageHandler(INetService service, byte index, NetIncomingMessage message);

    public class NetManager : GameComponent, INetService
    {
        public const int DefaultPort = 44566;

        private static readonly Logger s_log = LogManager.GetCurrentClassLogger();

        private NetPeer _peer;
        private NetClient _clientPeer;
        private NetServer _serverPeer;
        private readonly MessageHandler[] _handlers = new MessageHandler[byte.MaxValue];

        public NetManager(Game game)
            : base(game)
        {
            game.Services.AddService(typeof(INetService), this);
        }

        public event TypedEventHandler<INetService, NetConnectionEventArgs> ConnectionApproval;

        public event TypedEventHandler<INetService, NetConnectionStatusChangedEventArgs> ConnectionStatusChanged;

        public bool Online => _peer != null;

        public bool IsServer => _serverPeer != null;

        public bool IsClient => _clientPeer != null;

        public NetPeer Peer => _peer;

        public NetServer ServerPeer => _serverPeer;

        public NetClient ClientPeer => _clientPeer;

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _peer?.Shutdown("Disposing");
            }
            base.Dispose(disposing);
        }

        public override void Update(GameTime gameTime)
        {
            if (!Online)
                return;

            // Peer can be set to null during this loop if a message causes the game to exit.
            NetIncomingMessage msg;
            while (_peer != null && (msg = _peer.ReadMessage()) != null)
            {
                switch (msg.MessageType)
                {
                    case NetIncomingMessageType.StatusChanged:
                        var status = (NetConnectionStatus) msg.ReadByte();
                        string reason = msg.ReadString();

                        var e = new NetConnectionStatusChangedEventArgs(msg.SenderConnection, status, reason);
                        ConnectionStatusChanged?.Invoke(this, e);
                        break;

                    case NetIncomingMessageType.UnconnectedData:
                        s_log.Warn("Unknown unconnected data");
                        break;

                    case NetIncomingMessageType.ConnectionApproval:
                        ConnectionApproval?.Invoke(this, new NetConnectionEventArgs(msg.SenderConnection));
                        break;

                    case NetIncomingMessageType.Data:
                        byte handlerIndex = msg.ReadByte();

                        if (handlerIndex == 0)
                        {
                            s_log.Error("Received message for reserved handler 0");
                            break;
                        }

                        MessageHandler handler = _handlers[handlerIndex];

                        if (handler == null)
                        {
                            s_log.Warn("received message for unknown handler: " + handlerIndex);
                            break;
                        }
                        
                        handler(this, handlerIndex, msg);
                        break;

                    default:
                        Console.WriteLine("Unhandled type: " + msg.MessageType);
                        break;

                    case NetIncomingMessageType.VerboseDebugMessage:
                    case NetIncomingMessageType.DebugMessage:
                        s_log.Debug("Net: " + msg.ReadString());
                        break;
                    case NetIncomingMessageType.WarningMessage:
                        s_log.Warn("Net: " + msg.ReadString());
                        break;
                    case NetIncomingMessageType.ErrorMessage:
                        s_log.Error("Net: " + msg.ReadString());
                        break;
                }

                _peer.Recycle(msg);
            }
        }

        public void Listen(int port = DefaultPort)
        {
            if (Online)
                throw new InvalidOperationException();

            var config = new NetPeerConfiguration("Ramp")
            {
                Port = port,
                MaximumConnections = 4,
#if DEBUG
                ConnectionTimeout = 3600
#endif
            };

            config.EnableMessageType(NetIncomingMessageType.ConnectionApproval);
            config.EnableMessageType(NetIncomingMessageType.UnconnectedData);

            _peer = _serverPeer = new NetServer(config);
            _peer.Start();
        }

        public void Connect(string host, int port = DefaultPort, NetBuffer hail = null)
        {
            if (Online)
                throw new InvalidOperationException();

            var config = new NetPeerConfiguration("Ramp")
            {
#if DEBUG
                ConnectionTimeout = 3600
#endif
            };

            config.EnableMessageType(NetIncomingMessageType.ConnectionApproval);
            config.EnableMessageType(NetIncomingMessageType.UnconnectedData);

            _peer = _clientPeer = new NetClient(config);
            _peer.Start();
            if (hail == null)
            {
                _peer.Connect(host, port);
            }
            else
            {
                NetOutgoingMessage hailm = _peer.CreateMessage(hail.LengthBytes);
                hailm.Write(hail);
                _peer.Connect(host, port, hailm);
            }
        }

        public void Shutdown()
        {
            if (!Online)
                return;

            _peer.Shutdown("Shutdown");
            _peer = null;
            _serverPeer = null;
            _clientPeer = null;
        }

        public void SetMessageHandler(byte index, MessageHandler handler)
        {
            if (index == 0)
                throw new ArgumentOutOfRangeException(nameof(index), "reserved index");

            _handlers[index] = handler;
        }

        public NetOutgoingMessage CreateMessage(byte handlerIndex, int? capacity = null)
        {
            if (handlerIndex == 0)
                throw new ArgumentOutOfRangeException(nameof(handlerIndex), "reserved index");
            NetOutgoingMessage msg = capacity.HasValue ? _peer.CreateMessage(capacity.Value + 1) : _peer.CreateMessage();
            msg.Write(handlerIndex);
            return msg;
        }

        // Converts 1.2.3.4:5 to 1-2-3-4-5 for creating folders on disk
        public string CreateId(IPEndPoint endpoint)
        {
            string eps = endpoint.ToString();
            eps = eps.Replace('.', '-');
            eps = eps.Replace(':', '-');
            return eps;
        }
    }
}