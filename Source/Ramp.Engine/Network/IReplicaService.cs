﻿using System;
using System.Threading.Tasks;
using Lidgren.Network;

namespace Ramp.Network
{
    public delegate Task<IReplica> CreateReplicaHandler(uint id, NetRole role, NetBuffer creationId);

    public delegate void DestroyReplicaHandler(IReplica replica);

    public delegate void WriteCreationIdHandler(IReplica replica, NetBuffer buffer);

    /// <summary>
    ///     Provides object replication over the network.
    /// </summary>
    /// <remarks>
    ///     Replica IDs from 0 to 65535 are reserved for user-specification and must not be generated automatically.
    /// </remarks>
    public interface IReplicaService
    {
        ///// <summary>
        /////     Raised when this client receives a new replica from the server.
        ///// </summary>
        //event TypedEventHandler<IReplicaService, CreateReplicaEventArgs> ReplicaCreating;

        /// <summary>
        ///     Raised when a replica has been created on this client.
        /// </summary>
        event TypedEventHandler<IReplicaService, ReplicaEventArgs> ReplicaCreated;

        /// <summary>
        ///     Raised when a replica has been destroyed on this client.
        /// </summary>
        event TypedEventHandler<IReplicaService, ReplicaEventArgs> ReplicaDestroyed;

        /// <summary>
        ///     The network service being used.
        /// </summary>
        INetService NetService { get; }

        CreateReplicaHandler CreateReplicaHandler { get; set; }

        DestroyReplicaHandler DestroyReplicaHandler { get; set; }

        WriteCreationIdHandler WriteCreationIdHandler { get; set; }

        /// <summary>
        ///     Get a tracked replica.
        /// </summary>
        /// <param name="id"> A valid replica ID. </param>
        /// <returns> An IReplica instance if found, or null. </returns>
        /// <exception cref="ArgumentOutOfRangeException"> id is 0. </exception>
        IReplica GetReplica(uint id);

        /// <summary>
        ///     Add a replication connection.
        /// </summary>
        /// <param name="connection"> An IReplicaConnection instance </param>
        /// <exception cref="ArgumentNullException"> Connection is null. </exception>
        /// <exception cref="ArgumentException"> The inner connection or created set is null. </exception>
        /// <exception cref="InvalidOperationException"> The network service is a client. </exception>
        void AddConnection(IReplicaConnection connection);

        void RemoveConnection(IReplicaConnection connection);

        /// <summary>
        ///     Begin tracking a replica instance for replication.
        /// </summary>
        /// <param name="replica"> The replica to track. </param>
        /// <param name="id">
        ///     The desired replica ID. If 0, a replica ID is generated. IDs 0 to 65535 are reserved for user
        ///     specification and will not be automatically generated.
        /// </param>
        void TrackReplica(IReplica replica, uint id = 0);

        // TODO: Untracking must explicitly destroy the replica on client if necessary

        /// <summary>
        ///     Stop tracking a replica.
        /// </summary>
        /// <param name="replica"> The currently tracked replica. </param>
        void UntrackReplica(IReplica replica);

        /// <summary>
        ///     Create a message for sending an event to the specified replica on a remote machine.
        /// </summary>
        /// <param name="replica"> A replica. </param>
        /// <returns></returns>
        NetOutgoingMessage CreateReplicaMessage(IReplica replica);

        /// <summary>
        ///     Send a message created with CreateEventMessage().
        /// </summary>
        /// <param name="message"> A previously created message. </param>
        /// <param name="recipient"> The message's recipient. Can be left null when sending from client to server. </param>
        /// <param name="method"> The delivery mmethod. </param>
        void SendReplicaMessage(NetOutgoingMessage message, IReplicaConnection recipient, NetDeliveryMethod method);
    }
}