﻿using System.Collections.Generic;
using Lidgren.Network;

namespace Ramp.Network
{
    /// <summary>
    ///     Represents a connection that objects are being replicated to.
    /// </summary>
    public interface IReplicaConnection
    {
        /// <summary>
        ///     Gets the inner connection object.
        /// </summary>
        NetConnection Connection { get; }

        /// <summary>
        ///     Gets a dictionary containing server replica information.
        /// </summary>
        IDictionary<IReplica, ServerReplicaInfo> ReplicaInfo { get; }
    }
}