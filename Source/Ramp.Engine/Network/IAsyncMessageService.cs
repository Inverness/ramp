﻿using System.Threading.Tasks;
using Lidgren.Network;

namespace Ramp.Network
{
    /// <summary>
    ///     A service that provides task-based messaging on specific channels.
    /// </summary>
    public interface IAsyncMessageService
    {
        /// <summary>
        ///     Check if a channel is being received on.
        /// </summary>
        /// <param name="channel"> The channel number. </param>
        /// <returns> True if the channel is in use. </returns>
        bool IsChannelUsed(ushort channel);

        /// <summary>
        ///     Creates a message to be sent on the specified async channel.
        /// </summary>
        /// <param name="channel"> The channel number. </param>
        /// <returns> An outgoing message ready to have data written to it. </returns>
        NetOutgoingMessage CreateSendMessage(ushort channel);

        /// <summary>
        ///     Send a message to the specified recipient and then receive on the same channel.
        /// </summary>
        /// <param name="message"> The outgoing message. </param>
        /// <param name="recipient"> The message recipient. </param>
        /// <param name="method"> The reliable delivery method. </param>
        /// <returns> A task that completes when a response is received on the same channel. </returns>
        Task<NetIncomingMessage> Send(NetOutgoingMessage message, NetConnection recipient, NetDeliveryMethod method);

        /// <summary>
        ///     Receive on a channel.
        /// </summary>
        /// <param name="channel"> The channel number. </param>
        /// <returns> A task that completes when a message has been received on the channel. </returns>
        Task<NetIncomingMessage> Receive(ushort channel);

        /// <summary>
        ///     Cancels a receive and cancels the receiving task.
        /// </summary>
        /// <param name="channel"> The channel number. </param>
        void CancelReceive(ushort channel);
    }
}