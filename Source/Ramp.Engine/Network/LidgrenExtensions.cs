﻿using System;
using System.IO;
using System.Runtime.Serialization;
using Lidgren.Network;
using Microsoft.Xna.Framework;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;

namespace Ramp.Network
{
    public static class LidgrenExtensions
    {
        private static readonly JsonSerializerSettings SerializerSettings = new JsonSerializerSettings
        {
            ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor,
            Context = new StreamingContext(StreamingContextStates.CrossMachine),
            DefaultValueHandling = DefaultValueHandling.Include,
            Formatting = Formatting.None,
            MissingMemberHandling = MissingMemberHandling.Ignore,
            NullValueHandling = NullValueHandling.Include,
            ObjectCreationHandling = ObjectCreationHandling.Auto,
            TypeNameAssemblyFormatHandling = TypeNameAssemblyFormatHandling.Simple,
            TypeNameHandling = TypeNameHandling.Auto
        };

        /// <summary>
        ///     Reads a string written using Write(string), and returns null if the strength length is zero.
        /// </summary>
        /// <param name="buffer"></param>
        /// <returns></returns>
        public static string ReadStringNull(this NetBuffer buffer)
        {
            string value = buffer.ReadString();
            return value.Length == 0 ? null : value;
        }

        /// <summary>
        ///     Write a buffer object into the current buffer. Allows ReadBuffer() to retrieve the buffer object.
        /// </summary>
        /// <param name="buffer"> The current buffer. </param>
        /// <param name="value"> A buffer to be written. </param>
        public static void WriteBuffer(this NetBuffer buffer, NetBuffer value)
        {
            if (value == null)
                throw new ArgumentNullException(nameof(value));

            buffer.WriteVariableInt32(value.LengthBits);
            if (value.LengthBits != 0)
                buffer.Write(value.Data, 0, value.LengthBytes);
        }

        /// <summary>
        ///		Read a buffer object from the current buffer.
        /// </summary>
        /// <param name="buffer"> The current buffer. </param>
        /// <returns> A new buffer object. </returns>
        public static NetBuffer ReadBuffer(this NetBuffer buffer)
        {
            var output = new NetBuffer();
            int lengthBits = buffer.ReadVariableInt32();

            if (lengthBits != 0)
            {
                output.LengthBits = lengthBits;
                buffer.ReadBytes(output.Data, 0, output.LengthBytes);
            }

            return output;
        }

        public static void Write(this NetBuffer buffer, Vector2 value)
        {
            buffer.Write(value.X);
            buffer.Write(value.Y);
        }

        public static Vector2 ReadVector2(this NetBuffer buffer)
        {
            return new Vector2(buffer.ReadSingle(), buffer.ReadSingle());
        }

        public static void Write(this NetBuffer buffer, Vector3 value)
        {
            buffer.Write(value.X);
            buffer.Write(value.Y);
            buffer.Write(value.Z);
        }

        public static Vector3 ReadVector3(this NetBuffer buffer)
        {
            return new Vector3(buffer.ReadSingle(), buffer.ReadSingle(), buffer.ReadSingle());
        }

        public static void Write(this NetBuffer buffer, Point value)
        {
            buffer.Write(value.X);
            buffer.Write(value.Y);
        }

        public static Point ReadPoint(this NetBuffer buffer)
        {
            return new Point(buffer.ReadInt32(), buffer.ReadInt32());
        }

        public static void Write(this NetBuffer buffer, Rectangle value)
        {
            buffer.Write(value.X);
            buffer.Write(value.Y);
            buffer.Write(value.Width);
            buffer.Write(value.Height);
        }

        public static Rectangle ReadRectangle(this NetBuffer buffer)
        {
            return new Rectangle(buffer.ReadInt32(), buffer.ReadInt32(), buffer.ReadInt32(), buffer.ReadInt32());
        }

        public static void Write(this NetBuffer buffer, Color value)
        {
            buffer.Write(value.PackedValue);
        }

        public static Color ReadColor(this NetBuffer buffer)
        {
            return new Color { PackedValue = buffer.ReadUInt32() };
        }

        public static void Write(this NetBuffer buffer, TimeSpan value)
        {
            buffer.Write(value.Ticks);
        }

        public static TimeSpan ReadTimeSpan(this NetBuffer buffer)
        {
            return TimeSpan.FromTicks(buffer.ReadInt64());
        }

        public static bool ReadExpected(this NetBuffer buffer, byte id)
        {
            if (buffer.PeekByte() != id)
                return false;
            buffer.Position += 8;
            return true;
        }

        public static bool ReadExpected(this NetBuffer buffer, ushort id)
        {
            if (buffer.ReadUInt16() != id)
                return false;
            buffer.Position += 18;
            return true;
        }

        public static void WriteJsonObject<T>(this NetBuffer buffer, T obj, JsonSerializer serializer = null)
            where T : class
        {
            if (obj == null)
            {
                buffer.WriteVariableInt32(0);
                return;
            }

            using (var ms = new MemoryStream())
            using (var jw = new BsonDataWriter(ms))
            {
                JsonSerializer s = serializer ?? JsonSerializer.Create(SerializerSettings);
                s.Serialize(jw, obj);

                var length = checked((int) ms.Length);
                buffer.WriteVariableInt32(length);
                buffer.Write(ms.GetBuffer(), 0, length);
            }
        }

        public static T ReadJsonObject<T>(this NetBuffer buffer, JsonSerializer serializer = null)
            where T : class
        {
            int length = buffer.ReadVariableInt32();
            if (length == 0)
                return null;

            using (var ms = new MemoryStream(buffer.ReadBytes(length)))
            using (var jr = new BsonDataReader(ms))
            {
                JsonSerializer s = serializer ?? JsonSerializer.Create(SerializerSettings);
                return s.Deserialize<T>(jr);
            }
        }
    }
}