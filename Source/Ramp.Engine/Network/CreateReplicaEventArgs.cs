﻿using System;
using System.Threading.Tasks;
using Lidgren.Network;

namespace Ramp.Network
{
    public class CreateReplicaEventArgs : EventArgs
    {
        public CreateReplicaEventArgs(NetBuffer creationId, NetRole role)
        {
            CreationId = creationId;
            Role = role;
        }

        public NetBuffer CreationId { get; private set; }

        public NetRole Role { get; private set; }

        public IReplica Replica { get; set; }

        public Task<IReplica> ReplicaTask { get; set; }
    }
}