﻿using System;

namespace Ramp.Network
{
    /// <summary>
    ///     Holds persistent information about a replica on the server.
    /// </summary>
    public sealed class ServerReplicaInfo
    {
        /// <summary>
        ///     Gets the current delta writer.
        /// </summary>
        public DeltaWriter DeltaWriter { get; internal set; }

        /// <summary>
        ///     Gets the time the replica was last serialized at.
        /// </summary>
        public TimeSpan LastUpdateTime { get; internal set; }

        /// <summary>
        ///		Gets whether the replica has been created remotely.
        /// </summary>
        public bool IsRemoteCreated { get; internal set; }
    }
}