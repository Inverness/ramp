﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using NLog;
using Lidgren.Network;
using Microsoft.Xna.Framework;
using Ramp.Data;

namespace Ramp.Network
{
    /// <summary>
    ///     Manages the downloading of files from server to client.
    /// </summary>
    public sealed class FileDownloadManager : GameComponent, IFileDownloadService
    {
        private const byte FileRequestMessageId = 0;
        private const byte FileDataMessageId = 1;
        private const byte ManifestRequestMessageId = 2;
        private const byte ManifestDataMessageId = 3;

        private static readonly Logger s_log = LogManager.GetCurrentClassLogger();

        private readonly IConfigService _config;
        private readonly IFileService _file;
        private readonly INetService _net;
        private Dictionary<string, long> _serverManifest;

        public FileDownloadManager(Game game)
            : base(game)
        {
            _config = game.Services.GetRequiredService<IConfigService>();
            _net = game.Services.GetRequiredService<INetService>();
            _file = game.Services.GetRequiredService<IFileService>();
            game.Services.AddService(typeof(IFileDownloadService), this);
        }

        public event TypedEventHandler<IFileDownloadService, FileRequestEventArgs> FileReceived;

        public event TypedEventHandler<IFileDownloadService, EventArgs> ManifestReceived;

        public string CacheDirectory { get; set; }

        public string CacheFileExtension { get; set; }

        public IDictionary<string, long> ServerManifest => _serverManifest;

        public override void Initialize()
        {
            CacheDirectory = Path.Combine(_config.UserDirectory, "NetCache");
            CacheFileExtension = ".cache";
            _net.SetMessageHandler(MessageHandlerIndices.FileDownloadManager, OnDataReceived);
            //_file.FilePreparing += OnFilePreparing;
        }

        // Download all files that aren't on the client
        //public void SendFileDifferenceRequests()
        //{
        //    if (_serverManifest == null)
        //        return;
        //    Debug.Assert(_serverManifest.Count > 0);

        //    foreach (string fileName in _serverManifest.Keys)
        //    {

        //    }
        //}

        //private void OnFilePreparing(IFileService sender, PrepareFileEventArgs e)
        //{
        //    if (_net.IsClient && _serverManifest != null && _serverManifest.ContainsKey(e.Name))
        //    {

        //    }
        //}

        // Send a request for multiple files 
        public bool ClientRequestFiles(IReadOnlyCollection<string> fileNames)
        {
            if (!_net.IsClient)
                throw new InvalidOperationException();
            if (fileNames == null)
                throw new ArgumentNullException(nameof(fileNames));
            if (fileNames.Count == 0)
                throw new ArgumentException("empty collection", nameof(fileNames));

            // TODO: Make the server send us a list of the level it has to avoid invalid requests.
            NetOutgoingMessage msg = _net.CreateMessage(MessageHandlerIndices.FileDownloadManager);
            msg.Write(FileRequestMessageId);
            msg.Write(fileNames.Count);

            s_log.Info("Requesting files from server:");
            foreach (string fileName in fileNames)
            {
                Debug.Assert(!string.IsNullOrEmpty(fileName), "!String.IsNullOrEmpty(fileName)");
                msg.Write(fileName);
                s_log.Info("  {0}", fileName);
            }

            _net.ClientPeer.SendMessage(msg, NetDeliveryMethod.ReliableOrdered);

            return true;
        }

        private void OnDataReceived(INetService service, byte channel, NetIncomingMessage message)
        {
            switch (message.ReadByte())
            {
                case FileRequestMessageId:
                    OnReceiveFileRequest(message);
                    break;
                case FileDataMessageId:
                    OnReceiveFileData(message);
                    break;
                case ManifestRequestMessageId:
                    OnReceiveManifestRequest(message);
                    break;
                case ManifestDataMessageId:
                    OnReceiveManifestData(message);
                    break;
            }
        }

        // Handles file requests and calls SendFileData() for each file name in the message
        private void OnReceiveFileRequest(NetIncomingMessage msg)
        {
            int count = msg.ReadInt32();
            for (int i = 0; i < count; i++)
            {
                string fileName = msg.ReadString();
                if (fileName.Length == 0)
                {
                    s_log.Warn("Received empty filename during file request from " + msg.SenderConnection);
                    continue;
                }

                byte[] data;

                FileManifestEntry info = _file.GetEntry(fileName);

                if (info == null)
                {
                    s_log.Error("Requested file not found: {0}", fileName);
                    continue;
                }

                try
                {
                    // TODO: File streaming
                    data = File.ReadAllBytes(info.Path);
                }
                catch (IOException ex)
                {
                    s_log.Error("File read error", ex);
                    continue;
                }

                s_log.Info("Sending file to client: {0}, {1} bytes, {2}", fileName, info.Size, msg.SenderEndPoint);

                SendFileData(msg.SenderConnection, fileName, data);
            }
        }

        // Sends file data to the client.
        // TODO: Add file streaming do we don't run into memory issues.
        private void SendFileData(NetConnection client, string name, byte[] data, string message = null)
        {
            NetOutgoingMessage msg = _net.ServerPeer.CreateMessage();
            msg.Write(MessageHandlerIndices.FileDownloadManager);
            msg.Write(FileDataMessageId);
            msg.Write(name);
            bool hasData = data != null;
            msg.Write(hasData);
            if (hasData)
            {
                msg.Write(data.Length);
                msg.Write(data);
            }
            else
            {
                msg.Write(message);
            }
            _net.ServerPeer.SendMessage(msg, client, NetDeliveryMethod.ReliableOrdered);
        }

        // Called when we receive a file from the server
        private void OnReceiveFileData(NetIncomingMessage msg)
        {
            string fileName = msg.ReadString();
            bool hasData = msg.ReadBoolean();
            byte[] data;
            if (hasData)
            {
                int dataSize = msg.ReadInt32();
                data = msg.ReadBytes(dataSize);
            }
            else
            {
                string errorMessage = msg.ReadString();
                s_log.Error("Level request failed: {0}", errorMessage);
                return;
            }

            if (_file.HasEntry(fileName))
            {
                s_log.Error("File already exists");
                return;
            }

            // Saves the file to the cache directory and adds it to the file manifest
            string filePath = CreateCachedFile(fileName, data);

            // TODO: Use the server's file info
            //_file.AddFile(fileName, "", filePath, true);

            FileReceived?.Invoke(this, new FileRequestEventArgs(fileName, _file));

            s_log.Info("Received file from server: {0}, {1} bytes", fileName, data.Length);

            throw new NotImplementedException();
        }

        public void ClientRequestManifest()
        {
            if (!_net.IsClient)
                throw new InvalidOperationException();

            s_log.Info("Requesting file manifest from server.");

            NetOutgoingMessage msg = _net.CreateMessage(MessageHandlerIndices.FileDownloadManager);
            msg.Write(ManifestRequestMessageId);
            _net.ClientPeer.SendMessage(msg, NetDeliveryMethod.ReliableOrdered);
        }

        private void OnReceiveManifestRequest(NetIncomingMessage msg)
        {
            ServerSendManifest(msg.SenderConnection);
        }

        public void ServerSendManifest(NetConnection client)
        {
            if (!_net.IsServer)
                throw new InvalidOperationException();

            throw new NotImplementedException();

            //NetOutgoingMessage msg = _net.CreateMessage(NetChannel, ManifestDataMessageId);
            //msg.Write(_file.Manifest.EntryCount);

            //foreach (KeyValuePair<string, FileManifestEntry> entries in _file.Manifest.GetEntries())
            //{
            //    Debug.Assert(!String.IsNullOrEmpty(entries.Key));
            //    msg.Write(entries.Key);
            //    msg.WriteVariableInt64(entries.Value.Size);
            //}

            //_net.ServerPeer.SendMessage(msg, client, NetDeliveryMethod.ReliableOrdered);
        }

        private void OnReceiveManifestData(NetIncomingMessage msg)
        {
            uint count = msg.ReadUInt32();

            if (count == 0)
                return;

            if (_serverManifest == null)
                _serverManifest = new Dictionary<string, long>();
            else
                _serverManifest.Clear();

            for (int i = 0; i < count; i++)
            {
                string fileName = msg.ReadString();
                long fileSize = msg.ReadVariableInt64();
                _serverManifest[fileName] = fileSize;
            }

            ManifestReceived?.Invoke(this, EventArgs.Empty);
        }

        private string CreateCachedFile(string fileName, byte[] data)
        {
            string serverId = _net.CreateId(_net.ClientPeer.ServerConnection.RemoteEndPoint);
            string filePath = Path.Combine(CacheDirectory, serverId, fileName);
            string fileDir = Path.GetDirectoryName(filePath);
            if (fileDir != null && !Directory.Exists(fileDir))
                Directory.CreateDirectory(fileDir);
            File.WriteAllBytes(filePath, data);
            return filePath;
        }
    }

    public class FileRequestEventArgs : EventArgs
    {
        public FileRequestEventArgs(string fileName, IFileService fileService)
        {
            FileName = fileName;
            FileService = fileService;
        }

        public string FileName { get; private set; }

        public IFileService FileService { get; private set; }
    }
}