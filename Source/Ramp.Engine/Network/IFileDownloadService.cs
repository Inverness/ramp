﻿using System;
using System.Collections.Generic;
using Lidgren.Network;

namespace Ramp.Network
{
    /// <summary>
    ///     Service that allows downloading levels from server to client.
    ///     Requires INetService and ILevelService.
    /// </summary>
    public interface IFileDownloadService
    {
        /// <summary>
        ///     Raised when a level request has been completed and the file has been cached on this client and is ready to
        ///     be loaded.
        /// </summary>
        event TypedEventHandler<IFileDownloadService, FileRequestEventArgs> FileReceived;

        /// <summary>
        ///     Raised on the client when a manifest is received from the server.
        /// </summary>
        event TypedEventHandler<IFileDownloadService, EventArgs> ManifestReceived;

        /// <summary>
        ///     Root directory for storing level files. Directories are created inside this one for each server based on its
        ///     address.
        /// </summary>
        string CacheDirectory { get; set; }

        /// <summary>
        ///     The file extension to append to cached files.
        /// </summary>
        string CacheFileExtension { get; set; }

        /// <summary>
        ///     The server's manifest, a pair of file name and file size.
        /// </summary>
        IDictionary<string, long> ServerManifest { get; }

        /// <summary>
        ///     Send a file requests to the server. This method returns immediately. The FileRequestCompleted event will be
        ///     raised when each download is complete.
        /// </summary>
        /// <returns> True if the request is valid and has been sent to the server. </returns>
        bool ClientRequestFiles(IReadOnlyCollection<string> fileNames);

        /// <summary>
        ///		Send a request for the server's manifest.
        /// </summary>
        void ClientRequestManifest();

        /// <summary>
        ///     Send the server's manifest to the specified client.
        /// </summary>
        /// <param name="client"> A client's connection. </param>
        void ServerSendManifest(NetConnection client);

        //void SendFileDifferenceRequests();
    }
}