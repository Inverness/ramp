﻿namespace Ramp.Network
{
    /// <summary>
    ///     Provides a context for replica serialization.
    /// </summary>
    public sealed class ReplicationContext
    {
        /// <summary>
        ///     Gets the replication destination.
        /// </summary>
        public IReplicaConnection Destination { get; internal set; }
    }
}