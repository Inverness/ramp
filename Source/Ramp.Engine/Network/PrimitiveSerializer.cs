﻿using System;
using Lidgren.Network;
using Microsoft.Xna.Framework;

namespace Ramp.Network
{
    /// <summary>
    ///     Allows for fast serialization of primitive objects.
    /// </summary>
    public static class PrimitiveSerializer
    {
        private static readonly byte[] s_emptyByteArray = new byte[0];

        public static void Write(NetBuffer buffer, object item)
        {
            if (item == null)
            {
                buffer.Write((byte) ItemType.Null);
                return;
            }

            ItemType itemType = GetItemType(item.GetType());

            if (itemType == ItemType.Invalid)
                throw new ArgumentException("Item type not supported: " + item.GetType().FullName, nameof(item));

            buffer.Write((byte) itemType);

            switch (itemType)
            {
                case ItemType.Null:
                    break;
                case ItemType.Boolean:
                    buffer.Write((bool) item);
                    break;
                case ItemType.Char:
                    buffer.Write((char) item);
                    break;
                case ItemType.SByte:
                    buffer.Write((sbyte) item);
                    break;
                case ItemType.Byte:
                    buffer.Write((byte) item);
                    break;
                case ItemType.Int16:
                    buffer.Write((short) item);
                    break;
                case ItemType.UInt16:
                    buffer.Write((ushort) item);
                    break;
                case ItemType.Int32:
                    buffer.Write((int) item);
                    break;
                case ItemType.UInt32:
                    buffer.Write((uint) item);
                    break;
                case ItemType.Int64:
                    buffer.Write((long) item);
                    break;
                case ItemType.UInt64:
                    buffer.Write((ulong) item);
                    break;
                case ItemType.Single:
                    buffer.Write((float) item);
                    break;
                case ItemType.Double:
                    buffer.Write((double) item);
                    break;
                case ItemType.String:
                    buffer.Write((string) item);
                    break;
                case ItemType.DateTime:
                    buffer.Write(((DateTime) item).ToBinary());
                    break;
                case ItemType.TimeSpan:
                    buffer.Write(((TimeSpan) item).Ticks);
                    break;
                case ItemType.ByteArray:
                    var ba = (byte[]) item;
                    buffer.WriteVariableInt32(ba.Length);
                    if (ba.Length != 0)
                        buffer.Write(ba);
                    break;
                case ItemType.NetBuffer:
                    buffer.WriteBuffer((NetBuffer) item);
                    break;
                case ItemType.Vector2:
                    buffer.Write((Vector2) item);
                    break;
                case ItemType.Vector3:
                    buffer.Write((Vector3) item);
                    break;
                case ItemType.Point:
                    buffer.Write((Point) item);
                    break;
                case ItemType.Rectangle:
                    buffer.Write((Rectangle) item);
                    break;
                case ItemType.Color:
                    buffer.Write((Color) item);
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        public static void WriteArray(NetBuffer buffer, object[] array)
        {
            for (int i = 0; i < array.Length; i++)
                Write(buffer, array[i]);
        }

        public static bool Read(NetBuffer buffer, out object arg)
        {
            var argType = (ItemType) buffer.ReadByte();
            switch (argType)
            {
                case ItemType.Null:
                    arg = null;
                    break;
                case ItemType.Boolean:
                    arg = buffer.ReadBoolean();
                    break;
                case ItemType.Char:
                    arg = (char) buffer.ReadUInt16();
                    break;
                case ItemType.SByte:
                    arg = buffer.ReadSByte();
                    break;
                case ItemType.Byte:
                    arg = buffer.ReadByte();
                    break;
                case ItemType.Int16:
                    arg = buffer.ReadInt16();
                    break;
                case ItemType.UInt16:
                    arg = buffer.ReadUInt16();
                    break;
                case ItemType.Int32:
                    arg = buffer.ReadInt32();
                    break;
                case ItemType.UInt32:
                    arg = buffer.ReadUInt32();
                    break;
                case ItemType.Int64:
                    arg = buffer.ReadInt64();
                    break;
                case ItemType.UInt64:
                    arg = buffer.ReadUInt64();
                    break;
                case ItemType.Single:
                    arg = buffer.ReadSingle();
                    break;
                case ItemType.Double:
                    arg = buffer.ReadDouble();
                    break;
                case ItemType.String:
                    arg = buffer.ReadString();
                    break;
                case ItemType.DateTime:
                    arg = DateTime.FromBinary(buffer.ReadInt64());
                    break;
                case ItemType.TimeSpan:
                    arg = TimeSpan.FromTicks(buffer.ReadInt64());
                    break;
                case ItemType.ByteArray:
                    int length = buffer.ReadVariableInt32();
                    arg = length != 0 ? buffer.ReadBytes(length) : s_emptyByteArray;
                    break;
                case ItemType.NetBuffer:
                    arg = buffer.ReadBuffer();
                    break;
                case ItemType.Vector2:
                    arg = buffer.ReadVector2();
                    break;
                case ItemType.Vector3:
                    arg = buffer.ReadVector3();
                    break;
                case ItemType.Point:
                    arg = buffer.ReadPoint();
                    break;
                case ItemType.Rectangle:
                    arg = buffer.ReadRectangle();
                    break;
                case ItemType.Color:
                    arg = buffer.ReadColor();
                    break;
                default:
                    arg = null;
                    return false;
            }
            return true;
        }

        public static bool ReadArray(NetBuffer buffer, object[] array)
        {
            for (int i = 0; i < array.Length; i++)
                if (!Read(buffer, out array[i]))
                    return false;
            return true;
        }

        private static ItemType GetItemType(Type type)
        {
            switch (Type.GetTypeCode(type))
            {
                case TypeCode.Object:
                    if (type == typeof(TimeSpan))
                        return ItemType.TimeSpan;

                    if (type == typeof(byte[]))
                        return ItemType.ByteArray;

                    if (type == typeof(NetBuffer))
                        return ItemType.NetBuffer;

                    if (type == typeof(Vector2))
                        return ItemType.Vector2;

                    if (type == typeof(Vector3))
                        return ItemType.Vector3;

                    if (type == typeof(Point))
                        return ItemType.Point;

                    if (type == typeof(Rectangle))
                        return ItemType.Rectangle;

                    if (type == typeof(Color))
                        return ItemType.Color;

                    break;
                case TypeCode.Boolean:
                    return ItemType.Boolean;
                case TypeCode.Char:
                    return ItemType.Char;
                case TypeCode.SByte:
                    return ItemType.SByte;
                case TypeCode.Byte:
                    return ItemType.Byte;
                case TypeCode.Int16:
                    return ItemType.Int16;
                case TypeCode.UInt16:
                    return ItemType.UInt16;
                case TypeCode.Int32:
                    return ItemType.Int32;
                case TypeCode.UInt32:
                    return ItemType.UInt32;
                case TypeCode.Int64:
                    return ItemType.Int64;
                case TypeCode.UInt64:
                    return ItemType.UInt64;
                case TypeCode.Single:
                    return ItemType.Single;
                case TypeCode.Double:
                    return ItemType.Double;
                case TypeCode.DateTime:
                    return ItemType.DateTime;
                case TypeCode.String:
                    return ItemType.String;
            }

            return ItemType.Invalid;
        }

        private enum ItemType : byte
        {
            // Basic framework types
            Invalid,
            Null,
            Boolean,
            Char,
            SByte,
            Byte,
            Int16,
            UInt16,
            Int32,
            UInt32,
            Int64,
            UInt64,
            Single,
            Double,
            String,
            DateTime,
            TimeSpan,
            ByteArray,

            // Lidgren types
            NetBuffer,

            // XNA types
            Vector2,
            Vector3,
            Point,
            Rectangle,
            Color
        }
    }
}