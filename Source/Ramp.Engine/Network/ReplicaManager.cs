﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using NLog;
using Lidgren.Network;
using Microsoft.Xna.Framework;

namespace Ramp.Network
{
    /// <summary>
    ///     Implements the replica service in order to provide object replication over the network.
    /// </summary>
    public sealed class ReplicaManager : GameComponent, IReplicaService
    {
        private static readonly Logger s_log = LogManager.GetCurrentClassLogger();

        private readonly INetService _net;

        // Server: All existing replicas in the game that can be replicated
        // Client: All replicas that are currently being replicated from the server
        private readonly Dictionary<uint, IReplica> _replicas = new Dictionary<uint, IReplica>();

        // Server: All connected clients. Each client stores information about serialization messages previously sent
        // to it to allow delta serialization based on differences.
        private readonly List<IReplicaConnection> _connections = new List<IReplicaConnection>();

        // Client: Delta serialization information received from the server, keyed by replica ID.
        private readonly Dictionary<uint, DeltaReader> _deltaReaders = new Dictionary<uint, DeltaReader>();

        // Server: A random number generator for replica IDs.
        private readonly Random _random = new Random();

        // Server: Time in seconds between each update
        private readonly TimeSpan _updateRate = TimeSpan.FromSeconds(0.2);
        private TimeSpan _nextUpdateTime;
        private TimeSpan _time;

        // Server: A cached serialization message to avoid rellocation.
        private NetOutgoingMessage _serializationMessage;

        // The serialziation context provided to replicas. Allocated once then reused.
        private readonly ReplicationContext _replicationContext = new ReplicationContext();

        public ReplicaManager(Game game)
            : base(game)
        {
            _net = game.Services.GetRequiredService<INetService>();

            game.Services.AddService(typeof(IReplicaService), this);
        }

        /// <summary>
        ///     Raised after a replica has been created on the client.
        /// </summary>
        public event TypedEventHandler<IReplicaService, ReplicaEventArgs> ReplicaCreated;

        /// <summary>
        ///     Raised after a replica has been destroyed on the client. A handler must handle disposing of the underlying
        ///     object.
        /// </summary>
        public event TypedEventHandler<IReplicaService, ReplicaEventArgs> ReplicaDestroyed;

        public CreateReplicaHandler CreateReplicaHandler { get; set; }

        public DestroyReplicaHandler DestroyReplicaHandler { get; set; }

        public WriteCreationIdHandler WriteCreationIdHandler { get; set; }

        public INetService NetService => _net;

        public override void Initialize()
        {
            _net.SetMessageHandler(MessageHandlerIndices.ReplicaManager, OnDataReceived);
        }

        public override void Update(GameTime gameTime)
        {
            _time = gameTime.TotalGameTime;

            if (_time < _nextUpdateTime)
                return;

            _nextUpdateTime = _time + _updateRate;

            if (_connections.Count == 0 || !_net.IsServer)
                return;

            UpdateReplicas();
        }

        /// <summary>
        ///     Register a new network connection with the replica manager
        /// </summary>
        public void AddConnection(IReplicaConnection connection)
        {
            if (connection == null)
                throw new ArgumentNullException(nameof(connection));
            if (connection.Connection == null)
                throw new ArgumentException("client's connection is null", nameof(connection));
            if (connection.ReplicaInfo == null)
                throw new ArgumentException("client's created set is null", nameof(connection));
            if (_net.IsClient)
                throw new InvalidOperationException("can't add clients in client mode");
            Debug.Assert(!_connections.Contains(connection));
            _connections.Add(connection);
        }

        /// <summary>
        ///     Unregister a network connection
        /// </summary>
        public void RemoveConnection(IReplicaConnection connection)
        {
            if (connection == null)
                throw new ArgumentNullException(nameof(connection));
            _connections.Remove(connection);
        }

        /// <summary>
        ///     Retrieve a replica by its randomly generated ID.
        /// </summary>
        /// <param name="id"> The replica's ID. </param>
        /// <returns> The matching replica, or null if none was found. </returns>
        public IReplica GetReplica(uint id)
        {
            if (id == 0)
                throw new ArgumentOutOfRangeException(nameof(id));
            IReplica replica;
            _replicas.TryGetValue(id, out replica);
            return replica;
        }

        /// <summary>
        ///     Begin tracking a replica and provide it with a unique ID.
        /// </summary>
        public void TrackReplica(IReplica replica, uint id = 0)
        {
            if (replica == null)
                throw new ArgumentNullException(nameof(replica));
            if (replica.ReplicaService == this)
                throw new InvalidOperationException("already tracked");
            if (replica.ReplicaService != null)
                throw new InvalidOperationException("tracked by another replica service");

            if (id == 0)
                id = NewReplicaId();

            replica.OnTrack(this, id);

            Debug.Assert(replica.ReplicaId == id && replica.ReplicaService == this);
            Debug.Assert(!_replicas.ContainsKey(replica.ReplicaId));

            _replicas[replica.ReplicaId] = replica;

            s_log.Debug("Track replica " + id);
        }

        /// <summary>
        ///     Stop tracking a replica.
        /// </summary>
        public void UntrackReplica(IReplica replica)
        {
            if (replica == null)
                throw new ArgumentNullException(nameof(replica));

            if (replica.ReplicaId == 0)
                return;

            if (replica.ReplicaService != this)
                throw new InvalidOperationException("replica not owned by this service");

            uint id = replica.ReplicaId;
            _replicas.Remove(replica.ReplicaId);

            // Clean up serverside data
            foreach (IReplicaConnection connection in _connections)
                connection.ReplicaInfo.Remove(replica);

            // Clean up clientside data
            _deltaReaders.Remove(replica.ReplicaId);

            replica.OnUntrack(this);

            Debug.Assert(replica.ReplicaId == 0 && replica.ReplicaService == null,
                         "replica.ReplicaID == 0 && replica.ReplicaService == null");
            s_log.Debug("Untrack replica " + id);
        }

        private void OnDataReceived(INetService sender, byte channel, NetIncomingMessage message)
        {
            try
            {
                switch (message.ReadByte())
                {
                    case MessageTypes.CreateReplica:
                        OnReceiveCreateReplica(message);
                        break;
                    case MessageTypes.DestroyReplica:
                        OnReceiveDestroyReplica(message);
                        break;
                    case MessageTypes.UpdateReplica:
                        OnReceiveUpdateReplica(message);
                        break;
                    case MessageTypes.ConfirmReplica:
                        OnReceiveConfirmReplica(message);
                        break;
                    case MessageTypes.ReplicaMessage:
                        OnReceiveReplicaMessage(message);
                        break;
                    default:
                        s_log.Warn("ReplicaManager received unknown message type");
                        break;
                }
            }
            catch (Exception ex)
            {
                s_log.Error(ex, "Exception during replica manager receive.");
            }
        }

        // Create a new non-zero replica ID. The first 0 to 65535 are reserved.
        private uint NewReplicaId()
        {
            // TODO: Used a pooled indexer or list
            var id = unchecked((uint) _random.Next(int.MinValue + ushort.MaxValue, int.MaxValue));
            Debug.Assert(!_replicas.ContainsKey(id) && id != 0, "!_replicasByID.ContainsKey(id) && id != 0");
            return id;
        }

        /// <summary>
        ///		Sends creation, destruction, and update message for all replicas as necessary
        /// </summary>
        private void UpdateReplicas()
        {
            Debug.Assert(_net.IsServer);

            foreach (IReplicaConnection connection in _connections)
            {
                IDictionary<IReplica, ServerReplicaInfo> infoDictionary = connection.ReplicaInfo;

                foreach (IReplica replica in _replicas.Values)
                {
                    ServerReplicaInfo sri;
                    infoDictionary.TryGetValue(replica, out sri);

                    bool relevant = replica.RemoteRole != NetRole.None && replica.IsRelevantTo(connection);

                    // The presence of server replication info indicates that that the replica exists on the client
                    // or the creation message has been sent.
                    if (sri != null)
                    {
                        if (relevant)
                        {
                            if (sri.IsRemoteCreated && _time - sri.LastUpdateTime > replica.ReplicaUpdateRate)
                            {
                                SendUpdateReplica(connection, replica, sri);
                                sri.LastUpdateTime = _time;
                            }
                        }
                        else
                        {
                            SendDestroyReplica(connection, replica);
                            infoDictionary.Remove(replica);
                        }
                    }
                    else if (relevant)
                    {
                        SendCreateReplica(connection, replica);
                        infoDictionary[replica] = new ServerReplicaInfo();
                    }
                }
            }
        }

        private void SendCreateReplica(IReplicaConnection connection, IReplica replica)
        {
            Debug.Assert(_net.IsServer);

            NetOutgoingMessage message = _net.CreateMessage(MessageHandlerIndices.ReplicaManager);
            message.Write(MessageTypes.CreateReplica);
            message.Write(replica.ReplicaId);
            message.Write((byte) replica.RemoteRole);

            var creationIdBuffer = new NetBuffer();
            WriteCreationIdHandler(replica, creationIdBuffer);
            message.WriteBuffer(creationIdBuffer);

            _net.ServerPeer.SendMessage(message,
                                        connection.Connection,
                                        NetDeliveryMethod.ReliableOrdered,
                                        OrderingChannels.Replication);
        }

        private async void OnReceiveCreateReplica(NetIncomingMessage message)
        {
            uint id = message.ReadUInt32();
            var role = (NetRole) message.ReadByte();
            NetBuffer creationIdBuffer = message.ReadBuffer();

            s_log.Debug("Creating replica {0} with role {1}", id, role);

            IReplica replica = await CreateReplicaHandler(id, role, creationIdBuffer);

            Debug.Assert(replica != null, "replica != null");
            Debug.Assert(replica.Role == role && replica.RemoteRole == NetRole.Authority,
                         "replica.Role == role && replica.RemoteRole == NetRole.Authority");

            TrackReplica(replica, id);

            SendConfirmReplica(replica);

            ReplicaCreated?.Invoke(this, new ReplicaEventArgs(replica));

            s_log.Debug("Created replica " + id);
        }

        private void SendDestroyReplica(IReplicaConnection connection, IReplica replica)
        {
            Debug.Assert(_net.IsServer);

            NetOutgoingMessage message = _net.CreateMessage(MessageHandlerIndices.ReplicaManager);
            message.Write(MessageTypes.DestroyReplica);
            message.Write(replica.ReplicaId);

            _net.ServerPeer.SendMessage(message,
                                        connection.Connection,
                                        NetDeliveryMethod.ReliableOrdered,
                                        OrderingChannels.Replication);
        }

        private void OnReceiveDestroyReplica(NetIncomingMessage message)
        {
            uint id = message.ReadUInt32();

            IReplica replica;
            if (!_replicas.TryGetValue(id, out replica))
            {
                s_log.Warn("Received destroy replica message for untracked replica. ID={0}", id);
                return;
            }

            ReplicaDestroyed?.Invoke(this, new ReplicaEventArgs(replica));

            DestroyReplicaHandler(replica);
        }

        private void SendConfirmReplica(IReplica replica)
        {
            Debug.Assert(_net.IsClient);

            NetOutgoingMessage message = _net.CreateMessage(MessageHandlerIndices.ReplicaManager);
            message.Write(MessageTypes.ConfirmReplica);
            message.Write(replica.ReplicaId);

            _net.ClientPeer.SendMessage(message,
                                        NetDeliveryMethod.ReliableOrdered,
                                        OrderingChannels.Replication);
        }

        private void OnReceiveConfirmReplica(NetIncomingMessage message)
        {
            uint id = message.ReadUInt32();

            IReplica replica;
            if (!_replicas.TryGetValue(id, out replica))
            {
                s_log.Error("Received confirm replica message for untracked replica. ID={0}", id);
                return;
            }

            IReplicaConnection connection = _connections.FirstOrDefault(c => c.Connection == message.SenderConnection);
            if (connection == null)
            {
                s_log.Error("Received confirm replica message from unknown client. ReplicaID={0}", id);
                return;
            }

            ServerReplicaInfo info;
            if (!connection.ReplicaInfo.TryGetValue(replica, out info))
            {
                s_log.Error("Received confirm replica message for replica without info. ReplicaID={0}", id);
                return;
            }

            if (info.IsRemoteCreated)
            {
                s_log.Error("Received unnecessary confirm replica message. ReplicaID={0}", id);
                return;
            }

            info.IsRemoteCreated = true;

            s_log.Debug("Confirmed creation of replica " + id);
        }

        private void SendUpdateReplica(IReplicaConnection connection, IReplica replica, ServerReplicaInfo sri)
        {
            Debug.Assert(_net.IsServer);

            // Reused a preallocated message in this case due to the number of sends that will fail due to no
            // changes taking place.
            if (_serializationMessage == null)
                _serializationMessage = _net.Peer.CreateMessage();
            _serializationMessage.LengthBits = 0; // reset length to erase junk from previous attempt

            _serializationMessage.Write(MessageHandlerIndices.ReplicaManager);
            _serializationMessage.Write(MessageTypes.UpdateReplica);
            _serializationMessage.Write(replica.ReplicaId);

            _replicationContext.Destination = connection;

            DeltaWriter delta = sri.DeltaWriter ?? (sri.DeltaWriter = new DeltaWriter());
            delta.BeginWrite(_serializationMessage);
            replica.WriteDelta(delta, _replicationContext);
            ushort writeCount = delta.EndWrite();

            if (writeCount == 0)
                return;

            //Log.DebugFormat("Serialized replica: ID={0}, DataSize={1}, WriteCount={2}",
            //                replica.ReplicaId, _serializationMessage.LengthBytes, writeCount);

            _net.ServerPeer.SendMessage(_serializationMessage,
                                        connection.Connection,
                                        NetDeliveryMethod.ReliableOrdered,
                                        OrderingChannels.Replication);

            _serializationMessage = null;
        }

        private void OnReceiveUpdateReplica(NetIncomingMessage message)
        {
            uint id = message.ReadUInt32();

            //Log.DebugFormat("Deserializing replica: ID={0}, DataSize={1}", id, message.LengthBytes);

            IReplica replica;
            if (!_replicas.TryGetValue(id, out replica))
                throw new InvalidDataException("Received serialization for replica that doesn't exist: " + id);

            DeltaReader deltaReader;
            if (!_deltaReaders.TryGetValue(id, out deltaReader))
                _deltaReaders[id] = (deltaReader = new DeltaReader());

            deltaReader.BeginRead(message);
            replica.ReadDelta(deltaReader, null);
            deltaReader.EndRead();
        }

        public NetOutgoingMessage CreateReplicaMessage(IReplica replica)
        {
            if (replica == null)
                throw new ArgumentNullException(nameof(replica));
            if (replica.ReplicaService != this)
                throw new ArgumentException("not managed by this service");

            Debug.Assert(replica.ReplicaId != 0, "replica.ReplicaID != 0");

            NetOutgoingMessage msg = _net.CreateMessage(MessageHandlerIndices.ReplicaManager);
            msg.Write(MessageTypes.ReplicaMessage);
            msg.Write(replica.ReplicaId);
            return msg;
        }

        public void SendReplicaMessage(NetOutgoingMessage message, IReplicaConnection recipient,
                                       NetDeliveryMethod deliveryMethod)
        {
            if (message == null)
                throw new ArgumentNullException(nameof(message));

            int sequenceChannel;

            switch (deliveryMethod)
            {
                case NetDeliveryMethod.ReliableOrdered:
                case NetDeliveryMethod.ReliableSequenced:
                case NetDeliveryMethod.UnreliableSequenced:
                    sequenceChannel = OrderingChannels.Event;
                    break;
                case NetDeliveryMethod.ReliableUnordered:
                case NetDeliveryMethod.Unreliable:
                    sequenceChannel = 0;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(deliveryMethod));
            }

            if (_net.IsClient)
            {
                _net.ClientPeer.SendMessage(message, deliveryMethod, sequenceChannel);
            }
            else
            {
                if (recipient == null)
                    throw new ArgumentNullException(nameof(recipient), "no recipient specified for server to client message");
                Debug.Assert(_connections.Count != 0);
                _net.ServerPeer.SendMessage(message, recipient.Connection, deliveryMethod, sequenceChannel);
            }
        }

        private void OnReceiveReplicaMessage(NetIncomingMessage msg)
        {
            uint id = msg.ReadUInt32();

            IReplica replica;
            if (!_replicas.TryGetValue(id, out replica))
            {
                s_log.Error("received replica message for unknown replica: " + id);
                return;
            }

            IReplicaConnection connection;
            if (_net.IsServer)
            {
                connection = _connections.FirstOrDefault(c => c.Connection == msg.SenderConnection);
                if (connection == null)
                {
                    s_log.Error("Received replica message from unknown sender. ReplicaID={0}", id);
                    return;
                }
            }
            else
            {
                connection = null;
            }

            replica.OnReceiveMessage(msg, connection);
        }

        private static class MessageTypes
        {
            public const byte CreateReplica = 1;
            public const byte DestroyReplica = 2;
            public const byte UpdateReplica = 3;
            public const byte ConfirmReplica = 4;
            public const byte ReplicaMessage = 5;
        }

        private static class OrderingChannels
        {
            // The ordering channel used for replication messages.
            public const byte Replication = 1;

            // The ordering channel used for replica events.
            public const byte Event = 2;
        }
    }
}