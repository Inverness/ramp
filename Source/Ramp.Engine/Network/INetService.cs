﻿using System.Net;
using Lidgren.Network;

namespace Ramp.Network
{
    /// <summary>
    ///		Provides core client-server networking services.
    /// </summary>
    public interface INetService
    {
        event TypedEventHandler<INetService, NetConnectionEventArgs> ConnectionApproval;

        event TypedEventHandler<INetService, NetConnectionStatusChangedEventArgs> ConnectionStatusChanged;

        /// <summary>
        ///		Gets whether a network connection is open either as a client or server.
        /// </summary>
        bool Online { get; }

        /// <summary>
        ///		Gets whether the service is online and a server.
        /// </summary>
        bool IsServer { get; }

        /// <summary>
        ///		Gets whether the service is online and a client.
        /// </summary>
        bool IsClient { get; }

        /// <summary>
        ///		Gets the Lidgren NetPeer if currently online.
        /// </summary>
        NetPeer Peer { get; }

        /// <summary>
        ///		Gets the Lidgren NetServer if currently a server.
        /// </summary>
        NetServer ServerPeer { get; }

        /// <summary>
        ///		Gets the Lidgren NetClient if currently a client.
        /// </summary>
        NetClient ClientPeer { get; }

        /// <summary>
        ///		Begin listening as a server on the speciifed port.
        /// </summary>
        /// <param name="port"> The port number. </param>
        void Listen(int port);

        /// <summary>
        ///		Connect as a client to the specified server.
        /// </summary>
        /// <param name="host"> The server host. </param>
        /// <param name="port"> The port number. </param>
        /// <param name="hail"> An optional hail message to be sent to the server. </param>
        void Connect(string host, int port, NetBuffer hail = null);

        /// <summary>
        ///		Shutdown any active network connection.
        /// </summary>
        void Shutdown();

        /// <summary>
        ///     Set a handler for messages with the specified index.
        /// </summary>
        /// <param name="index"> An index. Must not be 0. </param>
        /// <param name="handler"> A message handling delegate </param>
        void SetMessageHandler(byte index, MessageHandler handler);

        /// <summary>
        ///		Create a friendly ID based on an endpoint. The same endpoint will always result in the same ID.
        /// </summary>
        /// <param name="endpoint"> The endpoint. </param>
        /// <returns> An ID. </returns>
        string CreateId(IPEndPoint endpoint);

        /// <summary>
        ///		Create an outgoing message for the specified handler. The message should be sent directly with the
        ///     desired NetPeer instance.
        /// </summary>
        /// <param name="handlerIndex"> The index of the receiving message handler. </param>
        /// <param name="capacity"> An optional initial capacity for the message. </param>
        /// <returns> The outgoing message. </returns>
        NetOutgoingMessage CreateMessage(byte handlerIndex, int? capacity = null);
    }
}