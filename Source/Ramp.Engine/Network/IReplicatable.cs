﻿namespace Ramp.Network
{
    /// <summary>
    ///     Describes an object that can be directly or indirectly replicated over a network.
    /// </summary>
    public interface IReplicatable
    {
        /// <summary>
        ///     Serialize data for this object using a delta writer.
        /// </summary>
        /// <param name="writer"> The delta writer. </param>
        /// <param name="context"> The replciation context. </param>
        void WriteDelta(DeltaWriter writer, ReplicationContext context);

        /// <summary>
        ///     Deserialize data for this object using a delta reader.
        /// </summary>
        /// <param name="reader"> The delta reader. </param>
        /// <param name="context"> The replciation context. </param>
        void ReadDelta(DeltaReader reader, ReplicationContext context);
    }
}