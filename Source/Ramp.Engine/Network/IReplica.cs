﻿using System;
using Lidgren.Network;

namespace Ramp.Network
{
    /// <summary>
    ///     An interface for an object that can be replicated over the network.
    /// </summary>
    public interface IReplica : IReplicatable
    {
        /// <summary>
        ///     The unique ID for this object. A value of 0 indicates that the object is not being tracked for replication.
        /// </summary>
        uint ReplicaId { get; }

        /// <summary>
        ///     The replica service that manages this object. Will be null if ReplicaID is 0.
        /// </summary>
        IReplicaService ReplicaService { get; }

        /// <summary>
        ///		Gets the replication rate for this object.
        /// </summary>
        TimeSpan ReplicaUpdateRate { get; }

        /// <summary>
        ///     This object's role on the current machine. This will be the value for RemoteRole after replication.
        /// </summary>
        NetRole Role { get; }

        /// <summary>
        ///     This object's role on the remote machine. This will be the value for Role after replication.
        /// </summary>
        NetRole RemoteRole { get; }

        /// <summary>
        ///     Called when the replica service begins tracking.
        /// </summary>
        /// <param name="service"> The replica service doing the tracking. </param>
        /// <param name="id"> The new replica ID. Never zero. </param>
        void OnTrack(IReplicaService service, uint id);

        /// <summary>
        ///     Called when the replica service stops tracking.
        /// </summary>
        /// <param name="service"> The replica service that previously tracked it. </param>
        void OnUntrack(IReplicaService service);

        /// <summary>
        ///     Test if this replica should be replicated to the specified connection.
        /// </summary>
        /// <remarks>
        ///     This will only be called if the RemoteRole is not None.
        /// </remarks>
        /// <param name="connection"> The destination client for the replica. </param>
        /// <returns> True if this component should be replicated. </returns>
        bool IsRelevantTo(IReplicaConnection connection);

        /// <summary>
        ///     Called when the replica service receives a message for this replica.
        /// </summary>
        /// <param name="message"> The incoming message </param>
        /// <param name="source"> The source, or null if received on the client. </param>
        void OnReceiveMessage(NetIncomingMessage message, IReplicaConnection source);
    }
}