﻿using Squid;

namespace Ramp.Rendering
{
    public static class RenderUtil
    {
        public static readonly SquidEventArgs EmptySquidEventArgs = new SquidEventArgs();

        public static void InitializeVScrollBar(ScrollBar scrollbar)
        {
            scrollbar.Orientation = Orientation.Vertical;
            scrollbar.Margin = new Margin(0, 6, 6, 6);
            scrollbar.Size = new Point(14, 10);
            scrollbar.Slider.Style = "VScrollTrack";
            scrollbar.Slider.Button.Style = "VScrollButton";
            scrollbar.ButtonUp.Style = "VScrollUp";
            scrollbar.ButtonUp.Size = new Point(10, 20);
            scrollbar.ButtonDown.Style = "VScrollUp";
            scrollbar.ButtonDown.Size = new Point(10, 20);
            scrollbar.Slider.Margin = new Margin(0, 2, 0, 2);
        }

        public static void InitializeHScrollBar(ScrollBar scrollbar)
        {
            scrollbar.Orientation = Orientation.Horizontal;
            scrollbar.Margin = new Margin(6, 0, 6, 6);
            scrollbar.Size = new Point(10, 14);
            scrollbar.Slider.Style = "HScrollTrack";
            scrollbar.Slider.Button.Style = "HScrollButton";
            scrollbar.ButtonUp.Style = "hScrollUp";
            scrollbar.ButtonUp.Size = new Point(20, 10);
            scrollbar.ButtonDown.Style = "HScrollUp";
            scrollbar.ButtonDown.Size = new Point(20, 10);
            scrollbar.Slider.Margin = new Margin(2, 0, 2, 0);
        }

        public static void InitializeScrollbars(ListBox listBox)
        {
            InitializeVScrollBar(listBox.Scrollbar);
        }

        public static void InitializeScrollbars(Panel panel)
        {
            InitializeVScrollBar(panel.VScroll);
            InitializeHScrollBar(panel.HScroll);
        }

        public static Panel NewScrollingPanel(Control parent = null)
        {
            var panel = new Panel
            {
                Parent = parent,
                Style = "TextBox",
                Dock = DockStyle.Fill,
                ClipFrame = { Margin = new Margin(8) }
            };

            InitializeScrollbars(panel);

            return panel;
        }

        public static void CenterOnDesktop(Control control)
        {
            Desktop desktop = control.Desktop;
            control.Position = new Point(desktop.Size.x / 2 - control.Size.x / 2,
                                         desktop.Size.y / 2 - control.Size.y / 2);
        }

        public static Rectangle ToSquidRectangle(Microsoft.Xna.Framework.Rectangle rect)
        {
            return new Rectangle(rect.X, rect.Y, rect.Width, rect.Height);
        }
    }
}