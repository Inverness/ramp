﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Ramp.Data;
using Squid;
using Rectangle = Squid.Rectangle;
using SquidFont = Squid.Font;
using SquidPoint = Squid.Point;

namespace Ramp.Rendering
{
    internal class SquidRenderer : ISquidRenderer
    {
        private readonly Dictionary<int, SpriteFont> _fonts = new Dictionary<int, SpriteFont>();
        private readonly Dictionary<string, int> _fontLookup = new Dictionary<string, int>();

        private readonly Dictionary<int, Texture2D> _textures = new Dictionary<int, Texture2D>();
        private readonly Dictionary<string, int> _textureLookup = new Dictionary<string, int>();

        private readonly Dictionary<string, string> _fontTypes = new Dictionary<string, string>();

        private readonly GraphicsDevice _device;
        private readonly IConfigService _config;
        private readonly RampContentManager _content;
        private readonly IntPtr _keyboardLayout;
        private readonly byte[] _keyboardState = new byte[0x100];

        private readonly Texture2D _blankTexture;
        private readonly SpriteBatch _batch;

        private readonly RasterizerState _rasterizer;
        private readonly SamplerState _sampler;

        private int _fontIndex;
        private int _textureIndex;

        public SquidRenderer(GraphicsDevice graphicsDevice, IConfigService config, RampContentManager contentManager)
        {
            Debug.Assert(graphicsDevice != null && config != null && contentManager != null);

            _device = graphicsDevice;
            _config = config;
            _content = contentManager;
            _keyboardLayout = GetKeyboardLayout(0);

            _batch = new SpriteBatch(_device);

            _blankTexture = new Texture2D(_device, 1, 1, false, SurfaceFormat.Color);
            _blankTexture.SetData(new[] { Color.White });

            _fontTypes["Debug"] = _config.Database.GetString("System.UI.DebugFont");
            _fontTypes["Normal"] = _config.Database.GetString("System.UI.NormalFont");
            _fontTypes["Large"] = _config.Database.GetString("System.UI.LargeFont");
            _fontTypes["Heading"] = _config.Database.GetString("System.UI.HeadingFont");
            _fontTypes["Title"] = _config.Database.GetString("System.UI.TitleFont");
            _fontTypes["default"] = _fontTypes["Normal"];


            _rasterizer = new RasterizerState { CullMode = CullMode.None, ScissorTestEnable = true };

            _sampler = SamplerState.LinearClamp;
        }

        public void Dispose()
        {
            _rasterizer.Dispose();
            _blankTexture.Dispose();
            _batch.Dispose();
        }

        public int GetFont(string name)
        {
            int index;
            if (_fontLookup.TryGetValue(name, out index))
                return index;

            string fontName;
            if (!_fontTypes.TryGetValue(name, out fontName))
                return -1;

            var font = _content.Load<SpriteFont>(fontName);
            index = ++_fontIndex;

            _fontLookup[name] = index;
            _fonts[index] = font;

            return index;
        }

        public SquidPoint GetTextSize(string text, int font)
        {
            if (string.IsNullOrEmpty(text))
                return new SquidPoint();

            SpriteFont f = _fonts[font];
            Vector2 size = f.MeasureString(text);
            return new SquidPoint((int) size.X, (int) size.Y);
        }

        public int GetTexture(string name)
        {
            int index;
            if (_textureLookup.TryGetValue(name, out index))
                return index;

            var texture = _content.Load<Texture2D>(name);
            index = ++_textureIndex;

            _textureLookup[name] = index;
            _textures[index] = texture;

            return index;
        }

        public SquidPoint GetTextureSize(int texture)
        {
            Texture2D t = _textures[texture];
            return new SquidPoint(t.Width, t.Height);
        }

        public bool TranslateKey(int scancode, ref char character)
        {
            // Get the windows virtual key code for the specified scan code
            uint vk = MapVirtualKeyEx((uint) scancode, 1, _keyboardLayout);
            if (vk == 0)
                return false;

            // Query the current keyboard state.
            // TODO: Make use of Squid internal keyboard state
            if (!GetKeyboardState(_keyboardState))
                return false;

            // Get the ASCII code for the specified virtual key depending on the current keyboard state.
            short asc;
            int r = ToAsciiEx(vk, (uint) scancode, _keyboardState, out asc, 0, _keyboardLayout);
            if (r != 1)
                return false;
            character = (char) asc;
            return true;
        }

        public void StartBatch()
        {
            _batch.Begin(SpriteSortMode.Deferred,
                         BlendState.NonPremultiplied,
                         _sampler,
                         DepthStencilState.None,
                         _rasterizer,
                         null,
                         Matrix.Identity);
        }

        public void EndBatch(bool final)
        {
            _batch.End();
        }

        public void DrawBox(int x, int y, int w, int h, int color)
        {
            var box = new Microsoft.Xna.Framework.Rectangle(x, y, w, h);
            _batch.Draw(_blankTexture, box, new Color { PackedValue = (uint) color });
        }

        public void DrawText(string text, int x, int y, int font, int color)
        {
            SpriteFont f;
            if (!_fonts.TryGetValue(font, out f))
                return;

            _batch.DrawString(f, text, new Vector2(x, y), new Color { PackedValue = (uint) color });
        }

        public void DrawTexture(int texture, int x, int y, int w, int h, Rectangle source, int color)
        {
            Texture2D tex;
            if (!_textures.TryGetValue(texture, out tex))
                return;

            var dest = new Microsoft.Xna.Framework.Rectangle(x, y, w, h);
            var src = new Microsoft.Xna.Framework.Rectangle(source.Left, source.Top, source.Width, source.Height);

            _batch.Draw(tex, dest, src, new Color { PackedValue = (uint) color }, 0, Vector2.Zero, SpriteEffects.None, 0);
        }

        public void Scissor(int x, int y, int width, int height)
        {
            _device.ScissorRectangle = new Microsoft.Xna.Framework.Rectangle(x, y, width, height);
        }

        public static uint GetScancodeFromVirtualKey(uint key)
        {
            return MapVirtualKeyEx(key, 0, IntPtr.Zero);
        }

        [DllImport("user32.dll")]
        private static extern uint MapVirtualKeyEx(uint uCode, uint uMapType, IntPtr dwhkl);

        [DllImport("user32.dll")]
        private static extern int ToAsciiEx(uint wVirtKey, uint wScanCode, byte[] lpKeyState, out short lpChar,
                                            uint uFlags,
                                            IntPtr dwhkl);

        [DllImport("user32.dll")]
        private static extern IntPtr GetKeyboardLayout(int dwLayout);

        [DllImport("user32.dll")]
        private static extern bool GetKeyboardState(byte[] lpKeyState);
    }
}