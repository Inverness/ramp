using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Windows.Forms;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Ramp.Data;
using Ramp.Input;
using Ramp.Session;
using Squid;
using KeyEventArgs = System.Windows.Forms.KeyEventArgs;
using Keys = System.Windows.Forms.Keys;
using MouseEventArgs = System.Windows.Forms.MouseEventArgs;
using Point = Squid.Point;
using SquidKeys = Squid.Keys;

namespace Ramp.Rendering
{
    /// <summary>
    ///     Manages the normal and debug UI for the game.
    /// </summary>
    public sealed partial class UIManager : DrawableGameComponent, IUIService
    {
        // Special keys not handled by MapVirtualKeyEx()

        private const uint MaxKeyChanges = 2000;
        private const double KeyRepeatDelay = 250;
        private const float UIEpsilon = 1.0f / 32.0f;

        private static readonly TimeSpan s_debugTextUpdateRate = TimeSpan.FromSeconds(0.1);

        private static readonly Dictionary<Keys, SquidKeys> s_specialKeys = new Dictionary<Keys, SquidKeys>
        {
            { Keys.Delete, SquidKeys.DELETE },
            { Keys.Home, SquidKeys.HOME },
            { Keys.End, SquidKeys.END },
            { Keys.Insert, SquidKeys.INSERT },
            { Keys.Up, SquidKeys.UP },
            { Keys.Left, SquidKeys.LEFT },
            { Keys.Down, SquidKeys.DOWN },
            { Keys.Right, SquidKeys.RIGHT }
        };

        private Texture2D _singlePixelTexture;

        private readonly IConfigService _config;
        private readonly IInputService _input;
        private readonly ISessionService _session;
        private readonly ILevelRenderer _levelRenderer;
        private readonly RampContentManager _content;

        private readonly StringBuilder _sb = new StringBuilder(200, 1000);
        private TimeSpan _currentTime;
        private ulong _frameCount;
        private ulong _lastFrameCount;
        private TimeSpan _fpsElapsedTime;
        private TimeSpan _debugTextElapsedTime;
        private double _fps;
        private StringBuilder _debugText = new StringBuilder();

        private bool _drawing;
        private SpriteBatch _spriteBatch;
        private SpriteFont _debugFont;
        private SpriteFont _normalFont;
        private SpriteFont _largeFont;
        private SpriteFont _headingFont;
        private SpriteFont _titleFont;

        private SquidRenderer _renderer;
        private DefaultDesktop _desktop;
        //private Factory _dwFactory;
        //private TextFormat _dwDialogTextFormat;

        private readonly LinkedList<string> _messages = new LinkedList<string>();
        private string _currentMessage = string.Empty;
        private double _currentMessageTime;
        private Vector2 _currentMessageSize;

        //private HealthBehavior _playerHealthBehavior;

        private int _mouseX;
        private int _mouseY;
        private int _mouseWheel;
        private bool _mouseLeft;
        private bool _mouseRight;
        private int _lastKey;
        private TimeSpan _nextKeyTime;
        private readonly List<KeyData> _keyChanges = new List<KeyData>();

        public UIManager(Game game)
            : base(game)
        {
            _config = game.Services.GetRequiredService<IConfigService>();
            _input = game.Services.GetRequiredService<IInputService>();
            _session = game.Services.GetRequiredService<ISessionService>();
            _levelRenderer = game.Services.GetService<ILevelRenderer>();
            _content = new RampContentManager(game.Services, "UIManager");

            InputEnabled = true;
            DebugTextVisible = true;
            PlayerHealthVisible = true;
            EventMessageVisible = true;

            game.Services.AddService(typeof(IUIService), this);
        }

        public ConsoleWindow Console { get; private set; }

        public Desktop Desktop => _desktop;

        public bool InputEnabled { get; set; }

        public bool DebugTextVisible { get; set; }

        public bool PlayerHealthVisible { get; set; }

        public bool EventMessageVisible { get; set; }

        public bool Focused => _desktop.FocusedControl != null;

        public TimeSpan LastUpdateTime { get; set; }

        public TimeSpan LastFrameTime { get; set; }

        public override void Initialize()
        {
            Debug.Assert(_session.State == SessionState.Idle);

            _input.AddHandler(this);
            _session.StateChanged += OnSessionStateChanged;

            InitializeControls();

            _desktop = new DefaultDesktop();

            Console = new ConsoleWindow
            {
                Parent = _desktop,
                Position = new Point(10, 10),
                Size = new Point(300, 300),
                Visible = false
            };

            base.Initialize();

            // We want to cache relevant player components, so listen for a change.
            //_playerService.LocalPlayer.ActorChanged += OnPlayerActorChanged;
            //AddEventMessage("Test");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                UnloadContent();
            }
            base.Dispose(disposing);
        }

        protected override void LoadContent()
        {
            GuiHost.Renderer = _renderer = new SquidRenderer(GraphicsDevice, _config, _content);

            _debugFont = _content.Load<SpriteFont>(_config.Database.GetString("System.UI.DebugFont"));
            _normalFont = _content.Load<SpriteFont>(_config.Database.GetString("System.UI.NormalFont"));
            _largeFont = _content.Load<SpriteFont>(_config.Database.GetString("System.UI.LargeFont"));
            _headingFont = _content.Load<SpriteFont>(_config.Database.GetString("System.UI.HeadingFont"));
            _titleFont = _content.Load<SpriteFont>(_config.Database.GetString("System.UI.TitleFont"));

            _borderTexture = _content.Load<Texture2D>("Textures/UIBorder");
            _spriteBatch = new SpriteBatch(GraphicsDevice);

            // This must have a normalized pixel format or the sprite batcher will choke on it
            _singlePixelTexture = new Texture2D(GraphicsDevice, 1, 1, false, SurfaceFormat.Color);
            _singlePixelTexture.SetData(new[] { Color.White });
        }

        protected override void UnloadContent()
        {
            _renderer?.Dispose();
            GuiHost.Renderer = null;
            _spriteBatch?.Dispose();
            _content?.Unload();
        }

        public override void Update(GameTime gameTime)
        {
            //if (_playerHealthBehavior == null && _session.LocalPlayerActor != null)
            //	_playerHealthBehavior = _session.LocalPlayerActor.GetComponent<HealthBehavior>();

            // Send input to the Squid GUI
            GuiHost.SetMouse(_mouseX, _mouseY, _mouseWheel);
            GuiHost.SetButtons(_mouseLeft, _mouseRight);
            if (_keyChanges.Count > 0)
            {
                KeyData[] data = _keyChanges.ToArray();
                _keyChanges.Clear();
                foreach (KeyData kd in data)
                {
                    if (kd.Pressed)
                    {
                        _keyChanges.Add(new KeyData { Pressed = false, Released = true, Scancode = kd.Scancode });
                    }
                }
                GuiHost.SetKeyboard(data);
            }
            GuiHost.TimeElapsed = (float) gameTime.ElapsedGameTime.TotalMilliseconds;

            // Update the desktop state
            _desktop.Update();

            // Tell the menu its free to handle input
            //if (ActiveMenu != -1)
            //    ProcessMenuInput(gameTime);

            // Update the event message
            if (_currentMessageTime <= 0)
            {
                if (_messages.Count > 0)
                {
                    _currentMessage = _messages.First.Value;
                    _currentMessageSize = _headingFont.MeasureString(_currentMessage);
                    _messages.RemoveFirst();
                    _currentMessageTime = 3;
                }
                else
                    _currentMessage = string.Empty;
            }
            else
                _currentMessageTime = Math.Max(_currentMessageTime - gameTime.ElapsedGameTime.TotalSeconds, 0);
        }

        public override void Draw(GameTime gameTime)
        {
            if (!Enabled)
                return;

            _frameCount++;
            _currentTime = gameTime.TotalGameTime;

            // Update FPS value
            _fpsElapsedTime += gameTime.ElapsedGameTime;
            if (_fpsElapsedTime >= TimeSpan.FromSeconds(1))
            {
                _fpsElapsedTime -= TimeSpan.FromSeconds(1);

                _fps = _frameCount - _lastFrameCount;
                _lastFrameCount = _frameCount;
            }

            // Update debug text
            _debugTextElapsedTime += gameTime.ElapsedGameTime;
            if (_debugTextElapsedTime >= s_debugTextUpdateRate)
            {
                _debugTextElapsedTime -= s_debugTextUpdateRate;

                _debugText.Clear();

                _debugText.Append("FPS: ").Append(_fps).AppendLine().
                           Append("UT: ").Append(LastUpdateTime.TotalMilliseconds).AppendLine();

                if (_levelRenderer != null)
                {
                    _debugText.Append("RT: ").Append(_levelRenderer.LastRenderTime).AppendLine().
                        Append("SRT: ").Append(_levelRenderer.LastSpriteRenderTime).AppendLine();
                }

                _debugText.Append("FT: ").Append(LastFrameTime.TotalMilliseconds).AppendLine().
                           Append("M: ").Append(GC.GetTotalMemory(false)).Append(" B");
            }

            // Render UI elements
            _spriteBatch.Begin(SpriteSortMode.Deferred,
                               GraphicsDevice.BlendState = BlendState.NonPremultiplied,
                               GraphicsDevice.SamplerStates[0] = SamplerState.PointClamp,
                               GraphicsDevice.DepthStencilState,
                               GraphicsDevice.RasterizerState = RasterizerState.CullNone);
            _drawing = true;

            var vpWidth = GraphicsDevice.Viewport.Width;
            var vpHeight = GraphicsDevice.Viewport.Height;

            // Messages are queued and displayed in one of the corners of the screen one at a time for a few seconds
            // to inform the player about various events.
            // Support for an accompanying icon might be added later.
            if (EventMessageVisible && _currentMessage != string.Empty)
            {
                var pos = new Vector2(10, vpHeight - _currentMessageSize.Y - 10);
                _spriteBatch.DrawString(_headingFont, _currentMessage, pos, Color.White, 0, default(Vector2), 1,
                                        SpriteEffects.None, 0);
            }

            // Draw the FPS counter and other debug information
            if (DebugTextVisible)
                _spriteBatch.DrawString(_debugFont, _debugText, new Vector2(6, 6), Color.White);

            // Draw the basic HUD
            //if (PlayerHealthVisible && _playerHealthBehavior != null)
            //{
            //	CompositeValue health = _playerHealthBehavior.Health;

            //	// Debug text
            //	//_sb.Append("HP: ").Append(health.Value).Append("/").Append(health.UndamagedValue);
            //	//_spriteBatch.DrawString(_debugTextFont, _sb.ToString(), new Vector2(6, 38), Color.White);
            //	//_sb.Clear();

            //	// Normal health bar
            //	float healthPercent = (float)health.Value / health.UndamagedValue;
            //	Color color;
            //	if (healthPercent < 0.2)
            //		color = Color.DarkRed;
            //	else if (healthPercent < 0.5)
            //		color = Color.DarkOrange;
            //	else
            //		color = Color.DarkGreen;
            //	DrawProgressBar(new Vector2(vpWidth - 16 * 16 - 8, 8), 16, healthPercent, color);
            //}

            // Draw the Squid destkop
            _desktop.Size = new Point(vpWidth, vpHeight);
            _desktop.Draw();

            //if (MenuVisible)
            //	DrawActiveMenu();

            //string wrapped = WrapText("test 123 super awesome duper cake", _textFont, 100);

            //DrawProgressBar(new Vector2(vpWidth - 16 * 10 - 8, 48), 10, 1, new Color(96, 96, 255, 128));

            //_spriteBatch.Draw(_borderTexture, new Vector2(64, 64), Color.White); 
            //DrawBorder(new Rectangle(500, 500, 128, 128), new Color(255, 255, 255, 255));
            //DrawWindow("Test", new Rectangle(500, 500, 128, 128), new Color(255, 255, 255, 255));
            //DrawSmallBorder(new Rectangle(500, 450, 128, 32), Color.White);

            _drawing = false;
            _spriteBatch.End();
        }

        public void AddEventMessage(string text)
        {
            _messages.AddLast(text);
        }

        public void DrawLoadingScreen()
        {
            if (_drawing)
                throw new InvalidOperationException();
            Vector2 size = _debugFont.MeasureString("Loading...");
            var pos = new Vector2(GraphicsDevice.Viewport.Width / 2 - size.X / 2,
                                  GraphicsDevice.Viewport.Height / 2 - size.Y / 2);
            _spriteBatch.Begin();
            _spriteBatch.DrawString(_debugFont, "Loading...", pos, Color.White);
            _spriteBatch.End();
        }

        public static string WrapText(string text, SpriteFont font, int width)
        {
            var result = new StringBuilder();
            var line = new StringBuilder();

            int begin = 0, end = 0, lastAppendPos = 0, wordCount = 0;

            for (int i = 1; i < text.Length; i++)
            {
                // find first empty space or last character of the string
                if (text[i] != ' ' && i != text.Length - 1)
                    continue;
                end = i + 1;

                // Insert the next word into the line
                lastAppendPos = line.Length;
                line.Append(text, begin, end - begin);
                wordCount++;

                // Measure the line size, if it is too large, remove the last
                // word and insert the undersized line into the result
                Vector2 lineSize = font.MeasureString(line);
                if (lineSize.X >= width && wordCount > 1)
                {
                    line.Remove(lastAppendPos, end - begin);
                    result.Append(line).Append('\n');
                    line.Clear();
                    line.Append(text, begin, end - begin);
                    wordCount = 0;
                }

                begin = end;
            }

            result.Append(line);

            return result.ToString();
        }

        public void OnInputEvent(InputEventType type, EventArgs e)
        {
            MouseEventArgs em;
            KeyEventArgs ek;
            switch (type)
            {
                case InputEventType.MouseDown:
                    em = (MouseEventArgs) e;
                    switch (em.Button)
                    {
                        case MouseButtons.Left:
                            _mouseLeft = true;
                            break;
                        case MouseButtons.Right:
                            _mouseRight = true;
                            break;
                    }
                    break;
                case InputEventType.MouseUp:
                    em = (MouseEventArgs) e;
                    switch (em.Button)
                    {
                        case MouseButtons.Left:
                            _mouseLeft = false;
                            break;
                        case MouseButtons.Right:
                            _mouseRight = false;
                            break;
                    }
                    break;
                case InputEventType.MouseMove:
                    em = (MouseEventArgs) e;
                    _mouseX = em.X;
                    _mouseY = em.Y;
                    break;
                case InputEventType.MouseWheel:
                    em = (MouseEventArgs) e;
                    _mouseWheel = em.Delta;
                    break;
                case InputEventType.KeyDown:
                    ek = (KeyEventArgs) e;
                    bool repeat = ek.KeyValue == _lastKey && _currentTime <= _nextKeyTime;

                    if (!repeat && _keyChanges.Count < MaxKeyChanges)
                    {
                        _keyChanges.Add(new KeyData
                        {
                            Pressed = true,
                            Released = false,
                            Scancode = GetScancode(ek.KeyCode)
                        });
                        _lastKey = ek.KeyValue;
                        _nextKeyTime = _currentTime + TimeSpan.FromMilliseconds(KeyRepeatDelay);
                    }

                    ek.Handled = Focused;
                    break;
                case InputEventType.KeyUp:
                    ek = (KeyEventArgs) e;
                    if (_keyChanges.Count < MaxKeyChanges)
                    {
                        _keyChanges.Add(new KeyData
                        {
                            Pressed = false,
                            Released = true,
                            Scancode = GetScancode(ek.KeyCode)
                        });
                        _lastKey = -1;
                    }

                    ek.Handled = Focused;
                    break;
            }
        }

        // Translates a WinForms virtual key code to a Squid scancode
        private static int GetScancode(Keys key)
        {
            SquidKeys skey;
            if (!s_specialKeys.TryGetValue(key, out skey))
                skey = (SquidKeys) SquidRenderer.GetScancodeFromVirtualKey((uint) key);
            return (int) skey;
        }

        private void OnSessionStateChanged(ISessionService sender, EventArgs e)
        {
            if (_session.State == SessionState.Idle)
            {
                _input.AddHandler(this);
            }
            else
            {
                _input.RemoveHandler(this);
            }
        }
    }
}