﻿using Squid;

namespace Ramp.Rendering
{
    public class DefaultDesktop : Desktop
    {
        public const string ItemStyleName = "Item";
        public const string TextBoxStyleName = "TextBox";
        public const string ButtonStyleName = "Button";
        public const string WindowStyleName = "Window";
        public const string FrameStyleName = "Frame";
        public const string TooltipStyleName = "Tooltip";
        public const string DebugTextStyleName = "DebugText";
        public const string HeadingTextStyleName = "HeadingText";
        public const string LargeTextStyleName = "LargeText";
        public const string TitleTextStyleName = "TitleText";

        public DefaultDesktop()
        {
            var skin = new Skin();

            #region Cursors

            var cursorSize = new Point(32, 32);
            Point halfSize = cursorSize / 2;

            skin.Cursors.Add(Cursors.Default,
                             new Cursor { Texture = "Textures/UI/Cursor/Arrow", Size = cursorSize, HotSpot = Point.Zero });
            skin.Cursors.Add(Cursors.Link,
                             new Cursor { Texture = "Textures/UI/Cursor/Link", Size = cursorSize, HotSpot = Point.Zero });
            skin.Cursors.Add(Cursors.Move,
                             new Cursor { Texture = "Textures/UI/Cursor/Move", Size = cursorSize, HotSpot = halfSize });
            skin.Cursors.Add(Cursors.Select,
                             new Cursor { Texture = "Textures/UI/Cursor/Select", Size = cursorSize, HotSpot = halfSize });
            skin.Cursors.Add(Cursors.SizeNS,
                             new Cursor { Texture = "Textures/UI/Cursor/SizeNS", Size = cursorSize, HotSpot = halfSize });
            skin.Cursors.Add(Cursors.SizeWE,
                             new Cursor { Texture = "Textures/UI/Cursor/SizeWE", Size = cursorSize, HotSpot = halfSize });
            skin.Cursors.Add(Cursors.HSplit,
                             new Cursor { Texture = "Textures/UI/Cursor/SizeNS", Size = cursorSize, HotSpot = halfSize });
            skin.Cursors.Add(Cursors.VSplit,
                             new Cursor { Texture = "Textures/UI/Cursor/SizeWE", Size = cursorSize, HotSpot = halfSize });
            skin.Cursors.Add(Cursors.SizeNESW,
                             new Cursor { Texture = "Textures/UI/Cursor/SizeNESW", Size = cursorSize, HotSpot = halfSize });
            skin.Cursors.Add(Cursors.SizeNWSE,
                             new Cursor { Texture = "Textures/UI/Cursor/SizeNWSE", Size = cursorSize, HotSpot = halfSize });

            #endregion

            #region Styles

            var baseStyle = new ControlStyle
            {
                Tiling = TextureMode.Grid,
                Grid = new Margin(3),
                Texture = "Textures/UI/Default/ButtonHot",
                Default = { Texture = "Textures/UI/Default/ButtonDefault" },
                Pressed = { Texture = "Textures/UI/Default/ButtonDown" },
                SelectedPressed = { Texture = "Textures/UI/Default/ButtonDown" },
                Focused = { Texture = "Textures/UI/Default/ButtonDown" },
                SelectedFocused = { Texture = "Textures/UI/Default/ButtonDown" },
                Selected = { Texture = "Textures/UI/Default/ButtonDown" },
                SelectedHot = { Texture = "Textures/UI/Default/ButtonDown" }
            };

            var itemStyle = new ControlStyle(baseStyle)
            {
                TextPadding = new Margin(10, 0, 0, 0),
                TextAlign = Alignment.MiddleLeft
            };

            var buttonStyle = new ControlStyle(baseStyle)
            {
                TextPadding = new Margin(0),
                TextAlign = Alignment.MiddleCenter
            };

            var tooltipStyle = new ControlStyle(buttonStyle)
            {
                TextPadding = new Margin(8),
                TextAlign = Alignment.TopLeft
            };

            var inputStyle = new ControlStyle
            {
                Texture = "Textures/UI/Default/InputDefault",
                Hot = { Texture = "Textures/UI/Default/InputFocused" },
                Focused = { Texture = "Textures/UI/Default/InputFocused", Tint = ColorInt.RGBA(1, 0, 0, 1) },
                TextPadding = new Margin(8),
                Tiling = TextureMode.Grid,
                Grid = new Margin(3)
            };

            var windowStyle = new ControlStyle
            {
                Tiling = TextureMode.Grid,
                Grid = new Margin(9),
                Texture = "Textures/UI/Default/Window"
            };

            var frameStyle = new ControlStyle
            {
                Tiling = TextureMode.Grid,
                Grid = new Margin(4),
                Texture = "Textures/UI/Default/Frame",
                TextPadding = new Margin(8)
            };

            var vscrollTrackStyle = new ControlStyle
            {
                Tiling = TextureMode.Grid,
                Grid = new Margin(3),
                Texture = "Textures/UI/Default/VScrollTrack"
            };

            var vscrollButtonStyle = new ControlStyle
            {
                Tiling = TextureMode.Grid,
                Grid = new Margin(3),
                Texture = "Textures/UI/Default/VScrollButton",
                Hot = { Texture = "Textures/UI/Default/VScrollButtonHot" },
                Pressed = { Texture = "Textures/UI/Default/VScrollButtonDown" }
            };

            var vscrollUp = new ControlStyle
            {
                Default = { Texture = "Textures/UI/Default/VScrollUpDefault" },
                Hot = { Texture = "Textures/UI/Default/VScrollUpHot" },
                Pressed = { Texture = "Textures/UI/Default/VScrollUpDown" },
                Focused = { Texture = "Textures/UI/Default/VScrollUpHot" }
            };

            var hscrollTrackStyle = new ControlStyle
            {
                Tiling = TextureMode.Grid,
                Grid = new Margin(3),
                Texture = "Textures/UI/Default/HScrollTrack"
            };

            var hscrollButtonStyle = new ControlStyle
            {
                Tiling = TextureMode.Grid,
                Grid = new Margin(3),
                Texture = "Textures/UI/Default/HScrollButton",
                Hot = { Texture = "Textures/UI/Default/HScrollButtonHot" },
                Pressed = { Texture = "Textures/UI/Default/HScrollButtonDown" }
            };

            var hscrollUp = new ControlStyle
            {
                Default = { Texture = "Textures/UI/Default/HScrollUpDefault" },
                Hot = { Texture = "Textures/UI/Default/HScrollUpHot" },
                Pressed = { Texture = "Textures/UI/Default/HScrollUpDown" },
                Focused = { Texture = "Textures/UI/Default/HScrollUpHot" }
            };

            var checkButtonStyle = new ControlStyle
            {
                Default = { Texture = "Textures/UI/Default/CheckboxDefault" },
                Hot = { Texture = "Textures/UI/Default/CheckboxHot" },
                Pressed = { Texture = "Textures/UI/Default/CheckboxDown" },
                Checked = { Texture = "Textures/UI/Default/CheckboxChecked" },
                CheckedFocused = { Texture = "Textures/UI/Default/CheckboxCheckedHot" },
                CheckedHot = { Texture = "Textures/UI/Default/CheckboxCheckedHot" },
                CheckedPressed = { Texture = "Textures/UI/Default/CheckboxDown" }
            };

            var comboLabelStyle = new ControlStyle
            {
                TextPadding = new Margin(10, 0, 0, 0),
                Default = { Texture = "Textures/UI/Default/ComboDefault" },
                Hot = { Texture = "Textures/UI/Default/ComboHot" },
                Pressed = { Texture = "Textures/UI/Default/ComboDown" },
                Focused = { Texture = "Textures/UI/Default/ComboHot" },
                Tiling = TextureMode.Grid,
                Grid = new Margin(3, 0, 0, 0)
            };

            var comboButtonStyle = new ControlStyle
            {
                Default = { Texture = "Textures/UI/Default/ComboButtonDefault" },
                Hot = { Texture = "Textures/UI/Default/ComboButtonHot" },
                Pressed = { Texture = "Textures/UI/Default/ComboButtonDown" },
                Focused = { Texture = "Textures/UI/Default/ComboButtonHot" }
            };

            var labelStyle = new ControlStyle
            {
                TextAlign = Alignment.TopLeft,
                TextPadding = new Margin(8)
            };

            skin.Styles.Add("Item", itemStyle);
            skin.Styles.Add("TextBox", inputStyle);
            skin.Styles.Add("Button", buttonStyle);
            skin.Styles.Add("Window", windowStyle);
            skin.Styles.Add("Frame", frameStyle);
            skin.Styles.Add("CheckBox", checkButtonStyle);
            skin.Styles.Add("ComboLabel", comboLabelStyle);
            skin.Styles.Add("ComboButton", comboButtonStyle);
            skin.Styles.Add("VScrollTrack", vscrollTrackStyle);
            skin.Styles.Add("VScrollButton", vscrollButtonStyle);
            skin.Styles.Add("VScrollUp", vscrollUp);
            skin.Styles.Add("HScrollTrack", hscrollTrackStyle);
            skin.Styles.Add("HScrollButton", hscrollButtonStyle);
            skin.Styles.Add("HScrollUp", hscrollUp);
            skin.Styles.Add("Multiline", labelStyle);
            skin.Styles.Add("Tooltip", tooltipStyle);

            skin.Styles.Add("DebugText", new ControlStyle { Font = "Debug" });
            skin.Styles.Add("HeadingText", new ControlStyle { Font = "Heading" });
            skin.Styles.Add("LargeText", new ControlStyle { Font = "Large" });
            skin.Styles.Add("TitleText", new ControlStyle { Font = "Title" });

            GuiHost.SetSkin(skin);

            #endregion

            //var window5 = new FullWindow
            //{
            //	Parent = this,
            //	Size = new Point(440, 340),
            //	Position = new Point(30, 40),
            //	Resizable = true,
            //	Title = "Panel, TextBox",
            //	ContentStyle = "TextBox",
            //	//Panel =
            //	//{
            //	//	VScroll =
            //	//	{
            //	//		Margin = new Margin(0, 8, 8, 8),
            //	//		Size = new Point(14, 10),
            //	//		Slider = { Style = "VScrollTrack", Button = { Style = "VScrollButton" }, Margin = new Margin(0, 2, 0, 2) },
            //	//		ButtonUp = { Style = "VScrollUp", Size = new Point(10, 20) },
            //	//		ButtonDown = { Style = "VScrollUp", Size = new Point(10, 20) }
            //	//	},
            //	//	HScroll =
            //	//	{
            //	//		Margin = new Margin(8, 0, 8, 8),
            //	//		Size = new Point(10, 14),
            //	//		Slider = { Style = "HScrollTrack", Button = { Style = "HScrollButton" }, Margin = new Margin(2, 0, 2, 0) },
            //	//		ButtonUp = { Style = "HScrollUp", Size = new Point(20, 10) },
            //	//		ButtonDown = { Style = "HScrollUp", Size = new Point(20, 10) }
            //	//	}
            //	//}
            //};

            //for (int i = 0; i < 10; i++)
            //{
            //	var label = new Label
            //	{
            //		Text = "label control:",
            //		Size = new Point(100, 35),
            //		Position = new Point(10, 10 + 45 * i)
            //	};
            //	window5.Panel.Content.Controls.Add(label);

            //	var txt = new TextBox
            //	{
            //		Text = "lorem ipsum",
            //		Size = new Point(222, 35),
            //		Position = new Point(110, 10 + 45 * i),
            //		Style = "TextBox",
            //		AllowDrop = true,
            //		TabIndex = 1 + i
            //	};
            //	txt.OnDragDrop += OnTextBoxDragDrop;
            //	txt.OnGotFocus += OnTextBoxGotFocus;
            //	window5.Panel.Content.Controls.Add(txt);
            //}

            //window5.Visible = false;
        }

        //private void OnTextBoxGotFocus(Control sender)
        //{
        //	var txt = sender as TextBox;
        //	txt.Select(0, txt.Text.Length - 1);
        //}

        //private void OnTextBoxDragDrop(Control sender, DragDropEventArgs e)
        //{
        //	((TextBox) sender).Text = ((Button) e.DraggedControl).Text;
        //}
    }
}