﻿using System;
using System.Collections.Generic;
using System.Linq;
using Squid;

namespace Ramp.Rendering
{
    public class MainMenu : Control, IControlContainer
    {
        private const int ChoicesOffsetX = 64;
        private const int ChoicesOffsetY = 256;
        private const int ChoiceSpacing = 32;
        private const int TitleOffsetX = 64;
        private const int TitleOffsetY = 64;

        private readonly Label _titleLabel;
        private string[] _choices;
        private Label[] _choiceLabels = new Label[0];

        public MainMenu()
        {
            Controls = new ControlCollection(this);

            _titleLabel = new Label
            {
                Parent = this,
                Style = "TitleText",
                Position = new Point(TitleOffsetX, TitleOffsetY),
                AutoSize = AutoSize.HorizontalVertical
            };
        }

        public event TypedEventHandler<MainMenu, MainMenuChoiceSelectedEventArgs> ChoiceSelected;

        public ControlCollection Controls { get; set; }

        public IReadOnlyList<string> Choices => _choices;

        public string TitleText
        {
            get { return _titleLabel.Text; }

            set { _titleLabel.Text = value; }
        }

        public void SetChoices(params string[] items)
        {
            SetChoices((IEnumerable<string>) items);
        }

        public void SetChoices(IEnumerable<string> items)
        {
            _choices = items.ToArray();
            UpdateChoiceLabels();
        }

        protected void UpdateChoiceLabels()
        {
            Array.ForEach(_choiceLabels, l =>
            {
                l.Parent = null;
                l.MouseDown -= OnItemMouseDown;
            });

            var labels = new List<Label>();
            int y = ChoicesOffsetY;
            int index = 0;

            foreach (string choice in _choices)
            {
                var label = new Label
                {
                    Parent = this,
                    Style = "HeadingText",
                    Position = new Point(ChoicesOffsetX, y),
                    AutoSize = AutoSize.HorizontalVertical,
                    AllowFocus = true,
                    Text = choice,
                    Tag = index++
                };

                label.MouseDown += OnItemMouseDown;

                labels.Add(label);

                y += ChoiceSpacing;
            }

            _choiceLabels = labels.ToArray();
        }

        protected virtual void OnChoiceSelected(MainMenuChoiceSelectedEventArgs e)
        {
            ChoiceSelected?.Invoke(this, e);
        }

        private void OnItemMouseDown(Control sender, MouseEventArgs e)
        {
            var item = (Label) sender;
            OnChoiceSelected(new MainMenuChoiceSelectedEventArgs(item.Text, (int) item.Tag));
        }
    }
}