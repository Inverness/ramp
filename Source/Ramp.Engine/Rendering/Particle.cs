﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Ramp.Rendering
{
    /// <summary>
    ///     A particle in a particle system.
    /// </summary>
    public class Particle
    {
        internal int Index = -1;

        internal Particle()
        {
        }

        /// <summary>
        ///     Gets or sets the angular velocity in radians per second.
        /// </summary>
        public float AngularVelocity { get; set; }

        /// <summary>
        ///     Gets or sets the color of the particle.
        /// </summary>
        public Color Color { get; set; }

        /// <summary>
        ///     Gets whether this particle has died and been removed from its particle system.
        /// </summary>
        public bool Dead => Index == -1;

        /// <summary>
        ///     Gets or sets the elapsed time since the particle was emitted.
        /// </summary>
        public TimeSpan ElapsedTime { get; set; }

        /// <summary>
        ///     Gets or sets the total lifetime.
        /// </summary>
        public TimeSpan Lifetime { get; set; }

        /// <summary>
        ///     Gets or sets the location in world coordinates relative to the system's origin.
        /// </summary>
        public Vector2 Position { get; set; }

        /// <summary>
        ///     Gets or sets the rotation in radians.
        /// </summary>
        public float Rotation { get; set; }

        /// <summary>
        ///     Gets or sets the size in world coordinates.
        /// </summary>
        public Vector2 Size { get; set; }

        /// <summary>
        ///     Gets the texture used to render this particle.
        /// </summary>
        public Texture2D Texture { get; internal set; }

        /// <summary>
        ///     Gets or sets the velocity in world coordinates per second.
        /// </summary>
        public Vector2 Velocity { get; set; }
    }
}