using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Xna.Framework;
using Newtonsoft.Json;

namespace Ramp.Rendering
{
    /// <summary>
    ///     Represents a sprite animation.
    /// </summary>
    public sealed class Animation
    {
        /// <summary>
        ///     True if the animation should loop when the last frame in a set is finished.
        /// </summary>
        public bool Loop { get; set; }

        /// <summary>
        ///     True if frame sets names match each direction name and will be used to allow different animations per
        ///     direction.
        /// </summary>
        public bool Directional { get; set; }

        /// <summary>
        ///     The animation's sprite types.
        /// </summary>
        public Dictionary<string, AnimationSpriteType> SpriteTypes { get; } = new Dictionary<string, AnimationSpriteType>();

        /// <summary>
        ///     The frames in this animation, keyed by frame list.
        /// </summary>
        /// <remarks>
        ///     Directional animations should have one frame list per direction name.
        /// </remarks>
        public Dictionary<string, List<AnimationFrame>> Frames { get; } = new Dictionary<string, List<AnimationFrame>>();

        public void Save(string path)
        {
            if (path == null)
                throw new ArgumentNullException(nameof(path));

            using (var stream = new FileStream(path, FileMode.Create, FileAccess.Write, FileShare.None))
                Save(stream);
        }

        public void Save(Stream stream)
        {
            if (stream == null)
                throw new ArgumentNullException(nameof(stream));

            using (var streamWriter = new StreamWriter(stream))
            {
                var jsonTextWriter = new JsonTextWriter(streamWriter);
                var serializer = new JsonSerializer { Formatting = Formatting.Indented };
                serializer.Serialize(jsonTextWriter, this);
            }
        }

        /// <summary>
        ///     Creates an animation from the specified JSON file.
        /// </summary>
        /// <param name="path"> Path to a JSON document. </param>
        /// <returns> A new animation constructed using the JSON document. </returns>
        public static Animation Load(string path)
        {
            if (path == null)
                throw new ArgumentNullException(nameof(path));

            using (var stream = new FileStream(path, FileMode.Open, FileAccess.Read))
                return Load(stream);
        }

        /// <summary>
        ///     Creates an animation from the specified stream providing a JSON document.
        /// </summary>
        /// <param name="stream"> A stream providing a JSON document. </param>
        /// <returns> A new animation constructed using the JSON document. </returns>
        public static Animation Load(Stream stream)
        {
            if (stream == null)
                throw new ArgumentNullException(nameof(stream));

            return JsonSerializer.CreateDefault().Deserialize<Animation>(new JsonTextReader(new StreamReader(stream)));
        }
    }

    /// <summary>
    ///     A sprite that can be placed in the animation's frames
    /// </summary>
    public sealed class AnimationSpriteType
    {
        /// <summary>
        ///     The name of the texture used for this sprite.
        /// </summary>
        public string TextureName { get; set; }

        /// <summary>
        ///     The area of the texture used for this sprite.
        /// </summary>
        public Rectangle TextureRect { get; set; }
    }

    /// <summary>
    ///     A single frame of an animation.
    /// </summary>
    public sealed class AnimationFrame
    {
        /// <summary>
        ///     The durection of this frame in seconds.
        /// </summary>
        public double Duration { get; set; }

        /// <summary>
        ///     The sprites displayed during this frame.
        /// </summary>
        public List<AnimationSprite> Sprites { get; } = new List<AnimationSprite>();

        /// <summary>
        ///     Events that are dispatched during this frame.
        /// </summary>
        public List<string> Events { get; } = new List<string>();
    }

    /// <summary>
    ///     An instance of a sprite placed in an animation frame.
    /// </summary>
    public sealed class AnimationSprite
    {
        /// <summary>
        ///     Gets the name of the sprite type.
        /// </summary>
        public string SpriteType { get; set; }

        /// <summary>
        ///     The position of the sprite in the frame.
        /// </summary>
        public Point Position { get; set; }

        /// <summary>
        ///     The color of the sprite.
        /// </summary>
        public Color Color { get; set; }

        /// <summary>
        ///     The rotation of the sprite in radians.
        /// </summary>
        public float Rotation { get; set; }
    }
}