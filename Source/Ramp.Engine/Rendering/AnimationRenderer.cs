using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using NLog;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Ramp.Data;
using Ramp.Naming;
using Ramp.Network;
using Ramp.Simulation;
using static Coroutines.StandardActions;

namespace Ramp.Rendering
{
    /// <summary>
    ///     Describes the state of an animation renderer.
    /// </summary>
    public enum AnimationStatus
    {
        /// <summary>
        ///     No activity.
        /// </summary>
        Stopped,

        /// <summary>
        ///     Play() was invoked and an animation is currently loading.
        /// </summary>
        Loading,

        /// <summary>
        ///     An animation has finished loading and is currently playing.
        /// </summary>
        Playing,

        /// <summary>
        ///     The animation has finished playing and remains on the last frame.
        /// </summary>
        Finished
    }

    /// <summary>
    ///     Describes the type of frame change. CurrentFrame is valid for all but the Stopped change type.
    /// </summary>
    public enum AnimationFrameChangeType
    {
        /// <summary>
        ///     The animation has started. Is not called for successive loops.
        /// </summary>
        Started,

        /// <summary>
        ///     The animation has advanced to the next frame.
        /// </summary>
        Advanced,

        /// <summary>
        ///     The animation has advanced to the last frame and will either pause on it or loop back to the first.
        /// </summary>
        Finished,

        /// <summary>
        ///     The animation was stopped.
        /// </summary>
        Stopped
    }

    /// <summary>
    ///     Provides sprite animation by combining a sprite animation definition file with a sprite component
    /// </summary>
    public sealed class AnimationRenderer : Renderer
    {
        /// <summary>
        ///     The name of the default frame list for non-directional animations.
        /// </summary>
        public const string DefaultFrameListName = "Default";

        /// <summary>
        ///     The number of texture aliases allowed in an animation.
        /// </summary>
        public const byte TextureAliasCount = 10;

        private static readonly Logger s_log = LogManager.GetCurrentClassLogger();

        private readonly AssetReference<Animation> _ref;

        private AnimationStatus _status;
        private string _frameListName;
        private IReadOnlyList<AnimationFrame> _frameList;
        private int _frameIndex = -1;
        private TimeSpan _remaining;

        private Direction _direction;
        private Color _color;
        private Vector2 _offset;
        private Vector2 _size;
        private readonly AssetReference<Texture2D>[] _aliasTextures = new AssetReference<Texture2D>[TextureAliasCount];
        private readonly Dictionary<string, TextureInfo> _textures = new Dictionary<string, TextureInfo>();
        private int _playVersion;

        public AnimationRenderer()
        {
            Actor.IsActive = true;

            _ref = new AssetReference<Animation>();
            Color = Color.White;

            for (int i = 0; i < _aliasTextures.Length; i++)
                _aliasTextures[i] = new AssetReference<Texture2D>();

            Color = Actor.GetSpawnArgument("Color", Color.White);
            Size = Actor.GetSpawnArgument<Vector2>("RendererSize");
            Offset = Actor.GetSpawnArgument<Vector2>("RendererOffset");

            var ani = Actor.GetSpawnArgument<string>("Animation");
            var flist = Actor.GetSpawnArgument<string>("AnimationFrameList");
            if (ani != null)
                Play(ani, flist);
        }

        /// <summary>
        ///     Raised when a frame changes.
        /// </summary>
        public event TypedEventHandler<AnimationRenderer, AnimationFrameChangedEventArgs> FrameChanged;

        public event TypedEventHandler<AnimationRenderer, EventArgs> StatusChanged;

        /// <summary>
        ///     Gets the currently playing animation.
        /// </summary>
        public Animation Animation => _ref.LoadTargetAsset();

        /// <summary>
        ///     Gets the name of the currently playing animation.
        /// </summary>
        public string AnimationName => _ref.Name;

        /// <summary>
        ///     The name of the currently playing frame list.
        /// </summary>
        public string FrameListName => _frameListName;

        /// <summary>
        ///     The overall color to render the animation in.
        /// </summary>
        public Color Color
        {
            get { return _color; }

            set { _color = value; }
        }

        /// <summary>
        ///     The current frame if any.
        /// </summary>
        public AnimationFrame CurrentFrame => _frameIndex != -1 ? _frameList[_frameIndex] : null;

        public AnimationStatus Status
        {
            get { return _status; }

            set
            {
                if (_status == value)
                    return;
                _status = value;
                StatusChanged?.Invoke(this, EventArgs.Empty);
            }
        }
        /// <summary>
        ///     The offset of the animation from the actor position.
        /// </summary>
        public Vector2 Offset
        {
            get { return _offset; }

            set { _offset = value; }
        }

        /// <summary>
        ///     The size of the visual bounding box of the animation.
        /// </summary>
        public Vector2 Size
        {
            get { return _size; }

            set { _size = value; }
        }

        /// <summary>
        ///     The visual bounding box of the animation.
        /// </summary>
        public override BoundingBox LocalBoundingBox
        {
            get
            {
                Vector2 extent = _size / 2;
                return new BoundingBox(new Vector3(_offset - extent, 0), new Vector3(_offset + extent, 0));
            }
        }

        protected internal override void OnActorDisposing()
        {
            Stop(); // Dispatches events
            base.OnActorDisposing();
        }

        public override void Update(TimeSpan elapsed)
        {
            base.Update(elapsed);

            if (Status == AnimationStatus.Loading)
                FinishInitialize();

            if (!Enabled || Status < AnimationStatus.Playing)
                return;
            
            if (_frameIndex == -1)
            {
                AdvanceFrame();
            }
            else
            {
                // If time has reached zero, advance to next frame
                // TODO: Frame skipping if the time intervals are too large.
                _remaining -= elapsed;
                if (_remaining <= TimeSpan.Zero)
                    AdvanceFrame();
            }
        }

        public override void Draw(ILevelObjectRenderer ri)
        {
            if (!Enabled || _frameList == null)
                return;

            Debug.Assert(_frameIndex >= -1 && _frameIndex <= _frameList.Count);

            // Even if we haven't properly advanced into the first frame, draw it anyways to avoid flickering
            int index;
            if (_frameIndex == -1)
                index = 0;
            else if (_frameIndex == _frameList.Count)
                index = _frameList.Count - 1;
            else
                index = _frameIndex;

            DrawFrame(index, ri);
        }

        /// <summary>
        ///     Set a texture alias.
        /// </summary>
        /// <param name="index"> The alias index. </param>
        /// <param name="name"> The name of the texture, or null. </param>
        public void SetTextureAlias(byte index, string name)
        {
            if (index > TextureAliasCount)
                throw new ArgumentOutOfRangeException(nameof(index));

            _aliasTextures[index].Name = name;
        }

        /// <summary>
        ///     Resets the texture list for the current animation. This clears the list if there is no Animation.
        /// </summary>
        public void ResetTextures()
        {
            _textures.Clear();

            if (!_ref.IsLoaded)
                return;

            // TODO: Simplify this to avoid recreating asset references.

            foreach (KeyValuePair<string, AnimationSpriteType> type in _ref.LoadTargetAsset().SpriteTypes)
            {
                TextureInfo textureInfo;

                if (type.Value.TextureName == null)
                {
                    textureInfo = new TextureInfo();
                }
                else if (type.Value.TextureName.StartsWith("@"))
                {
                    byte aliasIndex;
                    if (!byte.TryParse(type.Value.TextureName.Substring(1), out aliasIndex) || aliasIndex >= TextureAliasCount)
                        textureInfo = new TextureInfo();
                    else
                        textureInfo = new TextureInfo(aliasIndex);
                }
                else
                {
                    textureInfo = new TextureInfo(new AssetReference<Texture2D>(type.Value.TextureName));
                }

                _textures.Add(type.Key, textureInfo);
            }
        }

        /// <summary>
        ///     Play a sprite animation.
        /// </summary>
        /// <param name="name"> The name of the animation definition to play. </param>
        /// <param name="direction">
        ///     A direction indicating which frame list to play. If the direction is None, the default frame list
        ///     is used.
        /// </param>
        /// <exception cref="ArgumentException"> Name is empty or null. </exception>
        public bool Play(string name, Direction direction)
        {
            return Play(name, direction != Direction.None ? direction.ToString() : null);
        }

        /// <summary>
        ///     Play a sprite animation.
        /// </summary>
        /// <param name="name"> The name of the animation definition to play. </param>
        /// <param name="frameList"> The name of the frame list to use. If null, uses default directional frame list. </param>
        /// <exception cref="ArgumentException"> Name is empty or null. </exception>
        public bool Play(string name, string frameList = null)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("name");

            if (_ref.Name != name || _frameListName != frameList)
            {
                Stop(); // invokes proper events

                BeginInitialize(name, frameList);
            }

            _playVersion++;

            return Status != AnimationStatus.Stopped;
        }

        /// <summary>
        /// Create a coroutine that waits until a frame event. Sets the thread result to true if the event was reached,
        /// or false if the animation finished without reaching the event.
        /// </summary>
        /// <param name="name">The name of the frame event to wait for.</param>
        public IEnumerable WaitForFrameEventCor(string name)
        {
            int version = _playVersion;
            while (Status != AnimationStatus.Stopped && Status != AnimationStatus.Finished && _playVersion == version)
            {
                if (CurrentFrame != null && CurrentFrame.Events.Contains(name))
                {
                    yield return Result(true);
                }
                else
                {
                    yield return null;
                }
            }
            yield return Result(false);
        }

        /// <summary>
        /// Create a coroutine that waits until the animation has finished or stopped.
        /// </summary>
        public IEnumerable WaitFinishedCor()
        {
            int version = _playVersion;
            while (Status != AnimationStatus.Stopped && Status != AnimationStatus.Finished && _playVersion == version)
                yield return null;
        } 

        /// <summary>
        ///     Stop the current animation if any is playing.
        /// </summary>
        public void Stop()
        {
            if (Status != AnimationStatus.Stopped)
                BeginInitialize(null, null);
        }

        public override void WriteDelta(DeltaWriter writer, ReplicationContext context)
        {
            base.WriteDelta(writer, context);

            for (int i = 0; i < TextureAliasCount; i++)
                writer.Write(_aliasTextures[i].Name);
            writer.Write(_ref.Name);
            writer.Write(_frameListName);
            writer.Write((byte) _direction);
            writer.Write(_color);
            writer.Write(_offset);
        }

        public override void ReadDelta(DeltaReader reader, ReplicationContext context)
        {
            base.ReadDelta(reader, context);

            for (int i = 0; i < TextureAliasCount; i++)
            {
                string name = null;
                if (!reader.Read(ref name))
                    continue;

                _aliasTextures[i].Name = name;
                //World.Content.LoadAsync(_aliasTextures[i]);
                //if (!World.Content.Load(_aliasTextures[i], true))
                //    Log.ErrorFormat("AnimationRenderer deserialized bad texture name: " + name);
            }

            string animationName = null;
            string frameListName = null;
            bool animationChanged = reader.Read(ref animationName);
            bool frameListChanged = reader.Read(ref frameListName);
            reader.ReadEnumByte(ref _direction);
            reader.Read(ref _color);
            reader.Read(ref _offset);

            // Since we don't replicate animation playback state, we just call Play() ourselves whenever
            // there is a change.
            if (animationChanged || frameListChanged)
            {
                if (animationName == null)
                    Stop();
                else
                    Play(animationChanged ? animationName : _ref.Name, frameListChanged ? frameListName : _frameListName);
            }
        }

        // Draw the specified frame's sprites.
        private void DrawFrame(int index, ILevelObjectRenderer ri)
        {
            if (!_ref.IsLoaded)
                return;

            AnimationFrame frame = _frameList[index];
            Vector2 origin = Transform.Location + _offset;

            // ReSharper disable once ForCanBeConvertedToForeach
            for (int i = 0; i < frame.Sprites.Count; i++)
            {
                AnimationSprite sprite = frame.Sprites[i];

                AnimationSpriteType type;
                if (!_ref.LoadTargetAsset().SpriteTypes.TryGetValue(sprite.SpriteType, out type))
                    continue; // no matching sprite type found

                // The _textures list will either specify an alias index or a texture asset for each sprite type
                TextureInfo textureInfo = _textures[sprite.SpriteType];

                AssetReference<Texture2D> reference = textureInfo.HasAlias ? _aliasTextures[textureInfo.AliasIndex] : textureInfo.Asset;

                Texture2D texture = reference?.LoadTargetAsset();
                if (texture == null)
                    continue;

                var voff = new Vector2(sprite.Position.X, sprite.Position.Y) / ri.PixelsPerTile;
                var size = new Vector2(type.TextureRect.Width, type.TextureRect.Height) / ri.PixelsPerTile;

                ri.DrawSprite(texture, origin + voff, sprite.Color, size, type.TextureRect, 1f, sprite.Rotation);
            }
        }

        ///// <summary>
        /////     Updates the visual bounding box of this sprite based on the current animation's frame.
        ///// </summary>
        //private void UpdateBoundingBox(uint pixelsPerTile)
        //{
        //	// No bounds
        //	if (!IsPlaying)
        //	{
        //		_boundingBox = new BoundingBox();
        //		return;
        //	}
        //	var min = new Vector2();
        //	var max = new Vector2();
        //	AnimationFrame frame = _frameList[_frameIndex];
        //	for (int i = 0; i < frame.Sprites.Count; i++)
        //	{
        //		AnimationSprite sprite = frame.Sprites[i];
        //		// TODO: Determine how best to handle pixels per tile for good
        //		var voff = new Vector2((float)sprite.Position.X / pixelsPerTile,
        //							   (float)sprite.Position.Y / pixelsPerTile);
        //		var vmax = new Vector2(((float)sprite.Position.X + sprite.SpriteType.TextureRect.Width) / pixelsPerTile,
        //							   ((float)sprite.Position.Y + sprite.SpriteType.TextureRect.Height) / pixelsPerTile);
        //		if (voff.X < min.X)
        //			min.X = voff.X;
        //		if (voff.Y < min.Y)
        //			min.Y = voff.Y;
        //		if (vmax.X > max.X)
        //			max.X = vmax.X;
        //		if (vmax.Y > max.Y)
        //			max.Y = vmax.Y;
        //	}
        //	_boundingBox = new BoundingBox(new Vector3(min, 0), new Vector3(max, 0));
        //}

        // Initialize the animation at the first frame. Returns whether or not the setup was successful.
        private void BeginInitialize(string name, string frameListName)
        {
            _ref.Name = name;
            _frameListName = frameListName;
            _frameList = null;
            _frameIndex = -1;
            _remaining = TimeSpan.Zero;

            ResetTextures();

            if (name == null)
            {
                OnFrameChanged(AnimationFrameChangeType.Stopped);
                Status = AnimationStatus.Stopped;
            }
            else
            {
                Status = AnimationStatus.Loading;

                // Event could have played a new animation
                if (Status == AnimationStatus.Loading)
                    FinishInitialize();
            }
        }

        private void FinishInitialize()
        {
            Debug.Assert(Status == AnimationStatus.Loading);

            if (_ref.Status == ReferenceStatus.Loading)
                return;

            string name = _ref.Name;
            Asset<Animation> asset = _ref.LoadTarget();

            Debug.Assert(name != null);

            if (asset == null)
            {
                _ref.Name = null;
                Status = AnimationStatus.Stopped;

                s_log.Warn("Invalid asset name: {0}", name);
                return;
            }

            string validFrameListName = _frameListName ?? DefaultFrameListName;

            List<AnimationFrame> frameList;
            _ref.LoadTargetAsset().Frames.TryGetValue(validFrameListName, out frameList);

            if (frameList == null || frameList.Count == 0)
            {
                _ref.Name = null;
                Status = AnimationStatus.Stopped;

                s_log.Warn("Frame list {0} was empty or not found in {1}", validFrameListName, name);
                return;
            }

            _frameList = frameList;
            _frameIndex = -1;
            _remaining = TimeSpan.Zero;
            ResetTextures();
            Status = AnimationStatus.Playing;
        }

        // Advance to the next frame
        private void AdvanceFrame()
        {
            Debug.Assert(_frameIndex >= -1 && _frameIndex < _frameList.Count, "_frameIndex >= -1 && _frameIndex < _frameList.Count");


            Debug.Assert(_frameList.Count != 0, "_frameList.Count != 0");

            if (_frameIndex == -1)
            {
                _frameIndex = 0;
                _remaining = TimeSpan.FromSeconds(_frameList[_frameIndex].Duration);
                OnFrameChanged(AnimationFrameChangeType.Started);
            }
            else if (_frameIndex < _frameList.Count - 1)
            {
                _frameIndex++;
                _remaining = TimeSpan.FromSeconds(_frameList[_frameIndex].Duration);
                OnFrameChanged(AnimationFrameChangeType.Advanced);
            }
            else
            {
                if (_ref.LoadTargetAsset().Loop)
                {
                    _frameIndex = 0;
                    _remaining = TimeSpan.FromSeconds(_frameList[_frameIndex].Duration);
                }
                else
                {
                    _frameIndex = _frameList.Count - 1;
                    _remaining = TimeSpan.MaxValue;
                }
                Status = AnimationStatus.Finished;
                OnFrameChanged(AnimationFrameChangeType.Finished);
            }
        }

        private void OnFrameChanged(AnimationFrameChangeType type)
        {
            FrameChanged?.Invoke(this, new AnimationFrameChangedEventArgs(type));
        }

        private struct TextureInfo
        {
            public readonly AssetReference<Texture2D> Asset;

            public readonly byte AliasIndex;

            public readonly bool HasAlias;

            public TextureInfo(AssetReference<Texture2D> asset)
            {
                Asset = asset;
                AliasIndex = 0;
                HasAlias = false;
            }

            public TextureInfo(byte? aliasIndex)
            {
                Asset = null;
                AliasIndex = aliasIndex.GetValueOrDefault();
                HasAlias = aliasIndex.HasValue;
            }
        }
    }

    public class AnimationFrameChangedEventArgs : EventArgs
    {
        public AnimationFrameChangedEventArgs(AnimationFrameChangeType type)
        {
            ChangeType = type;
        }

        public AnimationFrameChangeType ChangeType { get; }
    }
}