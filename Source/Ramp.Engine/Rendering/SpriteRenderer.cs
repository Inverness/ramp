using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Ramp.Data;
using Ramp.Network;
using Ramp.Simulation;

namespace Ramp.Rendering
{
    /// <summary>
    ///     A component providing sprite rendering for actors.
    /// </summary>
    public class SpriteRenderer : Renderer
    {
        private readonly AssetReference<Texture2D> _texture = new AssetReference<Texture2D>();
        private Color _color;
        private bool _light;
        private Vector2 _size;
        private Vector2 _offset;
        private float _rotation;
        private float _scale;
        private Rectangle _texturePart;

        public SpriteRenderer()
        {
            _color = Actor.GetSpawnArgument("Color", Color.White);
            _light = Actor.GetSpawnArgument<bool>("Light");
            _size = Actor.GetSpawnArgument<Vector2>("RendererSize");
            _offset = Actor.GetSpawnArgument<Vector2>("RendererOffset");
            _rotation = Actor.GetSpawnArgument<float>("Rotation");
            _scale = Actor.GetSpawnArgument("Scale", 1f);
            _texture.Name = Actor.GetSpawnArgument<string>("Texture");
            _texturePart = Actor.GetSpawnArgument<Rectangle>("TexturePart");
        }

        public override BoundingBox LocalBoundingBox
        {
            get
            {
                Vector2 extent = _size / 2;
                return new BoundingBox(new Vector3(_offset - extent, 0), new Vector3(_offset + extent, 0));
            }
        }

        /// <summary>
        ///     Gets or sets the color of the sprite. Defaults to Color.White.
        /// </summary>
        public Color Color
        {
            get { return _color; }

            set { _color = value; }
        }

        /// <summary>
        ///     Gets or sets whether to render the sprite as a light by using additive blending.
        /// </summary>
        public bool Light
        {
            get { return _light; }

            set { _light = value; }
        }

        /// <summary>
        ///     Gets or sets the size of the tile in sprites.
        /// </summary>
        public Vector2 Size
        {
            get { return _size; }

            set { _size = value; }
        }

        /// <summary>
        ///     Gets or sets the offset of the sprite origin from the owning actor's location.
        /// </summary>
        public Vector2 Offset
        {
            get { return _offset; }

            set { _offset = value; }
        }

        /// <summary>
        ///		Gets or sets the rotation of the sprite in radians.
        /// </summary>
        public float Rotation
        {
            get { return _rotation; }

            set { _rotation = value; }
        }

        /// <summary>
        ///		Gets or sets multiplicative scale of the sprite. Defaults to 1.
        /// </summary>
        public float Scale
        {
            get { return _scale; }

            set { _scale = value; }
        }

        /// <summary>
        ///		Gets or sets the name of the texture that will be rendered.
        /// </summary>
        public string Texture
        {
            get { return _texture.Name; }

            set { _texture.Name = value; }
        }

        /// <summary>
        ///    Gets or sets the part of a texture that will be rendered.
        /// </summary>
        public Rectangle TexturePart
        {
            get { return _texturePart; }

            set { _texturePart = value; }
        }

        public override void Draw(ILevelObjectRenderer ri)
        {
            Texture2D texture = _texture.LoadTargetAsset();

            if (texture == null || _scale <= 0)
                return;

            ri.DrawSprite(texture,
                          Transform.Location + _offset,
                          _color,
                          _size != Vector2.Zero ? _size : (Vector2?) null,
                          _texturePart != Rectangle.Empty ? _texturePart : (Rectangle?) null,
                          _scale,
                          _rotation,
                          _light ? SpriteRenderMode.Light : SpriteRenderMode.Default);
        }

        public override void WriteDelta(DeltaWriter writer, ReplicationContext context)
        {
            base.WriteDelta(writer, context);
            _texture.Serialize(writer, context);
            writer.Write(_texturePart);
            writer.Write(_offset);
            writer.Write(_size);
            writer.Write(_color);
            writer.Write(_light);
            writer.Write(_rotation);
            writer.Write(_scale);
        }

        public override void ReadDelta(DeltaReader reader, ReplicationContext context)
        {
            base.ReadDelta(reader, context);
            _texture.Deserialize(reader, context);
            reader.Read(ref _texturePart);
            reader.Read(ref _offset);
            reader.Read(ref _size);
            reader.Read(ref _color);
            reader.Read(ref _light);
            reader.Read(ref _rotation);
            reader.Read(ref _scale);
        }
    }
}