using System;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Ramp.Data;
using Ramp.Simulation;
using Ramp.Simulation.Tiling;
using Ramp.Utilities.Collections;

namespace Ramp.Rendering
{
    // TODO: Remove tile caches from levels in favor of a single cache for the current rendering level.

    /// <summary>
    ///     Default implementation of <see cref="ILevelRenderer" />.
    /// </summary>
    public sealed class LevelRenderer : DrawableGameComponent, ILevelRenderer, ILevelObjectRenderer
    {
        /// <summary>
        ///     Size of tile sides in pixels.
        /// </summary>
        public const byte PixelsPerTile = 16;

        /// <summary>
        ///     Maximum number of layers that can be rendered.
        /// </summary>
        public const byte LayerCount = 8;

        // maximum number of sprites to render per layer
        private const int MaxSpritesPerLayer = 512;

        // Number of vertices and indices for a quad
        private const byte VerticesPerQuad = 4;
        private const byte IndicesPerQuad = 6;
        private const byte PrimitivesPerQuad = 2;

        // Maximum tiles allowed to be drawed at once
        private const ushort MaxTilesPerDraw = TileMap.GroupSize * TileMap.GroupSize;

        private readonly IConfigService _config;
        private readonly RampContentManager _content;

        private bool _isDrawing;
        private Level _level;

        // single pixel texture used to fill solids, since XNA doesn't support shapes.
        private Texture2D _fillTexture;

        // Stores visible renderers for each layer for each rendering operation
        private readonly RendererList[] _rendererLists = new RendererList[LayerCount];
        private readonly IComparer<Renderer> _rendererYComparer = Comparer<Renderer>.Create(CompareRendererYOrder);

        // Stores visible colliders on all layers when debugging
        private readonly List<Collider> _colliders = new List<Collider>(MaxSpritesPerLayer);

        // Fallback actor texture for debugging
        private Texture2D _actorTexture;

        // Dynamic vertex buffer, at least large enough to handle vertices for 4096 tiles
        private DynamicVertexBuffer _dynamicVertexBuffer;

        // Dynamic index buffer, at least large enough to handle indices for 4096 tiles
        private DynamicIndexBuffer _dynamicIndexBuffer;

        // Basic effect containing our matrices
        private BasicEffect _effect;

        // Measures each whole rendering operation
        private readonly Stopwatch _renderTime = new Stopwatch();

        // Measures sprite rendering time
        private readonly Stopwatch _spriteRenderTime = new Stopwatch();

        // Current tile vertices when rendering a tile map
        private VertexPositionColorTexture[] _tileVertices;
        private ushort[] _tileIndices;

        // Screen effect vertices
        private readonly VertexPositionColorTexture[] _screenEffectVertices = new VertexPositionColorTexture[6];

        // Sprite batches for normal sprites and lights.
        private SpriteBatch _normals;
        private SpriteBatch _lights;

        private SpriteFont _textFont;

        // Primitive batch for drawing basic shapes.
        //private PrimitiveBatch<VertexPositionColorTexture> _primitives;

        // Caches of tile vertices for the current level
        private TileRenderCache[,,] _tileCaches;

        // The DXGI back buffer and render target for Direct2D.
        //private Surface _backBuffer;
        //private RenderTarget _renderTargetD2D;

        public LevelRenderer(Game game)
            : base(game)
        {
            _config = game.Services.GetRequiredService<IConfigService>();

            _content = new RampContentManager(game.Services, "LevelRenderer");

            for (int i = 0; i < LayerCount; i++)
                _rendererLists[i] = new RendererList { Items = new Renderer[MaxSpritesPerLayer] };

            ShowBoundingBoxes = false;

            game.Services.AddService(typeof(ILevelRenderer), this);
        }

        public event TypedEventHandler<ILevelRenderer, LevelEventArgs> LevelChanging;

        public event TypedEventHandler<ILevelRenderer, EventArgs> LevelChanged;

        // Explicit to avoid name conflict
        byte ILevelObjectRenderer.PixelsPerTile => PixelsPerTile;

        byte ILevelObjectRenderer.LayerCount => LayerCount;

        public double LastRenderTime { get; private set; }

        public double LastSpriteRenderTime { get; private set; }

        public Vector2 Camera { get; set; }

        public Level Level
        {
            get { return _level; }

            set
            {
                if (_level == value)
                    return;
                OnLevelChanging(value);
                _level = value;
                OnLevelChanged();
            }
        }

        public Color? ScreenColor { get; set; }

        public bool ShowBoundingBoxes { get; set; }

        public override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            // Load some basic textures first
            string actorTextureName = _config.Database.GetString("System.Game.DefaultActorTexture") ?? "Textures/Actor";
            _actorTexture = _content.Load<Texture2D>(actorTextureName);
            _fillTexture = new Texture2D(GraphicsDevice, 1, 1, false, SurfaceFormat.Color);
            _fillTexture.SetData(new[] { Color.White });
            _textFont = _content.Load<SpriteFont>("Fonts/Arial12");

            // Load our basic effect shader
            _effect = new BasicEffect(GraphicsDevice)
            {
                VertexColorEnabled = true,
                TextureEnabled = true
            };

            // Initialize our dynamic buffers, which we use for all calls for now

            _dynamicVertexBuffer = new DynamicVertexBuffer(GraphicsDevice,
                                                           VertexPositionColorTexture.VertexDeclaration,
                                                           MaxTilesPerDraw * VerticesPerQuad,
                                                           BufferUsage.WriteOnly);

            _dynamicIndexBuffer = new DynamicIndexBuffer(GraphicsDevice,
                                                         IndexElementSize.SixteenBits,
                                                         MaxTilesPerDraw * IndicesPerQuad,
                                                         BufferUsage.WriteOnly);


            // Sprite batches for normal and light sprite rendering
            _normals = new SpriteBatch(GraphicsDevice);
            _lights = new SpriteBatch(GraphicsDevice);
        }

        protected override void UnloadContent()
        {
            //Disposable.Dispose(ref _renderTargetD2D);
            //Disposable.Dispose(ref _backBuffer);

            //RemoveAndDispose(ref _primitives);
            _normals?.Dispose();
            _lights?.Dispose();

            _dynamicIndexBuffer?.Dispose();
            _dynamicVertexBuffer?.Dispose();

            _fillTexture?.Dispose();
            _effect?.Dispose();
            _textFont = null;
            _content?.Unload();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                ClearLevelCache();
                UnloadContent();
            }
            base.Dispose(disposing);
        }

        public override void Update(GameTime gameTime)
        {
        }

        public override void Draw(GameTime gameTime)
        {
            if (Enabled && Level != null)
                DrawScene(Level, Camera);
        }

        public void DrawSprite(Texture2D texture, Vector2 location, Color? color = null, Vector2? size = null,
                               Rectangle? textureRect = null, float scale = 1f, float rotation = 0f,
                               SpriteRenderMode renderMode = SpriteRenderMode.Default)
        {
            Validate.ArgumentNotNull(texture, "texture");
            Validate.ArgumentInRange(scale > 0, "scale");
            Validate.ValidOperation(_isDrawing);

            if (!color.HasValue)
                color = Color.White;
            if (!size.HasValue)
                size = new Vector2((float) texture.Width / PixelsPerTile, (float) texture.Height / PixelsPerTile);
            if (!textureRect.HasValue || textureRect.Value.IsEmpty)
                textureRect = new Rectangle(0, 0, texture.Width, texture.Height);

            // The origin is always centered on the sprite's texture. The sprite's rotation and scale are both based
            // on this. Scale so when doubling the scale of a sprite its size when rendered will expand equally in all
            // directions instead of only to the bottom and right.
            var origin = new Vector2(textureRect.Value.Width / 2f, textureRect.Value.Height / 2f);

            // The sprite batches use projection matrices based on tile coordinates. This means if a sprite was
            // drawn with a scale of 1 each of its pixels would be the size of a whole tile.
            //
            // A sprite's size is specified in tile coordinates. This avoids any dependency on knowing the size of
            // a texture for proper sizing. It also means a 1,1 sized sprite must be the same size no matter how large
            // its texture is.
            //
            // A 32x16 pixel texture, drawn with a size of 2,2 would be stretched vertically to occupy 32x32 pixels.

            var textureAdjustment = new Vector2(1f / ((float) textureRect.Value.Width / PixelsPerTile),
                                                1f / ((float) textureRect.Value.Height / PixelsPerTile));

            size *= scale * textureAdjustment / PixelsPerTile;

            switch (renderMode)
            {
                case SpriteRenderMode.Default:
                    _normals.Draw(texture, location, textureRect, color.Value, rotation, origin, size.Value,
                                  SpriteEffects.None, 0);
                    break;
                case SpriteRenderMode.Light:
                    _lights.Draw(texture, location, textureRect, color.Value, rotation, origin, size.Value,
                                 SpriteEffects.None, 0);
                    break;
            }
        }

        public void DrawText(string text, Vector2 location, Color? color = null, string fontName = null,
                             bool centered = false)
        {
            Validate.Argument(!string.IsNullOrEmpty(text), "text must not be null or empty", "text");
            Validate.ValidOperation(_isDrawing);

            if (centered)
            {
                Vector2 size = _textFont.MeasureString(text) / PixelsPerTile;
                location -= size / 2;
            }
            _normals.DrawString(_textFont, text, location, color ?? Color.White, 0, Vector2.Zero, 1f / PixelsPerTile,
                                SpriteEffects.None, 0);
        }

        private void DrawScene(Level level, Vector2 cameraLocation)
        {
            _isDrawing = true;
            _renderTime.Start();

            // Calculate what tiles and groups will be visible, and set the projection matrix.

            Point center = GraphicsDevice.Viewport.Bounds.Center;

            var minLoc = new Vector2(cameraLocation.X - (float) center.X / PixelsPerTile,
                                     cameraLocation.Y - (float) center.Y / PixelsPerTile);
            var maxLoc = minLoc +
                         new Vector2((float) center.X / PixelsPerTile * 2, (float) center.Y / PixelsPerTile * 2);

            var cameraBounds = new BoundingBox(new Vector3(minLoc, 0), new Vector3(maxLoc, 0));

            Rectangle viewportGroups;
            viewportGroups.X = (int) Math.Truncate(minLoc.X / TileMap.GroupSize);
            viewportGroups.Y = (int) Math.Truncate(minLoc.Y / TileMap.GroupSize);
            viewportGroups.Width = ((int) Math.Truncate(maxLoc.X / TileMap.GroupSize) + 1) - viewportGroups.X;
            viewportGroups.Height = ((int) Math.Truncate(maxLoc.Y / TileMap.GroupSize) + 1) - viewportGroups.Y;

            _effect.Projection = Matrix.CreateOrthographicOffCenter(minLoc.X, maxLoc.X,
                                                                    maxLoc.Y, minLoc.Y,
                                                                    1.0f, -1.0f);

            // Clear the screen and setup the graphics device

            GraphicsDevice.Clear(Color.Black);
            GraphicsDevice.DepthStencilState = DepthStencilState.None;
            GraphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise;

            // Identify and buffer visible renderer components for all layers

            _spriteRenderTime.Start();

            LinkedArrayList<Renderer> renderers = level.InternalRenderers;
            for (int i = renderers.FirstIndex; i != -1;)
            {
                Renderer r = renderers.GetItemAndNextIndex(i, out i);

                if (r.Enabled && r.WorldBoundingBox.Intersects(cameraBounds))
                {
                    // Clamp to available layers
                    int layer = Math.Max(Math.Min(r.Layer ?? level.DefaultRendererLayer, _rendererLists.Length - 1), 0);

                    // Sort renderes into lists
                    RendererList list = _rendererLists[layer];
                    if (list.Count < MaxSpritesPerLayer)
                        list.Items[list.Count++] = r;
                    else
                        Debug.Fail("too many sprites to render");
                }
            }

            _spriteRenderTime.Stop();

            // Render visible tiles and renderers

            for (byte i = 0; i < LayerCount; i++)
            {
                if (level.TileMap != null)
                {
                    TileLayer layer = level.TileMap.GetVisualLayer(i);
                    Texture2D texture;

                    if (layer != null && !layer.IsHidden && layer.Color.A != 0 && layer.Tileset != null &&
                        (texture = layer.Tileset.Texture.LoadTargetAsset()) != null)
                    {
                        // We'll render whole tile groups if any part of them is visible
                        for (int y = 0; y < viewportGroups.Height; y++)
                        {
                            for (int x = 0; x < viewportGroups.Width; x++)
                            {
                                Point levelSize = level.TileMap.Size;
                                int groupX = viewportGroups.Left + x;
                                int groupY = viewportGroups.Top + y;

                                // If the camera is on the edge of the level we can get out of bounds group locations
                                if (groupX < 0 || groupX >= levelSize.X || groupY < 0 || groupY >= levelSize.Y)
                                    continue;
                                TileArray ta = layer.GetArray(groupX, groupY);

                                // We use this disposable to cache our vertices
                                TileRenderCache ri = _tileCaches[groupX, groupY, layer.Index];
                                if (ri == null)
                                {
                                    ri = new TileRenderCache();
                                    _tileCaches[groupX, groupY, layer.Index] = ri;
                                }

                                // NOTE: might need to use a stack in the future
                                _effect.World = Matrix.CreateTranslation(groupX * 64, groupY * 64, 0);
                                DrawTiles(ta, texture, Color.White, ri);
                                _effect.World = Matrix.Identity;
                            }
                        }
                    }
                }

                _spriteRenderTime.Start();

                // Sort the renderers so that objects further down on the screen are rendered first
                RendererList list = _rendererLists[i];
                Array.Sort(list.Items, 0, list.Count, _rendererYComparer);

                _effect.Texture = null; // Effect texture would override sprite texture

                _normals.Begin(SpriteSortMode.Deferred,
                               GraphicsDevice.BlendState = BlendState.NonPremultiplied,
                               GraphicsDevice.SamplerStates[0] = SamplerState.PointClamp,
                               GraphicsDevice.DepthStencilState = DepthStencilState.None,
                               GraphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise,
                               _effect);

                _lights.Begin(SpriteSortMode.Deferred,
                              GraphicsDevice.BlendState = BlendState.Additive,
                              GraphicsDevice.SamplerStates[0] = SamplerState.PointClamp,
                              GraphicsDevice.DepthStencilState = DepthStencilState.None,
                              GraphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise,
                              _effect);

                for (int e = 0; e < list.Count; e++)
                {
                    list.Items[e].Draw(this);
                    list.Items[e] = null;
                }
                list.Count = 0;

                _normals.End();
                _lights.End();

                _spriteRenderTime.Stop();
            }

            // If debugging, buffer and draw colliders
            if (ShowBoundingBoxes)
            {
                // Showing bounding boxes isn't normal so don't worry about enumerator allocation
                foreach (Collider collider in level.Colliders)
                {
                    if (!collider.Enabled || !collider.WorldBoundingBox.Intersects(cameraBounds))
                        continue;
                    if (_colliders.Count < _colliders.Capacity)
                        _colliders.Add(collider);
                }

                _normals.Begin(SpriteSortMode.Deferred,
                               GraphicsDevice.BlendState = BlendState.NonPremultiplied,
                               GraphicsDevice.SamplerStates[0] = SamplerState.PointClamp,
                               GraphicsDevice.DepthStencilState = DepthStencilState.None,
                               GraphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise,
                               _effect);

                foreach (Collider collider in _colliders)
                {
                    BoundingBox box = collider.WorldBoundingBox;

                    var size = new Vector2(box.Max.X - box.Min.X, box.Max.Y - box.Min.Y);
                    var location = new Vector2(box.Min.X + size.X / 2, box.Min.Y + size.Y / 2);
                    var color = new Color(192, 128, 128, 128);

                    //var v1 = new VertexPositionColor { Position = box.Minimum, Color = color };
                    //var v2 = new VertexPositionColor { Position = new Vector3(box.Maximum.X, box.Minimum.Y, 0), Color = color };
                    //var v3 = new VertexPositionColor { Position = box.Maximum, Color = color };
                    //var v4 = new VertexPositionColor { Position = new Vector3(box.Minimum.X, box.Maximum.Y, 0), Color = color };

                    DrawSprite(_fillTexture, location, color, size);
                }
                _colliders.Clear();
                _normals.End();
            }

            // Apply the screen color
            Color? screenColor = ScreenColor;
            if (screenColor.HasValue && screenColor.Value.A != 0)
            {
                VertexPositionColorTexture v;
                v.Color = screenColor.Value;
                v.TextureCoordinate = Vector2.Zero;

                v.Position = new Vector3(minLoc.X, minLoc.Y, 0);
                _screenEffectVertices[0] = v;

                v.Position = new Vector3(maxLoc.X, minLoc.Y, 0);
                _screenEffectVertices[1] = v;

                v.Position = new Vector3(minLoc.X, maxLoc.Y, 0);
                _screenEffectVertices[2] = v;

                v.Position = new Vector3(minLoc.X, maxLoc.Y, 0);
                _screenEffectVertices[3] = v;

                v.Position = new Vector3(maxLoc.X, minLoc.Y, 0);
                _screenEffectVertices[4] = v;

                v.Position = new Vector3(maxLoc.X, maxLoc.Y, 0);
                _screenEffectVertices[5] = v;

                _dynamicVertexBuffer.SetData(_screenEffectVertices, 0, 6);

                GraphicsDevice.BlendState = BlendState.NonPremultiplied;
                GraphicsDevice.SetVertexBuffer(_dynamicVertexBuffer);
                _effect.Texture = _fillTexture;

                EffectPassCollection passes = _effect.CurrentTechnique.Passes;
                for (int i = 0; i < passes.Count; i++)
                {
                    passes[i].Apply();
                    GraphicsDevice.DrawPrimitives(PrimitiveType.TriangleList, 0, 2);
                }
            }

            // Report the time spent rendering the scene
            _renderTime.Stop();
            LastRenderTime = _renderTime.Elapsed.TotalMilliseconds;
            LastSpriteRenderTime = _spriteRenderTime.Elapsed.TotalMilliseconds;
            _renderTime.Reset();
            _spriteRenderTime.Reset();

            _isDrawing = false;
        }

        /// <summary>
        ///     Draw an array of tiles to the screen.
        /// </summary>
        /// <param name="tiles"> Two-dimensional array of tiles. </param>
        /// <param name="texture"> Tileset texture. </param>
        /// <param name="color"> Color applied to all tiles. Defaults to Color.White if null. </param>
        /// <param name="renderCache"> A disposable object used to cache verteces and other data. </param>
        private void DrawTiles(TileArray tiles, Texture2D texture, Color? color, TileRenderCache renderCache)
        {
            if (tiles == null)
                throw new ArgumentNullException(nameof(tiles));
            if (texture == null)
                throw new ArgumentNullException(nameof(texture));
            if (!color.HasValue)
                color = Color.White;

            // Early checks to avoid more work
            if (color.Value.A == 0 || tiles.Area == 0)
                return;

            if (tiles.Width * tiles.Height > MaxTilesPerDraw)
            {
                Debug.Fail("too many tiles to draw");
                return;
            }

            bool valid; // Whether vertices need to be recreated
            VertexBuffer vertexBuffer = null;
            IndexBuffer indexBuffer = null;

            if (renderCache != null)
            {
                // Check if the render cache has valid buffers
                valid = renderCache.Valid &&
                        color == renderCache.Color &&
                        texture == renderCache.Texture;
                vertexBuffer = renderCache.VertexBuffer;
                indexBuffer = renderCache.IndexBuffer;
            }
            else
            {
                // If there was no caching then we're always invalid.
                valid = false;
            }

            int vertexCount;

            if (!valid)
            {
                // Generate vertices. The visual tiles count indicates how many vertices and indices were needed
                // for the tiles.
                var visibleTilesCount = (int) GenerateVertices(tiles,
                                                               color.Value,
                                                               texture.Width,
                                                               texture.Height,
                                                               ref _tileVertices,
                                                               ref _tileIndices);

                vertexCount = visibleTilesCount * VerticesPerQuad;
                int indexCount = visibleTilesCount * IndicesPerQuad;

                if (renderCache != null)
                {
                    // Update the render cache with the new valid data.
                    //
                    // Vertex and index buffers need to be:
                    //   Created if they don't already exist.
                    //   Recreated if they are the wrong size.
                    //   Disposed if there are no tiles to draw.

                    if (vertexBuffer == null || vertexBuffer.VertexCount != vertexCount)
                    {
                        vertexBuffer?.Dispose();

                        if (vertexCount != 0)
                        {
                            vertexBuffer = new VertexBuffer(GraphicsDevice,
                                                            VertexPositionColorTexture.VertexDeclaration,
                                                            vertexCount,
                                                            BufferUsage.WriteOnly);
                        }
                        else
                        {
                            vertexBuffer = null;
                        }

                        renderCache.VertexBuffer = vertexBuffer;
                    }

                    if (indexBuffer == null || indexBuffer.IndexCount != indexCount)
                    {
                        indexBuffer?.Dispose();

                        if (indexCount != 0)
                        {
                            indexBuffer = new IndexBuffer(GraphicsDevice,
                                                          IndexElementSize.SixteenBits,
                                                          indexCount,
                                                          BufferUsage.WriteOnly);
                        }
                        else
                        {
                            indexBuffer = null;
                        }

                        renderCache.IndexBuffer = indexBuffer;
                    }

                    renderCache.Color = color.Value;
                    renderCache.Texture = texture;
                    renderCache.Valid = true;
                }
                else
                {
                    // Just use the dynamic buffer if there is no cache. The dynamic buffers are initialized to
                    // the maximum allowed size so there is no need to check further.
                    vertexBuffer = _dynamicVertexBuffer;
                    indexBuffer = _dynamicIndexBuffer;
                }

                if (visibleTilesCount == 0)
                {
                    // Should only happen if no visible tiles. At this point the rendercache will have a null vertex
                    // and index buffer but will be considered valid.
                    return;
                }

                Debug.Assert(vertexBuffer != null && indexBuffer != null, "vertexBuffer != null && indexBuffer != null");
                Debug.Assert(vertexCount != 0 && vertexBuffer.VertexCount >= vertexCount);
                Debug.Assert(indexCount != 0 && indexBuffer.IndexCount >= indexCount);

                vertexBuffer.SetData(_tileVertices, 0, vertexCount);
                indexBuffer.SetData(_tileIndices, 0, indexCount);
            }
            else
            {
                Debug.Assert(renderCache != null, "renderCache != null");

                if (renderCache.VertexBuffer == null)
                    return; // There were no visible tiles during the previous call.

                vertexCount = vertexBuffer.VertexCount;
            }

            _effect.Texture = texture;
            GraphicsDevice.SamplerStates[0] = SamplerState.PointClamp;

            GraphicsDevice.SetVertexBuffer(vertexBuffer);
            GraphicsDevice.Indices = indexBuffer;
            GraphicsDevice.BlendState = BlendState.NonPremultiplied;

            EffectPassCollection passes = _effect.CurrentTechnique.Passes;
            for (int i = 0; i < passes.Count; i++)
            {
                passes[i].Apply();

                GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList,
                                                     0,
                                                     0,
                                                     vertexCount / PrimitivesPerQuad);
            }
        }

        private uint GenerateVertices(
            TileArray tiles,
            Color color,
            int texW,
            int texH,
            ref VertexPositionColorTexture[] verts,
            ref ushort[] indices)
        {
            Array.Resize(ref verts, tiles.Width * tiles.Height * VerticesPerQuad);
            Array.Resize(ref indices, tiles.Width * tiles.Height * IndicesPerQuad);
            float scaleX = (float) PixelsPerTile / texW;
            float scaleY = (float) PixelsPerTile / texH;

            uint visibleTilesCount = 0;
            ushort i = 0;
            for (ushort y = 0; y < tiles.Height; y++)
            {
                for (ushort x = 0; x < tiles.Width; x++)
                {
                    ushort tileCode = tiles[x, y];
                    // transparent/non-existant tiles with code 0 do not have vertices created for them.
                    if (tileCode == 0)
                        continue;
                    ushort tileXOut, tileYOut;
                    TileCode.Decode(tileCode, out tileXOut, out tileYOut);
                    var tileX = (float) tileXOut;
                    var tileY = (float) tileYOut;

                    VertexPositionColorTexture v;
                    v.Color = color;
                    v.Position = new Vector3(x, y, 0);
                    v.TextureCoordinate = new Vector2(tileX * scaleX, tileY * scaleY);
                    verts[i] = v;

                    v.Position = new Vector3((x + 1), y, 0);
                    v.TextureCoordinate = new Vector2((tileX + 1) * scaleX, tileY * scaleY);
                    verts[i + 1] = v;

                    v.Position = new Vector3(x, (y + 1), 0);
                    v.TextureCoordinate = new Vector2(tileX * scaleX, (tileY + 1) * scaleY);
                    verts[i + 2] = v;

                    v.Position = new Vector3((x + 1), (y + 1), 0);
                    v.TextureCoordinate = new Vector2((tileX + 1) * scaleX, (tileY + 1) * scaleY);
                    verts[i + 3] = v;

                    i += VerticesPerQuad;

                    visibleTilesCount++;
                }
            }

            SetQuadIndices012213(indices, 0, 0, visibleTilesCount);

            return visibleTilesCount;
        }

        private void OnLevelChanging(Level newLevel)
        {
            ClearLevelCache();
            LevelChanging?.Invoke(this, new LevelEventArgs(newLevel));
        }

        private void OnLevelChanged()
        {
            InitializeLevelCache();
            LevelChanged?.Invoke(this, EventArgs.Empty);
        }

        private void ClearLevelCache()
        {
            if (_tileCaches == null)
                return;

            foreach (TileRenderCache trc in _tileCaches)
            {
                if (trc?.VertexBuffer == null)
                    continue;
                trc.VertexBuffer.Dispose();
                trc.IndexBuffer.Dispose();
            }

            _tileCaches = null;
        }

        private void InitializeLevelCache()
        {
            if (_level == null || _level.TileMap == null)
                return;

            _tileCaches = new TileRenderCache[_level.TileMap.Size.X,
                _level.TileMap.Size.Y,
                _level.TileMap.Layers.Count];
        }

        private static int CompareRendererYOrder(Renderer a, Renderer b)
        {
            return a.WorldBoundingBox.Max.Y.CompareTo(b.WorldBoundingBox.Max.Y);
        }

        private static void SetQuadIndices012213(ushort[] indices, ushort indexOffset, ushort vertexOffset, uint count)
        {
            for (uint i = 0; i < count; i++)
            {
                indices[indexOffset] = vertexOffset;
                indices[indexOffset + 1] = (ushort) (vertexOffset + 1);
                indices[indexOffset + 2] = (ushort) (vertexOffset + 2);
                indices[indexOffset + 3] = (ushort) (vertexOffset + 2);
                indices[indexOffset + 4] = (ushort) (vertexOffset + 1);
                indices[indexOffset + 5] = (ushort) (vertexOffset + 3);

                indexOffset += 6;
                vertexOffset += 4;
            }
        }

        private class RendererList
        {
            public Renderer[] Items;

            public int Count;
        }

        // Cached information for rendering tiles
        private class TileRenderCache
        {
            public VertexBuffer VertexBuffer;
            public IndexBuffer IndexBuffer;
            public Color Color = Color.White;
            public Texture2D Texture;
            public bool Valid;
        }
    }
}