﻿using System.Collections;
using Squid;

namespace Ramp.Rendering
{
    /// <summary>
    ///     A window that includes a title bar with a close button.
    /// </summary>
    public class FullWindow : Window
    {
        private readonly TitleBar _titleBar;

        /// <summary>
        ///		Raised when the window is closing.
        /// </summary>
        public EventWithArgs Closing;

        /// <summary>
        ///     Raised when the window is closed.
        /// </summary>
        public EventWithArgs Closed;

        public FullWindow()
        {
            Padding = new Margin(6);
            MaxSize = Point.Zero;

            _titleBar = new TitleBar
            {
                Parent = this,
                Dock = DockStyle.Top,
                Size = new Point(0, 35),
                Cursor = Cursors.Move,
                Style = "Frame",
                Margin = new Margin(0, 0, 0, -1),
                TextAlign = Alignment.MiddleLeft,
                BBCodeEnabled = true
            };

            _titleBar.MouseDown += delegate { if (CanMove) StartDrag(); };
            _titleBar.MouseUp += delegate { StopDrag(); };
            _titleBar.Button.MouseClick += OnCloseButtonClicked;

            Content = new Frame
            {
                Parent = this,
                Style = "Frame",
                Dock = DockStyle.Fill,
                Padding = new Margin(8)
            };
        }

        /// <summary>
        ///     A frame containing the contents of the window.
        /// </summary>
        public Frame Content { get; private set; }

        public bool CanMove { get; set; }

        public string ContentStyle
        {
            get { return Content.Style; }

            set { Content.Style = value; }
        }

        public bool CanClose
        {
            get { return _titleBar.Button.Visible; }

            set { _titleBar.Button.Visible = value; }
        }

        public string Title
        {
            get { return _titleBar.Text; }

            set { _titleBar.Text = value; }
        }

        public override void Close()
        {
            var closingEventArgs = new SquidEventArgs();
            OnClosing(closingEventArgs);
            if (closingEventArgs.Cancel)
                return;

            Animation.Custom(FadeAndClose());
        }

        protected virtual void OnClosing(SquidEventArgs e)
        {
            Closing?.Invoke(this, e);
        }

        protected virtual void OnClosed(SquidEventArgs e)
        {
            Closed?.Invoke(this, e);
        }

        private void OnCloseButtonClicked(Control sender, MouseEventArgs args)
        {
            Close();
        }

        private IEnumerator FadeAndClose()
        {
            Enabled = false;
            Animation.Size(new Point(Size.x, 10), 500);
            yield return Animation.Opacity(0, 500);
            base.Close();
            OnClosed(RenderUtil.EmptySquidEventArgs);
        }
    }
}