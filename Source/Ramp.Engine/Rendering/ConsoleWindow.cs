﻿using System;
using System.Collections.Generic;
using Ramp.Utilities.Text;
using Squid;

namespace Ramp.Rendering
{
    public sealed class ConsoleWindow : FullWindow
    {
        public ConsoleWindow()
        {
            Title = "Console";
            Resizable = true;
            CanClose = false;

            Input = new TextBox
            {
                Parent = Content,
                Style = "TextBox",
                Size = new Point(0, 35),
                Margin = new Margin(0, 4, 0, 0),
                Dock = DockStyle.Bottom,
                Text = "test 123"
            };

            var panel = new Panel
            {
                Parent = Content,
                Style = "TextBox",
                Dock = DockStyle.Fill
                //ClipFrame = { Margin = new Margin(8) }
            };

            RenderUtil.InitializeScrollbars(panel);

            // Create the text second otherwise it will not respect the input box's top margin when it fills out
            Text = new Label
            {
                Parent = panel.Content,
                Dock = DockStyle.Fill,
                TextAlign = Alignment.TopLeft,
                TextWrap = true,
                Margin = new Margin(8)
            };

            Input.KeyDown += OnInputKeyDown;
        }

        public event TypedEventHandler<ConsoleWindow, ConsoleCommandEventArgs> Command;

        public TextBox Input { get; private set; }

        public Label Text { get; set; }

        public void Clear()
        {
            Text.Text = string.Empty;
        }

        public void AddLine(string text = null)
        {
            if (text == null)
                text = string.Empty;

            if (Text.Text.Length == 0)
                Text.Text = text;
            else
                Text.Text += text + "\n";
        }

        private void OnInputKeyDown(Control sender, KeyEventArgs args)
        {
            if (args.Key != Keys.RETURN)
                return;

            string input = Input.Text;
            if (string.IsNullOrEmpty(input))
                return;

            Input.Text = string.Empty;

            string[] tokens = StringTokenizer.Tokenize(input);

            try
            {
                Command?.Invoke(this, new ConsoleCommandEventArgs(tokens));
            }
            catch (Exception ex)
            {
                AddLine($"Error ({ex.GetType().Name}): {ex.Message}");
            }
        }
    }

    public class ConsoleCommandEventArgs : EventArgs
    {
        public ConsoleCommandEventArgs(IList<string> tokens)
        {
            if (tokens == null)
                throw new ArgumentNullException(nameof(tokens));
            Tokens = tokens;
        }

        public IList<string> Tokens { get; private set; }

        public bool Handled { get; set; }
    }
}