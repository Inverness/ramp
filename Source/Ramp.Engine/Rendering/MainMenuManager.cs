﻿using System;
using System.Collections.Generic;
using System.Linq;
using NLog;
using Microsoft.Xna.Framework;
using Ramp.Input;
using Ramp.Session;
using Point = Squid.Point;

namespace Ramp.Rendering
{
    /// <summary>
    ///		Manages the main menu GUI and allows it to operate when a player does not exist.
    /// </summary>
    public class MainMenuManager : DrawableGameComponent
    {
        private static readonly Logger s_log = LogManager.GetCurrentClassLogger();

        private static readonly KeyValuePair<string, string>[] s_idleChoices =
        {
            new KeyValuePair<string, string>("New Game", "BeginSingleplayerGame"),
            new KeyValuePair<string, string>("Connect to Server", null),
            new KeyValuePair<string, string>("Options", null),
            new KeyValuePair<string, string>("Exit", "Exit")
        };

        private static readonly KeyValuePair<string, string>[] s_ingameChoices =
        {
            new KeyValuePair<string, string>("Continue", "ToggleMainMenu false"),
            new KeyValuePair<string, string>("Exit to Main Menu", "EndGame")
        };

        private readonly ICommandService _commandService;
        private readonly ISessionService _sessionService;
        private readonly IUIService _uiService;
        private readonly List<KeyValuePair<string, string>> _choices = new List<KeyValuePair<string, string>>();
        private MainMenu _mainMenu;

        public MainMenuManager(Game game)
            : base(game)
        {
            _commandService = game.Services.GetRequiredService<ICommandService>();
            _sessionService = game.Services.GetRequiredService<ISessionService>();
            _uiService = game.Services.GetRequiredService<IUIService>();

            Visible = false;
            Enabled = false;
        }

        public override void Initialize()
        {
            _sessionService.StateChanged += OnSessionStateChanged;

            _mainMenu = new MainMenu
            {
                Parent = _uiService.Desktop,
                Position = Point.Zero,
                Visible = true,
                TitleText = "Ramp Demo"
            };

            _commandService.AddCommand("ToggleMainMenu", OnCommandToggleMainMenu);

            _mainMenu.ChoiceSelected += OnMainMenuChoiceSelected;

            UpdateState();

            base.Initialize();
            //MainMenu.SetChoices("New Game", "Connect to Server", "Options", "Exit");
        }

        protected virtual void SetChoices(IEnumerable<KeyValuePair<string, string>> choices)
        {
            _choices.Clear();
            _choices.AddRange(choices);
            _mainMenu.SetChoices(_choices.Select(c => c.Key));
        }

        protected virtual void UpdateState()
        {
            _mainMenu.Visible = _sessionService.State == SessionState.Idle;
            if (_sessionService.State == SessionState.Idle)
            {
                //MainMenu.SetChoices("New Game", "Connect to Server", "Options", "Exit");
                SetChoices(s_idleChoices);
            }
            else if (_sessionService.State == SessionState.Ingame)
            {
                //MainMenu.SetChoices("Continue", "Exit");
                SetChoices(s_ingameChoices);
            }
        }

        protected override void LoadContent()
        {
            _mainMenu.Size = new Point(GraphicsDevice.DisplayMode.Width, GraphicsDevice.DisplayMode.Height);
        }

        protected virtual void OnCommandToggleMainMenu(ICommandService source, CommandArguments args)
        {
            if (_sessionService.State != SessionState.Ingame)
                return;

            _mainMenu.Visible = args.GetValueOrDefault(0, !_mainMenu.Visible);

            if (_sessionService.SessionType != SessionType.Singleplayer)
                return;

            _sessionService.LocalPlayer.PauseGame(_mainMenu.Visible);
            _sessionService.LocalPlayer.ScreenColor = _mainMenu.Visible ? new Color(0, 0, 0, 200) : (Color?) null;
        }

        protected virtual void OnMainMenuChoiceSelected(MainMenu sender, MainMenuChoiceSelectedEventArgs e)
        {
            string command = _choices[e.Index].Value;
            if (command != null)
                _commandService.Execute(command);

            // Only handle choices when no player exists to handle them
            //if (_sessionService.State == SessionState.Idle)
            //{
            //	switch (e.Index)
            //	{
            //		case 0:
            //			try
            //			{
            //				await _sessionService.BeginSingleplayerGame();
            //			}
            //			catch (Exception ex)
            //			{
            //				Log.Fatal("BeginSingleplayerGame() failed.", ex);
            //				throw;
            //			}
            //			break;
            //		case 1:
            //		case 2:
            //			break;
            //		case 3:
            //			Game.Exit();
            //			break;
            //	}
            //}
            //else if (_sessionService.State == SessionState.Ingame)
            //{
            //	switch (e.Index)
            //	{
            //		case 0:
            //			_sessionService.LocalPlayer.PauseGame(false);
            //			break;
            //		case 1:
            //			Game.Exit();
            //			break;
            //	}
            //}
        }

        protected virtual void OnSessionStateChanged(ISessionService sender, EventArgs eventArgs)
        {
            UpdateState();
        }
    }
}