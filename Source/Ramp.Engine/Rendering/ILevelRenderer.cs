using System;
using Microsoft.Xna.Framework;
using Ramp.Simulation;

namespace Ramp.Rendering
{
    /// <summary>
    ///     Renders a level scene.
    /// </summary>
    public interface ILevelRenderer
    {
        /// <summary>
        ///     Raised before the level is changed and provides the new level.
        /// </summary>
        event TypedEventHandler<ILevelRenderer, LevelEventArgs> LevelChanging;

        /// <summary>
        ///     Raised after the level is changed.
        /// </summary>
        event TypedEventHandler<ILevelRenderer, EventArgs> LevelChanged;

        /// <summary>
        ///     Position of the camera (viewport center) in tile coordinates.
        /// </summary>
        Vector2 Camera { get; set; }

        /// <summary>
        ///     The target level to be rendered.
        /// </summary>
        Level Level { get; set; }

        /// <summary>
        ///     Average time to render in miliseconds. Updated every second.
        /// </summary>
        double LastRenderTime { get; }

        double LastSpriteRenderTime { get; }

        Color? ScreenColor { get; set; }

        bool ShowBoundingBoxes { get; set; }
    }
}