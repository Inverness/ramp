﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Ramp.Data;
using Ramp.Naming;

namespace Ramp.Rendering
{
    /// <summary>
    /// Allows edits to be made to a copy of a texture. UpdateTarget() must be called after changes are made.
    /// </summary>
    public sealed class TextureEditor
    {
        private readonly string _targetName;
        private readonly AssetReference<Texture2D> _source = new AssetReference<Texture2D>();
        private uint[] _sourceData;
        private Asset<Texture2D> _target;
        private bool _dirty;
        private readonly Dictionary<Color, Color> _colorReplacements = new Dictionary<Color, Color>();

        public TextureEditor(string targetNamePrefix)
        {
            _targetName = NamedDirectory.Current.GenerateName(targetNamePrefix);
        }

        /// <summary>
        /// Gets or sets the name of an existing source asset to edit.
        /// </summary>
        public string SourceName
        {
            get { return _source.Name; }

            set { _source.Name = value; }
        }

        /// <summary>
        /// Gets the name of the asset to be generated.
        /// </summary>
        public string TargetName => _targetName;
        
        /// <summary>
        /// Gets the target asset if it exists.
        /// </summary>
        public Asset<Texture2D> Target => _target;

        public Color? GetColorReplacement(Color source)
        {
            Color result;
            return _colorReplacements.TryGetValue(source, out result) ? (Color?) result : null;
        }
        
        public void SetColorReplacement(Color source, Color? target)
        {
            if (target.HasValue)
            {
                Color existing;
                if (!_colorReplacements.TryGetValue(source, out existing) || existing != target.Value)
                {
                    _colorReplacements[source] = target.Value;
                    _dirty = true;
                }
            }
            else
            {
                _dirty |= _colorReplacements.Remove(source);
            }
        }

        public void UpdateTarget()
        {
            Texture2D source = _source.LoadTargetAsset();

            if (source == null)
            {
                // If it's still loading, schedule UpdateTarget() to occurr in the future.
                if (_source.Status == ReferenceStatus.Loading)
                    _source.LoadTargetAssetAsync().ContinueWith(t => UpdateTarget());
                return;
            }

            //
            // If the target is null, need to duplicate the source
            //

            if (_target == null)
            {
                _sourceData = new uint[source.Width * source.Height];
                source.GetData(_sourceData);

                var targetTexture = new Texture2D(source.GraphicsDevice, source.Width, source.Height);
                targetTexture.SetData(_sourceData);

                _target = new Asset<Texture2D>(targetTexture, _targetName, null, _targetName, null);
            }

            //
            // If the editing parameters are dirty, need to reapply them to the generated asset
            //

            if (_dirty)
            {
                uint[] sourceData = _sourceData;

                // Refresh data from source before applying changes.
                source.GetData(sourceData);

                foreach (KeyValuePair<Color, Color> replacement in _colorReplacements)
                {
                    uint packedKey = replacement.Key.PackedValue;

                    // JIT should eliminate bounds-checking here
                    for (int i = 0; i < sourceData.Length; i++)
                    {
                        if (sourceData[i] == packedKey)
                            sourceData[i] = replacement.Value.PackedValue;
                    }
                }

                // Apply changes to the texture.
                _target.Target.SetData(sourceData);

                _dirty = false;
            }
        }
    }
}
