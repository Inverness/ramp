using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Ramp.Rendering
{
    // This code is responsible for handling the drawing of common controls for our UI.
    public sealed partial class UIManager
    {
        private Color _borderBackgroundColor;
        private Rectangle _borderFill;
        private Rectangle _borderTl;
        private Rectangle _borderTr;
        private Rectangle _borderBl;
        private Rectangle _borderBr;
        private Rectangle _progressBarLeftFill;
        private Rectangle _progressBarRightFill;
        private Rectangle _progressBarFill;
        private Rectangle _smallBorderTl;
        private Rectangle _smallBorderTr;
        private Rectangle _smallBorderBl;
        private Rectangle _smallBorderBr;

        private Texture2D _borderTexture;

        private void InitializeControls()
        {
            _borderFill = new Rectangle(32, 32, 32, 32);
            _borderTl = new Rectangle(0, 0, 32, 32);
            _borderBl = new Rectangle(0, 64, 32, 32);
            _borderBr = new Rectangle(64, 64, 32, 32);
            _borderTr = new Rectangle(64, 0, 32, 32);
            _borderBackgroundColor = new Color(0, 0, 0, 196);
            _progressBarLeftFill = new Rectangle(32, 96, 16, 32);
            _progressBarFill = new Rectangle(48, 96, 16, 32);
            _progressBarRightFill = new Rectangle(64, 96, 16, 32);
            _smallBorderTl = new Rectangle(0, 96, 16, 16);
            _smallBorderTr = new Rectangle(16, 96, 16, 16);
            _smallBorderBl = new Rectangle(0, 112, 16, 16);
            _smallBorderBr = new Rectangle(16, 112, 16, 16);
        }

        private void DrawBorder(Rectangle rectangle, Color? color = null, bool noBackground = false)
        {
            if (!_drawing)
                throw new InvalidOperationException("not drawing");
            if (!color.HasValue)
                color = Color.White;

            int minW = _borderTl.Width + _borderTr.Width;
            int minH = _borderTl.Height + _borderBl.Height;

            if (rectangle.Width < minW)
                rectangle.Width = minW;
            if (rectangle.Height < minH)
                rectangle.Height = minH;

            if (!noBackground && _borderBackgroundColor.A > 0)
                _spriteBatch.Draw(_singlePixelTexture, rectangle, _borderBackgroundColor);

            if (color.Value.A == 0)
                return;

            _spriteBatch.Draw(_borderTexture,
                              new Vector2(rectangle.X, rectangle.Y),
                              _borderTl, color.Value);
            _spriteBatch.Draw(_borderTexture,
                              new Vector2(rectangle.X, (rectangle.Y + rectangle.Height) - _borderBl.Height),
                              _borderBl, color.Value);
            _spriteBatch.Draw(_borderTexture,
                              new Vector2((rectangle.X + rectangle.Width) - _borderBr.Width,
                                          (rectangle.Y + rectangle.Height) - _borderBl.Height),
                              _borderBr, color.Value);
            _spriteBatch.Draw(_borderTexture,
                              new Vector2((rectangle.X + rectangle.Width) - _borderBr.Width, rectangle.Y),
                              _borderTr, color.Value);

            //_spriteBatch.Draw(_borderTexture,
            //                  new Vector2(rectangle.Left, rectangle.Top),
            //                  BorderTl, color.Value);
            //_spriteBatch.Draw(_borderTexture,
            //                  new Vector2(rectangle.Left, rectangle.Bottom - BorderBl.Height),
            //                  BorderBl, color.Value);
            //_spriteBatch.Draw(_borderTexture,
            //                  new Vector2(rectangle.Right - BorderBr.Width, rectangle.Bottom - BorderBl.Height),
            //                  BorderBr, color.Value);
            //_spriteBatch.Draw(_borderTexture,
            //                  new Vector2(rectangle.Right - BorderBr.Width, rectangle.Top),
            //                  BorderTr, color.Value);
        }

        private void DrawSmallBorder(Rectangle rectangle, Color? color = null, bool noBackground = false)
        {
            if (!_drawing)
                throw new InvalidOperationException("not drawing");
            if (!color.HasValue)
                color = Color.White;

            int minW = _smallBorderTl.Width + _smallBorderTr.Width;
            int minH = _smallBorderTl.Height + _smallBorderBl.Height;

            if (rectangle.Width < minW)
                rectangle.Width = minW;
            if (rectangle.Height < minH)
                rectangle.Height = minH;

            if (!noBackground && _borderBackgroundColor.A > 0)
                _spriteBatch.Draw(_singlePixelTexture, rectangle, _borderBackgroundColor);

            if (color.Value.A == 0)
                return;

            _spriteBatch.Draw(_borderTexture,
                              new Vector2(rectangle.X, rectangle.Y),
                              _smallBorderTl,
                              color.Value);
            _spriteBatch.Draw(_borderTexture,
                              new Vector2(rectangle.X, (rectangle.Y + rectangle.Height) - _smallBorderBl.Height),
                              _smallBorderBl,
                              color.Value);
            _spriteBatch.Draw(_borderTexture,
                              new Vector2((rectangle.X + rectangle.Width) - _smallBorderBr.Width,
                                          (rectangle.Y + rectangle.Height) - _smallBorderBl.Height),
                              _smallBorderBr,
                              color.Value);
            _spriteBatch.Draw(_borderTexture,
                              new Vector2((rectangle.X + rectangle.Width) - _smallBorderBr.Width, rectangle.Y),
                              _smallBorderTr,
                              color.Value);
        }

        private void DrawWindow(string text, Rectangle rectangle, Color? color = null)
        {
            DrawBorder(rectangle, color);
            if (text == null)
                return;

            if (!color.HasValue)
                color = Color.White;
            _spriteBatch.DrawString(_largeFont, text, new Vector2(rectangle.X + 6, rectangle.Y + 6),
                                    color.Value);
        }

        private void DrawProgressBar(Vector2 position, int size, float progress, Color color)
        {
            if (!_drawing)
                throw new InvalidOperationException("not drawing");
            if (progress < 0 && progress > 1)
                throw new ArgumentOutOfRangeException(nameof(progress));
            // must be at least big enough to draw left and right ends
            if (size < 2)
                throw new ArgumentOutOfRangeException(nameof(size));

            // Progress bar parts need to be uniform in size
            int width = _progressBarFill.Width;
            int height = _progressBarFill.Height;
            //float widthPercent = (float)width / (size * width);

            // Background and borders
            var rect = new Rectangle((int) position.X, (int) position.Y, size * width, height);
            DrawSmallBorder(rect, color);

            // Fill the inner progress part
            if (progress < 0)
                return;
            const int border = 6;
            var fillRect = new Rectangle(rect.X + border, rect.Y + border,
                                         (int) ((rect.Width - border * 2) * progress),
                                         rect.Height - border * 2);
            if (fillRect.Width > 0)
                _spriteBatch.Draw(_singlePixelTexture, fillRect, color);

            //if (progress > 0)
            //{
            //    var part = ProgressBarLeftFill;
            //    if (progress < widthPercent)
            //        part.Width = (int)(part.Width * widthPercent);
            //    _spriteBatch.Draw(_borderTexture, new Vector2(rect.Left, rect.Top), part, color);
            //}
            //float rightBeginPercent = widthPercent * (size - 1);
            //if (progress >= rightBeginPercent)
            //{
            //    var part = ProgressBarRightFill;
            //    if (progress < 1)
            //        part.Width = (int)(part.Width * (progress - rightBeginPercent) / widthPercent);
            //    _spriteBatch.Draw(_borderTexture, new Vector2(rect.Right - width, rect.Top), part, color);
            //}
            //if (progress >= widthPercent)
            //{
            //    for (int i = 1; i < progress - 1; i++)
            //        _spriteBatch.Draw(_borderTexture, new Vector2(rect.Left + width * i, rect.Top), ProgressBarFill, color);
            //}
            //if (!_drawing)
            //    throw new InvalidOperationException("not drawing");
            //if (progress > size)
            //    throw new ArgumentOutOfRangeException("progress");
            //// must be at least big enough to draw left and right ends
            //if (size < 2)
            //    throw new ArgumentOutOfRangeException("size < 2");
            //if (progress < 0)
            //    throw new ArgumentOutOfRangeException("progress < 0");

            //if (progress > 0)
            //    _spriteBatch.Draw(_borderTexture, new Vector2(rect.Left, rect.Top), ProgressBarLeftFill, color);
            //if (progress > 1)
            //    _spriteBatch.Draw(_borderTexture, new Vector2(rect.Right - width, rect.Top), ProgressBarRightFill, color);
            //if (progress > 2)
            //{
            //    for (int i = 1; i < progress - 1; i++)
            //        _spriteBatch.Draw(_borderTexture, new Vector2(rect.Left + width * i, rect.Top), ProgressBarFill, color);
            //}
        }
    }
}