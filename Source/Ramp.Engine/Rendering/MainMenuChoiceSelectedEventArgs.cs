﻿using System;

namespace Ramp.Rendering
{
    public class MainMenuChoiceSelectedEventArgs : EventArgs
    {
        public MainMenuChoiceSelectedEventArgs(string text, int index)
        {
            Text = text;
            Index = index;
        }

        public string Text { get; private set; }

        public int Index { get; private set; }
    }
}