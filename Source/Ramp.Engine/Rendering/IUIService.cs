using Ramp.Input;
using Squid;

namespace Ramp.Rendering
{
    /// <summary>
    ///     Provides UI rendering and input handling.
    /// </summary>
    public interface IUIService : IInputHandler
    {
        ConsoleWindow Console { get; }

        Desktop Desktop { get; }

        bool Focused { get; }

        bool InputEnabled { get; set; }

        bool DebugTextVisible { get; set; }

        bool PlayerHealthVisible { get; set; }

        bool EventMessageVisible { get; set; }

        //bool MenuVisible { get; set; }

        //ushort ActiveMenu { get; set; }

        void AddEventMessage(string message);

        void DrawLoadingScreen();
    }
}