using Squid;

namespace Ramp.Rendering
{
    public class TitleBar : Label
    {
        public TitleBar()
        {
            Button = new Button
            {
                Size = new Point(30, 30),
                Style = "Button",
                Tooltip = "Close Window",
                Dock = DockStyle.Right,
                Margin = new Margin(0, 8, 8, 8),
                Text = "X"
            };

            Elements.Add(Button);
        }

        public Button Button { get; private set; }
    }
}