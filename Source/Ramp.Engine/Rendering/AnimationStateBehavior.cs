namespace Ramp.Rendering
{
    /*
	public sealed class AnimationStateBehavior : Behavior
	{
		[JsonProperty("States")] private readonly Dictionary<string, Tuple<string, string>> _states =
			new Dictionary<string, Tuple<string, string>>();

		[JsonProperty("Renderer")] private AnimationRenderer _renderer;
		[JsonProperty("StateName")] private string _currentState;
		[JsonProperty("DefaultStateName")] private string _defaultStateName;

		public TypedEventHandler<AnimationStateBehavior, EventArgs> StateChanged;

		public IReadOnlyDictionary<string, Tuple<string, string>> States
		{
			get { return _states; }
		}

		public string CurrentState
		{
			get { return _currentState; }
		}

		public AnimationRenderer Renderer
		{
			get { return _renderer; }
		}

		protected internal override void OnCreated()
		{
			base.OnCreated();
			Require(ref _renderer, true);
		}

		public bool SetState(string name, string frameSet = null)
		{
			if (name == null)
				return true;
			AssertEnabled();

			Tuple<string, string> aniName = _states[name];
			try
			{
				_renderer.Play(aniName.Item1, frameSet ?? aniName.Item2);
			}
			catch (AssetNotFoundException)
			{
				return false;
			}
			_currentState = name;

			EventUtility.Invoke(StateChanged, this, EventArgs.Empty);
			return true;
		}

		//public async Task<bool> SetStateLatent(string name, string frameSet = null)
		//{
		//	if (!SetState(name, frameSet))
		//		return false;
		//	await _renderer.WaitFinished();
		//	return true;
		//}

		public void AddState(string name, string animationName, string frameSetName = null)
		{
			if (name == null)
				throw new ArgumentNullException("name");
			_states[name] = Tuple.Create(animationName, frameSetName);
			if (_defaultStateName == null)
				_defaultStateName = name;
		}

		public void RemoveState(string name)
		{
			_states.Remove(name);
			if (_defaultStateName == name)
				_defaultStateName = null;
		}
	}
	*/
}