﻿using System;
using NLog;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Ramp.Data;
using Ramp.Simulation;
using Ramp.Utilities.Collections;

namespace Ramp.Rendering
{
    /// <summary>
    ///     Renders a particle system.
    /// </summary>
    public class ParticleSystemRenderer : Renderer
    {
        protected readonly Random Random = new Random();

        private const int MaximumPoolCapacity = 200;

        private static readonly Logger s_log = LogManager.GetCurrentClassLogger();

        private readonly Pool<Particle> _pool = new Pool<Particle>(MaximumPoolCapacity);
        private readonly LinkedArrayList<Particle> _particles = new LinkedArrayList<Particle>();

        private readonly MovementBehavior _movement;
        private BoundingBox _particleBounds;
        private readonly TimeSpan _spawnTime;
        private TimeSpan _nextEmitTime;
        private TimeSpan _currentTime;
        private ushort _maxParticles;

        public ParticleSystemRenderer()
        {
            Actor.IsActive = true;

            // Single texture for now
            StartTexture = new AssetReference<Texture2D>();
            Duration = TimeSpan.MaxValue;
            Loop = true;
            StartColor = Color.White;
            MaxParticles = ushort.MaxValue;

            GetComponent(out _movement);

            _spawnTime = Level.Time;
        }

        public override BoundingBox LocalBoundingBox => _particleBounds;

        /// <summary>
        ///     Gets or sets the duration the particle system will emit for.
        /// </summary>
        public TimeSpan Duration { get; set; }

        /// <summary>
        ///     Gets or sets the number of particles emitted during each automatic emission.
        /// </summary>
        public ushort EmitCount { get; set; }

        /// <summary>
        ///     Gets or sets the rate at which particle emissions occurr.
        /// </summary>
        public TimeSpan EmitRate { get; set; }

        /// <summary>
        ///     Gets or sets whether the particle system emission will loop.
        /// </summary>
        public bool Loop { get; set; }

        /// <summary>
        ///     Gets or sets the starting lifetime of a particle.
        /// </summary>
        public TimeSpan StartLifetime { get; set; }

        /// <summary>
        ///     Gets or sets the starting speed of a particle.
        /// </summary>
        public float StartSpeed { get; set; }

        /// <summary>
        ///     Gets or sets the starting size of a particle.
        /// </summary>
        public Vector2 StartSize { get; set; }

        /// <summary>
        ///     Gets or sets the startion rotation of a particle.
        /// </summary>
        public float StartRotation { get; set; }

        /// <summary>
        ///     Gets or sets the starting color of a particle.
        /// </summary>
        public Color StartColor { get; set; }

        /// <summary>
        ///     Gets or sets the starting texture name of a particle.
        /// </summary>
        public AssetReference<Texture2D> StartTexture { get; private set; }

        /// <summary>
        ///     Gets or sets the factor of the actor's velocity that a new particle will inherit.
        /// </summary>
        public float InheritVelocity { get; set; }

        /// <summary>
        ///     Gets or sets whether particles will be simulated relative to the world or the actor.
        /// </summary>
        public bool WorldSimulation { get; set; }

        /// <summary>
        ///     Gets or sets the maximum number of particles that can exist.
        /// </summary>
        public ushort MaxParticles
        {
            get { return _maxParticles; }

            set
            {
                _maxParticles = value;
                _pool.Capacity = Math.Min((int) value, MaximumPoolCapacity);
            }
        }

        /// <summary>
        ///     Gets or sets the render mode for particles.
        /// </summary>
        public SpriteRenderMode RenderMode { get; set; }

        public override void Update(TimeSpan elapsed)
        {
            base.Update(elapsed);

            if (!Enabled)
                return;

            _currentTime += elapsed;

            // Disable the particle emitter if we're beyond its duration
            if (_currentTime >= _spawnTime + Duration)
            {
                Enabled = false;
                return;
            }

            // Update all particles
            _particleBounds.Min = Vector3.Zero;
            _particleBounds.Max = Vector3.Zero;

            for (int i = _particles.FirstIndex; i != -1;)
            {
                Particle p = _particles.GetItemAndNextIndex(i, out i);

                if (UpdateParticle(p, elapsed))
                    UpdateParticleBounds(p);
                else
                    DestroyParticle(p);
            }

            // Emit particles automatically 
            if (EmitCount != 0 && Level.Time >= _nextEmitTime)
            {
                _nextEmitTime = Level.Time + EmitRate;
                Emit(EmitCount);
            }
        }

        public override void Draw(ILevelObjectRenderer ri)
        {
            VerifyEnabled();

            if (_particles.Count == 0)
                return;

            Vector2 origin = WorldSimulation ? Vector2.Zero : Transform.Location;

            for (int i = _particles.FirstIndex; i != -1;)
            {
                Particle p = _particles.GetItemAndNextIndex(i, out i);

                if (p.Texture != null)
                    ri.DrawSprite(p.Texture, origin + p.Position, p.Color, p.Size, rotation: p.Rotation,
                                  renderMode: RenderMode);
            }
        }

        /// <summary>
        ///     Emits particles. If the amount of particles existing exceeds MaxParticles, old particles will be destroyed.
        /// </summary>
        /// <param name="count"> The number of particles to emit. </param>
        public void Emit(ushort count)
        {
            VerifyNotDisposed();

            Validate.ArgumentInRange(count > 0, "count");

            for (int i = 0; i < count; i++)
                EmitParticle();
        }

        /// <summary>
        ///     Initializes a new particle object after it has been acquired from the pool.
        /// </summary>
        /// <param name="p"> A new particle. </param>
        protected virtual void InitializeParticle(Particle p)
        {
            Validate.ArgumentNotNull(p, "p");

            p.Color = StartColor;
            p.ElapsedTime = TimeSpan.Zero;
            p.Lifetime = StartLifetime;
            p.Position = (WorldSimulation ? Transform.Location : Vector2.Zero) - (StartSize / 2);
            p.Rotation = StartRotation;
            p.Size = StartSize;
            p.Texture = StartTexture.LoadTargetAsset();
            p.Velocity = _movement?.Velocity * InheritVelocity ?? Vector2.Zero;
        }

        /// <summary>
        ///     Updates a particle object's elapsed time, location, and rotation.
        /// </summary>
        /// <param name="p"> A particle. </param>
        /// <param name="elapsed"> The elapsed time since the previous component update. </param>
        /// <returns> True if the particle is still alive, false if it will now be considered dead. </returns>
        protected virtual bool UpdateParticle(Particle p, TimeSpan elapsed)
        {
            Validate.ArgumentNotNull(p, "p");

            var elapsedSeconds = (float) elapsed.TotalSeconds;

            p.ElapsedTime += elapsed;
            if (p.ElapsedTime >= p.Lifetime)
                return false;

            p.Position += p.Velocity * elapsedSeconds;
            p.Rotation += p.AngularVelocity * elapsedSeconds;

            return true;
        }

        // Emits new particles and culls old particles if the count has exceeded MaxParticles
        private void EmitParticle()
        {
            Particle p = _pool.Acquire() ?? new Particle();
            InitializeParticle(p);
            p.Index = _particles.AddLast(p);

            while (_particles.Count > MaxParticles)
                DestroyParticle(_particles.First);
        }

        // Destroy a particle and release it back into the pool.
        private void DestroyParticle(Particle p)
        {
            _particles.RemoveAt(p.Index);
            p.Index = -1;
            _pool.Release(p);
        }

        // Updates the bounds used to determine when the particle system is visible.
        private void UpdateParticleBounds(Particle p)
        {
            float hw = p.Size.X / 2;
            float hh = p.Size.Y / 2;

            float minX = p.Position.X - hw;
            float minY = p.Position.Y - hh;
            float maxX = p.Position.X + hw;
            float maxY = p.Position.Y + hh;

            _particleBounds.Min.X = Math.Min(minX, _particleBounds.Min.X);
            _particleBounds.Min.Y = Math.Min(minY, _particleBounds.Min.Y);
            _particleBounds.Max.X = Math.Max(maxX, _particleBounds.Max.X);
            _particleBounds.Max.Y = Math.Max(maxY, _particleBounds.Max.Y);
        }
    }
}