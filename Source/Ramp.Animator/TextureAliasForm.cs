﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Ramp.Animator
{
    public partial class TextureAliasForm : Form
    {
        private const string TextBoxLineEnding = "\r\n";
        private static readonly char[] KeyDelim = { '=' };

        public TextureAliasForm()
        {
            InitializeComponent();

            lblMaxAliases.Text = "";
        }

        public void PullData(IDictionary<string, string> data)
        {
            var text = new StringBuilder();
            foreach (KeyValuePair<string, string> alias in data)
            {
                text.Append(alias.Key).Append(" = ").Append(alias.Value).Append(TextBoxLineEnding);
            }
            txtText.Text = text.ToString();
        }

        public void PushData(IDictionary<string, string> data)
        {
            string[] lines = txtText.Text.Split(new[] { TextBoxLineEnding }, StringSplitOptions.None);

            foreach (string line in lines)
            {
                if (line.Length == 0)
                    continue;
                string[] lineParts = line.Split(KeyDelim, 2);
                data[lineParts[0].Trim()] = lineParts[1].Trim();
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        protected override void OnFormClosed(FormClosedEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
                DialogResult = DialogResult.Cancel;

            base.OnFormClosed(e);
        }
    }
}