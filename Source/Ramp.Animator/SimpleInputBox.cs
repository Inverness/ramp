﻿using System;
using System.Windows.Forms;

namespace Ramp.Animator
{
    public partial class SimpleInputBox : Form
    {
        public SimpleInputBox()
        {
            InitializeComponent();
        }

        public DialogResult Result { get; private set; }

        private void btnOk_Click(object sender, EventArgs e)
        {
            Result = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Result = DialogResult.Cancel;
        }

        private void SimpleInputBox_FormClosing(object sender, FormClosingEventArgs e)
        {
            Result = DialogResult.Cancel;
        }
    }
}