﻿using System;
using System.IO;
using System.Windows.Forms;

namespace Ramp.Animator
{
    internal static class Program
    {
        /// <summary>
        ///     The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            Directory.SetCurrentDirectory(Path.Combine(Path.GetDirectoryName(Application.ExecutablePath) ?? "", ".."));

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }
}