﻿using System.ComponentModel;
using Ramp.Rendering;

namespace Ramp.Animator
{
    internal class AnimationView
    {
        public AnimationView(Animation animation)
        {
            Animation = animation;
        }

        [Browsable(false)]
        public Animation Animation { get; private set; }

        [Category("Animation")]
        public bool Directional
        {
            get { return Animation.Directional; }

            set { Animation.Directional = value; }
        }

        [Category("Animation")]
        public bool Loop
        {
            get { return Animation.Loop; }

            set { Animation.Loop = value; }
        }
    }

    internal class AnimationSpriteTypeView
    {
        private readonly AnimationSpriteType _spriteType;

        public AnimationSpriteTypeView(string name, AnimationSpriteType spriteType)
        {
            Name = name;
            _spriteType = spriteType;
        }

        public string Name { get; }

        public string TextureName
        {
            get { return _spriteType.TextureName; }

            set { _spriteType.TextureName = value; }
        }
    }
}