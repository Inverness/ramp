﻿namespace Ramp.Animator
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.newToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.openToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.saveToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.cutToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.copyToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.pasteToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.helpToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.label1 = new System.Windows.Forms.Label();
            this.cbFrameLists = new System.Windows.Forms.ComboBox();
            this.tbFrames = new System.Windows.Forms.TrackBar();
            this.btnPlay = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnNewFrame = new System.Windows.Forms.Button();
            this.btnDeleteFrame = new System.Windows.Forms.Button();
            this.lblFrameNumber = new System.Windows.Forms.Label();
            this.btnFrameListAdd = new System.Windows.Forms.Button();
            this.btnFrameListRemove = new System.Windows.Forms.Button();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.pgAnimation = new System.Windows.Forms.PropertyGrid();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.pgFrame = new System.Windows.Forms.PropertyGrid();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.pgSprite = new System.Windows.Forms.PropertyGrid();
            this.btnTextureAliases = new System.Windows.Forms.Button();
            this.ucWorkspace = new Ramp.Animator.Controls.AnimationWorkspace();
            this.ucSpriteList = new Ramp.Animator.Controls.SpriteList();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbFrames)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripButton,
            this.openToolStripButton,
            this.saveToolStripButton,
            this.toolStripSeparator,
            this.cutToolStripButton,
            this.copyToolStripButton,
            this.pasteToolStripButton,
            this.toolStripSeparator1,
            this.helpToolStripButton});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1200, 25);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            this.toolStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.toolStrip1_ItemClicked);
            // 
            // newToolStripButton
            // 
            this.newToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.newToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("newToolStripButton.Image")));
            this.newToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.newToolStripButton.Name = "newToolStripButton";
            this.newToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.newToolStripButton.Text = "&New";
            // 
            // openToolStripButton
            // 
            this.openToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.openToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("openToolStripButton.Image")));
            this.openToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.openToolStripButton.Name = "openToolStripButton";
            this.openToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.openToolStripButton.Text = "&Open";
            // 
            // saveToolStripButton
            // 
            this.saveToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("saveToolStripButton.Image")));
            this.saveToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveToolStripButton.Name = "saveToolStripButton";
            this.saveToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.saveToolStripButton.Text = "&Save";
            // 
            // toolStripSeparator
            // 
            this.toolStripSeparator.Name = "toolStripSeparator";
            this.toolStripSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // cutToolStripButton
            // 
            this.cutToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.cutToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("cutToolStripButton.Image")));
            this.cutToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cutToolStripButton.Name = "cutToolStripButton";
            this.cutToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.cutToolStripButton.Text = "C&ut";
            // 
            // copyToolStripButton
            // 
            this.copyToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.copyToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("copyToolStripButton.Image")));
            this.copyToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.copyToolStripButton.Name = "copyToolStripButton";
            this.copyToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.copyToolStripButton.Text = "&Copy";
            // 
            // pasteToolStripButton
            // 
            this.pasteToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.pasteToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("pasteToolStripButton.Image")));
            this.pasteToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.pasteToolStripButton.Name = "pasteToolStripButton";
            this.pasteToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.pasteToolStripButton.Text = "&Paste";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // helpToolStripButton
            // 
            this.helpToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.helpToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("helpToolStripButton.Image")));
            this.helpToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.helpToolStripButton.Name = "helpToolStripButton";
            this.helpToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.helpToolStripButton.Text = "He&lp";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 740);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1200, 22);
            this.statusStrip1.TabIndex = 3;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(953, 493);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Frame Lists";
            // 
            // cbFrameLists
            // 
            this.cbFrameLists.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbFrameLists.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFrameLists.FormattingEnabled = true;
            this.cbFrameLists.Location = new System.Drawing.Point(956, 509);
            this.cbFrameLists.Name = "cbFrameLists";
            this.cbFrameLists.Size = new System.Drawing.Size(226, 21);
            this.cbFrameLists.Sorted = true;
            this.cbFrameLists.TabIndex = 14;
            this.cbFrameLists.SelectedIndexChanged += new System.EventHandler(this.cbFrameLists_SelectedIndexChanged);
            // 
            // tbFrames
            // 
            this.tbFrames.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFrames.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.tbFrames.LargeChange = 1;
            this.tbFrames.Location = new System.Drawing.Point(219, 660);
            this.tbFrames.Maximum = 1;
            this.tbFrames.Name = "tbFrames";
            this.tbFrames.Size = new System.Drawing.Size(725, 45);
            this.tbFrames.TabIndex = 15;
            this.tbFrames.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.tbFrames.ValueChanged += new System.EventHandler(this.tbFrames_ValueChanged);
            // 
            // btnPlay
            // 
            this.btnPlay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPlay.Location = new System.Drawing.Point(219, 711);
            this.btnPlay.Name = "btnPlay";
            this.btnPlay.Size = new System.Drawing.Size(75, 23);
            this.btnPlay.TabIndex = 16;
            this.btnPlay.Text = "&Play";
            this.btnPlay.UseVisualStyleBackColor = true;
            // 
            // btnStop
            // 
            this.btnStop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnStop.Location = new System.Drawing.Point(300, 711);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(75, 23);
            this.btnStop.TabIndex = 17;
            this.btnStop.Text = "&Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            // 
            // btnNewFrame
            // 
            this.btnNewFrame.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnNewFrame.Location = new System.Drawing.Point(381, 711);
            this.btnNewFrame.Name = "btnNewFrame";
            this.btnNewFrame.Size = new System.Drawing.Size(75, 23);
            this.btnNewFrame.TabIndex = 18;
            this.btnNewFrame.Text = "&New Frame";
            this.btnNewFrame.UseVisualStyleBackColor = true;
            this.btnNewFrame.Click += new System.EventHandler(this.btnNewFrame_Click);
            // 
            // btnDeleteFrame
            // 
            this.btnDeleteFrame.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDeleteFrame.Location = new System.Drawing.Point(462, 711);
            this.btnDeleteFrame.Name = "btnDeleteFrame";
            this.btnDeleteFrame.Size = new System.Drawing.Size(75, 23);
            this.btnDeleteFrame.TabIndex = 19;
            this.btnDeleteFrame.Text = "&Del Frame";
            this.btnDeleteFrame.UseVisualStyleBackColor = true;
            this.btnDeleteFrame.Click += new System.EventHandler(this.btnDeleteFrame_Click);
            // 
            // lblFrameNumber
            // 
            this.lblFrameNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFrameNumber.AutoSize = true;
            this.lblFrameNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFrameNumber.Location = new System.Drawing.Point(962, 673);
            this.lblFrameNumber.Name = "lblFrameNumber";
            this.lblFrameNumber.Size = new System.Drawing.Size(31, 20);
            this.lblFrameNumber.TabIndex = 20;
            this.lblFrameNumber.Text = "0/0";
            this.lblFrameNumber.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnFrameListAdd
            // 
            this.btnFrameListAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFrameListAdd.Location = new System.Drawing.Point(956, 536);
            this.btnFrameListAdd.Name = "btnFrameListAdd";
            this.btnFrameListAdd.Size = new System.Drawing.Size(22, 23);
            this.btnFrameListAdd.TabIndex = 21;
            this.btnFrameListAdd.Text = "A";
            this.btnFrameListAdd.UseVisualStyleBackColor = true;
            this.btnFrameListAdd.Click += new System.EventHandler(this.btnFrameListAdd_Click);
            // 
            // btnFrameListRemove
            // 
            this.btnFrameListRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFrameListRemove.Location = new System.Drawing.Point(985, 536);
            this.btnFrameListRemove.Name = "btnFrameListRemove";
            this.btnFrameListRemove.Size = new System.Drawing.Size(22, 23);
            this.btnFrameListRemove.TabIndex = 22;
            this.btnFrameListRemove.Text = "R";
            this.btnFrameListRemove.UseVisualStyleBackColor = true;
            this.btnFrameListRemove.Click += new System.EventHandler(this.btnFrameListRemove_Click);
            // 
            // colorDialog1
            // 
            this.colorDialog1.AnyColor = true;
            this.colorDialog1.Color = System.Drawing.Color.White;
            // 
            // pgAnimation
            // 
            this.pgAnimation.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pgAnimation.HelpVisible = false;
            this.pgAnimation.Location = new System.Drawing.Point(6, 19);
            this.pgAnimation.Name = "pgAnimation";
            this.pgAnimation.PropertySort = System.Windows.Forms.PropertySort.Alphabetical;
            this.pgAnimation.Size = new System.Drawing.Size(226, 110);
            this.pgAnimation.TabIndex = 28;
            this.pgAnimation.ToolbarVisible = false;
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.pgAnimation);
            this.groupBox4.Location = new System.Drawing.Point(950, 355);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(238, 135);
            this.groupBox4.TabIndex = 28;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Animation";
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox5.Controls.Add(this.pgFrame);
            this.groupBox5.Location = new System.Drawing.Point(950, 214);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(238, 135);
            this.groupBox5.TabIndex = 27;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Frame";
            // 
            // pgFrame
            // 
            this.pgFrame.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pgFrame.HelpVisible = false;
            this.pgFrame.Location = new System.Drawing.Point(6, 19);
            this.pgFrame.Name = "pgFrame";
            this.pgFrame.PropertySort = System.Windows.Forms.PropertySort.Alphabetical;
            this.pgFrame.Size = new System.Drawing.Size(226, 110);
            this.pgFrame.TabIndex = 29;
            this.pgFrame.ToolbarVisible = false;
            // 
            // groupBox6
            // 
            this.groupBox6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox6.Controls.Add(this.pgSprite);
            this.groupBox6.Location = new System.Drawing.Point(950, 28);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(238, 180);
            this.groupBox6.TabIndex = 26;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Sprite";
            // 
            // pgSprite
            // 
            this.pgSprite.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pgSprite.HelpVisible = false;
            this.pgSprite.Location = new System.Drawing.Point(6, 19);
            this.pgSprite.Name = "pgSprite";
            this.pgSprite.PropertySort = System.Windows.Forms.PropertySort.Alphabetical;
            this.pgSprite.Size = new System.Drawing.Size(226, 155);
            this.pgSprite.TabIndex = 30;
            this.pgSprite.ToolbarVisible = false;
            // 
            // btnTextureAliases
            // 
            this.btnTextureAliases.Location = new System.Drawing.Point(956, 565);
            this.btnTextureAliases.Name = "btnTextureAliases";
            this.btnTextureAliases.Size = new System.Drawing.Size(100, 23);
            this.btnTextureAliases.TabIndex = 29;
            this.btnTextureAliases.Text = "Texture Aliases";
            this.btnTextureAliases.UseVisualStyleBackColor = true;
            this.btnTextureAliases.Click += new System.EventHandler(this.btnTextureAliases_Click);
            // 
            // ucWorkspace
            // 
            this.ucWorkspace.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ucWorkspace.AutoScroll = true;
            this.ucWorkspace.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ucWorkspace.Location = new System.Drawing.Point(219, 28);
            this.ucWorkspace.Name = "ucWorkspace";
            this.ucWorkspace.SelectedFrameIndex = 0;
            this.ucWorkspace.SelectedFrameListName = null;
            this.ucWorkspace.SelectedSprite = null;
            this.ucWorkspace.Size = new System.Drawing.Size(725, 626);
            this.ucWorkspace.SpriteListControl = this.ucSpriteList;
            this.ucWorkspace.TabIndex = 5;
            // 
            // ucSpriteList
            // 
            this.ucSpriteList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.ucSpriteList.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ucSpriteList.Location = new System.Drawing.Point(13, 28);
            this.ucSpriteList.Name = "ucSpriteList";
            this.ucSpriteList.SelectedAnimation = null;
            this.ucSpriteList.Size = new System.Drawing.Size(199, 706);
            this.ucSpriteList.TabIndex = 4;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1200, 762);
            this.Controls.Add(this.btnTextureAliases);
            this.Controls.Add(this.btnFrameListRemove);
            this.Controls.Add(this.btnFrameListAdd);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.cbFrameLists);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.lblFrameNumber);
            this.Controls.Add(this.btnDeleteFrame);
            this.Controls.Add(this.btnNewFrame);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.btnPlay);
            this.Controls.Add(this.tbFrames);
            this.Controls.Add(this.ucWorkspace);
            this.Controls.Add(this.ucSpriteList);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.toolStrip1);
            this.Name = "MainForm";
            this.Text = "Ramp Animator";
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbFrames)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton newToolStripButton;
        private System.Windows.Forms.ToolStripButton openToolStripButton;
        private System.Windows.Forms.ToolStripButton saveToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
        private System.Windows.Forms.ToolStripButton cutToolStripButton;
        private System.Windows.Forms.ToolStripButton copyToolStripButton;
        private System.Windows.Forms.ToolStripButton pasteToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton helpToolStripButton;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private Controls.SpriteList ucSpriteList;
		private Controls.AnimationWorkspace ucWorkspace;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ComboBox cbFrameLists;
		private System.Windows.Forms.TrackBar tbFrames;
		private System.Windows.Forms.Button btnPlay;
		private System.Windows.Forms.Button btnStop;
		private System.Windows.Forms.Button btnNewFrame;
		private System.Windows.Forms.Button btnDeleteFrame;
		private System.Windows.Forms.Label lblFrameNumber;
		private System.Windows.Forms.Button btnFrameListAdd;
		private System.Windows.Forms.Button btnFrameListRemove;
		private System.Windows.Forms.ColorDialog colorDialog1;
		private System.Windows.Forms.PropertyGrid pgAnimation;
		private System.Windows.Forms.GroupBox groupBox4;
		private System.Windows.Forms.GroupBox groupBox5;
		private System.Windows.Forms.PropertyGrid pgFrame;
		private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.PropertyGrid pgSprite;
        private System.Windows.Forms.Button btnTextureAliases;
    }
}

