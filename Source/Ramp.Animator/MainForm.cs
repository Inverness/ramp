﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using Ramp.Animator.Controls;
using Ramp.Rendering;

namespace Ramp.Animator
{
    partial class MainForm : Form
    {
        private Animation _animation;

        public MainForm()
        {
            InitializeComponent();

            //TypeDescriptor.AddAttributes(typeof(Rectangle),
            //                             new TypeConverterAttribute(typeof(RectangleConverter)));
            //TypeDescriptor.AddAttributes(typeof(Point),
            //                             new TypeConverterAttribute(typeof(PointConverter)));

            ucWorkspace.SelectedSpriteChanged += ucWorkspace_SelectedSpriteChanged;
        }

        private void MainForm_Shown(object sender, EventArgs e)
        {
            _animation = Animation.Load("Content\\Animations\\A_Char_Sword.rani");

            ucSpriteList.TextureAliases["@0"] = "Content\\Textures\\T_TestBody.png";
            ucSpriteList.TextureAliases["@1"] = "Content\\Textures\\T_TestHead.png";
            ucSpriteList.TextureAliases["@5"] = "Content\\Textures\\T_TestSword2.png";

            ucSpriteList.SelectedAnimation = _animation;
            ucSpriteList.Refresh();

            RefreshAnimationProperties();
        }

        private void RefreshAnimationProperties()
        {
            // Populate frame list combo box
            cbFrameLists.Items.Clear();

            if (ucSpriteList.SelectedAnimation == null)
                return;

            foreach (string name in ucSpriteList.SelectedAnimation.Frames.Keys)
                cbFrameLists.Items.Add(name);

            if (cbFrameLists.Items.Count != 0 && cbFrameLists.SelectedIndex == -1)
                cbFrameLists.SelectedIndex = 0;

            pgAnimation.SelectedObject = _animation;
        }

        private void RefreshFrameProperties()
        {
            List<AnimationFrame> frames = ucWorkspace.SelectedFrameList;
            // Move to first frame if one exists
            if (ucWorkspace.SelectedFrameIndex == -1)
            {
                if (frames != null && frames.Count != 0)
                    ucWorkspace.SelectedFrameIndex = 0;
            }

            if (frames != null)
            {
                tbFrames.Maximum = ucWorkspace.SelectedFrameList.Count - 1;
                lblFrameNumber.Text = string.Format("{0}/{1}", ucWorkspace.SelectedFrameIndex + 1,
                                                    ucWorkspace.SelectedFrameList.Count);
            }
            else
            {
                tbFrames.Maximum = 0;
                lblFrameNumber.Text = "0/0";
            }

            if (tbFrames.Value != ucWorkspace.SelectedFrameIndex && ucWorkspace.SelectedFrameIndex != -1)
                tbFrames.Value = ucWorkspace.SelectedFrameIndex;

            pgFrame.SelectedObject = ucWorkspace.SelectedFrame;
        }

        private void RefreshSpriteProperties()
        {
            pgSprite.SelectedObject = ucWorkspace.SelectedSprite;
        }

        private void SetCurrentFrameList(string name)
        {
            ucWorkspace.SelectedFrameListName = name;
            RefreshFrameProperties();
        }

        private void SetCurrentFrame(int index)
        {
            ucWorkspace.SelectedFrameIndex = index;
            RefreshFrameProperties();
        }

        private void NewFrameList()
        {
            string name = Interaction.InputBox("Input the new frame list name", "New Frame List");
            if (string.IsNullOrEmpty(name))
                return;

            if (_animation.Frames.ContainsKey(name))
                return;

            _animation.Frames[name] = new List<AnimationFrame>();
            RefreshAnimationProperties();
            RefreshFrameProperties();
        }

        private void DeleteSelectedFrameList()
        {
            var name = (string) cbFrameLists.SelectedItem;
            if (name == null)
                return;

            // Deselect the frame list we're about to delete
            cbFrameLists.SelectedItem = _animation.Frames.Keys.FirstOrDefault(n => n != name);
            _animation.Frames.Remove(name);
            RefreshAnimationProperties();
            RefreshFrameProperties();
        }

        private void NewFrame()
        {
            List<AnimationFrame> frameList = ucWorkspace.SelectedFrameList;
            if (frameList == null)
                return;
            frameList.Add(new AnimationFrame());
            RefreshFrameProperties();
        }

        private void DeleteSelectedFrame()
        {
            int index = ucWorkspace.SelectedFrameIndex;
            if (index == -1)
                return;
            List<AnimationFrame> frames = ucWorkspace.SelectedFrameList;
            frames.RemoveAt(index);

            index = Math.Max(-1, Math.Min(index + 1, frames.Count - 1));
            ucWorkspace.SelectedFrameIndex = index;

            RefreshFrameProperties();
        }

        private void EditTextureAliases()
        {
            var form = new TextureAliasForm();

            IDictionary<string, string> aliases = ucSpriteList.TextureAliases;
            form.PullData(aliases);
            if (form.ShowDialog() != DialogResult.OK)
                return;

            aliases.Clear();
            form.PushData(aliases);

            ucSpriteList.LoadImages();
            ucWorkspace.Refresh();
        }

        private void ucWorkspace_SelectedSpriteChanged(AnimationWorkspace sender, EventArgs eventArgs)
        {
            RefreshSpriteProperties();
        }

        private void cbFrameLists_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetCurrentFrameList((string) cbFrameLists.SelectedItem);
        }

        private void tbFrames_ValueChanged(object sender, EventArgs e)
        {
            SetCurrentFrame(tbFrames.Value);
        }

        private void btnFrameListAdd_Click(object sender, EventArgs e)
        {
            NewFrameList();
        }

        private void btnFrameListRemove_Click(object sender, EventArgs e)
        {
            DeleteSelectedFrameList();
        }

        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
        }

        private void btnNewFrame_Click(object sender, EventArgs e)
        {
            NewFrame();
        }

        private void btnDeleteFrame_Click(object sender, EventArgs e)
        {
            DeleteSelectedFrame();
        }

        private void btnTextureAliases_Click(object sender, EventArgs e)
        {
            EditTextureAliases();
        }
    }
}