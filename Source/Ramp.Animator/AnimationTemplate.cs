﻿namespace Ramp.Animator
{
    //internal class AnimationTemplate
    //{
    //	public int Version;

    //	public bool Loop;

    //	public bool Directional;

    //	public readonly Dictionary<string, AnimationTemplateSpriteType> SpriteTypes =
    //		new Dictionary<string, AnimationTemplateSpriteType>();

    //	public readonly Dictionary<string, List<AnimationTemplateFrame>> Frames =
    //		new Dictionary<string, List<AnimationTemplateFrame>>();

    //	public void Save(Stream stream)
    //	{
    //		new JsonSerializer().Serialize(new JsonTextWriter(new StreamWriter(stream)), this);
    //	}

    //	public static AnimationTemplate Load(string path)
    //	{
    //		using (Stream stream = File.OpenRead(path))
    //			return Load(stream);
    //	}

    //	public static AnimationTemplate Load(Stream stream)
    //	{
    //		return new JsonSerializer().Deserialize<AnimationTemplate>(new JsonTextReader(new StreamReader(stream)));
    //		//var t = new AnimationTemplate();
    //		//var root = JObject.Load(new JsonTextReader(new StreamReader(stream)));
    //		//t.Version = (int)root["Version"];
    //		//t.Loop = (bool)root["Loop"];
    //		//t.Directional = (bool)root["Directional"];

    //		////foreach (var item in t["SpriteTypes"])

    //		//return t;
    //	}
    //}

    //[JsonConverter(typeof(AnimationTemplateSpriteTypeConverter))]
    //internal class AnimationTemplateSpriteType
    //{
    //	public string TextureName;

    //	public Rectangle TexturePart;
    //}

    //internal class AnimationTemplateFrame
    //{
    //	public double Time;

    //	public readonly List<AnimationTemplateSprite> Sprites = new List<AnimationTemplateSprite>();

    //	public readonly List<string> Events = new List<string>();
    //}

    //[JsonConverter(typeof(AnimationTemplateSpriteConverter))]
    //internal class AnimationTemplateSprite
    //{
    //	public string TypeName;

    //	public Point Position;
    //}

    //// these converters are temporary until the animation format is revised for easier serialization
    //internal class AnimationTemplateSpriteTypeConverter : JsonConverter
    //{
    //	public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
    //	{
    //		var type = (AnimationTemplateSpriteType)value;
    //		writer.WriteStartArray();
    //		writer.WriteValue(0);
    //		writer.WriteValue(type.TextureName);
    //		writer.WriteValue(type.TexturePart.X);
    //		writer.WriteValue(type.TexturePart.Y);
    //		writer.WriteValue(type.TexturePart.Width);
    //		writer.WriteValue(type.TexturePart.Height);
    //		writer.WriteEndArray();
    //	}

    //	public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
    //	{
    //		var type = new AnimationTemplateSpriteType();
    //		if (reader.TokenType != JsonToken.StartArray)
    //			throw new JsonReaderException();
    //		reader.Read(); // skip number
    //		type.TextureName = reader.ReadAsString();
    //		type.TexturePart.X = reader.ReadAsInt32() ?? 0;
    //		type.TexturePart.Y = reader.ReadAsInt32() ?? 0;
    //		type.TexturePart.Width = reader.ReadAsInt32() ?? 0;
    //		type.TexturePart.Height = reader.ReadAsInt32() ?? 0;
    //		reader.Read();
    //		if (reader.TokenType != JsonToken.EndArray)
    //			throw new JsonReaderException();
    //		return type;
    //	}

    //	public override bool CanConvert(Type objectType)
    //	{
    //		return objectType == typeof(AnimationTemplateSpriteType);
    //	}
    //}

    //internal class AnimationTemplateSpriteConverter : JsonConverter
    //{
    //	public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
    //	{
    //		var sprite = (AnimationTemplateSprite)value;
    //		writer.WriteStartArray();
    //		writer.WriteValue(sprite.TypeName);
    //		writer.WriteValue(sprite.Position.X);
    //		writer.WriteValue(sprite.Position.Y);
    //		writer.WriteEndArray();
    //	}

    //	public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
    //	{
    //		var sprite = new AnimationTemplateSprite();
    //		if (reader.TokenType != JsonToken.StartArray)
    //			throw new JsonReaderException();
    //		sprite.TypeName = reader.ReadAsString();
    //		sprite.Position.X = reader.ReadAsInt32() ?? 0;
    //		sprite.Position.Y = reader.ReadAsInt32() ?? 0;
    //		reader.Read();
    //		if (reader.TokenType != JsonToken.EndArray)
    //			throw new JsonReaderException();
    //		return sprite;
    //	}

    //	public override bool CanConvert(Type objectType)
    //	{
    //		return objectType == typeof(AnimationTemplateSprite);
    //	}
    //}
}