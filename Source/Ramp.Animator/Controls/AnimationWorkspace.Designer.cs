﻿namespace Ramp.Animator.Controls
{
    partial class AnimationWorkspace
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.newSpriteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.deleteSpriteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.contextMenuStrip.SuspendLayout();
			this.SuspendLayout();
			// 
			// contextMenuStrip
			// 
			this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newSpriteToolStripMenuItem,
            this.deleteSpriteToolStripMenuItem});
			this.contextMenuStrip.Name = "contextMenuStrip1";
			this.contextMenuStrip.Size = new System.Drawing.Size(141, 48);
			this.contextMenuStrip.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.contextMenuStrip_ItemClicked);
			// 
			// newSpriteToolStripMenuItem
			// 
			this.newSpriteToolStripMenuItem.Name = "newSpriteToolStripMenuItem";
			this.newSpriteToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
			this.newSpriteToolStripMenuItem.Text = "New Sprite";
			// 
			// deleteSpriteToolStripMenuItem
			// 
			this.deleteSpriteToolStripMenuItem.Name = "deleteSpriteToolStripMenuItem";
			this.deleteSpriteToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
			this.deleteSpriteToolStripMenuItem.Text = "Delete Sprite";
			// 
			// AnimationWorkspace
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
			this.DoubleBuffered = true;
			this.Name = "AnimationWorkspace";
			this.contextMenuStrip.ResumeLayout(false);
			this.ResumeLayout(false);

        }

        #endregion

		private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
		private System.Windows.Forms.ToolStripMenuItem newSpriteToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem deleteSpriteToolStripMenuItem;
    }
}
