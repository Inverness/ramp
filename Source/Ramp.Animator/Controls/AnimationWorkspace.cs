﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Ramp.Rendering;
using XnaPoint = Microsoft.Xna.Framework.Point;

namespace Ramp.Animator.Controls
{
    partial class AnimationWorkspace : UserControl
    {
        private static readonly Pen AxisPen = new Pen(Color.LightGreen, 1);
        private static readonly Pen SelectionPen = new Pen(Color.White, 1);

        private Point _origin;
        private string _selectedFrameListName;
        private int _selectedFrameIndex;
        private AnimationSprite _selectedSprite;
        private Point _selectOffset;
        private Point _prevMouseLocation;
        private bool _dragging;

        private readonly List<Tuple<Rectangle, AnimationSprite>> _sprites =
            new List<Tuple<Rectangle, AnimationSprite>>();

        public AnimationWorkspace()
        {
            InitializeComponent();

            _origin = new Point(128, 128);
            GridSize = 16;
        }

        public event TypedEventHandler<AnimationWorkspace, EventArgs> SelectedSpriteChanged;

        [Browsable(false)]
        public List<AnimationFrame> SelectedFrameList
        {
            get
            {
                if (SpriteListControl == null || SpriteListControl.SelectedAnimation == null ||
                    _selectedFrameListName == null)
                    return null;
                List<AnimationFrame> frames;
                SpriteListControl.SelectedAnimation.Frames.TryGetValue(_selectedFrameListName, out frames);
                return frames;
            }
        }

        [Browsable(false)]
        public string SelectedFrameListName
        {
            get { return _selectedFrameListName; }

            set
            {
                if (value == _selectedFrameListName)
                {
                    Refresh();
                    return;
                }
                _selectedFrameListName = value;
                SelectedFrameIndex = -1;
                SelectedSprite = null;
                Refresh();
            }
        }

        [Browsable(false)]
        public AnimationFrame SelectedFrame => _selectedFrameIndex == -1 ? null : SelectedFrameList[_selectedFrameIndex];

        [Browsable(false)]
        public int SelectedFrameIndex
        {
            get { return _selectedFrameIndex; }

            set
            {
                if (value == _selectedFrameIndex)
                {
                    Refresh();
                    return;
                }
                _selectedFrameIndex = value;
                SelectedSprite = null;
                Refresh();
            }
        }

        [Browsable(false)]
        public AnimationSprite SelectedSprite
        {
            get { return _selectedSprite; }

            set
            {
                if (value == _selectedSprite)
                {
                    Refresh();
                    return;
                }
                _selectedSprite = value;
                SelectedSpriteChanged?.Invoke(this, EventArgs.Empty);
                Refresh();
            }
        }

        [DefaultValue(16)]
        public int GridSize { get; set; }

        public SpriteList SpriteListControl { get; set; }

        /// <summary>
        ///     Select the sprite at the specified location.
        /// </summary>
        /// <param name="location"> The mouse location. </param>
        public void SelectSprite(Point location)
        {
            if (_sprites.Count == 0)
                return;

            var locRect = new Rectangle(location, Size.Empty);

            List<Tuple<Rectangle, AnimationSprite>> results =
                _sprites.Where(i => i.Item1.IntersectsWith(locRect)).ToList();

            AnimationSprite resultSprite = null;
            Rectangle resultRect = Rectangle.Empty;

            if (results.Count != 0)
            {
                if (SelectedSprite != null)
                {
                    int index = results.FindIndex(i => i.Item2 == SelectedSprite);
                    if (index != -1 && index < results.Count - 1)
                    {
                        resultSprite = results[index + 1].Item2;
                        resultRect = results[index + 1].Item1;
                    }
                    else
                    {
                        resultSprite = results[0].Item2;
                        resultRect = results[0].Item1;
                    }
                }
                else
                {
                    resultSprite = results[0].Item2;
                    resultRect = results[0].Item1;
                }
            }

            SelectedSprite = resultSprite;
            _selectOffset = new Point(locRect.X - resultRect.X, locRect.Y - resultRect.Y);

            Refresh();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            DrawAxis(e.Graphics, e.ClipRectangle);
            DrawSprites(e.Graphics, e.ClipRectangle);
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);

            _prevMouseLocation = e.Location;
            if (e.Button == MouseButtons.Left)
            {
                SelectSprite(e.Location);
            }
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            base.OnMouseUp(e);

            if (e.Button == MouseButtons.Right)
            {
                _dragging = false;
            }
        }

        protected override void OnMouseClick(MouseEventArgs e)
        {
            base.OnMouseClick(e);

            if (e.Button == MouseButtons.Right)
            {
                if (!_dragging)
                    contextMenuStrip.Show(this, e.X, e.Y);
            }
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);

            if ((e.Button & MouseButtons.Right) != 0)
            {
                _origin.X += e.Location.X - _prevMouseLocation.X;
                _origin.Y += e.Location.Y - _prevMouseLocation.Y;
                _prevMouseLocation = e.Location;
                _dragging = true; // set so the right-click menu doesn't open after a drag
                Refresh();
            }
            else if ((e.Button & MouseButtons.Left) != 0)
            {
                if (e.Location != _prevMouseLocation)
                {
                    AnimationSprite sprite = SelectedSprite;
                    if (sprite != null)
                    {
                        sprite.Position = new XnaPoint(e.Location.X - _origin.X - _selectOffset.X,
                                                       e.Location.Y - _origin.Y - _selectOffset.Y);
                    }
                    Refresh();
                }
            }
        }

        protected void DrawAxis(Graphics graphics, Rectangle clip)
        {
            var x1 = new Point(0, _origin.Y);
            var x2 = new Point(ClientRectangle.Width, _origin.Y);
            var y1 = new Point(_origin.X, 0);
            var y2 = new Point(_origin.X, ClientRectangle.Height);
            graphics.DrawLine(AxisPen, x1, x2);
            graphics.DrawLine(AxisPen, y1, y2);
        }

        protected void DrawSprites(Graphics graphics, Rectangle clip)
        {
            _sprites.Clear();

            List<AnimationFrame> frames = SelectedFrameList;
            if (frames == null)
                return;

            if (_selectedFrameIndex < 0 || _selectedFrameIndex >= frames.Count)
                return;

            AnimationFrame frame = frames[_selectedFrameIndex];

            throw new NotImplementedException();
            //foreach (AnimationSprite sprite in frame.Sprites)
            //{
            //    AnimationSpriteType type = SpriteListControl.SelectedAnimation.SpriteTypes[sprite.SpriteTypeIndex];
            //    Image image = SpriteListControl.SpriteImages[sprite.SpriteTypeIndex];
            //    if (image == null)
            //        continue;

            //    var dest = new Rectangle(_origin.X + sprite.Position.X, _origin.Y + sprite.Position.Y,
            //                             type.TextureRect.Width,
            //                             type.TextureRect.Height);
            //    var src = new Rectangle(type.TextureRect.X, type.TextureRect.Y, type.TextureRect.Width,
            //                            type.TextureRect.Height);
            //    graphics.DrawImage(image, dest, src, GraphicsUnit.Pixel);

            //    _sprites.Add(Tuple.Create(dest, sprite));

            //    if (SelectedSprite == sprite)
            //    {
            //        Rectangle selection = dest;
            //        selection.X -= 2;
            //        selection.Y -= 2;
            //        selection.Width += 4;
            //        selection.Height += 4;
            //        graphics.DrawRectangle(SelectionPen, selection);
            //    }
            //}
        }

        private void contextMenuStrip_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if (e.ClickedItem == newSpriteToolStripMenuItem)
            {
                if (SpriteListControl.SelectedSpriteType == null)
                    return;
                AnimationFrame frame = SelectedFrame;
                if (frame == null)
                    return;

                throw new NotImplementedException();
                //var sprite = new AnimationSprite(SpriteListControl.SelectedSpriteTypeIndex)
                //{
                //    Position = new XnaPoint(_prevMouseLocation.X - _origin.X,
                //                            _prevMouseLocation.Y - _origin.Y)
                //};

                //frame.Sprites.Add(sprite);

                //SelectedSprite = sprite;
            }
            else if (e.ClickedItem == deleteSpriteToolStripMenuItem)
            {
                AnimationSprite sprite = SelectedSprite;
                if (sprite == null)
                    return;
                SelectedFrame.Sprites.Remove(sprite);
                SelectedSprite = null;
            }
        }
    }
}