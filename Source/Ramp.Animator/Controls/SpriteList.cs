﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Ramp.Rendering;

namespace Ramp.Animator.Controls
{
    /// <summary>
    ///     Displays sprites defined in an animation.
    /// </summary>
    partial class SpriteList : UserControl
    {
        private readonly Pen SelectionPen = new Pen(Color.White, 1);
        private Animation _animation;
        private int _selectedSpriteTypeIndex;
        private readonly List<Image> _spriteImages = new List<Image>();
        private readonly List<Rectangle> _spriteBounds = new List<Rectangle>();
        private readonly Dictionary<string, string> _textureAliases = new Dictionary<string, string>();

        public SpriteList()
        {
            InitializeComponent();
            Spacing = 12;
            HScroll = false;
            VScroll = true;
        }

        /// <summary>
        ///     Raised when the selected sprite type is changed.
        /// </summary>
        public event TypedEventHandler<SpriteList, EventArgs> SelectedSpriteTypeChanged;

        /// <summary>
        ///     Get or set the currently selected animation that sprites will be loaded and displayed for.
        /// </summary>
        [Browsable(false)]
        public Animation SelectedAnimation
        {
            get { return _animation; }

            set
            {
                if (value == _animation)
                    return;
                _animation = value;
                LoadImages();
            }
        }

        /// <summary>
        ///     Get the currently selected sprite type.
        /// </summary>
        public AnimationSpriteType SelectedSpriteType
        {
            get
            {
                if (_animation == null || _selectedSpriteTypeIndex == -1)
                    return null;
                throw new NotImplementedException();
                //return _animation.SpriteTypes[_selectedSpriteTypeIndex];
            }
        }

        /// <summary>
        ///     Get the index of the selected sprite type in the animation.
        /// </summary>
        public int SelectedSpriteTypeIndex => _selectedSpriteTypeIndex;

        /// <summary>
        ///     Get a read only list containing images for each sprite type.
        /// </summary>
        [Browsable(false)]
        public IReadOnlyList<Image> SpriteImages => _spriteImages;

        /// <summary>
        ///     Get a dictionary containing texture names for each alias, keyed by name.
        /// </summary>
        [Browsable(false)]
        public IDictionary<string, string> TextureAliases => _textureAliases;

        /// <summary>
        ///     Get or set a value indicating the spacing in pixels between each sprite.
        /// </summary>
        [DefaultValue(12)]
        public int Spacing { get; set; }

        /// <summary>
        ///     Loads or reloads images for each sprite type in the current animation.
        /// </summary>
        public void LoadImages()
        {
            if (_spriteImages.Count != 0)
                _spriteImages.Where(i => i != null).ForEach(i => i.Dispose());
            _spriteImages.Clear();

            if (SelectedAnimation == null || SelectedAnimation.SpriteTypes.Count == 0)
                return;

            throw new NotImplementedException();
            //foreach (AnimationSpriteType spriteType in SelectedAnimation.SpriteTypes)
            //{
            //    string textureName = spriteType.TextureName;
            //    if (textureName != null && spriteType.TextureName.StartsWith("@"))
            //        _textureAliases.TryGetValue(spriteType.TextureName, out textureName);

            //    if (textureName == null)
            //    {
            //        _spriteImages.Add(null);
            //        continue;
            //    }

            //    try
            //    {
            //        using (FileStream stream = File.OpenRead(textureName))
            //            _spriteImages.Add(Image.FromStream(stream));
            //    }
            //    catch (FileNotFoundException)
            //    {
            //        // TODO: Error image
            //        _spriteImages.Add(null);
            //    }
            //}

            //Refresh();
        }

        /// <summary>
        ///     Select the sprite type at the specified pixel location on the control.
        /// </summary>
        /// <param name="location"> The pixel location. </param>
        public void SelectSpriteType(Point location)
        {
            var mouse = new Rectangle(location.X + -AutoScrollPosition.X, location.Y + -AutoScrollPosition.Y, 0, 0);

            int result = -1;
            for (int i = 0; i < _spriteBounds.Count; i++)
            {
                if (_spriteBounds[i].IntersectsWith(mouse))
                {
                    result = i;
                    break;
                }
            }

            if (result == _selectedSpriteTypeIndex)
                return;

            _selectedSpriteTypeIndex = result;
            SelectedSpriteTypeChanged?.Invoke(this, EventArgs.Empty);
            Refresh(); // draw the selection box
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            throw new NotImplementedException();
            //base.OnPaint(e);

            //_spriteBounds.Clear();

            //// Nothing to paint if we have none of these defined
            //if (SelectedAnimation == null || SelectedAnimation.SpriteTypes.Count == 0)
            //    return;

            //Debug.Assert(_spriteImages.Count == SelectedAnimation.SpriteTypes.Count,
            //             "_images.Count == Animation.SpriteTypes.Count");

            //e.Graphics.TranslateTransform(AutoScrollPosition.X, AutoScrollPosition.Y);
            //e.Graphics.PixelOffsetMode = PixelOffsetMode.None;

            //int x = 8, y = 8;
            //for (int i = 0; i < SelectedAnimation.SpriteTypes.Count; i++)
            //{
            //    //AnimationSpriteType type = SelectedAnimation.SpriteTypes[i];
            //    //var destRect = new Rectangle(x, y, type.TextureRect.Width, type.TextureRect.Height);

            //    //if (_spriteImages[i] != null)
            //    //{
            //    //    var srcRect = new Rectangle(type.TextureRect.X, type.TextureRect.Y,
            //    //                                type.TextureRect.Width, type.TextureRect.Height);
            //    //    e.Graphics.DrawImage(_spriteImages[i], destRect, srcRect, GraphicsUnit.Pixel);
            //    //    _spriteBounds.Add(destRect);
            //    //}
            //    //else
            //    //{
            //    //    _spriteBounds.Add(Rectangle.Empty);
            //    //}

            //    //if (_selectedSpriteTypeIndex == i)
            //    //{
            //    //    destRect.Offset(-2, -2);
            //    //    destRect.Inflate(4, 4);
            //    //    e.Graphics.DrawRectangle(SelectionPen, destRect);
            //    //}

            //    //y += type.TextureRect.Height + Spacing;
            //}

            //AutoScrollMinSize = new Size(0, y + Spacing);
        }

        protected override void OnMouseClick(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                SelectSpriteType(e.Location);
            }
            else if (e.Button == MouseButtons.Right)
            {
            }

            base.OnMouseClick(e);
        }
    }
}