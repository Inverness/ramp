﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;
using Ramp.Data;
using Ramp.Logic;
using Ramp.Naming;
using Ramp.Rendering;

namespace Ramp
{
    internal class DemoGame : RampGame
    {
        protected QuestManager QuestManager;
        protected RegionManager RegionManager;
        protected ServerDatabaseManager Database;
        protected AccountManager Account;

        private readonly List<INamedReference> _persistentAssets = new List<INamedReference>();

        protected override void Initialize()
        {
            base.Initialize();
            
            _persistentAssets.Add(new AssetReference<Texture2D>("Textures/TestBody"));
            _persistentAssets.Add(new AssetReference<Texture2D>("Textures/TestHead"));
            _persistentAssets.Add(new AssetReference<Animation>("Animations/Char/Idle"));
            _persistentAssets.Add(new AssetReference<Animation>("Animations/Char/Walk"));
            _persistentAssets.Add(new AssetReference<Animation>("Animations/Char/Push"));
            _persistentAssets.Add(new AssetReference<Animation>("Animations/Char/Damaged"));
            _persistentAssets.Add(new AssetReference<Animation>("Animations/Char/Sword"));
        }

        protected override void CreateGameSystems()
        {
            base.CreateGameSystems();

            DeclManager.RegisterTypes();

            Database = new ServerDatabaseManager(this);
            Components.Add(Database);

            Account = new AccountManager(this);
            Components.Add(Account);

            // Create the quest manager
            QuestManager = new QuestManager(this)
            {
                UpdateOrder = 4,
                Enabled = true
            };
            Components.Add(QuestManager);

            RegionManager = new RegionManager(this)
            {
                UpdateOrder = 4,
                Enabled = true
            };
            Components.Add(RegionManager);
        }
    }
}