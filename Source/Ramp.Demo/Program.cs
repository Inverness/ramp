﻿using System;
using System.Threading;
using Microsoft.Xna.Framework;
using NLog;

namespace Ramp
{
    public static class Program
    {
        private static readonly Logger s_log = LogManager.GetCurrentClassLogger();

        [STAThread]
        public static void Main(string[] args)
        {
            try
            {
                using (var game = new DemoGame())
                {
                    game.Run(GameRunBehavior.Synchronous);
                }
            }
            catch (Exception ex)
            {
                s_log.Error(ex, "Failed to run game");
                throw;
            }
        }
    }
}