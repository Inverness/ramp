using System;
using Newtonsoft.Json.Linq;

namespace Ramp.Logic
{
    public enum QuestStageType
    {
        Normal,
        Start,
        Complete,
        Fail
    }

    public sealed class QuestStage
    {
        // Construct from validated JSON object.
        internal QuestStage(Quest quest, string id, JObject root)
        {
            Quest = quest;
            Id = id;
            StageType = (QuestStageType) Enum.Parse(typeof(QuestStageType), (string) root["StageType"]);
            JournalEntry = (string) root["JournalEntry"];
        }

        public string Id { get; private set; }

        public string JournalEntry { get; private set; }

        public Quest Quest { get; private set; }

        public QuestStageType StageType { get; private set; }
    }

    //public class QuestStage
    //{
    //	public QuestStage(Quest quest, ushort id)
    //	{
    //		if (quest == null)
    //			throw new ArgumentNullException("quest");

    //		Quest = quest;
    //		ID = id;
    //	}

    //	public Quest Quest { get; private set; }

    //	public ushort ID { get; private set; }

    //	public string Name { get; set; }

    //	public QuestStageType StageType { get; set; }

    //	public string JournalEntry { get; set; }
    //}
}