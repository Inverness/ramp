using Ramp.Simulation;

namespace Ramp.Logic
{
    public class QuestStageChangedEventArgs : QuestEventArgs
    {
        public QuestStageChangedEventArgs(Quest quest, IPlayer player, QuestStage stage)
            : base(quest)
        {
            Player = player;
            Stage = stage;
        }

        public IPlayer Player { get; private set; }

        public QuestStage Stage { get; private set; }
    }
}