using System;
using Ramp.Simulation;

namespace Ramp.Logic
{
    /// <summary>
    ///     Manages state for the actor's components.
    /// </summary>
    public sealed class StateComponent : ActorComponent
    {
        /// <summary>
        ///     Raised when the state changes.
        /// </summary>
        public TypedEventHandler<StateComponent, StateChangedEventArgs> StateChanged;

        /// <summary>
        ///     Gets the state at the top of the stack if any.
        /// </summary>
        public string Current { get; private set; }

        /// <summary>
        ///     Set the current state. This will replace the state at the top of the stack.
        /// </summary>
        /// <param name="state"> The state name. </param>
        /// <param name="force"> True to force a state change even if already in the specified state. </param>
        /// <returns>The previous state.</returns>
        public string Set(string state, bool force = false)
        {
            if (!force && Current == state)
                return state;

            string previous = Current;
            Current = state;

            OnStateChanged(previous);
            return previous;
        }

        /// <summary>
        ///     Pushes a new state onto the stack and returns an object that can be disposed to pop it.
        /// </summary>
        /// <param name="state"> The state name. </param>
        /// <param name="verify"> Verify that the current state is still current when disposing the scope. </param>
        /// <returns> An object that can be disposed to pop the state. </returns>
        public StateScope Push(string state, bool verify = false)
        {
            string previous = Current;
            Set(state);
            return new StateScope(this, previous, state, verify);
        }

        private void OnStateChanged(string previous)
        {
            StateChanged?.Invoke(this, new StateChangedEventArgs(previous));
        }
    }

    /// <summary>
    ///     Pops a state from the stack when disposed.
    /// </summary>
    public struct StateScope : IDisposable
    {
        private StateComponent _component;
        private readonly string _previous;
        private readonly string _current;
        private readonly bool _check;

        internal StateScope(StateComponent component, string previous, string current, bool check)
        {
            _component = component;
            _previous = previous;
            _current = current;
            _check = check;
        }

        public void Dispose()
        {
            if (_component != null)
            {
                if (_check && _component.Current != _current)
                    throw new InvalidOperationException("expected state: " + _current);

                _component.Set(_previous);
                _component = null;
            }
        }
    }

    public static class StateBehaviorExtensions
    {
        public static string GetState(this Actor actor)
        {
            Validate.ArgumentNotNull(actor, "actor");
            
            return actor.GetComponent<StateComponent>()?.Current;
        }

        public static void SetState(this Actor actor, string state, bool force = false)
        {
            Validate.ArgumentNotNull(actor, "actor");
            
            actor.GetComponent<StateComponent>()?.Set(state, force);
        }
    }

    public class StateChangedEventArgs : EventArgs
    {
        public StateChangedEventArgs(string oldState)
        {
            OldState = oldState;
        }

        public string OldState { get; private set; }
    }
}