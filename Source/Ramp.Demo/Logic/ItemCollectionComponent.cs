using System;
using System.Collections.Generic;
using System.Linq;
using Ramp.Simulation;
using Ramp.Utilities.Text;

namespace Ramp.Logic
{
    /// <summary>
    ///     Stores item instances.
    /// </summary>
    public class ItemCollectionComponent : ActorComponent
    {
        // Current items in the collection
        private readonly List<Item> _items = new List<Item>();

        public ItemCollectionComponent()
        {
            ParseItemList(Actor.GetSpawnArgument<string>("Items"));
        }

        /// <summary>
        ///     Raised when an item is added to the collection.
        /// </summary>
        public TypedEventHandler<ItemCollectionComponent, ItemEventArgs> ItemAdded;

        /// <summary>
        ///     Raised when an item is removed from the collection.
        /// </summary>
        public TypedEventHandler<ItemCollectionComponent, ItemEventArgs> ItemRemoved;

        /// <summary>
        ///     Items in this container.
        /// </summary>
        public IReadOnlyList<Item> Items => _items;

        /// <summary>
        ///     Add items of the specified type to this container.
        /// </summary>
        public void Add(string type, uint count, bool silent = false)
        {
            Validate.ArgumentNotNull(type, "type");
            Validate.ArgumentInRange(count > 0, "count");
            // impose a limitation for coding purposes
            Validate.ValidOperation(_items.Count < ushort.MaxValue, "max items reached");

            ItemType typeObject = ResolveItemType(type);

            if (typeObject.Stackable)
            {
                Item item = Get(typeObject);
                Add(item ?? new Item(typeObject), count, true);
            }
            else
            {
                for (uint i = 0; i < count; i++)
                    Add(new Item(typeObject), 1, true);
            }

            OnItemAdded(typeObject, count, silent);
        }

        /// <summary>
        ///		Add an item to this collection.
        /// </summary>
        /// <param name="item"> The item object. </param>
        /// <param name="count"> The count to add. </param>
        /// <param name="silent"> True if the player should not be notified. </param>
        /// <remarks>
        ///		There are three behaviors to this method depending on what collection the provided item is in.
        /// 
        ///		If the item is not in any collection, it is added to this collection and its count is set to the specified
        ///		count regardless of previous value.
        /// 
        ///		If the item is in this collection, it's count is incremented by the specified count.
        /// 
        ///		If the item is in another collection, it is removed from that collection and added to the current with the
        ///		same count if the specified count is greater than or equal to it. If the specified count is less than
        ///		the count of the item, then a duplicate of the item is created in the current collection with the specified
        ///		count.
        /// </remarks>
        public void Add(Item item, uint count, bool silent = false)
        {
            if (item.Collection == null)
            {
                item.Collection = this;
                item.Count = count;
                _items.Add(item);
            }
            else if (item.Collection == this)
            {
                item.Count = checked(item.Count + count);
            }
            else
            {
                count = item.Collection.Remove(item, count, silent);
                // If it couldn't be completely removed, create a duplicate or stack an existing one
                if (item.Collection != null)
                {
                    Item existing = GetStackable(item);
                    Add(existing ?? item.CloneWithoutCollection(), count, true);
                }
                else
                {
                    Add(item, count, true);
                }
            }
            
            OnItemAdded(item.ItemType, count, silent);
        }

        /// <summary>
        ///		Remove items of the specified type.
        /// </summary>
        /// <param name="type"> The item type name. </param>
        /// <param name="count"> The quantity to remove. </param>
        /// <param name="silent"> True if the player should not be notified. </param>
        /// <returns> The count removed. </returns>
        public uint Remove(string type, uint count, bool silent = false)
        {
            Validate.ArgumentNotNull(type, "type");
            Validate.ArgumentInRange(count > 0, "count");

            ItemType decl = ResolveItemType(type);

            uint remaining = count;
            foreach (Item item in _items)
            {
                if (item.ItemType != decl)
                    continue;

                remaining -= Remove(item, remaining, true);

                if (remaining == 0)
                    break;
            }
            
            OnItemRemoved(decl, count, silent);

            return count - remaining;
        }

        /// <summary>
        ///		Remove an item from this collection.
        /// </summary>
        /// <param name="item"> The item. </param>
        /// <param name="count"> The count to remove. </param>
        /// <param name="silent"> True if the player should not be notified. </param>
        /// <returns> The count removed. </returns>
        public uint Remove(Item item, uint count, bool silent = false)
        {
            Validate.ArgumentNotNull(item, "item");
            Validate.ArgumentInRange(count > 0, "count");
            VerifyOwned(item);

            uint removed;
            if (count >= item.Count)
            {
                _items.Remove(item);
                removed = item.Count;
                item.Collection = null;
            }
            else
            {
                item.Count -= count;
                removed = count;
            }
            
            OnItemRemoved(item.ItemType, count, silent);

            return removed;
        }

        public uint Count(string type)
        {
            if (type == null)
                throw new ArgumentNullException(nameof(type));

            ItemType decl = ResolveItemType(type);

            return _items.Where(item => item.ItemType == decl).
                          Aggregate<Item, uint>(0, (current, item) => checked(current + item.Count));
        }

        public Item Get(string type)
        {
            if (type == null)
                throw new ArgumentNullException(nameof(type));

            ItemType decl = ResolveItemType(type);

            return _items.FirstOrDefault(item => item.ItemType == decl);
        }

        public IEnumerable<Item> GetAll(string type)
        {
            if (type == null)
                throw new ArgumentNullException(nameof(type));

            ItemType decl = ResolveItemType(type);

            return _items.Where(item => item.ItemType == decl);
        }

        public Item GetStackable(Item other)
        {
            if (other == null)
                throw new ArgumentNullException(nameof(other));

            return _items.FirstOrDefault(item => item.CanStackWith(other));
        }

        protected Item Get(ItemType type)
        {
            return _items.FirstOrDefault(item => item.ItemType == type);
        }

        protected ItemType ResolveItemType(string name)
        {
            var type = Directory.Find<ItemType>(name);
            if (type == null)
                throw new ArgumentException("invalid item type name", nameof(name));
            return type;
        }

        protected void VerifyOwned(Item item)
        {
            if (item.Collection != this)
                throw new InvalidOperationException("item not owned by current ItemCollectionComponent");
        }

        protected virtual void OnItemAdded(ItemType type, uint count, bool silent)
        {
            ItemAdded?.Invoke(this, new ItemEventArgs(type, count, silent));
        }

        protected virtual void OnItemRemoved(ItemType type, uint count, bool silent)
        {
            ItemRemoved?.Invoke(this, new ItemEventArgs(type, count, silent));
        }

        private void ParseItemList(string list)
        {
            if (string.IsNullOrEmpty(list))
                return;

            // Coin:500, DungeonKey

            foreach (string item in DelimitedStringParser.ParseCommaDelimited(list, true))
            {
                int colonIndex = item.LastIndexOf(':');

                if (colonIndex == -1)
                {
                    Add(item, 1);
                }
                else
                {
                    string realItem = item.Substring(0, colonIndex);
                    uint count = uint.Parse(item.Substring(colonIndex + 1));
                    Add(realItem, count);
                }
            }
        }
    }

    public class ItemEventArgs : EventArgs
    {
        public ItemEventArgs(ItemType type, uint count, bool silent)
        {
            ItemType = type;
            Count = count;
            Silent = silent;
        }

        public ItemType ItemType { get; private set; }

        public uint Count { get; private set; }

        public bool Silent { get; private set; }
    }
}