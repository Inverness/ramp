using System;
using JetBrains.Annotations;
using Newtonsoft.Json;
using Ramp.Naming;

namespace Ramp.Logic
{
    /// <summary>
    ///     The parameters used to instantiate an effect on an object.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public sealed class EffectParameters : Freezable
    {
        private NamedReference<EffectType> _effectType = new NamedReference<EffectType>();
        private TimeSpan _duration;
        private double _magnitude;
        private double _area;

        /// <summary>
        ///     Gets the underlying effect type.
        /// </summary>
        [JsonProperty]
        public NamedReference<EffectType> EffectType
        {
            get { return _effectType; }

            [UsedImplicitly]
            private set
            {
                VerifyNotFrozen();
                _effectType = value ?? new NamedReference<EffectType>();
            }
        }

        /// <summary>
        ///     Gets or sets the duration of the effect. This is not valid for constant effects.
        /// </summary>
        [JsonProperty]
        public TimeSpan Duration
        {
            get { return _duration; }

            set
            {
                VerifyNotFrozen();
                _duration = value;
            }
        }

        /// <summary>
        ///     Gets or sets the magnitude of the effect. The meaning of this value is determined by the effect type.
        /// </summary>
        [JsonProperty]
        public double Magnitude
        {
            get { return _magnitude; }

            set
            {
                VerifyNotFrozen();
                _magnitude = value;
            }
        }

        /// <summary>
        ///     Gets or sets the area of the effect in tiles. The meaning of this value is determined by the effect type.
        /// </summary>
        [JsonProperty]
        public double Area
        {
            get { return _area; }

            set
            {
                VerifyNotFrozen();
                _area = value;
            }
        }

        protected override Freezable CreateInstanceCore()
        {
            return new EffectParameters();
        }

        protected override void CloneCore(Freezable source, bool cloneFrozen)
        {
            var realSource = (EffectParameters) source;

            EffectType.Name = realSource.EffectType.Name;
            Duration = realSource.Duration;
            Magnitude = realSource.Magnitude;
            Area = realSource.Area;
        }
    }
}