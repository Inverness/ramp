using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Ramp.Utilities.Text;

namespace Ramp.Logic.Signaling
{
    [TypeConverter(typeof(MulticastSignalConverter))]
    public struct MulticastSignal
    {
        private readonly Signal[] _items;

        public MulticastSignal(Signal signal)
        {
            if (!signal.IsValid)
                throw new ArgumentException("signal is invalid", nameof(signal));
            _items = new[] { signal };
        }

        public MulticastSignal(IEnumerable<Signal> signals)
        {
            if (signals == null)
                throw new ArgumentNullException(nameof(signals));
            _items = signals.ToArray();
            if (_items.Length == 0)
                throw new ArgumentException("must have at least one signal", nameof(signals));
            if (_items.Any(i => !i.IsValid))
                throw new ArgumentException("all signals must be valid", nameof(signals));
        }

        public IReadOnlyList<Signal> Items => _items ?? Array.Empty<Signal>();

        public bool HasItems => Items.Count != 0;

        public bool CanReceive(Signal other)
        {
            if (_items == null)
                return false;

            for (int i = 0; i < _items.Length; i++)
                if (_items[i].CanReceive(other))
                    return true;

            return false;
        }

        public override string ToString()
        {
            if (_items == null)
                return ":InvalidSignal";

            return DelimitedStringParser.ToCommaDelimited(_items);
        }

        public static MulticastSignal Parse(string signals, SignalScope scope = SignalScope.Default)
        {
            if (signals == null)
                throw new ArgumentNullException(nameof(signals));
            return
                new MulticastSignal(
                    DelimitedStringParser.ParseCommaDelimited(signals, true).Select(signal => Signal.Parse(signal)));
        }

        public static implicit operator MulticastSignal(Signal signal)
        {
            return new MulticastSignal(signal);
        }
    }
}