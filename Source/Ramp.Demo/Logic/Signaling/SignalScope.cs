namespace Ramp.Logic.Signaling
{
    public enum SignalScope
    {
        Default,
        Actor,
        Level,
        Region,
        World
    }
}