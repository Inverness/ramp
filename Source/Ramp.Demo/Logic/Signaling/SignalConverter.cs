using System.ComponentModel;
using System.Globalization;
using Ramp.Data;

namespace Ramp.Logic.Signaling
{
    internal class SignalConverter : StringTypeConverter
    {
        protected override object ConvertFromStringCore(ITypeDescriptorContext context, CultureInfo culture,
                                                        string value)
        {
            return value != null ? Signal.Parse(value) : new Signal();
        }
    }
}