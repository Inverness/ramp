using System.ComponentModel;
using System.Globalization;
using Ramp.Data;

namespace Ramp.Logic.Signaling
{
    internal class MulticastSignalConverter : StringTypeConverter
    {
        protected override object ConvertFromStringCore(ITypeDescriptorContext context, CultureInfo culture,
                                                        string value)
        {
            return value != null ? MulticastSignal.Parse(value) : new MulticastSignal();
        }
    }
}