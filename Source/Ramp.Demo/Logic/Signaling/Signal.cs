using System;
using System.ComponentModel;

namespace Ramp.Logic.Signaling
{
    [TypeConverter(typeof(SignalConverter))]
    public struct Signal
    {
        private readonly string _name;
        private readonly SignalScope _scope;

        private Signal(string name, SignalScope scope = SignalScope.Default)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("signal name must not be null or empty", nameof(name));

            if (name.IndexOf(':') != -1 || name.IndexOf(',') != -1)
                throw InvalidSignalString(name);

            _name = name;
            _scope = scope;
        }

        public string Name => _name;

        public SignalScope Scope => _scope;

        public bool IsValid => _name != null;

        public bool CanReceive(Signal other)
        {
            if (_name == null || other._name == null)
                return false;
            return _name.EqualsIgnoreCase(other._name);
        }

        public Signal SetScope(SignalScope scope)
        {
            return new Signal(_name, scope);
        }

        public override string ToString()
        {
            if (_name == null)
                return ":InvalidSignal";

            string prefix;
            switch (_scope)
            {
                case SignalScope.Default:
                    prefix = null;
                    break;
                case SignalScope.Actor:
                    prefix = "Actor:";
                    break;
                case SignalScope.Level:
                    prefix = "Level:";
                    break;
                case SignalScope.Region:
                    prefix = "Region:";
                    break;
                case SignalScope.World:
                    prefix = "World:";
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return prefix + _name;
        }

        public static Signal Parse(string name, SignalScope scope = SignalScope.Default)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("signal name must not be null or empty", nameof(name));

            ParseSignal(ref name, ref scope);

            return new Signal(name, scope);
        }

        internal static void ParseSignal(ref string signal, ref SignalScope scope)
        {
            int colonIndex = signal.IndexOf(':');
            if (colonIndex == -1)
                return;
            if (colonIndex == signal.Length - 1)
                throw InvalidSignalString(signal);

            string scopeName = signal.Substring(0, colonIndex);
            if (scopeName.EqualsIgnoreCase("Actor"))
                scope = SignalScope.Actor;
            else if (scopeName.EqualsIgnoreCase("Level"))
                scope = SignalScope.Level;
            else if (scopeName.EqualsIgnoreCase("Region"))
                scope = SignalScope.Region;
            else if (scopeName.EqualsIgnoreCase("World"))
                scope = SignalScope.World;
            else
                throw InvalidSignalString(signal);

            string realSignal = signal.Substring(colonIndex + 1);
            if (string.IsNullOrEmpty(realSignal))
                throw InvalidSignalString(signal);
            signal = realSignal;
        }

        internal static FormatException InvalidSignalString(string s)
        {
            return new FormatException("invalid signal string: " + s);
        }
    }
}