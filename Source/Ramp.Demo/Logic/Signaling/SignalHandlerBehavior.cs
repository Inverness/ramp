﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ramp.Simulation;

namespace Ramp.Logic.Signaling
{
    /// <summary>
    ///     A behavior that supports sending simple signals throughout a level.
    /// </summary>
    public abstract class SignalHandlerBehavior : Behavior
    {
        public const string ActivateSignal = "Activate";

        public const string SignalObjectTag = "SignalObject";

        private IRegionService _regionService;
        private SignalScope _defaultSendScope = SignalScope.Actor;

        protected SignalHandlerBehavior()
        {
            Actor.AddTag(SignalObjectTag);
        }

        public SignalScope DefaultSendScope
        {
            get { return _defaultSendScope; }

            set
            {
                if (value == SignalScope.Default)
                    throw new ArgumentOutOfRangeException(nameof(value), "must specify a default scope other than Default");
                _defaultSendScope = value;
            }
        }

        public void SendSignal(MulticastSignal signal, SignalScope scope = SignalScope.Default, Actor instigator = null)
        {
            if (!signal.HasItems || !Enabled)
                return;

            foreach (Signal bs in signal.Items)
            {
                Signal s = bs;

                if (s.Scope == SignalScope.Default)
                    s = s.SetScope(DefaultSendScope);

                foreach (SignalHandlerBehavior h in GetSignalReceivers(s.Scope))
                    h.ReceiveSignal(s, instigator, this);
            }
        }

        protected virtual void ReceiveSignal(Signal signal, Actor instigator, SignalHandlerBehavior sender)
        {
        }

        protected IEnumerable<SignalHandlerBehavior> GetSignalReceivers(SignalScope scope)
        {
            switch (scope)
            {
                case SignalScope.Actor:
                    return Actor.GetComponents<SignalHandlerBehavior>();
                case SignalScope.Level:
                    return Level.GetActorsWithTag(SignalObjectTag).
                                 SelectMany(a => a.GetComponents<SignalHandlerBehavior>());
                case SignalScope.Region:
                    if (_regionService == null)
                        _regionService = World.ServiceProvider.GetRequiredService<IRegionService>();

                    Region r = _regionService.GetRegion(Level);
                    if (r == null)
                        return GetSignalReceivers(SignalScope.Level);

                    return _regionService.GetLevelsInRegion(r).
                                          SelectMany(l => Level.GetActorsWithTag(SignalObjectTag)).
                                          SelectMany(a => a.GetComponents<SignalHandlerBehavior>());
                case SignalScope.World:
                    return World.Levels.SelectMany(l => Level.GetActorsWithTag(SignalObjectTag)).
                                 SelectMany(a => a.GetComponents<SignalHandlerBehavior>());
                default:
                    throw new ArgumentOutOfRangeException(nameof(scope));
            }
        }

        protected static bool CanReceive(MulticastSignal receiver, Signal signal)
        {
            return signal.IsValid && receiver.HasItems && receiver.CanReceive(signal);
        }
    }
}