﻿using System;
using System.Collections.Generic;
using Microsoft.Collections.Extensions;

namespace Ramp.Logic.Signaling
{
    public sealed class SignalDispatcher
    {
        private readonly MultiValueDictionary<Signal, Action<Signal>> _handlers = new MultiValueDictionary<Signal, Action<Signal>>();

        public void AddHandler(Signal signal, Action<Signal> handler)
        {
            _handlers.Add(signal, handler);
        }

        //public void AddHandler(SignalSocket socket, Action<Signal> handler)
        //{
        //    foreach (Signal s in socket.Items)
        //    {
        //        _handlers.Add(s, handler);
        //    }
        //}

        public void Dispatch(Signal signal)
        {
            if (!_handlers.TryGetValue(signal, out IReadOnlyCollection<Action<Signal>> handlers))
                return;

            foreach (Action<Signal> handler in handlers)
                handler(signal);
        }
    }
}
