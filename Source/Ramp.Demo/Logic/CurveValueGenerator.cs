﻿using System;
using System.Diagnostics;
using Microsoft.Xna.Framework;

namespace Ramp.Logic
{
    public sealed class CurveValueGenerator : ValueGenerator
    {
        public CurveValueGenerator()
        {
            Multiplier = 1;
        }

        public double Minimum { get; set; }

        public double Maximum { get; set; }

        public CurveType CurveType { get; set; }

        public double Multiplier { get; set; }

        public double Additive { get; set; }

        public override double Next(double time)
        {
            //var f = (Math.Sin(time * Math.PI + (-Math.PI / 2)) + 1) / 2;
            //var f = (Math.Cos(time * Math.PI + Math.PI) + 1) / 2;
            double processedTime = time * Multiplier + Additive;
            double result;

            switch (CurveType)
            {
                case CurveType.Multiple:
                    result = processedTime;
                    break;
                case CurveType.Sine:
                    // Basic sine wave formula has the bottom of a wave at time 0 and the peak at time 1
                    result = (Math.Cos(processedTime * Math.PI + Math.PI) + 1) / 2;
                    result = MathHelper.Lerp((float) Minimum, (float) Maximum, (float) result);
                    break;
                default:
                    Debug.Fail("Unhandled case");
                    return 0;
            }

            return Clamp(result, Minimum, Maximum);
        }

        private static double Clamp(double value, double min, double max)
        {
            if (value < min)
                return min;
            if (value > max)
                return max;
            return value;
        }
    }
}