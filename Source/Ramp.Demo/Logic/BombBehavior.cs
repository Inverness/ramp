using System;
using NLog;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Ramp.Logic.Character;
using Ramp.Network;
using Ramp.Rendering;
using Ramp.Simulation;

namespace Ramp.Logic
{
    /// <summary>
    ///     Provides logic for an actor that acts as a bomb
    /// </summary>
    public class BombBehavior : Behavior, IActivatable
    {
        private static readonly Logger s_log = LogManager.GetCurrentClassLogger();

        private AnimationRenderer _detonationAnimation;
        private readonly Collider _collider;
        private double _detonateTime;
        private double _time;
        private Actor _instigator;

        public BombBehavior()
        {
            Actor.IsActive = true;

            GetComponentRequired(out _collider);

            DetonationAnimation = Actor.GetSpawnArgument("DetonationAnimation", "Animations/BombExplosion");
            DetonationAnimationOffset = Actor.GetSpawnArgument("DetonationAnimationOffset", new Vector2(-4, -4));
            Timer = Actor.GetSpawnArgument("Timer", 3.0);
            Damage = Actor.GetSpawnArgument("Damage", 14);
            DamageRange = Actor.GetSpawnArgument("DamageRange", 4);
        }

        /// <summary>
        ///     True when the bomb has been activated.
        /// </summary>
        public bool WasActivated { get; private set; }

        /// <summary>
        ///     True once the bomb has detonated and can be disposed of.
        /// </summary>
        public bool WasDetonated { get; private set; }

        /// <summary>
        ///     Time from activation to detonation.
        /// </summary>
        public double Timer { get; set; }

        /// <summary>
        ///     Amount of damage dealt on detonation.
        /// </summary>
        public int Damage { get; set; }

        /// <summary>
        ///     Type of damage dealt by the explosion.
        /// </summary>
        public DamageType DamageType { get; set; }

        /// <summary>
        ///     Damage range in tiles.
        /// </summary>
        public float DamageRange { get; set; }

        /// <summary>
        ///     The name of the animation to spawn on detonation.
        /// </summary>
        public string DetonationAnimation { get; set; }

        /// <summary>
        ///     The offset from this actor to spawn the animation.
        /// </summary>
        public Vector2 DetonationAnimationOffset { get; set; }

        public string ActivationAnimation { get; set; }

        public override void Update(TimeSpan elapsed)
        {
            base.Update(elapsed);

            if (Actor.ClientSide)
                return;

            _time += elapsed.TotalSeconds;

            // Check if it's time to detonate.
            if (WasActivated && !WasDetonated && _time >= _detonateTime)
                Detonate(_instigator);
        }

        /// <summary>
        ///     Activates the bomb, starting a countdown if Timer > 0, or detonating immediately.
        /// </summary>
        /// <param name="instigator"> The instigator, or null. </param>
        public virtual void Activate(Actor instigator = null)
        {
            if (WasActivated || WasDetonated)
                return;

            _instigator = instigator;
            _detonateTime = _time + Timer;

            WasActivated = true;

            if (Timer <= 0)
                Detonate(instigator);
            else if (_detonationAnimation != null && ActivationAnimation != null)
                _detonationAnimation.Play(ActivationAnimation);
        }

        /// <summary>
        ///     Detonates the bomb.
        /// </summary>
        /// <param name="instigator"> The instigator, or null. </param>
        public virtual void Detonate(Actor instigator = null)
        {
            if (WasDetonated)
                return;

            _instigator = instigator;

            // Apply damage to actors within range.
            ApplyDamage(instigator);

            WasDetonated = true;

            // Now either destroy the actor immediately if necessary or play the animation then destroy.
            if (!PlayDetonationAnimation())
                Actor.MarkForDispose();
        }

        /// <summary>
        ///     Applies the bomb damage to actors within range.
        /// </summary>
        /// <param name="instigator"> The actor that instigated the detonation. </param>
        protected virtual void ApplyDamage(Actor instigator = null)
        {
            if (Actor.ClientSide)
                return;

            var sphere = new BoundingSphere(new Vector3(_collider.Transform.Location, 0), DamageRange);
            foreach (Actor actor in Level.GetActorsInBounds(sphere))
            {
                actor.GetComponent<HealthBehavior>()?.Damage(Damage, instigator, DamageType);
            }
        }

        /// <summary>
        ///     Plays the detonation animation for this bomb.
        /// </summary>
        /// <returns> Returns true if an animation is actually being played which postpones the destruction of this actor. </returns>
        [RemoteCallable]
        protected virtual bool PlayDetonationAnimation()
        {
            if (string.IsNullOrEmpty(DetonationAnimation))
                return false;

            Actor aniActor = Level.SpawnActor(_collider.Transform.Location + DetonationAnimationOffset, owner: Actor);
            _detonationAnimation = aniActor.AddComponent<AnimationRenderer>();

            try
            {
                _detonationAnimation.Play(DetonationAnimation);
            }
            catch (ContentLoadException ex)
            {
                s_log.Error("Failed to play detonation animation: {0}", ex);
                aniActor.MarkForDispose();
                return false;
            }

            _detonationAnimation.FrameChanged += OnDetonationAnimationFrameChanged;
            return true;
        }
        
        protected virtual void OnDetonationAnimationFrameChanged(AnimationRenderer sender, AnimationFrameChangedEventArgs e)
        {
            if (e.ChangeType < AnimationFrameChangeType.Finished)
                return;

            _detonationAnimation.FrameChanged -= OnDetonationAnimationFrameChanged;
            _detonationAnimation.Actor.MarkForDispose();
            _detonationAnimation = null;

            Actor.MarkForDispose();
        }
    }
}