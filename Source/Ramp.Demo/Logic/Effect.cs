using System;
using Ramp.Simulation;

namespace Ramp.Logic
{
    /// <summary>
    ///     An effect applied to an actor.
    /// </summary>
    public class Effect
    {
        internal TimeSpan TotalElapsedTime;

        internal TimeSpan LastUpdateElapsedTime;

        private static readonly TimeSpan s_oneSecond = TimeSpan.FromSeconds(1);

        public Effect(EffectParameters p, Actor target, Actor instigator)
        {
            Validate.ArgumentNotNull(p, "p");
            Parameters = p;
            Target = target;
            Instigator = instigator;
        }

        /// <summary>
        ///     Gets the effect parameters.
        /// </summary>
        public EffectParameters Parameters { get; private set; }

        /// <summary>
        ///     Gets the actor that caused the effect to be applied.
        /// </summary>
        public Actor Instigator { get; private set; }

        /// <summary>
        ///     Gets the target that the effect has been applied to.
        /// </summary>
        public Actor Target { get; private set; }

        /// <summary>
        ///     Gets whether this is a constant effect with no duration.
        /// </summary>
        public bool Constant => Parameters.Duration == TimeSpan.Zero;

        internal void Begin(TimeSpan time)
        {
            OnBegin(time);
        }

        internal void End(TimeSpan time)
        {
            OnEnd(time);
        }

        // Returns true if the effect should be removed
        internal bool Update(TimeSpan elapsed)
        {
            TotalElapsedTime += elapsed;
            LastUpdateElapsedTime += elapsed;

            if (!Constant && TotalElapsedTime >= Parameters.Duration)
                return true;

            if (LastUpdateElapsedTime >= s_oneSecond)
            {
                OnUpdate(LastUpdateElapsedTime);
                LastUpdateElapsedTime = TimeSpan.Zero;
            }

            return false;
        }

        /// <summary>
        ///     Begin applying the effect to the target actor.
        /// </summary>
        protected virtual void OnBegin(TimeSpan time)
        {
        }

        /// <summary>
        ///     Update the effect if necessary.
        /// </summary>
        /// <remarks>
        ///     This method is called once per second after the Begin() has been called.
        /// </remarks>
        protected virtual void OnUpdate(TimeSpan elapsed)
        {
        }

        /// <summary>
        ///     End the effect on the target actor.
        /// </summary>
        protected virtual void OnEnd(TimeSpan time)
        {
        }
    }
}