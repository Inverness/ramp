﻿using System;
using System.Collections.Generic;
using System.Linq;
using NLog;
using Microsoft.Xna.Framework;
using Ramp.Naming;
using Ramp.Session;
using Ramp.Simulation;

namespace Ramp.Logic
{
    public sealed class RegionManager : GameComponent, IRegionService
    {
        private static readonly Logger s_log = LogManager.GetCurrentClassLogger();

        private readonly IWorldManagerService _worldManagerService;
        private readonly ISessionService _sessionService;
        private readonly NamedDirectory _directory;
        private readonly Dictionary<string, Region> _regions = new Dictionary<string, Region>();
        private readonly Dictionary<Level, Region> _levelRegions = new Dictionary<Level, Region>();
        private readonly Dictionary<IPlayer, Region> _playerRegions = new Dictionary<IPlayer, Region>();

        public RegionManager(Game game)
            : base(game)
        {
            _worldManagerService = game.Services.GetRequiredService<IWorldManagerService>();
            _sessionService = game.Services.GetRequiredService<ISessionService>();
            _directory = NamedDirectory.Current;
            game.Services.AddService(typeof(IRegionService), this);
        }

        public event TypedEventHandler<IRegionService, RegionChangedEventArgs> RegionChanged;

        public override void Initialize()
        {
            _worldManagerService.World.LevelAdded += (sender, e) => RegisterLevel(e.Level);
            _worldManagerService.World.LevelRemoving += (sender, e) => UnregisterLevel(e.Level);
            _sessionService.PlayerLogin += (sender, e) => RegisterPlayer(e.Player);
            _sessionService.PlayerLogout += (sender, e) => UnregisterPlayer(e.Player);

            _directory.GetInners(null).OfType<Region>().ForEach(r => _regions[r.Name] = r);
        }

        public override void Update(GameTime gameTime)
        {
        }

        public void CheckRegion(IPlayer player)
        {
            if (player == null)
                throw new ArgumentNullException(nameof(player));
            if (player.Actor == null)
                return;

            Region matchingRegion, currentRegion;
            _levelRegions.TryGetValue(player.Actor.Level, out matchingRegion);
            _playerRegions.TryGetValue(player, out currentRegion);

            if (currentRegion != matchingRegion)
                ChangeRegion(player, currentRegion, matchingRegion);
        }

        public Region GetRegion(Level level)
        {
            Region r;
            _levelRegions.TryGetValue(level, out r);
            return r;
        }

        public IEnumerable<Level> GetLevelsInRegion(Region region)
        {
            foreach (KeyValuePair<Level, Region> item in _levelRegions)
            {
                if (item.Value == region)
                    yield return item.Key;
            }
        }

        private void ChangeRegion(IPlayer player, Region currentRegion, Region newRegion)
        {
            _playerRegions[player] = newRegion;
            OnRegionChanged(player, currentRegion, newRegion);
        }

        private void RegisterLevel(Level level)
        {
            Region region = _regions.Values.FirstOrDefault(z => z.Contains(level.Name));
            if (region != null)
                _levelRegions[level] = region;
        }

        private void UnregisterLevel(Level level)
        {
            _levelRegions.Remove(level);
        }

        private void RegisterPlayer(IPlayer player)
        {
            player.ActorChanged += OnPlayerActorChanged;
            player.ActorLevelChanged += OnPlayerActorLevelChanged;
            CheckRegion(player);
        }

        private void UnregisterPlayer(IPlayer player)
        {
            player.ActorChanged -= OnPlayerActorChanged;
            player.ActorLevelChanged -= OnPlayerActorLevelChanged;
            _playerRegions.Remove(player);
        }

        private void OnPlayerActorChanged(IPlayer sender, EventArgs e)
        {
            CheckRegion(sender);
        }

        private void OnPlayerActorLevelChanged(IPlayer sender, PlayerActorLevelChangedEventArgs e)
        {
            CheckRegion(sender);
        }

        private void OnRegionChanged(IPlayer player, Region oldRegion, Region newRegion)
        {
            RegionChanged?.Invoke(this, new RegionChangedEventArgs(player, oldRegion, newRegion));
        }
    }
}