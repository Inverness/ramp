﻿using System.Collections.Generic;
using Ramp.Simulation;

namespace Ramp.Logic
{
    public interface IRegionService
    {
        event TypedEventHandler<IRegionService, RegionChangedEventArgs> RegionChanged;

        Region GetRegion(Level level);

        IEnumerable<Level> GetLevelsInRegion(Region region);
    }
}