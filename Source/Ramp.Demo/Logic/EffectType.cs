﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Ramp.Data;
using Ramp.Naming;
using Ramp.Simulation;

namespace Ramp.Logic
{
    /// <summary>
    ///     Describes how a spell containing an effect is cast.
    /// </summary>
    public enum EffectCastingType
    {
        Concentration,
        Single,
        Constant
    }

    /// <summary>
    ///     Describes how a spell containing an effect is delivered.
    /// </summary>
    public enum EffectDeliveryType
    {
        Self,
        Contact,
        Aimed,
        TargetActor,
        TargetLocation
    }

    /// <summary>
    ///     Provides common properties for an effect.
    /// </summary>
    public class EffectType : Decl
    {
        public EffectType(string name, NamedObject outer)
            : base(name, outer)
        {
            Tags = new HashSet<string>();
        }

        /// <summary>
        ///     The name of the effect display in UI.
        /// </summary>
        [JsonProperty]
        public string FullName { get; private set; }

        /// <summary>
        ///     The casting type.
        /// </summary>
        [JsonProperty]
        public EffectCastingType CastingType { get; private set; }

        /// <summary>
        ///     The delivery type.
        /// </summary>
        [JsonProperty]
        public EffectDeliveryType DeliveryType { get; private set; }

        /// <summary>
        ///     The base mana cost for this effect.
        /// </summary>
        [JsonProperty]
        public double BaseCost { get; private set; }

        /// <summary>
        ///     Whether this effect indicates a hostile action.
        /// </summary>
        [JsonProperty]
        public bool Hostile { get; private set; }

        /// <summary>
        ///     The tags applied to this effect type.
        /// </summary>
        [JsonProperty]
        public ISet<string> Tags { get; private set; }

        public virtual Effect CreateEffect(EffectParameters p, Actor target, Actor instigator)
        {
            return new Effect(p, target, instigator);
        }
    }
}