﻿using System;
using Microsoft.Xna.Framework;

namespace Ramp.Logic
{
    public sealed class RandomCurveValueGenerator : ValueGenerator
    {
        private readonly Random _random = new Random();

        public RandomCurveValueGenerator()
        {
            Minimum = new CurveValueGenerator();
            Maximum = new CurveValueGenerator();
        }

        public CurveValueGenerator Minimum { get; private set; }

        public CurveValueGenerator Maximum { get; private set; }

        public override double Next(double time)
        {
            return MathHelper.Lerp(Minimum.NextSingle(time), Maximum.NextSingle(time), (float) _random.NextDouble());
        }
    }
}