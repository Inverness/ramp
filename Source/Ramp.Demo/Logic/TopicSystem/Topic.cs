using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Ramp.Data;
using Ramp.Naming;

namespace Ramp.Logic.TopicSystem
{
    public sealed class Topic : Decl
    {
        private readonly List<Response> _responses = new List<Response>();

        public Topic(string name, NamedObject outer)
            : base(name, outer)
        {
        }

        public string Text { get; private set; }

        public bool Random { get; private set; }

        public IReadOnlyList<Response> Responses => _responses;

        public override Task Update(JObject decl, bool validated, DeclManager manager)
        {
            Text = (string) decl["Text"];
            Random = (bool) decl["Random"];
            return Task.CompletedTask;
        }

        internal void AddResponse(Response response)
        {
            Debug.Assert(response != null && !_responses.Contains(response));
            _responses.Add(response);
            _responses.Sort((r1, r2) => r1.Index.CompareTo(r2.Index));
        }
    }
}