using System;
using System.Linq;
using Ramp.Utilities.Text;

namespace Ramp.Logic.TopicSystem
{
    public enum Comparison
    {
        Equals,
        NotEquals,
        LessThan,
        LessThanEquals,
        GreaterThan,
        GreaterThenEquals
    }

    /// <summary>
    ///     A condition of a topic response evaluated to determine whether a topic response is valid.
    /// </summary>
    public class Condition
    {
        private static readonly string[] s_comparisonSymbols = { "==", "!=", "<", "<=", ">", ">=" };

        private Condition(string functionName, string argument1, string argument2, Comparison comparison,
                          string value, bool or)
        {
            FunctionName = functionName;
            Argument1 = argument1;
            Argument2 = argument2;
            Comparison = comparison;
            Value = value;
            Or = or;
        }

        public string FunctionName { get; private set; }

        public string Argument1 { get; private set; }

        public string Argument2 { get; private set; }

        public Comparison Comparison { get; private set; }

        public string Value { get; private set; }

        public bool Or { get; private set; }

        /// <summary>
        ///     Parses a topic condition string.
        /// </summary>
        /// <remarks>
        ///     Condition must be in the format: FunctionName [Argument1] [Argument2] Comparison Value [Or]
        ///     Comparison must be one of: ==, !=, &lt;, &lt;=, &gt;, &gt;=.
        ///     Tokens in braces are optional. Or, if specified, must be "OR".
        /// </remarks>
        /// <param name="input"> The condition string. </param>
        /// <returns> A TopicCondition. </returns>
        public static Condition Parse(string input)
        {
            // Func [P1 P2] Eq Val [Or]
            string[] tokens = StringTokenizer.Tokenize(input);
            if (tokens.Length < 3 || tokens.Length > 6)
                throw new FormatException("wrong number of tokens");

            // Identify comparison in the three possible indices and parse it
            int comparisonIndex = -1;
            var comparison = Comparison.Equals;
            for (int i = 1; i < 4; i++)
            {
                if (!s_comparisonSymbols.Contains(tokens[i]))
                    continue;
                comparisonIndex = i;
                comparison = (Comparison) Array.IndexOf(s_comparisonSymbols, tokens[i]);
                break;
            }

            if (comparisonIndex == -1)
                throw new FormatException("comparison not found");

            // Parse arguments
            int argumentCount = comparisonIndex - 1;
            string argument1 = argumentCount > 0 ? tokens[comparisonIndex - argumentCount] : null;
            string argument2 = argumentCount > 1 ? tokens[comparisonIndex - argumentCount + 1] : null;

            // Parse value
            string value = tokens[comparisonIndex + 1];

            // Parse "or"
            bool or;
            if (comparisonIndex + 2 >= tokens.Length)
            {
                or = false;
            }
            else
            {
                if (tokens[comparisonIndex + 2] != "OR")
                    throw new FormatException("bad 'OR' token");
                or = true;
            }

            return new Condition(tokens[0], argument1, argument2, comparison, value, or);
        }
    }
}