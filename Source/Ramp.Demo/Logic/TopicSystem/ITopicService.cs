using System.Collections.Generic;

namespace Ramp.Logic.TopicSystem
{
    public interface ITopicService
    {
        //event TypedEventHandler<ITopicService, ResponseReceivedEventArgs> ClientResponseReceived;

        void GetResponse(string topic, string actor, int choice, out string text, out IEnumerable<string> choices);

        KeyValuePair<string, string>[] GetTopicList(string topicList);

        void ClientGetResponse(string topic, string actor, int choice = 0);

        void ClientGetTopicList(string topicList);
    }

    //public class ResponseReceivedEventArgs : EventArgs
    //{
    //	public ResponseReceivedEventArgs(string responseId, string text, IEnumerable<string> choices)
    //	{
    //		ResponseId = responseId;
    //		Text = text;
    //		Choices = choices;
    //	}

    //	public string ResponseId { get; private set; }

    //	public string Text { get; private set; }

    //	public IEnumerable<string> Choices { get; private set; }
    //}

    //public class TopicListReceivedEventArgs : EventArgs
    //{
    //	public TopicListReceivedEventArgs(IEnumerable<KeyValuePair<string, string>> topicList)
    //	{
    //		TopicList = topicList;
    //	}

    //	public IEnumerable<KeyValuePair<string, string>> TopicList { get; private set; }
    //}
}