using System.Collections.Generic;

namespace Ramp.Logic.TopicSystem
{
    public class ResponseRequestReply
    {
        private static readonly List<string> s_emptyChoices = new List<string>();

        public ResponseRequestReply(string topicId, string responseId, string title, string text,
                                    IEnumerable<string> choices)
        {
            TopicId = topicId;
            ResponseId = responseId;
            Title = title;
            Text = text;
            Choices = choices == null ? s_emptyChoices : new List<string>(choices);
        }

        public string TopicId { get; private set; }

        public string ResponseId { get; private set; }

        public string Title { get; private set; }

        public string Text { get; private set; }

        public IReadOnlyList<string> Choices { get; private set; }
    }
}