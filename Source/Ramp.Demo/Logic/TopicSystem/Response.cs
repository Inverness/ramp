using System;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Ramp.Data;
using Ramp.Naming;

namespace Ramp.Logic.TopicSystem
{
    public sealed class Response : Decl
    {
        private static readonly Condition[] s_noConditions = new Condition[0];

        public Response(string name, NamedObject outer)
            : base(name, outer)
        {
        }

        public string Topic { get; private set; }

        public ushort Index { get; private set; }

        public string Text { get; private set; }

        public Condition[] Conditions { get; private set; }

        public override Task Update(JObject decl, bool validated, DeclManager manager)
        {
            Topic = (string) decl["Topic"];
            Index = (ushort) decl["Index"];
            Text = (string) decl["Text"];

            var jConditions = decl["Conditions"] as JArray;
            Conditions = jConditions != null
                             ? jConditions.Select(c => Condition.Parse((string) c)).ToArray()
                             : s_noConditions;

            return Task.CompletedTask;
        }
    }
}