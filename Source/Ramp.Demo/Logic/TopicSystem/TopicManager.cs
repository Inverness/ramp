using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Ramp.Data;
using Ramp.Naming;

namespace Ramp.Logic.TopicSystem
{
    public class TopicManager : GameComponent, ITopicService
    {
        private readonly IConfigService _configService;
        private readonly NamedDirectory _directory;

        public TopicManager(Game game)
            : base(game)
        {
            _configService = game.Services.GetRequiredService<IConfigService>();
            _directory = NamedDirectory.Current;
        }

        //public event TypedEventHandler<ITopicService, ResponseReceivedEventArgs> ClientResponseReceived;

        public override void Initialize()
        {
            base.Initialize();

            // Match responses to topics
            foreach (Response response in _directory.GetInners(null).OfType<Response>())
            {
                var topic = _directory.Find<Topic>(response.Topic);
                topic.AddResponse(response);
            }
        }

        public void GetResponse(string topic, string actor, int choice, out string text, out IEnumerable<string> choices)
        {
            throw new NotImplementedException();
        }

        public KeyValuePair<string, string>[] GetTopicList(string topicList)
        {
            throw new NotImplementedException();
        }

        public void ClientGetResponse(string topic, string actor, int choice = 0)
        {
            throw new NotImplementedException();
        }

        public void ClientGetTopicList(string topicList)
        {
            throw new NotImplementedException();
        }
    }
}