﻿using System;
using Microsoft.Xna.Framework;
using Ramp.Rendering;
using Ramp.Simulation;

namespace Ramp.Logic
{
    /// <summary>
    ///     Displays a sprite for a container of items in a level. This is used when placing an item on the ground
    ///     instead of inside a container.
    /// </summary>
    public class ItemSpriteBehavior : Behavior, IActivatable
    {
        private readonly SpriteRenderer _sprite;
        private readonly ItemCollectionComponent _items;
        private readonly Collider _collider;

        public ItemSpriteBehavior()
        {
            GetComponentRequired(out _sprite);
            GetComponentRequired(out _items);
            if (GetComponent(out _collider))
            {
                _collider.ActorOverlapBegan += OnActorOverlapBegan;
                _collider.ActorBlocked += OnActorBlocked;
            }

            if (_items.Items.Count == 0)
                throw new InvalidOperationException("must have items already");

            _sprite.Texture = _items.Items[0].ItemType.Icon;
            _sprite.TexturePart = _items.Items[0].ItemType.IconPart ?? Rectangle.Empty;

            ActivateOnCollision = Actor.GetSpawnArgument("ActivateOnCollision", true);
        }

        public bool ActivateOnCollision { get; set; }

        public void Activate(Actor instigator)
        {
            if (!Enabled || instigator == null)
                return;

            var targetItems = instigator.GetComponent<ItemCollectionComponent>();
            if (targetItems == null)
                return;

            foreach (Item sourceItem in _items.Items)
                targetItems.Add(sourceItem, sourceItem.Count);

            Actor.MarkForDispose();
        }

        private void OnActorBlocked(Collider sender, ActorCollisionEventArgs e)
        {
            if (ActivateOnCollision)
            {
                Activate(e.Other.Actor);

                if (Actor.Status >= ActorStatus.Disposing)
                    e.Cancel = true;
            }
        }

        private void OnActorOverlapBegan(Collider sender, ActorCollisionEventArgs e)
        {
            if (ActivateOnCollision)
            {
                Activate(e.Other.Actor);

                if (Actor.Status >= ActorStatus.Disposing)
                    e.Cancel = true;
            }
        }
    }
}