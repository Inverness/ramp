using Ramp.Simulation;

namespace Ramp.Logic
{
    // Will contain special logic when entering a dungeon.
    public class DungeonEntryBehavior : Behavior
    {
        public DungeonEntryBehavior()
        {
            World.ActorSpawned += OnActorSpawned;
            World.ActorDisposed += OnActorDisposed;
        }

        public string QuestId { get; set; }

        public ushort QuestStage { get; set; }

        private void OnActorDisposed(World sender, ActorEventArgs e)
        {
            if (e.Actor.Static)
                return;
        }

        private void OnActorSpawned(World sender, ActorEventArgs e)
        {
            if (e.Actor.Static)
                return;
        }
    }
}