using System.Collections.Generic;
using Ramp.Simulation;

namespace Ramp.Logic
{
    public interface IQuestService
    {
        event TypedEventHandler<IQuestService, QuestStageChangedEventArgs> QuestStageChanged;

        IReadOnlyDictionary<string, Quest> Quests { get; }

        QuestStage GetCurrentStage(string quest, IPlayer player);

        bool SetCurrentStage(string quest, IPlayer player, string id);
    }
}