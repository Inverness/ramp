using System;
using System.Collections.Generic;
using System.ComponentModel;
using Ramp.Simulation;

namespace Ramp.Logic.Character
{
    /// <summary>
    ///     Describes slots that items to be equipped to in an EquipmentBehavior.
    /// </summary>
    public enum EquipmentSlot : byte
    {
        LeftHand,
        RightHand,
        Head,
        Body,
        Arms,
        Legs,
        Amulet,
        Ring
    }

    /// <summary>
    ///     Allows the equipping of items.
    /// </summary>
    public class EquipmentBehavior : Behavior
    {
        private readonly Item[] _slots;
        private readonly ItemCollectionComponent _items;
        private readonly HealthBehavior _health;
        private readonly CharacterBehavior _character;

        public EquipmentBehavior()
        {
            _slots = new Item[Enum.GetNames(typeof(EquipmentSlot)).Length];
            GetComponentRequired(out _items);
            GetComponentRequired(out _health);
            GetComponentRequired(out _character);

            ArmorResistanceType = Directory.Find<DamageType>(DamageTypeNames.Physical);
        }

        /// <summary>
        ///     Raised after an item is equipped.
        /// </summary>
        public event TypedEventHandler<EquipmentBehavior, EquipEventArgs> Equipped;

        /// <summary>
        ///     Raised before an item is eqiupped. Allows cancel.
        /// </summary>
        public event TypedEventHandler<EquipmentBehavior, EquipEventArgs> Equipping;

        /// <summary>
        ///     Raised after an item is unequipped.
        /// </summary>
        public event TypedEventHandler<EquipmentBehavior, EquipEventArgs> Unequipped;

        /// <summary>
        ///     Raised before an item is unequipped. Allows cancel.
        /// </summary>
        public event TypedEventHandler<EquipmentBehavior, EquipEventArgs> Unequipping;

        //public event TypedEventHandler<EquipmentBehavior, EquipEventArgs> Activated; 

        /// <summary>
        ///     Equipment slots indexed by <see cref="EquipmentSlot" />.
        /// </summary>
        public IReadOnlyList<Item> Slots => _slots;

        /// <summary>
        ///     Gets or sets the damage type that armor helps resist.
        /// </summary>
        public DamageType ArmorResistanceType { get; set; }

        public Item GetItem(EquipmentSlot slot)
        {
            return _slots[(int) slot];
        }

        /// <summary>
        ///     Equiip an item of the specified type.
        /// </summary>
        /// <param name="item"> The item to be equipped. </param>
        /// <returns> True if the equip succeeds; false if the equip is canceled. </returns>
        public virtual bool Equip(string item)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item));
            VerifyEnabled();

            Item itemObject = _items.Get(item);
            if (itemObject == null)
                throw new ArgumentException("item not found with matching type name", nameof(item));

            return Equip(itemObject);
        }

        /// <summary>
        ///     Equip an item.
        /// </summary>
        /// <param name="item"> The item to be equipped. </param>
        /// <returns> True if equip succeeds; false if equip is canceled. </returns>
        /// <exception cref="ArgumentNullException">Raised when item is null.</exception>
        /// <exception cref="ArgumentException">Raised when item is not in the current actor's container.</exception>
        public virtual bool Equip(Item item)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item));
            if (item.Collection != _items)
                throw new ArgumentException("item is not in local container", nameof(item));
            VerifyEnabled();

            EquipmentSlot? slot = GetEquipSlot(item);
            if (!slot.HasValue)
                return false;

            // Try unequiping an existing item if the slot is occupied.
            Item equippedItem = _slots[(int) slot.Value];
            if (equippedItem != null)
            {
                if (!Unequip(equippedItem))
                    return false;
            }

            // Raise Equipping and maybe cancel the equip if game logic dictates.
            if (OnEquipping(item, slot.Value))
                return false;

            _slots[(int) slot.Value] = item;

            OnEquipped(item, slot.Value);
            return true;
        }

        /// <summary>
        ///     Unequip the item in the specified slot.
        /// </summary>
        /// <param name="slot"> The slot. </param>
        /// <returns> True if the unequip succeeds or the slot is empty; false if the unequip is canceled. </returns>
        public bool Unequip(EquipmentSlot slot)
        {
            Item item = _slots[(int) slot];
            if (item == null)
                return true;
            return Unequip(item);
        }

        /// <summary>
        ///     Unequip an item.
        /// </summary>
        /// <param name="item"> The item to unequip. </param>
        /// <returns> True if unequip succeedes or the slot is empty; false if unequip is canceled. </returns>
        public virtual bool Unequip(Item item)
        {
            VerifyEnabled();

            if (item == null)
                throw new ArgumentNullException(nameof(item));

            EquipmentSlot? slot = GetCurrentSlot(item);
            if (!slot.HasValue)
                return false;

            // Raise Unequipping and maybe cancel the unequip if game logic dictates.
            if (OnEquipping(item, slot.Value))
                return false;

            _slots[(int) slot.Value] = null;

            OnUnequipped(item, slot.Value);
            return true;
        }

        /// <summary>
        ///     Get the slot an item is currently equipped to.
        /// </summary>
        /// <param name="item"> An item. </param>
        /// <returns> The slot the item occupies, or null if the item is not equipped. </returns>
        public EquipmentSlot? GetCurrentSlot(Item item)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item));
            if (item.Collection != _items)
                throw new ArgumentException("item is not in local container", nameof(item));

            for (byte i = 0; i < _slots.Length; i++)
            {
                if (_slots[i] == item)
                    return (EquipmentSlot) i;
            }

            return null;
        }

        /// <summary>
        ///     Check if an item is currently equipped.
        /// </summary>
        /// <param name="item"> An item. </param>
        /// <returns> True if the item is equipped. </returns>
        public bool IsEquipped(Item item)
        {
            return GetCurrentSlot(item).HasValue;
        }

        //public bool Activate(EquipmentSlot slot)
        //{
        //    Item item = GetItem(slot);
        //    if (item == null)
        //        return false;
        //    bool canceled = OnActivated(item, slot);
        //    return !canceled;
        //}

        /// <summary>
        ///     Gets the slot an item will be equipped to based on type.
        /// </summary>
        /// <param name="item"> An item. </param>
        /// <returns> The matching slot, or null. </returns>
        protected virtual EquipmentSlot? GetEquipSlot(Item item)
        {
            if (item.ItemType is WeaponItemType)
            {
                return EquipmentSlot.RightHand;
            }

            if (item.ItemType is SpellGemItemType)
            {
                return EquipmentSlot.LeftHand;
            }

            if (item.ItemType is ArmorItemType)
            {
                var armor = (ArmorItemType) item.ItemType;
                switch (armor.ArmorType)
                {
                    case ArmorType.Head:
                        return EquipmentSlot.Head;
                    case ArmorType.Body:
                        return EquipmentSlot.Body;
                    case ArmorType.Arms:
                        return EquipmentSlot.Arms;
                    case ArmorType.Legs:
                        return EquipmentSlot.Legs;
                }
            }

            return null;
        }

        protected virtual bool OnEquipping(Item item, EquipmentSlot slot)
        {
            if (Equipping == null)
                return false;
            var e = new EquipEventArgs(item, slot);
            Equipping(this, e);
            return e.Cancel;
        }

        protected virtual void OnEquipped(Item item, EquipmentSlot slot)
        {
            // TODO: Actual logic should be placed in a subclass or attached script.
            var asArmor = item.ItemType as ArmorItemType;
            if (asArmor != null)
                ModifyArmorResistanceValue(asArmor.ArmorValue);

            Equipped?.Invoke(this, new EquipEventArgs(item, slot));
        }

        protected virtual bool OnUnequipping(Item item, EquipmentSlot slot)
        {
            if (Unequipping == null)
                return false;
            var e = new EquipEventArgs(item, slot);
            Unequipping(this, e);
            return e.Cancel;
        }

        protected virtual void OnUnequipped(Item item, EquipmentSlot slot)
        {
            var asArmor = item.ItemType as ArmorItemType;
            if (asArmor != null)
                ModifyArmorResistanceValue(-asArmor.ArmorValue);

            Unequipped?.Invoke(this, new EquipEventArgs(item, slot));
        }

        protected bool ModifyArmorResistanceValue(float value)
        {
            CompositeValue cv = _health.GetResistance(ArmorResistanceType);
            if (cv == null)
                return false;

            cv.Base += value;
            return true;
        }

        //protected virtual bool OnActivated(Item item, EquipmentSlot slot)
        //{
        //    if (Activated == null)
        //        return false;
        //    var e = new EquipEventArgs(item, slot);
        //    Activated(this, e);
        //    return e.Cancel;
        //}
    }

    public class EquipEventArgs : CancelEventArgs
    {
        public EquipEventArgs(Item item, EquipmentSlot slot)
        {
            Item = item;
            Slot = slot;
        }

        public Item Item { get; private set; }

        public EquipmentSlot Slot { get; private set; }
    }
}