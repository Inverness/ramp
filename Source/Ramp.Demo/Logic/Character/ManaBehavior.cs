﻿using System;
using Ramp.Simulation;

namespace Ramp.Logic.Character
{
    public class ManaBehavior : Behavior
    {
        public ManaBehavior()
        {
            Actor.IsActive = true;

            Mana = new CompositeValue { Minimum = 0 };
            ManaBase = new CompositeValue { Minimum = 0, Base = 20 };
            ManaMultiplier = new CompositeValue { Minimum = 0, Base = 1 };
            ManaRegenRate = new CompositeValue { Minimum = 0, Base = 1 };

            ManaBase.ValueChanged += (sender, e) => UpdateMana();
            ManaMultiplier.ValueChanged += (sender, e) => UpdateMana();
        }

        public CompositeValue Mana { get; private set; }

        public CompositeValue ManaBase { get; private set; }

        public CompositeValue ManaMultiplier { get; private set; }

        public CompositeValue ManaRegenRate { get; private set; }

        public override void Update(TimeSpan elapsed)
        {
            base.Update(elapsed);

            if (!Enabled)
                return;

            if (Mana.Damage > 0 && ManaRegenRate.Value > 0)
                Mana.Damage -= ManaRegenRate.Value * (float) elapsed.TotalSeconds;
        }

        private void UpdateMana()
        {
            Mana.Base = ManaBase.Value * ManaMultiplier.Value;
        }
    }
}