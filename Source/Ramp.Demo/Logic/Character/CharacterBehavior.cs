using System;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using NLog;
using Ramp.Data;
using Ramp.Network;
using Ramp.Rendering;
using Ramp.Simulation;

namespace Ramp.Logic.Character
{
    public enum ArrowType
    {
        Normal
    }

    public enum CharacterAttribute
    {
        Strength,
        Dexterity,
        Constitution,
        Intelligence,
        Wisdom,
        Charisma
    }

    /// <summary>
    ///     Handles inter-component logic needed for NPC and player characters.
    /// </summary>
    public sealed class CharacterBehavior : Behavior
    {
        public const byte BodyAliasIndex = 0;
        public const byte HeadAliasIndex = 1;
        public const byte SwordAliasIndex = 2;
        public const byte ShieldAliasIndex = 3;
        public const byte HatAliasIndex = 4;

        /// <summary>
        ///		Number of attribute values per rank.
        /// </summary>
        public const byte AttributeRankScale = 5;

        /// <summary>
        ///		Number of level values per rank.
        /// </summary>
        public const byte LevelRankScale = 3;

        public const string IdleState = "Idle";
        public const string WalkingState = "Walking";
        public const string PushingState = "Pushing";
        public const string DeadState = "Dead";

        private static readonly TimeSpan s_pushWaitTime = TimeSpan.FromSeconds(1);
        private static readonly TimeSpan s_pushStopTime = TimeSpan.FromSeconds(0.2);

        private static readonly Logger s_log = LogManager.GetCurrentClassLogger();

        private static readonly Color s_outlineColorKey = new Color(0, 0, 0, 255);
        private static readonly Color s_beltColorKey = new Color(0, 0, 255, 255);
        private static readonly Color s_sleeveColorKey = new Color(255, 0, 0, 255);
        private static readonly Color s_coatColorKey = new Color(255, 255, 255, 255);
        private static readonly Color s_skinColorKey = new Color(255, 173, 107, 255);
        private static readonly Color s_shoeColorKey = new Color(204, 24, 41, 255);

        private readonly StateComponent _state;
        private readonly MovementBehavior _movement;
        private readonly AnimationRenderer _animation;
        private readonly HealthBehavior _health;
        private Direction _lastMoveDirection = Direction.Down; // last direction we moved in, never None.
        private readonly TextureEditor _bodyTextureEditor = new TextureEditor("EditedCharacterBody");

        public CharacterBehavior()
        {
            Actor.IsActive = true;

            GetComponentRequired(out _state);
            GetComponentRequired(out _movement);
            GetComponentRequired(out _animation);
            if (GetComponent(out _health))
                _health.Damaged += OnHealthDamaged;

            _state.StateChanged += OnStateChanged;
            _movement.DirectionChanged += OnMovementDirectionChanged;

            UpdateHealthBase();

            _bodyTextureEditor.SourceName = "Textures/TestBody";
            _bodyTextureEditor.UpdateTarget();

            _animation.SetTextureAlias(BodyAliasIndex, _bodyTextureEditor.TargetName);
            _animation.SetTextureAlias(HeadAliasIndex, "Textures/TestHead");

            _state.Set(IdleState);
        }

        public AssetReference<Animation> IdleAnimation { get; } = new AssetReference<Animation>("Animations/Char/Idle");

        public AssetReference<Animation> WalkAnimation { get; } = new AssetReference<Animation>("Animations/Char/Walk");

        public AssetReference<Animation> PushAnimation { get; } = new AssetReference<Animation>("Animations/Char/Push");

        public AssetReference<Animation> DamagedAnimation { get; } = new AssetReference<Animation>("Animations/Char/Damaged");

        public CompositeValue Strength { get; } = new CompositeValue(5, 1, 25);

        public CompositeValue Dexterity { get; } = new CompositeValue(5, 1, 25);

        public CompositeValue Constitution { get; } = new CompositeValue(5, 1, 25);

        public CompositeValue Intelligence { get; } = new CompositeValue(5, 1, 25);

        public CompositeValue Wisdom { get; } = new CompositeValue(5, 1, 25);

        public CompositeValue Charisma { get; } = new CompositeValue(5, 1, 25);

        public Color? BodyBeltColor
        {
            get { return _bodyTextureEditor.GetColorReplacement(s_beltColorKey); }

            set
            {
                _bodyTextureEditor.SetColorReplacement(s_beltColorKey, value);
                _bodyTextureEditor.UpdateTarget();
            }
        }

        public Color? BodySleeveColor
        {
            get { return _bodyTextureEditor.GetColorReplacement(s_sleeveColorKey); }

            set
            {
                _bodyTextureEditor.SetColorReplacement(s_sleeveColorKey, value);
                _bodyTextureEditor.UpdateTarget();
            }
        }

        public Color? BodyCoatColor
        {
            get { return _bodyTextureEditor.GetColorReplacement(s_coatColorKey); }

            set
            {
                _bodyTextureEditor.SetColorReplacement(s_coatColorKey, value);
                _bodyTextureEditor.UpdateTarget();
            }
        }

        public Color? BodySkinColor
        {
            get { return _bodyTextureEditor.GetColorReplacement(s_skinColorKey); }

            set
            {
                _bodyTextureEditor.SetColorReplacement(s_skinColorKey, value);
                _bodyTextureEditor.UpdateTarget();
            }
        }

        public Color? BodyShoeColor
        {
            get { return _bodyTextureEditor.GetColorReplacement(s_shoeColorKey); }

            set
            {
                _bodyTextureEditor.SetColorReplacement(s_shoeColorKey, value);
                _bodyTextureEditor.UpdateTarget();
            }
        }

        public Direction LastMoveDirection => _lastMoveDirection;

        public bool IsPushing => _state.Current == PushingState;

        public CompositeValue GetAttribute(CharacterAttribute attribute)
        {
            switch (attribute)
            {
                case CharacterAttribute.Strength:
                    return Strength;
                case CharacterAttribute.Dexterity:
                    return Dexterity;
                case CharacterAttribute.Constitution:
                    return Constitution;
                case CharacterAttribute.Intelligence:
                    return Intelligence;
                case CharacterAttribute.Wisdom:
                    return Wisdom;
                case CharacterAttribute.Charisma:
                    return Charisma;
                default:
                    throw new ArgumentOutOfRangeException(nameof(attribute));
            }
        }

        public override void Update(TimeSpan elapsed)
        {
            base.Update(elapsed);

            if (Enabled)
                DetectPush();
        }

        //public void Activate(Actor target)
        //{
        //    if (target == null)
        //        throw new ArgumentNullException("target");
        //    if (target == Actor)
        //        throw new ArgumentException("cannot target self", "target");

        //    foreach (var activatable in target.GetComponents<IActivatable>())
        //        activatable.Activate(Actor);
        //}

        public void StopPush()
        {
            if (IsPushing)
                _state.Set(IdleState);
        }

        public override void WriteDelta(DeltaWriter writer, ReplicationContext context)
        {
            base.WriteDelta(writer, context);

            IdleAnimation.Serialize(writer, context);
            WalkAnimation.Serialize(writer, context);
            PushAnimation.Serialize(writer, context);
            DamagedAnimation.Serialize(writer, context);
        }

        public override void ReadDelta(DeltaReader reader, ReplicationContext context)
        {
            base.ReadDelta(reader, context);

            IdleAnimation.Deserialize(reader, context);
            WalkAnimation.Deserialize(reader, context);
            PushAnimation.Deserialize(reader, context);
            DamagedAnimation.Deserialize(reader, context);
        }

        public static Rank GetAttributeRank(CompositeValue value)
        {
            return RankUtility.GetRank(value.Value, AttributeRankScale);
        }

        public static Rank GetLevelRank(int level)
        {
            return RankUtility.GetRank(level, LevelRankScale);
        }

        /// <summary>
        ///     Detect when the character is pushing something and play the appropriate animation.
        /// </summary>
        private void DetectPush()
        {
            bool failed = _movement.LastMoveTime - _movement.LastSuccessfulMoveTime >= s_pushWaitTime;
            bool hadRecentMove = Level.Time - _movement.LastSuccessfulMoveTime < s_pushStopTime;
            bool stoppedMoving = Level.Time - _movement.LastMoveTime > s_pushStopTime;

            if ((hadRecentMove || stoppedMoving) && IsPushing)
                _state.Set(IdleState);
            else if (failed && !stoppedMoving && !IsPushing)
                _state.Set(PushingState);
        }

        private void OnStateChanged(StateComponent sender, StateChangedEventArgs e)
        {
            Debug.Assert(MovementUtility.IsCardinal(_lastMoveDirection));

            switch (_state.Current)
            {
                case IdleState:
                    _animation.Play(IdleAnimation.Name, _lastMoveDirection);
                    break;
                case WalkingState:
                    _animation.Play(WalkAnimation.Name, _lastMoveDirection);
                    break;
                case PushingState:
                    _animation.Play(PushAnimation.Name, _lastMoveDirection);
                    break;
                case DeadState:
                    s_log.Info("Character {0} has died", Actor.Name);
                    break;
            }
        }

        private void OnHealthDamaged(HealthBehavior sender, HealthChangedEventArgs e)
        {
            if (_health.IsDead)
                _state.Set(DeadState);
        }

        // Called when the movement direction changes so we can change the animation.
        private void OnMovementDirectionChanged(MovementBehavior sender, EventArgs e)
        {
            switch (sender.Direction)
            {
                case Direction.None:
                    if (_state.Current == WalkingState)
                        _state.Set(IdleState);
                    break;
                case Direction.Up:
                case Direction.Left:
                case Direction.Down:
                case Direction.Right:
                    _lastMoveDirection = sender.Direction;
                    goto case Direction.UpRight;
                case Direction.UpLeft:
                case Direction.DownLeft:
                case Direction.DownRight:
                case Direction.UpRight:
                    if (_state.Current == WalkingState || _state.Current == IdleState)
                        _state.Set(WalkingState, true);
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        /// <summary>
        ///     Update the health base value based on attributes. Does nothing if the actor has no attributes.
        /// </summary>
        private void UpdateHealthBase()
        {
            if (_health == null)
                return;
            Debug.Assert(Constitution.Minimum >= 1);
            _health.Health.Base = _health.HealthBase.Value + Constitution.Value * _health.HealthMultiplier.Value;
        }
    }
}