using System;
using System.ComponentModel;
using System.Diagnostics;
using NLog;
using Ramp.Network;
using Ramp.Simulation;

// ReSharper disable CompareOfFloatsByEqualityOperator

namespace Ramp.Logic.Character
{
    /// <summary>
    ///     Behavior used for actors that have health that can be damaged and killed.
    /// </summary>
    public class HealthBehavior : Behavior
    {
        private static readonly Logger s_log = LogManager.GetCurrentClassLogger();

        public HealthBehavior()
        {
            Actor.IsActive = true;
            Health = new CompositeValue(Actor.GetSpawnArgument(nameof(Health), 10f), 0f);
            HealthMultiplier = new CompositeValue(Actor.GetSpawnArgument(nameof(HealthMultiplier), 3f), 1);
            HealthBase = new CompositeValue(Health.Value, 0f);
            HealthRegenRate = new CompositeValue(Actor.GetSpawnArgument(nameof(HealthRegenRate), 0.2f), 0);
            PhysicalResistance = new CompositeValue(Actor.GetSpawnArgument(nameof(PhysicalResistance), 0f), 0, 0.8f);
            MagicResistance = new CompositeValue(Actor.GetSpawnArgument(nameof(MagicResistance), 0f), 0, 1);
            FireResistance = new CompositeValue(Actor.GetSpawnArgument(nameof(FireResistance), 0f), 0, 1);
            FrostResistance = new CompositeValue(Actor.GetSpawnArgument(nameof(FrostResistance), 0f), 0, 1);
            ShockResistance = new CompositeValue(Actor.GetSpawnArgument(nameof(ShockResistance), 0f), 0, 1);
        }

        /// <summary>
        ///     Raised before damage is about to be dealt.
        /// </summary>
        public event TypedEventHandler<HealthBehavior, HealthChangingEventArgs> Damaging;

        /// <summary>
        ///     Raised when any damage is dealt to health.
        /// </summary>
        public event TypedEventHandler<HealthBehavior, HealthChangedEventArgs> Damaged;

        /// <summary>
        ///     Raised when damage is healed.
        /// </summary>
        public event TypedEventHandler<HealthBehavior, HealthChangedEventArgs> Healed;

        /// <summary>
        ///     Raised when health is reset to maximum value.
        /// </summary>
        public event TypedEventHandler<HealthBehavior, HealthChangedEventArgs> HealthReset;

        /// <summary>
        ///		Gets the health.
        /// </summary>
        public CompositeValue Health { get; }

        /// <summary>
        ///     The number Constitution is multiplied by to get the base health.
        /// </summary>
        public CompositeValue HealthMultiplier { get; }

        /// <summary>
        ///     The base value of health.
        /// </summary>
        /// <remarks>
        ///     If the actor has no AttributeComponent, Health is set to this value.
        ///     Otherwise, Health = HealthBase + Constitution * HealthMultiplier.
        /// </remarks>
        public CompositeValue HealthBase { get; }

        /// <summary>
        ///     Gets the amount of health regenerated per second.
        /// </summary>
        public CompositeValue HealthRegenRate { get; }

        public CompositeValue PhysicalResistance { get; }

        public CompositeValue MagicResistance { get; }

        public CompositeValue FireResistance { get; }

        public CompositeValue FrostResistance { get; }

        public CompositeValue ShockResistance { get; }

        /// <summary>
        ///     True when health is zero.
        /// </summary>
        public bool IsDead
        {
            get
            {
                Debug.Assert(Health.Value >= 0);
                return Health.Value == 0;
            }
        }

        public override void Update(TimeSpan elapsed)
        {
            base.Update(elapsed);

            if (!Enabled)
                return;

            if (Health.Damage > 0 && HealthRegenRate.Value > 0 && !IsDead)
                Health.Damage -= HealthRegenRate.Value * (float) elapsed.TotalSeconds;
        }

        public override void WriteDelta(DeltaWriter writer, ReplicationContext context)
        {
            base.WriteDelta(writer, context);

            Health.WriteDelta(writer, context);
            HealthBase.WriteDelta(writer, context);
            HealthRegenRate.WriteDelta(writer, context);
            HealthMultiplier.WriteDelta(writer, context);

            PhysicalResistance.WriteDelta(writer, context);
            MagicResistance.WriteDelta(writer, context);
            FireResistance.WriteDelta(writer, context);
            FrostResistance.WriteDelta(writer, context);
            ShockResistance.WriteDelta(writer, context);
        }

        public override void ReadDelta(DeltaReader reader, ReplicationContext context)
        {
            base.ReadDelta(reader, context);

            Health.ReadDelta(reader, context);
            HealthBase.ReadDelta(reader, context);
            HealthRegenRate.ReadDelta(reader, context);
            HealthMultiplier.ReadDelta(reader, context);

            PhysicalResistance.ReadDelta(reader, context);
            MagicResistance.ReadDelta(reader, context);
            FireResistance.ReadDelta(reader, context);
            FrostResistance.ReadDelta(reader, context);
            ShockResistance.ReadDelta(reader, context);
        }

        /// <summary>
        ///     Damage health by the specified amount.
        /// </summary>
        /// <param name="amount"> Amount of damage to deal; must be at least zero. </param>
        /// <param name="instigator"> The actor that instigated the damage. </param>
        /// <param name="damageType"> Type of damage dealt. </param>
        public void Damage(float amount, Actor instigator = null, DamageType damageType = null)
        {
            if (amount < 0)
                throw new ArgumentOutOfRangeException(nameof(amount), "amount must be >= 0");

            if (IsDead)
                return;

            if (damageType == null)
                damageType = Directory.Find<DamageType>(DamageTypeNames.Raw);

            amount = ApplyResistance(amount, damageType);

            // Invoke damaging event
            amount = OnDamaging(instigator, amount, damageType);

            // Damage to health should not exceed the amount necessary to reduce it to zero.
            Health.Damage = Math.Min(Health.Damage + amount, Health.UndamagedValue);

            OnDamaged(instigator, amount, damageType, IsDead);
        }

        /// <summary>
        ///     Heal health by the specified amount.
        /// </summary>
        /// <param name="instigator"> The actor that instigated the healing. </param>
        /// <param name="amount"> Amount of health to heal, must be at least zero. </param>
        public void Heal(int amount, Actor instigator = null)
        {
            if (amount < 0)
                throw new ArgumentOutOfRangeException(nameof(amount), "amount must be >= 0");

            // Health needs to be reset to clear the death flag
            if (IsDead)
                return;

            Health.Damage -= amount;

            OnHealed(instigator, amount);
        }

        /// <summary>
        ///     Reset health to full; can revive the dead.
        /// </summary>
        public void Reset(Actor instigator = null)
        {
            if (Health.Damage == 0)
                return;

            float healed = Health.Damage;
            Health.Damage = 0;
            Debug.Assert(!IsDead);

            OnHealthReset(instigator, healed);
        }

        public CompositeValue GetResistance(DamageType type)
        {
            switch (type.Name)
            {
                case DamageTypeNames.Raw:
                    return null;
                case DamageTypeNames.Physical:
                    return PhysicalResistance;
                case DamageTypeNames.Magic:
                    return MagicResistance;
                case DamageTypeNames.Fire:
                    return FireResistance;
                case DamageTypeNames.Frost:
                    return FrostResistance;
                case DamageTypeNames.Shock:
                    return ShockResistance;
                default:
                    throw new ArgumentOutOfRangeException(nameof(type));
            }
        }

        protected float ApplyResistance(float damage, DamageType damageType)
        {
            CompositeValue cv = GetResistance(damageType);
            if (cv == null)
                return damage;
            return damage * (1.0f - cv.Value);
        }

        protected virtual float OnDamaging(Actor instigator, float amount, DamageType damageType)
        {
            if (Damaging == null)
                return amount;

            var e = new HealthChangingEventArgs(instigator, -amount, damageType);
            Damaging(this, e);
            if (e.Cancel)
                return amount;

            return -e.Amount;
        }

        protected virtual void OnDamaged(Actor instigator, float amount, DamageType damageType, bool isDead)
        {
            Damaged?.Invoke(this, new HealthChangedEventArgs(instigator, -amount, damageType, IsDead));
        }

        protected virtual void OnHealed(Actor instigator, float amount)
        {
            Healed?.Invoke(this, new HealthChangedEventArgs(instigator, amount, null, false));
        }

        protected virtual void OnHealthReset(Actor instigator, float healed)
        {
            HealthReset?.Invoke(this, new HealthChangedEventArgs(instigator, healed, null, false));
        }
    }

    public class HealthChangingEventArgs : CancelEventArgs
    {
        public HealthChangingEventArgs(Actor instigator, float amount, DamageType type)
        {
            Instigator = instigator;
            Amount = amount;
            DamageType = type;
        }

        public Actor Instigator { get; private set; }

        public float Amount { get; set; }

        public DamageType DamageType { get; private set; }
    }

    /// <summary>
    ///     Signifies a health changed raised by a HealthBehavior.
    /// </summary>
    /// <remarks>
    ///     A positive amount signifies healing, while a negative amount signifies damage. Amount will never be zero.
    /// </remarks>
    public class HealthChangedEventArgs : EventArgs
    {
        public HealthChangedEventArgs(Actor instigator, float amount, DamageType type, bool killed)
        {
            Instigator = instigator;
            Amount = amount;
            DamageType = type;
            Killed = killed;
        }

        public Actor Instigator { get; private set; }

        public float Amount { get; private set; }

        public DamageType DamageType { get; private set; }

        public bool Killed { get; private set; }
    }
}