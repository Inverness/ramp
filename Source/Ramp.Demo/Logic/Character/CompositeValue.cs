using System;
using System.Diagnostics;
using Newtonsoft.Json;
using Ramp.Network;

namespace Ramp.Logic.Character
{
    /// <summary>
    ///     A numerical composite value used to describe an actor.
    /// </summary>
    [DebuggerDisplay("{Value}, Base = {Base}, Modifier = {Modifier}, Damage = {Damage}")]
    public class CompositeValue : IReplicatable
    {
        private float _minimum;
        private float _maximum;
        private float _base;
        private float _modifier;
        private float _damage;

        public CompositeValue(float baseValue = 0, float minimum = float.MinValue, float maximum = float.MaxValue)
        {
            Base = baseValue;
            Minimum = minimum;
            Maximum = maximum;
        }

        /// <summary>
        ///     Raised when any values change.
        /// </summary>
        public event TypedEventHandler<CompositeValue, CompositeValueChangedEventArgs> ValueChanged;

        /// <summary>
        ///     Gets or sets the minimum allowed attribute value. The Value and UndamagedValue are clamped to this.
        /// </summary>
        public float Minimum
        {
            get { return _minimum; }

            set { UpdateField(ref _minimum, value, true); }
        }

        /// <summary>
        ///     Gets or sets the maximum allowed attribute value. The Value and UndamagedValue are clamped to this.
        /// </summary>
        public float Maximum
        {
            get { return _maximum; }

            set { UpdateField(ref _maximum, value, true); }
        }

        /// <summary>
        ///     Gets or sets the base value to which modifier and damage are applied to before clamping.
        /// </summary>
        public float Base
        {
            get { return _base; }

            set { UpdateField(ref _base, value, true); }
        }

        /// <summary>
        ///     Gets or sets the attribute modifier. Temporary modifications to an attribute should be applied to this value
        ///     to be incorporated into the total value.
        /// </summary>
        public float Modifier
        {
            get { return _modifier; }

            set { UpdateField(ref _modifier, value, true); }
        }

        /// <summary>
        ///     Gets or sets the damage to the attribute. The total value will be decreased by this amount. Clamped to non-negative number.
        /// </summary>
        public float Damage
        {
            get { return _damage; }

            set { UpdateField(ref _damage, value, false); }
        }

        /// <summary>
        ///     Gets total attribute value. This is updated whenever minimum, maximum, base, or modifier values are changed.
        /// </summary>
        [JsonIgnore]
        public float Value => Math.Min(Math.Max(_base + _modifier - _damage, _minimum), _maximum);

        /// <summary>
        ///     Gets total attribute value excluding the application of damage. Clamped to Minimum and Maximum.
        /// </summary>
        [JsonIgnore]
        public float UndamagedValue => Math.Min(_maximum, Math.Max(_minimum, _base + _modifier));

        public void WriteDelta(DeltaWriter writer, ReplicationContext context)
        {
            writer.Write(_minimum);
            writer.Write(_maximum);
            writer.Write(_base);
            writer.Write(_modifier);
            writer.Write(_damage);
        }

        public void ReadDelta(DeltaReader reader, ReplicationContext context)
        {
            reader.Read(ref _minimum);
            reader.Read(ref _maximum);
            reader.Read(ref _base);
            reader.Read(ref _modifier);
            reader.Read(ref _damage);
        }

        // Handles field updating and its side effects
        private void UpdateField(ref float fieldValue, float newFieldValue, bool negativeAllowed)
        {
            if (!negativeAllowed && newFieldValue < 0)
                newFieldValue = 0;

            // ReSharper disable once CompareOfFloatsByEqualityOperator
            if (fieldValue == newFieldValue)
                return;

            float oldValue = Value;
            fieldValue = newFieldValue;

            ValueChanged?.Invoke(this, new CompositeValueChangedEventArgs(oldValue));
        }
    }
}