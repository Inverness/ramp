﻿namespace Ramp.Logic.Character
{
    public static class StandardDamageTypeName
    {
        public static readonly string Physical = "Physical";

        public static readonly string Magic = "Magic";

        public static readonly string Fire = "Fire";

        public static readonly string Frost = "Frost";

        public static readonly string Shock = "Shock";

        public static readonly string Poison = "Poison";
    }
}