using JetBrains.Annotations;
using Newtonsoft.Json;
using Ramp.Data;
using Ramp.Naming;

namespace Ramp.Logic.Character
{
    /// <summary>
    ///     Defines a type of damage that can be dealt to an object.
    /// </summary>
    public class DamageType : Decl
    {
        public DamageType(string name, NamedObject outer)
            : base(name, outer)
        {
        }

        [JsonProperty]
        public string FullName { get; [UsedImplicitly] private set; }
    }
}