namespace Ramp.Logic.Character
{
    /// <summary>
    ///     The names for the damage type decls.
    /// </summary>
    public static class DamageTypeNames
    {
        public const string Raw = "Raw";
        public const string Physical = "Physical";
        public const string Magic = "Magic";
        public const string Fire = "Fire";
        public const string Frost = "Frost";
        public const string Shock = "Shock";
    }
}