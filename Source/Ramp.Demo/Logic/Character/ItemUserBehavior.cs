using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Coroutines;
using NLog;
using Microsoft.Xna.Framework;
using Ramp.Data;
using Ramp.Rendering;
using Ramp.Simulation;
using static Coroutines.StandardActions;

namespace Ramp.Logic.Character
{
    /// <summary>
    ///     A component that allows a character to use items it has equipped.
    /// </summary>
    public class ItemUserBehavior : Behavior
    {
        /// <summary>
        ///     The animation texture replacement index used for the current weapon texture.
        /// </summary>
        public const byte WeaponAliasIndex = 5;

        public const string UsingItemState = "UsingItem";

        private const float SwordRange = 2;
        private static readonly Logger s_log = LogManager.GetCurrentClassLogger();
        private static readonly Vector2 s_playerCenterOffset = new Vector2(1, 1);

        private readonly StateComponent _state;
        private readonly AnimationRenderer _animation;
        private readonly CharacterBehavior _character;
        private readonly EquipmentBehavior _equipment;
        private readonly ProjectileSpawnerBehavior _projectileSpawner;
        private Item _currentItem;

        public ItemUserBehavior()
        {
            DamageStrengthMultiplier = 2;
            DamageStrengthBase = 5;
            SwordAttackAnimation = new AssetReference<Animation>("Animations/Char/Sword");
            BowAttackAnimation = new AssetReference<Animation>();

            GetComponentRequired(out _state);
            GetComponentRequired(out _animation);
            GetComponentRequired(out _character);
            GetComponentRequired(out _equipment);
            GetComponentRequired(out _projectileSpawner);

            _animation.FrameChanged += OnAnimationFrameChanged;
        }

        public int DamageDexterityMultiplier { get; set; }

        public int DamageStrengthMultiplier { get; set; }

        public int DamageDexterityBase { get; set; }

        /// <summary>
        ///     Strength required for normal damage. A penalty or bonus is applied to damage based on whether
        ///     strength is above or below this.
        /// </summary>
        public int DamageStrengthBase { get; set; }

        /// <summary>
        ///     Body animation for weapon attack.
        /// </summary>
        public AssetReference<Animation> SwordAttackAnimation { get; }

        public AssetReference<Animation> BowAttackAnimation { get; }

        /// <summary>
        ///     True if the current animation state is WeaponAttack.
        /// </summary>
        public bool IsUsingItem => _state.Current == UsingItemState;

        public void Activate(EquipmentSlot slot)
        {
            Actor.StartCoroutine(ActivateCor(slot));
        }

        public IEnumerable ActivateCor(EquipmentSlot slot)
        {
            Item item = _equipment.GetItem(slot);
            if (item == null)
                yield break;

            var weapon = item.ItemType as WeaponItemType;
            if (weapon != null)
                yield return ActivateWeaponCor(item, weapon, slot);

            var spellGem = item.ItemType as SpellGemItemType;
            if (spellGem != null)
                yield return ActivateSpellGemCor(item, spellGem, slot);
        } 

        private IEnumerable ActivateSpellGemCor(Item item, SpellGemItemType spellGem, EquipmentSlot slot)
        {
            if (Actor.ClientSide)
            {
                s_log.Warn("Attempt to call WeaponUserBehavior.ActivateSpellGem() on client.");
                yield break;
            }

            if (IsUsingItem)
                yield break;

            EffectCastingType? castType = spellGem.GetCastingType();
            if (!castType.HasValue)
                yield break;

            Debug.Assert(castType.Value == EffectCastingType.Single);

            using (_state.Push(UsingItemState))
            {
                throw new NotImplementedException();
            }

            //return true;
        }

        private IEnumerable ActivateWeaponCor(Item item, WeaponItemType weapon, EquipmentSlot slot)
        {
            if (Actor.ClientSide)
            {
                s_log.Warn("Attempt to call WeaponUserBehavior.ActivateWeapon() on client.");
                yield break;
            }

            // No re-entry
            if (IsUsingItem)
                yield break;

            using (_state.Push(UsingItemState, true))
            {
                // Only thing supported at the moment
                if (weapon.WeaponType == WeaponType.Sword)
                {
                    // play animation
                    string animation = GetAttackAnimation(weapon);
                    _animation.SetTextureAlias(WeaponAliasIndex, weapon.Texture);

                    if (_animation.Play(animation, _character.LastMoveDirection))
                    {
                        _currentItem = item;

                        yield return _animation.WaitForFrameEventCor("WeaponImpact");

                        if (CoroutineThread.Current.GetResultOrDefault<bool>())
                        {
                            ApplyWeaponDamage(weapon);

                            yield return _animation.WaitFinishedCor();
                        }
                    }
                    else
                    {
                        s_log.Error("Failed to play weapon animation: " + animation);
                    }
                }
                else if (weapon.WeaponType == WeaponType.Bow)
                {
                    Vector2 launchPos = _animation.Transform.Location +
                                        MovementUtility.GetVector(_character.LastMoveDirection);
                    _projectileSpawner.SpawnProjectile(launchPos,
                                                       "Arrow",
                                                       _character.LastMoveDirection,
                                                       6,
                                                       18,
                                                       "Textures/TestSprites",
                                                       true,
                                                       Actor);
                }
                else
                {
                    throw new NotImplementedException();
                }
            }
        }

        private void OnAnimationFrameChanged(AnimationRenderer sender, AnimationFrameChangedEventArgs e)
        {
            //if (_state.Current != UsingItemState)
            //    return;

            //WeaponItemType weapon;
            //if (e.ChangeType >= AnimationFrameChangeType.Finished && _currentItem != null)
            //{
            //    _currentItem = null;
            //    _state.Set(CharacterBehavior.IdleState);
            //}
            //else if (e.ChangeType < AnimationFrameChangeType.Finished &&
            //         sender.CurrentFrame.Events.Contains("WeaponImpact") &&
            //         (weapon = _currentItem?.ItemType as WeaponItemType) != null)
            //{
            //    ApplyWeaponDamage(weapon);
            //}
        }

        private void ApplyWeaponDamage(WeaponItemType weapon)
        {
            switch (weapon.WeaponType)
            {
                case WeaponType.Sword:
                {
                    // Calculate the strength bonus. DamageStrengthBase is a baseline where you receive a damage bonus if
                    // your strength is above it, or a penalty if your strength is below it. It's possible for strength to
                    // be low enough to reduce damage to zero.
                    float strBonus = (_character.Strength.Value - DamageStrengthBase) * DamageStrengthMultiplier;

                    float damage = Math.Max(weapon.Damage + strBonus, 0);

                    // A weapon can have multiple bounding spheres to indicate hit location.
                    foreach (Actor actor in Level.GetActorsInBounds(GetWeaponDamageBounds(weapon)))
                    {
                        if (actor == Actor)
                            continue;
                        actor.GetComponent<HealthBehavior>()?.Damage(damage, Actor, weapon.DamageType.LoadTarget());
                    }
                }
                    break;

                case WeaponType.Bow:
                {
                    float dexBonus = (_character.Dexterity.Value - DamageDexterityBase) * DamageDexterityMultiplier;
                    float damage = Math.Max(weapon.Damage + dexBonus, 0);
                }
                    break;
            }
        }

        private IEnumerable<BoundingSphere> GetWeaponDamageBounds(WeaponItemType item)
        {
            Vector2 mod = MovementUtility.GetVector(_character.LastMoveDirection);
            Vector2 center = _animation.Transform.Location + s_playerCenterOffset;

            switch (item.WeaponType)
            {
                case WeaponType.Sword:
                    Vector2 target = new Vector2(SwordRange) * mod + center;
                    var targetBounds = new BoundingSphere(new Vector3(target, 0), 1.25f);
                    return new[] { targetBounds };

                default:
                    throw new ArgumentException("unhandled weapon type");
            }
        }

        private string GetAttackAnimation(WeaponItemType item)
        {
            switch (item.WeaponType)
            {
                case WeaponType.Sword:
                case WeaponType.Dagger:
                case WeaponType.Axe:
                    return SwordAttackAnimation.Name;
                case WeaponType.Bow:
                    return BowAttackAnimation.Name;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        //private void OnAnimationFrameEvent(AnimationRenderer sender, AnimationEventEventArgs e)
        //{
        //    if (e.Event == "WeaponImpact" && _state.CurrentState == UsingItemState)
        //    {

        //    }
        //}

        //private EffectCastingType? GetCastingType(SpellGem spellGem)
        //{
        //    var effectTypeNames = spellGem.Effects.Select(e => e.EffectType);

        //    EffectCastingType? type = null;

        //    foreach (string t in effectTypeNames)
        //    {
        //        var decl = ObjectManager.FindObject<EffectType>(World.DeclProvider.DeclPackage, t);
        //        if (decl == null)
        //        {
        //            Log.WarnFormat("Invalid effect decl {0} for {1}", t, spellGem);
        //            continue;
        //        }

        //        if (type.HasValue && decl.CastingType != type.Value)
        //        {
        //            Log.ErrorFormat("Mismatched casting types for {0}", spellGem);
        //            return null;
        //        }

        //        type = decl.CastingType;
        //    }

        //    return type;
        //}
    }
}