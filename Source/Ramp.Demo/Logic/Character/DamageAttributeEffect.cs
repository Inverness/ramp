using System;
using Ramp.Naming;
using Ramp.Simulation;

namespace Ramp.Logic.Character
{
    /// <summary>
    ///     An effect that damages one of the target's attributes.
    /// </summary>
    public class DamageAttributeEffect : Effect
    {
        private CharacterBehavior _character;


        public DamageAttributeEffect(EffectParameters p, Actor target, Actor instigator)
            : base(p, target, instigator)
        {
        }

        public DamageAttributeEffectType EffectType => (DamageAttributeEffectType) Parameters.EffectType.LoadTarget();

        protected override void OnBegin(TimeSpan time)
        {
            _character = Target.GetComponent<CharacterBehavior>();
            if (NamedObject.IsNullOrDisposed(_character))
                return;

            _character.GetAttribute(EffectType.Target).Damage += (int) Math.Max(Parameters.Magnitude, 0);
        }

        protected override void OnEnd(TimeSpan time)
        {
            if (NamedObject.IsNullOrDisposed(_character))
                return;

            _character.GetAttribute(EffectType.Target).Damage -= (int) Math.Max(Parameters.Magnitude, 0);
        }
    }
}