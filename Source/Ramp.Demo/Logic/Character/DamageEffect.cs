﻿using System;
using Ramp.Simulation;

namespace Ramp.Logic.Character
{
    /// <summary>
    ///     An effect that deals damage to the target
    /// </summary>
    public class DamageEffect : Effect
    {
        private HealthBehavior _health;

        public DamageEffect(EffectParameters p, Actor target, Actor instigator)
            : base(p, target, instigator)
        {
        }

        public DamageEffectType EffectType => (DamageEffectType) Parameters.EffectType.LoadTarget();

        protected override void OnBegin(TimeSpan time)
        {
            if (Target != null)
                _health = Target.GetComponent<HealthBehavior>();
            ApplyDamage();
        }

        protected override void OnEnd(TimeSpan time)
        {
            _health = null;
        }

        protected override void OnUpdate(TimeSpan elapsed)
        {
            ApplyDamage();
        }

        private void ApplyDamage()
        {
            _health?.Damage((float) Parameters.Magnitude, Instigator, EffectType.DamageType.LoadTarget());
        }
    }
}