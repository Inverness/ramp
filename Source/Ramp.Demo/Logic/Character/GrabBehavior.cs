﻿using System;
using System.ComponentModel;
using Ramp.Simulation;

namespace Ramp.Logic.Character
{
    /// <summary>
    ///     Allows an actor to grab or be grabbed by another.
    /// </summary>
    public class GrabBehavior : Behavior
    {
        public event TypedEventHandler<GrabBehavior, GrabEventArgs> Grabbed;

        //public event TypedEventHandler<GrabBehavior, GrabEventArgs> GrabBegan;

        //public event TypedEventHandler<GrabBehavior, GrabEventArgs> GrabEnded;

        //public GrabBehavior Instigator { get; private set; }
        //public GrabBehavior Other { get; private set; }
        /// <summary>
        ///     The object that instigated the grab.
        /// </summary>
        /// <summary>
        ///     The other object involved in a grab.
        /// </summary>
        /// <summary>
        ///     Grab an actor.
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public virtual bool Grab(Actor target)
        {
            var targetGrabBehavior = target.GetComponent<GrabBehavior>();
            if (targetGrabBehavior == null)
                return false;

            //if (targetGrabBehavior.Other != null)
            //{
            //	if (targetGrabBehavior.Other == this)
            //		return true;
            //	if (!targetGrabBehavior.EndGrab(this))
            //		return false;
            //}

            //if (!targetGrabBehavior.BeginGrap(this))
            //	return false;

            //Other = targetGrabBehavior;
            //Instigator = this;

            targetGrabBehavior.OnGrabbed(this);

            return true;
        }

        //protected virtual bool BeginGrap(GrabBehavior instigator)
        //{
        //	if (GrabbingBy != null)
        //	{
        //		var e = new GrabCancelEventArgs(instigator, instigator);
        //		GrabbingBy(this, e);
        //		if (e.Cancel)
        //			return false;
        //	}
        //	Other = instigator;
        //	Instigator = instigator;
        //	if (GrabbedBy != null)
        //	{

        //	}
        //	return true;
        //}

        //protected virtual bool EndGrab(GrabBehavior instigator)
        //{
        //	Other = null;
        //	Instigator = null;
        //	return true;
        //}

        protected virtual void OnGrabbed(GrabBehavior instigator)
        {
            Grabbed?.Invoke(this, new GrabEventArgs(instigator, instigator));
        }
    }

    public class GrabEventArgs : EventArgs
    {
        public GrabEventArgs(GrabBehavior instigator, GrabBehavior other)
        {
            Instigator = instigator;
            Other = other;
        }

        public GrabBehavior Instigator { get; private set; }

        public GrabBehavior Other { get; private set; }
    }

    public class GrabCancelEventArgs : CancelEventArgs
    {
        public GrabCancelEventArgs(GrabBehavior instigator, GrabBehavior other)
        {
            Instigator = instigator;
            Other = other;
        }

        public GrabBehavior Instigator { get; private set; }

        public GrabBehavior Other { get; private set; }
    }
}