using System;

namespace Ramp.Logic.Character
{
    /// <summary>
    ///     Event arguments provided when a attribute has changed value.
    /// </summary>
    public class CompositeValueChangedEventArgs : EventArgs
    {
        public CompositeValueChangedEventArgs(float oldValue = 0)
        {
            OldValue = oldValue;
        }

        public float OldValue { get; private set; }
    }
}