using Newtonsoft.Json;
using Ramp.Naming;
using Ramp.Simulation;

namespace Ramp.Logic.Character
{
    /// <summary>
    ///     An effect that does a type of damage to the target.
    /// </summary>
    public class DamageEffectType : EffectType
    {
        public DamageEffectType(string name, NamedObject outer)
            : base(name, outer)
        {
        }

        [JsonProperty]
        public NamedReference<DamageType> DamageType { get; private set; }

        public override Effect CreateEffect(EffectParameters p, Actor target, Actor instigator)
        {
            return new DamageEffect(p, target, instigator);
        }
    }
}