﻿using Newtonsoft.Json;
using Ramp.Naming;
using Ramp.Simulation;

namespace Ramp.Logic.Character
{
    /// <summary>
    ///     An effect that damages one of the target's attributes.
    /// </summary>
    public class DamageAttributeEffectType : EffectType
    {
        public DamageAttributeEffectType(string name, NamedObject outer)
            : base(name, outer)
        {
        }

        /// <summary>
        ///     The targetted attribute.
        /// </summary>
        [JsonProperty]
        public CharacterAttribute Target { get; private set; }

        public override Effect CreateEffect(EffectParameters p, Actor target, Actor instigator)
        {
            return new DamageAttributeEffect(p, target, instigator);
        }
    }
}