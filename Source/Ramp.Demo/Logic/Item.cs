using System;
using System.Diagnostics;

namespace Ramp.Logic
{
    /// <summary>
    ///     An instance of an ItemType within a collection.
    /// </summary>
    public sealed class Item
    {
        private uint _count;

        /// <summary>
        ///     Initialize an item of the specified type.
        /// </summary>
        /// <param name="type"> The type of the item. </param>
        /// <exception cref="ArgumentNullException"> type is null. </exception>
        public Item(ItemType type)
        {
            Validate.ArgumentNotNull(type, "type");

            ItemType = type;
        }

        /// <summary>
        ///     The collection that contains this item.
        /// </summary>
        public ItemCollectionComponent Collection { get; internal set; }

        /// <summary>
        ///     The type of this item.
        /// </summary>
        public ItemType ItemType { get; private set; }

        /// <summary>
        ///     The quanitty of this item, never 0.
        /// </summary>
        public uint Count
        {
            get
            {
                Debug.Assert(_count != 0, "_count != 0");
                return _count;
            }

            set
            {
                Validate.ArgumentInRange(value > 0, "value");
                Validate.ArgumentInRange(value == 1 || ItemType.Stackable, "value", "not a stackable type");
                _count = value;
            }
        }

        /// <summary>
        ///		Check if this item is stackable with another, but does not check if they're in the same collection.
        /// </summary>
        /// <param name="other"> The other item. </param>
        /// <returns> True if stacking is possible </returns>
        public bool CanStackWith(Item other)
        {
            if (other == null)
                throw new ArgumentNullException(nameof(other));

            return ItemType == other.ItemType;
        }

        internal Item CloneWithoutCollection()
        {
            return new Item(ItemType) { Count = _count };
        }
    }
}