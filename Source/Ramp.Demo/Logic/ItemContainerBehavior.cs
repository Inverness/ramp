﻿using Ramp.Session;
using Ramp.Simulation;

namespace Ramp.Logic
{
    /// <summary>
    ///     Handles interaction with a physical container of items backed by an item collection.
    /// </summary>
    public class ItemContainerBehavior : Behavior, IActivatable
    {
        private readonly ItemCollectionComponent _items;

        public ItemContainerBehavior()
        {
            GetComponentRequired(out _items);
        }

        public void Activate(Actor instigator)
        {
            if (!Enabled || instigator == null)
                return;
            var player = PlayerPawnBehavior.GetTopPlayer(instigator) as DemoLocalPlayer;
            player?.ShowContainerWindow(Actor);
        }
    }
}