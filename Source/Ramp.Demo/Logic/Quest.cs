using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json.Linq;
using Ramp.Simulation;

namespace Ramp.Logic
{
    public sealed class Quest
    {
        private const string CurrentStagePlayerKey = "Stage";

        private readonly Dictionary<string, QuestStage> _stages = new Dictionary<string, QuestStage>();
        private readonly Dictionary<string, object> _properties = new Dictionary<string, object>();

        // Load a quest from a validated JSON object
        internal Quest(QuestManager manager, JObject root)
        {
            Manager = manager;
            Id = root.Value<string>("ID");
            IconName = root.Value<string>("IconName");
            Name = root.Value<string>("Name");

            foreach (KeyValuePair<string, JToken> pair in (JObject) root["Stages"])
                _stages[pair.Key] = new QuestStage(this, pair.Key, (JObject) pair.Value);

            var properties = root["Properties"] as JObject;
            if (properties != null)
            {
                foreach (KeyValuePair<string, JToken> pair in properties)
                {
                    _properties[pair.Key] = ((JValue) pair.Value).Value;
                }
            }
        }

        public bool Enabled { get; internal set; }

        public string Id { get; private set; }

        public string IconName { get; private set; }

        public QuestManager Manager { get; private set; }

        public string Name { get; private set; }

        public IEnumerable<QuestStage> Stages => _stages.Values;

        public QuestStage GetStage(string id)
        {
            QuestStage stage;
            _stages.TryGetValue(id, out stage);
            return stage;
        }

        public bool SetCurrentStage(IPlayer player, string id)
        {
            if (player == null)
                throw new ArgumentNullException(nameof(player));
            if (id == null)
                throw new ArgumentNullException(nameof(id));

            QuestStage stage;
            if (!_stages.TryGetValue(id, out stage))
                return false;

            SetPlayerValue(player, CurrentStagePlayerKey, id);

            Manager.OnQuestStageChanged(this, player, stage);

            return true;
        }

        public QuestStage GetCurrentStage(IPlayer player)
        {
            if (player == null)
                throw new ArgumentNullException(nameof(player));

            var id = GetPlayerValue<string>(player, CurrentStagePlayerKey);

            return id == null ? null : GetStage(id);
        }

        public T GetPlayerValue<T>(IPlayer player, string name, T defaultValue = default(T))
        {
            return player.Data.GetValue(GetPlayerValueName(name), defaultValue);
        }

        public void SetPlayerValue<T>(IPlayer player, string name, T value)
        {
            player.Data[GetPlayerValueName(name)] = value;
        }

        private string GetPlayerValueName(string name)
        {
            StringBuilder sb = StringBuilderCache.Acquire(7 + Id.Length + name.Length);
            sb.Append("Quest.").Append(Id).Append(".").Append(name);
            return StringBuilderCache.ReleaseAndGetString(sb);
        }
    }

    //public enum QuestStartupType
    //{
    //	None,
    //}

    //public static class QuestStage
    //{
    //	public const ushort None = 0;
    //	public const ushort Start = 1;
    //	public const ushort Finished = 10000;
    //}

    //public class Quest
    //{
    //	private const string StageValueName = "Stage";

    //	private readonly HashSet<ushort> _stages = new HashSet<ushort>();
    //	private readonly string _id;
    //	private readonly string _name;
    //	private readonly QuestManager _manager;
    //	private bool _enabled;
    //	private QuestStartupType _startupType;
    //	private IConfigService _configService;
    //	private readonly string _configSectionName;

    //	public Quest(QuestManager manager, string id, string name)
    //	{
    //		if (manager == null)
    //			throw new ArgumentNullException("manager");
    //		if (String.IsNullOrEmpty(id))
    //			throw new ArgumentException("id is null or empty", "id");
    //		_manager = manager;
    //		_id = id;
    //		_name = name;
    //		_configSectionName = "Quest." + id;
    //	}

    //	public QuestManager Manager
    //	{
    //		get { return _manager; }
    //	}

    //	public string ID
    //	{
    //		get { return _id; }
    //	}

    //	public string Name
    //	{
    //		get { return _name; }
    //	}

    //	public bool Enabled
    //	{
    //		get { return _enabled; }

    //		set
    //		{
    //			if (_enabled == value)
    //				return;
    //			_enabled = value;
    //			OnEnabledChanged();
    //		}
    //	}

    //	public QuestStartupType StartupType
    //	{
    //		get { return _startupType; }

    //		set { _startupType = value; }
    //	}

    //	public ICollection<ushort> Stages
    //	{
    //		get { return _stages; }
    //	}

    //	private IConfigService ConfigService
    //	{
    //		get
    //		{
    //			if (_configService == null)
    //				_configService = Manager.ServiceProvider.GetService<IConfigService>(true);
    //			return _configService;
    //		}
    //	}

    //	public ushort GetStage(Player player)
    //	{
    //		try
    //		{
    //			return GetPlayerValue<ushort>(player, StageValueName);
    //		}
    //		catch (KeyNotFoundException)
    //		{
    //			return 0;
    //		}
    //	}

    //	public bool SetStage(Player player, ushort id)
    //	{
    //		if (!_stages.Contains(id))
    //			return false;

    //		SetPlayerValue(player, StageValueName, id);
    //		OnStageChanged(player, id);

    //		return true;
    //	}

    //	public virtual void Initialize()
    //	{
    //		AssertEnabled();
    //	}

    //	public virtual void OnPlayerLogin(Player player)
    //	{
    //		AssertEnabled();
    //	}

    //	public virtual void OnPlayerLogout(Player player)
    //	{
    //		AssertEnabled();
    //	}

    //	public virtual void Update(GameTime gameTime)
    //	{
    //		AssertEnabled();
    //	}

    //	protected void AssertEnabled()
    //	{
    //		if (!_enabled)
    //			throw new InvalidOperationException("quest is not enabled");
    //	}

    //	protected T GetConfigValue<T>(string name)
    //	{
    //		return ConfigService.GetValue<T>(_configSectionName, name);
    //	}

    //	protected T GetConfigValue<T>(string name, T default_)
    //	{
    //		return ConfigService.GetValue(_configSectionName, name, default_);
    //	}

    //	protected IEnumerable<T> GetConfigValues<T>(string name)
    //	{
    //		return ConfigService.GetValues<T>(_configSectionName, name);
    //	}

    //	protected T GetPlayerValue<T>(Player player, string name)
    //	{
    //		return (T)player.Data[GetPlayerValueName(name)];
    //	}

    //	protected void SetPlayerValue<T>(Player player, string name, T value)
    //	{
    //		player.Data[GetPlayerValueName(name)] = value;
    //	}

    //	protected virtual void OnEnabledChanged()
    //	{
    //		_manager.OnQuestEnabledChanged(this);
    //	}

    //	protected virtual void OnStageChanged(Player player, ushort stage)
    //	{
    //		_manager.OnQuestStageChanged(this, player, stage);
    //	}

    //	private string GetPlayerValueName(string name)
    //	{
    //		var sb = new StringBuilder();
    //		sb.Append("Quest.").Append(_id).Append(".").Append(name);
    //		return sb.ToString();
    //	}
    //}
}