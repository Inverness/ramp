﻿namespace Ramp.Logic
{
    /// <summary>
    ///		Ranks used to classify characters, attributes, and items.
    /// </summary>
    public enum Rank : byte
    {
        None,
        E,
        D,
        C,
        B,
        A,
        Ap,
        App,
        Appp,
        S
    }
}