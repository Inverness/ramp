﻿using System;
using Microsoft.Xna.Framework;
using Ramp.Rendering;
using Ramp.Simulation;

namespace Ramp.Logic
{
    /// <summary>
    ///     A chest that can store an item and give them to the opener.
    /// </summary>
    public class ChestBehavior : Behavior, IActivatable
    {
        private static readonly Rectangle s_closedPart = new Rectangle(0, 0, 32, 32);
        private static readonly Rectangle s_openPart = new Rectangle(32, 0, 32, 32);

        //private SignalComponent _signal;
        private readonly SpriteRenderer _spriteRenderer;

        public ChestBehavior()
        {
            GetComponentRequired(out _spriteRenderer);
            UpdateTexture();
        }

        public bool IsOpen { get; private set; }

        public uint ItemCount { get; set; }

        public string ItemType { get; set; }

        public string Texture { get; set; }

        public void Open(Actor instigator)
        {
            if (instigator == null)
                throw new ArgumentNullException(nameof(instigator));

            if (IsOpen)
                return;

            var items = instigator.GetComponent<ItemCollectionComponent>();
            if (items == null)
                throw new InvalidOperationException("instigator has no ItemCollectionComponent");

            items.Add(ItemType, ItemCount);

            IsOpen = true;
            UpdateTexture();
        }

        void IActivatable.Activate(Actor instigator)
        {
            Open(instigator);
        }

        //private void OnSignalGrab(SignalComponent sender, string name, object o)
        //{
        //    Open(sender.Actor);
        //}

        private void UpdateTexture()
        {
            _spriteRenderer.Texture = Texture;
            _spriteRenderer.TexturePart = IsOpen ? s_openPart : s_closedPart;
        }
    }
}