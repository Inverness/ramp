﻿using System;

namespace Ramp.Logic
{
    /// <summary>
    ///		Provides methods for handling ranks,.,
    /// </summary>
    public static class RankUtility
    {
        public static string GetDisplayString(Rank rank)
        {
            switch (rank)
            {
                case Rank.None:
                    return "--";
                case Rank.E:
                case Rank.D:
                case Rank.C:
                case Rank.B:
                case Rank.A:
                case Rank.S:
                    return rank.ToString();
                case Rank.Ap:
                    return "A+";
                case Rank.App:
                    return "A++";
                case Rank.Appp:
                    return "A+++";
                default:
                    throw new ArgumentOutOfRangeException(nameof(rank));
            }
        }

        public static Rank ParseDisplayString(string s)
        {
            if (string.IsNullOrEmpty(s))
                throw new ArgumentException("Required", nameof(s));
            if (s == "--")
                return Rank.None;
            if (s == "A+")
                return Rank.Ap;
            if (s == "A++")
                return Rank.App;
            if (s == "A+++")
                return Rank.Appp;
            return (Rank) Enum.Parse(typeof(Rank), s, false);
        }

        public static Rank GetRank(float value, float scale)
        {
            var r = (int) Math.Ceiling(value / scale);
            if (r >= (int) Rank.S)
                return Rank.S;
            if (r <= (int) Rank.None)
                return Rank.None;
            return (Rank) r;
        }
    }
}