using NLog;
using Ramp.Logic.Signaling;
using Ramp.Simulation;

namespace Ramp.Logic.Dungeon
{
    /// <summary>
    ///     Activates objects in a room when activated or when an event occurs.
    /// </summary>
    public class DungeonActivatorBehavior : DungeonObjectBehavior
    {
        private static readonly Logger s_log = LogManager.GetCurrentClassLogger();

        private bool _activated;
        private readonly Collider _collider;

        public DungeonActivatorBehavior()
        {
            GetComponentRequired(out _collider);

            _collider.ActorOverlapBegan += OnActorOverlapBegan;
            _collider.ActorOverlapEnded += OnActorOverlapEnded;
            _collider.ActorBlocked += OnActorBlocked;

            BlockSignal = Actor.GetSpawnArgument<MulticastSignal>("BlockSignal");
            BeginOverlapSignal = Actor.GetSpawnArgument<MulticastSignal>("BeginOverlapSignal");
            EndOverlapSignal = Actor.GetSpawnArgument<MulticastSignal>("EndOverlapSignal");
        }

        /// <summary>
        ///     The current collider.
        /// </summary>
        public Collider Collider => _collider;

        /// <summary>
        ///     Activate when blocked if a collider is available.
        /// </summary>
        public MulticastSignal BlockSignal { get; set; }

        public MulticastSignal BeginOverlapSignal { get; set; }

        public MulticastSignal EndOverlapSignal { get; set; }

        //[JsonProperty]
        //public bool ActivateOnPlayerEnters { get; set; }

        //[JsonProperty]
        //public bool ActivateOnPlayerLeaves { get; set; }

        protected override void ReceiveSignal(Signal signal, Actor instigator, SignalHandlerBehavior sender)
        {
            //if (!Enabled || PawnBehavior.GetTopPlayer(instigator) != null)
            //    return;

            //foreach (DungeonObjectBehavior ro in GroupObjects)
            //{
            //    ro.Activate(instigator);
            //}
        }

        private void OnActorBlocked(Collider sender, ActorCollisionEventArgs e)
        {
            if (BlockSignal.HasItems)
                SendSignal(BlockSignal, instigator: e.Other.Actor);
        }

        private void OnActorOverlapBegan(Collider sender, ActorCollisionEventArgs e)
        {
            s_log.Debug("OnOverlapBegan: Sender = {0}, Instigator = {1}", sender, e.Other);
            //UpdateOverlapping(e.Instigator != null ? e.Instigator.Actor : null);
            if (BeginOverlapSignal.HasItems && _collider.OverlappingColliders.Count != 0 && !_activated)
            {
                SendSignal(BeginOverlapSignal, instigator: e.Other?.Actor);
                _activated = true;
            }
        }

        private void OnActorOverlapEnded(Collider sender, ActorCollisionEventArgs e)
        {
            s_log.Debug("OnOverlapEnded: Sender = {0}, Instigator = {1}", sender, e.Other);
            if (EndOverlapSignal.HasItems && _collider.OverlappingColliders.Count == 0 && _activated)
            {
                SendSignal(EndOverlapSignal, instigator: e.Other?.Actor);
                _activated = false;
            }
            //UpdateOverlapping(e.Instigator != null ? e.Instigator.Actor : null);
        }

        //private void UpdateOverlapping(Actor instigator)
        //{
        //    if (!ActivateOnOverlap)
        //        return;

        //    if (Collider.OverlappingColliders.Any() && !_activated)
        //    {
        //        Activate(instigator);
        //        _activated = true;
        //        Log.DebugFormat("Activated {0}", this);
        //    }
        //    else if (!Collider.OverlappingColliders.Any() && _activated)
        //    {
        //        Activate(instigator);
        //        _activated = false;
        //        Log.DebugFormat("Deactivated {0}", this);
        //    }
        //}
    }
}