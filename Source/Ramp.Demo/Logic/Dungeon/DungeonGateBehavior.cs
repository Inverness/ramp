using System;
using Ramp.Logic.Signaling;
using Ramp.Rendering;
using Ramp.Simulation;

namespace Ramp.Logic.Dungeon
{
    /// <summary>
    ///     Logic for a dungeon gate that opens or closes when activated.
    /// </summary>
    public class DungeonGateBehavior : DungeonObjectBehavior
    {
        public const string ClosedState = "Closed";
        public const string ClosingState = "Closing";
        public const string OpenState = "Open";
        public const string OpeningState = "Opening";

        private readonly StateComponent _state;
        private readonly Collider _collider;
        private readonly AnimationRenderer _animation;
        private string _openAnimation;
        private string _closeAnimation;

        public DungeonGateBehavior()
        {
            GetComponentRequired(out _state);
            GetComponentRequired(out _collider);
            GetComponentRequired(out _animation);
            _animation.FrameChanged += OnAnimationFrameChanged;

            OpenSignal = Actor.GetSpawnArgument<MulticastSignal>("OpenSignal");
            CloseSignal = Actor.GetSpawnArgument<MulticastSignal>("CloseSignal");
            OpenAnimation = Actor.GetSpawnArgument<string>("OpenAnimation");
            CloseAnimation = Actor.GetSpawnArgument<string>("CloseAnimation");

            _animation.Play(_closeAnimation);
            _state.Set(ClosingState);
        }

        public MulticastSignal OpenSignal { get; set; }

        public MulticastSignal CloseSignal { get; set; }

        /// <summary>
        ///     The animation to play when the gate is opening.
        /// </summary>
        public string OpenAnimation
        {
            get { return _openAnimation; }

            set
            {
                if (_openAnimation == value)
                    return;
                _openAnimation = value;
                if (_state.Current == OpenState)
                    _animation.Play(_openAnimation);
            }
        }

        /// <summary>
        ///     The animation to play when the gate is closing.
        /// </summary>
        public string CloseAnimation
        {
            get { return _closeAnimation; }

            set
            {
                if (_closeAnimation == value)
                    return;
                _closeAnimation = value;
                if (_state.Current == ClosedState)
                    _animation.Play(_closeAnimation);
            }
        }

        protected override void ReceiveSignal(Signal signal, Actor instigator, SignalHandlerBehavior sender)
        {
            if (CanReceive(OpenSignal, signal))
            {
                if (_state.Current != OpenState && _state.Current != OpeningState)
                {
                    _animation.Play(_openAnimation);
                    _state.Set(OpeningState);
                }
            }
            else if (CanReceive(CloseSignal, signal))
            {
                if (_state.Current != ClosedState && _state.Current != ClosingState)
                {
                    _animation.Play(_closeAnimation);
                    _state.Set(ClosingState);
                }
            }
        }

        //public override void Activate(Actor instigator)
        //{
        //    switch (_state.CurrentState)
        //    {
        //        case ClosedState:
        //        case ClosingState:
        //            _animation.Play(_openAnimation);
        //            _state.Set(OpeningState);
        //            break;
        //        case OpenState:
        //        case OpeningState:
        //            _animation.Play(_closeAnimation);
        //            _state.Set(ClosingState);
        //            break;
        //    }
        //}

        private void OnAnimationFrameChanged(AnimationRenderer sender, AnimationFrameChangedEventArgs e)
        {
            if (e.ChangeType < AnimationFrameChangeType.Finished)
                return;

            switch (_state.Current)
            {
                case ClosingState:
                    _state.Set(ClosedState);
                    _collider.PawnResponse = CollisionResponse.Block;
                    break;
                case OpeningState:
                    _state.Set(OpenState);
                    _collider.PawnResponse = CollisionResponse.Ignore;
                    break;
            }
        }
    }
}