using Ramp.Logic.Signaling;

namespace Ramp.Logic.Dungeon
{
    /// <summary>
    ///     An object in a dungeon room that can be grouped and activated.
    /// </summary>
    /// <remarks>
    ///     This base class is used to set up simple logic between groups of objects. Subclasses specify conditions
    ///     for how Activated() will be used and its result on other room objects.
    ///     Actors with this component will have the DungeonObject tag added to them.
    /// </remarks>
    public abstract class DungeonObjectBehavior : SignalHandlerBehavior
    {
        /// <summary>
        ///     A tag added to actors that have instances of this class.
        /// </summary>
        public const string Tag = "DungeonObject";

        protected DungeonObjectBehavior()
        {
            Actor.AddTag(Tag);
        }
    }
}