using System;
using Microsoft.Xna.Framework;
using Ramp.Simulation;

namespace Ramp.Logic.Dungeon
{
    public enum MovementType
    {
        Free,
        Grid,
        Single
    }

    /// <summary>
    ///     Behavior for an pushable block that triggers an event when it reaches its destination.
    /// </summary>
    public class DungeonBlockBehavior : DungeonObjectBehavior
    {
        private readonly Collider _collider;
        //private bool _moving;

        public DungeonBlockBehavior()
        {
            GetComponentRequired(out _collider);

            _collider.ActorBlocked += OnActorBlocked;

            Origin = _collider.Transform.Location;
            MovementType = Actor.GetSpawnArgument<MovementType>("BlockMovementType");
        }

        /// <summary>
        ///     Gets the block's movement type. Only settable from spawn arguments.
        /// </summary>
        public MovementType MovementType { get; private set; }

        /// <summary>
        ///     The movement direction the block is allowed if is not free.
        /// </summary>
        public Direction MovementDirection { get; set; }

        /// <summary>
        ///     The position of the block when it was spawned.
        /// </summary>
        public Vector2 Origin { get; private set; }

        /// <summary>
        ///     The block's movement speed when not free.
        /// </summary>
        public float Speed { get; set; }

        private static Direction Reverse(Direction direction)
        {
            switch (direction)
            {
                case Direction.Up:
                    return Direction.Down;
                case Direction.Left:
                    return Direction.Right;
                case Direction.Down:
                    return Direction.Up;
                case Direction.Right:
                    return Direction.Left;
                default:
                    throw new ArgumentOutOfRangeException(nameof(direction));
            }
        }

        private void OnActorBlocked(Collider sender, ActorCollisionEventArgs e)
        {
            //if (_moving)
            //    return;

            //switch (MovementType)
            //{
            //    case MovementType.Free:
            //        // Move the block by the same delta that the instigator moved.
            //        // Only cancel the instigator's block event if the block is still able to be pushed, otherwise the instigator
            //        // will begin intersecting with the block
            //        _moving = true;
            //        e.Cancel = Level.MoveActor(Actor, e.Delta);
            //        _moving = false;
            //        break;
            //    case MovementType.Grid:
            //        throw new NotImplementedException();
            //    case MovementType.Single:
            //        throw new NotImplementedException();
            //}
        }
    }
}