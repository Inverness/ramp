using Ramp.Logic.Signaling;
using Ramp.Simulation;

namespace Ramp.Logic.Dungeon
{
    /// <summary>
    ///     When activated, enables or disables all behaviors in the current actor, or the current group.
    /// </summary>
    public class DungeonEnablerBehavior : DungeonObjectBehavior
    {
        private bool _enabledOthers;

        public DungeonEnablerBehavior()
        {
            EnabledOthers = Actor.GetSpawnArgument<bool>("EnabledOthers");
            ToggleSignal = Actor.GetSpawnArgument<MulticastSignal>("ToggleSignal");
            EnableSignal = Actor.GetSpawnArgument<MulticastSignal>("EnableSignal");
            DisableSignal = Actor.GetSpawnArgument<MulticastSignal>("DisableSignal");
        }

        /// <summary>
        ///     True if other behaviors in the current actor or group are currently enable. Set this to update
        ///     the states of other behaviors.
        /// </summary>
        public bool EnabledOthers
        {
            get { return _enabledOthers; }

            set
            {
                _enabledOthers = value;
                UpdateActorBehaviors(Actor);
            }
        }

        public MulticastSignal ToggleSignal { get; set; }

        public MulticastSignal EnableSignal { get; set; }

        public MulticastSignal DisableSignal { get; set; }

        protected override void ReceiveSignal(Signal signal, Actor instigator, SignalHandlerBehavior sender)
        {
            if (CanReceive(ToggleSignal, signal))
                EnabledOthers = !EnabledOthers;
            else if (CanReceive(EnableSignal, signal))
                EnabledOthers = true;
            else if (CanReceive(DisableSignal, signal))
                EnabledOthers = false;
        }

        protected void UpdateActorBehaviors(Actor actor)
        {
            foreach (ActorComponent component in actor.Components)
            {
                if (component == this)
                    continue;
                var behavior = component as Behavior;
                if (behavior == null)
                    continue;
                behavior.Enabled = _enabledOthers;
            }
        }
    }
}