using System;
using Ramp.Rendering;
using Ramp.Simulation;

namespace Ramp.Logic
{
    /// <summary>
    ///     Used to make lights flicker by changing the scale randomly at the specified rate.
    /// </summary>
    public class LightFlickerBehavior : Behavior
    {
        private static readonly Random s_random = new Random();

        private readonly SpriteRenderer _sprite;

        private double _elapsed;

        public LightFlickerBehavior()
        {
            Actor.IsActive = true;
            GetComponent(out _sprite);

            Rate = Actor.GetSpawnArgument<float>("FlickerRate");
            MinimumScale = Actor.GetSpawnArgument<float>("FlickerMinimum");
            MaximumScale = Actor.GetSpawnArgument<float>("FlickerMaximum");
        }

        public float MinimumScale { get; set; }

        public float MaximumScale { get; set; }

        public float Rate { get; set; }

        public override void Update(TimeSpan elapsed)
        {
            base.Update(elapsed);

            if (!Enabled || _sprite == null)
                return;

            _elapsed += elapsed.TotalSeconds;

            if (_elapsed < Rate || Rate <= 0)
                return;

            _elapsed = 0;

            float scale = MinimumScale + ((MaximumScale - MinimumScale) * (float) s_random.NextDouble());
            _sprite.Scale = scale;
        }
    }
}