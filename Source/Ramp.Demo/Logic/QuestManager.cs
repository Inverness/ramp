using System;
using System.Collections.Generic;
using NLog;
using Microsoft.Xna.Framework;
using Ramp.Data;
using Ramp.Session;
using Ramp.Simulation;

namespace Ramp.Logic
{
    public class QuestManager : GameComponent, IQuestService
    {
        private static readonly Logger s_log = LogManager.GetCurrentClassLogger();
        private readonly Dictionary<string, Quest> _quests = new Dictionary<string, Quest>();
        private readonly IConfigService _configService;
        private readonly IFileService _fileService;
        private readonly ISessionService _sessionService;

        public QuestManager(Game game)
            : base(game)
        {
            _configService = game.Services.GetService<IConfigService>();
            _fileService = game.Services.GetService<IFileService>();
            _sessionService = game.Services.GetService<ISessionService>();

            if (_sessionService != null)
            {
                _sessionService.PlayerLogin += OnPlayerLogin;
                _sessionService.PlayerLogout += OnPlayerLogout;
            }

            game.Services.AddService(typeof(IQuestService), this);
        }

        public event TypedEventHandler<IQuestService, QuestStageChangedEventArgs> QuestStageChanged;

        public IReadOnlyDictionary<string, Quest> Quests => _quests;

        public override void Initialize()
        {
            base.Initialize();

            if (_configService == null)
                return;

            //Log.DebugFormat("Loading quests from configuration:");

            //// pairs of questID : qualified type name
            //foreach (KeyValuePair<string, string> pair in _configService.GetSection<string>("Quests"))
            //{
            //    Log.DebugFormat("  " + pair.Key);
            //    if (_quests.ContainsKey(pair.Key))
            //        Log.ErrorFormat("Quest with specified key already exists: " + pair.Key);
            //    Type questType = Type.GetType(pair.Value, true);
            //    if (!questType.IsSubclassOf(typeof(Quest)))
            //        throw new InvalidDataException("specified quest type does not subclass Quest");
            //    var quest = (Quest) Activator.CreateInstance(questType, this, pair.Key);
            //    _quests[pair.Key] = quest;
            //}
        }

        public bool SetCurrentStage(string quest, IPlayer player, string stage)
        {
            if (quest == null)
                throw new ArgumentNullException(nameof(quest));
            if (player == null)
                throw new ArgumentNullException(nameof(player));
            if (stage == null)
                throw new ArgumentNullException(nameof(stage));

            Quest q = _quests[quest];
            if (!_quests.TryGetValue(quest, out q))
                return false;

            return q.SetCurrentStage(player, stage);
        }

        public QuestStage GetCurrentStage(string quest, IPlayer player)
        {
            if (quest == null)
                throw new ArgumentNullException(nameof(quest));
            if (player == null)
                throw new ArgumentNullException(nameof(player));

            Quest q = _quests[quest];
            if (!_quests.TryGetValue(quest, out q))
                return null;

            return q.GetCurrentStage(player);
        }

        public override void Update(GameTime gameTime)
        {
            foreach (Quest quest in _quests.Values)
            {
                //if (quest.Enabled)
                //	quest.Update(gameTime);
            }
        }

        internal void OnQuestEnabledChanged(Quest quest)
        {
        }

        internal void OnQuestStageChanged(Quest quest, IPlayer player, QuestStage stage)
        {
            QuestStageChanged?.Invoke(this, new QuestStageChangedEventArgs(quest, player, stage));
        }

        protected virtual void OnPlayerLogin(ISessionService sender, PlayerEventArgs e)
        {
            foreach (Quest quest in _quests.Values)
            {
                //if (quest.Enabled)
                //	quest.OnPlayerLogin(e.Player);
            }
        }

        protected virtual void OnPlayerLogout(ISessionService sender, PlayerEventArgs e)
        {
            foreach (Quest quest in _quests.Values)
            {
                //if (quest.Enabled)
                //	quest.OnPlayerLogout(e.Player);
            }
        }
    }

    public class QuestEventArgs : EventArgs
    {
        public QuestEventArgs(Quest quest)
        {
            Quest = quest;
        }

        public Quest Quest { get; private set; }
    }
}