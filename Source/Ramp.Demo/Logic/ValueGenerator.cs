﻿namespace Ramp.Logic
{
    public abstract class ValueGenerator
    {
        public abstract double Next(double time);

        public float NextSingle(double time)
        {
            return (float) Next(time);
        }
    }
}