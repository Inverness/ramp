﻿using System.Text.RegularExpressions;
using Microsoft.Xna.Framework;
using Newtonsoft.Json;
using Ramp.Data;
using Ramp.Naming;
using Ramp.Utilities.Text;

namespace Ramp.Logic
{
    public enum RegionType
    {
        Exterior,
        Interior
    }

    /// <summary>
    ///     A logical zone in the game world consisting of levels.
    /// </summary>
    public class Region : Decl
    {
        private string _matchString;
        private string _matchStringRegex;

        public Region(string name, NamedObject outer)
            : base(name, outer)
        {
        }

        /// <summary>
        ///     The descriptive name of the zone.
        /// </summary>
        [JsonProperty]
        public string FullName { get; private set; }

        /// <summary>
        ///     The level name or prefix to match.
        /// </summary>
        [JsonProperty]
        public string MatchString
        {
            get { return _matchString; }

            set
            {
                _matchString = value;
                _matchStringRegex = value != null ? WildcardPattern.ToRegex(value) : null;
            }
        }

        [JsonProperty]
        public RegionType RegionType { get; private set; }

        /// <summary>
        ///     The color filter to apply the player's screen while in the zone.
        /// </summary>
        [JsonProperty]
        public Color? ScreenColor { get; private set; }

        public bool Contains(string levelName)
        {
            return Regex.IsMatch(levelName, _matchStringRegex);
        }
    }
}