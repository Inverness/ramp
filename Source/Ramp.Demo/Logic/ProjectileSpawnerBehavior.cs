﻿using System;
using NLog;
using Microsoft.Xna.Framework;
using Ramp.Simulation;
using Ramp.Simulation.Tiling;
using Ramp.Utilities.Collections;

namespace Ramp.Logic
{
    public class ProjectileSpawnerBehavior : Behavior, IActivatable
    {
        private static readonly Logger s_log = LogManager.GetCurrentClassLogger();

        private readonly NameValueDictionary _spawnArgs = new NameValueDictionary();

        public ProjectileSpawnerBehavior()
        {
            Archetype = Actor.GetSpawnArgument<string>("ProjectileArchetype");
            Direction = Actor.GetSpawnArgument<Direction>("ProjectileDirection");
            Layer = Actor.GetSpawnArgument<int>("ProjectileLayer");
            Speed = Actor.GetSpawnArgument<float>("ProjectileSpeed");
            TextureName = Actor.GetSpawnArgument<string>("ProjectileTexture");
        }

        public TypedEventHandler<ProjectileSpawnerBehavior, ProjectileImpactEventArgs> Impacted;

        public string Archetype { get; set; }

        public Direction Direction { get; set; }

        public int Layer { get; set; }

        public float Speed { get; set; }

        public string TextureName { get; set; }

        public bool UseDirectionalArchetypes { get; set; }

        public void Activate(Actor instigator)
        {
        }

        public Actor SpawnProjectile(Vector2 position, string archetype, Direction direction,
                                     int? layer = null, float? speed = null, string texture = null,
                                     bool useDirectionalArchetypes = false, Actor instigator = null)
        {
            //const float speed = 18;
            ////const float damage = 4;
            //const int layer = 6;
            //const string texture = "T_TestSprites";
            ////const int arrowTypeOffset = 32;

            // SPRITES:
            // Arrow:
            //   Right: 0, 402, 30, 9
            //   Left; 0, 412, 30, 9
            //   Up; 11, 422, 9, 30
            //   Down: 1, 422, 9, 30


            Vector2 offset;
            switch (direction)
            {
                case Direction.Up:
                    offset = new Vector2(0, -2);
                    break;
                case Direction.Left:
                    offset = new Vector2(-2, 0);
                    break;
                case Direction.Down:
                    offset = new Vector2(0, 1);
                    break;
                case Direction.Right:
                    offset = new Vector2(1, 0);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(direction));
            }

            if (useDirectionalArchetypes)
                archetype += direction;

            _spawnArgs["Direction"] = direction;
            _spawnArgs["Speed"] = speed ?? 0;
            _spawnArgs["Layer"] = layer ?? 0;
            _spawnArgs["Texture"] = texture;

            Actor projectile = Level.SpawnActor(position + offset, archetype, Actor, args: _spawnArgs);

            var collider = projectile.GetComponent<Collider>();
            if (collider != null)
            {
                collider.ActorOverlapBegan += OnProjectileActorOverlapBegan;
                collider.WorldBlocked += OnProjectileWorldBlocked;
            }
            else
            {
                s_log.Warn("Spawned projectile did not have a collider");
            }

            return projectile;
        }

        protected virtual void OnImpacted(Actor projectile, Actor instigator)
        {
            Impacted?.Invoke(this, new ProjectileImpactEventArgs(projectile, instigator));
        }

        protected virtual void OnProjectileWorldBlocked(Collider sender, TileCollisionEventArgs e)
        {
            sender.WorldBlocked -= OnProjectileWorldBlocked;
            OnImpacted(sender.Actor, null);
            sender.Actor.MarkForDispose();
        }

        protected virtual void OnProjectileActorOverlapBegan(Collider sender, ActorCollisionEventArgs e)
        {
            if (e.Other.Actor == Actor)
                return;
            sender.ActorOverlapBegan -= OnProjectileActorOverlapBegan;
            OnImpacted(sender.Actor, null);
            sender.Actor.MarkForDispose();
        }
    }

    public class ProjectileImpactEventArgs : EventArgs
    {
        public ProjectileImpactEventArgs(Actor projectile, Actor instigator)
        {
            Projectile = projectile;
            Instigator = instigator;
        }

        public Actor Projectile { get; private set; }

        public Actor Instigator { get; private set; }
    }
}