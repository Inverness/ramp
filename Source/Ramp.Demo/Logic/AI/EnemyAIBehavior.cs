﻿using System;
using System.Collections;
using Microsoft.Xna.Framework;
using Ramp.Simulation;

namespace Ramp.Logic.AI
{
    [ServerComponent]
    public class EnemyAIBehavior : CharacterAIBehavior
    {
        protected override void BeginNextTask()
        {
            AICoroutineExecutor.Start(Main());
        }

        protected IEnumerable Main()
        {
            while (true)
            {
                if (TargetActor == null)
                {
                    yield return FindTarget(false, true, 32, TimeSpan.FromSeconds(1));
                }
                else if (TargetTransform != null)
                {
                    // Get within reach of the target.
                    if (Vector2.Distance(TargetTransform.Location, Transform.Location) > Reach)
                    {
                        yield return MoveToTargetActor(TargetActor);
                    }
                }

                yield return null;
            }
            // ReSharper disable once FunctionNeverReturns
        }
    }
}