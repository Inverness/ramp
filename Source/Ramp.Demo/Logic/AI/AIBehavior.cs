﻿using System;
using Coroutines;
using Ramp.Session;
using Ramp.Simulation;

namespace Ramp.Logic.AI
{
    public abstract class AIBehavior : Behavior
    {
        private DemoGameMaster _gameMaster;
        private readonly CoroutineExecutor _ce = new CoroutineExecutor();

        protected AIBehavior()
        {
            Actor.IsActive = true;
        }

        public DemoGameMaster GameMaster => _gameMaster ?? (_gameMaster = (DemoGameMaster) World.GameMaster);

        public CoroutineExecutor AICoroutineExecutor => _ce;

        public override void Update(TimeSpan elapsed)
        {
            base.Update(elapsed);

            if (!Enabled)
                return;

            if (!_ce.Tick(elapsed))
                BeginNextTask();
        }

        protected virtual void BeginNextTask()
        {
        }
    }
}