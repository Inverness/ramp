﻿using System;
using System.Collections;
using System.Collections.Generic;
using Coroutines;
using Microsoft.Xna.Framework;
using Ramp.Logic.Character;
using Ramp.Simulation;
using static Coroutines.StandardActions;

namespace Ramp.Logic.AI
{
    /// <summary>
    ///     The base class for AI attached to a character.
    /// </summary>
    [ServerComponent]
    public class CharacterAIBehavior : AIBehavior
    {
        private static readonly TimeSpan s_pathfindRate = TimeSpan.FromSeconds(1);

        internal readonly Pathfinder Pathfinder = new Pathfinder();

        private readonly TransformComponent _transform;
        private readonly InputMovementBehavior _movement;
        private readonly CharacterBehavior _character;
        private readonly HealthBehavior _health;

        private float _reach;
        private float _maxPathDistance;
        private HealthBehavior _targetHealth;
        private TransformComponent _targetTransform;

        public CharacterAIBehavior()
        {
            GetComponentRequired(out _transform);
            GetComponentRequired(out _movement);
            GetComponentRequired(out _character);
            GetComponent(out _health);

            Reach = 3f;
            MaxPathDistance = 96;
        }

        public TransformComponent Transform => _transform;

        public InputMovementBehavior Movement => _movement;

        public CharacterBehavior Character => _character;

        public HealthBehavior Health => _health;
        public float Reach
        {
            get { return _reach; }

            set
            {
                if (value < 0)
                    throw new ArgumentOutOfRangeException(nameof(value));
                _reach = value;
            }
        }

        public float MaxPathDistance
        {
            get { return _maxPathDistance; }

            set
            {
                if (value <= 0)
                    throw new ArgumentOutOfRangeException(nameof(value));
                _maxPathDistance = value;
            }
        }

        public bool IsDead => _health != null && _health.IsDead;

        public Vector2? TargetLocation { get; private set; }

        public Actor TargetActor { get; private set; }

        protected TransformComponent TargetTransform => _targetTransform;

        public override void Update(TimeSpan elapsed)
        {
            base.Update(elapsed);

            if (!Enabled || !GameMaster.AIUpdateEnabled)
                return;

            if (TargetActor != null && _targetHealth != null && _targetHealth.Health.Value <= 0)
                SetTargetDead();
        }

        public void SetTargetActor(Actor target)
        {
            if (IsDead || target == null)
            {
                TargetActor = null;
                _targetHealth = null;
                _targetTransform = null;
                return;
            }

            if (TargetActor == target)
                return;

            TargetActor = target;
            _targetHealth = target.GetComponent<HealthBehavior>();
            _targetTransform = target.GetComponent<TransformComponent>();

            if (_targetHealth != null && _targetHealth.Health.Value <= 0)
            {
                SetTargetDead();
                return;
            }

            // incomplete
        }

        public void SetTargetLocation(Vector2? location)
        {
            if (IsDead || location == null)
            {
                TargetLocation = null;
                return;
            }

            TargetLocation = location;
        }

        protected void SetTargetDead()
        {
            SetTargetActor(null);
        }

        protected IEnumerable MoveToTargetLocation(Vector2 location)
        {
            return MoveToTarget(location, null);
        }

        protected IEnumerable MoveToTargetActor(Actor actor)
        {
            return MoveToTarget(Vector2.Zero, actor);
        }

        private IEnumerable MoveToTarget(Vector2 targetLocation, Actor targetActor)
        {
            List<Vector2> steps = new List<Vector2>();
            int currentStepIndex = 0;
            Vector2 lastTargetLocation = Vector2.Zero;
            TimeSpan lastPathfindElapsed = TimeSpan.Zero;

            while (true)
            {
                if (targetActor != null)
                {
                    Vector2? candidateLocation = targetActor.GetLocation();

                    if (!candidateLocation.HasValue || targetActor.Status >= ActorStatus.Disposing)
                    {
                        MarkTargetUnreachable();
                        yield break;
                    }

                    targetLocation = candidateLocation.Value;
                }

                // Find a path if if one isn't already calculated or if the target moved.

                if (steps.Count == 0 || (targetLocation != lastTargetLocation && lastPathfindElapsed >= s_pathfindRate))
                {
                    steps.Clear();
                    currentStepIndex = 0;
                    lastPathfindElapsed = TimeSpan.Zero;

                    Pathfinder.Find(Actor, targetLocation, steps, Reach, MaxPathDistance);

                    if (steps.Count == 0)
                    {
                        MarkTargetUnreachable();
                        yield break;
                    }
                }

                lastPathfindElapsed += CoroutineThread.Current.ElapsedTime;
                lastTargetLocation = targetLocation;

                Vector2 currentStep = steps[currentStepIndex];
                Vector2 location = Transform.Location;

                // TODO: Detect if we overshoot the target due to speed or something.
                while (Vector2.Distance(location, currentStep) <= 0.1f)
                {
                    // Snap actor to step location
                    if (location != currentStep)
                    {
                        Movement.MoveTo(currentStep, 0);
                        location = Transform.Location;
                    }

                    currentStepIndex++;
                    if (currentStepIndex == steps.Count)
                    {
                        Movement.InputVector = Vector2.Zero;
                        yield break;
                    }

                    currentStep = steps[currentStepIndex];
                }

                // If the step becomes unreachable due to actor movement, clear the path and have it redone next loop.
                if (IsUnreachable(currentStep, false, true))
                {
                    steps.Clear();
                }
                else
                {
                    // Specify a direction so input is always at full speed towards the current step.
                    Movement.InputDirection = MovementUtility.GetDirection(currentStep - location);
                }

                yield return null;
            }
        }

        protected IEnumerable<CoroutineAction> FindTarget(bool npcs, bool players, int range, TimeSpan rate)
        {
            while (TargetActor == null)
            {
                foreach (Actor actor in Level.GetActorsInBounds(Transform.Location, range))
                {
                    if (actor == Actor)
                        continue;

                    bool isPlayer = actor.HasTag(PlayerPawnBehavior.PlayerPawnTag);
                    bool gotTarget = (players && isPlayer) || (npcs && !isPlayer);

                    if (gotTarget)
                    {
                        SetTargetActor(actor);
                        yield break;
                    }
                }

                yield return Delay(rate);
            }
        }

        protected void MarkTargetUnreachable()
        {
            SetTargetLocation(null);
            Movement.InputVector = Vector2.Zero;
        }

        protected bool IsUnreachable(Vector2 pos, bool pathCheck, bool actorsOnly)
        {
            if (actorsOnly)
            {
                if (Level.TestOverlap(Actor, pos, false, Reach))
                    return true;
            }
            else if (pathCheck)
            {
                if (Pathfinder.Find(Actor, pos, null, Reach, MaxPathDistance) == 0)
                    return true;
            }
            else
            {
                if (Level.TestOverlap(Actor, pos, true, Reach))
                    return true;
            }

            return false;
        }
    }
}
