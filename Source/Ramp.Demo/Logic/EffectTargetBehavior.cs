using System;
using System.Collections.Generic;
using Ramp.Simulation;

namespace Ramp.Logic
{
    /// <summary>
    ///     Allows effects to be applied to the owning actor.
    /// </summary>
    public class EffectTargetBehavior : Behavior
    {
        private readonly List<Effect> _effects = new List<Effect>();
        private readonly List<Effect> _added = new List<Effect>();
        private readonly List<Effect> _removed = new List<Effect>();
        private TimeSpan _time;

        public EffectTargetBehavior()
        {
            Actor.IsActive = true;
        }

        /// <summary>
        ///     The active effects.
        /// </summary>
        public IReadOnlyList<Effect> Effects => _effects;

        /// <summary>
        ///     Updates the elapsed times for all effects. Adds new effects to the active effects list, and removes
        ///     effects that have run their course.
        /// </summary>
        /// <param name="elapsed"></param>
        public override void Update(TimeSpan elapsed)
        {
            base.Update(elapsed);

            _time += elapsed;

            // ReSharper disable ForCanBeConvertedToForeach
            for (int i = 0; i < _effects.Count; i++)
            {
                if (_effects[i].Update(elapsed))
                    _removed.Add(_effects[i]);
            }

            for (int i = 0; i < _added.Count; i++)
            {
                _effects.Add(_added[i]);
                _added[i].Begin(_time);
            }
            _added.Clear();

            for (int i = 0; i < _removed.Count; i++)
            {
                _removed[i].End(_time);
                _effects.Remove(_removed[i]);
            }
            _removed.Clear();
            // ReSharper restore ForCanBeConvertedToForeach
        }

        /// <summary>
        ///     Add an effect to the current actor.
        /// </summary>
        public Effect AddEffect(EffectParameters p, Actor instigator)
        {
            Validate.ArgumentNotNull(p, "p");

            if (p.Duration < TimeSpan.Zero)
                throw new ArgumentOutOfRangeException(nameof(p), "duration");
            if (p.Area < 0)
                throw new ArgumentOutOfRangeException(nameof(p), "area");

            EffectType typeDecl = p.EffectType.LoadTarget();
            if (typeDecl == null)
                throw new ArgumentException("The effect type was not found: " + p.EffectType, nameof(p));

            Effect effect = typeDecl.CreateEffect((EffectParameters) p.GetAsFrozen(), Actor, instigator);
            _added.Add(effect);
            return effect;
        }

        /// <summary>
        ///     Removes the effect with the specified index.
        /// </summary>
        /// <param name="index"> The effect index. </param>
        public void RemoveEffect(int index)
        {
            Effect effect = _effects[index];
            if (!_removed.Contains(effect))
                _removed.Add(effect);
        }
    }
}