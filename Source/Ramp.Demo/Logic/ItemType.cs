using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Ramp.Data;
using Ramp.Logic.Character;
using Ramp.Naming;

namespace Ramp.Logic
{
    public enum WeaponType
    {
        Sword,
        Dagger,
        Axe,
        Bow
    }

    public enum ArmorType
    {
        Head,
        Body,
        Arms,
        Legs
    }

    /// <summary>
    ///     The abstract base for all item types.
    /// </summary>
    public abstract class ItemType : Decl
    {
        protected ItemType(string name, NamedObject outer, bool stackable)
            : base(name, outer)
        {
            Stackable = stackable;
            Tags = new StringSet();
        }

        /// <summary>
        ///     True if item instances can be stacked. This should only apply to item types that cannot be equipped or
        ///     individually altered.
        /// </summary>
        public bool Stackable { get; set; }

        /// <summary>
        ///     A helpful description of the item.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        ///     The name of the icon texture.
        /// </summary>
        public string Icon { get; set; }

        /// <summary>
        ///     The part of the icon texture.
        /// </summary>
        public Rectangle? IconPart { get; set; }

        /// <summary>
        ///     The full name of the item.
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        ///     The item's tags.
        /// </summary>
        public StringSet Tags { get; private set; }

        /// <summary>
        ///     The weight of the item.
        /// </summary>
        public double Weight { get; set; }

        /// <summary>
        ///     The currency value of the item.
        /// </summary>
        public uint Value { get; set; }
    }

    public sealed class BasicItemType : ItemType
    {
        public BasicItemType(string name, NamedObject outer)
            : base(name, outer, true)
        {
        }
    }

    public sealed class WeaponItemType : ItemType
    {
        public WeaponItemType(string name, NamedObject outer)
            : base(name, outer, false)
        {
            DamageType = new NamedReference<DamageType>(DamageTypeNames.Physical);
        }

        /// <summary>
        ///     The type of the weapon that determines its behavior.
        /// </summary>
        public WeaponType WeaponType { get; set; }

        /// <summary>
        ///		The weapon's rank, represents what skill rank is required to use it along with its power.
        /// </summary>
        public Rank Rank { get; set; }

        /// <summary>
        ///     The weapon's damage on impact.
        /// </summary>
        public ushort Damage { get; set; }

        /// <summary>
        ///     The range of the weapon in tiles.
        /// </summary>
        public ushort Range { get; set; }

        /// <summary>
        ///     The speed of the weapon.
        /// </summary>
        public ushort Speed { get; set; }

        /// <summary>
        ///     The weapon's texture for animations.
        /// </summary>
        public string Texture { get; set; }

        /// <summary>
        ///     Gets or sets the weapon's damage type.
        /// </summary>
        public NamedReference<DamageType> DamageType { get; set; }
    }

    public sealed class ArmorItemType : ItemType
    {
        public ArmorItemType(string name, NamedObject outer)
            : base(name, outer, false)
        {
        }

        /// <summary>
        ///     The amount of armor provided.
        /// </summary>
        public ushort ArmorValue { get; set; }

        /// <summary>
        ///     Gets or sets the type of the armor which determines which slot it occupies.
        /// </summary>
        public ArmorType ArmorType { get; set; }
    }

    public enum SpellGemType
    {
        Spell,
        Ability
    }

    public sealed class SpellGemItemType : ItemType
    {
        public SpellGemItemType(string name, NamedObject outer)
            : base(name, outer, false)
        {
            Effects = new List<EffectParameters>();
        }

        public List<EffectParameters> Effects { get; private set; }

        public SpellGemType GemType { get; private set; }

        public EffectCastingType? GetCastingType()
        {
            EffectCastingType? result = null;

            foreach (EffectParameters p in Effects)
            {
                EffectType et = p.EffectType.LoadTarget();
                if (et == null)
                    continue;
                if (!result.HasValue)
                    result = et.CastingType;
                else if (et.CastingType != result.Value)
                    return null;
            }

            return result;
        }
    }
}