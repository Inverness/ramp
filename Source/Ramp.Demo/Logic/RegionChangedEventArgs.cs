﻿using System;
using Ramp.Simulation;

namespace Ramp.Logic
{
    public class RegionChangedEventArgs : EventArgs
    {
        public RegionChangedEventArgs(IPlayer player, Region oldRegion, Region newRegion)
        {
            Player = player;
            OldRegion = oldRegion;
            NewRegion = newRegion;
        }

        public IPlayer Player { get; private set; }

        public Region OldRegion { get; private set; }

        public Region NewRegion { get; private set; }
    }
}