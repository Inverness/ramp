<?xml version="1.0" encoding="UTF-8"?>
<tileset name="Default" tilewidth="16" tileheight="16">
 <image source="../Textures/Tiles/Test2B.png" width="2048" height="512"/>
 <terraintypes>
  <terrain name="Grass" tile="-1"/>
  <terrain name="Dirt" tile="-1"/>
 </terraintypes>
 <tile id="1288" terrain="1,1,0,0"/>
 <tile id="1289" terrain="0,0,1,1"/>
 <tile id="1290" terrain="1,1,1,1"/>
 <tile id="2176" terrain=",1,,1"/>
 <tile id="2315" terrain=",,1,"/>
 <tile id="2316" terrain=",,,1"/>
 <tile id="2443" terrain="1,,1,1"/>
 <tile id="2444" terrain=",1,1,1"/>
 <tile id="2561" terrain="1,1,,1"/>
 <tile id="2689" terrain=",1,1,1"/>
 <tile id="3983" terrain="0,0,0,0"/>
</tileset>
