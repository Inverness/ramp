<?xml version="1.0" encoding="UTF-8"?>
<tileset name="meta" tilewidth="16" tileheight="16">
 <properties>
  <property name="BlockingRect" value="0,0,3,2"/>
 </properties>
 <image source="../Textures/Tiles/Meta.png" width="256" height="512"/>
 <tile id="0">
  <properties>
   <property name="Blocking" value="true"/>
  </properties>
 </tile>
 <tile id="1">
  <properties>
   <property name="Blocking" value="true"/>
  </properties>
 </tile>
 <tile id="2">
  <properties>
   <property name="Blocking" value="true"/>
  </properties>
 </tile>
 <tile id="3">
  <properties>
   <property name="Blocking" value="true"/>
  </properties>
 </tile>
 <tile id="16">
  <properties>
   <property name="Blocking" value="true"/>
  </properties>
 </tile>
 <tile id="17">
  <properties>
   <property name="Blocking" value="true"/>
  </properties>
 </tile>
 <tile id="18">
  <properties>
   <property name="Blocking" value="true"/>
  </properties>
 </tile>
 <tile id="19">
  <properties>
   <property name="Blocking" value="true"/>
  </properties>
 </tile>
 <tile id="32">
  <properties>
   <property name="Blocking" value="true"/>
  </properties>
 </tile>
 <tile id="33">
  <properties>
   <property name="Blocking" value="true"/>
  </properties>
 </tile>
 <tile id="34">
  <properties>
   <property name="Blocking" value="true"/>
  </properties>
 </tile>
 <tile id="35">
  <properties>
   <property name="Blocking" value="true"/>
  </properties>
 </tile>
 <tile id="48">
  <properties>
   <property name="Blocking" value="true"/>
  </properties>
 </tile>
 <tile id="49">
  <properties>
   <property name="Blocking" value="true"/>
  </properties>
 </tile>
 <tile id="50">
  <properties>
   <property name="Blocking" value="true"/>
  </properties>
 </tile>
 <tile id="51">
  <properties>
   <property name="Blocking" value="true"/>
  </properties>
 </tile>
</tileset>
