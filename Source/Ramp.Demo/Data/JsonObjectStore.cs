﻿using System;
using System.Data;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;

namespace Ramp.Data
{
    /// <summary>
    ///     An interface to a key value store that provides support for JSON object serialization.
    /// </summary>
    public class JsonObjectStore
    {
        public static readonly string VersionKey = "JS_Version";
        public static readonly string BinaryKey = "JS_Binary";
        public static readonly ushort Version = 1;

        private readonly IKeyValueStore _store;
        private readonly bool _binary;
        private readonly JsonSerializer _serializer;

        /// <summary>
        ///		Initialize the store.
        /// </summary>
        /// <param name="store"> The underlying key value store. </param>
        /// <param name="settings"> The JSON serializer settings. If null, uses default settings. </param>
        /// <param name="binary"> True to serialize using the binary BSON format. </param>
        public JsonObjectStore(IKeyValueStore store, JsonSerializerSettings settings = null, bool binary = false)
        {
            if (store == null)
                throw new ArgumentNullException(nameof(store));
            _store = store;
            _binary = binary;
            _serializer = settings == null ? JsonSerializer.CreateDefault() : JsonSerializer.CreateDefault(settings);
            Initialize();
        }

        /// <summary>
        ///		Gets whether BSON is being used to store objects.
        /// </summary>
        public bool Binary => _binary;

        /// <summary>
        ///		Gets the underlying key value store.
        /// </summary>
        public IKeyValueStore Store => _store;

        /// <summary>
        ///		Get an object from the store.
        /// </summary>
        /// <typeparam name="T"> The object type. </typeparam>
        /// <param name="bucket"> The bucket. </param>
        /// <param name="key"> The key. </param>
        /// <param name="serializer"> The serializer to use for deserializing the object. If null, uses the default. </param>
        /// <returns> The deserialized object. </returns>
        public T GetObject<T>(string bucket, string key, JsonSerializer serializer = null)
        {
            serializer = serializer ?? _serializer;

            if (_binary)
            {
                byte[] data = _store.GetByteArray(bucket, key);
                if (data == null || data.Length == 0)
                    return default(T);

                using (var reader = new BsonDataReader(new MemoryStream(data), false, DateTimeKind.Utc))
                    return serializer.Deserialize<T>(reader);
            }
            else
            {
                string data = _store.GetString(bucket, key);
                if (string.IsNullOrEmpty(data))
                    return default(T);

                using (var reader = new JsonTextReader(new StringReader(data)))
                    return serializer.Deserialize<T>(reader);
            }
        }

        /// <summary>
        ///		Set an object in the store.
        /// </summary>
        /// <typeparam name="T"> The object type. </typeparam>
        /// <param name="bucket"> The bucket. </param>
        /// <param name="key"> The key. </param>
        /// <param name="value"> The object. </param>
        /// <param name="serializer"> The serializer to use for serializing the object. If null, uses the default. </param>
        public void SetObject<T>(string bucket, string key, T value, JsonSerializer serializer = null)
        {
            serializer = serializer ?? _serializer;

            if (_binary)
            {
                byte[] data;
                using (var memoryStream = new MemoryStream())
                {
                    using (var writer = new BsonDataWriter(memoryStream))
                    {
                        writer.DateTimeKindHandling = DateTimeKind.Utc;
                        serializer.Serialize(writer, value, typeof(T));
                    }
                    data = memoryStream.ToArray();
                }
                _store.SetByteArray(bucket, key, data);
            }
            else
            {
                string data;
                using (var stringWriter = new StringWriter())
                {
                    using (var writer = new JsonTextWriter(stringWriter))
                    {
                        serializer.Serialize(writer, value, typeof(T));
                    }
                    data = stringWriter.ToString();
                }
                _store.SetString(bucket, key, data);
            }
        }

        private void Initialize()
        {
            string bucket = _store.MetadataBucketName;

            long version = _store.GetInt64(bucket, VersionKey) ?? 0;
            if (version > Version)
                throw new InvalidDataException("Store version is higher than current verison.");

            bool isBinary = _store.GetBoolean(bucket, BinaryKey) ?? _binary;
            if (isBinary != _binary)
                throw new InvalidOperationException("Existing store has different format.");

            // Updates would be performed here.

            using (IDbTransaction t = _store.BeginTransaction())
            {
                _store.SetInt64(bucket, VersionKey, Version);
                _store.SetBoolean(bucket, BinaryKey, _binary);
                t.Commit();
            }
        }
    }
}