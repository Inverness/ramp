﻿using System;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using Microsoft.Xna.Framework;

namespace Ramp.Data
{
    public class AccountManager : GameComponent, IAccountService
    {
        public const string AccountsBucket = "Accounts";
        public const string AdministratorAccountName = "Administrator";
        public const string AdministratorAccountPassword = "default";

        private readonly IServerDatabaseService _db;
        private SHA1 _sha1;

        public AccountManager(Game game)
            : base(game)
        {
            _db = game.Services.GetRequiredService<IServerDatabaseService>();
            game.Services.AddService(typeof(IAccountService), this);
        }

        public override void Initialize()
        {
            _sha1 = SHA1.Create();

            using (IDbTransaction t = _db.Store.BeginTransaction())
            {
                if (!_db.Store.Exists(AccountsBucket, AdministratorAccountName))
                    CreateAccount(AdministratorAccountName, AdministratorAccountPassword);

                Account admin = Authenticate(AdministratorAccountName, AdministratorAccountPassword);
                Debug.Assert(admin != null);

                t.Commit();
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _sha1?.Dispose();
            }
            base.Dispose(disposing);
        }

        public Account Authenticate(string name, string password)
        {
            if (name == null)
                throw new ArgumentNullException(nameof(name));

            Account account = GetAccount(name);
            if (account == null)
                return null;

            // An empty byte array indicates that no password has been set
            if (account.PasswordHash.Length != 0)
            {
                byte[] passwordHash = _sha1.ComputeHash(Encoding.UTF8.GetBytes(password));
                if (!BytesEqual(account.PasswordHash, passwordHash))
                    return null;
            }

            account.LastLogin = DateTime.UtcNow;
            UpdateAccount(account);

            return account;
        }

        public Account CreateAccount(string name, string password)
        {
            if (name == null)
                throw new ArgumentNullException(nameof(name));

            if (_db.Store.Exists(AccountsBucket, name))
                throw new InvalidOperationException("Account already exists.");

            byte[] passwordHash = string.IsNullOrEmpty(password)
                                      ? new byte[0]
                                      : _sha1.ComputeHash(Encoding.UTF8.GetBytes(password));

            var account = new Account
            {
                Name = name,
                Created = DateTime.UtcNow,
                PasswordHash = passwordHash
            };

            _db.ObjectStore.SetObject(AccountsBucket, name, account);

            return account;
        }

        public Account GetAccount(string name)
        {
            if (name == null)
                throw new ArgumentNullException(nameof(name));

            return _db.ObjectStore.GetObject<Account>(AccountsBucket, name);
        }

        public void UpdateAccount(Account account)
        {
            if (account == null)
                throw new ArgumentNullException(nameof(account));

            _db.ObjectStore.SetObject(AccountsBucket, account.Name, account);
        }

        private static bool BytesEqual(byte[] left, byte[] right)
        {
            if (left.Length != right.Length)
                return false;

            for (int i = 0; i < left.Length; i++)
            {
                if (left[i] != right[i])
                    return false;
            }

            return true;
        }
    }
}