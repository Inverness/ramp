﻿using System;
using System.Data.SQLite;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;
using Microsoft.Xna.Framework;
using Newtonsoft.Json;

namespace Ramp.Data
{
    /// <summary>
    ///		Manages the server database connection for interested services.
    /// </summary>
    public class ServerDatabaseManager : GameComponent, IServerDatabaseService
    {
        private readonly IConfigService _config;

        public ServerDatabaseManager(Game game)
            : base(game)
        {
            _config = game.Services.GetRequiredService<IConfigService>();
            game.Services.AddService(typeof(IServerDatabaseService), this);
        }

        public IKeyValueStore Store { get; private set; }

        public JsonObjectStore ObjectStore { get; private set; }

        public override void Initialize()
        {
            var builder = new SQLiteConnectionStringBuilder
            {
                BinaryGUID = true,
                DataSource = Path.Combine(_config.UserDirectory, @"ServerDatabase.sqlite"),
                DateTimeFormat = SQLiteDateFormats.UnixEpoch,
                DateTimeKind = DateTimeKind.Utc,
                FailIfMissing = false,
                ForeignKeys = false,
                JournalMode = SQLiteJournalModeEnum.Truncate,
                LegacyFormat = false,
                UseUTF16Encoding = false,
                Version = 3
            };

            var settings = new JsonSerializerSettings
            {
                ConstructorHandling = ConstructorHandling.Default,
                Context = new StreamingContext(StreamingContextStates.Persistence),
                DateFormatHandling = DateFormatHandling.IsoDateFormat,
                DateParseHandling = DateParseHandling.DateTime,
                DateTimeZoneHandling = DateTimeZoneHandling.Utc,
                DefaultValueHandling = DefaultValueHandling.Include,
                Formatting = Formatting.None,
                MissingMemberHandling = MissingMemberHandling.Ignore,
                NullValueHandling = NullValueHandling.Include,
                ObjectCreationHandling = ObjectCreationHandling.Auto,
                PreserveReferencesHandling = PreserveReferencesHandling.None,
                ReferenceLoopHandling = ReferenceLoopHandling.Error,
                StringEscapeHandling = StringEscapeHandling.Default,
                TypeNameAssemblyFormatHandling = TypeNameAssemblyFormatHandling.Simple,
                TypeNameHandling = TypeNameHandling.Auto
            };

            Store = new SQLiteKeyValueStore(builder.ToString());
            ObjectStore = new JsonObjectStore(Store, settings);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                ((SQLiteKeyValueStore) Store)?.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}