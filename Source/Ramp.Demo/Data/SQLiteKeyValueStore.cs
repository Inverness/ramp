﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Diagnostics;

namespace Ramp.Data
{
    /// <summary>
    ///     An SQLite-backed key value store.
    /// </summary>
    public sealed class SQLiteKeyValueStore : IKeyValueStore, IDisposable
    {
        /// <summary>
        ///		The maximum length of a bucket string.
        /// </summary>
        public const uint MaxBucketLength = 64;

        /// <summary>
        ///		The maximum length of a key string.
        /// </summary>
        public const uint MaxKeyLength = 192;

        public const string DefaultBucketName = "";

        public const string MetadataBucketName = "_M";

        private const long CurrentVersion = 1;

        private readonly SQLiteCommand _get;
        private readonly SQLiteCommand _set;
        private readonly SQLiteCommand _delete;
        private readonly SQLiteCommand _exists;
        private readonly SQLiteCommand _getKeys;

        private static readonly byte[] s_emptyBytes = new byte[0];

        private static readonly Dictionary<Type, StoredValueType> s_valueTypes = new Dictionary<Type, StoredValueType>
        {
            { typeof(bool), StoredValueType.Boolean },
            { typeof(int), StoredValueType.Int64 },
            { typeof(double), StoredValueType.Double },
            { typeof(string), StoredValueType.String },
            { typeof(byte[]), StoredValueType.ByteArray }
        };

        public SQLiteKeyValueStore(string connectionString)
        {
            if (connectionString == null)
                throw new ArgumentNullException(nameof(connectionString));

            // Create/open the database
            Connection = new SQLiteConnection(connectionString);
            Connection.Open();

            // Update the database schema to latest version
            Update();

            // Prepare all needed commands
            _get = new SQLiteCommand("SELECT Type, Value FROM Store WHERE Bucket = ? AND Key = ?", Connection);
            _get.Parameters.Add(null, DbType.String);
            _get.Parameters.Add(null, DbType.String);
            _get.Prepare();

            _set = new SQLiteCommand("INSERT OR REPLACE INTO Store (Bucket, Key, Type, Value) VALUES (?, ?, ?, ?)", Connection);
            _set.Parameters.Add(null, DbType.String);
            _set.Parameters.Add(null, DbType.String);
            _set.Parameters.Add(null, DbType.Byte);
            _set.Parameters.Add(null, DbType.Object);
            _set.Prepare();

            _delete = new SQLiteCommand("DELETE FROM Store WHERE Bucket = ? AND Key = ?", Connection);
            _delete.Parameters.Add(null, DbType.String);
            _delete.Parameters.Add(null, DbType.String);
            _delete.Prepare();

            _exists = new SQLiteCommand("SELECT EXISTS(SELECT 1 FROM Store WHERE Bucket = ? AND Key = ?)", Connection);
            _exists.Parameters.Add(null, DbType.String);
            _exists.Parameters.Add(null, DbType.String);
            _exists.Prepare();

            _getKeys = new SQLiteCommand("SELECT Key FROM Store WHERE Bucket = ?", Connection);
            _getKeys.Parameters.Add(null, DbType.String);
            _getKeys.Prepare();
        }

        /// <summary>
        ///		The SQLite connection.
        /// </summary>
        public SQLiteConnection Connection { get; }

        string IKeyValueStore.DefaultBucketName => DefaultBucketName;

        string IKeyValueStore.MetadataBucketName => MetadataBucketName;

        public void Dispose()
        {
            _getKeys.Dispose();
            _delete.Dispose();
            _set.Dispose();
            _get.Dispose();
            Connection.Dispose();
        }

        public bool? GetBoolean(string bucket, string key)
        {
            return (bool?) GetCore(bucket, key, StoredValueType.Boolean);
        }

        public long? GetInt64(string bucket, string key)
        {
            return (long?) GetCore(bucket, key, StoredValueType.Int64);
        }

        public double? GetDouble(string bucket, string key)
        {
            return (double?) GetCore(bucket, key, StoredValueType.Double);
        }

        public string GetString(string bucket, string key)
        {
            return (string) GetCore(bucket, key, StoredValueType.String);
        }

        public byte[] GetByteArray(string bucket, string key)
        {
            return (byte[]) GetCore(bucket, key, StoredValueType.ByteArray);
        }

        public object Get(string bucket, string key)
        {
            return GetCore(bucket, key, StoredValueType.None);
        }

        public bool SetBoolean(string bucket, string key, bool? value)
        {
            return SetCore(bucket, key, value, StoredValueType.Boolean);
        }

        public bool SetInt64(string bucket, string key, long? value)
        {
            return SetCore(bucket, key, value, StoredValueType.Int64);
        }

        public bool SetDouble(string bucket, string key, double? value)
        {
            return SetCore(bucket, key, value, StoredValueType.Double);
        }

        public bool SetString(string bucket, string key, string value)
        {
            return SetCore(bucket, key, value, StoredValueType.String);
        }

        public bool SetByteArray(string bucket, string key, byte[] value)
        {
            return SetCore(bucket, key, value, StoredValueType.ByteArray);
        }

        public bool Set(string bucket, string key, object value)
        {
            return SetCore(bucket, key, value, StoredValueType.None);
        }

        public bool Exists(string bucket, string key)
        {
            ValidateArgs(bucket, key);

            _exists.Parameters[0].Value = bucket ?? DefaultBucketName;
            _exists.Parameters[1].Value = key;
            return (long) _exists.ExecuteScalar() != 0;
        }

        public IEnumerable<string> GetKeys(string bucket)
        {
            ValidateBucket(bucket);

            _getKeys.Parameters[0].Value = bucket ?? DefaultBucketName;
            using (SQLiteDataReader reader = _getKeys.ExecuteReader())
                while (reader.Read())
                    yield return reader.GetString(0);
        }

        public IDbTransaction BeginTransaction()
        {
            return Connection.BeginTransaction();
        }

        public IDbTransaction BeginTransaction(IsolationLevel isolationLevel)
        {
            return Connection.BeginTransaction(isolationLevel);
        }

        //private IEnumerable<KeyValuePair<string, object>> GetItems(string bucket)
        //{
        //    _getItems.Parameters[0].Value = bucket ?? DefaultBucketName;
        //    using (SQLiteDataReader reader = _getItems.ExecuteReader())
        //    {
        //        while (reader.Read())
        //        {
        //            string key = reader.GetString(0);
        //            object value = reader.GetValue(1);
        //            Debug.Assert(!Convert.IsDBNull(value));
        //            yield return new KeyValuePair<string, object>(key, value);
        //        }
        //    }
        //}

        // Updates the store to the latest schema
        private void Update()
        {
            long version;
            using (SQLiteCommand cmd = Connection.CreateCommand())
            {
                cmd.CommandText = "PRAGMA user_version";
                version = (long) cmd.ExecuteScalar();
            }

            if (version >= CurrentVersion)
                return;

            using (SQLiteTransaction transaction = Connection.BeginTransaction())
            {
                if (version < 1)
                {
                    using (SQLiteCommand cmd = Connection.CreateCommand())
                    {
                        cmd.CommandText =
                            "CREATE TABLE Store (Bucket TEXT NOT NULL, Key TEXT NOT NULL, Type INTEGER NOT NULL, Value BLOB NOT NULL, PRIMARY KEY (Bucket, Key))";
                        cmd.ExecuteNonQuery();
                    }
                }

                using (SQLiteCommand cmd = Connection.CreateCommand())
                {
                    cmd.CommandText = "PRAGMA user_version = " + CurrentVersion;
                    cmd.ExecuteNonQuery();
                }

                transaction.Commit();
            }
        }

        private void ValidateBucket(string bucket)
        {
            if (bucket != null && bucket.Length > MaxBucketLength)
                throw new ArgumentOutOfRangeException(nameof(bucket));
        }

        private void ValidateArgs(string bucket, string key)
        {
            if (bucket != null && bucket.Length > MaxBucketLength)
                throw new ArgumentOutOfRangeException(nameof(bucket));
            if (key == null)
                throw new ArgumentNullException(nameof(key));
            if (key.Length > MaxKeyLength)
                throw new ArgumentOutOfRangeException(nameof(key));
        }

        private object GetCore(string bucket, string key, StoredValueType coerceType)
        {
            ValidateArgs(bucket, key);

            object value;
            StoredValueType valueType;
            return ExecuteGet(bucket, key, coerceType, out value, out valueType) ? value : null;
        }

        private bool SetCore(string bucket, string key, object value, StoredValueType valueType)
        {
            ValidateArgs(bucket, key);

            if (value == null)
                return ExecuteDelete(bucket, key);

            StoredValueType resolvedValueType;
            if (valueType != StoredValueType.None)
            {
                resolvedValueType = valueType;
            }
            else
            {
                if (!s_valueTypes.TryGetValue(value.GetType(), out resolvedValueType))
                    throw new ArgumentOutOfRangeException(nameof(value));
            }

            ExecuteSet(bucket, key, value, resolvedValueType);
            return true;
        }

        private bool ExecuteGet(
            string bucket,
            string key,
            StoredValueType coerceType,
            out object value,
            out StoredValueType valueType)
        {
            _get.Parameters[0].Value = bucket ?? DefaultBucketName;
            _get.Parameters[1].Value = key;

            // ExecuteScalar is not used since the column being BLOB would result in sqlite3_column_blob being called
            // by GetValue().
            // Instead we use the reader directly and request specific types that map to the correct sqlite3_column
            // method. This ensures any required casting operation is performed at the native level.
            using (SQLiteDataReader reader = _get.ExecuteReader(CommandBehavior.SingleRow))
            {
                if (!reader.Read())
                {
                    value = null;
                    valueType = default(StoredValueType);
                    return false;
                }

                valueType = coerceType != StoredValueType.None ? coerceType : (StoredValueType) reader.GetByte(0);
                Debug.Assert(valueType != StoredValueType.None, "valueType != StoredValueType.None");

                switch (valueType)
                {
                    case StoredValueType.Boolean:
                        // sqlite3_column_int64
                        value = reader.GetInt64(1) != 0;
                        break;
                    case StoredValueType.Int64:
                        // sqlite3_column_int64
                        value = reader.GetInt64(1);
                        break;
                    case StoredValueType.Double:
                        // sqlite3_column_double
                        value = reader.GetDouble(1);
                        break;
                    case StoredValueType.String:
                        // sqlite3_column_bytes, sqlite3_column_text
                        value = reader.GetString(1);
                        break;
                    case StoredValueType.ByteArray:
                        // sqlite3_column_bytes, sqlite3_column_blob
                        int n = (int) reader.GetBytes(1, 0, null, 0, 0);
                        if (n != 0)
                        {
                            byte[] b = new byte[n];
                            reader.GetBytes(1, 0, b, 0, n);
                            value = b;
                        }
                        else
                        {
                            value = s_emptyBytes;
                        }
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(valueType));
                }
            }

            return true;
        }

        private void ExecuteSet(string bucket, string key, object value, StoredValueType valueType)
        {
            Debug.Assert(valueType != StoredValueType.None, "valueType != StoredValueType.None");

            _set.Parameters[0].Value = bucket ?? DefaultBucketName;
            _set.Parameters[1].Value = key;
            _set.Parameters[2].Value = (byte) valueType;
            _set.Parameters[3].Value = value;

            _set.ExecuteNonQuery();

            _set.Parameters[3].Value = null; // value could be large, don't prevent a GC
        }

        private bool ExecuteDelete(string bucket, string key)
        {
            _delete.Parameters[0].Value = bucket ?? DefaultBucketName;
            _delete.Parameters[1].Value = key;
            return _delete.ExecuteNonQuery() != 0;
        }

        private enum StoredValueType : byte
        {
            None,
            Boolean,
            Int64,
            Double,
            String,
            ByteArray
        }
    }
}