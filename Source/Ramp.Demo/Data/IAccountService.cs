﻿namespace Ramp.Data
{
    /// <summary>
    ///		Provides management for player accounts.
    /// </summary>
    public interface IAccountService
    {
        /// <summary>
        ///		Authenticate a user and return their account if authenticaiton succeeds.
        /// </summary>
        /// <param name="name"> The account name. </param>
        /// <param name="password"> The password. </param>
        /// <returns> The user's account or null. </returns>
        Account Authenticate(string name, string password);

        /// <summary>
        ///		Create an account with the specified name and password.
        /// </summary>
        /// <param name="name"> The account name. </param>
        /// <param name="password"> The password. </param>
        /// <returns> The new account object. </returns>
        Account CreateAccount(string name, string password);

        /// <summary>
        ///     Get the account with the specified name.
        /// </summary>
        /// <param name="name"> The account name. </param>
        /// <returns> A matching account, or null if one was not found. </returns>
        Account GetAccount(string name);

        /// <summary>
        ///		Update an account's database entry.
        /// </summary>
        /// <param name="account"> The account. </param>
        void UpdateAccount(Account account);
    }
}