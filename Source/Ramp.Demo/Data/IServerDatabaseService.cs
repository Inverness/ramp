﻿namespace Ramp.Data
{
    /// <summary>
    ///		Provides access to the database for the Demo's server.
    /// </summary>
    public interface IServerDatabaseService
    {
        /// <summary>
        ///		A key value store.
        /// </summary>
        IKeyValueStore Store { get; }

        /// <summary>
        ///		A JSON object store.
        /// </summary>
        JsonObjectStore ObjectStore { get; }
    }
}