﻿using System;
using System.Collections.Generic;
using System.Data;

namespace Ramp.Data
{
    /// <summary>
    ///     Represents a key value store with buckets.
    /// </summary>
    public interface IKeyValueStore
    {
        /// <summary>
        ///     Gets the name of the default bucket used if null is specified for a bucket argument.
        /// </summary>
        string DefaultBucketName { get; }

        /// <summary>
        ///		Gets the name of the bucket used for storing metadata.
        /// </summary>
        string MetadataBucketName { get; }

        bool? GetBoolean(string bucket, string key);

        long? GetInt64(string bucket, string key);

        double? GetDouble(string bucket, string key);

        string GetString(string bucket, string key);

        byte[] GetByteArray(string bucket, string key);

        object Get(string bucket, string key);

        bool SetBoolean(string bucket, string key, bool? value);

        bool SetInt64(string bucket, string key, long? value);

        bool SetDouble(string bucket, string key, double? value);

        bool SetString(string bucket, string key, string value);

        bool SetByteArray(string bucket, string key, byte[] value);

        bool Set(string bucket, string key, object value);

        /// <summary>
        ///		Check if an entry exists in the store.
        /// </summary>
        /// <param name="bucket"> The bucket, or null to use the default bucket. </param>
        /// <param name="key"> The key. </param>
        /// <returns> True if the entry exists. </returns>
        /// <exception cref="ArgumentNullException"> Key is null. </exception>
        bool Exists(string bucket, string key);

        /// <summary>
        ///		Get all key names within a bucket.
        /// </summary>
        /// <param name="bucket"> The bucket, or null to use the default bucket. </param>
        /// <returns></returns>
        IEnumerable<string> GetKeys(string bucket);

        /// <summary>
        ///		Begin a transaction with the underlying database.
        /// </summary>
        /// <returns> A database transaction object. </returns>
        IDbTransaction BeginTransaction();

        /// <summary>
        ///		Begin a transaction with the underlying database.
        /// </summary>
        /// <param name="isolationLevel"> The transaction isolation level. </param>
        /// <returns> A database transaction object. </returns>
        IDbTransaction BeginTransaction(IsolationLevel isolationLevel);
    }
}