﻿using System;
using Newtonsoft.Json;

namespace Ramp.Data
{
    /// <summary>
    ///		Represents a player account.
    /// </summary>
    [JsonObject]
    public class Account
    {
        /// <summary>
        ///		Gets or sets the account name.
        /// </summary>
        [JsonProperty]
        public string Name { get; set; }

        /// <summary>
        ///		Get sor sets the time the account was created.
        /// </summary>
        [JsonProperty]
        public DateTime Created { get; set; }

        /// <summary>
        ///		Gets or sets the time the account last performed a login.
        /// </summary>
        [JsonProperty]
        public DateTime LastLogin { get; set; }

        /// <summary>
        ///		Gets or sets the SHA-1 hash of the password. An empty array indicates no password was set.
        /// </summary>
        [JsonProperty]
        public byte[] PasswordHash { get; set; }
    }
}