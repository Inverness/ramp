using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using NLog;
using Microsoft.Xna.Framework;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Ramp.Input;
using Ramp.Logic;
using Ramp.Logic.Character;
using Ramp.Rendering;
using Ramp.Simulation;

namespace Ramp.Session
{
    /// <summary>
    ///     Handles top level game logic for the demo.
    /// </summary>
    public class DemoGameMaster : GameMaster
    {
        private static readonly Logger s_log = LogManager.GetCurrentClassLogger();

        protected JsonSerializer SaveSerializer = JsonSerializer.CreateDefault();

        private ICommandVariableService _cvarService;

        public CommandVariable<bool> AIUpdateEnabled { get; private set; }

        protected override void Initialize(IServiceProvider serviceProvider)
        {
            base.Initialize(serviceProvider);

            _cvarService = serviceProvider.GetRequiredService<ICommandVariableService>();

            AIUpdateEnabled = _cvarService.AddVariable("AIUpdateEnabled", true);
        }

        protected override async Task<Actor> LoginAsync(Player player, string username, string password)
        {
            Tuple<Level, Vector2?> spawnInfo = await FindPlayerSpawnAsync();

            if (spawnInfo == null)
                throw new InvalidOperationException("Player spawn not found");

            Actor actor = SpawnPlayer(player, spawnInfo.Item1, spawnInfo.Item2.GetValueOrDefault());
            RunTests(actor);

            return actor;
        }

        protected override void OnGameSaving(JObject data)
        {
            base.OnGameSaving(data);

            Actor actor = SessionService.LocalPlayerActor;

            data["Level"] = actor.Level.Name;
            data["Location"] = JToken.FromObject(actor.GetLocation(), SaveSerializer);
        }

        protected override void OnGameLoading(JObject data)
        {
            base.OnGameLoading(data);

            var levelName = data.Value<string>("Level");
            var pos = data.Value<JToken>("Location").ToObject<Vector2>(SaveSerializer);

            SessionService.LocalPlayerActor.ChangeLevel(levelName, pos);
        }

        /// <summary>
        ///     Spawn a player's actor.
        /// </summary>
        /// <param name="player"> The player requesting an actor. </param>
        /// <param name="level"> The level to spawn in. </param>
        /// <param name="location"> The location to spawn the actor at. </param>
        /// <returns> Newly spawned actor. </returns>
        protected Actor SpawnPlayer(Player player, Level level, Vector2 location)
        {
            string actorName = player is LocalPlayer ? "LocalPlayer" : null;

            Actor actor = level.SpawnActor(location, "PlayerCharacter", name: actorName);
            var ani = actor.GetComponent<AnimationRenderer>();
            var health = actor.GetComponent<HealthBehavior>();
            var inv = actor.GetComponent<ItemCollectionComponent>();
            var equip = actor.GetComponent<EquipmentBehavior>();
            Debug.Assert(ani != null, "ani != null");
            Debug.Assert(health != null, "health != null");
            Debug.Assert(inv != null, "inv != null");
            Debug.Assert(equip != null);

            //health.Damage(13, null, DamageType.Physical);

            inv.Add("Longsword", 1);
            inv.Add("Axe", 1);
            inv.Add("MagicStaff", 1);
            inv.Add("Bow03", 1);
            inv.Add("Coin", 100);

            equip.Equip("Longsword");


            // test damage against another character
            //if (SessionService.SessionType < SessionType.Client)
            //    level.Spawn(location + new Vector2(5, 0), "AICharacter", name: "TestNpc");

            return actor;
        }

        // Locate a level and location to spawn a new player at.
        protected async Task<Tuple<Level, Vector2?>> FindPlayerSpawnAsync()
        {
            Level level;

            try
            {
                // Preload the entry level perhaps?
                level = await World.LoadLevelAsync("Levels/Demo/Entry");
            }
            catch (LevelLoadException ex)
            {
                s_log.Error("Level load failed while finding player spawn", ex);
                return null;
            }

            Vector2? position = level.GetActorsWithTag("PlayerSpawn").Select(a => a.GetLocation()).FirstOrDefault();

            return Tuple.Create(level, position);
        }

        private void RunTests(Actor actor)
        {
            //Actor testNpc = actor.Level.SpawnActor(new Vector2(40, 40), "AICharacter");

            //var npc = actor.Level.GetActor("TestNpc");

            //var aic = npc.GetComponent<BodyAIBehavior>();
            //var task = aic.MoveToPosition(actor.Position + new Vector2(0, 10));

            //MoveResult result = await task;
            //var result = Level.FindPath(actor, actor.Position + new Vector2(0.25f, 10), true);
        }
    }
}