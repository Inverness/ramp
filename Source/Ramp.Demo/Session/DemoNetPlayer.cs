﻿using Lidgren.Network;

namespace Ramp.Session
{
    public class DemoNetPlayer : NetPlayer, IDemoPlayer
    {
        public DemoNetPlayer(NetConnection connection)
            : base(connection)
        {
        }
    }
}