using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using Coroutines;
using NLog;
using Microsoft.Xna.Framework;
using Ramp.Logic;
using Ramp.Logic.Character;
using Ramp.Rendering;
using Ramp.Simulation;

namespace Ramp.Session
{
    public class DemoLocalPlayer : LocalPlayer, IDemoPlayer
    {
        private static readonly Logger s_log = LogManager.GetCurrentClassLogger();

        private readonly IQuestService _questService;
        private readonly IRegionService _regionService;
        private InventoryWindow _inventoryWindow;
        private ActorDialogWindow _dialogWindow;
        private ContainerWindow _containerWindow;

        public DemoLocalPlayer(IServiceProvider services)
            : base(services)
        {
            _questService = services.GetService<IQuestService>();
            _regionService = services.GetService<IRegionService>();

            if (UIService != null)
            {
                if (_questService != null)
                    _questService.QuestStageChanged += OnQuestStageChanged;
                if (_regionService != null)
                    _regionService.RegionChanged += OnRegionChanged;
            }
        }

        public void ShowContainerWindow(Actor target)
        {
            if (UIService == null)
                return;

            if (_containerWindow == null)
            {
                _containerWindow = new ContainerWindow
                {
                    Parent = UIService.Desktop,
                    Visible = false,
                    Size = new Squid.Point(800, 600)
                };

                RenderUtil.CenterOnDesktop(_containerWindow);
            }

            _containerWindow.SetTargets(Actor, target, false);
            _containerWindow.Visible = target != null;
        }

        public void ActivateTarget()
        {
            if (!ActionInputEnabled)
                return;

            var body = Actor.GetComponent<CharacterBehavior>();
            if (body == null)
                return;
            var transform = Actor.GetComponent<TransformComponent>();
            if (transform == null)
                return;

            Vector2 playerCenter = transform.Location + new Vector2(1, 1);
            Vector2 target = playerCenter + MovementUtility.GetVector(body.LastMoveDirection) * 2;
            var boundingSphere = new BoundingSphere(new Vector3(target, 0), 1f);

            // TODO: Investigate performance of repeated lookups like this
            foreach (Actor actor in Actor.Level.GetActorsInBounds(boundingSphere))
                foreach (IActivatable activatable in actor.GetComponents<IActivatable>())
                    activatable.Activate(Actor);
        }

        public override void Dispose()
        {
            if (UIService != null)
            {
                if (_questService != null)
                    _questService.QuestStageChanged -= OnQuestStageChanged;
                if (_regionService != null)
                    _regionService.RegionChanged -= OnRegionChanged;
            }

            base.Dispose();
        }

        protected override void AddCommands()
        {
            base.AddCommands();

            if (CommandService == null)
                return;

            CommandService.AddCommand("Player.UseLeftHand", (s, a) => UseLeftHand());
            CommandService.AddCommand("Player.UseRightHand", (s, a) => UseRightHand());
            CommandService.AddCommand("Player.ActivateTarget", (s, a) => ActivateTarget());
            CommandService.AddCommand("Player.ToggleInventoryWindow", (s, a) => ToggleInventoryWindow());
            CommandService.AddCommand("Player.ToggleDialogWindow",
                                      (s, a) => ToggleDialogWindow(a.GetValueOrDefault<bool?>(0),
                                                                   a.GetValueOrDefault(1)));
        }

        protected override void RemoveCommands()
        {
            base.RemoveCommands();

            if (CommandService == null)
                return;

            CommandService.RemoveCommand("Player.UseLeftHand");
            CommandService.RemoveCommand("Player.UseRightHand");
            CommandService.RemoveCommand("Player.ActivateTarget");
            CommandService.RemoveCommand("Player.ToggleInventoryWindow");
            CommandService.RemoveCommand("Player.ToggleDialogWindow");
        }

        // These handle methods are hacks until input commands are added
        //protected override Task<bool> HandleInventoryKey()
        //{
        //	ToggleInventoryWindow();
        //	return Task.FromResult(true);
        //}

        //protected override Task<bool> HandleTestKey()
        //{
        //	ToggleTopicWindow();
        //	return Task.FromResult(true);
        //}

        //protected override async Task<bool> HandleRightHandKey()
        //{
        //	var weapon = Actor.GetComponent<WeaponUserBehavior>();
        //	if (weapon == null || weapon.IsAttacking)
        //		return false;
        //	IgnoreMovementInput(true); // stop player movement while attacking
        //	await weapon.AttackRight();
        //	IgnoreMovementInput(false);
        //	return true;
        //}

        //protected override Task<bool> HandleActionKey()
        //{
        //	ActivateInDirection();
        //	return Task.FromResult(true);
        //}

        protected override void OnActorChanged(EventArgs e)
        {
            base.OnActorChanged(e);
            if (_inventoryWindow != null)
                _inventoryWindow.Actor = Actor;
        }

        protected void UseLeftHand()
        {
            var bomb = Actor.Level.SpawnActor(Actor.GetLocation() + new Vector2(0, 6), "Bomb", Actor);
            //bomb.GetComponent<BombBehavior>().Activate(Actor);

            //var bomb = Actor.Level.SpawnActor(Actor.Position + new Vector2(0, 6), null, Actor);
            //var br = bomb.AddComponent<SpriteRenderer>();

            //br.TextureName = "Textures/TestBombs";
            //br.TexturePart = new Rectangle(0, 0, 32, 32);
            //br.Size = new Vector2(2, 2);

            //var bc = bomb.AddComponent<BoxCollider>();
            //bc.Size = new Vector2(2, 2);

            //var bb = bomb.AddComponent<BombBehavior>();
        }

        protected void UseRightHand()
        {
            if (!ActionInputEnabled)
                return;

            var itemUserBehavior = Actor.GetComponent<ItemUserBehavior>();
            if (itemUserBehavior == null || itemUserBehavior.IsUsingItem)
                return;

            itemUserBehavior.Actor.StartCoroutine(UseRightHandCor(itemUserBehavior));
        }

        protected IEnumerable UseRightHandCor(ItemUserBehavior itemUserBehavior)
        {
            IgnoreMovementInput(true);

            yield return itemUserBehavior.ActivateCor(EquipmentSlot.RightHand);

            IgnoreMovementInput(false);
        } 

        protected void ToggleInventoryWindow()
        {
            if (!MenuInputEnabled || UIService == null)
                return;

            if (_inventoryWindow == null)
            {
                _inventoryWindow = new InventoryWindow
                {
                    Parent = UIService.Desktop,
                    Size = new Squid.Point(600, 480),
                    Visible = false,
                    Actor = Actor
                };

                RenderUtil.CenterOnDesktop(_inventoryWindow);
            }

            _inventoryWindow.Actor = Actor;
            _inventoryWindow.Visible = !_inventoryWindow.Visible;
        }

        protected void ToggleDialogWindow(bool? visible, string dialogName)
        {
            if (!MenuInputEnabled || UIService == null)
                return;

            if (_dialogWindow == null)
            {
                _dialogWindow = new ActorDialogWindow(ServiceProvider)
                {
                    Parent = UIService.Desktop,
                    Visible = false
                };

                RenderUtil.CenterOnDesktop(_dialogWindow);
            }

            _dialogWindow.Visible = visible ?? !_dialogWindow.Visible;
            if (_dialogWindow.Visible)
                _dialogWindow.SetDialog(dialogName);
        }

        protected void OnQuestStageChanged(IQuestService sender, QuestStageChangedEventArgs e)
        {
            switch (e.Stage.StageType)
            {
                case QuestStageType.Start:
                    UIService.AddEventMessage("Quest Started: " + e.Quest.Name);
                    break;
                case QuestStageType.Complete:
                    UIService.AddEventMessage("Quest Completed: " + e.Quest.Name);
                    break;
                case QuestStageType.Fail:
                    UIService.AddEventMessage("Quest Failed: " + e.Quest.Name);
                    break;
            }
        }

        protected void OnRegionChanged(IRegionService sender, RegionChangedEventArgs e)
        {
            if (e.NewRegion != null)
            {
                UIService.AddEventMessage("Entered " + e.NewRegion.FullName);
                ScreenColor = e.NewRegion.ScreenColor;
            }
            else
            {
                s_log.Warn("Entered undefined region");
                ScreenColor = null;
            }
        }
    }
}