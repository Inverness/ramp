﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Ramp.Data;
using Ramp.Naming;

namespace Ramp.Rendering
{
    /// <summary>
    ///		Describes dialog for interaction between a player and NPC.
    /// </summary>
    [JsonObject]
    public class ActorDialog : Decl
    {
        public ActorDialog(string name, NamedObject outer)
            : base(name, outer)
        {
            Choices = new List<string>();
            Commands = new List<string>();
        }

        /// <summary>
        ///		Gets or sets the name of a dialog that this will automatically redirect to.
        /// </summary>
        [JsonProperty]
        public string Redirect { get; set; }

        /// <summary>
        ///		Gets or sets the tex that will be displayed.
        /// </summary>
        [JsonProperty]
        public string Text { get; set; }

        /// <summary>
        ///		Gets or sets the title that will be displayed when this dialog is a choice in another.
        /// </summary>
        [JsonProperty]
        public string Title { get; set; }

        /// <summary>
        ///		Gets a list of dialog decl names that describe choices presented when viewing this dialog.
        /// </summary>
        [JsonProperty]
        public List<string> Choices { get; private set; }

        /// <summary>
        ///		Gets a list of commands that will be executed when this dialog is displayed.
        /// </summary>
        [JsonProperty]
        public List<string> Commands { get; private set; }
    }
}