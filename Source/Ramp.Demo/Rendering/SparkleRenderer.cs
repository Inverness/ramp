﻿using System;
using Microsoft.Xna.Framework;
using Ramp.Logic;
using Ramp.Simulation;

namespace Ramp.Rendering
{
    // Particle system test
    public class SparkleRenderer : ParticleSystemRenderer
    {
        private readonly CurveValueGenerator _speedCurve = new CurveValueGenerator
        {
            CurveType = CurveType.Multiple,
            Maximum = 15,
            Minimum = 0,
            Multiplier = 4
        };

        private readonly CurveValueGenerator _speedCurve2 = new CurveValueGenerator
        {
            CurveType = CurveType.Sine,
            Maximum = 6,
            Minimum = 2,
            Multiplier = 2
        };

        public SparkleRenderer()
        {
            StartTexture.Name = "Textures/Particles/X";
            StartLifetime = TimeSpan.FromSeconds(2);
            StartColor = Color.White;
            StartSize = new Vector2(1);
            StartSpeed = 4;
            EmitRate = TimeSpan.FromSeconds(0.25);
            EmitCount = 3;
            MaxParticles = 200;
            WorldSimulation = true;
            RenderMode = SpriteRenderMode.Light;
        }

        protected override void InitializeParticle(Particle p)
        {
            base.InitializeParticle(p);

            //p.Velocity = new Vector2(Random.NextFloat(-4, 4), Random.NextFloat(-4, 4));
            p.Velocity = new Vector2(Random.NextSingle(-1, 1), Random.NextSingle(-1, 1));
            p.Rotation = Random.NextSingle(0, (float) Math.PI * 2);
            p.AngularVelocity = Random.NextSingle(-1, 1);
            p.Color = new Color(Random.NextSingle(0, 1), Random.NextSingle(0, 1), Random.NextSingle(0, 1), 1f);
        }

        protected override bool UpdateParticle(Particle p, TimeSpan elapsed)
        {
            if (!base.UpdateParticle(p, elapsed))
                return false;

            float speed = _speedCurve2.NextSingle(p.ElapsedTime.TotalSeconds);

            p.Velocity = Vector2.Normalize(p.Velocity) * speed;

            return true;
        }
    }
}