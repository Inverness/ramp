﻿using System;
using Ramp.Logic;
using Ramp.Simulation;
using Squid;

namespace Ramp.Rendering
{
    /// <summary>
    ///     A control for viewing and moving items between two actors.
    /// </summary>
    public class ContainerWindow : FullWindow
    {
        private readonly ListBox _leftItemList;
        private readonly ListBox _rightItemList;
        private ItemCollectionComponent _leftItems;
        private ItemCollectionComponent _rightItems;
        private readonly Panel _infoPanel;
        private readonly Label _infoLabel;
        private bool _readOnly;
        private bool _deselecting;

        public ContainerWindow()
        {
            CanClose = true;
            CanMove = false;

            _infoPanel = new Panel
            {
                Parent = Content,
                Style = DefaultDesktop.FrameStyleName,
                Dock = DockStyle.Bottom,
                Size = new Point(0, 120),
                ClipFrame = { Margin = new Margin(6) }
            };

            _infoLabel = new Label
            {
                Parent = _infoPanel.Content,
                AutoSize = AutoSize.HorizontalVertical,
                Position = new Point(2, 2),
                Anchor = AnchorStyles.Left | AnchorStyles.Top
            };

            _leftItemList = new ListBox
            {
                Parent = Content,
                Style = DefaultDesktop.TextBoxStyleName,
                Dock = DockStyle.Left,
                Size = new Point(350, 0),
                Multiselect = false,
                ClipFrame = { Margin = new Margin(1) }
            };

            _rightItemList = new ListBox
            {
                Parent = Content,
                Style = DefaultDesktop.TextBoxStyleName,
                Dock = DockStyle.Right,
                Size = new Point(350, 0),
                Multiselect = false,
                ClipFrame = { Margin = new Margin(1) }
            };

            RenderUtil.InitializeVScrollBar(_leftItemList.Scrollbar);
            RenderUtil.InitializeVScrollBar(_rightItemList.Scrollbar);

            _leftItemList.SelectedItemChanged += OnSelectedItemChanged;
            _rightItemList.SelectedItemChanged += OnSelectedItemChanged;
        }

        /// <summary>
        ///     Set the targets to be viewed.
        /// </summary>
        /// <param name="left"> The actor to appear on the lefthand side of the window, usually the player. </param>
        /// <param name="right"> The actor to appear on the righthand side of the window, usually a lootable container. </param>
        /// <param name="readOnly"></param>
        public void SetTargets(Actor left, Actor right, bool readOnly)
        {
            if (left == null)
                throw new ArgumentNullException(nameof(left));
            if (right == null)
                throw new ArgumentNullException(nameof(right));
            if (left == right)
                throw new ArgumentException();

            _leftItems = left.GetComponent<ItemCollectionComponent>();
            _rightItems = right.GetComponent<ItemCollectionComponent>();
            _readOnly = readOnly;

            UpdateItemList(_leftItemList, _leftItems);
            UpdateItemList(_rightItemList, _rightItems);
        }

        public void ClearTargets()
        {
            _leftItems = null;
            _rightItems = null;
            _readOnly = false;

            UpdateItemList(_leftItemList, null);
            UpdateItemList(_rightItemList, null);
        }

        protected override void OnClosing(SquidEventArgs e)
        {
            ClearTargets();
            Visible = false;

            e.Cancel = true;

            base.OnClosing(e);
        }

        private void SetItemInfo(Item item)
        {
            _infoLabel.Text = item.ItemType.FullName + "\n" + InventoryWindow.GetItemDescription(item);
        }

        // Update a list box with an item collection.
        private void UpdateItemList(ListBox list, ItemCollectionComponent source)
        {
            list.Items.Clear();

            if (source == null)
                return;

            foreach (Item item in source.Items)
            {
                ItemType type = item.ItemType;
                if (type.Tags.Contains("Hidden"))
                    continue;

                // Format item name in a "Delicious Cake" or "Delicious Cake (3)" format if the count is more than one.
                string name = item.Count == 1 ? type.FullName : string.Format("{0} ({1})", type.FullName, item.Count);

                var lbi = new ListBoxItem
                {
                    Style = "Item",
                    Text = name,
                    Tag = item,
                    Size = new Point(0, 24)
                };

                lbi.MouseEnter += OnMouseEnterListItem;

                list.Items.Add(lbi);
            }

            list.SelectedItem = null;

            list.Items.Sort((a, b) => string.Compare(a.Text, b.Text, StringComparison.Ordinal));
        }

        private void OnSelectedItemChanged(Control sender, ListBoxItem value)
        {
            if (_deselecting)
                return;
            // Deselect the item
            _deselecting = true;
            value.Selected = false;
            _deselecting = false;

            // Move the item
            ItemCollectionComponent to = ReferenceEquals(sender, _leftItemList) ? _rightItems : _leftItems;

            // Move one at a time until quantity selection dialog is added.
            to.Add((Item) value.Tag, 1);

            // Update the list to view changes
            SetTargets(_leftItems.Actor, _rightItems.Actor, _readOnly);
        }

        private void OnMouseEnterListItem(Control sender)
        {
            SetItemInfo((Item) sender.Tag);
        }
    }
}