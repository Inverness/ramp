using System;
using System.Diagnostics;
using System.Text;
using Microsoft.Xna.Framework;
using Ramp.Logic;
using Ramp.Logic.Character;
using Ramp.Simulation;
using Squid;
using Point = Squid.Point;
using Rectangle = Squid.Rectangle;

namespace Ramp.Rendering
{
    /// <summary>
    ///     Window that provides a view of an actor's inventory.
    /// </summary>
    public sealed class InventoryWindow : FullWindow
    {
        private readonly Panel _itemPanel;
        private readonly Label _itemName;
        private readonly ImageControl _itemImage;
        private readonly Label _itemText;
        private readonly ListBox _itemList;
        private Actor _actor;
        private bool _deselecting;

        public InventoryWindow()
        {
            Title = "Inventory";
            CanClose = false;
            CanMove = false;

            // Information panel on the left
            _itemPanel = new Panel
            {
                Parent = Content,
                Style = null,
                Size = new Point(300, 0),
                Dock = DockStyle.Left,
                ClipFrame = { Margin = new Margin(6) }
            };

            RenderUtil.InitializeScrollbars(_itemPanel);

            _itemImage = new ImageControl
            {
                Parent = _itemPanel.Content,
                Size = new Point(32, 32),
                Position = new Point(0, 0),
                Texture = "Textures/BasicIcons",
                TextureRect = new Rectangle(0, 32, 32, 32),
                Color = (int) Color.White.PackedValue
            };

            _itemName = new Label
            {
                Parent = _itemPanel.Content,
                Style = "HeadingText",
                AutoSize = AutoSize.HorizontalVertical,
                Position = new Point(36, 2),
                Anchor = AnchorStyles.Left | AnchorStyles.Top,
                Text = "Unknown item name"
            };

            _itemText = new Label
            {
                Parent = _itemPanel.Content,
                AutoSize = AutoSize.Vertical,
                Position = new Point(0, 38),
                Size = new Point(280, 0),
                Anchor = AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Bottom,
                TextWrap = true
            };

            // Item list on the right
            _itemList = new ListBox
            {
                Parent = Content,
                Style = "TextBox",
                Dock = DockStyle.Fill,
                Multiselect = false,
                ClipFrame = { Margin = new Margin(1) }
            };

            RenderUtil.InitializeVScrollBar(_itemList.Scrollbar);

            _itemList.SelectedItemChanged += OnSelectedItemChanged;
        }

        public Actor Actor
        {
            get { return _actor; }

            set
            {
                if (_actor == value)
                    return;
                _actor = value;
                OnActorChanged();
            }
        }

        public static string GetItemDescription(Item item)
        {
            ItemType type = item.ItemType;
            var text = new StringBuilder();

            text.Append("Value: ").Append(type.Value).Append('\n');

            if (type is WeaponItemType)
            {
                text.Append("Damage: ").Append(((WeaponItemType) type).Damage);
            }
            else if (type is ArmorItemType)
            {
                text.Append("Armor: ").Append(((ArmorItemType) type).ArmorValue);
            }

            if (!string.IsNullOrEmpty(type.Description))
            {
                text.Append("\n \n").Append(type.Description);
            }

            return text.ToString();
        }

        private void UpdateItemList()
        {
            _itemList.Items.Clear();

            if (_actor == null)
                return;

            Debug.Assert(!_actor.IsDisposed, "!_actor.IsDisposed");

            var itemCollection = _actor.GetComponent<ItemCollectionComponent>();
            var equipmentBehavior = _actor.GetComponent<EquipmentBehavior>();
            Debug.Assert(itemCollection != null && equipmentBehavior != null);

            foreach (Item item in itemCollection.Items)
            {
                ItemType type = item.ItemType;
                if (type.Tags.Contains("Hidden") || type.Tags.Contains("Coin"))
                    continue;

                string name = item.Count == 1 ? type.FullName : string.Format("{0} ({1})", type.FullName, item.Count);

                if (equipmentBehavior.IsEquipped(item))
                    name += " (Equipped)";

                var lbi = new ListBoxItem
                {
                    Style = "Item",
                    Text = name,
                    UserData = item,
                    Size = new Point(0, 24)
                };

                lbi.MouseEnter += OnListBoxItemMouseEnter;

                _itemList.Items.Add(lbi);
            }

            //Item defaultItem = _itemList.Items.Count != 0 ? (Item) _itemList.Items[0].UserData : null;
            //UpdateSelectedItemPanel(defaultItem);
        }

        private void UpdateSelectedItemPanel(Item item)
        {
            Debug.Assert(_actor == null || !_actor.IsDisposed, "_actor == null || !_actor.IsDisposed");

            if (item == null)
            {
                _itemImage.Visible = false;
                _itemName.Visible = false;
                return;
            }

            _itemImage.Visible = true;
            _itemName.Visible = true;

            ItemType type = item.ItemType;

            if (type.Icon != null)
            {
                _itemImage.Texture = type.Icon;
                _itemImage.TextureRect = type.IconPart.HasValue
                                             ? RenderUtil.ToSquidRectangle(type.IconPart.Value)
                                             : new Rectangle(0, 32, 32, 32);
                _itemImage.Color = (int) Color.White.PackedValue;
            }
            else
            {
                _itemImage.Texture = "Textures/BasicIcons";
                _itemImage.TextureRect = new Rectangle(0, 32, 32, 32);
                _itemImage.Color = (int) Color.White.PackedValue;
            }

            _itemName.Text = type.FullName;
            _itemText.Text = GetItemDescription(item);
        }

        private void ActivateItem(Item item)
        {
            var equipmentBehavior = _actor.GetComponent<EquipmentBehavior>();
            Debug.Assert(equipmentBehavior != null);

            if (equipmentBehavior.IsEquipped(item))
                equipmentBehavior.Unequip(item);
            else
                equipmentBehavior.Equip(item);

            UpdateItemList();
        }

        private void OnActorChanged()
        {
            UpdateItemList();
        }

        private void OnItemsChanged(ItemCollectionComponent sender, EventArgs e)
        {
        }

        private void OnSelectedItemChanged(Control sender, ListBoxItem value)
        {
            if (_deselecting)
                return;

            _deselecting = true;
            value.Selected = false;
            _deselecting = false;

            if (_actor == null)
                return;

            ActivateItem((Item) value.UserData);
        }

        private void OnListBoxItemMouseEnter(Control sender)
        {
            UpdateSelectedItemPanel((Item) sender.UserData);
        }
    }
}