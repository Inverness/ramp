﻿using Microsoft.Xna.Framework;
using Ramp.Network;
using Ramp.Session;
using Ramp.Simulation;

namespace Ramp.Rendering
{
    /// <summary>
    ///		Renders a character'a name beneath them.
    /// </summary>
    public class NameRenderer : Renderer
    {
        private readonly PlayerPawnBehavior _playerPawn;
        private string _text;
        private Vector2 _offset;
        private bool _showLocalPlayerName;
        private Color _color;
        private Color _shadowColor;

        public NameRenderer()
        {
            GetComponent(out _playerPawn);
            _text = Actor.GetSpawnArgument<string>("NameText");
            _offset = Actor.GetSpawnArgument("NameOffset", new Vector2(0, 2));
            _color = Actor.GetSpawnArgument("NameColor", Color.Gold);
            _shadowColor = Actor.GetSpawnArgument("NameShadowColor", Color.Black);
            Layer = Actor.GetSpawnArgument<byte>("NameLayer");
        }

        public override BoundingBox LocalBoundingBox
        {
            get
            {
                var extent = new Vector2(1, 1);
                return new BoundingBox(new Vector3(_offset - extent, 0), new Vector3(_offset + extent, 0));
            }
        }

        /// <summary>
        ///		The color to draw the text in.
        /// </summary>
        public Color Color
        {
            get { return _color; }

            set { _color = value; }
        }

        /// <summary>
        ///		The color to draw the text shadow in.
        /// </summary>
        public Color ShadowColor
        {
            get { return _shadowColor; }

            set { _shadowColor = value; }
        }

        /// <summary>
        ///		The text to be rendered.
        /// </summary>
        public string Text
        {
            get { return _text; }

            set { _text = value; }
        }

        /// <summary>
        ///		The offset of the text from the actor's origin.
        /// </summary>
        public Vector2 Offset
        {
            get { return _offset; }

            set { _offset = value; }
        }

        /// <summary>
        ///		Whether to render if the current actor is the local player's.
        /// </summary>
        public bool ShowLocalPlayerName
        {
            get { return _showLocalPlayerName; }

            set { _showLocalPlayerName = value; }
        }

        public override void Draw(ILevelObjectRenderer ri)
        {
            if (!Enabled || string.IsNullOrEmpty(_text))
                return;

            if (!_showLocalPlayerName && _playerPawn != null && _playerPawn.Player is LocalPlayer)
                return;

            // TODO: Font and size customization based on archetype or something
            ri.DrawText(_text, Transform.Location + _offset + new Vector2(0.1f, 0.1f), _shadowColor, centered: true);
            ri.DrawText(_text, Transform.Location + _offset, _color, centered: true);
        }

        public override void WriteDelta(DeltaWriter writer, ReplicationContext context)
        {
            base.WriteDelta(writer, context);
            writer.Write(_text);
            writer.Write(_offset);
            writer.Write(_showLocalPlayerName);
            writer.Write(_color);
            writer.Write(_shadowColor);
        }

        public override void ReadDelta(DeltaReader reader, ReplicationContext context)
        {
            base.ReadDelta(reader, context);
            reader.Read(ref _text);
            reader.Read(ref _offset);
            reader.Read(ref _showLocalPlayerName);
            reader.Read(ref _color);
            reader.Read(ref _shadowColor);
        }
    }
}