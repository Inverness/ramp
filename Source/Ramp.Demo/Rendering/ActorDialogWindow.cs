using System;
using NLog;
using Ramp.Input;
using Ramp.Naming;
using Squid;

namespace Ramp.Rendering
{
    public class ActorDialogWindow : Frame
    {
        private static readonly Logger s_log = LogManager.GetCurrentClassLogger();

        private readonly ICommandService _commandService;
        private readonly Panel _textPanel;
        private readonly Label _textLabel;
        private readonly ListBox _choiceList;
        private readonly NamedDirectory _directory;
        private bool _deselecting;

        public ActorDialogWindow(IServiceProvider services)
        {
            if (services == null)
                throw new ArgumentNullException(nameof(services));

            _commandService = services.GetRequiredService<ICommandService>();
            _directory = NamedDirectory.Current;

            Style = DefaultDesktop.FrameStyleName;
            Size = new Point(694, 350);
            Padding = new Margin(6);

            _choiceList = new ListBox
            {
                Parent = this,
                Style = DefaultDesktop.TextBoxStyleName,
                Dock = DockStyle.Bottom,
                Size = new Point(0, 128),
                Multiselect = false
            };

            RenderUtil.InitializeScrollbars(_choiceList);

            _textPanel = new Panel
            {
                Parent = this,
                Style = DefaultDesktop.TextBoxStyleName,
                Dock = DockStyle.Fill,
                ClipFrame = { Margin = new Margin(6) }
            };

            RenderUtil.InitializeScrollbars(_textPanel);

            _textLabel = new Label
            {
                Parent = _textPanel.Content,
                Dock = DockStyle.Fill,
                TextWrap = true,
                TextAlign = Alignment.TopLeft
            };

            _choiceList.SelectedItemChanged += OnChoiceListSelectedItemChanged;
        }

        public void SetDialog(string name)
        {
            if (name == null)
            {
                _textLabel.Text = string.Empty;
                _choiceList.Items.Clear();
                return;
            }

            // First, get the matching dialog in order to set the text
            var dialog = _directory.Find<ActorDialog>(name);
            if (dialog == null)
                throw new ArgumentException("Invalid actor dialog name: " + name);

            bool redirecting = false;
            if (dialog.Redirect != null)
            {
                if (_directory.Find<ActorDialog>(dialog.Redirect) == null)
                    throw new ArgumentException("Invalid redirect name: " + dialog.Redirect);
                redirecting = true;
            }

            // Finally, execute commands as necessary
            foreach (string command in dialog.Commands)
            {
                CommandResult result = _commandService.Execute(command);
                if (!result.Handled)
                    throw new ArgumentException("Unknown dialog command: " + command);
            }

            if (redirecting)
            {
                SetDialog(dialog.Redirect);
                return;
            }

            _textLabel.Text = dialog.Text;

            // Update choices
            _choiceList.Items.Clear();
            int count = 0;
            foreach (string choiceDialogName in dialog.Choices)
            {
                var choiceDialog = _directory.Find<ActorDialog>(choiceDialogName);
                if (choiceDialog == null)
                    throw new ArgumentException("Invalid choice dialog name: " + choiceDialogName);

                var item = new ListBoxItem
                {
                    Style = DefaultDesktop.ItemStyleName,
                    TextAlign = Alignment.MiddleLeft,
                    Text = $"{++count}. {choiceDialog.Title}",
                    Size = new Point(0, 24),
                    Margin = new Margin(1),
                    Tag = choiceDialog.Name
                };

                _choiceList.Items.Add(item);
            }
        }

        private void OnChoiceListSelectedItemChanged(Control sender, ListBoxItem value)
        {
            if (_deselecting)
                return;

            // Topic items act as buttons, so deselect immediately
            _deselecting = true;
            value.Selected = false;
            _deselecting = false;

            var dialogName = (string) value.Tag;
            try
            {
                SetDialog(dialogName);
            }
            catch (ArgumentException ex)
            {
                s_log.Error("Exception while setting dialog to {0}: {1}", dialogName, ex.Message);
            }
        }
    }
}