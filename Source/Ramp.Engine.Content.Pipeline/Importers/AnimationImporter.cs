﻿using Microsoft.Xna.Framework.Content.Pipeline;
using Ramp.Data;
using Ramp.Rendering;

namespace Ramp.Content.Pipeline.Importers
{
    [ContentImporter(".ani.yaml", DisplayName = "Ramp Animation Importer", DefaultProcessor = "PassThroughProcessor")]
    public class AnimationImporter : YamlToJsonDeserializingImporter<Animation>
    {
        public override Animation Import(string filename, ContentImporterContext context)
        {
            using (XnaConverter.AddTypeConverters())
                return base.Import(filename, context);
        }
    }
}