﻿using System.IO;
using Microsoft.Xna.Framework.Content.Pipeline;
using Newtonsoft.Json.Linq;
using Ramp.Data;
using YamlDotNet.RepresentationModel;

namespace Ramp.Content.Pipeline.Importers
{
    /// <summary>
    ///     Imports a YAML document as a JContainer.
    /// </summary>
    [ContentImporter(".yaml", DisplayName = "YAML to JSON Importer", DefaultProcessor = "PassThroughProcessor")]
    public class YamlToJsonImporter : ContentImporter<JContainer>
    {
        public override JContainer Import(string filename, ContentImporterContext context)
        {
            using (var streamReader = new StreamReader(filename))
            {
                var ys = new YamlStream();
                ys.Load(streamReader);
                return YamlConverter.ToJson(ys.Documents[0]);
            }
        }
    }
}