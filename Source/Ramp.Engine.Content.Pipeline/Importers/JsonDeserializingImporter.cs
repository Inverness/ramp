﻿using System.IO;
using Microsoft.Xna.Framework.Content.Pipeline;
using Newtonsoft.Json;

namespace Ramp.Content.Pipeline.Importers
{
    public abstract class JsonDeserializingImporter<T> : ContentImporter<T>
    {
        private JsonSerializer _serializer;

        public virtual JsonSerializerSettings Settings => new JsonSerializerSettings();

        public override T Import(string filename, ContentImporterContext context)
        {
            if (_serializer == null)
                _serializer = JsonSerializer.CreateDefault(Settings);

            using (var reader = new JsonTextReader(new StreamReader(filename)))
                return _serializer.Deserialize<T>(reader);
        }
    }
}