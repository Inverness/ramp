﻿using System.IO;
using Microsoft.Xna.Framework.Content.Pipeline;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Ramp.Content.Pipeline.Importers
{
    /// <summary>
    ///     Imports a JSON document as a JObject.
    /// </summary>
    [ContentImporter(".json", ".js", DisplayName = "JSON Importer", DefaultProcessor = "PassThroughProcessor")]
    public class JsonImporter : ContentImporter<JContainer>
    {
        public override JContainer Import(string filename, ContentImporterContext context)
        {
            using (var streamReader = new StreamReader(filename))
            using (var reader = new JsonTextReader(streamReader))
            {
                return (JContainer) JToken.Load(reader);
            }
        }
    }
}