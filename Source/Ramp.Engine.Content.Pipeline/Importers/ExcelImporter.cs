﻿using Microsoft.Xna.Framework.Content.Pipeline;
using Ramp.Data;

namespace Ramp.Content.Pipeline.Importers
{
    [ContentImporter(".xlsx", DisplayName = "Excel Spreadsheet Importer", DefaultProcessor = "PassThroughProcessor")]
    public class ExcelImporter : ContentImporter<DataTableSource>
    {
        public override DataTableSource Import(string filename, ContentImporterContext context)
        {
            var dts = new DataTableSource();

            using (var reader = new XlsxReader(filename))
            {
                foreach (string s in reader.GetSheetNames())
                {
                    dts.Tables.Add(s, reader.ReadSheet(s));
                }
            }

            return dts;
        }
    }
}
