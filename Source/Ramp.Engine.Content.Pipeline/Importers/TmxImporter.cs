﻿using Microsoft.Xna.Framework.Content.Pipeline;
using Ramp.Simulation.Tmx;

namespace Ramp.Content.Pipeline.Importers
{
    [ContentImporter(".tmx", DisplayName = "TMX Level Importer", DefaultProcessor = "TmxProcessor")]
    public class TmxImporter : ContentImporter<TmxLoader>
    {
        static TmxImporter()
        {
            Logging.Initialize();
        }

        public override TmxLoader Import(string filename, ContentImporterContext context)
        {
            var loader = new TmxLoader();
            loader.Load(filename);
            return loader;
        }
    }
}