using Microsoft.Xna.Framework.Content.Pipeline;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Ramp.Data;

namespace Ramp.Content.Pipeline.Importers
{
    public abstract class YamlToJsonDeserializingImporter<T> : ContentImporter<T>
    {
        private JsonSerializer _serializer;

        public virtual JsonSerializerSettings Settings => new JsonSerializerSettings();

        public override T Import(string filename, ContentImporterContext context)
        {
            JContainer root = JsonUtility.LoadYaml(filename, Settings);

            if (_serializer == null)
                _serializer = JsonSerializer.CreateDefault(Settings);

            using (var reader = new JTokenReader(root))
                return _serializer.Deserialize<T>(reader);
        }
    }
}