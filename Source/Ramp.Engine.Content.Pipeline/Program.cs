﻿using System;

namespace Ramp.Content.Pipeline
{
    public static class Program
    {
        public static int Main(string[] args)
        {
            Console.Error.WriteLine("Reference this assembly with the MonoGame Content Builder");
            return 1;
        }
    }
}
