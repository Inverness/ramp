﻿using Microsoft.Xna.Framework.Content.Pipeline;
using Ramp.Simulation.Tmx;

namespace Ramp.Content.Pipeline.Processors
{
    [ContentProcessor(DisplayName = "TMX Level Processor")]
    public class TmxProcessor : ContentProcessor<TmxLoader, TmxLoader>
    {
        public TmxProcessor()
        {
            TileTextureDirectory = "Textures/Tiles/";
        }

        public override TmxLoader Process(TmxLoader input, ContentProcessorContext context)
        {
            if (!string.IsNullOrEmpty(TileTextureDirectory))
                input.UpdateTilesetTextures(TileTextureDirectory);
            return input;
        }

        public string TileTextureDirectory { get; set; }
    }
}