﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;

namespace Ramp.Content.Pipeline.Writers
{
    public static class ContentWriterExtensions
    {
        public static void Write(this ContentWriter writer, Rectangle value)
        {
            writer.Write(value.X);
            writer.Write(value.Y);
            writer.Write(value.Width);
            writer.Write(value.Height);
        }

        public static void Write(this ContentWriter writer, Point value)
        {
            writer.Write(value.X);
            writer.Write(value.Y);
        }
    }
}