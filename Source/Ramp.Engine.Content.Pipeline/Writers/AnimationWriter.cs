﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;
using Ramp.Rendering;

namespace Ramp.Content.Pipeline.Writers
{
    //[ContentTypeWriter]
    //public class AnimationWriter : BsonWriter<Animation>
    //{
    //    public override string GetRuntimeReader(TargetPlatform targetPlatform)
    //    {
    //        return "Ramp.Content.AnimationReader, Ramp.Engine";
    //    }

    //    protected override void Write(ContentWriter output, object value)
    //    {
    //        using (XnaConverter.AddTypeConverters())
    //            base.Write(output, value);
    //    }
    //}

    [ContentTypeWriter]
    internal class AnimationWriter : ContentTypeWriter<Animation>
    {
        public override string GetRuntimeReader(TargetPlatform targetPlatform)
        {
            return "Ramp.Content.AnimationReader, Ramp.Engine";
        }

        protected override void Write(ContentWriter output, Animation a)
        {
            output.Write(a.Loop);
            output.Write(a.Directional);
            output.Write(a.SpriteTypes.Count);
            output.Write(a.Frames.Count);

            foreach (KeyValuePair<string, AnimationSpriteType> st in a.SpriteTypes)
            {
                output.Write(st.Key);
                output.Write(st.Value.TextureName);
                output.Write(st.Value.TextureRect);
            }

            foreach (KeyValuePair<string, List<AnimationFrame>> fl in a.Frames)
            {
                output.Write(fl.Key);
                output.Write(fl.Value.Count);

                foreach (AnimationFrame f in fl.Value)
                {
                    output.Write(f.Duration);
                    output.Write(f.Sprites.Count);
                    output.Write(f.Events.Count);

                    foreach (AnimationSprite s in f.Sprites)
                    {
                        output.Write(s.SpriteType);
                        output.Write(s.Position);
                        output.Write(s.Color);
                        output.Write(s.Rotation);
                    }

                    foreach (string e in f.Events)
                        output.Write(e);
                }
            }
        }
    }
}