﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;
using Ramp.Data;

namespace Ramp.Content.Pipeline.Writers
{
    [ContentTypeWriter]
    internal class DataTableSourceWriter : ContentTypeWriter<DataTableSource>
    {
        public override string GetRuntimeReader(TargetPlatform targetPlatform)
        {
            return "Ramp.Content.DataTableSourceReader, Ramp.Engine";
        }

        protected override void Write(ContentWriter output, DataTableSource value)
        {
            output.Write(value.Tables.Count);

            foreach (KeyValuePair<string, string[,]> table in value.Tables)
            {
                int rowCount = table.Value.GetLength(0);
                int colCount = table.Value.GetLength(1);
                string[,] data = table.Value;

                output.Write(table.Key);
                output.Write(rowCount);
                output.Write(colCount);

                for (int r = 0; r < rowCount; r++)
                {
                    for (int c = 0; c < colCount; c++)
                    {
                        output.Write(data[r, c] ?? DataTableSource.NullString);
                    }
                }
            }
        }
    }
}
