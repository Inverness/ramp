﻿using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;
using Newtonsoft.Json.Linq;

namespace Ramp.Content.Pipeline.Writers
{
    [ContentTypeWriter]
    public class JObjectWriter : BsonWriter<JObject>
    {
        public override string GetRuntimeReader(TargetPlatform targetPlatform)
        {
            return "Ramp.Content.JObjectReader, Ramp.Core";
        }
    }
}