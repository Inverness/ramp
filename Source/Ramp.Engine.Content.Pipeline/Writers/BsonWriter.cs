﻿using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;

namespace Ramp.Content.Pipeline.Writers
{
    /// <summary>
    ///     Serializes an object as BSON.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class BsonWriter<T> : ContentTypeWriter<T>
    {
        /// <summary>
        ///     The current serializer. This is initialized during the first call to Write().
        /// </summary>
        protected JsonSerializer Serializer;

        /// <summary>
        ///     Gets the settings that will be used to create the serializer.
        /// </summary>
        protected virtual JsonSerializerSettings Settings => new JsonSerializerSettings();

        protected override void Write(ContentWriter output, T value)
        {
            if (Serializer == null)
                Serializer = JsonSerializer.CreateDefault(Settings);

            using (var writer = CreateWriter(output))
            {
                writer.CloseOutput = false;
                Serializer.Serialize(writer, value);
            }
        }

        protected virtual BsonDataWriter CreateWriter(ContentWriter output)
        {
            return new BsonDataWriter(output);
        }
    }
}