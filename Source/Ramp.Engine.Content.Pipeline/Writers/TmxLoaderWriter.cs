﻿using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;
using Ramp.Data;
using Ramp.Simulation.Tmx;

namespace Ramp.Content.Pipeline.Writers
{
    [ContentTypeWriter]
    public class TmxLoaderWriter : BsonWriter<TmxLoader>
    {
        public override string GetRuntimeReader(TargetPlatform targetPlatform)
        {
            return "Ramp.Content.TmxLoaderReader, Ramp.Engine";
        }

        protected override void Write(ContentWriter output, object value)
        {
            using (XnaConverter.AddTypeConverters())
                base.Write(output, value);
        }
    }
}