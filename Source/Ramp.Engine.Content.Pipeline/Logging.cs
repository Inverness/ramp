﻿using System;
using NLog;
using NLog.Config;
using NLog.Targets;

namespace Ramp.Content.Pipeline
{
    internal static class Logging
    {
        private static bool s_initialized;

        public static void Initialize()
        {
            if (s_initialized)
                return;

            s_initialized = true;

            LogLevel minLevel =
#if DEBUG
                LogLevel.Debug;
#else
                LogLevel.Info;
#endif

            var consoleTarget = new ConsoleTarget
            {
                Layout = "${level:uppercase=true} ${logger} - ${message}",
                Name = "console"
            };

            if (LogManager.Configuration == null)
                LogManager.Configuration = new LoggingConfiguration();

            LogManager.Configuration.AddTarget(consoleTarget);
            
            LogManager.Configuration.AddRule(minLevel, LogLevel.Fatal, consoleTarget);

            LogManager.ReconfigExistingLoggers();
        }

        public static Logger GetLogger(Type type)
        {
            Initialize();
            return LogManager.GetLogger(type.FullName);
        }
    }
}